#!/usr/bin/python

import os
import os.path
import sys

def findFiles(dir, suffix, files):
	if (dir.endswith(".svn")):
		return
	try:
		names = os.listdir(dir)
	except os.error, msg:
		sys.stderr.write("Can't list directory: %s\n" % dir)
		return 1
	for name in names:
		fn = dir + "/" + name
		if os.path.isdir(fn):
			findFiles(fn, suffix, files)
		else:
			if len(suffix)==0:
				files.append(fn)
			else:
				for s in suffix:
					if os.path.normcase(fn).endswith(s):
						files.append(fn)

def getFileActions(platform, inFile, outDir, outFiles, commands):
	outPath = outDir + "/" + inFile[5:]
	outPath = outPath.replace(platform+"__", "");

	inFileOrg = inFile
	inFile = inFile.replace(platform+"__", "");

	if platform=="ios":
			
		compress = False
		
		if inFile.find("gfx/") != -1:
			compress = True

		if inFile.find("gfx/apple.png") != -1:
			compress = False

		if inFile.find("decals/") != -1:
			compress = True

		if inFile.find("loading.jpg") != -1:
			compress = False

		if inFile.find("menu/world") != -1 and inFile.find("_bonus") == -1:
			compress = True

		if inFile.find("menu/shop") != -1 and inFile.find("_button") == -1:
			compress = True

		if inFile.find("menu/") != -1 and inFile.find("_select.jpg") != -1:
			compress = True

		if inFile.find("menubg.jpg") != -1:
			compress = True
        
		if inFile.find(".jpg") == -1 and inFile.find(".png") == -1:
			compress = False

		if compress:
			compressedPath = outPath + ".pvrtc" 
			outFiles.append(compressedPath)
			commands.append("texturetool -f pvr -e PVRTC -m --channel-weighting-perceptual --bits-per-pixel-4 --alpha-is-opacity -o " + compressedPath + " " + inFileOrg)
			#outFiles.append(outPath)
			#commands.append("cp " + inFileOrg + " " + outPath)
		else:
			if inFile.find(".png") != -1:
				mtxPath = outPath + ".mtx"
				outFiles.append(mtxPath)
				commands.append("mtx/mtx " + inFileOrg + " " + mtxPath)
			else:
				outFiles.append(outPath)
				commands.append("cp " + inFileOrg + " " + outPath)

	elif platform=="android":
		compress = False
		
		if inFile.find("levels/") != -1 and inFile.find(".xml") != -1:
			compress = True

		if inFile.find("_retina.") != -1:
			return

		if inFile.find(".motion") != -1:
			compress = True

		if compress:
			compressedPath = outPath + ".gz.mp3"
			outFiles.append(compressedPath)
			commands.append("cp " + inFileOrg + " " + compressedPath + "; gzip -f " + compressedPath + "; mv " + compressedPath + ".gz " + compressedPath)
		else:
			if inFile.find(".png") != -1:
				mtxPath = outPath + ".mtx"
				outFiles.append(mtxPath + ".mp3")
				commands.append("mtx/mtx " + inFileOrg + " " + mtxPath + ".mp3")
			else:
				outFiles.append(outPath + ".mp3")
				commands.append("cp " + inFileOrg + " " + outPath + ".mp3")

	else:
		outFiles.append(outPath)
		commands.append("cp " + inFileOrg + " " + outPath)

def makeplatform(files, platform):
	res = ""
	out = "data_" + platform
	targets = ""
	for file in files:
		if file.find("DS_Store") != -1:
			continue
		if file.find("svn-commit") != -1:
			continue
			
		#For specific platform but not this platform
		if file.find("__")!=-1 and file.find(platform+"__")==-1:
			continue

		#Has platform specific version
		tmp = os.path.split(file)
		psFile = tmp[0] + "/" + platform + "__" + tmp[1]
		if file.find("__")==-1 and os.path.exists(psFile):
			continue
			
		outFiles = []
		commands = []
		getFileActions(platform, file, out, outFiles, commands)
		for i in range(0,len(outFiles)):
			outFile = outFiles[i]
			res = res + outFile + ": " + file + "\n"
			res = res + "\t@echo Creating " + outFile + "\n"
			res = res + "\t@mkdir -p " + os.path.dirname(outFile) + "\n"
			res = res + "\t@" + commands[i] + "\n"
			targets = targets + outFile + " "

	tmp = platform + ": " + targets + "\n\n"
	tmp = tmp + res
	tmp = tmp + "\n"
	tmp = tmp + "clean_" + platform + ":\n\trm -Rf data_" + platform + "\n"
	return tmp

files = []
findFiles("data", [], files)	
mk = makeplatform(files, "ios") + "\n"
mk = mk + makeplatform(files, "android") + "\n"
mk = mk + "clean: clean_ios clean_android\n"
open("makefile.assets", "w").write(mk)

args = ""
for a in sys.argv:
	args = args + a + " "

os.system("make -f makefile.assets " + args)
