sceneLength = {}
sceneStart = {}
sceneFunc = {}
sceneCalled = {}

--Tweak time offset
timeOffset = 0

function init()
	-- Add all scenes here
	addScene(sceneFirst, 25.0)
	addScene(sceneEnd, 1.0)

	ending_bg = mgCreateImage("ending.jpg")
	ending_fg = mgCreateImage("ending_fg.png")
	black = mgCreateImage("black.jpg")
	
	canvas = mgCreateCanvas(1024, 768)
	
	local current = 0.0
	for i=1, #sceneLength do
		sceneStart[i] = current
		current = current + sceneLength[i]
	end	

	mgCommand("audio.playBackgroundMusic movie/intro.ogg")
end


function addScene(func, length)
	sceneFunc[#sceneFunc+1] = func
	sceneLength[#sceneLength+1] = length
	sceneCalled[#sceneLength+1] = false
end


function getScene(time)
	local t = time + timeOffset
	scene = 0
	sceneTime = 0.0
	for i=1, #sceneStart do
		if (t > sceneStart[i]) then
			scene = i
			sceneTime = t-sceneStart[i]
		end
	end
	return scene, sceneTime
end


function frame()
end


function draw()
	time = mgScriptTime()
	local scene = 0
	local t = 0.0
	scene, t = getScene(time)
	sceneFunc[scene](t, not sceneCalled[scene])
	sceneCalled[scene] = true
end


----------------- SCENES -----------------


function sceneFirst(t, first)
	if first then
		mgSetScale(canvas, 1.5, 1.5)
		mgSetPos(canvas, 0, -150)
		mgSetScale(ending_fg, 1.7, 1.7)
		mgSetPos(ending_fg, 710, 450)
		mgSetPos(canvas, 0, -150)
		mgSetPos(black, 400, 0)
		mgSetScale(black, 20, 15)
		mgSetAlpha(black, 0)
	end
	
	mgPushCanvas(canvas)
	mgDraw(ending_bg)
	mgDraw(ending_fg)
	mgSetScale(canvas, 1.0, 1.0, "easeout", 10)
	mgSetPos(canvas, -450, 0, "easeout", 10)
	mgSetScale(ending_fg, 1, 1, "easeout", 10)
	mgDraw(black)

	if(t > 24) then
		mgSetAlpha(black, 1, "linear", .3)
	end

	mgPopCanvas()
end


function sceneEnd(t, first)
	if first then
		mgCommand("game.menu")
	end
end

