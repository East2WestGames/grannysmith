sceneLength = {}
sceneStart = {}
sceneFunc = {}
sceneCalled = {}

--Tweak time offset
timeOffset = 2

function init()
	-- Add all scenes here
	addScene(sceneFirst, 8.0)
	addScene(sceneCloseup, 5.0)
	addScene(sceneThief, 8.0)
	addScene(sceneShed, 5.0)
	addScene(sceneBlack, 1.4)
	addScene(sceneEnd, 1.0)

	bg_garden = mgCreateImage("bg_garden.jpg")
	garden_grammophone = mgCreateImage("garden_grammophone.png")
	garden_chair = mgCreateImage("garden_chair.png")
	garden_granny = mgCreateImage("garden_granny.png")
	garden_granny_surprise = mgCreateImage("garden_granny_surprise.png")
	garden_motion_blur = mgCreateImage("garden_motion_blur.png")
	garden_apple = mgCreateImage("garden_apple.png")
	garden_apple2 = mgCreateImage("garden_apple2.png")
	garden_apple3 = mgCreateImage("garden_apple.png")
	garden_apple_thief = mgCreateImage("garden_apple.png")
	garden_thief = mgCreateImage("garden_thief.png")
	garden_arm = mgCreateImage("garden_thief_arm.png")
	garden_shadow = mgCreateImage("shadow.png")
	garden_mask = mgCreateImage("garden_mask.png")

	bg_closeup = mgCreateImage("bg_closeup.jpg")
	closeup_granny = mgCreateImage("closeup_granny.png")
	closeup_apple = mgCreateImage("closeup_apple.png")
	closeup_hankie = mgCreateImage("closeup_hankie.png")
	closeup_heart = mgCreateImage("closeup_heart.png")
	closeup_heart2 = mgCreateImage("closeup_heart.png")

	bg_shed = mgCreateImage("bg_shed.jpg")
	shed_granny = mgCreateImage("shed_granny.png")
	shed_arm = mgCreateImage("shed_arm.png")
	shed_arm_back = mgCreateImage("shed_arm.png")
	shed_skates = mgCreateImage("shed_skates.png")
	shed_skates_dirty = mgCreateImage("shed_skates_dirty.png")
	shed_blow = mgCreateImage("shed_blow.png")
	shed_dust = mgCreateImage("shed_dust.png")

	black = mgCreateImage("black.jpg")
	
	canvas = mgCreateCanvas(1024, 768)
	canvas2 = mgCreateCanvas(1024, 768)
	canvas3 = mgCreateCanvas(1024, 768)
	canvas4 = mgCreateCanvas(1024, 768)
	
	local current = 0.0
	for i=1, #sceneLength do
		sceneStart[i] = current
		current = current + sceneLength[i]
	end	

	mgCommand("audio.playBackgroundMusic movie/intro.ogg")
end


function addScene(func, length)
	sceneFunc[#sceneFunc+1] = func
	sceneLength[#sceneLength+1] = length
	sceneCalled[#sceneLength+1] = false
end


function getScene(time)
	local t = time + timeOffset
	scene = 0
	sceneTime = 0.0
	for i=1, #sceneStart do
		if (t > sceneStart[i]) then
			scene = i
			sceneTime = t-sceneStart[i]
		end
	end
	return scene, sceneTime
end


function frame()
end


function draw()
	time = mgScriptTime()
	local scene = 0
	local t = 0.0
	scene, t = getScene(time)
	sceneFunc[scene](t, not sceneCalled[scene])
	sceneCalled[scene] = true
end


----------------- SCENES -----------------


function sceneFirst(t, first)
	if first then
		mgSetScale(canvas, 1, 1)
		mgSetScale(canvas, 1.1, 1.1, "cosine", 8)
		mgSetPos(canvas, -200, 0)
		mgSetPos(canvas, 0, -75, "cosine", 8)

		mgSetScale(garden_apple, 1, 1)
		mgSetPos(garden_apple, 890, 210)

		mgSetScale(garden_apple2, 1, 1)
		mgSetPos(garden_apple2, 840, 300)

		mgSetScale(garden_apple3, .8, .8)
		mgSetPos(garden_apple3, 970, 300)

		mgSetScale(garden_chair, 1.18, 1.18)
		mgSetOrigo(garden_chair, "pixel", 128, 250)
		mgSetPos(garden_chair, 280, 620)

		mgSetScale(garden_granny, 1.18, 1.18)
		mgSetOrigo(garden_granny, "pixel", 128, 250)
		mgSetPos(garden_granny, 280, 620)

		mgSetScale(garden_grammophone, 1.38, 1.38)
		mgSetPos(garden_grammophone, -35, 315)
	end

	mgSetRot(garden_chair, math.sin(t)/6)
	mgSetRot(garden_granny, math.sin(t)/6)
	
	mgPushCanvas(canvas)
	mgDraw(bg_garden)
	mgDraw(garden_apple)
	mgDraw(garden_apple2)
	mgDraw(garden_apple3)
	mgDraw(garden_chair)
	mgDraw(garden_granny)
	mgDraw(garden_grammophone)
	mgPopCanvas()
end



function sceneCloseup(t, first)
	if first then
		mgSetScale(canvas2, 1, 1, 0)
		mgSetOrigo(canvas2, "center")

		mgSetOrigo(bg_closeup, "center")
		mgSetPos(bg_closeup, 512, 380)

		mgSetPos(canvas2, 512, 384, 0)

		mgSetOrigo(closeup_granny, "center")
		mgSetPos(closeup_granny, 510, 350, 0)
		mgSetScale(closeup_granny, 1.64, 1.64)

		mgSetOrigo(closeup_apple, "center")
		mgSetScale(closeup_apple, 1.2, 1.2, 0)

		mgSetOrigo(closeup_hankie, "pixel", 58, 202)
		mgSetPos(closeup_hankie, 280, 625, 0)
		mgSetScale(closeup_hankie, 1.15, 1.15, 0)

		mgSetScale(closeup_heart, 0, 0, 0)
		mgSetOrigo(closeup_heart, "center")
		mgSetScale(closeup_heart2, 0, 0, 0)
		mgSetOrigo(closeup_heart2, "center")

		mgSetOrigo(black, "center")
		mgSetPos(black, 512, 384)
		mgSetScale(black, 16, 12)
		mgSetAlpha(black, 0)
	end

	if(t > 3 and t < 3.1) then
		mgSetPos(closeup_heart, 405, 218, 0)
		mgSetScale(closeup_heart, 1, 1, "bounce", 1)
	end

	if(t > 3.2 and t < 3.3) then
		mgSetPos(closeup_heart2, 620, 218, 0)
		mgSetScale(closeup_heart2, 1, 1, "bounce", 1.5)
	end

	if(t > 4.6 and t < 4.7) then
		mgSetAlpha(black, 1, "linear", .2)
	end

	mgSetRot(closeup_hankie, math.sin(t)/4-.1)
	mgSetRot(closeup_apple, math.sin(t/2)/4)
	mgSetPos(closeup_apple, 530, 600+2*math.sin(t))

	mgSetScale(bg_closeup, 1.25+math.sin(t)/6, 1+math.sin(t)/6, 0)

	mgPushCanvas(canvas2)
	mgDraw(bg_closeup)
	mgDraw(closeup_granny)
	mgDraw(closeup_apple)
	mgDraw(closeup_hankie)
	mgDraw(closeup_heart)
	mgDraw(closeup_heart2)
	mgDraw(black)
	mgPopCanvas()
end



function sceneThief(t, first)
	if first then
		mgSetOrigo(black, "center")
		mgSetPos(black, 512, 384)
		mgSetScale(black, 18, 12)
		mgSetAlpha(black, 1)
		mgSetAlpha(black, 0, "linear", .2)

		mgSetScale(canvas3, 1, 1)
		mgSetPos(canvas3, -50, 0)
		mgSetPos(canvas3, -400, 0, "easeout", 10)

		mgSetScale(garden_apple, 1, 1)
		mgSetPos(garden_apple, 890, 210)

		mgSetScale(garden_apple2, 1, 1)
		mgSetPos(garden_apple2, 840, 300)

		mgSetScale(garden_apple3, .8, .8)
		mgSetPos(garden_apple3, 970, 300)

		mgSetScale(garden_granny, 1.18, 1.18)
		mgSetOrigo(garden_granny, "pixel", 128, 250)
		mgSetPos(garden_granny, 280, 620)
		mgSetScale(garden_chair, 1.18, 1.18)
		mgSetOrigo(garden_chair, "pixel", 128, 250)
		mgSetPos(garden_chair, 280, 620)
		mgSetScale(garden_granny_surprise, 1.18, 1.18)
		mgSetOrigo(garden_granny_surprise, "pixel", 128, 250)
		mgSetPos(garden_granny_surprise, 280, 620)
		mgSetAlpha(garden_granny_surprise, 0)

		mgSetScale(garden_grammophone, 1.38, 1.38)
		mgSetPos(garden_grammophone, -35, 315)
				
		mgSetPos(garden_thief, 280, 330)
		mgSetAlpha(garden_thief, 0)
		mgSetOrigo(garden_arm, "center")
		mgSetPos(garden_arm, 205, 390)
		mgSetAlpha(garden_arm, 0)
		mgSetPos(garden_shadow, 250, 270)
		mgSetAlpha(garden_shadow, 0)
		mgSetPos(garden_mask, 367, 307)
		mgSetPos(garden_apple_thief, 205, 390)
		mgSetAlpha(garden_apple_thief, 0)
		mgSetScale(garden_apple_thief, .9, .9)

		mgSetPos(garden_motion_blur, 240, 325)
		mgSetScale(garden_motion_blur, 1, 2.2)
		mgSetAlpha(garden_motion_blur, 0)

		thiefJump = 1
	end

	mgSetRot(garden_chair, math.sin(t)/6)
	mgSetRot(garden_granny, math.sin(t)/6)
	mgSetRot(garden_granny_surprise, math.sin(t)/6)

	if(t > 2) then
		mgSetPos(garden_thief, t*530-785, 295+math.sin(t*2+2.2)*55-thiefJump)
		mgSetPos(garden_arm, t*530-750, 355+math.sin(t*2+2.2)*55-thiefJump)
		mgSetPos(garden_shadow, t*500-740, 465+math.sin(t*1.9+2.2)*55)
		mgSetPos(garden_apple_thief, t*530-715, 300+math.sin(t*2+2.2)*55-thiefJump)
	end

	if(t > 2.15) then
		mgSetAlpha(garden_thief, 1, "linear", .2)
		mgSetAlpha(garden_arm, 1, "linear", .2)
		mgSetAlpha(garden_shadow, .4, "linear", .2)
	end

	if(t > 2.6) then
		mgSetRot(garden_arm, .6, "easeout", .3)
	end

	if(t > 2.7) then
		mgSetScale(garden_thief, 1, .8, "linear", .2)
	end

	if(t > 2.8 and t < 3.8) then
		thiefJump = math.sin((t-.3)*5)*70
		mgSetRot(garden_thief, .1, "bounce", 1)
	end

	if(t > 2.9) then
		mgSetScale(garden_thief, 1, 1, "easeout", .5)
	end

	if(t > 2.8 and t < 2.9) then
		mgSetAlpha(garden_apple2, 0, "linear", 0.1)
		mgSetPos(garden_apple2, 840, 350, "linear", 0.1)
		mgSetAlpha(garden_apple_thief, 1, "linear", .1)
	end

	if(t > 3 and t < 3.1) then
		mgSetAlpha(garden_apple, 0, "linear", 0.1)
		mgSetPos(garden_apple, 890, 260, "linear", 0.1)
	end

	if(t > 3.1 and t < 3.2) then
		mgSetAlpha(garden_apple3, 0, "linear", 0.1)
		mgSetPos(garden_apple3, 970, 350, "linear", 0.1)
	end

	if(t > 3.5 and t < 3.6) then
		mgSetPos(canvas3, -50, -300, "easeout", .5)
		mgSetScale(canvas3, 1.5, 1.5, "easeout", .5)
		mgSetAlpha(garden_granny, 0, "linear", .1)
		mgSetAlpha(garden_granny_surprise, 1, "linear", .05)
	end

	if(t > 5 and t < 5.1) then
		mgSetAlpha(garden_granny_surprise, 0, "linear", .1)
		mgSetScale(garden_motion_blur, 6, 2.2, "linear", .2)
		mgSetAlpha(garden_motion_blur, 1, "linear", .2)
	end

	if(t > 5.5 and t < 5.6) then
		mgSetScale(canvas3, 1.0, 1.0, "easeout", .5)
		mgSetPos(canvas3, -675, 0, "easeout", .5)
		mgSetAlpha(garden_motion_blur, 0, "linear", .1)
	end

	if(t > 6 and t < 6.1) then
		mgSetScale(canvas3, 2.5, 2.5, "easein", 4)
		mgSetPos(canvas3, -3000, -600, "easein", 4)
		mgSetOrigo(garden_motion_blur, "pixel", 128, 64)
		mgSetScale(garden_motion_blur, 2.5, 1)
		mgSetAlpha(garden_motion_blur, 1)
		mgSetPos(garden_motion_blur, 1400, 410)
		mgSetScale(garden_motion_blur, .1, 1, "linear", .2)
		mgSetAlpha(garden_motion_blur, 0, "linear", .2)
	end

	if(t > 7.5 and t < 7.6) then
		mgSetAlpha(canvas3, 0, "linear", .2)
	end
	
	mgPushCanvas(canvas3)
	mgDraw(bg_garden)
	mgDraw(garden_apple)
	mgDraw(garden_apple2)
	mgDraw(garden_apple3)
	mgDraw(garden_shadow)
	mgDraw(garden_thief)
	mgDraw(garden_apple_thief)
	mgDraw(garden_arm)
	mgDraw(garden_mask)
	mgDraw(garden_chair)
	mgDraw(garden_granny)
	mgDraw(garden_granny_surprise)
	mgDraw(garden_motion_blur)
	mgDraw(garden_grammophone)
	mgDraw(black)
	mgPopCanvas()
end



function sceneShed(t, first)
	if first then
		mgSetOrigo(black, "center")
		mgSetPos(black, 512, 384)
		mgSetScale(black, 16, 12)
		mgSetAlpha(black, 1)
		mgSetAlpha(black, 0, "linear", 2)

		mgSetOrigo(canvas4, "center")
		mgSetScale(canvas4, 1, 1, 0)
		mgSetScale(canvas4, 1.2, 1.2, "linear", 10)

		mgSetOrigo(bg_shed, "center")
		mgSetPos(bg_shed, 512, 380)
		mgSetScale(bg_shed, 1, .75)

		mgSetPos(shed_arm_back, 165, 335)
		mgSetOrigo(shed_granny, "pixel", 350, 500)
		mgSetRot(shed_granny, .1)
		mgSetPos(shed_granny, 300, 708)

		mgSetPos(shed_arm, 135, 335)
		mgSetRot(shed_arm, -.3)
		mgSetRot(shed_arm_back, -.3)

		mgSetPos(shed_skates_dirty, 300, 430)
		mgSetPos(canvas4, 512, 384, 0)
		mgSetPos(canvas4, 512, 320, "linear", 10)
		mgSetAlpha(shed_skates, 0)

		mgSetAlpha(canvas4, 0)
		mgSetAlpha(canvas4, 1, "linear", 2)

		mgSetPos(shed_blow, 220, 250)
		mgSetScale(shed_blow, .8, 1)
		mgSetAlpha(shed_blow, 0)

		mgSetPos(shed_dust, 315, 310)
		mgSetOrigo(shed_dust, "pixel", 0, 64)
		mgSetScale(shed_dust, 1.5, 1)
		mgSetRot(shed_dust, -.4)
		mgSetAlpha(shed_dust, 0)

	end

	if(t > 1 and t < 1.1) then
		mgSetRot(shed_granny, 0, "cosine", 1)
		mgSetRot(shed_arm, .2, "cosine", 1)
		mgSetRot(shed_arm_back, .21, "cosine", 1)
		mgSetPos(shed_skates_dirty, 340, 310, "cosine", 1)
		mgSetPos(shed_skates, 340, 310)
		mgSetPos(shed_arm, 175, 335, "linear", 1)
		mgSetPos(shed_arm_back, 205, 335, "linear", 1)
	end

	if(t > 2 and t < 2.1) then
		mgSetAlpha(shed_blow, 1, "linear", .1)		
		mgSetScale(shed_blow, 1, 1, "easeout", .3)
		mgSetScale(shed_dust, 9, 6, "easeout", 1)
		mgSetAlpha(shed_dust, .75, "linear", .1)		
		mgSetAlpha(shed_skates, 1, "linear", .5)		
		mgSetAlpha(shed_skates_dust, 0, "linear", .5)		
	end

	if(t > 2.2 and t < 2.3) then
		mgSetAlpha(shed_dust, 0, "linear", 1)		
	end

	if(t > 3.5 and t < 3.6) then
		mgSetAlpha(shed_blow, 0, "linear", .1)		
		mgSetScale(shed_blow, .8, 1, "easeout", .3)
	end

	mgPushCanvas(canvas4)
	mgDraw(bg_shed)
	mgDraw(shed_arm_back)
	mgDraw(shed_granny)
	mgDraw(shed_skates_dirty)
	mgDraw(shed_skates)
	mgDraw(shed_arm)
	mgDraw(shed_blow)
	mgDraw(shed_dust)
	mgDraw(black)
	mgPopCanvas()
end


function sceneBlack(t, first)
end


function sceneEnd(t, first)
	if first then
		mgCommand("game.menu")
	end
end

