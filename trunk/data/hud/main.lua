-------------------------------------------------------------------------------
--------------------- SCRATCHES -----------------------------------------------
-------------------------------------------------------------------------------

function initScratches()
	scratch = {}
	scratch[1] = mgCreateImage("scratches.png", 55, 37, 89, 138 )
	scratch[2] = mgCreateImage("scratches.png", 141, 55, 195, 96 )
	scratch[3] = mgCreateImage("scratches.png", 228, 60, 270, 92 )
	scratch[4] = mgCreateImage("scratches.png", 208, 105, 268, 149 )
	scratch[5] = mgCreateImage("scratches.png", 298, 34, 320, 400 )
	scratch[6] = mgCreateImage("scratches.png", 55, 152, 77, 169 )
	mgSetScale(scratch[5], 1, 2)
end

function drawScratches()
	if mgGet("level.replay") == "0" then return end
	if mgGet("game.disablesepia") == "1" then
		return
	end

	for i=1, 6 do
		if math.random(20) == 1 then
			mgSetPos(scratch[i], math.random(0, 1024), math.random(0,768));
			mgDraw(scratch[i])
		end
	end
end

-------------------------------------------------------------------------------
--------------------- HUD -----------------------------------------------------
-------------------------------------------------------------------------------

function initHud()
	clickSnd = mgCreateSound("snd/clickdown0.ogg")
	
	pauseMenu = mgCreateUi("pausemenu.xml")
	mgSetOrigo(pauseMenu, "center")
	mgSetScale(pauseMenu, 0.0, 0.0)
	mgSetUiEnabled(pauseMenu, false)

	hudMeterBg = mgCreateImage("hud.png", 15, 10, 16, 35 )
	mgSetScale(hudMeterBg, 1024, 1)
	hudMeter = mgCreateImage("hud.png", 36, 10, 511, 23)
	mgSetScale(hudMeter, 1024/(511-36), 1)

	hudScoreBg = mgCreateImage("hud.png", 10, 412, 211, 473)
	mgSetOrigo(hudScoreBg, "topright")
	mgSetPos(hudScoreBg, 600, top+20)
	hudHighScoreBg = mgCreateImage("hud.png", 222, 412, 432, 495)
	mgSetOrigo(hudHighScoreBg, "topright")
	mgSetPos(hudHighScoreBg, 600, top+20)
	hudScoreText = mgCreateText("numbers", true)
	mgSetScale(hudScoreText, 0.8, 0.8)
	mgSetPos(hudScoreText, 512, top+46)
	hudScoreTextGold = mgCreateText("numbers_gold", true)
	mgSetScale(hudScoreTextGold, 0.8, 0.8)
	mgSetPos(hudScoreTextGold, 512, top+46)
	
	hudGranny = mgCreateImage("hud.png", 102, 10, 133, 41)
	mgSetOrigo(hudGranny, "center")
	hudBadGuy = mgCreateImage("hud.png", 144, 10, 175, 41)
	mgSetOrigo(hudBadGuy, "center")
	hudApple = mgCreateImage("hud.png", 62, 10, 91, 41)
	mgSetOrigo(hudApple, "center")
	
	restartButton = mgCreateImage("hud.png", 346, 137, 428, 223)
	mgSetOrigo(restartButton, "center")
	mgSetPos(restartButton, right-50, top+50)

	pauseButton = mgCreateImage("hud.png", 346, 46, 414, 126)
	mgSetOrigo(pauseButton, "center")
	mgSetPos(pauseButton, left+50, top+50)

	ctrlCane = mgCreateImage("hud.png", 10, 46, 167, 203)
	mgSetOrigo(ctrlCane, "center");
	mgSetPos(ctrlCane, left+130, bottom-110);	

	ctrlJump = mgCreateImage("hud.png", 178, 46, 335, 203)
	mgSetOrigo(ctrlJump, "center");
	mgSetPos(ctrlJump, right-130, bottom-110);

	puBanana = mgCreateImage("hud.png", 10, 313, 176,401)
	mgSetOrigo(puBanana, "center")
	mgSetPos(puBanana, left+100, bottom-260)
	mgSetScale(puBanana, 1.0, 1.0)
	puBananaText = mgCreateText("numbers", true)
	mgSetScale(puBananaText, 0.7, 0.7)
	x, y = mgGetPos(puBanana)
	mgSetPos(puBananaText, x+50, y-4)
	
	puBaseBall = mgCreateImage("hud.png", 10, 214, 161,302)
	mgSetOrigo(puBaseBall, "center")
	mgSetPos(puBaseBall, right-100, bottom-260)
	mgSetScale(puBaseBall, 1.0, 1.0)
	puBaseBallText = mgCreateText("numbers", true)
	mgSetScale(puBaseBallText, 0.7, 0.7)
	x, y = mgGetPos(puBaseBall)
	mgSetPos(puBaseBallText, x-45, y-4)
end


function frameHud()
	if not mgIsVisible(pauseMenu) then
		if mgGet("level.replay") ~= "1" and scoreMode==false then
			if mgIsTouched(left+50, top+50, 100, 1) then
				mgPlaySound(clickSnd)
			end
			if mgIsTouched(left+50, top+50, 100, 2) then
				mgPlaySound(clickSnd)
				handleCommand("showpausemenu")
			end
		end

		if mgIsTouched(right-50, top+50, 100, 1) then
			mgPlaySound(clickSnd)
			tapped = 0
		end
		if mgIsTouched(right-50, top+50, 100, 2) then
			mgPlaySound(clickSnd)
			mgCommand("level.restart")
			mgCommand("hud.reload")
		end	
	end
end


function drawHud()
	replay = false
	if mgGet("level.replay") == "1" then replay = true end

	if not scoreMode and replay then
		if mgIsTouched(512, 384, 1000, 1) then
			mgCommand("game.menu")
		end
		return
	end
	
	mgDraw(restartButton)

	if scoreMode or replay then
		return
	end

	mgDraw(pauseButton)
	
	if mgGet("game.ctrlcane") == "1" then
		mgSetScale(ctrlCane, 0.93)
		mgSetColor(ctrlCane, 0.8, 0.8, 0.8)
	else
		mgSetScale(ctrlCane, 1.0)
		mgSetColor(ctrlCane, 1.0, 1.0, 1.0)
	end
	
	if mgGet("game.ctrljump") == "1" then
		mgSetScale(ctrlJump, 0.93)
		mgSetColor(ctrlJump, 0.8, 0.8, 0.8)
	else
		mgSetScale(ctrlJump, 1.0)
		mgSetColor(ctrlJump, 1.0, 1.0, 1.0)
	end
	
	mgDraw(ctrlJump)
	mgDraw(ctrlCane)

	baseBallCount = tonumber(mgGet("player.baseball"))
	if baseBallCount > 0 then
		mgDraw(puBaseBall)
		mgSetText(puBaseBallText, baseBallCount)
		mgSetOrigo(puBaseBallText, "center")
		mgDraw(puBaseBallText)
	end
	
	bananaCount = tonumber(mgGet("player.banana"))
	if bananaCount > 0 then
		mgDraw(puBanana)
		mgSetText(puBananaText, bananaCount)
		mgSetOrigo(puBananaText, "center")
		mgDraw(puBananaText)
	end					

	mgDraw(hudMeterBg)
	
	score = mgGet("level.score")
	dudePos = mgGet("level.progress.dude") * 1024
	badguyPos = mgGet("level.progress.badguy") * 1024
	applepos0 = mgGet("level.applepos0") * 1024
	applepos1 = mgGet("level.applepos1") * 1024
	applepos2 = mgGet("level.applepos2") * 1024
	mgSetPos(hudMeter, dudePos-1000, top)
	mgDraw(hudMeter)	

	--if dudePos > badguyPos then
		--mgSetText(hudDistance, score)
		--mgSetOrigo(hudDistance, "center")
		--if dudePos-badguyPos < 100 then
		--	if dudePos < 100 then
		--		mgSetPos(hudDistance, 175+dudePos+60, topY+24)
		--	else
		--		mgSetPos(hudDistance, 175+badguyPos-60, topY+24)
		--	end
		--else
		--	mgSetPos(hudDistance, 175+(dudePos+badguyPos)*0.5, topY+24)
		--end
		--mgDraw(hudDistance)
	--end
	--mgSetPos(hudBadGuy, badguyPos, top+15)
	--mgDraw(hudBadGuy)
	--mgSetPos(hudGranny, dudePos, top+15)
	--mgDraw(hudGranny)
	--mgSetPos(hudApple, applepos0, top+15)
	--mgDraw(hudApple)
	--mgSetPos(hudApple, applepos1, top+15)
	--mgDraw(hudApple)
	--mgSetPos(hudApple, applepos2, top+15)
	--mgDraw(hudApple)

	score = tonumber(mgGet("level.score"))
	highScore = tonumber(mgGet("level.highscore"))
	if score > highScore and highScore > 0 then
		mgDraw(hudHighScoreBg)
		mgSetText(hudScoreTextGold, score)
		mgSetOrigo(hudScoreTextGold, "center")
		mgDraw(hudScoreTextGold)
	else
		mgDraw(hudScoreBg)
		mgSetText(hudScoreText, score)
		mgSetOrigo(hudScoreText, "center")
		mgDraw(hudScoreText)
	end

	a = mgGetScale(pauseMenu)
	if a > 0 then
		mgFullScreenColor(1, 1, 1, 0.5*a)
	end
	mgDraw(pauseMenu)
end


-------------------------------------------------------------------------------
--------------------- NOTIFY --------------------------------------------------
-------------------------------------------------------------------------------


function initNotify()
	spinText = mgCreateText("hud")
	mgSetPos(spinText, 512, 200)
	mgSetScale(spinText, 0, 0)
	mgSetAlpha(spinText, 0)

	goodLanding = mgCreateImage("hud/landing_good.png")
	mgSetOrigo(goodLanding, "center")
	mgSetPos(goodLanding, 512, 250)
	mgSetScale(goodLanding, 0, 0)
	mgSetAlpha(goodLanding, 0)
	
	perfectLanding = mgCreateImage("hud/landing_perfect.png")
	mgSetOrigo(perfectLanding, "center")
	mgSetPos(perfectLanding, 512, 250)
	mgSetScale(perfectLanding, 0, 0)
	mgSetAlpha(perfectLanding, 0)
	
	scoreNotify = mgCreateText("numbers", true)	

	sndDouble = mgCreateSound("snd/flip_double.ogg")
	sndTriple = mgCreateSound("snd/flip_multiple.ogg")

	spinTimer = 0.0
end

	
function frameNotify()
end


function drawNotify()
	if mgGet("level.replay") == "1" then
		return
	end
	s = spinTimer - 0.015
	if spinTimer > 0 and s < 0 then
		--mgSetScale(spinText, 0, 0, "cosine", 0.3)
		--mgSetAlpha(spinText, 0, "cosine", 0.3)
		--mgSetRot(spinText, 0)

		mgSetScale(goodLanding, 0, 0, "cosine", 0.3)
		mgSetAlpha(goodLanding, 0, "cosine", 0.3)
		mgSetScale(perfectLanding, 0, 0, "cosine", 0.3)
		mgSetAlpha(perfectLanding, 0, "cosine", 0.3)
	end
	spinTimer = s
	--mgDraw(spinText)
	mgDraw(goodLanding)
	mgDraw(perfectLanding)
	mgDraw(scoreNotify)
end


-------------------------------------------------------------------------------
--------------------- CLEARED -------------------------------------------------
-------------------------------------------------------------------------------


function initCleared()
	cleared = mgCreateImage("cleared.png")
	mgSetScale(cleared, 1.0, 1.0)
	mgSetOrigo(cleared, "center")
	mgSetRot(cleared, -0.5)
	mgSetPos(cleared, 512, -200)
	
	scoreCanvas = mgCreateCanvas(1024, 250)
	mgSetOrigo(scoreCanvas, "bottomleft")
	mgSetPos(scoreCanvas, 512, bottom+127)
	scoreBg = mgCreateImage("score.png", 0, 0, 1024, 208)
	mgSetPos(scoreBg, 0, 42)

	scoreApple = {}
	for i=1, 3 do
		scoreApple[i] = mgCreateImage("score.png", 10, 219, 145, 351)
		mgSetOrigo(scoreApple[i], "center")
		mgSetScale(scoreApple[i], 4, 4)
		mgSetAlpha(scoreApple[i], 0)
	end
	mgSetPos(scoreApple[1], 399, 173)
	mgSetPos(scoreApple[2], 516, 173)
	mgSetPos(scoreApple[3], 633, 173)
	
	scoreText = mgCreateText("numbers", true)
	mgSetPos(scoreText, 200, 171)
	scoreCoinText = mgCreateText("numbers", true)
	mgSetPos(scoreCoinText, 880, 171)

	scoreAppleSound = mgCreateSound("hud/cork.ogg")
	
	scoreHighScoreBg = mgCreateImage("score.png", 156, 219, 354, 292)
	mgSetPos(scoreHighScoreBg, 60, 80)
	scoreHighScoreText = mgCreateText("numbers_gold", true)
	mgSetPos(scoreHighScoreText, 180, 109)
	mgSetScale(scoreHighScoreText, 0.6, 0.6)
	
	scoreMode = false
	scoreFrame = 0
	scorePoints = 0
	scoreCoins = 0
	scoreApples = 0

	highScore = 0
	scoreHighScore = mgCreateImage("highscore.png")
	mgSetOrigo(scoreHighScore, "center")
	mgSetPos(scoreHighScore, 512, 380)
	mgSetScale(scoreHighScore, 0, 0)
	mgSetAlpha(scoreHighScore, 0)

	sndHighScore = mgCreateSound("highscore.ogg")
	sndCoins = mgCreateSound("coins.ogg")
	sndScore = mgCreateSound("score.ogg")
	sndApple1 = mgCreateSound("winapple0.ogg")
	sndApple2 = mgCreateSound("winapple1.ogg")
	sndApple3 = mgCreateSound("winapple2.ogg")
	
	mgSetOrigo(scoreCanvas, "center");
	mgSetScale(scoreCanvas, 1.01, 1.01)
	
	tapped = 0
end


function frameCleared()
	if scoreMode then
		scoreFrame = scoreFrame + 1
		hideScore = false

		if scoreFrame > 1 and mgIsTouched(512, 384, 1000, 1) then
			tapped = tapped + 1
			if tapped == 1 then	hideScore = true end
			if tapped == 2 then	
				if mgGet("level.name") == "titan" then
					mgCommand("movie.start movie/ending.lua") 
				else
					mgCommand("game.menu") 
				end
			end
		end

		if scoreFrame == 30 and scoreApples > 0 then
			mgSetScale(scoreApple[1], 1, 1, "easeout", 0.2)
			mgSetAlpha(scoreApple[1], 1, "linear", 0.2)
			mgPlaySound(sndApple1)
		end
		if scoreFrame == 40 and scoreApples > 0 then
			mgSetScale(scoreCanvas, 1.0, 1.0)
			mgSetScale(scoreCanvas, 1.02, 1.02, "linear", 0.2)
		end
		if scoreFrame == 50 and scoreApples > 1 then
			mgSetScale(scoreApple[2], 1, 1, "easeout", 0.2)
			mgSetAlpha(scoreApple[2], 1, "linear", 0.2)
			mgPlaySound(sndApple2)
		end
		if scoreFrame == 60 and scoreApples > 1 then
			mgSetScale(scoreCanvas, 1.0, 1.0)
			mgSetScale(scoreCanvas, 1.03, 1.03, "linear", 0.2)
		end
		if scoreFrame == 70 and scoreApples > 2 then
			mgSetScale(scoreApple[3], 1, 1, "easeout", 0.2)
			mgSetAlpha(scoreApple[3], 1, "linear", 0.2)
			mgPlaySound(sndApple3)
		end
		if scoreFrame == 80 and scoreApples > 2 then
			mgSetScale(scoreCanvas, 1.0, 1.0)
			mgSetScale(scoreCanvas, 1.02, 1.02, "linear", 0.2)
		end
		
		if scoreFrame >= 90 and scoreFrame <= 110 then
			f = (scoreFrame-90)/20
			if f > 1 then f = 1 end
			s = math.floor(f * scoreCoins)
			mgSetText(scoreCoinText, s)
		end
		if scoreFrame == 90 and scoreCoins > 0 then
			mgPlaySound(sndCoins)
		end

		if scoreFrame >= 140 and scoreFrame <= 160 then
			f = (scoreFrame-140)/20
			if f > 1 then f = 1 end
			s = math.floor(f * scorePoints)
			mgSetText(scoreText, s)
		end
		if scoreFrame == 140 and scorePoints > 0 then
			mgPlaySound(sndScore)
		end

		if scoreFrame == 180  then
			if scorePoints > highScore then
				mgSetText(scoreHighScoreText, scorePoints)
				if highScore > 0 then
					mgPlaySound(sndHighScore)
					s = mgGetScale(scoreHighScore)
					if s == 0.0 then
						mgSetScale(scoreHighScore, 1, 1, "bounce", 0.8)
					end
				end
			end
		end

		if (scoreFrame == 250 and tapped == 0) or hideScore then
			if mgGet("level.replay") ~= "1" then
				mgCommand("level.replay")
				mgCommand("audio.playBackgroundMusic snd/replay.ogg")
			end
		end
		
		if hideScore then
			mgSetPos(cleared, 512, -100, "easein", 0.6)

			if scorePoints > highScore and highScore > 0 then
				mgSetPos(scoreHighScore, 100, 100, "cosine", 0.4)
				mgSetRot(scoreHighScore, 0.3, "cosine", 0.4)
				mgSetScale(scoreHighScore, 0.5, 0.5, "cosine", 0.4)
			end

			mgSetPos(scoreCanvas, 512, bottom+125, "easein", 0.6)			
		end
	end
end


function drawCleared()
	mgDraw(cleared)
	
	if scoreMode then
		mgPushCanvas(scoreCanvas)
		mgDraw(scoreBg)

		mgDraw(scoreApple[1])
		mgDraw(scoreApple[2])
		mgDraw(scoreApple[3])

		mgSetOrigo(scoreText, "center")
		mgDraw(scoreText)

		mgSetOrigo(scoreCoinText, "center")
		mgDraw(scoreCoinText)
		
		mgDraw(scoreHighScoreBg)
		mgSetOrigo(scoreHighScoreText, "center")
		mgDraw(scoreHighScoreText)
		
		mgPopCanvas()
		mgDraw(scoreHighScore)
	end
end


-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------


function init()
	top = tonumber(mgGet("display.visibleTop"))
	left = tonumber(mgGet("display.visibleLeft"))
	right = tonumber(mgGet("display.visibleRight"))
	bottom = tonumber(mgGet("display.visibleBottom"))
	
	initHud()
	initNotify()
	initCleared()
	initScratches()
	
	reply = false
end


function frame()
	replay = false
	if mgGet("game.replay") == "1" then
		replay = true
	end
	
	frameHud()
	frameNotify()
	frameCleared()
end


function draw()
	drawNotify()
	drawScratches()
	drawHud()
	drawCleared()
end


function win()
	mgSetPos(cleared, 512, 150, "bounce", 1)
	mgSetRot(cleared, 0, "bounce", 1)

	highScore = tonumber(mgGet("level.highscore"))
	mgSetText(scoreHighScoreText, highScore)
	
	scoreMode = true
	scoreTimer = 0
	scoreCoins = tonumber(mgGet("level.coins"))
	scorePoints = tonumber(mgGet("level.score"))
	scoreApples = tonumber(mgGet("level.apples"))

	if scorePoints > highScore then
		mgSetAlpha(scoreHighScore, 1)
	end

	mgSetPos(scoreCanvas, 512, bottom-124, "easeout", 0.5)
	mgSetPos(scoreCoin, -100, 0)

	mgCommand("audio.playBackgroundMusic")
	mgCommand("audio.playForegroundMusic snd/win.ogg")
end


function startsWith(str,start)
   return string.sub(str,1,string.len(start))==start
end

function getFirst(str)
	i = string.find(str, " ")
	if i > 0 then
		return string.sub(str, 1, i-1)
	end
	return ""
end

function getSecond(str)
	i = string.find(str, " ")
	if i > 0 then
		i = i + 1
		return string.sub(str, i, string.len(str))
	end
	return ""
end


function handleCommand(cmd)
	if cmd == "showpausemenu" then
		mgSetUiEnabled(pauseMenu, true)
		mgSetUiModal(pauseMenu, true)
		mgSetPos(pauseMenu, 512, 384, "bounce", 0.7)
		mgSetScale(pauseMenu, 1, 1, "bounce", 0.7)
		mgSet("game.paused", "1")
	end
	
	if cmd == "hidepausemenu" then
		mgSetUiEnabled(pauseMenu, false)
		mgSetUiModal(pauseMenu, false)
		mgSetPos(pauseMenu, 50, top+50, "cosine", 0.3)
		mgSetScale(pauseMenu, 0, 0, "cosine", 0.3)
		mgSet("game.paused", "0")
	end
	
	if cmd == "cleared" then
		win()
	end

	if startsWith(cmd, "apple") then
		args = getSecond(cmd)
	end
	
	if startsWith(cmd, "score") then
		s = getSecond(cmd)
		x,y = mgGetPose("dude.torso")
		x,y = mgGetScreenCoord(x, y)
		mgSetText(scoreNotify, s)
		mgSetPos(scoreNotify, x, y)
		mgSetPos(scoreNotify, x, y-200, "easein", 1.0)
		mgSetAlpha(scoreNotify, 1)
		mgSetAlpha(scoreNotify, 0, "easein", 1.0)
	end
	
	if startsWith(cmd, "land") and not replay then
		args = getSecond(cmd)
		landing = tonumber(getFirst(args))
		angle = tonumber(getSecond(args))
		show = false
		rot = 0
		if angle > 5.0 and angle < 8.5 then
			rot = -2*3.14159
			show = true
		end
		if angle > 11.3 and angle < 14.8 then
			rot = -4*3.14159
			show = true
		end
		if angle > 18.0 then
			rot = -6*3.14159
			show = true
		end
		
		img = -1
		if show and landing > 0.9 then
			img = perfectLanding
		elseif show and landing > 0.75 then
			img = goodLanding
		end
		if img ~= -1 then
			if (angle > 11) then
				mgSetRot(img, angle)
				mgSetRot(img, 0, "easeout", 0.7)
				if angle > 18 then
					mgPlaySound(sndTriple)
				else
					mgPlaySound(sndDouble)
				end
			end
			mgSetScale(img, 1, 1, "cosine", 0.3)
			mgSetAlpha(img, 1, "cosine", 0.3)
			spinTimer = 1.0
		end
	end


	if cmd == "backbutton" then	
		if mgGet("level.replay") == "0" and scoreMode == false then
			if mgIsVisible(pauseMenu) then
				handleCommand("hidepausemenu")
			else
				handleCommand("showpausemenu")
			end
		else
			mgCommand("game.menu")
		end
	end

	if cmd == "menubutton" then
		if mgGet("level.replay") == "0" and scoreMode == false then
			if mgIsVisible(pauseMenu) then
				handleCommand("hidepausemenu")
			else
				handleCommand("showpausemenu")
			end
		end
	end
end

