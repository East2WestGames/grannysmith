uniform mat4 uMvpMatrix;
uniform mat4 uModelViewMatrix;
uniform sampler2D uTexture0;

varying vec4 vColor;
varying vec2 vTexCoord;

#ifdef VERTEX
attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec2 aTexCoord;

void main(void)
{
	gl_Position = uMvpMatrix * vec4(aPosition, 1.0);
	vec3 worldNormal = (uModelViewMatrix * vec4(aNormal, 0.0)).xyz;
	vTexCoord = aTexCoord;
		
	float light = dot(worldNormal, vec3(0.7, 0.7, 0.7));
	light = 0.5  + 0.5 * light * light;
	vColor = vec4(1.0, 1.0, 1.0, light);
}
#endif

#ifdef FRAGMENT
void main(void) 
{
	vec4 texColor = texture2D(uTexture0, vTexCoord);
	gl_FragColor = mix(texColor, vColor, 1.0-texColor.a);
}
#endif

