uniform mat4 uMvpMatrix;
uniform sampler2D uTexture0;
uniform float uAngle;
uniform vec2 uTexOffset0;
uniform vec2 uTexScale0;

varying vec4 vColor;
varying vec2 vTexCoord;

#ifdef VERTEX
attribute vec3 aPosition;
attribute vec2 aTexCoord;
attribute vec4 aColor;
attribute float aAmplitude;
attribute float aFrequency;

void main(void)
{
	vTexCoord = aTexCoord * uTexScale0 + uTexOffset0;
	vColor = aColor * 2.0;
	vec3 pos = aPosition;
	pos.x += aAmplitude * sin(uAngle*aFrequency);
	gl_Position = uMvpMatrix * vec4(pos, 1.0);
}
#endif

#ifdef FRAGMENT
void main(void) 
{
	vec4 texColor = texture2D(uTexture0, vTexCoord);
	gl_FragColor = texColor * vColor;
}
#endif

