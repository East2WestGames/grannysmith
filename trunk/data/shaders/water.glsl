uniform mat4 uMvpMatrix;
uniform mat4 uObjectMatrix;
uniform sampler2D uTexture0;
uniform vec4 uColor;
uniform vec2 uCameraPos;
uniform float uTime;

varying vec3 vPos;

#ifdef VERTEX
attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec2 aTexCoord;
attribute float aLight;

void main(void)
{
	gl_Position = uMvpMatrix * vec4(aPosition, 1.0);
	vPos = (uObjectMatrix * vec4(aPosition, 1.0)).xyz * 10.0;
}
#endif

#ifdef FRAGMENT
void main(void) 
{
	mediump float i = texture2D(uTexture0, vPos.xz*0.0023).x;
	mediump float jx = sin(vPos.x*0.07);
	mediump float jz = sin(vPos.z*0.062);
	mediump float s = sin(jx+jz+6.28*i + uTime*2.0);
	s = max(0.0, s);
	s = pow(s, 10.0) * 0.4;
	gl_FragColor = vec4(0.6+s, 0.8+s, 0.9+s, 1.0);
}
#endif

