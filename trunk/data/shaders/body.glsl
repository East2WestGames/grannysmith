uniform mat4 uMvpMatrix;
uniform mat4 uObjectMatrix;
uniform vec4 uColor;

varying lowp vec4 vCol;
varying vec2 vTexCoord;
varying float vShadow;

#ifdef VERTEX
attribute vec3 aPosition;
attribute vec3 aNormal;
attribute float aLight;

void main(void)
{
	vec3 normal = (uObjectMatrix * vec4(aNormal, 0)).xyz;
	float light = 0.8 + dot(vec3(-0.15, 0.15, 0.15), normal);
	light = max(0.7, light);
	vCol = uColor * light;
	vShadow = 1.0-aLight;
	gl_Position = uMvpMatrix * vec4(aPosition, 1.0);
}
#endif

#ifdef FRAGMENT
void main(void) 
{
	gl_FragColor = vCol * (1.0-vShadow*vShadow);
}
#endif

