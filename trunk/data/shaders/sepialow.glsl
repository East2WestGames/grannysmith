uniform mat4 uMvpMatrix;
uniform sampler2D uTexture0;
uniform vec4 uSepia;
uniform float uFocus;
uniform vec2 uTexScale0;

varying vec4 vColor;
varying vec2 vTexCoord;

#ifdef VERTEX
attribute vec3 aPosition;
attribute vec2 aTexCoord;

void main(void)
{
	vTexCoord = aTexCoord * uTexScale0;
	gl_Position = uMvpMatrix * vec4(aPosition, 1.0);
	vec2 d = (aTexCoord-vec2(0.5, 0.5))*2.0;
	float l = length(d);
	float a = 1.0-0.4*l*l;
	vColor = uSepia * a;
}
#endif

#ifdef FRAGMENT
void main(void) 
{
	mediump vec4 texColor = texture2D(uTexture0, vTexCoord);
	mediump float avg = texColor.x + texColor.y + texColor.z;
	mediump vec4 bw = vec4(avg, avg, avg, 1.0);
	gl_FragColor = bw * vColor;
}
#endif


