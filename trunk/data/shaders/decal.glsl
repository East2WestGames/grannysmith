uniform mat4 uMvpMatrix;
uniform sampler2D uTexture0;

varying vec4 vColor;
varying vec2 vTexCoord;

#ifdef VERTEX
attribute vec3 aPosition;
attribute vec2 aTexCoord;
attribute vec4 aColor;

void main(void)
{
	vTexCoord = aTexCoord;
	vColor = aColor * 2.0;
	gl_Position = uMvpMatrix * vec4(aPosition, 1.0);
}
#endif

#ifdef FRAGMENT
void main(void) 
{
	vec4 texColor = texture2D(uTexture0, vTexCoord);
	gl_FragColor = texColor * vColor;
}
#endif

