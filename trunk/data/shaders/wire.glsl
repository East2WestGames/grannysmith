uniform mat4 uMvpMatrix;

varying float vShade;

#ifdef VERTEX
attribute vec2 aPosition;
attribute vec2 aNormal;

void main(void)
{
	vShade = 0.7 + 0.4*dot(aNormal, vec2(0.8, -0.8));
	gl_Position = uMvpMatrix * vec4(aPosition, 0.0, 1.0);
}
#endif

#ifdef FRAGMENT
void main(void) 
{
	gl_FragColor = vec4(vShade, vShade, vShade, 1.0);
}
#endif

