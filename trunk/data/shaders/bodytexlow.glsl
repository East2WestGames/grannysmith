uniform mat4 uMvpMatrix;
uniform mat4 uObjectMatrix;
uniform sampler2D uTexture0;
uniform vec4 uColor;

varying lowp vec4 vCol;
varying vec2 vTexCoord0;

#ifdef VERTEX
attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec2 aTexCoord;
attribute float aLight;

void main(void)
{
	vTexCoord0 = aTexCoord;
	vec3 normal = (uObjectMatrix * vec4(aNormal, 0)).xyz;
	float light = 0.8 + dot(vec3(-0.15, 0.15, 0.15), normal);
	light = max(0.7, light) * (aLight * 0.5 + 0.5);
	vCol = uColor * light;
	gl_Position = uMvpMatrix * vec4(aPosition, 1.0);
}
#endif

#ifdef FRAGMENT
void main(void) 
{
	gl_FragColor = texture2D(uTexture0, vTexCoord0) * vCol;
}
#endif

