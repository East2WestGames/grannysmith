uniform mat4 uMvpMatrix;
uniform mat4 uModelViewMatrix;

varying vec4 vColor;

#ifdef VERTEX
attribute vec3 aPosition;
attribute vec3 aNormal;

void main(void)
{
	gl_Position = uMvpMatrix * vec4(aPosition, 1.0);
	vec3 worldNormal = (uModelViewMatrix * vec4(aNormal, 0.0)).xyz;
		
	float light = dot(worldNormal, vec3(0.7, 0.7, 0.7));
	light = 0.5  + 0.5 * light * light;
	vColor = vec4(1.0, 1.0, 1.0, light);
}
#endif

#ifdef FRAGMENT
void main(void) 
{
	gl_FragColor = vColor;
}
#endif

