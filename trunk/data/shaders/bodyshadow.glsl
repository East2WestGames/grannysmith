uniform mat4 uMvpMatrix;
uniform mat4 uObjectMatrix;
uniform vec4 uColor;

varying float vAlpha;

#ifdef VERTEX
attribute vec3 aPosition;
attribute float aAlpha;

void main(void)
{
	vAlpha = aAlpha;
	gl_Position = uMvpMatrix * vec4(aPosition, 1.0);
}
#endif

#ifdef FRAGMENT
void main(void) 
{
	gl_FragColor = vec4(0, 0, 0, vAlpha*vAlpha);
}
#endif

