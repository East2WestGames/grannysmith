uniform mat4 uMvpMatrix;
uniform sampler2D uTexture0;
uniform vec4 uSepia;
uniform float uFocus;
uniform vec2 uTexScale0;

varying vec4 vColor;
varying vec2 vTexCoord;
varying vec2 vTexCoord1;
varying vec2 vTexCoord2;
varying vec2 vTexCoord3;
varying vec2 vTexCoord4;

#ifdef VERTEX
attribute vec3 aPosition;
attribute vec2 aTexCoord;

void main(void)
{
	vTexCoord = aTexCoord * uTexScale0;
	vTexCoord1 = vTexCoord + vec2(0.003, 0.003);
	vTexCoord2 = vTexCoord + vec2(-0.003, 0.003);
	vTexCoord3 = vTexCoord + vec2(0.003, -0.003);
	vTexCoord4 = vTexCoord + vec2(-0.003, -0.003);
	gl_Position = uMvpMatrix * vec4(aPosition, 1.0);
	vec2 d = (aTexCoord-vec2(0.5, 0.5))*2.0;
	float l = length(d);
	float a = 1.0-0.4*l*l;
	vColor = uSepia * a;
}
#endif

#ifdef FRAGMENT
void main(void) 
{
	mediump vec4 sharp = texture2D(uTexture0, vTexCoord);
	mediump vec4 blur = sharp * 0.2;
	blur += texture2D(uTexture0, vTexCoord1) * 0.2;
	blur += texture2D(uTexture0, vTexCoord2) * 0.2;
	blur += texture2D(uTexture0, vTexCoord3) * 0.2;
	blur += texture2D(uTexture0, vTexCoord4) * 0.2;
	mediump vec4 texColor = mix(blur, sharp, uFocus);
	
	mediump float avg = texColor.x + texColor.y + texColor.z;
	mediump vec4 bw = vec4(avg, avg, avg, 1.0);
	gl_FragColor = bw * vColor*0.9 + texColor*0.1;
}
#endif


