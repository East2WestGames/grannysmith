function startsWith(str,start)
   return string.sub(str,1,string.len(start))==start
end

function getFirst(str)
	i = string.find(str, " ")
	if i > 0 then
		return string.sub(str, 1, i-1)
	end
	return ""
end

function getSecond(str)
	i = string.find(str, " ")
	if i > 0 then
		i = i + 1
		return string.sub(str, i, string.len(str))
	end
	return ""
end


-------------------------------------------------------------------------------
--------------------- MAIN ----------------------------------------------------
-------------------------------------------------------------------------------
function checkAds()
	if mgGet("player.adsfront") == "1" or mgGet("player.adsplus") == "1" then
		ads_icon = mgCreateUi("ads_icon.xml")
		mgSetOrigo(ads_icon, "center")
		mgSetPos(ads_icon, left+100, top+100)
		mgSetScale(ads_icon, 1, 1)

		ads_ui = mgCreateUi("user://ads.xml")
		mgSetOrigo(ads_ui, "center")
		mgSetPos(ads_ui, 512, 384)
		mgSetScale(ads_ui, 0, 0)

		if mgGet("player.adsshown") == "1" or mgGet("player.adsfront") == "0" then
			mgSetScale(ads_icon, 0, 0)
		end
	end
end

function initMain()
	mainCanvas = mgCreateCanvas(1024, 768)

	options = -1
	more = -1 
	
	button_movie = mgCreateUi("button_movie.xml")
	mgSetOrigo(button_movie, "center")
	mgSetPos(button_movie, 915, 125)
	mgSetScale(button_movie, 2.1, 2.1)
	mgSetAlpha(button_movie, 0, 0)

	button_options = mgCreateUi("button_options.xml")
	mgSetOrigo(button_options, "center")
	mgSetPos(button_options, 915, 255)
	mgSetScale(button_options, 2.1, 2.1)
	mgSetAlpha(button_options, 0, 0)

	button_info = mgCreateUi("button_info.xml")
	mgSetOrigo(button_info, "center")
	mgSetPos(button_info, 915, 385)
	mgSetScale(button_info, 2.1, 2.1)
	mgSetAlpha(button_info, 0, 0)

	button_gamecenter = mgCreateUi("button_gamecenter.xml")
	mgSetOrigo(button_gamecenter, "center")
	mgSetPos(button_gamecenter, 915, 515)
	mgSetScale(button_gamecenter, 2.1, 2.1)
	mgSetAlpha(button_gamecenter, 0, 0)
	mgSetUiEnabled(button_gamecenter, false)

	moreButton = mgCreateUi("more.xml")
	mgSetOrigo(moreButton, "pixel", 128, 128)
	mgSetPos(moreButton, 1025, bottom)
	mgSetScale(moreButton, 0, 0)
	mgSetAlpha(moreButton, 0)
	mgSetUiEnabled(moreButton, true)

	bg = mgCreateImage("menubg.jpg")
	mgSetScale(bg, 1.0, 0.75)
	mgSetOrigo(bg, "topleft")

	logo = mgCreateImage("logo.png")
	mgSetOrigo(logo, "center")
	mgSetPos(logo, 450, 250)
	mgSetScale(logo, 2.0, 2.0)
	mgSetAlpha(logo, 0)

	play_button = mgCreateUi("play.xml")
	mgSetOrigo(play_button, "center")
	mgSetPos(play_button, 450, 505)
	mgSetScale(play_button, 2.0, 2.0)
	mgSetAlpha(play_button, 0)

	tv_top = mgCreateImage("tv_top.png")
	tv_bottom = mgCreateImage("tv_bottom.png")
	tv_left = mgCreateImage("tv_left.png")
	tv_right = mgCreateImage("tv_right.png")
	mgSetPos(tv_top, 67, 71)
	mgSetPos(tv_bottom, 67, 620)
	mgSetPos(tv_left, 67, 136)
	mgSetPos(tv_right, 756, 136)

	granny_canvas = mgCreateCanvas(256, 256)
	mgSetPos(granny_canvas, 120, bottom-20)
	
	granny = mgCreateImage("granny.png")
	mgSetOrigo("center")
	mgSetPos(granny, -140, -280)

	granny_blink = mgCreateImage("granny_blink.png")
	mgSetOrigo(granny_blink, "center")
	mgSetPos(granny_blink, 9, -234)

	granny_smile = mgCreateImage("granny_smile.png")
	mgSetOrigo(granny_smile, "center")
	mgSetPos(granny_smile, 9, -234)
	
	blinkFrame = 0
	granny_rotVel = 0.01
	granny_rot = 0
	granny_pivot = 0
	granny_has_touch = false
	granny_smile_timer = 0
	
	granny_oh = mgCreateSound("snd/granny/oh2.ogg")
	
	ads_ui = -1
	ads_icon = -1
	checkAds()
end

function drawMain()
	mgPushCanvas(mainCanvas)
	mgDraw(bg)
	
	if mgIsVisible(mainCanvas) then
		mgDrawReplay(67, 71, 818, 685)
	end
	
	mgDraw(tv_top)
	mgDraw(tv_bottom)
	mgDraw(tv_left)
	mgDraw(tv_right)

	mgPushCanvas(granny_canvas)
	mgDraw(granny)
	
	if granny_smile_timer > 0 then
		mgDraw(granny_smile)
		granny_smile_timer = granny_smile_timer - 1
	else
		if math.abs(granny_rot) > 0.1 then
			granny_smile_timer = 120
		end
		blinkFrame = blinkFrame - 1
		if blinkFrame < 0 then
			mgDraw(granny_blink)
		end
		if blinkFrame < -5 then
			blinkFrame = math.random(20, 120)
		end
	end
	mgPopCanvas()
	
	if mgIsTouched(150, bottom-150, 200, 1) then	
		granny_has_touch = true
		granny_pivot, tmp = mgGetLastTouch();
	end

	if mgIsTouched(512, 384, 1000) and granny_has_touch then
		x, y = mgGetLastTouch();
		granny_rot = (granny_pivot-x) * 0.003;
		if granny_rot > 1 then granny_rot = math.pow(granny_rot, 0.2) end
		if granny_rot < -1 then granny_rot = -math.pow(-granny_rot, 0.2) end
		granny_rot = granny_rot * 0.5
		granny_rotVel = 0
	else
		if granny_has_touch and math.abs(granny_rot) > 0.3 and math.random(1,10) > 5 then
			if mapMode==false then
			mgPlaySound(granny_oh)
		end
		end
		granny_has_touch = false
	end
	
	granny_rotVel = granny_rotVel*0.98 - granny_rot * 0.01
	granny_rot = granny_rot + granny_rotVel
	mgSetRot(granny_canvas, granny_rot)

	mgDraw(logo)
	mgSetScale(logo, 1.0, 1.0, "bounce", 1)
	mgSetAlpha(logo, 1, "linear", 0.5)

	mgDraw(play_button)
	mgSetAlpha(play_button, 1, "linear", 0.5)
	mgSetScale(play_button, 1+math.sin(frame/30)/10, 1+math.sin(frame/30)/10)

	if frame == 10 then
		mgSetScale(button_movie, 1.1, 1.1, "bounce", 1)
		mgSetAlpha(button_movie, 1, "linear", 0.5)
	end
	
	if frame == 15 then
		mgSetScale(button_options, 1.1, 1.1, "bounce", 1)
		mgSetAlpha(button_options, 1, "linear", 0.5)
	end
	
	if frame == 20 then
		mgSetScale(button_info, 1.1, 1.1, "bounce", 1)
		mgSetAlpha(button_info, 1, "linear", 0.5)
	end
	
	if frame == 25 then
		if mgGet("game.platform") == "ios" and mgGet("game.gamecenteravailable") == "1" then
			mgSetUiEnabled(button_gamecenter, true)
			mgSetScale(button_gamecenter, 1.1, 1.1, "bounce", 1)
			mgSetAlpha(button_gamecenter, 1, "linear", 0.5)
		end
	end

	mgDraw(button_movie)
	mgDraw(button_options)
	mgDraw(button_gamecenter)
	mgDraw(button_info)

	if options ~= -1 then
		mgFullScreenColor(1, 1, 1, mgGetScale(options)*0.7)
		mgDraw(options)
	end
	
	mgPopCanvas()
end

-------------------------------------------------------------------------------
--------------------- MAP -----------------------------------------------------
-------------------------------------------------------------------------------
function initMap()
	levelCanvas = mgCreateCanvas(1024, 768)
	mgSetCanvasEnabled(levelCanvas, false)
	mgSetPos(levelCanvas, 1024, 0)

	scrollCanvas = mgCreateCanvas(4864, 768)
	mgSetPos(scrollCanvas, mgGet("game.levelpos"), 0)
	mgSetCanvasWindow(scrollCanvas, 0, 0, 1024, 768)
	mgSetCanvasMovable(scrollCanvas, "x")

	world1 = mgCreateUi("world1.xml")
	mgSetScale(world1, 1.0, 0.75)

	world1_bonus = mgCreateUi("world1_bonus.xml")
	mgSetPos(world1_bonus, 1024, 0)
	mgSetScale(world1_bonus, 1.0, 1.0)

	world2 = mgCreateUi("world2.xml")
	mgSetPos(world2, 1280, 0)
	mgSetScale(world2, 1.0, 0.75)

	world2_bonus = mgCreateUi("world2_bonus.xml")
	mgSetPos(world2_bonus, 2304, 0)
	mgSetScale(world2_bonus, 1.0, 1.0)
	
	world3 = mgCreateUi("world3.xml")
	mgSetPos(world3, 2560, 0)
	mgSetScale(world3, 1.0, 0.75)

	world3_bonus = mgCreateUi("world3_bonus.xml")
	mgSetPos(world3_bonus, 3584, 0)
	mgSetScale(world3_bonus, 1.0, 1.0)

	world4 = mgCreateUi("world4.xml")
	mgSetPos(world4, 3840, 0)
	mgSetScale(world4, 1.0, 0.75)

	mapApples0 = mgCreateImage("map_0_apples.png")
	mgSetOrigo(mapApples0, "center")
	mapApples1 = mgCreateImage("map_1_apples.png")
	mgSetOrigo(mapApples1, "center")
	mapApples2 = mgCreateImage("map_2_apples.png")
	mgSetOrigo(mapApples2, "center")
	mapApples3 = mgCreateImage("map_3_apples.png")
	mgSetOrigo(mapApples3, "center")

	mapLock = mgCreateImage("map_locked.png");
	mgSetOrigo(mapLock, "center")
	mgSetScale(mapLock, 1.02, 1.02)

	mapIndicator = mgCreateImage("map_indicator.png")
	mgSetOrigo(lock, "center")

	hardIcon = mgCreateImage("hard_icon.png")
	mgSetOrigo(hardIcon, "topright")
	mgSetPos(hardIcon, right, top)

	levels = {}
	addAllLevels()
end

function drawMap()
	mgPushCanvas(levelCanvas)	
	mgPushCanvas(scrollCanvas)
		
	mgDraw(world1)
	mgDraw(world1_bonus)
	mgDraw(world2)
	mgDraw(world2_bonus)
	mgDraw(world3)
	mgDraw(world3_bonus)
	mgDraw(world4)
	
	drawLevelIcons()
	
	mgPopCanvas()

	if mgGet("player.hard")=="1" then
		mgDraw(hardIcon)
	end

	mgPopCanvas()	
end

-------------------------------------------------------------------------------
--------------------- SHOP ----------------------------------------------------
-------------------------------------------------------------------------------


function initShop()
	shopCanvas = mgCreateCanvas(1024, 618);
	mgSetCanvasEnabled(shopCanvas, false)
	mgSetPos(shopCanvas, 0, 768)

	shopScrollCanvas = mgCreateCanvas(1024, 1024);
	mgSetCanvasWindow(shopScrollCanvas, 0, 0, 1024, 618)
	mgSetCanvasMovable(shopScrollCanvas, "y")

	shop = mgCreateUi("shop.xml")
	shopMode = false

	shopHelmet = mgCreateText("numbers", true)
	mgSetScale(shopHelmet, 0.6, 0.6)
	shopBanana = mgCreateText("numbers", true)
	mgSetScale(shopBanana, 0.6, 0.6)
	shopBaseball = mgCreateText("numbers", true)
	mgSetScale(shopBaseball, 0.6, 0.6)
	
	shopLock = mgCreateImage("menu/shop_lock.png");
	
	itemSnd = mgCreateSound("menu/snd/item.ogg")
	coinsSnd = mgCreateSound("menu/snd/coins.ogg")
	grannySnd = -1
	scruffySnd = -1
	stanleySnd = -1
	ouieSnd = -1 

	unlockScruffy = -1
	unlockStanley = -1
	unlockOuie = -1
	hardModeInfo = -1
	unlockMode = false
	unlockedMode = false
	
end

function frameShop()
end

function drawShop()
	mgPushCanvas(shopCanvas)
	mgPushCanvas(shopScrollCanvas)
	mgDraw(shop)
	mgSetText(shopHelmet, mgGet("player.helmet"))
	mgSetOrigo(shopHelmet, "center")
	mgSetText(shopBanana, mgGet("player.banana"))
	mgSetOrigo(shopBanana, "center")
	mgSetText(shopBaseball, mgGet("player.baseball"))
	mgSetOrigo(shopBaseball, "center")
	mgSetPos(shopHelmet, 202, 360)
	mgDraw(shopHelmet)
	mgSetPos(shopBanana, 442, 360)
	mgDraw(shopBanana)
	mgSetPos(shopBaseball, 682, 360)
	mgDraw(shopBaseball)
	
	if mgIsCharacterAvailable("scruffy") == false then
		mgSetPos(shopLock, 330, 720)
		mgDraw(shopLock)
	end
	if mgIsCharacterAvailable("stanley") == false then
		mgSetPos(shopLock, 570, 720)
		mgDraw(shopLock)
	end
	if mgIsCharacterAvailable("ouie") == false then
		mgSetPos(shopLock, 810, 720)
		mgDraw(shopLock)
	end
		
	mgPopCanvas()

	if unlockScruffy ~= -1 then
		mgDraw(unlockScruffy)
	end
	if unlockStanley ~= -1 then
		mgDraw(unlockStanley)	
	end
	if unlockOuie ~= -1 then
		mgDraw(unlockOuie)	
	end
	if hardModeInfo ~= -1 then
		mgDraw(hardModeInfo)	
	end
	
	mgPopCanvas()
end


-------------------------------------------------------------------------------
--------------------- INFO BOX ------------------------------------------------
-------------------------------------------------------------------------------


function initInfoBox()
	infoCanvas = mgCreateCanvas(512, 512)
	mgSetScale(infoCanvas, 0, 0)
	mgSetAlpha(infoCanvas, 0)

	infoBox = mgCreateUi("infobox.xml")
	mgSetOrigo(infoBox, "center")

	infoReplay = mgCreateUi("infobox_replay.xml")
	mgSetOrigo(infoReplay, "center")
	mgSetPos(infoReplay, 0, -50)

	infoSkip = mgCreateUi("infobox_skip.xml")
	mgSetOrigo(infoSkip, "center")
	mgSetPos(infoSkip, 0, -50)

	infoText = mgCreateText("hud")
	mgSetPos(infoText, 0, -155)
	mgSetScale(infoText, 0.8, 0.8)

	highscoreText = mgCreateText("numbers", true)
	mgSetPos(highscoreText, -110, -78)
	mgSetScale(highscoreText, 0.75, 0.75)

	selectedLevel = ""
	showInfo = false
	infoX = 0
	infoY = 0
end


function drawInfoBox()
	mgPushCanvas(infoCanvas)
	if mgIsVisible(infoBox) then
		mgDraw(infoBox)
		mgDraw(infoReplay)
		mgDraw(infoSkip)
		
		mgSetText(highscoreText, "Highscore "..levels[selectedLevel][3])
		mgDraw(infoText)
		mgDraw(highscoreText)
	end
	mgPopCanvas()
end


-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------


function init()
	top = tonumber(mgGet("display.visibleTop"))
	left = tonumber(mgGet("display.visibleLeft"))
	right = tonumber(mgGet("display.visibleRight"))
	bottom = tonumber(mgGet("display.visibleBottom"))

	lastLevel = ""	
	mapMode = false

	assetServer = mgCreateText("hud_white")
	mgSetScale(assetServer, 0.5, 0.5)
	mgSetColor(assetServer, 1, 0, 0)
	mgSetPos(assetServer, 512, 70)
	
	swoosh = mgCreateSound("snd/character/cane_swoosh1.ogg")
	zoomSnd = mgCreateSound("snd/zoom.ogg")

	mgCommand("audio.playBackgroundMusic menu/menu.ogg")

	frame = 0
	
	spinWait = mgCreateImage("wait.png")
	mgSetOrigo(spinWait, "center")
	mgSetPos(spinWait, 512, 374)
	
	unlockAll = mgCreateUi("unlock_all.xml")
	mgSetOrigo(unlockAll, "center")
	mgSetPos(unlockAll, 512, 384)
	mgSetScale(unlockAll, 0, 0)
	mgSetAlpha(unlockAll, 0)

	outOfCash = mgCreateUi("out_of_cash.xml")
	mgSetOrigo(outOfCash, "center")
	mgSetPos(outOfCash, 512, 384)
	mgSetScale(outOfCash, 0, 0)
	mgSetAlpha(outOfCash, 0)
	
	rateDialog = -1
	
	displayCoins = tonumber(mgGet("player.coins"))
	
	------- BOTTOM CANVAS
	bottomCanvas = mgCreateCanvas(1024, 200)
	mgSetPos(bottomCanvas, 0, bottom); 

	coinsText = mgCreateText("numbers", true)
	mgSetPos(coinsText, 780, 77)
	
	coinsBg = mgCreateImage("shop_plaque.png")
	mgSetScale(coinsBg, 1.07, 1.07)
	mgSetPos(coinsBg, 595, -20)
	
	fence = mgCreateImage("map_fence.png");
	mgSetPos(fence, 0, 50)

	back = mgCreateUi("back.xml")
	mgSetPos(back, 0, 50)
	
	shopButton = mgCreateUi("shop_button.xml")
	mgSetOrigo(shopButton, "pixel", 225, 40)
	mgSetPos(shopButton, 465, 85)

	moreMapButton = mgCreateUi("more_map.xml")
	mgSetOrigo(moreMapButton, "center")
	mgSetUiEnabled(moreMapButton, true)
	mgSetPos(moreMapButton, 950, 114)
	--------- 
	
	unlockedScruffy = -1
	unlockedStanley = -1
	unlockedOuie = -1
	
	shopButtonWiggle = 0
	
	initMain()
	initMap()
	initShop()
	initInfoBox()
end


function addAllLevels()
	levelsCleared = 0
	levels = {}
	addLevels(world1)
	addLevels(world1_bonus)
	addLevels(world2)
	addLevels(world2_bonus)
	addLevels(world3)
	addLevels(world3_bonus)
	addLevels(world4)
	
	if levels["2"][3]==-1 then
		lastLevel = "warming_up"
	end
end


function addLevels(ui)
	xOffset, yOffset = mgGetPos(ui)
	xScale, yScale = mgGetScale(ui)
	c = mgGetUiSelectionCount(ui)
	for i=0,c-1 do
		cmd, x0, y0, x1, y1 = mgGetUiSelectionInfo(ui, i)
		if startsWith(cmd, "script:showinfobox") then
			level = getSecond(cmd)
			x = xScale * (x0+x1)/2 + xOffset
			y = yScale * (y0+y1)/2 + yOffset
			available, score, skipped, apples, title = mgGetLevelInfo(level)
			if not available then score = -1 end
			levels[level] = {x, y, score, skipped, apples, title}
			if tonumber(levels[level][3]) > 0 then
				levelsCleared = levelsCleared + 1
			end
		end
	end
end


function drawLevelIcons()
	for k, v in pairs(levels) do
		score = tonumber(v[3])
		apples = tonumber(v[5])
		if score > 0 then
			mgSetPos(mapSign, v[1]-63, v[2]-29)
			mgDraw(mapSign)
		end
		if score == -1 then
			mgSetPos(mapLock, v[1], v[2])
			mgDraw(mapLock)
		elseif apples == 0 and score > 0 then
				mgSetPos(mapApples0, v[1], v[2])
				mgDraw(mapApples0)
		elseif apples == 1 then
			mgSetPos(mapApples1, v[1], v[2])
			mgDraw(mapApples1)
		elseif apples == 2 then
			mgSetPos(mapApples2, v[1], v[2])
			mgDraw(mapApples2)
		elseif apples == 3 then
			mgSetPos(mapApples3, v[1], v[2])
			mgDraw(mapApples3)
		end
		if k == lastLevel then
			mgSetPos(mapIndicator, v[1]-31, v[2]-50+math.sin(mgScriptTime()*4)*10)
			mgDraw(mapIndicator)
		end
	end
end


function draw()
	targetCoins = tonumber(mgGet("player.coins"))
	if targetCoins > displayCoins then
		displayCoins = math.floor(math.min(targetCoins, displayCoins + (targetCoins-displayCoins)/10 + 1))
	elseif targetCoins < displayCoins then
		displayCoins = math.floor(math.max(targetCoins, displayCoins + (targetCoins-displayCoins)/10 - 1))
	end	
	
	--MAIN MENU

	frame = frame + 1

	drawMain()
	drawMap()
	drawShop()

	mgSetRot(shopButton, math.sin(mgScriptTime()*11)*shopButtonWiggle*0.5)
	shopButtonWiggle = shopButtonWiggle * 0.95 

	mgPushCanvas(bottomCanvas)
	mgSetText(coinsText, displayCoins)
	mgSetOrigo(coinsText, "topright")
	mgDraw(fence)
	mgDraw(coinsBg)
	mgDraw(coinsText)
	mgDraw(back)
	mgDraw(shopButton)
	mgPopCanvas()
	
	if mgIsVisible(infoCanvas) then
		s = mgGetScale(infoCanvas)
		mgFullScreenColor(0, 0, 0, s*0.5)
		drawInfoBox()
	end
	
	if mgIsVisible(unlockAll) then
		mgFullScreenColor(0, 0, 0, mgGetAlpha(unlockAll)*0.5)
		mgDraw(unlockAll)
	end

	if mgIsVisible(outOfCash) then
		mgFullScreenColor(0, 0, 0, mgGetAlpha(outOfCash)*0.5)
		mgDraw(outOfCash)
	end

	if rateDialog ~= -1 and mgIsVisible(rateDialog) then
		mgFullScreenColor(0, 0, 0, mgGetAlpha(rateDialog)*0.5)
		mgDraw(rateDialog)
	end

	if unlockedScruffy ~= -1 then
		mgDraw(unlockedScruffy)
	end
	if unlockedStanley ~= -1 then
		mgDraw(unlockedStanley)	
	end
	if unlockedOuie ~= -1 then
		mgDraw(unlockedOuie)	
	end

	if more ~= -1 then
		mgDraw(more)
	end

	if ads_icon ~= -1 then
		mgSetRot(ads_icon, math.sin(mgScriptTime()*4)*0.3)
		mgDraw(ads_icon)
		
		a = mgGetScale(ads_ui)
		if a > 0 then
		mgFullScreenColor(1, 1, 1, a*0.7)
		mgDraw(ads_ui)
		end
	end

	as = mgGet("game.assetserver")
	if as ~= "" then
		mgSetText(assetServer, "Connected to " .. str(as))
		mgSetOrigo(assetServer, "center")
		mgDraw(assetServer)
	end
	
	if mgGet("game.purchasing") == "1" then
		mgFullScreenColor(0, 0, 0, 0.5)
		mgSetRot(spinWait, -5*mgScriptTime())
		mgDraw(spinWait)
	end
end


function saveLevelPos()
	x,y = mgGetPos(scrollCanvas)
	mgSet("game.levelpos", x)
	mgCommand("game.saveConfig")
end


function handleCommand(cmd)
	if cmd == "showoptions" then
		if options == -1 then
			options = mgCreateUi("options.xml")
			mgSetOrigo(options, "center")
			mgSetPos(options, 910, 260)
			mgSetScale(options, 0.0, 0.0)
			mgSetUiEnabled(options, false)
		end

		mgSetUiEnabled(options, true)
		mgSetUiModal(options, true)
		mgSetPos(options, 512, 374, "cosine", 0.25)
		mgSetScale(options, 1.0, 1.0, "bounce", 0.8)
		if (mgGet("audio.musicEnabled") == "1") then
			mgRadioSelect(options, "m1") 
		else
			mgRadioSelect(options, "m0") 
		end
		if (mgGet("audio.soundEnabled") == "1") then
			mgRadioSelect(options, "s1") 
		else
			mgRadioSelect(options, "s0") 
		end	
		if mgGet("game.controls") == "1" then mgRadioSelect(options, "c1") end
		if mgGet("game.controls") == "2" then mgRadioSelect(options, "c2") end
		if mgGet("game.controls") == "3" then mgRadioSelect(options, "c3") end
		if mgGet("game.controls") == "4" then mgRadioSelect(options, "c4") end
	end

	if cmd == "hideoptions" then
		mgSetUiEnabled(options, false)
		mgSetUiModal(options, false)
		mgSetPos(options, 910, 260, "cosine", 0.2)
		mgSetScale(options, 0, 0, "easein", 0.3)
	end

	if cmd == "showMoreMenu" then
		if more == -1 then
			more = mgCreateUi("more_map_menu.xml")
			mgSetOrigo(more, "center")
			mgSetScale(more, 0, 0)
			mgSetPos(more, right-80, bottom-80)
			mgSetRot(more, 4)
			mgSetAlpha(more, 0.0)
		end

		mgSetUiEnabled(more, true)
		mgSetUiModal(more, true)
		mgSetPos(more, 512, 384, "easeout", 0.4)
		mgSetRot(more, 0, "easeout", 0.4)
		mgSetScale(more, 1, 1, "bounce", 0.8)
		mgSetAlpha(more, 1, "linear", 0.2)
		
		if ads_icon ~= -1 and mgGet("player.adsplus") == "1" then
			mgSetScale(ads_icon, 1, 1, "cosine", 0.3)
			mgSetUiModal(ads_icon, true)
		end		
	end

	if cmd == "hideMoreMenu" then
		mgSetUiEnabled(more, false)
		mgSetUiModal(more, false)
		mgSetScale(more, 0, 0, "easein", 0.5)
		mgSetPos(more, right-80, bottom-80, "easein", 0.5)
		mgSetRot(more, 4, "easein", 0.4)
		mgSetAlpha(more, 0, "linear", 0.6)

		if ads_icon ~= -1 then
			mgSetScale(ads_icon, 0, 0, "cosine", 0.3)
			mgSetUiModal(ads_icon, false)
		end
	end
	
	if cmd == "levels" then
		mapMode = true
		shopButtonWiggle = 1
		mgSetPos(bottomCanvas, 0, bottom-200, "easein", 0.4)
		mgSetCanvasEnabled(mainCanvas, false)
		mgSetCanvasEnabled(levelCanvas, true)
		mgSetPos(mainCanvas, -1024, 0, "cosine", 0.4)
		mgSetPos(levelCanvas, 0, 0, "cosine", 0.4)
		mgPlaySound(swoosh)
	end

	if cmd == "main" then
		if shopMode then
			handleCommand("toggleShop")
		else
			mapMode = false
			mgSetPos(bottomCanvas, 0, bottom, "cosine", 0.7); 
			
			mgCommand("audio.playBackgroundMusic menu/menu.ogg")

			mgSetOrigo(levelCanvas, "topleft")
			mgSetPos(levelCanvas, 0, 0)

			mgSetCanvasEnabled(mainCanvas, true)
			mgSetCanvasEnabled(levelCanvas, false)
			mgSetPos(mainCanvas, 0, 0, "cosine", 0.4)
			mgSetPos(levelCanvas, 1024, 0, "cosine", 0.4)
			mgPlaySound(swoosh)
			granny_rot = 0.2
		end
	end
	
	if startsWith(cmd, "showinfobox") then	
		selectedLevel = getSecond(cmd)
		if levels[selectedLevel][3] ~= -1 then
			lastLevel = selectedLevel
			mgPlaySound(zoomSnd)

			mgSetText(infoText, levels[selectedLevel][6])
			mgSetOrigo(infoText, "center")
			
			x,y = mgGetPos(scrollCanvas)
			infoX = levels[selectedLevel][1] + x
			infoY = levels[selectedLevel][2]
			mgSetPos(infoCanvas, infoX, infoY)
			mgSetPos(infoCanvas, 512, 384, "bounce", 0.7)
			mgSetScale(infoCanvas, 0, 0, "linear", 0.1)
			mgSetScale(infoCanvas, 1, 1, "bounce", 0.7)
			mgSetAlpha(infoCanvas, 1, "cosine", 0.1)
			mgSetUiModal(infoBox, true);

			mgSetOrigo(levelCanvas, "pixel", infoX, infoY)
			mgSetPos(levelCanvas, infoX, infoY)
			mgSetScale(levelCanvas, 1.5, 1.5, "cosine", 1.2)

			if levels[selectedLevel][3] == 0 then
				mgSetAlpha(infoReplay, 0.0)
				mgSetAlpha(highscoreText, 0)
				mgSetUiEnabled(infoReplay, false)
				if not levels[selectedLevel][4] then
					mgSetAlpha(infoSkip, 1.0)
					mgSetUiEnabled(infoSkip, true)
					mgSetUiModal(infoSkip, true);
				else
					mgSetAlpha(infoSkip, 0.0)
					mgSetUiEnabled(infoSkip, false)
				end
			else
				mgSetAlpha(infoReplay, 1.0)
				mgSetAlpha(highscoreText, 1)
				mgSetUiEnabled(infoReplay, true)
				mgSetAlpha(infoSkip, 0.0)
				mgSetUiEnabled(infoSkip, false)
				mgSetUiModal(infoReplay, true);
			end
		else
			mgSetScale(unlockAll, 1, 1, "bounce", 0.7)
			mgSetAlpha(unlockAll, 1, "bounce", 0.7)
			mgSetUiModal(unlockAll, true)
		end
	end
	
	if cmd == "hideunlockall" then
		mgSetScale(unlockAll, 0, 0, "cosine", 0.3)
		mgSetAlpha(unlockAll, 0, "cosine", 0.3)
		mgSetUiModal(unlockAll, false)
	end

	if cmd == "unlockall" then
		handleCommand("hideunlockall")
		mgCommand("player.unlockall")
	end
	
	if cmd == "restorepurchases" then
		handleCommand("hideunlockall")
		mgCommand("player.restorepurchases")
	end
	
	if cmd == "hideinfobox" then
		mgSetUiModal(infoBox, false);
		mgSetUiModal(infoReplay, false);
		mgSetUiModal(infoSkip, false);
		mgSetAlpha(infoCanvas, 0, "easein", 0.3)
		mgSetScale(infoCanvas, 0, 0, "easein", 0.3)
		x,y = mgGetPos(scrollCanvas)
		infoX = levels[selectedLevel][1] + x
		infoY = levels[selectedLevel][2]
		mgSetPos(infoCanvas, infoX, infoY, "easein", 0.3)
		mgSetScale(levelCanvas, 1, 1, "easeout", 0.3)
	end
	
	if cmd == "skiplevel" then
		if not levels[selectedLevel][4] then
			mgCommand("level.skip "..selectedLevel)
			addAllLevels()
			handleCommand("hideinfobox")
		end
	end
	
	if cmd == "startreplay" then
		if levels[selectedLevel][3] > 0 then
			saveLevelPos()
			mgSetUiModal(infoBox, false);
			mgSetScale(levelCanvas, 10, 10, "easeout", 0.4)
			mgCommand("level.startreplay "..selectedLevel)
			mgSetAlpha(infoCanvas, 0, "linear", 0.1)
		end
	end
	
	if cmd == "startlevel" then
		saveLevelPos()
		mgSetUiModal(infoBox, false);
		mgSetScale(levelCanvas, 10, 10, "easeout", 0.4)
		mgCommand("level.start "..selectedLevel)
		mgSetAlpha(infoCanvas, 0, "linear", 0.1)
	end

	if cmd == "toggleShop" then
		if shopMode then
			mgSetCanvasEnabled(shopCanvas, false)
			mgSetCanvasEnabled(levelCanvas, true)
			mgSetPos(shopCanvas, 0, 768, "cosine", 0.4)
			mgSetPos(levelCanvas, 0, 0, "cosine", 0.4)
			mgSetPos(bottomCanvas, 0, bottom-200, "cosine", 0.4)
			shopMode = false
		else
			mgSetCanvasEnabled(shopCanvas, true)
			mgSetCanvasEnabled(levelCanvas, false)
			mgSetPos(shopCanvas, 0, 150, "cosine", 0.4)
			mgSetPos(levelCanvas, 0, 0)
			mgSetOrigo(levelCanvas, "pixel", 0, 0)
			mgSetPos(levelCanvas, 0, -618, "cosine", 0.4)
			mgSetPos(bottomCanvas, 0, top-50, "cosine", 0.4)
			mgRadioSelect(shop, mgGet("player.character")) 
			if mgGet("player.hard")=="1" then
				mgRadioSelect(shop, "hard")
			else
				mgRadioSelect(shop, "normal")
			end
			shopMode = true
		end
	end
	
	if cmd == "activate" then
		--Center map at last level played
		infoX = levels[selectedLevel][1] + x
		dx = 512-infoX
		x, y = mgGetPos(scrollCanvas)
		x = x + dx
		mgSetPos(scrollCanvas, x, y)

		mgSetScale(levelCanvas, 1.0, 1.0, "easeout", 0.4)
		handleCommand("hideinfobox")
		addAllLevels()
		if mgGet("player.rated") == "0" and mgGet("player.hard")=="0" and levelsCleared == 9 then
			handleCommand("showratedialog")
		end
	end
	
	if cmd == "showoutofcash" then	
		mgSetScale(outOfCash, 1, 1, "bounce", 0.7)
		mgSetAlpha(outOfCash, 1, "bounce", 0.7)
		mgSetUiModal(outOfCash, true)
	end
	
	if cmd == "hideoutofcash" then
		mgSetScale(outOfCash, 0, 0, "cosine", 0.3)
		mgSetAlpha(outOfCash, 0, "cosine", 0.3)
		mgSetUiModal(outOfCash, false)
	end

	if cmd == "showratedialog" then
		if rateDialog == -1 then
			rateDialog = mgCreateUi("rate.xml")
			mgSetOrigo(rateDialog, "center")
			mgSetPos(rateDialog, 512, 384)
			mgSetScale(rateDialog, 0, 0)
			mgSetAlpha(rateDialog, 0)
		end

		mgCommand("player.rate")
		mgSetScale(rateDialog, 1, 1, "bounce", 0.7)
		mgSetAlpha(rateDialog, 1, "bounce", 0.7)
		mgSetUiModal(rateDialog, true)
	end
	
	if cmd == "hideratedialog" then	
		mgSetScale(rateDialog, 0, 0, "cosine", 0.3)
		mgSetAlpha(rateDialog, 0, "cosine", 0.3)
		mgSetUiModal(rateDialog, false)
	end
	
	if startsWith(cmd, "buycoins") then	
		amount = getSecond(cmd)
		mgCommand("player.buycoins "..amount)
		handleCommand("hideoutofcash")
	end
	
	if startsWith(cmd, "buy ") then
		item = getSecond(cmd)
		ok = mgCommand("player.buy " .. item)
		if ok == "1" then
			mgPlaySound(itemSnd)
		end
	end
	
	if startsWith(cmd, "hideunlock") then
		if unlockScruffy ~= -1 then
			mgSetScale(unlockScruffy, 0, 0, "cosine", 0.3)
			mgSetUiModal(unlockScruffy, false)
			unlockMode = false
		end
		if unlockStanley ~= -1 then
			mgSetScale(unlockStanley, 0, 0, "cosine", 0.3)
			mgSetUiModal(unlockStanley, false)
			unlockMode = false
		end
		if unlockOuie ~= -1 then
			mgSetScale(unlockOuie, 0, 0, "cosine", 0.3)
			mgSetUiModal(unlockOuie, false)
			unlockMode = false
		end
	end
	
	if startsWith(cmd, "hideunlocked") then
		if unlockedScruffy ~= -1 then
			mgSetScale(unlockedScruffy, 0, 0, "cosine", 0.3)
			mgSetUiModal(unlockedScruffy, false)
			unlockedMode = false
		end
		if unlockedStanley ~= -1 then
			mgSetScale(unlockedStanley, 0, 0, "cosine", 0.3)
			mgSetUiModal(unlockedStanley, false)
			unlockedMode = false
		end
		if unlockedOuie ~= -1 then
			mgSetScale(unlockedOuie, 0, 0, "cosine", 0.3)
			mgSetUiModal(unlockedOuie, false)
			unlockedMode = false
		end
	end
	
	if startsWith(cmd, "unlockedcharacter") then
		c = getSecond(cmd)
		if c == "scruffy" then
			if unlockedScruffy == -1 then
				unlockedScruffy = mgCreateUi("unlocked_scruffy.xml")
				mgSetOrigo(unlockedScruffy, "center")
				mgSetPos(unlockedScruffy, 512, 384)
			end
			mgSetScale(unlockedScruffy, 0, 0)
			mgSetScale(unlockedScruffy, 1, 1, "cosine", 0.3)
			mgSetUiModal(unlockedScruffy, true)
			unlockedMode = true
		end
		if c == "stanley" then
			if unlockedStanley == -1 then
				unlockedStanley = mgCreateUi("unlocked_stanley.xml")
				mgSetOrigo(unlockedStanley, "center")
				mgSetPos(unlockedStanley, 512, 384)
			end
			mgSetScale(unlockedStanley, 0, 0)
			mgSetScale(unlockedStanley, 1, 1, "cosine", 0.3)
			mgSetUiModal(unlockedStanley, true)
			unlockedMode = true
		end
		if c == "ouie" then
			if unlockedOuie == -1 then
				unlockedOuie = mgCreateUi("unlocked_ouie.xml")
				mgSetOrigo(unlockedOuie, "center")
				mgSetPos(unlockedOuie, 512, 384)
			end
			mgSetScale(unlockedOuie, 0, 0)
			mgSetScale(unlockedOuie, 1, 1, "cosine", 0.3)
			mgSetUiModal(unlockedOuie, true)
			unlockedMode = true
		end
	end

	if startsWith(cmd, "character") then
		c = getSecond(cmd)
		if mgIsCharacterAvailable(c) then
			mgRadioSelect(shop, c) 
			mgCommand("player.selectcharacter " .. c)
			if c=="granny" then
				if grannySnd == -1 then grannySnd = mgCreateSound("menu/snd/granny.ogg") end
				 mgPlaySound(grannySnd) 
			end
			if c=="scruffy" then 
				if scruffySnd == -1 then scruffySnd = mgCreateSound("menu/snd/scruffy.ogg") end
				mgPlaySound(scruffySnd) 
			end
			if c=="stanley" then
				if stanleySnd == -1 then stanleySnd = mgCreateSound("menu/snd/stanley.ogg") end
				mgPlaySound(stanleySnd) 
			end
			if c=="ouie" then
				if ouieSnd == -1 then ouieSnd = mgCreateSound("menu/snd/ouie.ogg") end
				mgPlaySound(ouieSnd)
			end
		else
			mgRadioSelect(shop, mgGet("player.character")) 
			if c == "scruffy" then
				if unlockScruffy == -1 then
					unlockScruffy = mgCreateUi("unlock_scruffy.xml")
					mgSetOrigo(unlockScruffy, "center")
					mgSetPos(unlockScruffy, 512, 284)
					mgSetScale(unlockScruffy, 1, 0)
				end
				mgSetScale(unlockScruffy, 1, 1, "bounce", 0.6)
				mgSetUiModal(unlockScruffy, true)
				unlockMode = true
			elseif c == "stanley" then
				if unlockStanley == -1 then
					unlockStanley = mgCreateUi("unlock_stanley.xml")
					mgSetOrigo(unlockStanley, "center")
					mgSetPos(unlockStanley, 512, 284)
					mgSetScale(unlockStanley, 1, 0)
				end
				mgSetScale(unlockStanley, 1, 1, "bounce", 0.6)
				mgSetUiModal(unlockStanley, true)
				unlockMode = true
			else
				if unlockOuie == -1 then
					unlockOuie = mgCreateUi("unlock_ouie.xml")
					mgSetOrigo(unlockOuie, "center")
					mgSetPos(unlockOuie, 512, 284)
					mgSetScale(unlockOuie, 1, 0)
				end
				mgSetScale(unlockOuie, 1, 1, "bounce", 0.6)
				mgSetUiModal(unlockOuie, true)
				unlockMode = true
			end
		end
	end

	if startsWith(cmd, "purchased") then
		item = getSecond(cmd)
		if startsWith(item, "com.mediocre.grannysmith.coins") then
			mgPlaySound(coinsSnd)
		elseif startsWith(item, "com.mediocre.grannysmith.unlockall") or item=="restore" then 
			addAllLevels()
		end
	end
	
	if cmd == "rate" then
		mgCommand("player.rate")
		if mgGet("game.platform") == "android" then
			mgCommand("game.url http://play.google.com/store/apps/details?id=com.mediocre.grannysmith")
		else
			mgCommand("game.url itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=529096189")
		end
	end

	if cmd == "moregames" then
		if mgGet("game.platform") == "android" then
			mgCommand("game.url http://www.grannysmithgame.com/moregames_android")
		else
			mgCommand("game.url http://www.grannysmithgame.com/moregames")
		end
	end
	
	if cmd == "backbutton" then	
		if options ~= -1 and mgIsVisible(options) then
			handleCommand("hideoptions")
		elseif hardModeInfo ~= -1 and mgIsVisible(hardModeInfo) then
			handleCommand("hidehardmodeinfo")
		elseif more ~= -1 and mgIsVisible(more) then
			handleCommand("hideMoreMenu")
		elseif mgIsVisible(infoCanvas) then
			handleCommand("hideinfobox")
		elseif unlockMode then
			handleCommand("hideunlock")
		elseif unlockedMode then
			handleCommand("hideunlocked")
		elseif shopMode then
			handleCommand("toggleShop") 
		elseif mapMode then
			handleCommand("main")
		else
			mgCommand("game.quit")
		end
	end

	if cmd == "menubutton" then
		if mapMode == false and (more == -1 or mgIsVisible(more)==false) then
			if options ~= -1 and mgIsVisible(options) then
				handleCommand("hideoptions")
			else
				handleCommand("showoptions")
			end
		end
	end
	
	if cmd == "normal" then
		mgCommand("game.difficulty normal")
		addAllLevels()
	end

	if cmd == "hard" then
		if hardModeInfo == -1 then
			hardModeInfo = mgCreateUi("hard_mode.xml")
			mgSetOrigo(hardModeInfo, "center")
			mgSetPos(hardModeInfo, 512, 284)
			mgSetScale(hardModeInfo, 0, 0)
		end

		mgSetScale(hardModeInfo, 1, 1, "bounce", 0.5)
		mgSetUiModal(hardModeInfo, true)

		mgCommand("game.difficulty hard")
		addAllLevels()
	end
	
	if cmd == "hidehardmodeinfo" then
		if hardModeInfo ~= -1 then
			mgSetUiModal(hardModeInfo, false)
			mgSetScale(hardModeInfo, 0, 0, "cosine", 0.2)
		end
	end
	
	if cmd == "showads" then
		handleCommand("hideMoreMenu")
		mgSetScale(ads_ui, 1.0, 1.0, "cosine", 0.3)
		mgSetUiModal(ads_ui, true)
		mgCommand("player.adsshown")
		mgSetScale(ads_icon, 0, 0, "cosine", 0.3)
	end

	if cmd == "hideads" then
		mgSetScale(ads_ui, 0.0, 0.0, "cosine", 0.3)
		mgSetUiModal(ads_ui, false)
	end

	if cmd == "checkads" then
		print("checking ads")
		if ads_ui ~= -1 then
			mgSetUiModal(ads_ui, false)
		end
		checkAds()
	end
end

