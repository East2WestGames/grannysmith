sceneLength = {}
sceneStart = {}
sceneFunc = {}
sceneCalled = {}

--Tweak time offset
timeOffset = 2

function init()

	-- Add all scenes here
	addScene(sceneDennisHenrik, 5.0)
	addScene(sceneErik, 3.0)
	addScene(sceneDouglas, 3.0)
	addScene(sceneUses, 5.0)
	addScene(sceneMediocre, 5.0)
	addScene(sceneEnd, 1.0)

	ball = mgCreateImage("ball.png")
	background = mgCreateImage("background.jpg")
	clouds = mgCreateImage("clouds.jpg")
	clouds2 = mgCreateImage("clouds.jpg")
	dennis_henrik = mgCreateImage("dennis_henrik.png")
	erik = mgCreateImage("erik.png")
	douglas = mgCreateImage("douglas.png")
	uses = mgCreateImage("uses.png")
	mediocre = mgCreateImage("mediocre.png")

	throwSnd = mgCreateSound("throw0.ogg")
	hitSnd = mgCreateSound("hit1.ogg")

	mgSetScale(clouds, 2, 2)
	mgSetAlpha(clouds, .4)
	mgSetScale(clouds2, 2, 2)
	mgSetAlpha(clouds2, .4)
	mgSetPos(clouds2, 1024, 0)

	canvas = mgCreateCanvas(1024, 768)
	canvas2 = mgCreateCanvas(1024, 768)
	canvas3 = mgCreateCanvas(1024, 768)
	canvas4 = mgCreateCanvas(1024, 768)
	canvas5 = mgCreateCanvas(1024, 768)
	
	local current = 0.0
	for i=1, #sceneLength do
		sceneStart[i] = current
		current = current + sceneLength[i]
	end	
end


function addScene(func, length)
	sceneFunc[#sceneFunc+1] = func
	sceneLength[#sceneLength+1] = length
	sceneCalled[#sceneLength+1] = false
end


function getScene(time)
	local t = time + timeOffset
	scene = 0
	sceneTime = 0.0
	for i=1, #sceneStart do
		if (t > sceneStart[i]) then
			scene = i
			sceneTime = t-sceneStart[i]
		end
	end
	return scene, sceneTime
end


function frame()
end


function draw()
	time = mgScriptTime()
	local scene = 0
	local t = 0.0
	scene, t = getScene(time)
	sceneFunc[scene](t, not sceneCalled[scene])
	sceneCalled[scene] = true
end


----------------- SCENES -----------------


function sceneDennisHenrik(t, first)
	if first then
		mgSetScale(canvas, 1, 1)
		mgSetOrigo(dennis_henrik, "center")
		mgSetPos(dennis_henrik, 512, 384)
		mgSetScale(dennis_henrik, 1, 0)
		mgSetScale(dennis_henrik, 1, 1, "bounce", 1)
		mgSetPos(ball, 300, -250)
		mgSetScale(ball, 2, 2)
		throwVar = true
		hitVar = true
	end
	
	mgPushCanvas(canvas)
	mgDraw(background)
	mgDraw(clouds)
	mgDraw(clouds2)
	mgDraw(dennis_henrik)
	mgPopCanvas()
	mgSetPos(clouds, -400, 0, "linear", 22)
	mgSetPos(clouds2, 624, 0, "linear", 22)
	mgDraw(ball)

	if(t > 3.8 and t < 4) then

		if(throwVar) then
			mgPlaySound(throwSnd)
			throwVar = false
		end

		mgSetPos(ball, 470, 350, "easeout", .2)
		mgSetScale(ball, 1, 1, "easeout", .2)
	end

	if(t > 4) then

		if(hitVar) then
			mgPlaySound(hitSnd)
			hitVar = false
		end

		mgSetPos(ball, 720, -250, "easeout", .3)
		mgSetScale(ball, 2, 2, "easeout", .3)
		mgSetPos(dennis_henrik, 512, 1100, "easeout", .5)
	end
end



function sceneErik(t, first)
	if first then
		mgSetScale(canvas2, 1, 1)
		mgSetOrigo(erik, "center")
		mgSetPos(erik, 512, 384)
		mgSetScale(erik, 1, 0)
		mgSetScale(erik, 1, 1, "bounce", 1)
		mgSetPos(ball, 300, 1000)
		mgSetScale(ball, 2, 2)
		throwVar = true
		hitVar = true
	end
	
	mgPushCanvas(canvas2)
	mgDraw(background)
	mgDraw(clouds)
	mgDraw(clouds2)
	mgDraw(erik)
	mgPopCanvas()
	mgDraw(ball)

	if(t > 1.8 and t < 2) then

		if(throwVar) then
			mgPlaySound(throwSnd)
			throwVar = false
		end

		mgSetPos(ball, 470, 150, "easeout", .2)
		mgSetScale(ball, 1, 1, "easeout", .2)
	end

	if(t > 2) then

		if(hitVar) then
			mgPlaySound(hitSnd)
			hitVar = false
		end

		mgSetPos(ball, 720, 1000, "easeout", .3)
		mgSetScale(ball, 2, 2, "easeout", .3)
		mgSetPos(erik, 512, -200, "easeout", .5)
	end
end



function sceneDouglas(t, first)
	if first then
		mgSetScale(canvas3, 1, 1)
		mgSetOrigo(douglas, "center")
		mgSetPos(douglas, 512, 384)
		mgSetScale(douglas, 1, 0)
		mgSetScale(douglas, 1, 1, "bounce", 1)
		mgSetPos(ball, 600, -250)
		mgSetScale(ball, 2, 2)
		throwVar = true
		hitVar = true
	end
	
	mgPushCanvas(canvas3)
	mgDraw(background)
	mgDraw(clouds)
	mgDraw(clouds2)
	mgDraw(douglas)
	mgDraw(ball)
	mgPopCanvas()

	if(t > 1.8 and t < 2) then

		if(throwVar) then
			mgPlaySound(throwSnd)
			throwVar = false
		end

		mgSetPos(ball, 470, 400, "easeout", .2)
		mgSetScale(ball, 1, 1, "easeout", .2)
	end

	if(t > 2) then

		if(hitVar) then
			mgPlaySound(hitSnd)
			hitVar = false
		end

		mgSetPos(ball, 250, -250, "easeout", .3)
		mgSetScale(ball, 2, 2, "easeout", .3)
		mgSetPos(douglas, 512, 1100, "easeout", .5)
	end
end



function sceneUses(t, first)
	if first then
		mgSetScale(canvas4, 1, 1)
		mgSetOrigo(uses, "center")
		mgSetPos(uses, 512, 384)
		mgSetScale(uses, 1, 0)
		mgSetScale(uses, 1, 1, "bounce", 1)
		mgSetPos(ball, -250, 50)
		mgSetScale(ball, 2, 2)
		throwVar = true
		hitVar = true
	end
	
	mgPushCanvas(canvas4)
	mgDraw(background)
	mgDraw(clouds)
	mgDraw(clouds2)
	mgDraw(uses)
	mgDraw(ball)
	mgPopCanvas()

	if(t > 3.8 and t < 4) then

		if(throwVar) then
			mgPlaySound(throwSnd)
			throwVar = false
		end

		mgSetPos(ball, 512, 380, "easeout", .2)
		mgSetScale(ball, 1, 1, "easeout", .2)
	end

	if(t > 4) then

		if(hitVar) then
			mgPlaySound(hitSnd)
			hitVar = false
		end

		mgSetPos(ball, 800, -250, "easeout", .3)
		mgSetScale(ball, 2, 2, "easeout", .3)
		mgSetPos(uses, 1400, 384, "easeout", .5)
	end
end



function sceneMediocre(t, first)
	if first then
		mgSetScale(canvas5, 1, 1)
		mgSetOrigo(mediocre, "center")
		mgSetPos(mediocre, 512, 0)
		mgSetPos(mediocre, 512, 384, "bounce", 1)
		mgSetScale(mediocre, 1, 0)
		mgSetScale(mediocre, 1, 1, "bounce", 1)
		mgSetPos(ball, 300, 900)
		mgSetScale(ball, 4, 4)
		throwVar = true
		hitVar = true
	end
	
	mgPushCanvas(canvas5)
	mgDraw(background)
	mgDraw(clouds)
	mgDraw(clouds2)
	mgDraw(mediocre)
	mgDraw(ball)
	mgPopCanvas()

	if(t > 3.8 and t < 4) then

		if(throwVar) then
			mgPlaySound(throwSnd)
			throwVar = false
		end

		mgSetPos(ball, 470, 100, "easeout", .2)
		mgSetScale(ball, .5, .5, "easeout", .2)
	end

	if(t > 4) then

		if(hitVar) then
			mgPlaySound(hitSnd)
			hitVar = false
		end

		mgSetPos(ball, 700, 900, "easeout", .3)
		mgSetScale(ball, 4, 4, "easeout", .3)
		mgSetPos(mediocre, 512, 300, "easeout", .5)
		mgSetScale(mediocre, 0, 0, "easeout", .2)
	end
end



function sceneEnd(t, first)
	if first then
		mgCommand("game.menu")
	end
end

