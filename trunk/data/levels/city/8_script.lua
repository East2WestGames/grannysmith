function init()
	leaves_01 = mgCreateEffect("leaves")
	leaves_02 = mgCreateEffect("leaves")
	leaves_03 = mgCreateEffect("leaves")
	leaves_04 = mgCreateEffect("leaves")
	leaves_05 = mgCreateEffect("leaves")
	leaves_06 = mgCreateEffect("leaves")
	leaves_07 = mgCreateEffect("leaves")
	leaves_08 = mgCreateEffect("leaves")
	leaves_09 = mgCreateEffect("leaves")
	leaves_10 = mgCreateEffect("leaves")
	leaves_11 = mgCreateEffect("leaves")
	leaves_12 = mgCreateEffect("leaves")
	
	
	leaves_fx_01 = false
	leaves_fx_02 = false
	leaves_fx_03 = false
	leaves_fx_04 = false
	leaves_fx_05 = false
	leaves_fx_06 = false
	leaves_fx_07 = false
	leaves_fx_08 = false
	leaves_fx_09 = false
	leaves_fx_10 = false
	leaves_fx_11 = false
	leaves_fx_12 = false
	
	
	vF = 0.1
	yF = 0.1
	xR1 = -2
	xR2 = 6
	yR1 = -2
	yR2 = 4
end


function frame()
	-- LEAVES FX
	x,y = mgGetPose("dude.torso")
	vx,vy = mgGetVelocity("dude.torso")


	if mgInSensor("leaf_sensor_01","dude.torso") and not leaves_fx_01 then
		for leaves_n = 0,40 do
			mgParticle(leaves_01, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*vF + mgRnd(xR1, xR2), vy*yF + mgRnd(yR1, yR2), 0);
		end
		leaves_fx_01 = true
	end
	
	if mgInSensor("leaf_sensor_02","dude.torso") and not leaves_fx_02 then
		for leaves_n = 0,40 do
			mgParticle(leaves_02, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*vF + mgRnd(xR1, xR2), vy*yF + mgRnd(yR1, yR2), 0);
		end
		leaves_fx_02 = true
	end
	
	if mgInSensor("leaf_sensor_03","dude.torso") and not leaves_fx_03 then
		for leaves_n = 0,40 do
			mgParticle(leaves_03, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*vF + mgRnd(xR1, xR2), vy*yF + mgRnd(yR1, yR2), 0);
		end
		leaves_fx_03 = true
	end
	
	if mgInSensor("leaf_sensor_04","dude.torso") and not leaves_fx_04 then
		for leaves_n = 0,40 do
			mgParticle(leaves_04, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*vF + mgRnd(xR1, xR2), vy*yF + mgRnd(yR1, yR2), 0);
		end
		leaves_fx_04 = true
	end	
	
	if mgInSensor("leaf_sensor_05","dude.torso") and not leaves_fx_05 then
		for leaves_n = 0,40 do
			mgParticle(leaves_05, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*vF + mgRnd(xR1, xR2), vy*yF + mgRnd(yR1, yR2), 0);
		end
		leaves_fx_05 = true
	end	
	
	if mgInSensor("leaf_sensor_06","dude.torso") and not leaves_fx_06 then
		for leaves_n = 0,40 do
			mgParticle(leaves_06, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*vF + mgRnd(xR1, xR2), vy*yF + mgRnd(yR1, yR2), 0);
		end
		leaves_fx_06 = true
	end	
	
	if mgInSensor("leaf_sensor_07","dude.torso") and not leaves_fx_07 then
		for leaves_n = 0,40 do
			mgParticle(leaves_07, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*vF + mgRnd(xR1, xR2), vy*yF + mgRnd(yR1, yR2), 0);
		end
		leaves_fx_07 = true
	end	
	
	if mgInSensor("leaf_sensor_08","dude.torso") and not leaves_fx_08 then
		for leaves_n = 0,40 do
			mgParticle(leaves_08, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*vF + mgRnd(xR1, xR2), vy*yF + mgRnd(yR1, yR2), 0);
		end
		leaves_fx_08 = true
	end	
	
	if mgInSensor("leaf_sensor_09","dude.torso") and not leaves_fx_09 then
		for leaves_n = 0,40 do
			mgParticle(leaves_09, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*vF + mgRnd(xR1, xR2), vy*yF + mgRnd(yR1, yR2), 0);
		end
		leaves_fx_09 = true
	end	
	
	if mgInSensor("leaf_sensor_10","dude.torso") and not leaves_fx_10 then
		for leaves_n = 0,40 do
			mgParticle(leaves_10, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*vF + mgRnd(xR1, xR2), vy*yF + mgRnd(yR1, yR2), 0);
		end
		leaves_fx_10 = true
	end	
	
	if mgInSensor("leaf_sensor_11","dude.torso") and not leaves_fx_11 then
		for leaves_n = 0,40 do
			mgParticle(leaves_11, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*vF + mgRnd(xR1, xR2), vy*yF + mgRnd(yR1, yR2), 0);
		end
		leaves_fx_11 = true
	end	
	
	if mgInSensor("leaf_sensor_12","dude.torso") and not leaves_fx_12 then
		for leaves_n = 0,40 do
			mgParticle(leaves_12, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*vF + mgRnd(xR1, xR2), vy*yF + mgRnd(yR1, yR2), 0);
		end
		leaves_fx_12 = true
	end	
end