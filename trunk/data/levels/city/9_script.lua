function init()
	trigger_01 = false
	trigger_02 = false
		
	leaves_01 = mgCreateEffect("leaves")
	leaves_02 = mgCreateEffect("leaves")
	leaves_03 = mgCreateEffect("leaves")
	leaves_04 = mgCreateEffect("leaves")
	leaves_05 = mgCreateEffect("leaves")
	leaves_06 = mgCreateEffect("leaves")
	
	leaves_fx_01 = false
	leaves_fx_02 = false
	leaves_fx_03 = false
	leaves_fx_04 = false
	leaves_fx_05 = false
	leaves_fx_06 = false
	
	vF = 0.1
	yF = 0.1
	xR1 = -2
	xR2 = 6
	yR1 = -2
	yR2 = 4
end


function frame()

	if mgInSensor("sns_invfloor_01", "dude.torso") and (trigger_01 == false) then
	trigger_01 = true
	
		if (trigger_02 == false) then
			mgSetFilter("gmt_invfloor_01", 4, 254)
		else
			mgSetFilter("gmt_invfloor_01", 4, 252)
		end
	end
	
	if mgInSensor("sns_invfloor_01", "badguy.torso") and (trigger_02 == false) then
	trigger_02 = true
	
		if (trigger_01 == false) then
			mgSetFilter("gmt_invfloor_01", 4, 253)
		else
			mgSetFilter("gmt_invfloor_01", 4, 252)
		end
	
	end
	
	
	
	
	
	
	-- LEAVES FX
	x,y = mgGetPose("dude.torso")
	vx,vy = mgGetVelocity("dude.torso")


	if mgInSensor("leaf_sensor_01","dude.torso") and not leaves_fx_01 then
		for leaves_n = 0,40 do
			mgParticle(leaves_01, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*vF + mgRnd(xR1, xR2), vy*yF + mgRnd(yR1, yR2), 0);
		end
		leaves_fx_01 = true
	end
	
	if mgInSensor("leaf_sensor_02","dude.torso") and not leaves_fx_02 then
		for leaves_n = 0,40 do
			mgParticle(leaves_02, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*vF + mgRnd(xR1, xR2), vy*yF + mgRnd(yR1, yR2), 0);
		end
		leaves_fx_02 = true
	end
	
	if mgInSensor("leaf_sensor_03","dude.torso") and not leaves_fx_03 then
		for leaves_n = 0,40 do
			mgParticle(leaves_03, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*vF + mgRnd(xR1, xR2), vy*yF + mgRnd(yR1, yR2), 0);
		end
		leaves_fx_03 = true
	end
	
	if mgInSensor("leaf_sensor_04","dude.torso") and not leaves_fx_04 then
		for leaves_n = 0,40 do
			mgParticle(leaves_04, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*vF + mgRnd(xR1, xR2), vy*yF + mgRnd(yR1, yR2), 0);
		end
		leaves_fx_04 = true
	end	
	
	if mgInSensor("leaf_sensor_05","dude.torso") and not leaves_fx_05 then
		for leaves_n = 0,40 do
			mgParticle(leaves_05, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*vF + mgRnd(xR1, xR2), vy*yF + mgRnd(yR1, yR2), 0);
		end
		leaves_fx_05 = true
	end	
	
	if mgInSensor("leaf_sensor_06","dude.torso") and not leaves_fx_06 then
		for leaves_n = 0,40 do
			mgParticle(leaves_06, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*vF + mgRnd(xR1, xR2), vy*yF + mgRnd(yR1, yR2), 0);
		end
		leaves_fx_06 = true
	end	
end