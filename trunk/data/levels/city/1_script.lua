function init()
	msg = mgCreateImage("hud/info_throw.png")
	mgSetOrigo(msg, "center")
	mgSetPos(msg, 690, tonumber(mgGet("display.visibleBottom"))-235)
	mgSetScale(msg, 1, 1)
end


function draw()
	if mgInSensor("throw", "dude.torso") or mgInSensor("throw2", "dude.torso") then
		mgDraw(msg)
	end
end

