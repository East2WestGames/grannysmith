function init()
	trigger_01 = false
	trigger_02 = false
end


function frame()
	if mgInSensor("sns_hidden_01", "dude.torso") and (trigger_01 == false) then
		trigger_01 = true
	
		if (trigger_02 == false) then
			mgSetFilter("gmt_hidden_01", 4, 253)
		else
			mgSetFilter("gmt_hidden_01", 4, 255)
		end
	end
	
	if mgInSensor("sns_hidden_01", "badguy.torso") and (trigger_02 == false) then
		trigger_02 = true

		if (trigger_01 == false) then
			mgSetFilter("gmt_hidden_01", 4, 254)
		else
			mgSetFilter("gmt_hidden_01", 4, 255)
		end
	end
end