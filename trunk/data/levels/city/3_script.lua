function init()
	leaves_01 = mgCreateEffect("leaves")
	leaves_02 = mgCreateEffect("leaves")
	
	leaves_fx_01 = false
	leaves_fx_02 = false
	
end


function frame()
	-- LEAVES FX
	x,y = mgGetPose("dude.torso")
	vx,vy = mgGetVelocity("dude.torso")


	if mgInSensor("leaf_sensor_01","dude.torso") or
	   mgInSensor("leaf_sensor_02","dude.torso") or
	   mgInSensor("leaf_sensor_03","dude.torso") and not leaves_fx_01 then
		for leaves_n = 0,40 do
			mgParticle(leaves_01, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*0.2 + mgRnd(-2, 6), vy*0.2 + mgRnd(-2, 6), 0);
		end
		leaves_fx_01 = true
	end
	
	if mgInSensor("leaf_sensor_04","dude.torso") and not leaves_fx_02 then
		for leaves_n = 0,40 do
			mgParticle(leaves_02, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*0.1 + mgRnd(-2, 6), vy*0.1 + mgRnd(-2, 6), 0);
		end
		leaves_fx_02 = true
	end


end