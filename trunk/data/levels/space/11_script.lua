function init()
	mgSetSensorEnabled("park_1_cameraRight", true)
	mgSetSensorEnabled("park_1_cameraLeft", false)
	
	mgSetSensorEnabled("cam_apple_Left", true)
	mgSetSensorEnabled("cam_apple_Down", false)
	
	swing_cam = true
	
	flip_park1 = false
	park1_once = false
	
	flip_park2 = false
	park2_once = false
	
end


function frame()
	--____________CAMERA PARCOUR SECTION 1
	if mgInSensor("sns_flipCamPark1", "dude.torso") then
	
		if (flip_park1 == false) and (park1_once == false) then
			mgSetSensorEnabled("park_1_cameraLeft", true)
			mgSetSensorEnabled("park_1_cameraRight", false)
		
			flip_park1 = true		
		elseif (park1_once == false) then
			mgSetSensorEnabled("park_1_cameraLeft", false)
			mgSetSensorEnabled("park_1_cameraRight", true)
		
			flip_park1 = false
		end
		
		park1_once = true
		
	else
		park1_once = false
	
	end	
	
	--____________CAMERA PARCOUR SECTION 2
	if mgInSensor("sns_flipCamPark2", "dude.torso") then
	
		if (flip_park2 == false) and (park2_once == false) then
			mgSetSensorEnabled("park_2_cameraLeft", true)
			mgSetSensorEnabled("park_2_cameraRight", false)
		
			flip_park2 = true		
		elseif (park2_once == false) then
			mgSetSensorEnabled("park_2_cameraLeft", false)
			mgSetSensorEnabled("park_2_cameraRight", true)
		
			flip_park2 = false
		end
		
		park2_once = true
		
	else
		park2_once = false
	
	end	
	
	--____________CAMERA APPLE BACKTRACK SECTION
	if mgInSensor("sns_apple_Down", "dude.torso") then
		
		mgSetSensorEnabled("cam_apple_Left", false)
		mgSetSensorEnabled("cam_apple_Down", true)
	
	end
end

