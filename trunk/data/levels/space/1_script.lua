function init()	
	trigger_01 = false
	trigger_02 = false
	
	mgSetSensorEnabled("die_timer_01", false)
	timer = 400
	tick = 0

end


function frame()
	--_________________GLASS FLOOR____________
	if mgInSensor("sns_invFloor", "dude.torso") and (trigger_01 == false) then
		trigger_01 = true
	
		if (trigger_02 == false) then
			mgSetFilter("gmt_invFloor", 4, 253)
		else
			mgSetFilter("gmt_invFloor", 4, 255)
		end
	end
	
	if mgInSensor("sns_invFloor", "badguy.torso") and (trigger_02 == false) then
		trigger_02 = true

		if (trigger_01 == false) then
			mgSetFilter("gmt_invFloor", 4, 254)
		else
			mgSetFilter("gmt_invFloor", 4, 255)
		end
	end
	
	if mgInSensor("sns_dieTimer_01", "dude.torso") then
		tick = tick + 1
		
		print (tick)
	 
		if (tick >= timer) then
		mgSetSensorEnabled("die_timer_01", true)
		
		
		end
	end
end