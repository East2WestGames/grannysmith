function init()
	scream = mgCreateSound("snd/env/titan_scream.ogg")
	screamed0 = false
	screamed1 = false
end

function frame()
	if mgIsBroken("hut0") and not screamed0 then
		mgPlaySound(scream)
		screamed0 = true
	end
	if mgIsBroken("hut1") and not screamed1 then
		mgPlaySound(scream)
		screamed1 = true
	end
end

