function init()	
	mgSetSensorEnabled("sns_force_01", false)
	mgSetSensorEnabled("sns_force_02", false)
	mgSetSensorEnabled("sns_force_03", false)
	mgSetSensorEnabled("sns_force_04", false)
	mgSetSensorEnabled("sns_force_05", false)
	mgSetSensorEnabled("sns_force_06", false)
	
	x1, y1, v1 = mgGetPose("gmt_spikey_01")
	x2, y2, v2 = mgGetPose("gmt_spikey_02")
	
	mgSetSensorEnabled("cam_mag_01", false)
	mgSetSensorEnabled("cam_mag_02", false)
	mgSetSensorEnabled("cam_mag_03", false)
	
	mgSetSensorEnabled("cam_parc_01", false)
	mgSetSensorEnabled("cam_parc_02", false)

end


function frame()
	-- Deactivate all forcefields each frame
	mgSetSensorEnabled("sns_force_01", false)
	mgSetSensorEnabled("sns_force_02", false)
	mgSetSensorEnabled("sns_force_03", false)
	mgSetSensorEnabled("sns_force_04", false)
	mgSetSensorEnabled("sns_force_05", false)
	mgSetSensorEnabled("sns_force_06", false)
	mgSetSensorEnabled("sns_force_07", false)

	-- Enable forcefields if player is on the magnetic-shape
	if mgInSensor("sns_force_activate", "dude.torso") then
		mgSetSensorEnabled("sns_force_01", true)
		mgSetSensorEnabled("sns_force_02", true)
		mgSetSensorEnabled("sns_force_03", true)
		mgSetSensorEnabled("sns_force_04", true)
		mgSetSensorEnabled("sns_force_05", true)
		mgSetSensorEnabled("sns_force_06", true)
		mgSetSensorEnabled("sns_force_07", true)		
	
	end
	
	--_______________SPIKEYS___________________
	if mgInSensor("sns_actSwingeyStone_02", "dude.torso") then
		x, y, v = mgGetVelocity("gmt_spikey_01")
		mgSetVelocity("gmt_spikey_01", 0, 0, v + 0.025)
		
		x, y, v = mgGetVelocity("gmt_spikey_02")
		mgSetVelocity("gmt_spikey_02", 0, 0, v - 0.025)
	end
	
	if mgInSensor("sns_actSwingeyStone_01", "dude.torso") then
		x, y, v = mgGetVelocity("gmt_spikey_03")
		mgSetVelocity("gmt_spikey_03", 0, 0, v + 0.03)
	end
	
	-- x, y, v = mgGetVelocity("gmt_spikey_04")
	-- mgSetVelocity("gmt_spikey_04", 0, 0, v + 0.040)
	
	-- x, y, v = mgGetVelocity("gmt_spikey_05")
	-- mgSetVelocity("gmt_spikey_05", 0, 0, v - 0.040)
	
	-- x, y, v = mgGetVelocity("gmt_spikey_06")
	-- mgSetVelocity("gmt_spikey_06", 0, 0, v - 0.040)
	
	--_______________CAMERAS - MAGNETSHAPE___________________
	if mgInSensor("sns_magCam_01", "dude.torso") then
	
		mgSetSensorEnabled("cam_mag_01", true)
		mgSetSensorEnabled("cam_mag_00", false)
	
	end	
	
	if mgInSensor("sns_magCam_02", "dude.torso") then
	
		mgSetSensorEnabled("cam_mag_02", true)
		mgSetSensorEnabled("cam_mag_01", false)
	
	end	
	
	if mgInSensor("sns_magCam_03", "dude.torso") then
	
		mgSetSensorEnabled("cam_mag_03", true)
		mgSetSensorEnabled("cam_mag_02", false)
	
	end

	if mgInSensor("sns_magCam_04", "dude.torso") then
	
		mgSetSensorEnabled("cam_mag_03", false)
	
	end	
	
	--_______________CAMERAS - PARCOUR 01_________________
	if mgInSensor("sns_parcCam_01", "dude.torso") then
		mgSetSensorEnabled("cam_parc_01", true)
		
	end
	
	if mgInSensor("sns_parcCam_02", "dude.torso") then
		mgSetSensorEnabled("cam_parc_01", false)
		mgSetSensorEnabled("cam_parc_02", true)
		
	end
	
	if mgInSensor("sns_parcCam_03", "dude.torso") then
		mgSetSensorEnabled("cam_parc_02", false)
		
	end
end