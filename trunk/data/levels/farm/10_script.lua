function init()
	trigger_01 = false
	
	hay = mgCreateEffect("hay")
	hay_fx = false
end


function frame()
	
	-- HAY FX
	
	x,y = mgGetPose("dude.torso")
	vx,vy = mgGetVelocity("dude.torso")


	if mgInSensor("hay","dude.torso") and not hay_fx then
		mgParticlePoof(hay, x, y+1, vx*0.3, 1, 1, 3, 40)
		hay_fx = true
	end
end