function init()
	leaves = mgCreateEffect("leaves")
	leaves_fx = false

end

function frame()

	-- LEAVES FX
	x,y = mgGetPose("dude.torso")
	vx,vy = mgGetVelocity("dude.torso")


	if mgInSensor("leaf_sensor","dude.torso") and not leaves_fx then
		for leaves_n = 0,40 do
			mgParticle(leaves, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*0.2 + mgRnd(-2, 8), vy*0.1 + mgRnd(-2, 8), 0);
		end
		leaves_fx = true
	end
end