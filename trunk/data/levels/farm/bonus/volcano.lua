function init()
	snow = mgCreateEffect("snow")
	snow_fx1 = false
	snow_fx2 = false
	snow_fx3 = false
	snow_fx4 = false
end

function frame()	
	if mgIsGrabbed("h1") then
		mgSetDynamic("grabber1", true)
		x, y = mgGetPose("grabber1")
		dx = -435.6 - x
		dy = -63.3 - y
		dist = math.sqrt(dx*dx + dy*dy)
		t = dist / 113.0
		if t > 1.0 then t = 1.0 end
		t = math.pow(math.sin(t*3.14159), 0.7)
		speed = t * 20 + 0.5
		mgMove("grabber1", -355, 18, 10, speed)
	else
		mgSetDynamic("grabber1", false)
	end
	if mgIsGrabbed("h1t", true) then
		mgSetDynamic("grabber1t", true)
		x, y = mgGetPose("grabber1t")
		dx = -435.6 - x
		dy = -63.3 - y
		dist = math.sqrt(dx*dx + dy*dy)
		t = dist / 113.0
		if t > 1.0 then t = 1.0 end
		t = math.pow(math.sin(t*3.14159), 0.7)
		speed = t * 20 + 0.5
		mgMove("grabber1t", -355, 18, 10, speed)
	else
		mgSetDynamic("grabber1t", false)
	end
	
	x,y = mgGetPose("dude.torso")
	vx,vy = mgGetVelocity("dude.torso")
	if mgInSensor("snow1","dude.torso") and not snow_fx1 then
		mgParticlePoof(snow, x, y-1, vx*0.3, -3, 1, 5, 50)
		snow_fx1 = true
	end
	if mgInSensor("snow2","dude.torso") and not snow_fx2 then
		mgParticlePoof(snow, x, y-1, vx*0.3, -3, 1, 4, 50)
		snow_fx2 = true
	end
	if mgInSensor("snow3","dude.torso") and not snow_fx3 then
		mgParticlePoof(snow, x, y-1, vx*0.3, -2, 1, 3, 50)
		snow_fx3 = true
	end
	if mgInSensor("snow4","dude.torso") and not snow_fx4 then
		mgParticlePoof(snow, x, y-1, vx*0.3, -2, 1, 3, 50)
		snow_fx4 = true
	end
end

