function init()
	leaves_01 = mgCreateEffect("leaves")
	leaves_02 = mgCreateEffect("leaves")
	leaves_03 = mgCreateEffect("leaves")
	leaves_04 = mgCreateEffect("leaves")
	
	leaves_fx_01 = false
	leaves_fx_02 = false
	leaves_fx_03 = false
	leaves_fx_04 = false
	
	FACTOR = 0.1
	
	
	-- EXPANTION EXPANTION EXPANTION --
	vRail_doOnce = false
	
end


function frame()
	-- LEAVES FX
	x,y = mgGetPose("dude.torso")
	vx,vy = mgGetVelocity("dude.torso")


	if mgInSensor("leaf_sensor_01","dude.torso") and not leaves_fx_01 then
		for leaves_n = 0,40 do
			mgParticle(leaves_01, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*FACTOR + mgRnd(-2, 6), vy*FACTOR + mgRnd(-2, 6), 0);
		end
		leaves_fx_01 = true
	end
	
	if mgInSensor("leaf_sensor_02","dude.torso") and not leaves_fx_02 then
		for leaves_n = 0,40 do
			mgParticle(leaves_02, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*FACTOR + mgRnd(-2, 6), vy*FACTOR + mgRnd(-2, 6), 0);
		end
		leaves_fx_02 = true
	end
	
	if mgInSensor("leaf_sensor_03","dude.torso") and not leaves_fx_03 then
		for leaves_n = 0,40 do
			mgParticle(leaves_03, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*FACTOR + mgRnd(-2, 6), vy*FACTOR + mgRnd(-2, 6), 0);
		end
		leaves_fx_03 = true
	end
	
	if mgInSensor("leaf_sensor_04","dude.torso") and not leaves_fx_04 then
		for leaves_n = 0,40 do
			mgParticle(leaves_04, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*0.2 + mgRnd(-2, 8), vy*0.2 + mgRnd(-2, 8), 0);
		end
		leaves_fx_04 = true
	end	
end

