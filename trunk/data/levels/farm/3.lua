function init()
	bottom = tonumber(mgGet("display.visibleBottom"))

	msg = mgCreateImage("hud/info_rotate.png")
	mgSetOrigo(msg, "center")
	mgSetPos(msg, 650, bottom-100)
	mgSetScale(msg, 1, 1)

	landOnFeet = mgCreateImage("hud/info_land.png")
	mgSetOrigo(landOnFeet, "center")
	mgSetPos(landOnFeet, 512, 300)	
	mgSetScale(landOnFeet, 0, 0)

	landOnFeetTimer = 0
end


function draw()
	if mgInSensor("align", "dude.torso") or mgInSensor("align2", "dude.torso") and landOnFeetTimer==0 then
		mgDraw(msg)
	end

	if landOnFeetTimer > 0 then
		landOnFeetTimer = landOnFeetTimer - 1
		if landOnFeetTimer == 0 then
			mgSetScale(landOnFeet, 0, 0, "cosine", 0.3)
		end
	end

	mgDraw(landOnFeet)
end


function handleCommand(cmd)
	if cmd=="die" then
		mgSetScale(landOnFeet, 1, 1, "bounce", 0.7)
		landOnFeetTimer = 150
	end
end
