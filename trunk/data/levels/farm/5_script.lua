function init()
	trigger_01 = false
	trigger_02 = false
	trigger_03 = false
	
	Pieces_Scarecrow = 9

	leaves = mgCreateEffect("leaves")
	leaves_fx = false
	
	hay = mgCreateEffect("hay")
	hay_fx = false
end

function rnd(low, high)
	r = math.random(0, 1000000) / 1000000
	r = 2.0*r - 1.0;
	if r > 0.0 then
		r = r*r;
	else
		r = -r*r;
	end
	return (low+high)/2.0 + r*(high-low)/2.0;
end

function frame()

	-- LEAVES FX
	x,y = mgGetPose("dude.torso")
	vx,vy = mgGetVelocity("dude.torso")


	if mgInSensor("leaf_sensor","dude.torso") and not leaves_fx then
		for leaves_n = 0,40 do
			mgParticle(leaves, x+mgRnd(-1, 1), y+mgRnd(0, 1), 0, vx*0.3 + mgRnd(-2, 8), mgRnd(-2, 8), 0);
		end
		leaves_fx = true
	end

	-- HAY FX
	
	x,y = mgGetPose("dude.torso")
	vx,vy = mgGetVelocity("dude.torso")


	if mgInSensor("hay","dude.torso") and not hay_fx then
		mgParticlePoof(hay, x, y+1, vx*0.3, 1, 1, 3, 40)
		hay_fx = true
	end
end