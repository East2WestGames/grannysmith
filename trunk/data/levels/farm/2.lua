function init()
	bottom = tonumber(mgGet("display.visibleBottom"))
	
	grab = mgCreateImage("hud/info_grab.png")
	mgSetOrigo(grab, "center")
	mgSetPos(grab, 320, bottom-100)
	mgSetScale(grab, 1, 1)

	landOnFeet = mgCreateImage("hud/info_land.png")
	mgSetOrigo(landOnFeet, "center")
	mgSetPos(landOnFeet, 512, 300)	
	mgSetScale(landOnFeet, 0, 0)

	landOnFeetTimer = 0
end


function draw()
	if mgInSensor("grab", "dude.torso") or mgInSensor("grab2", "dude.torso") then
		mgDraw(grab)
	end
	
	if landOnFeetTimer > 0 then
		landOnFeetTimer = landOnFeetTimer - 1
		if landOnFeetTimer == 0 then
			mgSetScale(landOnFeet, 0, 0, "cosine", 0.3)
		end
	end
	
	mgDraw(landOnFeet)
end


function handleCommand(cmd)
	if cmd=="die" then
		mgSetScale(landOnFeet, 1, 1, "bounce", 0.7)
		landOnFeetTimer = 150
	end
end
