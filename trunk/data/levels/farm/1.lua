function init()
	bottom = tonumber(mgGet("display.visibleBottom"))
	
	intro1 = mgCreateImage("hud/info_get.png")
	mgSetOrigo(intro1, "center")
	mgSetPos(intro1, 312, 250)
	mgSetScale(intro1, 1, 0)

	intro2 = mgCreateImage("hud/info_thief.png")
	mgSetText(intro2, "Before the thief does!")
	mgSetOrigo(intro2, "center")
	mgSetPos(intro2, 712, 350)
	mgSetScale(intro2, 1, 0)
	
	jump = mgCreateImage("hud/info_jump.png")
	mgSetOrigo(jump, "center")
	mgSetPos(jump, 700, bottom-100)	

	press = mgCreateImage("hud/info_press.png")
	mgSetOrigo(press, "center")
	mgSetPos(press, 700, bottom-100)	

	landOnFeet = mgCreateImage("hud/info_land.png")
	mgSetOrigo(landOnFeet, "center")
	mgSetPos(landOnFeet, 512, 300)	
	mgSetScale(landOnFeet, 0, 0)

	landOnFeetTimer = 0

	f = 0
end

function frame()
end

function draw()
	f = f + 1
	if f == 60 then
		mgSet("game.paused", "1")
	end
	if f == 60 then
		mgSetScale(intro1, 1, 1, "bounce", 0.8)
	end
	if f == 110 then
		mgSetScale(intro2, 1, 1, "bounce", 0.8)
	end
	
	if mgIsTouched(512, 384, 1000, 1) then
		mgSet("game.paused", "0")
		mgSetScale(intro1, 0, 0, "cosine", 0.3)
		mgSetScale(intro2, 0, 0, "cosine", 0.3)
		f = 500
	end
	
	if mgInSensor("jump", "dude.torso") then
		mgDraw(jump)
	end
	
	if  mgInSensor("press1", "dude.torso") or mgInSensor("press2", "dude.torso") or
		mgInSensor("press3", "dude.torso") or mgInSensor("press4", "dude.torso") then
		mgDraw(press)
	end
	
	if landOnFeetTimer > 0 then
		landOnFeetTimer = landOnFeetTimer - 1
		if landOnFeetTimer == 0 then
			mgSetScale(landOnFeet, 0, 0, "cosine", 0.3)
		end
	end

	mgDraw(intro1)
	mgDraw(intro2)
	mgDraw(landOnFeet)
end


function handleCommand(cmd)
	if cmd=="die" then
		mgSetScale(landOnFeet, 1, 1, "bounce", 0.7)
		landOnFeetTimer = 150
	end
end

