function init()
	trigger01_l01 = false
	trigger02_l01 = false
	
	trigger01_l02 = false
	trigger02_l02 = false

end


function frame()
	
	-- LOOP 01
	if mgInSensor("sns_loop_01", "dude.torso") and (trigger01_l01 == false) then
	trigger01_l01 = true
	
		if (trigger02_l01 == false) then
			mgSetFilter("gmt_loop_01", 4, 254)
		else
			mgSetFilter("gmt_loop_01", 4, 252)
		end
	end
	
	if mgInSensor("sns_loop_01", "badguy.torso") and (trigger02_l01 == false) then
	trigger02_l01 = true
	print("02 = true")
	
		if (trigger01_l01 == false) then
			mgSetFilter("gmt_loop_01", 4, 253)
		else
			mgSetFilter("gmt_loop_01", 4, 252)
		end
	
	end
	
	-- LOOP 02
	if mgInSensor("sns_loop_02", "dude.torso") and (trigger01_l02 == false) then
	trigger01_l02 = true
	
		if (trigger02_l02 == false) then
			mgSetFilter("gmt_loop_02", 4, 254)
		else
			mgSetFilter("gmt_loop_02", 4, 252)
		end
	end
	
	if mgInSensor("sns_loop_02", "badguy.torso") and (trigger02_l02 == false) then
	trigger02_l02 = true
	print("02 = true")
	
		if (trigger01_l02 == false) then
			mgSetFilter("gmt_loop_02", 4, 253)
		else
			mgSetFilter("gmt_loop_02", 4, 252)
		end
	
	end

end