function init()
	trigger_01 = false
	trigger_02 = false

	x1, y1, v1 = mgGetPose("gmt_conveyor")
end


function frame()
	
	if mgInSensor("sns_invfloor_01", "dude.torso") and (trigger_01 == false) then
		trigger_01 = true
	
		if (trigger_02 == false) then
			mgSetFilter("gmt_invfloor_01", 4, 253)
		else
			mgSetFilter("gmt_invfloor_01", 4, 255)
		end
	end
	
	if mgInSensor("sns_invfloor_01", "badguy.torso") and (trigger_02 == false) then
		trigger_02 = true

		if (trigger_01 == false) then
			mgSetFilter("gmt_invfloor_01", 4, 254)
		else
			mgSetFilter("gmt_invfloor_01", 4, 255)
		end
	end
	
	-- CONVEYOR
	if mgInSensor("sns_conveyor", "dude.torso") then
		mgSetDynamic("gmt_conveyor", 1)
		mgMove("gmt_conveyor", x1 - 2, y1, 100000, 2)
		
		x2, y2 = mgGetPose("gmt_conveyor")		
		if (x2 - x1 < -1.99) then
		
			mgSetPose("gmt_conveyor", x1, y1, v1)
		end		
	end
end