function init()
	trigger01 = false
	trigger02 = false
	trigger03 = false
	trigger04 = false
	
	x1, y1, v1 = mgGetPose("gmt_conveyor")
end


function frame()
	
	-- FLOOR 01
	if mgInSensor("sns_floor_01", "dude.torso") and (trigger01 == false) then
		trigger01 = true
	
		if (trigger02 == false) then
			mgSetFilter("gmt_floor_01", 4, 253)
		else
			mgSetFilter("gmt_floor_01", 4, 255)
		end
	end
	
	if mgInSensor("sns_floor_01", "badguy.torso") and (trigger02 == false) then
		trigger02 = trues
	
		if (trigger01 == false) then
			mgSetFilter("gmt_floor_01", 4, 254)
		else
			mgSetFilter("gmt_floor_01", 4, 255)
		end
	
	end
	
	-- LOOP 01
	if mgInSensor("sns_loop_01", "dude.torso") and (trigger03 == false) then
	trigger03 = true
	
		if (trigger04 == false) then
			mgSetFilter("gmt_loop_01", 4, 254)
		else
			mgSetFilter("gmt_loop_01", 4, 252)
		end
	end
	
	if mgInSensor("sns_loop_01", "badguy.torso") and (trigger04 == false) then
	trigger04 = true
	
		if (trigger03 == false) then
			mgSetFilter("gmt_loop_01", 4, 253)
		else
			mgSetFilter("gmt_loop_01", 4, 252)
		end
	
	end
	
	-- CONVEYOR
	if mgInSensor("sns_conveyor", "dude.torso") then
		mgSetDynamic("gmt_conveyor", 1)
		mgMove("gmt_conveyor", x1 - 2, y1, 100000, 2)
		
		x2, y2 = mgGetPose("gmt_conveyor")		
		if (x2 - x1 < -1.99) then
		
			mgSetPose("gmt_conveyor", x1, y1, v1)
		end		
	end
end 