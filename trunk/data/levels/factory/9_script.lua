function init()
	trigger01 = false
	trigger02 = false
	trigger03 = false
	trigger04 = false
	
	
end


function frame()
	
	-- FLOOR 01
	if mgInSensor("sns_floor_01", "dude.torso") and (trigger01 == false) then
		trigger01 = true
	
		if (trigger02 == false) then
			mgSetFilter("gmt_floor_01", 4, 253)
		else
			mgSetFilter("gmt_floor_01", 4, 255)
		end
	end
	
	if mgInSensor("sns_floor_01", "badguy.torso") and (trigger02 == false) then
		trigger02 = true
	
		if (trigger01 == false) then
			mgSetFilter("gmt_floor_01", 4, 254)
		else
			mgSetFilter("gmt_floor_01", 4, 255)
		end
	
	end
	
	
	-- FLOOR 02
	if mgInSensor("sns_floor_02", "dude.torso") and (trigger03 == false) then
		trigger03 = true
	
		if (trigger04 == false) then
			mgSetFilter("gmt_floor_02", 4, 253)
		else
			mgSetFilter("gmt_floor_02", 4, 255)
		end
	end
	
	if mgInSensor("sns_floor_02", "badguy.torso") and (trigger04 == false) then
		trigger04 = true
	
		if (trigger03 == false) then
			mgSetFilter("gmt_floor_02", 4, 254)
		else
			mgSetFilter("gmt_floor_02", 4, 255)
		end
	
	end	
end 