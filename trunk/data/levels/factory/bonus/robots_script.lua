function init()	
	trigger_01 = false

	x1 = {}
	y1 = {}
	x2 = {}
	y2 = {}
	x3 = {}
	y3 = {}
	x4 = {}
	y4 = {}
	x5 = {}
	y5 = {}
	for i = 1,3 do
		x1[i], y1[i] = mgGetPose("gmt_cogT01_0" .. i)
		x2[i], y2[i] = mgGetPose("gmt_cogT02_0" .. i)
		x3[i], y3[i] = mgGetPose("gmt_cogT03_0" .. i)
		x4[i], y4[i] = mgGetPose("gmt_cogT04_0" .. i)
		x5[i], y5[i] = mgGetPose("gmt_cogT05_0" .. i)
	end
end

function frame()
	if mgInSensor("sns_floor_01", "dude.torso") and trigger_01 == false then
		trigger_01 = true
		mgSetFilter("gmt_floor_01", 4, 253)
	end

	if mgInSensor("sns_cog01", "dude.torso") then
		for i = 1,3 do
			stringName = "gmt_cogT01_0" .. i
			mgSetVelocity(stringName, 0, 0, 2)
			mgMove(stringName, x1[i], y1[i], 100, 1)
			
			stringName = "gmt_cogT02_0" .. i
			mgSetVelocity(stringName, 0, 0, -2)
			mgMove(stringName, x2[i], y2[i], 100, 1)
			
			stringName = "gmt_cogT03_0" .. i
			mgSetVelocity(stringName, 0, 0, 2)
			mgMove(stringName, x3[i], y3[i], 100, 1)
		end
	end

	if mgInSensor("sns_cog02", "dude.torso") then
		for i = 1,3 do
			stringName = "gmt_cogT04_0" .. i
			mgSetVelocity(stringName, 0, 0, 2)
			mgMove(stringName, x4[i], y4[i], 100, 1)
			
			stringName = "gmt_cogT05_0" .. i
			mgSetVelocity(stringName, 0, 0, -2)
			mgMove(stringName, x5[i], y5[i], 100, 1)			
		end
	end	
end


