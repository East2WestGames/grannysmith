function init()
	beltX = {}
	beltY = {}
	beltSpeed = {}
	beltCount = 4
	
	obstacleCount = 8
	obstacleEnabled = {}
	for i=1, obstacleCount do
		obstacleEnabled[i] = false
	end
	
	for i=1, beltCount do
		x,y,a = mgGetPose("belt"..i)
		beltX[i] = x
		beltY[i] = y
	end
	
	beltSpeed[1] = 7
	beltSpeed[2] = 7
	beltSpeed[3] = 14
	beltSpeed[4] = 7
	beltSize = 2.0
	
	stompSnd = mgCreateSound("snd/env/stomp.ogg")
	
	movef = 0
end

function frame()
	for i=1, beltCount do
		mgMove("belt"..i, beltX[i] - 2*beltSize, beltY[i], 100000, beltSpeed[i])
		
		x, y, a = mgGetPose("belt"..i)
		if (beltX[i]-x > beltSize) then
			mgSetPose("belt"..i, beltX[i], beltY[i], 0)
		end		
	end
	
	for i=1, obstacleCount do
		if obstacleEnabled[i] == false then
			if mgInSensor("s"..i, "dude.torso") then
				mgSetDynamic("ob"..i, 1)
				obstacleEnabled[i] = true
			end
		end
	end
	
	if mgIsGrabbed("h1") then
		mgSetDynamic("grabber1", true)
		mgMove("grabber1", -106, -9, 10, 3)
	else
		mgSetDynamic("grabber1", false)
	end
	if mgIsGrabbed("h1t", true) then
		mgSetDynamic("grabber1t", true)
		mgMove("grabber1t", -106, -9, 10, 3)
	else
		mgSetDynamic("grabber1t", false)
	end
	
	if mgInSensor("smove", "dude.torso") then
		movef = movef + 1
		if (movef+0)%150 < 40 then
			mgMove("mover1", 68, -45, 1000, 8)
		else
			mgMove("mover1", 68, -42, 1000, 8)
		end
		if (movef+40)%120 < 30 then
			mgMove("mover2", 73, -45, 1000, 10)
		else
			mgMove("mover2", 73, -42, 1000, 10)
		end
		if (movef+130)%150 < 25 or (movef+30)%150 < 25 then
			mgMove("mover3", 78, -45, 1000, 10)
		else
			mgMove("mover3", 78, -42, 1000, 10)
		end
		if movef%150==25 or (movef+40)%120==20 then
			mgPlaySound(stompSnd)
			mgCameraShake(0.8)
		end
	end
	
	if mgIsGrabbed("h2") then
		mgSetDynamic("grabber2", true)
		mgMove("grabber2", 75, -31, 0.1, 3.5)
	else
		mgSetDynamic("grabber2", false)
	end
	if mgIsGrabbed("h2t", true) then
		mgSetDynamic("grabber2t", true)
		mgMove("grabber2t", 75, -31, 0.1, 3.5)
	else
		mgSetDynamic("grabber2t", false)
	end
	
	if mgIsGrabbed("h3") then
		mgSetDynamic("grabber3", true)
		mgMove("grabber3", -36, -12, 0.1, 4.5)
	else
		mgSetDynamic("grabber3", false)
	end
	if mgIsGrabbed("h3t", true) then
		mgSetDynamic("grabber3t", true)
		mgMove("grabber3t", -36, -12, 0.1, 4.5)
	else
		mgSetDynamic("grabber3t", false)
	end
	
end

