 function init()
	
	ROTSPEED = 0.04
	N_PIECES_cog = 3
	N_PIECES_ship = 10
	
	plane_start = false
	plane_liftoff = false
	plane_rampup = false
	
	velx = 0
	vely = 0
	
	plx, ply, pla = mgGetPose("gmt_plane_01")
end
                       
function frame()
	if mgInSensor("sns_cogspinney", "dude.torso") then	
	
		for piece = 1, N_PIECES_cog do
		
			stringName = "gmt_cog_0" .. piece
		
			x, y, v = mgGetPose(stringName)
			v = v + ROTSPEED
			mgSetPose(stringName, x, y, v)
			
		end	
	end 

	if mgInSensor("sns_cogtrap", "dude.torso") then
	
		for piece = 1,N_PIECES_cog do
		
			stringName = "gmt_cogT01_0" .. piece
		
			x, y, v = mgGetPose(stringName)
			v = v + ROTSPEED
			mgSetPose(stringName, x, y, v)
			
			stringName = "gmt_cogT02_0" .. piece
		
			x, y, v = mgGetPose(stringName)
			v = v - ROTSPEED
			mgSetPose(stringName, x, y, v)
			
		end
	end
		
	-------------- SPACE SHIP --------------

	d = mgGetDistanceBetweenBodies("gmt_plane_05", "dude.torso")
	if d < 0.5 and plane_liftoff == false then
		plane_liftoff = true
		plane_rampup = true
		mgSetFilter("gmt_plane_09", 8, 251)
	end
	
	if plane_rampup then
		x, y, a = mgGetPose("gmt_plane_01")
		mgRotate("gmt_plane_10", a, 100000, 0.3)
		mgRotate("gmt_plane_01", 0.5, 100000, 0.1)
	end	
		
	if plane_start then
		plx = plx + velx
		ply = ply + vely
		if velx < 0.5 then
			velx = velx + 0.001
		end
		if plane_liftoff then
			if vely < 0.3 then
				vely = vely + 0.00075
			end
			pla = vely
		end
		mgMove("gmt_plane_01", plx, ply, 10000, 100)
		mgRotate("gmt_plane_01", pla*1.3, 10000, 100)
	else
		if mgInSensor("sns_plane_start", "dude.torso") then
			plane_start = true
		end
	end
end


