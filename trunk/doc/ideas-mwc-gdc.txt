* Öka storleken på karaktärer till ca dubbla

* Lägg till förstörbara objekt

* Minska maxhastigheten, och öka arkadkänslan. Man borde komma upp snabbare i maxfart.

* Instruktionerna för kontrollerna måste göras om

* Snurrhopp triggas av användaren och snurrandet avstannar helt då man släpper upp knappen.

* Lösa problematik med att tjuven och gubben åker samma bana. Dubbla hinder eller dubbla vägar?

* Kunna rotera obekt runt y-axel

* Kunna plocka upp saker och kasta dem på hinder för att välta och förstöra
  - Var ska knappen sitta isf?
  - Blir detta för mycket i ett och samma spel?



