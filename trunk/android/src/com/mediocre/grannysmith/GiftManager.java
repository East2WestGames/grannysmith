package com.mediocre.grannysmith;

import java.util.Calendar;
import java.util.Random;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

public class GiftManager {
	private static final String KEY_GIFT_GOT_COUNT = "gift_got_count";
	private static final String KEY_GIFT_DAY = "gift_day";
	static final String TAG = "gf";
	
	public static final long DAY = 24*60*60*1000;
	public static final int GIFT_NONE = InitIap.STORE_FAILED;
	public static final int GIFT_1 = 1;
	public static final int GIFT_2 = 2;
	public static final int GIFT_3 = 3;
	public static final int GIFT_10 = 10;
	public static final int GIFT_20 = 20;
	public static final int GIFT_30 = 30;
	public static final int GIFT_40 = 40;
	private Main mMain;
	private GiftPopupWindow mPopup;
	public GiftManager(Main main){
		mMain = main;
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(System.currentTimeMillis());
	}
	
	public boolean getGift(){
		long giftday = getGiftDay();
		long now = System.currentTimeMillis();
		Random rand = new Random();
		int giftcode = rand.nextInt(GIFT_3) + 1;
		Log.i(TAG,"getGift giftcode = "+giftcode);
		Log.i(TAG,"getGift now = "+now);
		Log.i(TAG,"getGift giftday = "+giftday);
		if(giftday == 0){
			showGiftPopup(giftcode);
			return true;
		}else if(now > giftday){
			Calendar today = Calendar.getInstance();
			today.setTimeInMillis(now);
			Calendar giftDate = Calendar.getInstance();
			giftDate.setTimeInMillis(giftday);
			int ty = today.get(Calendar.YEAR);
			int tm = today.get(Calendar.MONTH);
			int td = today.get(Calendar.DATE);
			int gy = giftDate.get(Calendar.YEAR);
			int gm = giftDate.get(Calendar.MONTH);
			int gd = giftDate.get(Calendar.DATE);
			Log.i(TAG,"today year = "+ty);
			Log.i(TAG,"today month = "+tm);
			Log.i(TAG,"today day = "+td);
			Log.i(TAG,"giftday year = "+gy);
			Log.i(TAG,"giftday month = "+gm);
			Log.i(TAG,"giftday day = "+gd);
			if((ty != gy || tm != gm || td != gd) && today.after(giftDate)){
				int days = getGiftGotCount();
				Log.i(TAG,"getGift days = "+days);
				if(days == 3){
					giftcode = GIFT_10 + giftcode;
				}else if(days == 10){
					giftcode = GIFT_20 + giftcode;
				}else if(days == 20){
					giftcode = GIFT_30 + giftcode;
				}else if(days == 30){
					giftcode = GIFT_40 + giftcode;
				}else if(days > 30){
					setGiftGotCount(0);
				}
				showGiftPopup(giftcode);
				return true;
			}
		}
		return false;
	}
	
	private boolean mGiftGotClicked = false;
	private void showGiftPopup(final int giftcode){
		View content = mMain.findViewById(android.R.id.content);
		final View root = LayoutInflater.from(mMain).inflate(R.layout.day_gift, null);
		mPopup = new GiftPopupWindow(root, content);
		mPopup.show();
		
		final ImageView bg = (ImageView) root.findViewById(R.id.bg);
		root.findViewById(R.id.go).setOnClickListener(
				new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						mGiftGotClicked = true;
						mMain.onGiftGot(giftcode);
						setGiftDay(System.currentTimeMillis());
						mPopup.dismiss();
						int ys = giftcode%10;
						int cs = giftcode/10;
						Log.i(TAG,"ys = "+ys);
						Log.i(TAG,"cs = "+cs);
						String type = mMain.getResources().getResourceTypeName(R.string.app_name);
						int preId = getResIdFromName(mMain, type, "get_gift_"+ys);
						if(preId == -1)
							return;
						String msg;
						String pre = mMain.getString(preId);
						if(cs == 0)
							msg = pre + "！";
						else{
							int tid = getResIdFromName(mMain, type, "get_gift_"+cs+"0");
							msg = pre+mMain.getString(tid);
						}
						AlertDialog.Builder dialog = new AlertDialog.Builder(mMain);
						dialog.setTitle(R.string.get_gift_title);
						dialog.setMessage(msg);
						dialog.setNegativeButton(R.string.yes, null);
						dialog.show();
					}
				});
		root.post(new Runnable() {
			@Override
			public void run() {
				View v = root.findViewById(R.id.go);
				int top = bg.getTop();
				RelativeLayout.LayoutParams lp = (LayoutParams) v
						.getLayoutParams();
				int topMargin = (int) (top + bg.getHeight() * 0.4f);
				lp.topMargin = topMargin;
				lp.leftMargin = (int)(bg.getWidth()*0.23f);
				v.setLayoutParams(lp);
			}
		});
		mPopup.setOnDismissListener(new PopupWindow.OnDismissListener() {
			
			@Override
			public void onDismiss() {
				if(!mGiftGotClicked)
					mMain.onGiftGot(GIFT_NONE);
				int count = getGiftGotCount();
				Log.i(TAG, "popup on dismiss ---");
				setGiftGotCount(count + 1);
			}
		});
	}
	
	public static int getResIdFromName(Context ctx, String type, String name) {
		try {
			Resources res = ctx.getResources();
			return res.getIdentifier(name, type, ctx.getPackageName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	public void hideIfAny(){
		if(mPopup != null && mPopup.isShowing())
			mPopup.dismiss();
	}
	
	public class GiftPopupWindow extends PopupWindow {
		private View mAnchor;

		@SuppressWarnings("deprecation")
		public GiftPopupWindow(View v, View anchor) {
			super(v.getContext());
			mAnchor = anchor;
			setContentView(v);
			setBackgroundDrawable(null);
			setWidth(ViewGroup.LayoutParams.FILL_PARENT);
			setHeight(ViewGroup.LayoutParams.FILL_PARENT);
			setOutsideTouchable(true);
		}

		public void show() {
			showAtLocation(mAnchor, Gravity.CENTER, 0, 0);
		}
	}
	
	private long getGiftDay(){
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mMain);
		return sp.getLong(KEY_GIFT_DAY, 0);
	}
	private void setGiftDay(long day){
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mMain);
		sp.edit().putLong(KEY_GIFT_DAY, day).commit();
	}
	private int getGiftGotCount(){
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mMain);
		return sp.getInt(KEY_GIFT_GOT_COUNT, 0);
	}
	private void setGiftGotCount(int count){
		Log.i(TAG, "setGiftGotCount ---count = "+count);
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mMain);
		sp.edit().putInt(KEY_GIFT_GOT_COUNT, count).commit();
	}
}
