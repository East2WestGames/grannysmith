package com.mediocre.grannysmith;

public class Item {
	public static final String COP_NAME = "东品西尚网络科技（北京）有限公司";
	public static final String APP_NAME = "跑酷老奶奶";
	
	public static final String unlockAll_str = "开启后续关卡，送安全帽*5，香蕉皮*5，棒球棒*5，仅需信息费6元（不含通信费），通过短信代收，是否确认开启？";
	public static final String unlockAll_str_cn = "解锁全部关卡";
	public static final String unlockAll_id = "com.mediocre.grannysmith.unlockall3";
	public static final String unlockAll_price = "6";
	public static final String unlockAll_mmid = "30000793660618";
	
	public static final String unlockAllDie_str = unlockAll_str;
	public static final String unlockAllDie_str_cn = unlockAll_str_cn;
	public static final String unlockAllDie_id = "com.mediocre.grannysmith.die";
	public static final String unlockAllDie_price = unlockAll_price;
	public static final String unlockAllDie_mmid = "30000793660618";
	
	public static final String goShop_str = "去商店";
	public static final String goShop_str_cn = "去商店";
	public static final String goShop_id = "com.mediocre.grannysmith.shop";
	public static final String goShop_price = "0";
	
	public static final String unlockNext_str = "购买跳过本关，仅需信息费2元（不含通信费），通过短信代收，是否确认购买？";
	public static final String unlockNext_str_cn = "跳过本关";
	public static final String unlockNext_id = "com.mediocre.grannysmith.skip";
	public static final String unlockNext_price = "2";
	public static final String unlockNext_mmid = "30000793660617";
	
	public static final String unlockScruffy_str = "购买笨笨狗，仅需信息费2元（不含通信费），通过短信代收，是否确认购买？";
	public static final String unlockScruffy_str_cn = "解锁笨笨狗";
	public static final String unlockScruffy_id = "com.mediocre.grannysmith.scruffy";
	public static final String unlockScruffy_price = "2";
	public static final String unlockScruffy_mmid = "30000793660625";
	
	public static final String unlockStanley_str = "购买史爷爷，仅需信息费4元（不含通信费），通过短信代收，是否确认购买？";
	public static final String unlockStanley_str_cn = "解锁史爷爷";
	public static final String unlockStanley_id = "com.mediocre.grannysmith.stanley";
	public static final String unlockStanley_price = "4";
	public static final String unlockStanley_mmid = "30000793660626";
	
	public static final String unlockOuie_str = "购买救火员欧易，仅需信息费6元（不含通信费），通过短信代收，是否确认购买？";
	public static final String unlockOuie_str_cn = "解锁救火员欧易";
	public static final String unlockOuie_id = "com.mediocre.grannysmith.ouie";
	public static final String unlockOuie_price = "6";
	public static final String unlockOuie_mmid = "30000793660627";
	
	public static final String coin2000_str = "2元获得2000金币";
	public static final String coin2000_str_cn = "购买2000金币";
	public static final String coin2000_id = "com.mediocre.grannysmith.buycoin2000";
	public static final String coin2000_price = "2";
	public static final String coin2000_mmid = "30000793660619";
	
	public static final String coin5000_str = "4元获得5000金币";
	public static final String coin5000_str_cn = "购买5000金币";
	public static final String coin5000_id = "com.mediocre.grannysmith.buycoin5000";
	public static final String coin5000_price = "4";
	public static final String coin5000_mmid = "30000793660620";
	
	public static final String coin8000_str = "6元获得8000金币";
	public static final String coin8000_str_cn = "购买8000金币";
	public static final String coin8000_id = "com.mediocre.grannysmith.buycoin8000";
	public static final String coin8000_price = "6";
	public static final String coin8000_mmid = "30000793660621";
	
	public static final String coin15000_str = "10元获得15000金币";
	public static final String coin15000_str_cn = "购买15000金币";
	public static final String coin15000_id = "com.mediocre.grannysmith.buycoin15000";
	public static final String coin15000_price = "10";
	public static final String coin15000_mmid = "30000793660622";
	
	public static final String coin30000_str = "15元获得30000金币";
	public static final String coin30000_str_cn = "购买30000金币";
	public static final String coin30000_id = "com.mediocre.grannysmith.buycoin30000";
	public static final String coin30000_price = "15";
	public static final String coin30000_mmid = "30000793660623";
	
	public static final String coin50000_str = "20元获得50000金币";
	public static final String coin50000_str_cn = "购买50000金币";
	public static final String coin50000_id = "com.mediocre.grannysmith.buycoin50000";
	public static final String coin50000_price = "20";
	public static final String coin50000_mmid = "30000793660624";
	
	public static final String removeAds_str = "2元去除广告";
	public static final String removeAds_str_cn = "去除广告";
	public static final String removeAds_id = "com.mediocre.grannysmith.remove";
	public static final String removeAds_price = "2";
	public static final String removeAds_mmid = "30000793660600";
	
	public static final String gift_id = "com.mediocre.grannysmith.gift";
	
	public Item(String msg, String name, String pid, String price, String mmid) {
		this.name = name;
		this.price = price;
		this.pid = pid;
		this.msg = msg;
		this.mmid = mmid;
	}

	public static final Item[] items = new Item[] {
		new Item(unlockAll_str, unlockAll_str_cn, unlockAll_id, unlockAll_price, unlockAll_mmid), // 1
		new Item(unlockNext_str, unlockNext_str_cn, unlockNext_id, unlockNext_price, unlockNext_mmid), // 1
		new Item(unlockScruffy_str, unlockScruffy_str_cn, unlockScruffy_id, unlockScruffy_price, unlockScruffy_mmid), // 1
		new Item(unlockStanley_str, unlockStanley_str_cn, unlockStanley_id, unlockStanley_price, unlockStanley_mmid), // 1
		new Item(unlockOuie_str, unlockOuie_str_cn, unlockOuie_id, unlockOuie_price, unlockOuie_mmid), // 1
		new Item(coin2000_str, coin2000_str_cn, coin2000_id, coin2000_price, coin2000_mmid), // 0
		new Item(coin5000_str, coin5000_str_cn, coin5000_id, coin5000_price, coin5000_mmid), // 0
		new Item(coin8000_str, coin8000_str_cn, coin8000_id, coin8000_price, coin8000_mmid), // 0
		new Item(coin15000_str, coin15000_str_cn, coin15000_id, coin15000_price, coin15000_mmid), // 0
		new Item(coin30000_str, coin30000_str_cn, coin30000_id, coin30000_price, coin30000_mmid), // 0
		new Item(coin50000_str, coin50000_str_cn, coin50000_id, coin50000_price, coin50000_mmid), // 0
		new Item(removeAds_str, removeAds_str_cn, removeAds_id, removeAds_price, removeAds_mmid), // 0
		new Item(unlockAllDie_str, unlockAllDie_str_cn, unlockAllDie_id, unlockAllDie_price, unlockAllDie_mmid), // 0
		new Item(goShop_str, goShop_str_cn, goShop_id, goShop_price, ""), // 0
		new Item("", "", gift_id, "", ""), // 0
		null 
	};
	
	public static final String[] engName = new String[]{
		"unlock","skip","scruffy","stanley","ouie","Coins2000","Coins5000","Coins8000","Coins15000","Coins30000","Coins50000","removeAds", "unlockDie", "goshop", "gift","test"
	};

	public static int getItemIndex(String pid) {
		int i = 0;
		while (items[i] != null) {
			if (pid.compareTo(items[i].pid) == 0) {
				System.out.println("pid:" + pid + " index:" + i);
				return i;
			}
			i++;
		}
		return -1;
	}

	public String name;
	public String mmid;
	public String pid;
	public String price;
	public String msg;
}
