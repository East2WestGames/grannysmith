package com.mediocre.grannysmith;

import java.util.Vector;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import com.umeng.analytics.MobclickAgent;

public class InitIap extends Activity {
	private static final String TAG = "InitIap";

	public static int mWaitFrames;
	public static Vector<String> mRestoredProducts;
	public static int mPurchaseStatus;
	public static String mPurchaseProduct;
	public static int mPurchaseMode;
	public static MyAdManager mAdManager;
	public static boolean mShowAds = true;
	static String engName;

	static final int SHOW_AD = -1000;

	static final int STORE_WAITING = -1;
	static final int STORE_SUCCEEDED = -2;
	static final int STORE_FAILED = -3;

	static final int STORE_INIT_PURCHASE = 1;
	static final int STORE_INIT_RESTORE = 2;
	static final int STORE_IN_RESTORE = 3;
	static final int STORE_IN_PURCHASE = 4;
	static final int STORE_WAIT_FOR_RESTORED_PURCHASES = 5;

	public static InitIap activity;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		activity = this;

		mRestoredProducts = new Vector<String>();
		mPurchaseStatus = 0;
		mPurchaseProduct = "";
		mPurchaseMode = 0;

		MobclickAgent.updateOnlineConfig(this);
		mShowAds = PreferenceManager.getDefaultSharedPreferences(this)
				.getBoolean("show_ads", true);
		mAdManager = new MyAdManager(this);
		mAdManager.init();


	

		Intent intent = new Intent();
		intent.setClassName(InitIap.activity, "com.mediocre.grannysmith.Main");
		InitIap.activity.startActivity(intent);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

	}

	protected void onStop() {
		super.onStop();
		this.finish();
		Log.i(TAG, "onStop called.");
	}

	public static void onPurchaseSucceed() {
		Log.i(TAG, "onPurchaseSucceed  mPurchaseProduct = " + mPurchaseProduct);
		mPurchaseMode = 0;
		if (mPurchaseProduct.equals(Item.coin2000_id)) {
			mPurchaseStatus = 2000;
		} else if (mPurchaseProduct.equals(Item.coin5000_id)) {
			mPurchaseStatus = 5000;
		} else if (mPurchaseProduct.equals(Item.coin8000_id)) {
			mPurchaseStatus = 8000;
		} else if (mPurchaseProduct.equals(Item.coin15000_id)) {
			mPurchaseStatus = 15000;
		} else if (mPurchaseProduct.equals(Item.coin30000_id)) {
			mPurchaseStatus = 30000;
		} else if (mPurchaseProduct.equals(Item.coin50000_id)) {
			mPurchaseStatus = 50000;
		} else if (!mPurchaseProduct.equals(Item.gift_id)) {
			mPurchaseStatus = STORE_SUCCEEDED;
		}
		if (mPurchaseProduct.equals(Item.removeAds_id)) {
			mAdManager.hideAds();
			mShowAds = false;
			PreferenceManager.getDefaultSharedPreferences(activity).edit()
					.putBoolean("show_ads", false).commit();
		}
		sendUmengEvent(Item.getItemIndex(mPurchaseProduct), 2);
		MyFlurry.onLog(MyFlurry.PURCHASE_SUCCESS + engName);
	}

	public void onPurchaseFailed() {
		InitIap.mPurchaseStatus = InitIap.STORE_FAILED;
	}

	public static void sendUmengEvent(int index, int state) {
		String event = null;
		switch (index) {
		case 0:
			event = "unlock_all";
			break;
		case 1:
			event = "unlockNext";
			break;
		case 2:
			event = "unlockScruffy";
			break;
		case 3:
			event = "unlockStanley";
			break;
		case 4:
			event = "unlockOuie";
			break;
		case 5:
			event = "coin2000";
			break;
		case 6:
			event = "coin5000";
			break;
		case 7:
			event = "coin8000";
			break;
		case 8:
			event = "coin15000";
			break;
		case 9:
			event = "coin30000";
			break;
		case 10:
			event = "coin50000";
			break;
		case 11:
			event = "removeAds";
			break;
		case 12:
			event = "unlockall_die";
			break;
		case 13:
			event = "go_shop";
			break;
		}

		if (event != null)
			switch (state) {
			case 0:
				event += "_show";
				break;
			case 1:
				event += "_yes";
				break;
			case 2:
				event += "_ok";
				break;
			}

		if (event != null) {
			try {
				Log.i(TAG, "send umeng event: " + event);
				MobclickAgent.onEvent(activity, event);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	static {
		System.loadLibrary("grannysmith");
	}

}
