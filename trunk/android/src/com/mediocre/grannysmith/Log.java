package com.mediocre.grannysmith;

public class Log {
	public static final boolean LOGE = true;
	public static final boolean LOGW = true;
	public static final boolean LOGD = true;
	public static final boolean LOGV = true;
	public static final boolean LOGI = true;

	public static final boolean LOGS = true;
	public static final boolean TEST = false;

	public static void e(String tag, String msg) {
		if (LOGS && LOGE) {
			android.util.Log.e(tag, msg);
		}
	}

	public static void e(String tag, String msg, Throwable t) {
		if (LOGS && LOGE)
			android.util.Log.e(tag, msg, t);
	}

	public static void e(String tag, String msg, Exception e) {
		if (LOGS && LOGE)
			android.util.Log.e(tag, msg, e);
	}

	public static void w(String tag, String msg, Exception e) {
		if (LOGS && LOGW)
			android.util.Log.w(tag, msg, e);
	}

	public static void w(String tag, String msg, Throwable t) {
		if (LOGS && LOGW)
			android.util.Log.w(tag, msg, t);
	}

	public static void w(String tag, String msg) {
		if (LOGS && LOGW)
			android.util.Log.w(tag, msg);
	}

	public static void d(String tag, String msg) {
		if (LOGS && LOGD)
			android.util.Log.d(tag, msg);
	}

	public static void d(String tag, String msg, Throwable t) {
		if (LOGS && LOGD)
			android.util.Log.d(tag, msg, t);
	}

	public static void d(String tag, String msg, Exception e) {
		if (LOGS && LOGD)
			android.util.Log.d(tag, msg, e);
	}

	public static void v(String tag, String msg) {
		if (LOGS && LOGV)
			android.util.Log.v(tag, msg);
	}

	public static void i(String tag, String msg) {
		if (LOGS && LOGI)
			android.util.Log.i(tag, msg);
	}

	public static void i(String tag, String msg, Throwable t) {
		if (LOGS && LOGI)
			android.util.Log.i(tag, msg, t);
	}
}
