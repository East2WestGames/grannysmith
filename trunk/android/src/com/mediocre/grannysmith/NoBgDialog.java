package com.mediocre.grannysmith;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout.LayoutParams;

public class NoBgDialog extends MyDialog {

	public NoBgDialog(Context context) {
		super(context);
	}
	
	@Override
	protected void setBg() {
		View v = (View) mContent.getParent();
		LayoutParams lp = (LayoutParams) v.getLayoutParams();
		lp.leftMargin = 0;
		lp.rightMargin = 0;
		getWindow().setBackgroundDrawableResource(R.drawable.transparent_bg);
	}
}
