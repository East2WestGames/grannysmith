package com.mediocre.grannysmith;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import android.app.AlertDialog.Builder;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NativeActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;


import cn.east2west.sdk.East2westSDK;

import com.mediav.ads.sdk.adcore.Mvad;
import com.mediav.ads.sdk.interfaces.IMvAdEventListener;
import com.mediav.ads.sdk.interfaces.IMvInterstitialAd;
import com.qihoo.gamecenter.sdk.activity.ContainerActivity;
import com.qihoo.gamecenter.sdk.common.IDispatcherCallback;
import com.qihoo.gamecenter.sdk.matrix.Matrix;
import com.qihoo.gamecenter.sdk.protocols.ProtocolConfigs;
import com.qihoo.gamecenter.sdk.protocols.ProtocolKeys;
import com.umeng.analytics.MobclickAgent;
import com.umeng.analytics.game.UMGameAgent;

public class Main extends NativeActivity {
	
	public static final String TAG = "gs";
	PowerManager.WakeLock wakeLock;
	BackgroundThread mThread;
	private Context context;
	public static final String STRING_MSG_CODE = "msg_code";
	public static final String STRING_ERROR_CODE = "error_code";
	public static final String STRING_PAY_STATUS = "pay_status";
	public static final String STRING_PAY_PRICE = "pay_price";
	public static final String STRING_CHARGE_STATUS = "3rdpay_status";

	private static Main _activity;

	boolean mFirstTime;
	private GiftManager mGiftManager;
	final String adSpaceid = "55uvuxpPTO"; 
	IMvInterstitialAd interstitialAd;
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		  context=this;
		  //umeng
		  UMGameAgent.setDebugMode(true);//设置输出运行时日志
		    UMGameAgent.init( this );
		    UMGameAgent.updateOnlineConfig(context);
		    String value = UMGameAgent.getConfigParams(context, "spread" );
		    Log.e("Max", value);
		    if(value.equals("1"))
		    {
		    	East2westSDK.init(context,"1sUsNy","4a3f0e27c72846709ff596e584606e03776e683a");
		    }
		_activity = this;
		
		mThread = new BackgroundThread(this);
		mThread.execute(1);

		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wakeLock = pm
				.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "Movie");

		mFirstTime = PreferenceManager.getDefaultSharedPreferences(this)
				.getBoolean("first_startup_guoqing", true);
		if (mFirstTime)
			PreferenceManager.getDefaultSharedPreferences(this).edit()
					.putBoolean("first_startup_guoqing", false).commit();

		mGiftManager = new GiftManager(this);
		refreshPayMethod();
		
		//360初始化
		Matrix.init(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
		MyFlurry.onStart(_activity);
	}
	@Override
	protected void onDestroy() {
		super.onStart();
		Matrix.destroy(this);
	}
	@Override
	protected void onStop() {
		super.onStop();
		MyFlurry.onStop(_activity);
	}

	public void onResume() {
		super.onResume();
		 UMGameAgent.onResume(this);

		MobclickAgent.onResume(this);
		if (InitIap.mPurchaseProduct != null
				&& InitIap.mPurchaseProduct.equals("show_ads"))
			InitIap.mPurchaseStatus = InitIap.STORE_FAILED;
		East2westSDK.onResume(context);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		 UMGameAgent.onPause(this);
		East2westSDK.onPause(context);
	}

	private boolean giftSent = false;

	// Called from the game thread
	public String command(String cmd) {
	
		//System.out.println("cmd:"+cmd);
		String[] words = cmd.split(" ");

		if (words.length == 0)
			return "";
		
		//Log.i(TAG, "-----onCommand:" + cmd);
		if (words[0].equals("setalwayson")) {
			if (words.length > 1) {
				if (words[1].equals("true"))
					mThread.mWantedWakeLock = true;
				else
					mThread.mWantedWakeLock = false;
			}
		}

		else if (words[0].equals("getosname")) {
			return android.os.Build.VERSION.RELEASE;
		}

		else if (words[0].equals("getmodelname")) {
			return android.os.Build.MODEL;
		}

		else if (words[0].equals("getshowads")) {
			return InitIap.mShowAds ? "1" : "0";
		}

		else if (words[0].equals("getdensity")) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					if (mFirstTime) {
						final NoBgDialog dlg = new NoBgDialog(_activity);
						dlg.show();
						dlg.setButton(null, null, null);
						dlg.setTitle(0);
						final View root = dlg.setMyContent(R.layout.gift,
								_activity);
						final View bg = root.findViewById(R.id.bg);
						root.findViewById(R.id.go).setOnClickListener(
								new View.OnClickListener() {

									@Override
									public void onClick(View v) {
										dlg.dismiss();
										if (InitIap.mShowAds)
											InitIap.mAdManager
													.showAds("show_ads_init");
									}
								});
						root.post(new Runnable() {

							@Override
							public void run() {
								View v = root.findViewById(R.id.go);
								int top = bg.getTop();
								RelativeLayout.LayoutParams lp = (LayoutParams) v
										.getLayoutParams();
								int topMargin = (int) (top + bg.getHeight() * 0.665f);
								lp.topMargin = topMargin;
								v.setLayoutParams(lp);
							}
						});
					} else if (InitIap.mShowAds)
						InitIap.mAdManager.showAds("show_ads_init");
				}
			});
			DisplayMetrics metrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(metrics);
			return Integer.toString(metrics.densityDpi);
		}

		else if (words[0].equals("visiturl")) {
			if (words.length > 1) {
				Intent myIntent = new Intent(Intent.ACTION_VIEW,
						Uri.parse(words[1]));
				startActivity(myIntent);
			}
		}

		else if (words[0].equals("quit")) {
			mThread.mQuitFlag = true;
		}

		else if (words[0].equals("storeenabled")) {
			if (mFirstTime && !giftSent) {
				giftSent = true;
				return "truea";
			}
			return "true";
		}

		else if (words[0].equals("storepurchase") && words.length > 1) 
		{
			InitIap.mPurchaseProduct = words[1];
			Log.i(TAG, "currentPid:" + InitIap.mPurchaseProduct);
			if (InitIap.mPurchaseProduct.equals("hide_ads")) 
			{

				hideAds();
			} 
			else if (InitIap.mPurchaseProduct.equals("show_ads")
					|| InitIap.mPurchaseProduct.equals("show_pause_ads")) 
			{
				if (InitIap.mShowAds)
					runOnUiThread(new Runnable() 
					{
						@Override
						public void run() 
						{

							InitIap.mAdManager
									.showAds(InitIap.mPurchaseProduct);
						}
					});
			} 
			else if (!InitIap.mPurchaseProduct.equals("freecoins")) 
			{

				InitIap.mPurchaseStatus = InitIap.STORE_WAITING;
				InitIap.mPurchaseMode = InitIap.STORE_INIT_PURCHASE;
				_activity.runOnUiThread(new Runnable() {
					@Override
					public void run() 
					{
						if (InitIap.mPurchaseProduct
								.equals("com.mediocre.grannysmith.buycoins")) 
						{

							InitIap.mPurchaseStatus = InitIap.STORE_FAILED;
						} 
						else if (InitIap.mPurchaseProduct
								.equals("onbackbutton")) {
							mGiftManager.hideIfAny();

						} 
						else if (InitIap.mPurchaseProduct
								.startsWith("com.mediocre.grannysmith.freecoins")) 
						{
							InitIap.mPurchaseStatus = InitIap.STORE_FAILED;

						} 
						else if (InitIap.mPurchaseProduct
								.equals(Item.gift_id)) 
						{
							if (!mGiftManager.getGift())
								InitIap.mPurchaseStatus = InitIap.STORE_FAILED;

						} 
						else
						{

							int tmpIdx = -1;
							String tmpName = "";
							String tmpPrice = "";
							String tmpPriceCode = "";

								tmpIdx = Item
										.getItemIndex(InitIap.mPurchaseProduct);
								tmpName = Item.items[tmpIdx].name;
								tmpPrice = Item.items[tmpIdx].price;
								tmpPriceCode = tmpPrice + "00";
								Log.i(TAG, "tmpIdx:" + tmpIdx);
								Log.i(TAG, "tmpName:" + tmpName);
								Log.i(TAG, "tmpPrice:" + tmpPrice);
								Log.i(TAG, "tmpPriceCode:" + tmpPriceCode);

							final int index = tmpIdx;
							final String name = tmpName;
							final String priceCode = tmpPriceCode;
							InitIap.engName = Item.engName[index];
							MyFlurry.onLog(MyFlurry.CHECK_PURCHASE
									+ InitIap.engName);
							InitIap.sendUmengEvent(index, 0);
							if (InitIap.mPurchaseProduct.equals(Item.unlockAll_id)|| InitIap.mPurchaseProduct.equals(Item.unlockAllDie_id)|| InitIap.mPurchaseProduct.equals(Item.goShop_id))
							{	
								Log.e("gs","e9");
								InitIap.mPurchaseStatus = InitIap.STORE_FAILED;
							}
							else
								confirmBuy(index, name, priceCode);
						}
					}
				});
			}

			else 
			{
				// 用积分墙购买
				InitIap.mPurchaseStatus = InitIap.STORE_WAITING;
				InitIap.mPurchaseMode = InitIap.STORE_INIT_PURCHASE;

				MyFlurry.onLog(MyFlurry.CHECK_PURCHASE + InitIap.engName
						+ MyFlurry.LIMEI);
			}
			
		}

		else if (words[0].equals("storegetstatus")) {
			if (InitIap.mPurchaseMode == InitIap.STORE_WAIT_FOR_RESTORED_PURCHASES) {
				InitIap.mWaitFrames = InitIap.mWaitFrames + 1;

				if (InitIap.mWaitFrames > 120) {
					InitIap.mWaitFrames = 0;
					InitIap.onPurchaseSucceed();
				}
			}
			return "" + InitIap.mPurchaseStatus;
		}

		else if (words[0].equals("storerestore")) {
			InitIap.mRestoredProducts.clear();
			InitIap.mWaitFrames = 0;
			InitIap.mPurchaseStatus = InitIap.STORE_WAITING;
			InitIap.mPurchaseMode = InitIap.STORE_INIT_RESTORE;
		}
		else if (words[0].equals("ShowAd")) {
			ShowAd();
		}
		else if (words[0].equals("storeisrestored") && words.length > 1) {
			for (int i = 0; i < InitIap.mRestoredProducts.size(); i++) {
				String p = (String) InitIap.mRestoredProducts.elementAt(i);
				if (p != null && p.equals(words[1]))
					return "true";
			}
			return "false";
		}

		return "";
	}

	private void hideAds() {
		if (InitIap.mShowAds)
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					InitIap.mAdManager.hideAds();
				}
			});
	}

	public void onAdsDismissed() {
		InitIap.mPurchaseStatus = InitIap.STORE_SUCCEEDED;
	}




	public void ShowAd() 
	{
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				show360ad();
			}
		});
	}
	protected void show360ad()
	{
		Log.e("Jump", "360ad");
		interstitialAd = Mvad.showInterstitial(_activity, adSpaceid, false); 
		//Mvad.closeInterstitial(_activity); 
		interstitialAd.setAdEventListener (new IMvAdEventListener () {  
			@Override
			public void onAdviewIntoLandpage() {
				
			}
			
			@Override
			public void onAdviewGotAdSucceed() {
				InitIap.mPurchaseStatus = InitIap.STORE_WAITING;
			}
			
			@Override
			public void onAdviewGotAdFail() {
				
			}
			
			@Override
			public void onAdviewDismissedLandpage() {
				
			}
			
			@Override
			public void onAdviewDestroyed() {
				
			}
			
			@Override
			public void onAdviewClosed() {
				InitIap.mPurchaseStatus = InitIap.STORE_FAILED;
			}
			
			@Override
			public void onAdviewClicked() {
				InitIap.onPurchaseSucceed();
			}
		});

	}


	void showDlg(final String product, final int index, final String name,
			final String priceCode) {
		final NoBgDialog dlg = new NoBgDialog(_activity);
		dlg.setCanceledOnTouchOutside(true);
		dlg.setCancelable(true);
		dlg.show();
		dlg.setButton(null, null, null);
		dlg.setTitle(0);
		final View root = dlg.setMyContent(R.layout.unlock_all, _activity);
		final ImageView bg = (ImageView) root.findViewById(R.id.bg);
		TextView go = (TextView) root.findViewById(R.id.go);
		if (product.equals(Item.goShop_id)) {
			go.setText(Item.goShop_str_cn);
			bg.setImageResource(R.drawable.shop);
		} else {
			go.setText(R.string.go);
			bg.setImageResource(R.drawable.unlock_all);
		}
		go.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (product.equals(Item.goShop_id)) {
					InitIap.onPurchaseSucceed();
				} else
					confirmBuy(index, name, priceCode);
				dlg.dismiss();
			}
		});
		dlg.setOnCancelListener(new DialogInterface.OnCancelListener() {

			@Override
			public void onCancel(DialogInterface dialog) {
				InitIap.mPurchaseMode = 0;
				InitIap.mPurchaseStatus = InitIap.STORE_FAILED;
			}
		});
		root.post(new Runnable() {

			@Override
			public void run() {
				View v = root.findViewById(R.id.go);
				int top = bg.getTop();
				RelativeLayout.LayoutParams lp = (LayoutParams) v
						.getLayoutParams();
				int topMargin = (int) (top + bg.getHeight() * 0.6f);
				lp.topMargin = topMargin;
				v.setLayoutParams(lp);
			}
		});
	}

	private void confirmBuy(int index, String name, String priceCode) {
		Log.i(TAG, "confirmBuy ----------- ");
		refreshPayMethod();
		InitIap.sendUmengEvent(index, 1);
		if (Log.TEST) {
			InitIap.onPurchaseSucceed();
		} else {

			ShowAd();

		}
	}

	private void refreshPayMethod() {


	}



	private class BackgroundThread extends AsyncTask<Object, Integer, Long> {
		Activity mActivity;
		boolean mCurrentWakeLock;
		boolean mWantedWakeLock;
		boolean mQuitFlag;

		BackgroundThread(Activity a) {
			mActivity = a;
			mCurrentWakeLock = false;
			mWantedWakeLock = false;
			mQuitFlag = false;
		}

		protected Long doInBackground(Object... dummy) {
			while (true) {
				publishProgress(0);
				try {
					Thread.sleep(50);
				} catch (Exception e) {
				}
			}
		}

		protected void onProgressUpdate(Integer... progress) {
			if (mQuitFlag) {
				if (InitIap.mAdManager != null
						&& InitIap.mAdManager.isAdShowing())
					InitIap.mAdManager.hideAds();
				mGiftManager.hideIfAny();
				doSdkQuit(true);
//				Log.e("fff", "quit1");
//				AlertDialog.Builder dialog = new AlertDialog.Builder(mActivity);
//				dialog.setTitle(R.string.quit_dialog_title);
//				dialog.setMessage(R.string.quit_dialog_body);
//				dialog.setPositiveButton(R.string.quit_button,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								MobclickAgent.onKillProcess(mActivity);
//								int pid = android.os.Process.myPid();
//								Log.d(TAG, "kill process: " + pid);
//								android.os.Process
//										.killProcess(android.os.Process.myPid());
//								System.exit(0);
//							}
//						});
//				dialog.setNegativeButton(R.string.cancel_button, null);
//				dialog.show();
//				Log.e("fff", "quit2");
				mQuitFlag = false;
			}

			if (mCurrentWakeLock != mWantedWakeLock) {
				mCurrentWakeLock = mWantedWakeLock;
				if (mCurrentWakeLock)
					mActivity.getWindow().addFlags(
							WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
				else
					mActivity.getWindow().clearFlags(
							WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			}
		}

		protected void onPostExecute(Long result) {
		}
	}
	protected void doSdkQuit(boolean isLandScape) {

        Bundle bundle = new Bundle();

        // 界面相关参数，360SDK界面是否以横屏显示。
        bundle.putBoolean(ProtocolKeys.IS_SCREEN_ORIENTATION_LANDSCAPE, isLandScape);

        // 可选参数，登录界面的背景图片路径，必须是本地图片路径
       // bundle.putString(ProtocolKeys.UI_BACKGROUND_PICTRUE, "");

        // 必需参数，使用360SDK的退出模块。
        bundle.putInt(ProtocolKeys.FUNCTION_CODE, ProtocolConfigs.FUNC_CODE_QUIT);

        Intent intent = new Intent(this, ContainerActivity.class);
        intent.putExtras(bundle);

        Matrix.invokeActivity(this, intent, mQuitCallback);
    }
	private IDispatcherCallback mQuitCallback = new IDispatcherCallback() 
	{
	    @Override
			public void onFinished(String data)
			{
	    	  JSONObject json;
            try {
                json = new JSONObject(data);
                int which = json.optInt("which", -1);
                String label = json.optString("label");
                switch (which) {
                    case 0: // 用户关闭退出界面
                        return;
                    default:// 退出游戏
                        finish();
                        return;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
			}
	};
	public void onGiftGot(int code) {
		InitIap.mPurchaseStatus = code;
		InitIap.onPurchaseSucceed();
	}

}
