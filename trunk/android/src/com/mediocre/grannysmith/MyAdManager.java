package com.mediocre.grannysmith;

import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

public class MyAdManager{
	public static final String TAG = "ad";
	private boolean mReceived;
	
	public MyAdManager(InitIap act) {
	}

	public void init() {
	}
	
	public void destroy(){
	}
	
	public boolean isAdShowing() {
		return false;
	}
	

	public void showAds(String ads) {
		Log.i(Main.TAG, "showAds() -------------- mReceived = " + mReceived);
	}

	public void hideAds() {
	}

	public class AdPopupWindow extends PopupWindow {
		private View mAnchor;

		@SuppressWarnings("deprecation")
		public AdPopupWindow(View v, View anchor) {
			super(v.getContext());
			mAnchor = anchor;
			setContentView(v);
			setBackgroundDrawable(null);
			setWidth(ViewGroup.LayoutParams.FILL_PARENT);
			setHeight(ViewGroup.LayoutParams.FILL_PARENT);
			setOutsideTouchable(true);
		}

		public void show() {
			showAtLocation(mAnchor, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL,
					0, 0);
		}
	}
}
