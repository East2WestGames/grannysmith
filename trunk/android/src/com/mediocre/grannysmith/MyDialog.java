package com.mediocre.grannysmith;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MyDialog extends Dialog implements View.OnClickListener {
	protected TextView mLeft;
	protected TextView mRight;
	protected TextView mMiddle;
	protected TextView mContent;
	protected TextView mTitle;
	protected View.OnClickListener mLeftListener;
	protected View.OnClickListener mRightListener;
	protected View.OnClickListener mMiddleListener;

	public MyDialog(Context context) {
		super(context, R.style.MyDialogTheme);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog);
		mLeft = (TextView) findViewById(R.id.left);
		mRight = (TextView) findViewById(R.id.right);
		mMiddle = (TextView) findViewById(R.id.middle);
		mContent = (TextView) findViewById(R.id.content);
		mTitle = (TextView) findViewById(R.id.title);
		mLeft.setOnClickListener(this);
		mRight.setOnClickListener(this);
		mMiddle.setOnClickListener(this);
		setBg();
	}
	
	protected void setBg(){
		getWindow().setBackgroundDrawableResource(R.drawable.bg_buy);
	}
	
	@SuppressWarnings("deprecation")
	public View setMyContent(int layout, Context ctx) {
		View content = LayoutInflater.from(ctx).inflate(layout, null);
		ViewGroup vp = (ViewGroup) mContent.getParent();
		vp.removeAllViews();
		vp.addView(content, new LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT,
				ViewGroup.LayoutParams.FILL_PARENT));
		return content;
	}
	
	@SuppressWarnings("deprecation")
	public void setMyContent(View view){
		ViewGroup vp = (ViewGroup) mContent.getParent();
		vp.removeAllViews();
		vp.addView(view, new LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT,
				ViewGroup.LayoutParams.FILL_PARENT));
	}
	
	public void setTitle(int res){
		if(res == 0)
			mTitle.setVisibility(View.GONE);
		else{
			mTitle.setVisibility(View.VISIBLE);
			mTitle.setText(res);
		}
	}

	public void setButton(View.OnClickListener left, View.OnClickListener right) {
		setButton(left, right, null);
	}
	public void setButton(View.OnClickListener left, View.OnClickListener right, View.OnClickListener middle) {
		if(left == null){
			mLeft.setVisibility(View.GONE);
		}else{
			mLeft.setVisibility(View.VISIBLE);
			mLeftListener = left;
		}
		
		if(right == null){
			mRight.setVisibility(View.GONE);
		}else{
			mRight.setVisibility(View.VISIBLE);
			mRightListener = right;
		}
		
		if(middle == null){
			mMiddle.setVisibility(View.GONE);
		}else{
			mMiddle.setVisibility(View.VISIBLE);
			mMiddleListener = middle;
		}
	}
	
	public void setContent(String title){
		mContent.setText(title);
	}
	
	public void setContent(Spanned s){
		mContent.setText(s);
	}
	
	public void setContent(int title){
		setContent(getContext().getResources().getString(title));
	}

	@Override
	public void onClick(View v) {
		if (v == mLeft && mLeftListener != null) {
			mLeftListener.onClick(v);
		} else if (v == mRight && mRightListener != null) {
			mRightListener.onClick(v);
		} else if( v == mMiddle && mMiddleListener != null){
			mMiddleListener.onClick(v);
		}
		dismiss();
	}
}
