#include "config.h"
#include "qi_base.h"
#include "store.h"

QiString javaCommand(const QiString& cmd);

void storeInit()
{
}

void storeShutdown()
{
}

int storeEnabled()
{
	QiString re = javaCommand("storeenabled");
	int code;
	if(re == "true")
		code = 1;
	else if(re == "truea")
		code = 2;
	else
		code = 0;
	return code;
}

void storeInitPurchase(const char* product)
{
	javaCommand(QiString("storepurchase ") + product);
}

int storeGetPurchaseStatus()
{
	return javaCommand("storegetstatus").toInt();
}

void storeInitRestore()
{
	javaCommand("storerestore");
}

int storeGetRestoreStatus()
{
	return storeGetPurchaseStatus();
}

int storeIsRestored(const char* product)
{
	return (javaCommand(QiString("storeisrestored ") + product) == "true" ? 1 : 0);
}

void storeRestoreReset()
{
}

