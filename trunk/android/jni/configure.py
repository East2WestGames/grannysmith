import os
import os.path
import sys

def findFiles(dir, suffix, files):
	try:
		names = os.listdir(dir)
	except os.error, msg:
		sys.stderr.write("Can't list directory: %s\n" % dir)
		return 1
	for name in names:
		fn = dir + "/" + name
		if os.path.isdir(fn):
			if not os.path.exists(fn + "/.qiignore"):
				findFiles(fn, suffix, files)
		else:
			if os.path.basename(fn).startswith("._"):
				continue
			for s in suffix:
				if os.path.normcase(fn).endswith(s):
					files.append(fn)

os.system("rm -f qifw; ln -s ../../../qifw/src qifw")
os.system("rm -f src; ln -s ../../src src")

f = open("Android.mk.template", "r")
str = f.read()
f.close()

suffix = []
suffix.append(".cpp")
suffix.append(".c")

files = []
findFiles("src", suffix, files)

exclude = []
exclude.append("simpleapp")
exclude.append("main.cpp")
exclude.append("glew")
exclude.append("qimenu")
exclude.append("qigfxutil")
exclude.append("qiparticlesurface")

filesStr = ""
for f in files:
	exc = 0
	for e in exclude:
		if f.find(e)!=-1:
			exc = 1
	if exc==0:
		filesStr = filesStr + f + " "

str = str.replace("%SOURCE%", filesStr)
f = open("Android.mk", "w")
str = f.write(str)
f.close()
