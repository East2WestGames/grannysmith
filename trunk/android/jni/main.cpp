/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

//BEGIN_INCLUDE(all)
#include <jni.h>
#include <errno.h>

#include <EGL/egl.h>
#include <GLES2/gl2.h>

#include <android/log.h>
#include <android_native_app_glue.h>

#include <android/asset_manager.h>

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "qi_base.h"
#include "qi_file.h"
#include "qi_init.h"
#include "qi_gfx.h"
#include "qi_app.h"

#include "device.h"
#include "game.h"
#include "audio.h"
#include "display.h"
#include "store.h"

#include "nv_syscaps.h"

#if DEPLOY
#define LOGI(...) 
#define LOGW(...) 
#else
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "grannysmith: ", __VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, "grannysmith: ", __VA_ARGS__))
#endif

Game* gAndroidGame = NULL;
bool gAndroid2_3 = true;

extern void menuButtonPressed();
extern void backButtonPressed();
extern void backFromResume();

//Will be set to activity struct in main
QiString gDataPath;

int gCoreCount = 1;
bool gTegraAvailable = false;

static JNIEnv* s_jniEnv = NULL;
static jmethodID s_command = NULL;
static jobject s_nativeActivityObj = NULL;

QiString javaCommand(const QiString& cmd)
{
	if (s_command)
	{
		jobject jcmd = s_jniEnv->NewStringUTF(cmd.c_str());
		jstring s = (jstring)s_jniEnv->CallObjectMethod(s_nativeActivityObj, s_command, jcmd);
		const char* str = s_jniEnv->GetStringUTFChars(s, 0);
		QiString response(str);
    	s_jniEnv->ReleaseStringUTFChars(s, str);
		s_jniEnv->DeleteLocalRef(s);
		s_jniEnv->DeleteLocalRef(jcmd);
		QI_PRINT("command: " + cmd + " response: " + response);
		return response;
	}
	else
		return "";
}

static bool setupJni(struct android_app* state)
{
	    // Attaches the current thread to the JVM.
		JavaVM* s_vm = NULL;
		jclass s_nativeActivityCls = NULL;

	    jint lResult;
	    jint lFlags = 0;

	    ANativeActivity* act = state->activity;
	    s_vm = act->vm;
	    s_jniEnv = act->env;

	    JavaVMAttachArgs lJavaVMAttachArgs;
	    lJavaVMAttachArgs.version = JNI_VERSION_1_6;
	    lJavaVMAttachArgs.name = "NativeThread";
	    lJavaVMAttachArgs.group = NULL;

	    lResult = s_vm->AttachCurrentThread(&s_jniEnv, &lJavaVMAttachArgs);
	    if (lResult == JNI_ERR)
		{
	    	LOGI("java vm thread attach failed");
			return false;
		}

	    // Retrieves NativeActivity.
		s_nativeActivityObj = act->clazz;
		s_jniEnv->NewGlobalRef(s_nativeActivityObj);
	    s_nativeActivityCls = s_jniEnv->GetObjectClass(s_nativeActivityObj);

		if(!s_nativeActivityObj)
		{
			LOGI( "failed to bind native class");
			return false;
		}
		if(!s_nativeActivityCls)
		{
			LOGI( "failed to bind native obj");
			return false;
		}

		s_command = s_jniEnv->GetMethodID(s_nativeActivityCls, "command", "(Ljava/lang/String;)Ljava/lang/String;");
		
		if(!s_command)
		{
			LOGI( "failed to bind command method");
			return false;
		}

		return true;
}

float gWidth = 0.0f;
float gHeight = 0.0f;

class AndroidDevice : public Device
{
public:
	virtual QiString getAssetPath()
	{
		return "";
	}

	virtual QiString getUserPath()
	{
		return gDataPath;
	}

	virtual QiString getLanguage()
	{
		return QiString("en"); 
	}

	virtual int getCpuCount()
	{
		return gCoreCount;
	}
	
	virtual QiString getOsName()
	{
		return javaCommand("getosname");
	}
	
	virtual QiString getModelName()
	{
		return javaCommand("getmodelname");
	}
	
	virtual QiString getPlatformName()
	{
		return "android";
	}
	
	virtual void visitUrl(const QiString& url)
	{
		QI_PRINT("visitURL! " + url);
		javaCommand("visiturl " + url);
	}

	virtual bool isPhone()
	{
		static bool first = true;
		static bool phone = false;
		if (first)
		{
			int d = javaCommand("getdensity").toInt();
			if (d > 0 && gWidth > 0 )
			{
				LOGI("Width %f Density %i", gWidth, d);
				float w = gWidth / (float)d;
				phone = (w < 5.5f);
			}
			first = false;
		}
		return phone;
	}
	
	virtual bool showAds() {
		int d = javaCommand("getshowads").toInt();
		if(d == 1)
			return true;
		else
			return false;
	}

	virtual bool hasTegra()
	{
		return gTegraAvailable;
	}
	
	virtual bool setAlwaysOn(bool enable)
	{
		if (enable)
			javaCommand("setalwayson true");
		else
			javaCommand("setalwayson false");
	}
	
	virtual void quit()
	{
		javaCommand("quit");
	}
};

class QiAndroidDebugStream : public QiDebugStream
{
	virtual void print(const char* msg) { LOGI("%s", msg); }
} gAndroidDebugStream;


AAssetManager* gAndroidAssetManager = NULL;
AndroidDevice gAndroidDevice;
QiInput gAndroidInput;


/**
 * Shared state for our app.
 */
struct engine {
    struct android_app* app;

    int animating;
    EGLDisplay display;
    EGLSurface surface;
    EGLContext context;
};

static EGLConfig gConfigSelected;
static EGLint gPixelFormat;

static int engine_init_context(struct engine* engine) {
	LOGI("Init context");
	
    const EGLint attribs[] = {
            EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
			EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
            EGL_BLUE_SIZE, 5,
            EGL_GREEN_SIZE, 6,
            EGL_RED_SIZE, 5,
			EGL_ALPHA_SIZE, 0,
			EGL_DEPTH_SIZE, 16,
            EGL_NONE
    };

    const EGLint contextAttribs[] = 
	{
		EGL_CONTEXT_CLIENT_VERSION, 2,
    	EGL_NONE
	};
	

    EGLint dummy;
    EGLint numConfigs;
    EGLConfig config[16];
    EGLContext context;

    EGLDisplay display = engine->display;

    /* Here, the application chooses the configuration it desires. In this
     * sample, we have a very simplified selection process, where we pick
     * the first EGLConfig that matches our criteria */
    eglChooseConfig(display, attribs, config, 16, &numConfigs);

	int select = 0;
	bool found = false;
	int secondBest = -1;
	for(int i=0; i<numConfigs; i++)
	{
		EGLint buffer, r, g, b, a, d;
		eglGetConfigAttrib(display, config[i], EGL_BUFFER_SIZE, &buffer);
		eglGetConfigAttrib(display, config[i], EGL_RED_SIZE, &r);
		eglGetConfigAttrib(display, config[i], EGL_GREEN_SIZE, &g);
		eglGetConfigAttrib(display, config[i], EGL_BLUE_SIZE, &b);
		eglGetConfigAttrib(display, config[i], EGL_ALPHA_SIZE, &a);
		eglGetConfigAttrib(display, config[i], EGL_DEPTH_SIZE, &d);
		LOGI("buffer size: %i red: %i green: %i blue: %i alpha: %i depth: %i\n", buffer, r, g, b, a, d);
		if (a == 0 && d>16 && secondBest==-1)
			secondBest = i;
		if (buffer == 16 && d==16)
		{
			found = true;
			select = i;
			break;
		}
	}
	if (!found && secondBest != 0)
		select = secondBest;

	gConfigSelected = config[select];

    /* EGL_NATIVE_VISUAL_ID is an attribute of the EGLConfig that is
     * guaranteed to be accepted by ANativeWindow_setBuffersGeometry().
     * As soon as we picked a EGLConfig, we can safely reconfigure the
     * ANativeWindow buffers to match, using EGL_NATIVE_VISUAL_ID. */
    eglGetConfigAttrib(display, gConfigSelected, EGL_NATIVE_VISUAL_ID, &gPixelFormat);
	LOGI("Pixel format: %i\n", gPixelFormat);
    context = eglCreateContext(display, gConfigSelected, NULL, contextAttribs);
    engine->context = context;

	if (!context)
	{
		LOGW("NO CONTEXT");
		exit(-1);
	}

	return 0;
}

static void engine_term_display(struct engine* engine);

static int engine_init_display(struct engine* engine) {
	LOGI("Init display");

	//Close surface if already open
	if (engine->display || engine->surface)
		engine_term_display(engine);

    EGLSurface surface;
    EGLint w, h;
    EGLDisplay display = eglGetDisplay(EGL_DEFAULT_DISPLAY);

	//Init context if first time
	if (!engine->context)
	{
	    eglInitialize(display, 0, 0);
	    engine->display = display;
		engine_init_context(engine);
	}
    EGLContext context = (EGLContext)engine->context;

    ANativeWindow_setBuffersGeometry(engine->app->window, 0, 0, gPixelFormat);
    surface = eglCreateWindowSurface(display, gConfigSelected, engine->app->window, NULL);
	LOGI("Surface: %i\n", surface);
    if (eglMakeCurrent(display, surface, surface, context) == EGL_FALSE) {
        LOGW("Unable to eglMakeCurrent");
		exit(-1);
        return -1;
    }

    eglQuerySurface(display, surface, EGL_WIDTH, &w);
    eglQuerySurface(display, surface, EGL_HEIGHT, &h);
	LOGI("w: %i, h: %i", w, h);
	eglSwapInterval(display, 1);

    engine->surface = surface;
	engine->animating = 1;

	gWidth = w;
	gHeight = h;

	//Tegra check
	NvSysCaps tmpcaps;
	nvGetSystemCapabilities(&tmpcaps);
	if (strncmp("NVIDIA", tmpcaps.gl_vendor, 6)==0)
		gTegraAvailable = true;

	//Avoid setting core count to zero if buggy android
	if (tmpcaps.cpu_num_cores > 1)
		gCoreCount = tmpcaps.cpu_num_cores;

	//Enable extra effects for kal el
#ifdef ENABLE_MORE_EFFECTS
	if (gTegraAvailable && gCoreCount > 2)
		gMoreEffects = true;
#endif
	
	if (!gAndroidGame)
	{
		QI_PRINT("new");
		gAndroidGame = QI_NEW Game(&gAndroidDevice);
	}

    return 0;
}


static void engine_draw_frame(struct engine* engine) {
	if (!engine->context || !engine->display || !engine->surface)
		return;
		
	if (gGame->mAudio->mEngine.isMasterPaused())
		gGame->mAudio->mEngine.setMasterPause(false);
		
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	*gAndroidGame->mInput = gAndroidInput;
	gAndroidGame->mDisplay->setGeometry((int)gWidth, (int)gHeight);
	gAndroidGame->frame();
	
    eglSwapBuffers(engine->display, engine->surface);

/*
	static bool first=true;
	if (first)
	{
		storeInitPurchase("coins1000");
		first = false;
	}
	else
	{
		QI_PRINT("billing status: " + storeGetPurchaseStatus());
	}
	
	usleep(100000);
*/
	static QiTimer fpsTimer;
	float t = fpsTimer.getTime();
	if (t < 0.015f)
		usleep((0.015f-t)*1000000.0f);
	fpsTimer.reset();
}


static void engine_term_context(struct engine* engine)
{
	QI_DELETE(gAndroidGame);
	gAndroidGame = NULL;
	
    if (engine->context != EGL_NO_CONTEXT) {
        eglDestroyContext(engine->display, engine->context);
    }
    engine->context = EGL_NO_CONTEXT;

	eglTerminate(engine->display);
   	engine->display = EGL_NO_DISPLAY;
}


static void engine_term_display(struct engine* engine) 
{
	if (engine->surface == EGL_NO_SURFACE)
	{
		engine->animating = 0; //Should not be necessary
		return;
	}
		
    if (engine->display != EGL_NO_DISPLAY) {
		eglMakeCurrent(engine->display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        if (engine->surface != EGL_NO_SURFACE) {
            eglDestroySurface(engine->display, engine->surface);
        }
    }
    engine->surface = EGL_NO_SURFACE;
	engine->animating = 0;
}

/**
 * Process the next input event.
 */
static int32_t engine_handle_input(struct android_app* app, AInputEvent* event) {
    struct engine* engine = (struct engine*)app->userData;

	int source = AInputEvent_getSource(event);
	if (AInputEvent_getType(event) == AINPUT_EVENT_TYPE_MOTION && source == AINPUT_SOURCE_TOUCHSCREEN)
	{
		int c = AMotionEvent_getPointerCount(event);

		float x = 0;
		float y = 0;
		
		int tmp = AMotionEvent_getAction(event);
		int action = tmp & AMOTION_EVENT_ACTION_MASK;
		int ptrIndex = (tmp & AMOTION_EVENT_ACTION_POINTER_INDEX_MASK) >> AMOTION_EVENT_ACTION_POINTER_INDEX_SHIFT;
		int id = AMotionEvent_getPointerId(event, ptrIndex) + 1;
		
		for(int i=0; i<c; i++)
		{
			int mid = AMotionEvent_getPointerId(event, i) + 1;
			if (mid == id)
			{
		        x = AMotionEvent_getX(event, i);
		        y = AMotionEvent_getY(event, i);
			}			
		}

		if (action == AMOTION_EVENT_ACTION_DOWN || action == AMOTION_EVENT_ACTION_POINTER_DOWN)
			gAndroidInput.registerTouchBegin(id, x, y);
		else if (action == AMOTION_EVENT_ACTION_UP  || action == AMOTION_EVENT_ACTION_POINTER_UP)
			gAndroidInput.registerTouchEnd(id);
		else
		{
			//This API is so fucking retarded
			for(int i=0; i<c; i++)
			{
				id = AMotionEvent_getPointerId(event, i) + 1;
		        x = AMotionEvent_getX(event, i);
		        y = AMotionEvent_getY(event, i);
				gAndroidInput.registerTouchPos(id, x, y);
			}
		}
	}
	else if (AInputEvent_getType(event) == AINPUT_EVENT_TYPE_KEY)
	{
		int a = AKeyEvent_getAction(event);
		int c = AKeyEvent_getKeyCode(event);
		int dpad = 0;
		switch(c)
		{
			case AKEYCODE_DPAD_UP: dpad = QI_KEY_UP; break;
			case AKEYCODE_DPAD_DOWN: dpad = QI_KEY_LEFT; break;
			//case AKEYCODE_DPAD_LEFT: dpad = QI_KEY_LEFT; break;
			//case AKEYCODE_DPAD_RIGHT: dpad = QI_KEY_RIGHT; break;
			case AKEYCODE_DPAD_CENTER: dpad = QI_KEY_RIGHT; break;
			case AKEYCODE_BUTTON_Y: dpad = QI_KEY_DOWN; break;
			//case AKEYCODE_BUTTON_L1: dpad = QI_KEY_F1; break;
			//case AKEYCODE_BUTTON_R1: dpad = QI_KEY_F2; break;
			default: dpad = 0; break;
		}
		
		LOGI("Action: %d code %d\n", a, c);
		if (a == AKEY_EVENT_ACTION_DOWN && c == AKEYCODE_MENU)
		{
			menuButtonPressed();
			return 1;
		}
		else if (a == AKEY_EVENT_ACTION_DOWN && c == AKEYCODE_BACK)
		{
			backButtonPressed();
			return 1;
        }
		else if (a == AKEY_EVENT_ACTION_DOWN && dpad)
			gAndroidInput.registerKeyDown(dpad);
		else if (a == AKEY_EVENT_ACTION_UP && dpad)
			gAndroidInput.registerKeyUp(dpad);
	}

    return 0;
}

/**
 * Process the next main command.
 */
static void engine_handle_cmd(struct android_app* app, int32_t cmd) {
    struct engine* engine = (struct engine*)app->userData;
	LOGI("cmd: %i", cmd);
	static bool focusState = false;
	static bool resumeSequence = false;
	static bool recreate = false;
    switch (cmd) {
        case APP_CMD_INIT_WINDOW:
            // The window is being shown, get it ready.
            if (engine->app->window != NULL) 
			{
                engine_init_display(engine);
				if (gHeight > gWidth)
				{
					engine_term_display(engine);
					recreate = true;
				}
				else
					recreate = false;
            }
            break;
        case APP_CMD_TERM_WINDOW:
            // The window is being hidden or closed, clean it up.
            engine_term_display(engine);
			gAndroidGame->mAudio->mEngine.setMasterPause(true);
            break;
		case APP_CMD_CONFIG_CHANGED:
			QI_PRINT("config changed");
			if (engine->animating == 0 && recreate)
			{
				engine_init_display(engine);
				if (gHeight > gWidth)
				{
					engine_term_display(engine);
					recreate = true;
				}
				else
					recreate = false;
			}
            break;
        case APP_CMD_GAINED_FOCUS:
			focusState = true;
			gAndroidInput.clear();
			QI_PRINT("Resume");
			//2.3 Magic, don't touch
			if (engine->animating == 0 || resumeSequence)
			{
				engine_init_display(engine);
				if (gHeight > gWidth)
				{
					engine_term_display(engine);
					recreate = true;
				}
				else
					recreate = false;
			}
			if (resumeSequence)
			{
				gAndroidGame->mAudio->mEngine.setMasterPause(false);
			}
			resumeSequence = false;
        	break;
        case APP_CMD_LOST_FOCUS:
			focusState = false;
			break;
   		case APP_CMD_RESUME:
			gAndroidInput.clear();
			resumeSequence = true;
			if (focusState)
				engine_handle_cmd(app, APP_CMD_GAINED_FOCUS);
			backFromResume();
			break;
   		case APP_CMD_PAUSE:
			engine->animating = 0;
			gAndroidGame->mAudio->mEngine.setMasterPause(true);
			break;
		case APP_CMD_STOP:
			gAndroidGame->mAudio->mEngine.setMasterPause(true);
			break;
		case APP_CMD_DESTROY:
			LOGW("APP_CMD_DESTROY");
			//exit(0);
			break;
		case APP_CMD_INPUT_CHANGED:
			LOGW("APP_CMD_INPUT_CHANGED");
			QI_PRINT("Input queue: " + engine->app->inputQueue);
			QI_PRINT("Pending input queue: " + engine->app->pendingInputQueue);
			break;
    }
}


/**
 * This is the main entry point of a native application that is using
 * android_native_app_glue.  It runs in its own thread, with its own
 * event loop for receiving input events and doing other things.
 */
void android_main(struct android_app* state) {
    struct engine engine;

    // Make sure glue isn't stripped.
    app_dummy();

	LOGI("--- System startup ---");

	setupJni(state);

    memset(&engine, 0, sizeof(engine));
    state->userData = &engine;
    state->onAppCmd = engine_handle_cmd;
    state->onInputEvent = engine_handle_input;

	QiDebug::setPrintStream(&gAndroidDebugStream);
	QiDebug::setWarningStream(&gAndroidDebugStream);
	QiDebug::setErrorStream(&gAndroidDebugStream);

    gAndroidAssetManager = state->activity->assetManager;
	
    engine.app = state;

	if (state->activity->internalDataPath)
	{
		//Only works on Honeycomb
		gDataPath = state->activity->internalDataPath;
		LOGI(QiString("Native data path: ") + gDataPath);
	}
	else
	{
		//Backup for 2.3
		char fn[512];
		char tmpPath[512];
		sprintf(fn, "/proc/%i/cmdline", getpid());
		FILE* f = fopen(fn, "rb");
		int t = fread(tmpPath, 1, sizeof(tmpPath), f);
		fclose(f);
		tmpPath[t] = 0;
		gDataPath = QiString("/data/data/") + QiString(tmpPath) + QiString("/files");
		LOGI(QiString("Fallback data path: ") + gDataPath);
	}

    // loop waiting for stuff to do.
    while (1) 
	{
        // Read all pending events.
        int ident;
        int events;
        struct android_poll_source* source;

		gAndroidInput.registerBegin();
        while ((ident=ALooper_pollAll(engine.animating ? 0 : -1, NULL, &events,
                (void**)&source)) >= 0) {

            // Process this event.
            if (source != NULL) {
                source->process(state, source);
            }

            // Check if we are exiting.
            if (state->destroyRequested != 0) {
                engine_term_display(&engine);
                return;
            }
        }
		gAndroidInput.registerEnd();

        if (engine.animating)
            engine_draw_frame(&engine);
		else
			usleep(100000);
    }
}
