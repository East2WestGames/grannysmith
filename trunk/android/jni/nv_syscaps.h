/* TAGRELEASE: public */

#ifndef __INCLUDED_NV_SYSCAPS_H
#define __INCLUDED_NV_SYSCAPS_H

typedef struct _NvSysCaps
{
	char gl_vendor[256]; // 256 should be sufficient for our needs, I believe.
	char gl_version[256];
	char gl_renderer[256];

	char has_nvtime; // egl nvgetsystemtime
	char has_s3tc; // gl s3tc exists
	char has_nlz; // gl depth_nonlinear exists
	char has_csaa; // gl csaa exists

	char has_pvrtc;
	char has_atitc;

	char z_has_padding[6];

	int gpu_mem_total_mb; // total gpu mem (Tegra only)
	int gpu_mem_free_mb; // free gpu mem (Tegra only)

	int cpu_num_cores;
	int cpu_max_mhz;

	char cpu_has_neon; // some chips have neon, some don't.
	char cpu_vfp_vers; //v3 vs v4 -- cortex>A9 should probably be v4.

	char z_cpu_padding[2];

	int mem_total_mb;

	int z_cpu_more_padding[4];
} NvSysCaps;

void nvGetSystemCapabilities(NvSysCaps *syscaps);

#endif
