/* TAGRELEASE: public */

#include "nv_syscaps.h"

#include <jni.h>

#include <GLES2/gl2.h>

#include <android/log.h>
#define DEBUG_DATA(s,d) __android_log_print(ANDROID_LOG_DEBUG, "nv_syscaps", s, d)

#include <stdlib.h>
#include <unistd.h>
#include <sys/sysinfo.h>
#include <errno.h>
#include <stdio.h>

#define MAX_DATA_LEN	4096

#define MAX_SEPARATORS	6
char separators[MAX_SEPARATORS] = {' ', '\t', '\n', '\r', '\0', ':'};

bool charIsSeparator(char c)
{
	for (int i=0; i<MAX_SEPARATORS; i++)
	{
		if (c==separators[i])
			return true;
	}
	return false;
}

#define MAX_NEXT_TOKEN	128 // more than sufficient for scanning lines.
static char foundToken[MAX_NEXT_TOKEN];
const char *findDataNextToken(const char *data)
{
	const char *s = data;
	const char *outstr = NULL;
	int len = 0;

	// we presume start of the data is a sep of some sort, but skip extra seps until chars.
	while (charIsSeparator(*s))
		s++;

	outstr = s; // cache start of string.  len will give us end.
	while (!charIsSeparator(*s))
	{
		s++;
		len++;
	}

	strncpy(foundToken, outstr, len);
	foundToken[len] = 0; // null terminate the token.

	return foundToken;
}

// this is a functional strstr replacement, but for the moment IS NOT BEING USED.
//                                                             -----------------
// what we want to add to this:
// max len for walking data (so we don't have to pre-copy out smaller buffer)
// bool to force string to have a separator at the end, so it's a terminated token.
const char *findDataMatch(const char *data, const char *str)
{
	const char *c, *d, *f;

	d = data;
	c = str;
	while (*d)
	{
		// move d forward until it matches start of string.
		if (*d == *c)
		{
			f = d; // follow start of match.
			while (*c) // track until null
			{
				if (*d == *c)
				{
					d++;
					c++;
				}
				else
				{
					c = str; // reset search.
					break; // no match. break early.
				}
			}
			if (*c==0) // end of string, we finished match.
				return f; // start of match.
		}
		d++;
	}

	return NULL;
}


const char *findDataString(const char *data, const char *str, int maxlen)
{
	const char *start = data;
	const char *outstr = NULL;
	int dlen = maxlen;
	int slen = strlen(str);
	int len = 0;
	char pc, nc;

	//DEBUG_DATA("!!> findstring = %s", str);
	while (len + slen <= dlen)
	{
		// easiest is to use strstr.
		// doesn't guarantee whitespace or separators, but good enough for now
		outstr = strstr(start, str);
//		outstr = findDataFirstMatch(start, str);
		if (NULL==outstr)
			return NULL;

		// then look ahead and behind to ensure both are terminators.
		// first, behind:
		pc = *(outstr-1);
		//DEBUG_DATA("!!> findstring prevchar = 0x%02x", pc);
		if ( (outstr == data) || (charIsSeparator(pc)) )
		{
			// then, ahead
			nc = *(outstr+slen);
			//DEBUG_DATA("!!> findstring nextchar = 0x%02x", nc);
			if (charIsSeparator(nc))
			{ // fine, done.
				return outstr;
			}
		}

		// else, not bracketed, no match
		start = outstr+slen;
		len = (int)(start-data);
	}

	return outstr;
}


static char findStringTempData[MAX_DATA_LEN];
const char *findDataStringEOL(const char *data, const char *str)
{
	// quickly find EOL and use that length for search termination.
	int len = 0;
	const char *d = data;
	while (*d && *d!='\n' && *d!='\r') // EOL variants.
	{
		d++;
		len++;
	}

	// copy to temp string
	strncpy(findStringTempData, data, len);
	findStringTempData[len] = 0;

	return findDataString(findStringTempData, str, len);
}


int readDataFile(const char *fname, char *outbuf, size_t buflen)
{
	if (buflen==0)
		return -1;

	if (NULL==outbuf)
		return -1;

	if (NULL==fname)
		return -1;

    FILE *f = fopen(fname, "r");
    if (NULL == f)
    {
#ifdef _DEBUG
    	DEBUG_DATA("##> error reading data from: %s", fname);
#endif
    	return -1;
    }

	int count = fread(outbuf, 1, buflen, f);
	fclose(f);

	// sanity null-term the buffer.
	outbuf[buflen-1] = 0;

	return count;
}


void nvGetSystemCapabilities(NvSysCaps *syscaps)
{
	char databuf[MAX_DATA_LEN]; // something more than reasonable.
	int dataLen;
	const GLubyte *tmps;
    const char *exts;
    char *offs;

	if (syscaps==NULL)
		return; // sanity.

	// first let's do some checking of the system and detection of features.
	// this can be moved into a library at some point for ease of use.

	// clear struct.
	memset(syscaps, 0, sizeof(NvSysCaps));

	// cache the extensions string.
    exts = (const char*) glGetString(GL_EXTENSIONS);
#if 1 //def debug_only
    // for debugging...  print out the list of extensions supported.
    if (1)
    { // but build a copy and insert linefeeds...
    	char *d = databuf;
    	strcpy(databuf, exts); // so we can modify...
    	while (*d)
    	{
    		if (*d==' ') *d = '\n';
    		d++;
    	}
    	DEBUG_DATA("!!> GL_EXTENSIONS:\n%s", databuf);
    }
#endif

	// check for interesting GL strings..
	tmps = glGetString(GL_VENDOR);
	DEBUG_DATA("##> GL_VENDOR: %s", tmps);
    strncpy(syscaps->gl_vendor, (char*)tmps, 255);
    syscaps->gl_vendor[255]=0;

    tmps = glGetString(GL_RENDERER);
    DEBUG_DATA("##> GL_RENDERER: %s", tmps);
    strncpy(syscaps->gl_renderer, (char*)tmps, 255);
    syscaps->gl_renderer[255]=0;

    tmps = glGetString(GL_VERSION);
    DEBUG_DATA("##> GL_VERSION: %s", tmps);
    strncpy(syscaps->gl_version, (char*)tmps, 255);
    syscaps->gl_version[255]=0;

   	syscaps->has_nvtime = false;

	// look for extension matches.
    offs = strstr(exts, "GL_EXT_texture_compression_s3tc");
    if (NULL != offs)
    	syscaps->has_s3tc = true;
    DEBUG_DATA("##> Has S3TC: %s", syscaps->has_s3tc ? "true" : "false");

    offs = strstr(exts, "GL_NV_depth_nonlinear");
    if (NULL != offs)
    	syscaps->has_nlz = true;
    DEBUG_DATA("##> Has NLZ: %s", syscaps->has_nlz ? "true" : "false");

    offs = strstr(exts, "GL_NV_coverage_sample");
    if (NULL != offs)
    	syscaps->has_csaa = true;
    DEBUG_DATA("##> Has CSAA: %s", syscaps->has_csaa ? "true" : "false");

    offs = strstr(exts, "GL_IMG_texture_compression_pvrtc");
    if (NULL != offs)
    	syscaps->has_pvrtc = true;
    DEBUG_DATA("##> Has PVRTC: %s", syscaps->has_pvrtc ? "true" : "false");

    offs = strstr(exts, "GL_AMD_compressed_ATC_texture"); // this seems the proper one
    //offs = strstr(exts, "GL_ATI_texture_compression_atitc"); // this is older form?
    if (NULL != offs)
    	syscaps->has_atitc = true;
    DEBUG_DATA("##> Has ATITC: %s", syscaps->has_atitc ? "true" : "false");

    // find number of cpu cores.
    int cpuCount = 0;
    // approaches:
    // 1) sysconf isn't working for K32, K36 it seems to, so for HC+ app might be ok.
    //    cpuCount = sysconf(_SC_NPROCESSORS_CONF);
    // 2) can't use num_possible_cpus() or other cpumask.h function under android ndk,
    //    as headers won't compile.
    // 3) leaves us with reading sysfs entry for possible or present cpu string.
    dataLen = readDataFile("/sys/devices/system/cpu/present", databuf, MAX_DATA_LEN);
	if (dataLen>1 && dataLen<16) // some range that sounds decent to sanity clamp.
	{
		// in case of a LF or other termination, use our token function.
		const char *findstr = findDataNextToken(databuf);
		int fcount = strlen(findstr);
		// process the string.
		// format is '0' for one core, '0-1' for two, '0-3' for four.
		if (fcount==1) // if single character, there's only one core.
			cpuCount = 1;
		else
		{
			findstr += 2; // that jumps over "0-".
			cpuCount = 1 + atoi(findstr); // since indices are zero-based.
			if (cpuCount<1)
				cpuCount = 1;
		}
	}
    if (cpuCount>0)
    {
    	syscaps->cpu_num_cores = cpuCount;
    	DEBUG_DATA("##> Present CPUs: %d", syscaps->cpu_num_cores);
    }

	// look up cpu frequency info, max hz.
    dataLen = readDataFile("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq", databuf, MAX_DATA_LEN);
	if (dataLen>3 && dataLen<16) // some range that sounds decent to sanity clamp.
	{
		int mhz = atoi(databuf) / 1000;
		if (mhz<1)
			mhz = 0; // min clamp
		syscaps->cpu_max_mhz = mhz;
		DEBUG_DATA("##> CPU0 max freq: %dMHz", syscaps->cpu_max_mhz);
	}

	// look up further cpu info, arch level, neon, etc.
	dataLen = readDataFile("/proc/cpuinfo", databuf, MAX_DATA_LEN);
	//DEBUG_DATA("##> cpuinfo len %d", count);
	if (dataLen>10) // should be 100s of chars, just some min clamp.
	{
		// find cpu arch -- ARMv7, ARMv8, what we care is > v7... A8/9 vs A15 for example.
		// !!TODO

		// find Features strings
		const char *features = findDataString(databuf, "Features", dataLen);
		const char *findstr;
		if (NULL!=features)
		{
			findstr = findDataStringEOL(features, "neon");
			syscaps->cpu_has_neon = (NULL!=findstr);
			DEBUG_DATA("##> CPU %s NEON", syscaps->cpu_has_neon ? "has" : "doesn't have");

			findstr = findDataStringEOL(features, "vfpv4");
			if (NULL!=findstr)
				syscaps->cpu_vfp_vers = 4;
			else
			{
				findstr = findDataStringEOL(features, "vfpv3");
				if (NULL!=findstr)
					syscaps->cpu_vfp_vers = 3;
			}
			DEBUG_DATA("##> CPU is VFPv%d", syscaps->cpu_vfp_vers);
		}
		else
		{
			features = strstr(databuf, "Features");
			if (NULL!=features)
			{
				DEBUG_DATA("!!> CPU features string found with strstr only, len %d", strlen(features));
			}
		}
	}

	// look up some memory info.
	dataLen = readDataFile("/proc/meminfo", databuf, MAX_DATA_LEN);
	if (dataLen>10) // should be 100s of chars, just some min clamp.
	{
		// find total memory for now
		const char *mts = "MemTotal:";
		const char *findstr = findDataString(databuf, mts, dataLen);
		if (NULL!=findstr)
		{
			findstr += strlen(mts);
			const char *tok = findDataNextToken(findstr);
			if (NULL!=tok)
			{
				syscaps->mem_total_mb = atoi(tok) / 1024; // kB->MB.
				DEBUG_DATA("##> Total reported memory is %dMB", syscaps->mem_total_mb);
			}
		}
	}

	// NEW FOR 1.1:
	// look up gpu info, specifically total_size and free_size.
	// try new path FIRST, then old path, since new will outpace old.
    dataLen = readDataFile("/sys/devices/platform/tegra-nvmap/misc/nvmap/heap-generic-0/total_size", databuf, MAX_DATA_LEN);
    if (dataLen<0)
        dataLen = readDataFile("/sys/devices/virtual/misc/nvmap/heap-generic-0/total_size", databuf, MAX_DATA_LEN);
	if (dataLen>3 && dataLen<16) // some range that sounds decent to sanity clamp.
	{
		int size = atoi(databuf) / (1024*1024); // B->MB
		if (size<1)
			size = 0; // min clamp
		syscaps->gpu_mem_total_mb = size;
		DEBUG_DATA("##> GPU mem total: %dMB", syscaps->gpu_mem_total_mb);
	}
    dataLen = readDataFile("/sys/devices/platform/tegra-nvmap/misc/nvmap/heap-generic-0/free_size", databuf, MAX_DATA_LEN);
    if (dataLen<0)
        dataLen = readDataFile("/sys/devices/virtual/misc/nvmap/heap-generic-0/free_size", databuf, MAX_DATA_LEN);
	if (dataLen>3 && dataLen<16) // some range that sounds decent to sanity clamp.
	{
		int size = atoi(databuf) / (1024*1024); // B->MB
		if (size<1)
			size = 0; // min clamp
		syscaps->gpu_mem_free_mb = size;
		DEBUG_DATA("##> GPU mem free: %dMB", syscaps->gpu_mem_free_mb);
	}
}
