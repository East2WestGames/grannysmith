import os
import os.path
import sys
import re

BASEDIR = "../src"

def findDirs(dir, dirs):
    try:
        names = os.listdir(dir)
    except os.error, msg:
        sys.stderr.write("Can't list directory: %s\n" % dir)
        return 1
    for name in names:
        fn = dir + "/" + name
        if os.path.isdir(fn):
            dirs.append(name)

def findFiles(dir, suffix, files):
    try:
        names = os.listdir(dir)
    except os.error, msg:
        sys.stderr.write("Can't list directory: %s\n" % dir)
        return 1
    for name in names:
        fn = dir + "/" + name
        if os.path.normcase(fn).endswith(suffix):
            files.append(name)
            
dirs = []
findDirs(BASEDIR, dirs)

for dir in dirs:
	if dir == ".svn":
		continue
	str = "#ifndef QI_HEADERS_" + dir.upper() + "_H\n"
	str = str + "#define QI_HEADERS_" + dir.upper() + "_H\n\n"
	headers = []
	findFiles(BASEDIR + "/" + dir, ".h", headers)
	for header in headers:
		str = str + "#include \"" + dir + "/" + header + "\"\n"
	str = str + "\n#endif\n"
	f=open(BASEDIR + "/qi_" + dir + ".h", "w")
	f.write(str)
	f.close()	
	
