import os
import os.path
import sys

#find files recursively
#dir - input dir
#dir - valid file ending
#files - output list of files
def findFiles(dir, suffix, files):
	try:
		names = os.listdir(dir)
	except os.error, msg:
		sys.stderr.write("Can't list directory: %s\n" % dir)
		return 1
	for name in names:
		fn = dir + "/" + name
		if os.path.isdir(fn):
			if not os.path.exists(fn + "/.qiignore"):
				findFiles(fn, suffix, files)
		else:
			if os.path.basename(fn).startswith("._"):
				continue
			for s in suffix:
				if os.path.normcase(fn).endswith(s):
					files.append(fn)

def getObj(objdir, fileName):
    return objdir + "\\" + moduleName(fileName) + ".obj"            

def findHeadersHelper(incDirs, file, foundHeaders, skip):
	try:
		f=open(file, "r")
		for line in f:
			line = line.strip()
			if (line.startswith("#include")):
				line = line.replace("#include", "")
				line = line.replace("\"", "")
				line = line.replace("<", "")
				line = line.replace(">", "")
				line = line.replace(";", "")
				line = line.strip();
				line = line.split("//")[0]
				line = line.split("/*")[0]
				#only search for each headers once (opt) once
				if line in skip:
					continue
				skip.append(line)
				#search for this header in include dirs
				path = ""
				for incDir in incDirs:
					if os.path.exists(incDir + "/" + line):
						path = incDir + "/" + line
						break
				if path == "":
					#print "Could not parse header: " + line + " (" + file + ")"
					continue
				if path not in foundHeaders:
					foundHeaders.append(path)
					findHeadersHelper(incDirs, path, foundHeaders, skip)
	except:
		if f is None:
			f.close();

#Scan headers file dependencies for a certain source file
#incDirs - include dirs
#srcFile - input source file (start point for search)
#foundHeaders - list of dependent header files
def findHeaders(incDirs, srcFile, foundHeaders):
	tmp = []
	incDirs.append(os.path.dirname(srcFile))
	findHeadersHelper(incDirs, srcFile, foundHeaders, tmp)

def getObjFile(sourceFile):
	baseName = os.path.basename(sourceFile)
	baseName = baseName.replace(".cpp",".o")
	baseName = baseName.replace(".c",".o")
	return baseName;

def buildMakefile(name, cfg, src, inc, pre, flags, params={}, libDirs=[]):
	outputfile = "makefile.gen"
	
	#Handle endianess
	p = os.popen("uname -p")
	output = p.read();
	if output.find("powerpc") >= 0:
		arch = "-DQI_ARCH_PPC"
	else:
		arch = "-DQI_ARCH_INTEL"
	p.close()

	libs = ""

	p = os.popen("uname -s")
	output = p.read();
	if output.find("Darwin") >= 0:
		osname = "-DQI_MACOS"
	else:
		osname = "-DQI_LINUX"
		libs = libs + "-lpthread "
	p.close()

	opt = " -m32 "
	for p in flags:
		opt = opt + p + " "

	for p in pre:
		opt = opt + "-D" + p + " "

	include = ""
	for i in inc:
		include = include + "-I" + i + " "
	for s in src:
		include = include + "-I" + s + " "
	
	if not "outputDir" in params:
		params["outputDir"] = "."

	if not "ldflags" in params:
		params["ldflags"] = ""

	if not "ccflags" in params:
		params["ccflags"] = ""

	if not "cxxflags" in params:
		params["cxxflags"] = ""

	exe = params["outputDir"] + "/" + name

	objDirDebug = "objd"
	objDirRelease = "obj"

	makeMacApp = False
			
	for l in libDirs:
		libs = libs + "-L" + l + " "
			
	if "al" in cfg:
		libs = libs + "-framework OpenAL "
		opt = opt + "-DQI_USE_OPENAL "
	if "gl" in cfg:
		libs = libs + "-framework OpenGL -framework Carbon -framework AGL "
		opt = opt + "-DQI_USE_OPENGL "
	if "glut" in cfg:
		libs = libs + "-framework GLUT "
		opt = opt + "-DQI_USE_GLUT "
	if "cg" in cfg:
		libs = libs + "-framework Cg "
	if "sdl" in cfg:
		opt = opt + "-DQI_USE_SDL "
		libs = libs + "-framework SDL -framework Cocoa src/SDLMain.m "
	if "wx" in cfg:
		opt = opt + "-DQI_USE_OPENGL `wx-config --cxxflags` "
		libs = libs + "`wx-config --libs gl	` -framework OpenGL "
		if osname == "-DQI_MACOS":
			makeMacApp = True

	if makeMacApp:
		exe = params["outputDir"] + "/" + name + ".app/Contents/MacOS/" + name
		if not os.path.exists(params["outputDir"] + "/" + name + ".app"):
			os.mkdir(name + ".app")
		if not os.path.exists(params["outputDir"] + "/" + name + ".app/Contents"):
			os.mkdir(name + ".app" + "/Contents")
		if not os.path.exists(params["outputDir"] + "/" + name + ".app/Contents/MacOS"):
			os.mkdir(name + ".app" + "/Contents/MacOS")
		if not os.path.exists(params["outputDir"] + "/" + name):
			f=open(name, "w")
			f.write("cd " + name + ".app/Contents/MacOS; ./" + name)
			f.close()
			os.system("chmod 755 " + name)
			
	if "release" in cfg:
		objDir = objDirRelease
		opt = opt + "-O3 "
	else:
		objDir = objDirDebug
		opt = opt + "-g -DDEBUG "

		
	
	gcc = "gcc " + opt + " " + arch + " " + osname + " " + include + " -c " + params["ccflags"] + " "
	gpp = "g++ -Wall " + opt + " " + arch + " " + osname + " " + include + " -c " + params["cxxflags"] + " "
	ld = "g++ -Wall " + opt + " " + arch + " " + osname + " -o " + exe + " " + libs + " " + params["ldflags"]

	sourceFiles = []
	for srcDir in src:
		findFiles(srcDir, [".c", ".cpp"], sourceFiles)

	allInc = src + inc

	headerFiles = []
	for srcDir in allInc:
		findFiles(srcDir, [".h"], headerFiles)

	str = "default: " + exe + "\n\n"

	str = str + "clean:\n"
	str = str + "\trm -Rf "
	str = str + "obj/* objd/* "
	str = str + exe + " "
	str = str + exe + ".app "
	for sourceFile in sourceFiles:
		objFile = objDirDebug + "/" + getObjFile(sourceFile)
		str = str + objFile + " "
		objFile = objDirRelease + "/" + getObjFile(sourceFile)
		str = str + objFile + " "
	str = str + "\n\n"

	for sourceFile in sourceFiles:
		objFile = objDir + "/" + getObjFile(sourceFile)
		str = str + objFile + ": " + sourceFile + " "
		usedHeaders = []
		findHeaders(allInc, sourceFile, usedHeaders)
		for headerFile in usedHeaders:
			str = str + headerFile + " "
		str = str + "\n"
		if (sourceFile.endswith(".c")):
			str = str + "\t" + gcc + " " + sourceFile + " -o " + objFile + "\n"
		else:
			str = str + "\t" + gpp + " " + sourceFile + " -o " + objFile + "\n"
		str = str + "\n"

	str = str + exe + ": "
	for sourceFile in sourceFiles:
		objFile = objDir + "/" + getObjFile(sourceFile)
		str = str + objFile + " "
	str = str + "\n"
	str = str + "\t" + ld + " "
	for sourceFile in sourceFiles:
		objFile = objDir + "/" + getObjFile(sourceFile)
		str = str + objFile + " "
	str = str + "\n"
		
	f=open(outputfile, "w")
	f.write(str)
	f.close()	
	
	if not os.path.exists("obj"):
		os.mkdir("obj")
	if not os.path.exists("objd"):
		os.mkdir("objd")


def buildVc8(template, name, cfg, src, inc, pre, params={}, libDirs=[]):
	f = open(template, "r")
	str = f.read()
	f.close()

	subSys = "1"

	incStr = ""
	allInc = src + inc
	for incDir in allInc:
		incStr = incStr + incDir + ";"

	libDebugStr = "wsock32.lib ws2_32.lib "
	libReleaseStr = "wsock32.lib ws2_32.lib "
	
	runtimeDebugStr = "1"
	runtimeReleaseStr = "0"
	
	outputType = "1"

	if not "outputDir" in params:
		params["outputDir"] = "."

	libDir = ""
	for l in libDirs:
		libDir = libDir + l + ";"

	if "sdl" in cfg:
		pre.append("QI_USE_SDL")
		libDebugStr = libDebugStr + "sdl.lib sdlmain.lib "
		libReleaseStr = libReleaseStr + "sdl.lib sdlmain.lib "
		runtimeDebugStr = "3"
		runtimeReleaseStr = "2"

	if "gl" in cfg:
		pre.append("QI_USE_OPENGL")
		libDebugStr = libDebugStr + "opengl32.lib glu32.lib "
		libReleaseStr = libReleaseStr + "opengl32.lib glu32.lib "

	if "glut" in cfg:
		pre.append("QI_USE_GLUT")
		libDebugStr = libDebugStr + "glut32.lib "
		libReleaseStr = libReleaseStr + "glut32.lib "

	if "dynamiclib" in cfg:
		outputType = "2"
	if "staticlib" in cfg:
		outputType = "4"
			
	preDebugStr = "QI_ARCH_INTEL;QI_DEBUG;"
	preReleaseStr = "QI_ARCH_INTEL;"
	for p in pre:
		preDebugStr = preDebugStr + p + ";"
		preReleaseStr = preReleaseStr + p + ";"

	if "wx" in cfg:
		libDebugStr = libDebugStr + "wxmsw28d_gl.lib wxmsw28d_adv.lib wxmsw28d_core.lib "
		libDebugStr = libDebugStr + "wxbase28d.lib wxtiffd.lib wxjpegd.lib wxpngd.lib "
		libDebugStr = libDebugStr + "wxzlibd.lib wxregexd.lib wxexpatd.lib "
		libDebugStr = libDebugStr + "winmm.lib comctl32.lib rpcrt4.lib odbc32.lib Rpcrt4.lib "
		libReleaseStr = libReleaseStr + "wxmsw28_gl.lib wxmsw28_adv.lib wxmsw28_core.lib "
		libReleaseStr = libReleaseStr + "wxbase28.lib wxtiff.lib wxjpeg.lib wxpng.lib "
		libReleaseStr = libReleaseStr + "wxzlib.lib wxregex.lib wxexpat.lib "
		libReleaseStr = libReleaseStr + "winmm.lib comctl32.lib rpcrt4.lib odbc32.lib Rpcrt4.lib "
		preDebugStr = preDebugStr + "__WXMSW__;__WXDEBUG__;_WINDOWS;NOPCH;"
		preReleaseStr = preReleaseStr + "__WXMSW__;_WINDOWS;NOPCH;"
		subSys = "2"

	str = str.replace("%NAME%", name)
	str = str.replace("%INC%", incStr)
	str = str.replace("%SUBSYS%", subSys)
	str = str.replace("%PRE-DEBUG%", preDebugStr)
	str = str.replace("%PRE-RELEASE%", preReleaseStr)
	str = str.replace("%LIB-DEBUG%", libDebugStr)
	str = str.replace("%LIB-RELEASE%", libReleaseStr)
	str = str.replace("%RUNTIME-DEBUG%", runtimeDebugStr)
	str = str.replace("%RUNTIME-RELEASE%", runtimeReleaseStr)
	str = str.replace("%TYPE%", outputType)
	str = str.replace("%OUTPUTDIR%", params["outputDir"]);
	str = str.replace("%LIBDIR%", libDir);
	filesStr = ""

	for srcDir in src:
		filesStr = filesStr + "<Filter Name=\"" + srcDir.replace("../", "").replace("/", "_") + "\" Filter=\"\">\n"
		sourceFiles = []
		findFiles(srcDir, [".c", ".cpp", ".h", ".rc"], sourceFiles)
		currentFilter = ""
		for sourceFile in sourceFiles:
			filt = os.path.dirname(sourceFile)
			if filt.find(srcDir) != -1:
				filt = filt.split(srcDir, 1)[1]
			#remove intial slash
			if filt.find("/") == 0:
				filt = filt.split("/", 1)[1]
			splitfilt = filt.split("/")
			filt = ""
			for f in splitfilt:
				filt = filt + f + " "
			filt = filt.strip().replace(" ", "_")
			#switch filter if needed
			if filt != currentFilter:
				if currentFilter != "":
					filesStr = filesStr + "</Filter>\n"
				if filt != "":
					filesStr = filesStr + "<Filter Name=\"" + filt + "\">\n"
				currentFilter = filt
			
			filesStr = filesStr + "<File RelativePath=\"" + sourceFile.replace("/", "\\") + "\">"
			filesStr = filesStr + "</File>\n"

		if currentFilter != "":
			filesStr = filesStr + "</Filter>\n"
			
		filesStr = filesStr + "</Filter>\n"
	
	str = str.replace("%FILES%", filesStr)
	
	f = open(name + ".vcproj", "w")
	str = f.write(str)
	f.close()
		
def getFilter(src):
	filt = os.path.dirname(src)
	filt = filt.replace("../", "")
	filt = filt.replace("/", "\\")
	return filt

def addFilter(src, filters):
	filt = getFilter(src)
	if (filt == ""):
		return
	if filt.find("\\") > 0:
		addFilter(filt.split("\\", 0)[0], filters)
	if not filt in filters:
		filters.append(filt)
	
def buildVc2010(template, name, cfg, src, inc, pre, params={}, libDirs=[]):
	f = open(template, "r")
	str = f.read()
	f.close()

	subSys = "1"

	incStr = ""
	allInc = src + inc
	for incDir in allInc:
		incStr = incStr + incDir + ";"

	libDebugStr = "wsock32.lib;ws2_32.lib;"
	libReleaseStr = "wsock32.lib;ws2_32.lib;"
	
	runtimeDebugStr = "1"
	runtimeReleaseStr = "0"
	
	outputType = "1"

	if not "outputDir" in params:
		params["outputDir"] = "."

	libDir = ""
	for l in libDirs:
		libDir = libDir + l + ";"

	if "gl" in cfg:
		pre.append("QI_USE_OPENGL")
		libDebugStr = libDebugStr + "opengl32.lib;glu32.lib;"
		libReleaseStr = libReleaseStr + "opengl32.lib;glu32.lib;"

	if "al" in cfg:
		pre.append("QI_USE_OPENAL")

	if "glut" in cfg:
		pre.append("QI_USE_GLUT")
		libDebugStr = libDebugStr + "glut32.lib;"
		libReleaseStr = libReleaseStr + "glut32.lib;"

	if "dynamiclib" in cfg:
		outputType = "2"
	if "staticlib" in cfg:
		outputType = "4"
			
	preDebugStr = "QI_ARCH_INTEL;QI_DEBUG;"
	preReleaseStr = "QI_ARCH_INTEL;"
	for p in pre:
		preDebugStr = preDebugStr + p + ";"
		preReleaseStr = preReleaseStr + p + ";"

	str = str.replace("%NAME%", name)
	str = str.replace("%INC%", incStr)
	str = str.replace("%SUBSYS%", subSys)
	str = str.replace("%PRE-DEBUG%", preDebugStr)
	str = str.replace("%PRE-RELEASE%", preReleaseStr)
	str = str.replace("%LIB-DEBUG%", libDebugStr)
	str = str.replace("%LIB-RELEASE%", libReleaseStr)
	str = str.replace("%RUNTIME-DEBUG%", runtimeDebugStr)
	str = str.replace("%RUNTIME-RELEASE%", runtimeReleaseStr)
	str = str.replace("%TYPE%", outputType)
	str = str.replace("%OUTPUTDIR%", params["outputDir"]);
	str = str.replace("%LIBDIR%", libDir);
	filesStr = ""

	fileIncStr = ""
	fileSrcStr = ""
	filters = []
	for srcDir in src:
		sourceFiles = []
		findFiles(srcDir, [".c", ".cpp", ".h", ".rc"], sourceFiles)
		for sourceFile in sourceFiles:
			addFilter(sourceFile, filters)
			if sourceFile.endswith(".h") or sourceFile.endswith(".hpp"):
				fileIncStr = fileIncStr + "<ClInclude Include=\"" + sourceFile + "\"><Filter>" + getFilter(sourceFile) + "</Filter></ClInclude>\n"
			else:
				fileSrcStr = fileSrcStr + "<ClCompile Include=\"" + sourceFile + "\"><Filter>" + getFilter(sourceFile) + "</Filter></ClCompile>\n"

	filesStr = filesStr + "<ItemGroup>\n" + fileSrcStr + "</ItemGroup>\n"
	filesStr = filesStr + "<ItemGroup>\n" + fileIncStr + "</ItemGroup>\n"
	
	str = str.replace("%FILES%", filesStr)
	f = open(name + ".vcxproj", "w")
	str = f.write(str)
	f.close()
		
	filterFile = ""
	filterFile = filterFile + "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
	filterFile = filterFile + "<Project ToolsVersion=\"4.0\" xmlns=\"http://schemas.microsoft.com/developer/msbuild/2003\">\n"
	filterFile = filterFile + "<ItemGroup>\n"
	for filter in filters:
		filterFile = filterFile + "<Filter Include=\"" + filter + "\"/>\n"	
	filterFile = filterFile + "</ItemGroup>\n"
	filterFile = filterFile + "<ItemGroup>\n" + fileSrcStr + "</ItemGroup>\n"
	filterFile = filterFile + "<ItemGroup>\n" + fileIncStr + "</ItemGroup>\n"
	filterFile = filterFile + "</Project>\n"

	f = open(name + ".vcxproj.filters", "w")
	str = f.write(filterFile)
	f.close()

