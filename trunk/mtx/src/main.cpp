#include "qi_base.h"
#include "qi_file.h"
#include "qi_gfx.h"
#include "qi_init.h"
#include "gfx/qimtx.h"


void app(const QiCommandLine& cmd)
{
	QiString in = cmd.getString().getWord(0);
	QiString out = cmd.getString().getWord(1);

	int w, h;
	unsigned char* rgbaData;
	QiFileInputStream fin(in);
    int size = fin.getSize();
	QiPngDecoder dec(fin, size);
	w = dec.getWidth();
	h = dec.getHeight();
	rgbaData = (unsigned char*)QiAlloc(w*h*4);
	dec.decode((unsigned char*)rgbaData, false);
	fin.close();
	
	QiFileOutputStream fout(out);
	QiMtxEncoder mtxEnc(fout, w, h);
	mtxEnc.encode(rgbaData);
	fout.close();
	QiFree(rgbaData);
	
	// -------------------------------------------------------
	/*
	fin.open(out);
	QiMtxDecoder mtxDec(fin, fin.getSize());
	w = mtxDec.getWidth();
	h = mtxDec.getHeight();
	rgbaData = (unsigned char*)QiAlloc(w*h*4);
	mtxDec.decode(rgbaData);
	fin.close();
	
	fout.open("test.png");
	QiPngEncoder enc(fout, w, h, true);
	enc.encode(rgbaData);
	fout.close();		
	QiFree(rgbaData);
	*/
}	



int main(int argc, char** argv)
{
	QiInit();
	QiCommandLine cmd(argc, argv);
	app(cmd);
	QiShutdown();
}
