basedir = "../"

import sys
sys.path.append(basedir + "tools")
import buildsys

name = "mtx"

cfg = []
cfg.append("release")
cfg.append("gl")

src = []
src.append("src")
src.append("../src/qifw")

inc = []

pre = []

flags = []

buildsys.buildMakefile(name, cfg, src, inc, pre, flags)
template = basedir + "tools/buildsys-vc2010-template.vcxproj"
buildsys.buildVc2010(template, name, cfg, src, inc, pre)

