/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "font.h"
#include "game.h"
#include "debug.h"

Font::Font(const char* name, bool numbers) : mWidth(0.0f), mHeight(0.0f), mChars(0), mNumbers(numbers)
{
	mFmt.clear();
	mFmt.addField("aPosition", QiVertexFormat::FLOAT32, 3);
	mFmt.addField("aTexCoord", QiVertexFormat::FLOAT32, 2);
	mVb.init(mFmt, FONT_MAX_CHARS*4);
	mVb.clear();
	mIb.init(FONT_MAX_CHARS*6);
	mIb.clear();
	for(int i=0; i<FONT_MAX_CHARS; i++)
		mIb.quad(i*4+0, i*4+1, i*4+2, i*4+3);


	mTexture = gGame->mResMan->acquireTexture(QiString("fonts/") + name + QiString(".png"));
	if (!mTexture.getTexture())
	{
		gGame->logE(QiString("Couldn't find font for texture: ") + name);
		return;
	}

	int w = mTexture.getTexture()->getWidth();

	QiMemoryStream<256> tmp;
	if (gGame->mResMan->load(QiString("fonts/") + name + QiString(".fnt"), tmp))
	{
		char str[256];
		int rows = FONT_ROWS;
		int cols = FONT_COLS;
		if (mNumbers)
		{
			rows = 4;
			cols = 4;
		}
		int fontChars = rows*cols;
		for(int i=0; i<fontChars; i++)
		{
			if (!tmp.readLine(str, 256))
			{
				gGame->logW(QiString("Couldn't read spacing for font: ") + name);
				break;
			}
			mSpacing[i] = QiString(str).toFloat();
			if (mSpacing[i] == 0)
				mSpacing[i] = w/(float)cols;
		}
	}
}


void Font::setText(const QiString& str)
{
	int w = mTexture.getTexture()->getWidth();
	int h = mTexture.getTexture()->getHeight();

	float x = 0.0f;

	mText = str.toUpperCase();

	mVb.clear();

	mWidth = 0.0f;
	mChars = 0;

	int rows = FONT_ROWS;
	int cols = FONT_COLS;
	char first = FONT_FIRST;
	if (mNumbers)
	{
		rows = 4;
		cols = 4;
		first = '0';
	}
	int fontChars = rows*cols;

	float ch = h/(float)rows;
	for(int i=0; i<mText.getLength(); i++)
	{
		int c = (int)mText[i] - (int)first;
		if (c >= 0 && c < fontChars)
		{
			float cw = mSpacing[c];
			int row = c / cols;
			int col = c % cols;
			float ctw = 1.0f / (float)cols;
			float cth = 1.0f / (float)rows;
			float tlx = ctw*col;
			float tly = cth*row;
			float thx = tlx+(mSpacing[c]/(float)w);
			float thy = tly+cth;
			mVb.vertex();
			mVb.addFast(QiVec3(x, 0, 0));
			mVb.addFast(QiVec2(tlx, tly));
			mVb.vertex();
			mVb.addFast(QiVec3(x, ch, 0));
			mVb.addFast(QiVec2(tlx, thy));
			mVb.vertex();
			mVb.addFast(QiVec3(x+cw, ch, 0));
			mVb.addFast(QiVec2(thx, thy));
			mVb.vertex();
			mVb.addFast(QiVec3(x+cw, 0, 0));
			mVb.addFast(QiVec2(thx, tly));
			mChars++;
			x += cw;
			mWidth += cw;
		}
	}

	mHeight = ch;
}



