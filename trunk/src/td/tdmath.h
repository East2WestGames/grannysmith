/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#pragma once

#include "td.h"
#include "tdconfig.h"

#include <math.h>
#include <float.h>

#ifdef TD_SSE
#include <xmmintrin.h>
#endif

inline int TdAbs(int i) { return ::abs(i); }
inline long long TdAbs(long long i) { if (i < 0) return -i; else return i; }
template<class T> inline T TdMin(T a, T b) { return a < b ? a : b; }
template<class T> inline T TdMax(T a, T b) { return a > b ? a : b; }
template<class T> inline T TdMin(T a, T b, T c) { return TdMin(TdMin(a, b), c); }
template<class T> inline T TdMax(T a, T b, T c) { return TdMax(TdMax(a, b), c); }
template<class T> inline T TdClamp(T value, T minValue, T maxValue) { return TdMin(TdMax(value, minValue), maxValue); }
template<class T> inline void TdSwap(T& v0, T& v1) { T tmp = v1; v1 = v0; v0 = tmp; }

const float TD_PI = 3.14159265358979323846f;
const float TD_FLOAT_MAX = FLT_MAX;
const float TD_FLOAT_MIN = FLT_MIN;
const float TD_FLOAT_EPSILON = FLT_EPSILON;
const float TD_ORTHONORMAL_TOLERANCE = 0.99f;
const float TD_ORTHONORMAL_TOLERANCE_SQ = TD_ORTHONORMAL_TOLERANCE*TD_ORTHONORMAL_TOLERANCE;
const int TD_INT_MAX = 0x7fffffff;

inline int TdCeil(float f) { return int(::ceilf(f)); }
inline int TdFloor(float f) { return int(::floorf(f)); }
inline int TdRound(float a) { return int(::floorf(a + 0.5f)); }
inline float TdSin(float arg) { return ::sinf(arg); }
inline float TdCos(float arg) { return ::cosf(arg); }
inline float TdTan(float arg) { return ::tanf(arg); }
inline float TdASin(float arg) { return ::asinf(TdClamp(arg, -1.0f, 1.0f)); }
inline float TdACos(float arg) { return ::acosf(TdClamp(arg, -1.0f, 1.0f)); }
inline float TdATan(float arg) { return ::atanf(arg); }
inline float TdATan(float y, float x) { return ::atan2f(y, x); }
inline float TdAbs(float f) { return ::fabsf(f); }
inline float TdPow(float arg, float exp) { return ::powf(arg, exp); }
inline float TdMod(float a, float b) { return ::fmodf(a, b); }
inline float TdExp(float arg) { return ::expf(arg); }
inline float TdLog10(float arg) { return ::log10f(arg); }
inline float TdLog(float arg) { return ::logf(arg); }
inline float TdSign(float arg) { if (arg == 0.0f) return 0.0f; else return (arg < 0.0f ? -1.0f : 1.0f); }
inline int TdSign(int arg) { if (arg == 0) return 0; else return (arg < 0 ? -1 : 1); }

inline float TdSqrt(float arg)
{
	TD_ASSERT(arg >= 0, "Square root on negative value");
#ifdef TD_SSE
	float out;
	_mm_store_ss( &out, _mm_sqrt_ss( _mm_load_ss( &arg ) ) );
	return out;
#else
	return ::sqrtf(arg);
#endif	
}


inline float TdRSqrt(float arg)
{
	TD_ASSERT(arg >= 0, "Square root on negative value");
#ifdef TD_SSE
	float out;
	_mm_store_ss( &out, _mm_sqrt_ss( _mm_load_ss( &arg ) ) );
	return 1.0f/out;
#else
	return 1.0f/::sqrtf(arg);
#endif
}


inline float TdRSqrtFast(float arg)
{
	TD_ASSERT(arg >= 0, "Square root on negative value");
#ifdef TD_SSE
	float out;
	_mm_store_ss( &out, _mm_rsqrt_ss( _mm_load_ss( &arg ) ) );
	return out;
#else
	return 1.0f/::sqrtf(arg);
#endif
}


inline float TdSqrtCarmack(float x)
{
	TD_ASSERT(x >= 0, "Square root on negative value");
    float xhalf = 0.5f*x;
    int i = *(int*)&x;
    i = 0x5f3759df - (i>>1);
    x = *(float*)&i;
    x = x*(1.5f - xhalf*x*x);
    return 1.0f/x;
}


inline float TdRSqrtCarmack(float x)
{
	TD_ASSERT(x >= 0, "Square root on negative value");
    float xhalf = 0.5f*x;
    int i = *(int*)&x;
    i = 0x5f3759df - (i>>1);
    x = *(float*)&i;
    x = x*(1.5f - xhalf*x*x);
    return x;
}


#define TD_ASSERT_VEC3_NORMALIZED(vec) { TD_ASSERT(vec.lengthSquared() > TD_ORTHONORMAL_TOLERANCE_SQ && vec.lengthSquared() < 2.0f-TD_ORTHONORMAL_TOLERANCE_SQ, "Vector not normalized");  }

class TdVec3
{
public:
	float x,y,z;

	inline TdVec3(bool zero = true) { if (zero) {x=y=z=0.0f; } }
	inline TdVec3(float x, float y, float z) : x(x), y(y), z(z) { }
	inline TdVec3(const tdVec& vec) : x(vec.x), y(vec.y), z(vec.z) { }
	explicit inline TdVec3(const float e[3]) { x=e[0]; y=e[1]; z=e[2]; }
	inline void set(float x, float y, float z) { this->x = x; this->y = y; this->z = z; }
	inline float operator[](int i) const { return (&x)[i]; }
	inline float& operator[](int i) { return (&x)[i]; }

	inline TdVec3 operator+(const TdVec3& v) const { return TdVec3(x+v.x, y+v.y, z+v.z); }
	inline TdVec3 operator-(const TdVec3& v) const { return TdVec3(x-v.x, y-v.y, z-v.z); }
	inline void operator+=(const TdVec3& v) { x+=v.x; y+=v.y; z+=v.z; }
	inline void operator-=(const TdVec3& v) { x-=v.x; y-=v.y; z-=v.z; }
	inline TdVec3 operator-() const { return TdVec3(-x, -y, -z); }

	inline void operator*=(float f) { x*=f; y*=f; z*=f; } 
	inline void operator/=(float f) { x/=f; y/=f; z/=f; } 	
	inline TdVec3 operator*(float f) const { return TdVec3(x*f, y*f, z*f); }
	inline TdVec3 operator/(float f) const { return TdVec3(x/f, y/f, z/f); }

	inline bool operator==(const TdVec3& vec) const { return x==vec.x && y==vec.y && z==vec.z; }
	inline bool operator!=(const TdVec3 &vec) const { return x!=vec.x || y!=vec.y || z!=vec.z; }

	inline bool equals(const TdVec3& vec, float epsilon) const { return TdAbs(x-vec.x)<=epsilon && TdAbs(y-vec.y)<=epsilon && TdAbs(z-vec.z)<=epsilon; }

	inline float length() const { return TdSqrt(x*x + y*y + z*z); }
	inline float lengthSquared() const { return x*x + y*y + z*z; }

	inline static TdVec3 xAxis() { return TdVec3(1.0f, 0.0f, 0.0f); }
	inline static TdVec3 yAxis() { return TdVec3(0.0f, 1.0f, 0.0f); }
	inline static TdVec3 zAxis() { return TdVec3(0.0f, 0.0f, 1.0f); }
	static TdVec3 random(float length=1.0f);

	inline operator tdVec() { tdVec tmp; tmp.x=x; tmp.y=y; tmp.z=z; return tmp; }
};


inline TdVec3 operator*(float f, const TdVec3& vec) { return TdVec3(vec.x*f, vec.y*f, vec.z*f); }
inline TdVec3 operator/(float f, const TdVec3& vec) { return TdVec3(vec.x/f, vec.y/f, vec.z/f); }
inline float length(const TdVec3& vec) { return TdSqrt(vec.x*vec.x + vec.y*vec.y + vec.z*vec.z); }
inline float lengthSquared(const TdVec3& vec) { return vec.x*vec.x + vec.y*vec.y + vec.z*vec.z; }
inline float manhattanDistance(const TdVec3& vec) { return TdAbs(vec.x)+TdAbs(vec.y)+TdAbs(vec.z); }


inline TdVec3 normalize(const TdVec3& vec)
{
	float l2 = lengthSquared(vec);
	if (l2 > 0.0f)
	{
		float r = TdRSqrt(l2);
		return TdVec3(vec.x*r, vec.y*r, vec.z*r);
	}
	else
		return TdVec3(1.0f, 0.0f, 0.0f);
}


inline TdVec3 normalize(const TdVec3& vec, float& l)
{
	l = length(vec);
	if (l > 0.0f)
		return vec / l;
	else
		return TdVec3(1.0f, 0.0f, 0.0f);
}


inline TdVec3 normalizeFast(const TdVec3& vec)
{
	float rec = TdRSqrtFast(vec.x*vec.x + vec.y*vec.y + vec.z*vec.z);
	return TdVec3(vec.x*rec, vec.y*rec, vec.z*rec);
}


inline TdVec3 normalizeCarmack(const TdVec3& vec)
{
	float rec = TdRSqrtCarmack(vec.x*vec.x + vec.y*vec.y + vec.z*vec.z);
	return TdVec3(vec.x*rec, vec.y*rec, vec.z*rec);
}


inline TdVec3 multiplyPerElement(const TdVec3& a, const TdVec3& b) { return TdVec3(a.x*b.x, a.y*b.y, a.z*b.z); }
inline TdVec3 cross(const TdVec3& a, const TdVec3& b) { return TdVec3(a.y*b.z - b.y*a.z, a.z*b.x - b.z*a.x, a.x*b.y - b.x*a.y); }
inline float dot(const TdVec3 &a, const TdVec3& b) { return a.x*b.x + a.y*b.y + a.z*b.z; }
	
	
inline TdVec3 project(const TdVec3& normal, const TdVec3& vec)
{
	TD_ASSERT_VEC3_NORMALIZED(normal);
	return normal*dot(normal, vec);
}


inline float angle(const TdVec3& normal0, const TdVec3& normal1)
{
	TD_ASSERT_VEC3_NORMALIZED(normal0);
	TD_ASSERT_VEC3_NORMALIZED(normal1);
	return TdACos(dot(normal0, normal1));
}


inline TdVec3 perpendicular(const TdVec3& vec)
{
	if(TdAbs(vec.z) > 0.7f)
		return normalize(TdVec3(0.0f, -vec.z, vec.y));
	else
		return normalize(TdVec3(-vec.y, vec.x, 0.0f));
}


inline TdVec3 TdTransformVecP(const tdTransform* transform, const TdVec3& vec)
{
	return vec;
}


inline TdVec3 TdTransformInvVecP(const tdTransform* transform, const TdVec3& vec)
{
	return vec;
}


inline TdVec3 TdTransformVecM(const tdTransform* transform, const TdVec3& vec)
{
	return TdVec3(vec.x*transform->m[0] + vec.y*transform->m[4] + vec.z*transform->m[8],
				 vec.x*transform->m[1] + vec.y*transform->m[5] + vec.z*transform->m[9],
				 vec.x*transform->m[2] + vec.y*transform->m[6] + vec.z*transform->m[10]);
}


inline TdVec3 TdTransformInvVecM(const tdTransform* transform, const TdVec3& vec)
{
	return TdVec3(vec.x*transform->m[0] + vec.y*transform->m[1] + vec.z*transform->m[2],
				 vec.x*transform->m[4] + vec.y*transform->m[5] + vec.z*transform->m[6],
				 vec.x*transform->m[8] + vec.y*transform->m[9] + vec.z*transform->m[10]);
}

inline TdVec3 TdTransformPointP(const tdTransform* transform, const TdVec3& point)
{
	return TdVec3(point.x + transform->m[0],
				   point.y + transform->m[1],
				   point.z + transform->m[2]);
}

inline TdVec3 TdTransformInvPointP(const tdTransform* transform, const TdVec3& point)
{
	return TdVec3(point.x - transform->m[0],
				   point.y - transform->m[1],
				   point.z - transform->m[2]);
}


inline TdVec3 TdTransformInvPointM(const tdTransform* transform, const TdVec3& point)
{
	const float* m = transform->m;
	TdVec3 p = point-TdVec3(m[12], m[13], m[14]);
	TdVec3 ret;
	ret.x = p.x*m[0] + p.y*m[1] + p.z*m[2];
	ret.y = p.x*m[4] + p.y*m[5] + p.z*m[6];
	ret.z = p.x*m[8] + p.y*m[9] + p.z*m[10];
	return ret;
}

inline TdVec3 TdTransformPointM(const tdTransform* transform, const TdVec3& point)
{
	return TdVec3(point.x*transform->m[0] + point.y*transform->m[4] + point.z*transform->m[8] + transform->m[12],
				   point.x*transform->m[1] + point.y*transform->m[5] + point.z*transform->m[9] + transform->m[13],
				   point.x*transform->m[2] + point.y*transform->m[6] + point.z*transform->m[10] + transform->m[14]);
}

inline TdVec3 TdTransformVec(const tdTransform* transform, const TdVec3& vec)
{
	if (transform->type == TD_TRANSFORM_TYPE_M)
	{
		TdVec3 ret;
		const float* m = transform->m;
		ret.x = vec.x*m[0] + vec.y*m[4] + vec.z*m[8];
		ret.y = vec.x*m[1] + vec.y*m[5] + vec.z*m[9];
		ret.z = vec.x*m[2] + vec.y*m[6] + vec.z*m[10];
		return ret;
	}
	else
		return vec;
}

inline TdVec3 TdTransformInvVec(const tdTransform* transform, const TdVec3& vec)
{
	if (transform->type == TD_TRANSFORM_TYPE_M)
	{
		TdVec3 ret;
		const float* m = transform->m;
		ret.x = vec.x*m[0] + vec.y*m[1] + vec.z*m[2];
		ret.y = vec.x*m[4] + vec.y*m[5] + vec.z*m[6];
		ret.z = vec.x*m[8] + vec.y*m[9] + vec.z*m[10];
		return ret;
	}
	else
		return vec;
}

inline TdVec3 TdTransformPoint(const tdTransform* transform, const TdVec3& point)
{
	switch(transform->type)
	{
		case TD_TRANSFORM_TYPE_P:
		return point + (TdVec3&)*transform->m;

		case TD_TRANSFORM_TYPE_M:
		{
			const float* m = transform->m;
			TdVec3 ret;
			ret.x = point.x*m[0] + point.y*m[4] + point.z*m[8] + m[12];
			ret.y = point.x*m[1] + point.y*m[5] + point.z*m[9] + m[13];
			ret.z = point.x*m[2] + point.y*m[6] + point.z*m[10] + m[14];
			return ret;
		}

		default:
		return point;
	}
}


inline TdVec3 TdTransformInvPoint(const tdTransform* transform, const TdVec3& point)
{
	switch(transform->type)
	{
		case TD_TRANSFORM_TYPE_P:
		return point - (TdVec3&)*transform->m;

		case TD_TRANSFORM_TYPE_M:
		{
			const float* m = transform->m;
			TdVec3 p = point-TdVec3(m[12], m[13], m[14]);
			TdVec3 ret;
			ret.x = p.x*m[0] + p.y*m[1] + p.z*m[2];
			ret.y = p.x*m[4] + p.y*m[5] + p.z*m[6];
			ret.z = p.x*m[8] + p.y*m[9] + p.z*m[10];
			return ret;
		}

		default:
		return point;
	}
}


class TdQuat
{
public:
	float x,y,z,w;

	inline TdQuat() : x(0.0f), y(0.0f), z(0.0f), w(1.0f) { }
	inline TdQuat(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) { }
	inline TdQuat(const float f[4]) { x=f[0]; y=f[1]; z=f[2]; w=f[3]; }
	inline TdQuat(const TdVec3& axis, float angle) { setAxisAngle(axis, angle); }
	inline TdQuat(const tdQuat& q) : x(q.x), y(q.y), z(q.z), w(q.w) { }
	inline void set(float x, float y, float z, float w) { this->x = x; this->y = y; this->z = z; this->w = w; }
	
	inline TdQuat getNormalized() const
	{
		float square = x*x + y*y + z*z + w*w;
		if (square > 0.0f)
		{
			float s = TdRSqrt(square);
			return TdQuat(x*s, y*s, z*s, w*s);
		}
		else
			return TdQuat();
	}

	inline void normalize() { (*this) = getNormalized(); }
	inline TdQuat getConjugate() const { return TdQuat(-x, -y, -z, w); }

	inline TdVec3 rotate(const TdVec3& vec) const
	{
		TdVec3 v(x, y, z);
		float d = ::dot(v, vec);
		return vec * (2.0f*w*w-1.0f) + cross(v, vec)*(w*2.0f) + v*(d*2.0f);
	}

	inline TdVec3 rotateInv(const TdVec3& vec) const
	{
		TdVec3 v(x, y, z);
		float d = ::dot(v, vec);
		return vec * (2.0f*w*w-1.0f) - cross(v, vec)*(w*2.0f) + v*(d*2.0f);
	}
	
	inline void getAxisAngle(TdVec3& axis, float& angle) const
	{
		angle = TdACos(w) * 2.0f;
		float sa = TdSqrt(1.0f - w*w);
		if (sa > 0.0f)
			axis.set(x/sa, y/sa, z/sa);
		else
			axis.set(1,0,0);
	}

	inline void setAxisAngle(const TdVec3& axis, float angle)
	{
		TD_ASSERT_VEC3_NORMALIZED(axis);
		float half = angle*0.5f;
		float tmp = TdSin(half);
		x = axis.x * tmp;
		y = axis.y * tmp;
		z = axis.z * tmp;
		w = TdCos(half);
	}

	inline TdQuat operator*(const TdQuat& other) const
	{
		TdVec3 v(x, y, z);
		TdVec3 ov(other.x, other.y, other.z);
		TdVec3 v2 = cross(v, ov) + ov*w + v*other.w;
		float w2 = w*other.w - dot(v, ov);
		return TdQuat(v2.x, v2.y, v2.z, w2);
	}

	inline void operator*=(const TdQuat& other) { (*this) = (*this) * other; }

	inline TdVec3 getX() const { return TdVec3(1.0f - 2.0f*(y*y + z*z), 2.0f*(x*y + z*w), 2.0f*(x*z - y*w)); }
	inline TdVec3 getY() const { return TdVec3(2.0f*(x*y - z*w), 1.0f - 2.0f*(x*x + z*z), 2.0f*(y*z + x*w)); }
	inline TdVec3 getZ() const { return TdVec3(2.0f*(x*z + y*w), 2.0f*(y*z - x*w), 1.0f - 2.0f*(x*x + y*y)); }	
};

