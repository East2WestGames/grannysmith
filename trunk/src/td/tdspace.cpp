/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#include "td.h"
#include "tddbvt.h"

static const int BATCH_SIZE = 32;

#include "tdspace.h"

class TdSpaceDynamicAabb : public TdSpace
{
public:
	btDbvt mTree;
};

class BufferedOverlapDispatcher : public btDbvt::ICollide
{
public:
	inline BufferedOverlapDispatcher(TdSpace* space) : 
	mSpace(space)
	{
	}
		
	virtual void Process(const btDbvtNode* a, const btDbvtNode* b)
	{
		mSpace->mOverlaps0.push_back((int)a->data);
		mSpace->mOverlaps1.push_back((int)b->data);
	}
	
	virtual void Process(const btDbvtNode* a)
	{
		mSpace->mOverlaps0.push_back((int)a->data);
	}
	
	TdSpace* mSpace;
};


int tdSpaceCreate(const tdSpace* space)
{
	if (space->type == TD_SPACE_DYNAMIC_AABB || space->type == TD_SPACE_STATIC_AABB)
	{
		TdSpaceDynamicAabb* s = new TdSpaceDynamicAabb();
		s->mTree.m_eps = 0.01f; //TODO: Make configurable
		s->mType = space->type;
		return (int)s;
	}
	else
		return 0;
}


void tdSpaceDestroy(int space)
{
	TdSpace* s = (TdSpace*)space;
	delete s;
}


void tdSpaceOptimize(int space, int iterations)
{
	TdSpace* s = (TdSpace*)space;
	if (s->mType == TD_SPACE_DYNAMIC_AABB)
	{		
		TdSpaceDynamicAabb* s = (TdSpaceDynamicAabb*)space;
		if (iterations == 0)
		{
			s->mTree.optimizeBottomUp();
		}
		else
			s->mTree.optimizeIncremental(iterations);
	}
}


void tdSpaceClear(int space)
{
	TdSpace* s = (TdSpace*)space;
	if (s->mType == TD_SPACE_DYNAMIC_AABB || s->mType == TD_SPACE_STATIC_AABB)
	{
		TdSpaceDynamicAabb* s = (TdSpaceDynamicAabb*)space;
		s->mTree.clear();
	}	
}


int tdSpaceInsertAabb(int space, const tdAabb* volume, int userData)
{
	TdSpace* s = (TdSpace*)space;
	if (s->mType == TD_SPACE_DYNAMIC_AABB || s->mType == TD_SPACE_STATIC_AABB)
	{
		TdSpaceDynamicAabb* s = (TdSpaceDynamicAabb*)space;
		btDbvtVolume v;
		v.mi = volume->lower;
		v.mx = volume->upper;
		btDbvtNode* node = s->mTree.insert(v, (void*)userData);
		return (int)node;
	}
	else
		return 0;
}


void tdSpaceUpdateAabb(int space, int aabb, const tdAabb* volume)
{
	TdSpace* s = (TdSpace*)space;
	if (s->mType == TD_SPACE_DYNAMIC_AABB || s->mType == TD_SPACE_STATIC_AABB)
	{
		TdSpaceDynamicAabb* s = (TdSpaceDynamicAabb*)space;
		btDbvtNode* n = (btDbvtNode*)aabb;
		btDbvtVolume v;
		v.mi = volume->lower;
		v.mx = volume->upper;
		s->mTree.update(n, v);
	}
}


void tdSpaceRemoveAabb(int space, int aabb)
{
	TdSpace* s = (TdSpace*)space;
	if (s->mType == TD_SPACE_DYNAMIC_AABB || s->mType == TD_SPACE_STATIC_AABB)
	{
		TdSpaceDynamicAabb* s = (TdSpaceDynamicAabb*)space;
		btDbvtNode* n = (btDbvtNode*)aabb;
		s->mTree.remove(n);
	}
}


void tdSpaceOverlapSpace(int context, int space0, int space1)
{
	TdSpace* s0 = (TdSpace*)space0;
	TdSpace* s1 = (TdSpace*)space1;
	if (s0->mType == TD_SPACE_DYNAMIC_AABB || s0->mType == TD_SPACE_STATIC_AABB)
	{
		if (s1->mType == TD_SPACE_DYNAMIC_AABB || s1->mType == TD_SPACE_STATIC_AABB)
		{

			TdSpaceDynamicAabb* s0 = (TdSpaceDynamicAabb*)space0;
			TdSpaceDynamicAabb* s1 = (TdSpaceDynamicAabb*)space1;

			s0->clearOverlaps();
			BufferedOverlapDispatcher od(s0);
			s0->mTree.collideTT(s0->mTree.m_root, s1->mTree.m_root, od);
		}
	}
}


void tdSpaceOverlapAabb(int context, int space, const tdAabb* aabb)
{
	TdSpace* s = (TdSpace*)space;
	if (s->mType == TD_SPACE_DYNAMIC_AABB || s->mType == TD_SPACE_STATIC_AABB)
	{
		TdSpaceDynamicAabb* s = (TdSpaceDynamicAabb*)space;
		btDbvtVolume v;
		v.mi = aabb->lower;
		v.mx = aabb->upper;
		s->clearOverlaps();
		BufferedOverlapDispatcher od(s);
		s->mTree.collideTV(s->mTree.m_root, v, od);
	}
}


void tdSpaceOverlapObb(int context, int space, const tdObb* obb)
{
}


void tdSpaceOverlapLine(int context, int space, const tdVec* start, const tdVec* end)
{
	TdSpace* s = (TdSpace*)space;
	if (s->mType == TD_SPACE_DYNAMIC_AABB || s->mType == TD_SPACE_STATIC_AABB)
	{
		TdSpaceDynamicAabb* s = (TdSpaceDynamicAabb*)space;
		s->clearOverlaps();
		BufferedOverlapDispatcher od(s);
		s->mTree.rayTest(s->mTree.m_root, *start, *end, od);
	}
}


int tdSpaceFetchOverlaps(int context, int space, int* overlaps0, int* overlaps1, int maxCount)
{
	TdSpace* s = (TdSpace*)space;
	int c = TdMin((int)s->mOverlaps0.size()-s->mOverlapIndex, maxCount);
	if (overlaps0 && c>0)
		memcpy(overlaps0, &(s->mOverlaps0[0])+s->mOverlapIndex, c*sizeof(int));
	if (overlaps1 && s->mOverlaps1.size() == s->mOverlaps0.size() && c>0)
		memcpy(overlaps1, &(s->mOverlaps1[0])+s->mOverlapIndex, c*sizeof(int));
	s->mOverlapIndex += c;
	return c;
}

