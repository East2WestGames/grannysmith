/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#include "tdserialize.h"
#include "tdcontext.h"

#if 0
#include "qi_base.h"
#include "qi_file.h"

static QiFileOutputStream gSerOut;
static QiFileInputStream gSerIn;

static tdShape gSerShape0;
static tdShape gSerShape1;
static tdSimplex gSerSimplex;

enum tdSerType
{
	TD_SER_DISTANCE,
	TD_SER_SHAPECAST
};


static void writeContext(int context)
{
	TdContextInternal* ctxt = (TdContextInternal*)context;
	gSerOut.writeInt32(ctxt->maxIterations);
	gSerOut.writeFloat32(ctxt->tolerance);
}


static void readContext(int context)
{
	TdContextInternal* ctxt = (TdContextInternal*)context;
	gSerIn.readInt32(ctxt->maxIterations);
	gSerIn.readFloat32(ctxt->tolerance);
}


static void writeSimplex(const tdSimplex* simplex)
{
	gSerOut.writeBuffer(simplex, sizeof(tdSimplex));
}


static void readSimplex(tdSimplex* simplex)
{
	gSerIn.readBuffer(simplex, sizeof(tdSimplex));
}


static void writeShape(const tdShape* shape)
{
	gSerOut.writeBuffer(shape, sizeof(tdShape));
	if (shape->type == TD_SHAPE_TYPE_HULL)
	{
		for(int i=0; i<shape->hull.count; i++)
		{
			float* v = (float*)(((char*)shape->hull.vertices)+i*shape->hull.stride);
			gSerOut.writeFloat32(v[0]);
			gSerOut.writeFloat32(v[1]);
			gSerOut.writeFloat32(v[2]);
		}
	}
}

static void readShape(tdShape* shape)
{
	gSerIn.readBuffer(shape, sizeof(tdShape));
	if (shape->type == TD_SHAPE_TYPE_HULL)
	{
		TdVec3* data = new TdVec3[shape->hull.count];
		for(int i=0; i<shape->hull.count; i++)
		{
			gSerIn.readFloat32(data[i].x);
			gSerIn.readFloat32(data[i].y);
			gSerIn.readFloat32(data[i].z);
		}
		shape->hull.vertices = (float*)data;
		shape->hull.stride = sizeof(TdVec3);
	}
}

void tdSerWriteDistance(int context, const tdDistanceQuery* query)
{
	gSerOut.open("td_query.bin");
	gSerOut.writeUInt32(TD_SER_DISTANCE);
	writeContext(context);
	gSerOut.writeBuffer(query, sizeof(tdDistanceQuery));
	writeShape(query->shape0);
	writeShape(query->shape1);
	if (query->simplex)
		writeSimplex(query->simplex);
	gSerOut.close();
}


void tdSerReadDistance(int context, tdDistanceQuery* query)
{
	gSerIn.open("td_query.bin");
	tdSerType type;
	gSerIn.readUInt32((QiUInt32&)type);
	readContext(context);
	gSerIn.readBuffer(query, sizeof(tdDistanceQuery));

	readShape(&gSerShape0);
	query->shape0 = &gSerShape0;
	readShape(&gSerShape1);
	query->shape1 = &gSerShape1;

	if (query->simplex)
	{
		readSimplex(&gSerSimplex);
		query->simplex = &gSerSimplex;
	}

	gSerIn.close();
}

#endif

