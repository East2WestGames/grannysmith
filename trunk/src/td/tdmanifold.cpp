/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#include "td.h"
#include "tdcontext.h"

void tdManifold(int context, tdContactManifold* manifold, tdManifoldQuery* query, tdManifoldResult* result)
{
	tdSimplex simplex;
	float eps = 0.05f;
	float eps2 = eps*eps;

	if (manifold->count == 0 && manifold->normal0.x == 0.12345f)
	{
		bool valid = true;
		for(int i=0; i<3; i++)
		{
			TdVec3& p0 = (TdVec3&)manifold->points0[i];
			TdVec3& p1 = (TdVec3&)manifold->points1[i];
			if (lengthSquared(p0-TdTransformPoint(&query->transform, p1)) > eps2*0.1f)
			{
				valid = false;
				break;
			}
		}
		if (valid)
			return;
	}	

	//Compute drift and prune manifold
	TdVec3& normal0 = (TdVec3&)manifold->normal0;
	TdVec3& normal1 = (TdVec3&)manifold->normal1;

	//Check old manifold
	for(int i=0; i<manifold->count; i++)
	{
		TdVec3& p0 = (TdVec3&)manifold->points0[i];
		TdVec3& p1 = (TdVec3&)manifold->points1[i];
		TdVec3 drift = TdTransformPoint(&query->transform, p1)-p0;
		float perpDrift2 = lengthSquared(drift-normal0*dot(normal0, drift));
		if (perpDrift2 > eps2*4)
		{
			//manifold->points[i] = manifold->points[manifold->pointCount-1];
			//manifold->pointCount--;
			//i--;
			manifold->count = 0;
			break;
		}
		float normDrift = dot(normal0, drift);
		if (normDrift > eps*2)
		{
			manifold->points0[i] = manifold->points0[manifold->count-1];
			manifold->points1[i] = manifold->points1[manifold->count-1];
			manifold->count--;
			i--;		
		}
	}
	
	if (manifold->count > 0)
	{
		//All points are intact and orientation has not changed much. No update needed.
		if (dot(normal0, TdTransformVec(&query->transform, (TdVec3&)normal1)) > 0.99999f)
			return;
	}

	tdDistanceQuery q={0};
	q.flags |= TD_QUERY_COMPUTE_NORMAL;
	q.flags |= TD_QUERY_COMPUTE_POINTS;
	q.flags |= TD_QUERY_HANDLE_PENETRATION;	
	if (query->simplex)
	{
		q.flags |= TD_QUERY_LOAD_SIMPLEX;
		q.simplex = query->simplex;
	}
	else
	{
		q.simplex = &simplex;
	}		
	q.flags |= TD_QUERY_STORE_SIMPLEX;
	q.flags |= TD_QUERY_USE_MAX_DISTANCE;
	q.shape0 = query->shape0;
	q.shape1 = query->shape1;
	q.transform = query->transform;
	q.maxDistance = query->maxDistance;
	tdDistanceResult r;
	tdDistance(context, &q, &r);

	if (r.status & TD_RESULT_MAX_DISTANCE_REACHED || r.distance > q.maxDistance)
	{
		manifold->count = 0;
		(TdVec3&)manifold->points0[0] = TdVec3(1,0,0);
		(TdVec3&)manifold->points1[0] = TdTransformInvPoint(&query->transform, TdVec3(1,0,0));
		(TdVec3&)manifold->points0[1] = TdVec3(0,1,0);
		(TdVec3&)manifold->points1[1] = TdTransformInvPoint(&query->transform, TdVec3(0,1,0));
		(TdVec3&)manifold->points0[2] = TdVec3(0,0,1);
		(TdVec3&)manifold->points1[2] = TdTransformInvPoint(&query->transform, TdVec3(0,0,1));
		manifold->normal0.x = 0.12345f;
		return;
	}
	
	(TdVec3&)manifold->normal0 = (TdVec3&)r.normal;
	(TdVec3&)manifold->normal1 = TdTransformInvVec(&query->transform, (TdVec3&)manifold->normal0);

	if (manifold->count > 0)
	{
		TdVec3& newPoint = (TdVec3&)r.point0;
		int best = 0;
		if (manifold->count == 0)
		{
			manifold->count = 1;
		}
		else
		{
			float bestDist2 = lengthSquared(((TdVec3&)manifold->points0[0]) - newPoint);
			for(int i=1; i<manifold->count; i++)
			{
				TdVec3& p0 = (TdVec3&)manifold->points0[i];
				float dist2 = lengthSquared(newPoint-p0);
				if (dist2 < bestDist2)
				{
					bestDist2 = dist2;
					best = i;
				}
			}
			if (manifold->count < 4 && bestDist2 > eps2)
			{
				best = manifold->count;
				manifold->count++;
			}
		}

		//Fill in the new point and be done
		(TdVec3&)manifold->points0[best] = (TdVec3&)r.point0;
		(TdVec3&)manifold->points1[best] = (TdVec3&)r.point1;
		return;
	}
	
	(TdVec3&)manifold->points0[0] = (TdVec3&)r.point0;
	(TdVec3&)manifold->points1[0] = (TdVec3&)r.point1;
	manifold->count=1;
	
#if 1

	q.flags &= ~TD_QUERY_STORE_SIMPLEX;
	q.flags &= ~TD_QUERY_USE_MAX_DISTANCE;

	TdVec3 x,y;
	x = perpendicular(normal0);
	y = cross(normal0, x);
	TdQuat rot[4];
	rot[0].setAxisAngle(x, 0.05f);
	rot[1] = rot[0].getConjugate();
	rot[2].setAxisAngle(y, 0.05f);
	rot[3] = rot[2].getConjugate();
	manifold->count = 0;
	for(int i=0; i<query->maxCount; i++)
	{
		TdVec3 bx(query->transform.m[0], query->transform.m[1], query->transform.m[2]);
		TdVec3 by(query->transform.m[4], query->transform.m[5], query->transform.m[6]);
		TdVec3 bz(query->transform.m[8], query->transform.m[9], query->transform.m[10]);
		bx = rot[i].rotate(bx);
		by = rot[i].rotate(by);
		bz = rot[i].rotate(bz);
		q.transform.m[0] = bx.x; q.transform.m[1] = bx.y; q.transform.m[2] = bx.z; 
		q.transform.m[4] = by.x; q.transform.m[5] = by.y; q.transform.m[6] = by.z; 
		q.transform.m[8] = bz.x; q.transform.m[9] = bz.y; q.transform.m[10] = bz.z; 
		
		TdVec3 pos(query->transform.m[12], query->transform.m[13], query->transform.m[14]);
		TdVec3 tmp = rot[i].rotate(TdVec3(r.point0)-pos);
		pos -= -normal0*(2*eps) + normal0*dot(normal0, tmp);
		q.transform.m[12] = pos.x;
		q.transform.m[13] = pos.y;
		q.transform.m[14] = pos.z;

		tdDistance(context, &q, &r);

		bool found = false;
		TdVec3& a = (TdVec3&)r.point0;
		for(int i=0; i<manifold->count; i++)
		{
			TdVec3& b = (TdVec3&)manifold->points0[i];
			if (lengthSquared(a-b) < eps*eps)
			{
				found = true;
				break;
			}
		}

		if (!found)
		{
			TdVec3& n = (TdVec3&)r.normal;
			TdVec3& p0 = (TdVec3&)r.point0;
			TdVec3 p1 = TdTransformPoint(&query->transform, (TdVec3&)r.point1);
			p1 = p0 + n*dot(n, p1-p0);
			p1 = TdTransformInvPoint(&query->transform, p1);
			
			(TdVec3&)manifold->points0[manifold->count] = p0;
			(TdVec3&)manifold->points1[manifold->count] = p1;
			manifold->count++;
			if (manifold->count == 4)
				break;
		}
	}
#endif
}
