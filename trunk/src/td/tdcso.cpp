/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#include "tdcso.h"
#include "tdsupportfunc.h"


void tdSupportGeneric(const TdCso* cso, const TdVec3& dir, TdSimplexPoint& point)
{
	point.p0 = TdShapeGetSupport(cso->mShape0, dir);
	point.p1 = TdShapeGetSupport(cso->mShape1, TdTransformInvVec(cso->mTransform, -dir));
	point.cs = point.p0 - TdTransformPoint(cso->mTransform, point.p1);
}


void TdCso::setSupportFunc()
{
	int t = 0;
	switch(mTransform->type)
	{
		case TD_TRANSFORM_TYPE_P:
		t = 0;
		break;
		case TD_TRANSFORM_TYPE_M:
		t = 1;
		break;
		default:
		t = 2;
	};
	int s0 = (int)mShape0->type;
	int s1 = (int)mShape1->type;
	if (t < funcTableTransformCount && s0 < funcTableShapeCount && s1 < funcTableShapeCount)
		mFunc = funcTable[(s0*funcTableShapeCount + s1)*2 + t];
	else
		mFunc = tdSupportGeneric;
	mFunc = tdSupportGeneric;
}


