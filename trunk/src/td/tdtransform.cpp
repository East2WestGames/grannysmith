/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#include "td.h"
#include "tdmath.h"
#include <string.h>

void tdTransformInit(tdTransform* transform)
{
	transform->type = TD_TRANSFORM_TYPE_P;
	transform->m[0] = 0.0f;
	transform->m[1] = 0.0f;
	transform->m[2] = 0.0f;
}


void tdTransformInitP(tdTransform* transform, const tdVec* point)
{
	transform->type = TD_TRANSFORM_TYPE_P;
	transform->m[0] = point->x;
	transform->m[1] = point->y;
	transform->m[2] = point->z;
}


void tdTransformInitPQ(tdTransform* transform, const tdVec* point, const tdQuat* quat)
{
	const float& x = quat->x;
	const float& y = quat->y;
	const float& z = quat->z;
	const float& w = quat->w;
	//Find x y and z axis from quaternion
	TdVec3 xa(1.0f - 2.0f*(y*y + z*z), 2.0f*(x*y + z*w), 2.0f*(x*z - y*w));
	TdVec3 ya(2.0f*(x*y - z*w), 1.0f - 2.0f*(x*x + z*z), 2.0f*(y*z + x*w));
	TdVec3 za(2.0f*(x*z + y*w), 2.0f*(y*z - x*w), 1.0f - 2.0f*(x*x + y*y));
	//Fill in matrix
	transform->type = TD_TRANSFORM_TYPE_M;
	transform->m[0] = xa.x;
	transform->m[1] = xa.y;
	transform->m[2] = xa.z;
	transform->m[3] = 0.0f;
	transform->m[4] = ya.x;
	transform->m[5] = ya.y;
	transform->m[6] = ya.z;
	transform->m[7] = 0.0f;
	transform->m[8] = za.x;
	transform->m[9] = za.y;
	transform->m[10] = za.z;		
	transform->m[11] = 0.0f;
	transform->m[12] = point->x;
	transform->m[13] = point->y;
	transform->m[14] = point->z;
	transform->m[15] = 1.0f;
}


void tdTransformInitM16(tdTransform* transform, const float* matrix4x4)
{
	transform->type = TD_TRANSFORM_TYPE_M;
	memcpy(transform->m, matrix4x4, 16*sizeof(float));
}

