/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#pragma once

#include <string.h>
#include <stdlib.h>
#include <assert.h>

#ifdef _DEBUG
#define TD_DEBUG
#endif

//#define TD_PROFILE
//#define TD_DEBUG_TRACE

#if _MSC_VER
#pragma warning (disable: 4996)
#pragma warning (disable: 4530)
#define TD_WIN32
#endif

//#define TD_MACOS

#if defined(TD_WIN32) || defined(TD_MACOS)
#define TD_SSE
#endif

#ifdef TD_DEBUG
#include <stdio.h>
#define TD_WARNING(msg) { printf("Warning: %s\n"); }
#define TD_ERROR(msg) { printf("Error: %s\n", msg); assert(false); }
#define TD_ASSERT(cond, msg) {if(!(cond)){TD_ERROR(msg); }}
#else
#define TD_WARNING(msg) (void(0))
#define TD_ERROR(msg) (void(0))
#define TD_ASSERT(cond, msg) (void(0))
#endif

#ifdef TD_PROFILE
#include "dresscode.h"
#define TD_PROFILE_OPEN dcOpen("TD")
#define TD_PROFILE_CLOSE dcClose();
#define TD_PROFILE_ZONE(name) dcZone(name);
#define TD_PROFILE_ENTER(name) dcEnter(name);
#define TD_PROFILE_LEAVE() dcLeave();
#else
#define TD_PROFILE_OPEN (void(0))
#define TD_PROFILE_CLOSE (void(0))
#define TD_PROFILE_ZONE(name) (void(0))
#define TD_PROFILE_ENTER(name) (void(0))
#define TD_PROFILE_LEAVE() (void(0))
#endif

