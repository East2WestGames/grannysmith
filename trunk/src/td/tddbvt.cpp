/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */
/* Based on dbvt implementation from Bullet physics library, see below */

/*
Bullet Continuous Collision Detection and Physics Library
Copyright (c) 2003-2006 Erwin Coumans  http://continuousphysics.com/Bullet/

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.
Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it freely,
subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/
///btDbvt implementation by Nathanael Presson


#include "tddbvt.h"

typedef std::vector<btDbvtNode*>			tNodeArray;
typedef std::vector<const btDbvtNode*>		tConstNodeArray;

//
struct btDbvtNodeEnumerator : btDbvt::ICollide
{
	tConstNodeArray	nodes;
	void Process(const btDbvtNode* n) { nodes.push_back(n); }
};

//
static inline int indexof(const btDbvtNode* node)
{
	return(node->parent->childs[1]==node);
}

//
static inline btDbvtVolume	merge(	const btDbvtVolume& a, const btDbvtVolume& b)
{
	btDbvtVolume	res;
	Merge(a,b,res);
	return(res);
}

// volume+edge lengths
static inline float size(const btDbvtVolume& a)
{
	const TdVec3 edges=a.Lengths();
	return(	edges.x*edges.y*edges.z + edges.x+edges.y+edges.z);
}

//
static void getmaxdepth(const btDbvtNode* node,int depth,int& maxdepth)
{
	if(node->isinternal())
	{
		getmaxdepth(node->childs[0],depth+1,maxdepth);
		getmaxdepth(node->childs[0],depth+1,maxdepth);
	} else maxdepth=TdMax(maxdepth, depth);
}

//
static inline void deletenode(	btDbvt* pdbvt, btDbvtNode* node)
{
	delete pdbvt->m_free;
	pdbvt->m_free=node;
}

//
static void	recursedeletenode(	btDbvt* pdbvt, btDbvtNode* node)
{
	if(!node->isleaf())
	{
		recursedeletenode(pdbvt,node->childs[0]);
		recursedeletenode(pdbvt,node->childs[1]);
	}
	if(node==pdbvt->m_root) pdbvt->m_root=0;
	deletenode(pdbvt,node);
}

//
static inline btDbvtNode*	createnode(	btDbvt* pdbvt,
										   btDbvtNode* parent,
										   void* data)
{
	btDbvtNode*	node;
	if(pdbvt->m_free)
	{ node=pdbvt->m_free;pdbvt->m_free=0; }
	else
	{ node=new btDbvtNode(); }
	node->parent	=	parent;
	node->data		=	data;
	node->childs[1]	=	0;
	return(node);
}

//
static inline btDbvtNode*	createnode(	btDbvt* pdbvt,
										   btDbvtNode* parent,
										   const btDbvtVolume& volume,
										   void* data)
{
	btDbvtNode*	node=createnode(pdbvt,parent,data);
	node->volume=volume;
	node->childCount = 0;
	return(node);
}

//
static inline btDbvtNode*	createnode(	btDbvt* pdbvt,
										   btDbvtNode* parent,
										   const btDbvtVolume& volume0,
										   const btDbvtVolume& volume1,
										   void* data)
{
	btDbvtNode*	node=createnode(pdbvt,parent,data);
	Merge(volume0,volume1,node->volume);
	node->volume.Expand(pdbvt->m_eps);
	return(node);
}

//
static void insertleaf(	btDbvt* pdbvt, btDbvtNode* root, btDbvtNode* leaf)
{
	if(!pdbvt->m_root)
	{
		pdbvt->m_root	=	leaf;
		leaf->parent	=	0;
	}
	else
	{
		if(!root->isleaf())
		{
			do	{
				root=root->childs[Select(	leaf->volume,
					root->childs[0]->volume,
					root->childs[1]->volume)];
			} while(!root->isleaf());
		}
		btDbvtNode*	prev=root->parent;
		btDbvtNode*	node=createnode(pdbvt,prev,leaf->volume,root->volume,0);
		node->childCount = root->childCount+2;
		btDbvtNode*	p = prev;
		while(p)
		{
			p->childCount+=2;
			p = p->parent;
		}
		if(prev)
		{
			prev->childs[indexof(root)]	=	node;
			node->childs[0]				=	root;root->parent=node;
			node->childs[1]				=	leaf;leaf->parent=node;
			do	{
				if(!prev->volume.Contain(node->volume))
				{
					Merge(prev->childs[0]->volume,prev->childs[1]->volume,prev->volume);
					prev->volume.Expand(pdbvt->m_eps);
				}
				else
					break;
				node=prev;
			} while(0!=(prev=node->parent));
		}
		else
		{
			node->childs[0]	=	root;root->parent=node;
			node->childs[1]	=	leaf;leaf->parent=node;
			pdbvt->m_root	=	node;
		}
	}
}

//
static btDbvtNode* removeleaf(	btDbvt* pdbvt, btDbvtNode* leaf)
{
	btDbvtNode* p = leaf->parent;
	while(p)
	{
		p->childCount-=2;
		p = p->parent;
	}
	if(leaf==pdbvt->m_root)
	{
		pdbvt->m_root=0;
		return(0);
	}
	else
	{
		btDbvtNode*	parent=leaf->parent;
		btDbvtNode*	prev=parent->parent;
		btDbvtNode*	sibling=parent->childs[1-indexof(leaf)];			
		if(prev)
		{
			prev->childs[indexof(parent)]=sibling;
			sibling->parent=prev;
			deletenode(pdbvt,parent);
			while(prev)
			{
				const btDbvtVolume	pb=prev->volume;
				Merge(prev->childs[0]->volume,prev->childs[1]->volume,prev->volume);
				prev->volume.Expand(pdbvt->m_eps);
				if(NotEqual(pb,prev->volume))
				{
					prev=prev->parent;
				} else break;
			}
			return(prev?prev:pdbvt->m_root);
		}
		else
		{								
			pdbvt->m_root=sibling;
			sibling->parent=0;
			deletenode(pdbvt,parent);
			return(pdbvt->m_root);
		}			
	}
}

//
static void	fetchleaves(btDbvt* pdbvt, btDbvtNode* root, tNodeArray& leaves, int depth=-1)
{
	if(root->isinternal()&&depth)
	{
		fetchleaves(pdbvt,root->childs[0],leaves,depth-1);
		fetchleaves(pdbvt,root->childs[1],leaves,depth-1);
		deletenode(pdbvt,root);
	}
	else
	{
		leaves.push_back(root);
	}
}

//
static void split(	const tNodeArray& leaves,
					tNodeArray& left,	
					tNodeArray& right,
					const TdVec3& org,
					const TdVec3& axis)
{
	left.resize(0);
	right.resize(0);
	for(int i=0,ni=leaves.size(); i<ni; ++i)
	{
		if(dot(axis, leaves[i]->volume.Center()-org) < 0.0f)
			left.push_back(leaves[i]);
		else
			right.push_back(leaves[i]);
	}
}

//
static btDbvtVolume				bounds(	const tNodeArray& leaves)
{
	btDbvtVolume volume=leaves[0]->volume;
	for(int i=1,ni=leaves.size();i<ni;++i)
	{
		Merge(volume,leaves[i]->volume,volume);
	}
	return(volume);
}

//
static void						bottomup(	btDbvt* pdbvt,
										 tNodeArray& leaves)
{
	while(leaves.size()>1)
	{
		float	minsize=TD_FLOAT_MAX;
		int			minidx[2]={-1,-1};
		for(int i=0;i<(int)leaves.size();++i)
		{
			for(int j=i+1;j<(int)leaves.size();++j)
			{
				const float	sz=size(merge(leaves[i]->volume,leaves[j]->volume));
				if(sz<minsize)
				{
					minsize		=	sz;
					minidx[0]	=	i;
					minidx[1]	=	j;
				}
			}
		}
		btDbvtNode*	n[]	=	{leaves[minidx[0]],leaves[minidx[1]]};
		btDbvtNode*	p	=	createnode(pdbvt,0,n[0]->volume,n[1]->volume,0);
		p->childs[0]		=	n[0];
		p->childs[1]		=	n[1];
		n[0]->parent		=	p;
		n[1]->parent		=	p;
		leaves[minidx[0]]	=	p;
		TdSwap(leaves[minidx[1]],leaves[leaves.size()-1]);
//		leaves.swap(minidx[1],leaves.size()-1); // TODO: Verify!
		leaves.pop_back();
	}
	//Todo: update childCount
}

//
static btDbvtNode*			topdown(btDbvt* pdbvt,
									tNodeArray& leaves,
									int bu_treshold)
{
	static const TdVec3 axis[]={TdVec3(1,0,0),
		TdVec3(0,1,0),
		TdVec3(0,0,1)};
	if(leaves.size()>1)
	{
		if((int)leaves.size()>bu_treshold)
		{
			const btDbvtVolume	vol=bounds(leaves);
			const TdVec3			org=vol.Center();
			tNodeArray				sets[2];
			int						bestaxis=-1;
			int						bestmidp=leaves.size();
			int						splitcount[3][2]={{0,0},{0,0},{0,0}};
			int i;
			for( i=0;i<(int)leaves.size();++i)
			{
				const TdVec3 x=leaves[i]->volume.Center()-org;
				for(int j=0;j<3;++j)
				{
					++splitcount[j][dot(x, axis[j])>0?1:0];
				}
			}
			for( i=0;i<3;++i)
			{
				if((splitcount[i][0]>0)&&(splitcount[i][1]>0))
				{
					const int midp=TdAbs(splitcount[i][0]-splitcount[i][1]); // TODO: Verify
					if(midp<bestmidp)
					{
						bestaxis=i;
						bestmidp=midp;
					}
				}
			}
			if(bestaxis>=0)
			{
				sets[0].reserve(splitcount[bestaxis][0]);
				sets[1].reserve(splitcount[bestaxis][1]);
				split(leaves,sets[0],sets[1],org,axis[bestaxis]);
			}
			else
			{
				sets[0].reserve(leaves.size()/2+1);
				sets[1].reserve(leaves.size()/2);
				for(int i=0,ni=leaves.size();i<ni;++i)
				{
					sets[i&1].push_back(leaves[i]);
				}
			}
			btDbvtNode*	node=createnode(pdbvt,0,vol,0);
			node->childs[0]=topdown(pdbvt,sets[0],bu_treshold);
			node->childs[1]=topdown(pdbvt,sets[1],bu_treshold);
			node->childs[0]->parent=node;
			node->childs[1]->parent=node;
			return(node);
		}
		else
		{
			bottomup(pdbvt,leaves);
			return(leaves[0]);
		}
	}
	//Todo: update childCount
	return(leaves[0]);
}

//
static inline btDbvtNode*	sort(btDbvtNode* n,btDbvtNode*& r)
{
	btDbvtNode*	p=n->parent;
	if(p>n)
	{
		const int		i=indexof(n);
		const int		j=1-i;
		btDbvtNode*	s=p->childs[j];
		btDbvtNode*	q=p->parent;
		if(q) q->childs[indexof(p)]=n; else r=n;
		s->parent=n;
		p->parent=n;
		n->parent=q;
		p->childs[0]=n->childs[0];
		p->childs[1]=n->childs[1];
		n->childs[0]->parent=p;
		n->childs[1]->parent=p;
		n->childs[i]=p;
		n->childs[j]=s;
		TdSwap(p->volume,n->volume);
		TdSwap(p->childCount, n->childCount);
		return(p);
	}
	return(n);
}

//
// Api
//

//
btDbvt::btDbvt()
{
	m_root		=	0;
	m_free		=	0;
	m_lkhd		=	-1;
	m_leaves	=	0;
	m_opath		=	0;
	m_eps		=   0.0f;
}

//
btDbvt::~btDbvt()
{
	clear();
}

//
void			btDbvt::clear()
{
	if(m_root)	
		recursedeletenode(this,m_root);
	delete m_free;
	m_free=0;
	m_lkhd		=	-1;
	m_stkStack.clear();
	m_opath		=	0;
	
}

//
void			btDbvt::optimizeBottomUp()
{
	if(m_root)
	{
		tNodeArray leaves;
		leaves.reserve(m_leaves);
		fetchleaves(this,m_root,leaves);
		bottomup(this,leaves);
		m_root=leaves[0];
	}
}

//
void			btDbvt::optimizeTopDown(int bu_treshold)
{
	if(m_root)
	{
		tNodeArray	leaves;
		leaves.reserve(m_leaves);
		fetchleaves(this,m_root,leaves);
		m_root=topdown(this,leaves,bu_treshold);
	}
}

//
void			btDbvt::optimizeIncremental(int passes)
{
	if(passes<0) passes=m_leaves;
	if(m_root&&(passes>0))
	{
		do	{
			btDbvtNode*		node=m_root;
			unsigned	bit=0;
			while(node->isinternal())
			{
				node=sort(node, m_root)->childs[(m_opath>>bit)&1];
				bit=(bit+1)&(sizeof(unsigned)*8-1);
			}
			update(node);
			++m_opath;
		} while(--passes);
	}
}

//
btDbvtNode*	btDbvt::insert(const btDbvtVolume& volume,void* data)
{
	btDbvtNode*	leaf=createnode(this,0,volume,data);
	insertleaf(this,m_root,leaf);
	++m_leaves;
	return(leaf);
}

//
void			btDbvt::update(btDbvtNode* leaf,int lookahead)
{
	btDbvtNode*	root=removeleaf(this,leaf);
	if(root)
	{
		if(lookahead>=0)
		{
			for(int i=0;(i<lookahead)&&root->parent;++i)
			{
				root=root->parent;
			}
		} else root=m_root;
	}
	insertleaf(this,root,leaf);
}

//
void			btDbvt::update(btDbvtNode* leaf,btDbvtVolume& volume)
{
	if (leaf->parent)
	{
		if (leaf->parent->volume.Contain(volume))
		{
			leaf->volume = volume;
			return;
		}
	}
	btDbvtNode*	root=removeleaf(this,leaf);
	if(root)
	{
		if(m_lkhd>=0)
		{
			for(int i=0;(i<m_lkhd)&&root->parent;++i)
			{
				root=root->parent;
			}
		} else root=m_root;
	}
	leaf->volume=volume;
	insertleaf(this,root,leaf);
}

//
void			btDbvt::remove(btDbvtNode* leaf)
{
	removeleaf(this,leaf);
	deletenode(this,leaf);
	--m_leaves;
}

//
int				btDbvt::maxdepth(const btDbvtNode* node)
{
	int	depth=0;
	if(node) getmaxdepth(node,1,depth);
	return(depth);
}

//
int				btDbvt::countLeaves(const btDbvtNode* node)
{
	if(node->isinternal())
		return(countLeaves(node->childs[0])+countLeaves(node->childs[1]));
	else
		return(1);
}

//
void			btDbvt::extractLeaves(const btDbvtNode* node,std::vector<const btDbvtNode*>& leaves)
{
	if(node->isinternal())
	{
		extractLeaves(node->childs[0],leaves);
		extractLeaves(node->childs[1],leaves);
	}
	else
	{
		leaves.push_back(node);
	}	
}

