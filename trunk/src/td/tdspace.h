/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#pragma once

#include <vector>

class TdSpace
{
public:
	int mType;

	std::vector<int> mOverlaps0;
	std::vector<int> mOverlaps1;
	int mOverlapIndex;

	inline void clearOverlaps()
	{
		mOverlaps0.clear();
		mOverlaps1.clear();
		mOverlapIndex = 0;
	}
};

