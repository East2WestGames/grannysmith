/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#pragma once

#include "td.h"
#include "tdmath.h"
#include "tdconstrainttype.h"

class TdConstraintContact
{
public:
	class Point
	{
	public:
		TdVec3 mPos0;
		TdVec3 mPos1;
		float mTarget;
		float mImpulse;
		float mAff0;
		float mAff1;
		TdVec3 mAngImp0;
		TdVec3 mAngImp1;
	};
	
	int mTmp;
	
	int mBody0;
	int mBody1;
	
	TdVec3 mNormal0;
	TdVec3 mNormal1;

	int mCount;
	Point mPoints[4];

	TdVec3 mPos0;
	TdVec3 mPos1;
	TdVec3 mPerp0[2];
	TdVec3 mPerp1[2];
	float mFricTarget[3];
	float mFricImpulse[3];
	float mFricAff0[3];
	float mFricAff1[3];
	TdVec3 mPerpAngImp0[2];
	TdVec3 mPerpAngImp1[2];

	bool mSaturated;
	tdConstraintPersistent* mPersistent;
};


class TdConstraintTypeContact : public TdConstraintTypeBase<tdConstraintContactBB, TdConstraintContact>
{
public:
	virtual void init(TdSolver* solver, const class TdSolverState* state, int start, int end);
	virtual void solve(TdSolver* solver, const class TdSolverState* state, int start, int end);
	virtual void postSolve(TdSolver* solver);
};


