/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#pragma once

#include "tdconfig.h"
#include "tdgjk.h"
#include "tddebug.h"

class TdMpr
{
public:
	const TdCso* mCso;
	TdVec3 mDir;
	TdSimplexPoint mPortal[3];

	float mEps;
	int mMaxIterations;


	inline TdMpr() : 
	mCso(NULL), mEps(0.001f), mMaxIterations(32) 
	{
	}

	inline TdMpr(float tolerance, int maxIterations) :
	mCso(NULL), mEps(tolerance), mMaxIterations(maxIterations) 
	{
	}
	
	
	//Init MPR with a GJK in penetration. Initial portal will
	//be one of the four triangles in the tetrahedron
	inline void init(const TdGjk* gjk, const TdVec3& dir)
	{
		mCso = gjk->mCso;
		mDir = dir;
		TD_DEBUG_TEXT("Init from GJK");
		getPortalFromSimplex(gjk);
	}

	//Init MPR with search direction. An initial portal will be constructed
	//based on this search direction.
	inline void init(const TdCso* cso, const TdVec3& dir)
	{
		mCso = cso;
		mDir = dir;
		TD_DEBUG_TEXT("Init from CSO with direction");
		generatePortal();
	}
	
	//Init MPR with unknown search direction. The directino from geometric
	//center of the CSO to the origin will be used as search direction.
	inline void init(const TdCso* cso)
	{
		mCso = cso;
		mDir = -normalize(mCso->getMidPoint());
		TD_DEBUG_TEXT("Init from CSO");
		generatePortal();
	}
	
	//Init MPR with a known portal
	inline void init(const TdCso* cso, const TdVec3& dir, const TdSimplexPoint& p0, const TdSimplexPoint& p1, const TdSimplexPoint& p2)
	{
		mCso = cso;
		mDir = dir;
		mPortal[0] = p0;
		mPortal[1] = p1;
		mPortal[2] = p2;
		TD_DEBUG_TEXT("Init with explicit portal");
	}
	
	//Convert a 4-simplex from GJK overlap into portal. Choose
	//the one valid triangle in the tetrahedron.
	inline void getPortalFromSimplex(const TdGjk* gjk)
	{
		//Test the three top triangles
		mPortal[0] = gjk->mSimplex[0];
		mPortal[1] = gjk->mSimplex[1];
		mPortal[2] = gjk->mSimplex[3];
		if (isPortal())
			return;

		mPortal[0] = gjk->mSimplex[1];
		mPortal[1] = gjk->mSimplex[2];
		mPortal[2] = gjk->mSimplex[3];
		if (isPortal())
			return;

		mPortal[0] = gjk->mSimplex[2];
		mPortal[1] = gjk->mSimplex[0];
		mPortal[2] = gjk->mSimplex[3];
		if (isPortal())
			return;

		//Must be the bottom triangle (which is in reverse order)
		mPortal[0] = gjk->mSimplex[1];
		mPortal[1] = gjk->mSimplex[0];
		mPortal[2] = gjk->mSimplex[2];
	}
	
	//Generate initial portal for given direction
	//Note that initial portal triangle can be behind origin
	//This function is normally not called, since we often have a 4-simplex
	//from an overlap query.
	void generatePortal()
	{
		mCso->getSupport(mDir, mPortal[0]);
		TD_DEBUG_TEXT("Generate portal");
		TD_DEBUG_SIMPLEX(mPortal, 1);
		TD_DEBUG_PAUSE();
		
		expandPortal1();
		for(int i=0; i<8; i++)
		{
			if (!isPortal())
				adjustPortal();
			else
				return;
		}
		if (!isPortal())
			TD_WARNING("Failed to create portal");
	}
	
	//Expand the point at mPortal[0] to a line
	void expandPortal1()
	{
		TdVec3 dir2 = -(mPortal[0].cs - mDir*dot(mDir, mPortal[0].cs)); //VERIFY
		mCso->getSupport(mDir, mPortal[1]);
		TD_DEBUG_SIMPLEX(mPortal, 2);
		TD_DEBUG_PAUSE();
		expandPortal2();
	}
	
	//Expand the line from mPortal[0] to mPortal[1] into a triangle 
	void expandPortal2()
	{
		TdVec3 dir3 = cross(mDir, mPortal[1].cs - mPortal[0].cs);
		if (dot(mPortal[0].cs, dir3) > 0.0f)
			dir3 = -dir3;
		mCso->getSupport(dir3, mPortal[2]);

		//Make sure normal is same direction as search direction
		if (dot(mDir, cross(mPortal[1].cs - mPortal[0].cs, mPortal[2].cs - mPortal[1].cs)) < 0.0f)
			TdSwap(mPortal[0], mPortal[1]);

		TD_DEBUG_SIMPLEX(mPortal, 3);
		TD_DEBUG_PAUSE();
	}
	
	//Adjust an invalid portal to make it valid. Note that this is
	//no guarantee the portal will actually be valid afterwards.
	//Calling multiple times (until isPortal returns true) should
	//find a valid portal.
	void adjustPortal()
	{
		TD_DEBUG_TEXT("adjustPortal");
		TdVec3 e0 = mPortal[1].cs - mPortal[0].cs;
		TdVec3 e1 = mPortal[2].cs - mPortal[1].cs;
		TdVec3 e2 = mPortal[0].cs - mPortal[2].cs;
		float d0 = dot(cross(e0, mDir), mPortal[0].cs);
		float d1 = dot(cross(e1, mDir), mPortal[1].cs);
		float d2 = dot(cross(e2, mDir), mPortal[2].cs);
		if (d0 < 0.0f)
		{
			if (d1 < 0.0f)
			{
				mPortal[0] = mPortal[1];
				expandPortal1();
			}
			else if (d2 < 0.0f)
			{
				expandPortal1();
			}
			else
				expandPortal2();
		}
		else if (d1 < 0.0f)
		{
			if (d0 < 0.0f)
			{
				mPortal[0] = mPortal[1];
				expandPortal1();
			}
			else if (d2 < 0.0f)
			{
				mPortal[0] = mPortal[2];
				expandPortal1();
			}
			else
			{
				mPortal[0] = mPortal[1];
				mPortal[1] = mPortal[2];
				expandPortal2();
			}
		}
		else if (d2 < 0.0f)
		{
			if (d0 < 0.0f)
			{
				mPortal[0] = mPortal[2];
				expandPortal1();
			}
			else if (d1 < 0.0f)
			{
				mPortal[0] = mPortal[1];
				expandPortal1();
			}
			else
			{
				mPortal[1] = mPortal[0];
				mPortal[0] = mPortal[2];
				expandPortal2();
			}
		}
	}
	
	//Return true if ray through origin passes through portal
	//Note that portal triangle can also be behind the origin
	bool isPortal() const
	{
		TdVec3 e0 = mPortal[1].cs - mPortal[0].cs;
		TdVec3 e1 = mPortal[2].cs - mPortal[1].cs;
		TdVec3 e2 = mPortal[0].cs - mPortal[2].cs;
		float d0 = dot(cross(e0, mDir), mPortal[0].cs);
		float d1 = dot(cross(e1, mDir), mPortal[1].cs);
		float d2 = dot(cross(e2, mDir), mPortal[2].cs);
		return d0 >= 0.0f && d1 >= 0.0f && d2 >= 0.0f;
	}

	//This will run MPR iteratively to refine the search direction
	//based on the previous hit normal. It does not reconstruct the
	//entire portal from scratch, but starts with previous one.
	//This is a good backup routine for EPA, since we know an approximate
	//direction when EPA fails.
	void estimatePenetrationDepth()
	{
		TD_DEBUG_TEXT("estimatePenetrationDepth");
		int i;
		for(i=0; i<mMaxIterations; i++)
		{
			TD_DEBUG_PUSH();
			TD_DEBUG_SIMPLEX(mPortal, 3);
			TD_DEBUG_PAUSE();
			TD_DEBUG_POP();
			int c = 0;
			while(!isPortal() && c<mMaxIterations)
			{
				adjustPortal();
				c++;
			}
			TdVec3 n = trackSurface();
			if (dot(mDir, n) > 0.99f)
				break;
			mDir = n;
		}
	}
	

	//This is the main MPR algorithm, which performs a raycast
	//from the origin in the mDir direction to find the surface of the CSO
	//There needs to be a valid portal before this could be called
	TdVec3 trackSurface()
	{
		TdSimplexPoint sp;
		TdVec3 n;
		int i=0;
		
		for(i=0; i<mMaxIterations; i++)
		{
			TD_DEBUG_SCOPE();
			TD_DEBUG_TEXT("trackSurface iteration %i", i);
			TD_DEBUG_SIMPLEX(mPortal, 3);
			TD_DEBUG_PAUSE();
			TdVec3 e0 = mPortal[1].cs - mPortal[0].cs;
			TdVec3 e1 = mPortal[2].cs - mPortal[1].cs;
			n = normalize(cross(e0, e1));

			//Get the new point in search direction
			mCso->getSupport(n, sp);

			TD_DEBUG_POINT(sp.cs);
			TD_DEBUG_PAUSE();

			//Early out if distance is short
			float dist = dot(n, sp.cs - mPortal[0].cs);
			if (dist < mEps)
				break;

			TdVec3 d0 = cross(sp.cs, mPortal[0].cs - sp.cs);
			TdVec3 d1 = cross(sp.cs, mPortal[1].cs - sp.cs);
			TdVec3 d2 = cross(sp.cs, mPortal[2].cs - sp.cs);

			TdVec3 tt = mDir;

			if (dot(tt, d0)>=0.0f && dot(tt, d1)<=0.0f)
			{
				mPortal[2] = sp;
			}
			else if (dot(tt, d1)>=0.0f && dot(tt, d2)<=0.0f)
			{
				mPortal[0] = mPortal[1];
				mPortal[1] = mPortal[2];
				mPortal[2] = sp;
			}
			else if (dot(tt, d2)>=0.0f && dot(tt, d0)<=0.0f)
			{
				mPortal[1] = mPortal[0];
				mPortal[0] = mPortal[2];
				mPortal[2] = sp;
			}
			else
				break;
		}
		return n;
	}

	//Return distance from origin to the plane defined by current portal
	//in the search direction (mDir)
	inline float getDistanceToPortal(float& t0, float& t1)
	{
		TdVec3 edge0 = mPortal[1].cs - mPortal[0].cs;
		TdVec3 edge1 = mPortal[2].cs - mPortal[0].cs;
		TdVec3 N = cross(edge0, edge1);
		TdVec3 B = -mPortal[0].cs;
		TdVec3 BxDir = cross(B, mDir);
		float det = -dot(mDir, N);
		float oneOverDet = 1.0f / det;
		t0 = dot(edge1, BxDir) * oneOverDet;
		t1 = -dot(edge0, BxDir) * oneOverDet;
		return dot(B, N) * oneOverDet;
	}

	//Calculate configuration space and local space points
	inline void getPoints(float t0, float t1, TdVec3* cs, TdVec3* p0, TdVec3* p1)
	{
		if (cs)
			*cs = mPortal[0].cs + (mPortal[1].cs-mPortal[0].cs)*t0 + (mPortal[2].cs-mPortal[0].cs)*t1;
		if (p0)
			*p0 = mPortal[0].p0 + (mPortal[1].p0-mPortal[0].p0)*t0 + (mPortal[2].p0-mPortal[0].p0)*t1;
		if (p1)
			*p1 = mPortal[0].p1 + (mPortal[1].p1-mPortal[0].p1)*t0 + (mPortal[2].p1-mPortal[0].p1)*t1;
	}
};

