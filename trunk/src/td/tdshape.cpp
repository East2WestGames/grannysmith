/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#include "tdshape.h"


void tdShapeInitSphere(tdShape* shape, float radius)
{
	shape->type = TD_SHAPE_TYPE_SPHERE;
	shape->sphere.radius = radius;
}


void tdShapeInitBox(tdShape* shape, const tdVec* size)
{
	shape->type = TD_SHAPE_TYPE_BOX;
	shape->box.size = *size;
}


void tdShapeInitCylinder(tdShape* shape, tdAxis axis, float radius, float length)
{
	shape->type = TD_SHAPE_TYPE_CYLINDER;
	shape->cylinder.axis = axis;
	shape->cylinder.radius = radius;
	shape->cylinder.length = length;
}


void tdShapeInitCone(tdShape* shape, tdAxis axis, float radius, float length)
{
	shape->type = TD_SHAPE_TYPE_CONE;
	shape->cone.axis = axis;
	shape->cone.radius = radius;
	shape->cone.length = length;
}


void tdShapeInitCapsule(tdShape* shape, tdAxis axis, float radius, float length)
{
	shape->type = TD_SHAPE_TYPE_CAPSULE;
	shape->capsule.axis = axis;
	shape->capsule.radius = radius;
	shape->capsule.length = length;
}


void tdShapeInitHull(tdShape* shape, int count, int stride, const float* vertices)
{
	shape->type = TD_SHAPE_TYPE_HULL;
	shape->hull.count = count;
	shape->hull.stride = stride;
	shape->hull.vertices = vertices;
}


void tdShapeSupport(const tdShape* shape, const tdVec* direction, tdVec* point)
{
	*point = TdShapeGetSupport(shape, *direction);
}


