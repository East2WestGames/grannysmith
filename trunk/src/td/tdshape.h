/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#pragma once

#include "td.h"
#include "tdconfig.h"
#include "tdmath.h"


inline TdVec3 TdShapeGetSupportPoint(const tdShape* shape, const TdVec3& dir)
{
	return TdVec3(0.0f, 0.0f, 0.0f);
}


inline TdVec3 TdShapeGetSupportSphere(const tdShape* shape, const TdVec3& dir)
{
	return TdVec3(normalize(dir)*shape->sphere.radius);
}


inline TdVec3 TdShapeGetSupportBox(const tdShape* shape, const TdVec3& dir)
{
	return TdVec3(dir.x<0.0f ? -shape->box.size.x : shape->box.size.x, 
				   dir.y<0.0f ? -shape->box.size.y : shape->box.size.y, 
				   dir.z<0.0f ? -shape->box.size.z : shape->box.size.z);
}


inline TdVec3 TdShapeGetSupportCylinder(const tdShape* shape, const TdVec3& dir)
{
	TdVec3 p;
	if (shape->cylinder.axis == TD_AXIS_X)
	{
		p = normalize(TdVec3(0.0f, dir.y, dir.z))*shape->cylinder.radius;
		p.x = dir.x >= 0.0f ? shape->cylinder.length : -shape->cylinder.length;
		return p;
	}
	else if (shape->cylinder.axis == TD_AXIS_Y)
	{
		p = normalize(TdVec3(dir.x, 0.0f, dir.z))*shape->cylinder.radius;
		p.y = dir.y >= 0.0f ? shape->cylinder.length : -shape->cylinder.length;
		return p;
	}
	else
	{
		p = normalize(TdVec3(dir.x, dir.y, 0.0f))*shape->cylinder.radius;
		p.z = dir.z >= 0.0f ? shape->cylinder.length : -shape->cylinder.length;
		return p;
	}				
}


inline TdVec3 TdShapeGetSupportCone(const tdShape* shape, const TdVec3& dir)
{
	TdVec3 d;
	TdVec3 p0;
	TdVec3 p1;
	switch(shape->cone.axis)
	{
		case TD_AXIS_X:
		d=normalize(TdVec3(0.0f, dir.y, dir.z));
		p0.set(0.0f, d.y*shape->cone.radius, d.z*shape->cone.radius);
		p1.set(shape->cone.length, 0.0f, 0.0f);
		break;
		case TD_AXIS_Y:
		d=normalize(TdVec3(dir.x, 0.0f, dir.z));
		p0.set(d.x*shape->cone.radius, 0.0f, d.z*shape->cone.radius);
		p1.set(0.0f, shape->cone.length, 0.0f);
		break;
		default:
		d=normalize(TdVec3(dir.x, dir.y, 0.0f));
		p0.set(d.x*shape->cone.radius, d.y*shape->cone.radius, 0.0f);
		p1.set(0.0f, 0.0f, shape->cone.length);
		break;
	}
	if (dot(dir, p1-p0) < 0.0f)
		return p0;
	else
		return p1;
}


inline TdVec3 TdShapeGetSupportCapsule(const tdShape* shape, const TdVec3& dir)
{
	TdVec3 p;
	switch(shape->capsule.axis)
	{
		case TD_AXIS_X:
		if (dir.x >= 0.0f)
			p.set(shape->capsule.length,0,0);
		else
			p.set(-shape->capsule.length,0,0);
		break;
		case TD_AXIS_Y:
		if (dir.y >= 0.0f)
			p.set(0,shape->capsule.length,0);
		else
			p.set(0,-shape->capsule.length,0);
		break;
		default:
		if (dir.z >= 0.0f)
			p.set(0,0,shape->capsule.length);
		else
			p.set(0,0,-shape->capsule.length);
		break;
	}
	if (shape->capsule.radius > 0.0f)
		p+=normalize(dir)*shape->capsule.radius;
	return p;
}

/*
#define SIMD_ALIGN(type) type __attribute__ ((aligned(16)))

inline QiPoint tdShapeGetSupportHullSSE(const tdShape* shape, const QiVec& dir)
{
	int c = shape->hull.count;
	int supportFeature = 0;
	SIMD_ALIGN(float) m = -QI_FLOAT_MAX;

	static SIMD_ALIGN(float) datax[256];
	static SIMD_ALIGN(float) datay[256];
	static SIMD_ALIGN(float) dataz[256];

	static bool first = true;
	if (first)
	{
		for(int s=0; s<c; s++)
		{
			QiVec &v = (QiVec&)(((char*)shape->hull.vertices)[s*shape->hull.stride]);
			datax[s] = v.x;
			datay[s] = v.y;
			dataz[s] = v.z;
		}
		first = false;
	}
	
	SIMD_ALIGN(float) res[4];
	SIMD_ALIGN(float) dirx = dir.x;
	SIMD_ALIGN(float) diry = dir.y;
	SIMD_ALIGN(float) dirz = dir.z;
	SIMD_ALIGN(int) neg[4];

	int i;
	__m128 dx = _mm_load1_ps(&dirx);
	__m128 dy = _mm_load1_ps(&diry);
	__m128 dz = _mm_load1_ps(&dirz);
	__m128 mm = _mm_load1_ps(&m);
	for(i=0; i<c-3; i+=4)
	{
		__m128 x = _mm_load_ps(datax+i);
		__m128 y = _mm_load_ps(datay+i);
		__m128 z = _mm_load_ps(dataz+i);
		__m128 dotx = _mm_mul_ps(x, dx);
		__m128 doty = _mm_mul_ps(y, dy);
		__m128 dotz = _mm_mul_ps(z, dz);
		__m128 sum = _mm_add_ps(dotx, doty);
		sum = _mm_add_ps(sum, dotz);
		__m128 q = _mm_sub_ps(mm, sum);
		_mm_store_ps((float*)neg, q);
		int a = neg[0] | neg[1] | neg[2] | neg[3];
		if (a & 0x80000000)
		{
			_mm_store_ps(res, sum);
			for(int j=0; j<4; j++)
			{
				if (res[j] > m)
				{
					m = res[j];
					supportFeature = i+j;
				}
			}
			mm = _mm_load1_ps(&m);
		}
	}

	for(; i<c; i++)
	{
		QiVec &v = (QiVec&)(((char*)shape->hull.vertices)[i*shape->hull.stride]);
		float dot = v.dot(dir);
		if (dot > m)
		{
			m = dot;
			supportFeature = i;
		}
	}
	
	QiPoint &p = (QiPoint&)(((char*)shape->hull.vertices)[supportFeature*shape->hull.stride]);
	return p;
}
*/


inline TdVec3 TdShapeGetSupportHull(const tdShape* shape, const TdVec3& dir)
{
//	return tdShapeGetSupportHullSSE(shape, dir);
	
	int supportFeature = 0;
	float m = dot(dir, *((TdVec3*)shape->hull.vertices));
	for(int i=1; i<shape->hull.count; i++)
	{
		TdVec3 &v = (TdVec3&)(((char*)shape->hull.vertices)[i*shape->hull.stride]);
		float dt = dot(dir, v);
		if (dt > m)
		{
			m = dt;
			supportFeature = i;
		}
	}
	TdVec3 &p = (TdVec3&)(((char*)shape->hull.vertices)[supportFeature*shape->hull.stride]);
	return p;
}


inline TdVec3 TdShapeGetSupport(const tdShape* shape, const TdVec3& dir)
{
	switch(shape->type)
	{
		case TD_SHAPE_TYPE_SPHERE:
		return TdShapeGetSupportSphere(shape, dir);
		
		case TD_SHAPE_TYPE_BOX:
		return TdShapeGetSupportBox(shape, dir);

		case TD_SHAPE_TYPE_CYLINDER:
		return TdShapeGetSupportCylinder(shape, dir);

		case TD_SHAPE_TYPE_CONE:
		return TdShapeGetSupportCone(shape, dir);

		case TD_SHAPE_TYPE_CAPSULE:
		return TdShapeGetSupportCapsule(shape, dir);
		
		case TD_SHAPE_TYPE_HULL:
		return TdShapeGetSupportHull(shape, dir);
		
		default:
		return TdVec3(0.0f, 0.0f, 0.0f);		
	}
}

inline TdVec3 TdShapeGetMidPoint(const tdShape* shape)
{
	switch(shape->type)
	{
		case TD_SHAPE_TYPE_POINT:
		return TdVec3(0.0f, 0.0f, 0.0f);
		
		case TD_SHAPE_TYPE_SPHERE:
		return TdVec3(0.0f, 0.0f, 0.0f);
		
		case TD_SHAPE_TYPE_BOX:		
		return TdVec3(0.0f, 0.0f, 0.0f);

		case TD_SHAPE_TYPE_CYLINDER:
		return TdVec3(0.0f, 0.0f, 0.0f);

		case TD_SHAPE_TYPE_CONE:
		switch(shape->cone.axis)
		{
			case TD_AXIS_X:
			return TdVec3(shape->cone.length*0.5f, 0.0f, 0.0f);
			case TD_AXIS_Y:
			return TdVec3(0.0f, shape->cone.length*0.5f, 0.0f);
			default:
			return TdVec3(0.0f, 0.0f, shape->cone.length*0.5f);
		}

		case TD_SHAPE_TYPE_CAPSULE:
		return TdVec3(0.0f, 0.0f, 0.0f);

		case TD_SHAPE_TYPE_HULL:
		{
			TdVec3 mid;
			float div = 1.0f / shape->hull.count;
			for(int i=0; i<shape->hull.count; i++)
				mid += (TdVec3&)(((char*)shape->hull.vertices)[i*shape->hull.stride]) * div;
			return mid;
		}
	}
	return TdVec3();
}

