/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#ifndef TD_H
#define TD_H

#include <stdlib.h>

#ifdef __cplusplus
extern "C"
{
#endif

typedef void*(*tdMalloc)(size_t);
typedef void(*tdFree)(void*);

int tdContextCreate(tdMalloc customAllocator, tdFree customFree);
void tdContextSetMaxIterations(int context, int count);
void tdContextSetTolerance(int context, float tolerance);
void tdContextDestroy(int context);

void tdAsyncInit(int maxThreadCount);
void tdAsyncShutdown();
void tdAsyncReset();
void tdAsyncIncThreadCount();
void tdAsyncDecThreadCount();
void tdAsyncSetThreadCount(int count);
void tdAsyncFlush();
void tdAsyncWait(int job);
void tdAsyncWaitAll();

typedef struct
{
	float x,y,z;
} tdVec;

typedef struct
{
	float x,y,z,w;
} tdQuat;


///////////////////////////////////////////////////////////////

enum tdAxis
{
	TD_AXIS_X,
	TD_AXIS_Y,
	TD_AXIS_Z
};

enum tdShapeType
{
	TD_SHAPE_TYPE_POINT = 0,
	TD_SHAPE_TYPE_SPHERE,
	TD_SHAPE_TYPE_BOX,
	TD_SHAPE_TYPE_CYLINDER,
	TD_SHAPE_TYPE_CONE,
	TD_SHAPE_TYPE_CAPSULE,
	TD_SHAPE_TYPE_HULL
};

typedef struct
{
	float radius;
} tdShapeSphere;

typedef struct
{
	tdVec size;
} tdShapeBox;

typedef struct
{
	tdAxis axis;	
	float radius;
	float length;
}  tdShapeCylinder;

typedef struct
{
	int count;
	int stride;
	const float* vertices;
}  tdShapeHull;

typedef struct
{
	tdShapeType type;
	union
	{
		tdShapeSphere sphere;
		tdShapeBox box;
		tdShapeCylinder cylinder;
		tdShapeCylinder cone;
		tdShapeCylinder capsule;
		tdShapeHull hull;
	};
}  tdShape;

void tdShapeInitSphere(tdShape* shape, float radius);
void tdShapeInitBox(tdShape* shape, const tdVec* size);
void tdShapeInitCylinder(tdShape* shape, tdAxis axis, float radius, float length);
void tdShapeInitCone(tdShape* shape, tdAxis axis, float radius, float length);
void tdShapeInitCapsule(tdShape* shape, tdAxis axis, float radius, float length);
void tdShapeInitHull(tdShape* shape, int count, int stride, const float* vertices);

void tdShapeSupport(const tdShape* shape, const tdVec* direction, tdVec* point);


///////////////////////////////////////////////////////////////


enum tdTransformType
{
	TD_TRANSFORM_TYPE_P,
	TD_TRANSFORM_TYPE_M
};

typedef struct
{
	tdTransformType type;
	float m[16];
} tdTransform;

void tdTransformInit(tdTransform* transform);
void tdTransformInitP(tdTransform* transform, const tdVec* point);
void tdTransformInitPQ(tdTransform* transform, const tdVec* point, const tdQuat* quat);
void tdTransformInitM16(tdTransform* transform, const float matrix[16]);


///////////////////////////////////////////////////////////////

//Query flags
#define TD_QUERY_COMPUTE_POINTS			0x0001
#define TD_QUERY_COMPUTE_NORMAL			0x0002
#define TD_QUERY_HANDLE_PENETRATION		0x0010
#define TD_QUERY_USE_MAX_DISTANCE		0x0020
#define TD_QUERY_LOAD_SIMPLEX			0x0040
#define TD_QUERY_STORE_SIMPLEX			0x0080

//Result status flags
#define TD_RESULT_HAS_DISTANCE			0x0001
#define TD_RESULT_HAS_NORMAL			0x0002
#define TD_RESULT_HAS_POINTS			0x0004
#define TD_RESULT_SEPARATION			0X0010
#define TD_RESULT_PENETRATION			0x0020
#define TD_RESULT_MAX_DISTANCE_REACHED	0x0100
#define TD_RESULT_MISS					0x0200
#define TD_RESULT_UNRELIABLE			0x4000
#define TD_RESULT_FAILED				0x8000

typedef struct
{
	int count;
	float p0[12];
	float p1[12];
} tdSimplex;

typedef struct
{
	int flags;
	const tdShape* shape0;
	const tdShape* shape1;
	tdTransform transform;
	float maxDistance;
	tdSimplex* simplex;
} tdDistanceQuery;

typedef struct
{
	int status;
	float distance;
	tdVec normal;
	tdVec point0;
	tdVec point1;
} tdDistanceResult;

void tdDistance(int context, const tdDistanceQuery* query, tdDistanceResult* result);
int tdDistanceAsync(int context, const tdDistanceQuery* query, tdDistanceResult* result);


typedef struct
{
	int flags;
	const tdShape* shape0;
	const tdShape* shape1;
	tdTransform transform;
	tdVec direction;
	float maxDistance;
} tdShapecastQuery;

typedef struct
{
	int status;
	float distance;
	tdVec normal;
	tdVec point0;
	tdVec point1;
} tdShapecastResult;

void tdShapecast(int context, const tdShapecastQuery* query, tdShapecastResult* result);
int tdShapecastAsync(int context, const tdShapecastQuery* query, tdShapecastResult* result);


typedef struct
{
	int flags;
	const tdShape* shape0;
	const tdShape* shape1;
	tdTransform transform;
	tdSimplex* simplex;
} tdOverlapQuery;

typedef struct
{
	int status;
} tdOverlapResult;

void tdOverlap(int context, const tdOverlapQuery* query, tdOverlapResult* result);
int tdOverlapAsync(int context, const tdOverlapQuery* query, tdOverlapResult* result);


typedef struct
{
	int flags;
	const tdShape* shape;
	tdVec point;
	tdVec direction;
	float maxDistance;
} tdRaycastQuery;

typedef struct
{
	int status;
	float distance;
	tdVec normal;
	tdVec point;
} tdRaycastResult;

void tdRaycast(int context, const tdRaycastQuery* query, tdRaycastResult* result);
int tdRaycastAsync(int context, const tdRaycastQuery* query, tdRaycastResult* result);


/////////////////////////////////////////////////////////////////////////////////
// Manifold
/////////////////////////////////////////////////////////////////////////////////

typedef struct 
{
	int count;
	tdVec normal0;
	tdVec normal1;
	tdVec points0[4];
	tdVec points1[4];
} tdContactManifold; // 124 bytes

typedef struct
{
	int flags;
	tdShape* shape0;
	tdShape* shape1;
	tdTransform transform;
	float maxDistance;
	int maxCount;
	tdSimplex* simplex;
} tdManifoldQuery;

typedef struct 
{
	int status;
} tdManifoldResult;

void tdManifold(int context, tdContactManifold* manifold, tdManifoldQuery* query, tdManifoldResult* result);
int tdManifoldAsync(int context, tdContactManifold* manifold, tdManifoldQuery* query, tdManifoldResult* result);


/////////////////////////////////////////////////////////////////////////////////
//Bounding boxes
/////////////////////////////////////////////////////////////////////////////////


typedef struct
{
	tdVec lower;
	tdVec upper;
} tdAabb;


typedef struct
{
	float matrix[9];
	tdVec size;
} tdObb;


typedef struct 
{
	const tdShape* shape;
	tdTransform transform;
	tdVec sweep;
	float padding;
} tdBoundingBoxQuery;

void tdBoundingBoxAabb(int context, const tdBoundingBoxQuery* query, tdAabb* aabb);
int tdBoundingBoxAabbAsync(int context, const tdBoundingBoxQuery* query, tdAabb* aabb);

void tdBoundingBoxObb(int context, const tdBoundingBoxQuery* query, tdObb* obb);
int tdBoundingBoxObbAsync(int context, const tdBoundingBoxQuery* query, tdObb* obb);


/////////////////////////////////////////////////////////////////////////////////
// Spaces
/////////////////////////////////////////////////////////////////////////////////

#define TD_SPACE_DYNAMIC_PARTICLE	1
#define TD_SPACE_STATIC_PARTICLE	2
#define TD_SPACE_DYNAMIC_AABB		3
#define TD_SPACE_STATIC_AABB		4

typedef struct 
{
	int type;
	float particleSize;
} tdSpace;

//Manage spaces
int tdSpaceCreate(const tdSpace* space);
void tdSpaceDestroy(int space);
void tdSpaceOptimize(int space, int iterations);
void tdSpaceClear(int space);

//Manage volumes
int tdSpaceInsertAabb(int space, const tdAabb* volume, int userData);
int tdSpaceInsertParticle(int space, const tdVec* position, int userData);
void tdSpaceUpdateAabb(int space, int aabb, const tdAabb* volume);
void tdSpaceRemoveAabb(int space, int aabb);

//Query
void tdSpaceOverlapSpace(int context, int space0, int space1);
void tdSpaceOverlapAabb(int context, int space0, const tdAabb* aabb);
void tdSpaceOverlapObb(int context, int space0, const tdObb* obb);
void tdSpaceOverlapLine(int context, int space0, const tdVec* start, const tdVec* end);

//Fetch results
int tdSpaceFetchOverlaps(int context, int space, int* userData0, int* userData1, int maxCount);

/////////////////////////////////////////////////////////////////////////////////
// Dynamics
/////////////////////////////////////////////////////////////////////////////////

//Features. Use with tdSolverEnable and tdSolverDisable.
#define TD_SOLVER_FEATURE_CONTACT_STICKY		0x0000
#define TD_SOLVER_FEATURE_COUNT					0x0001

//Parameters. Use with tdSolverSetParam and tdSolverGetParam.
#define TD_SOLVER_PARAM_MAX_IMPULSE 			0
#define TD_SOLVER_PARAM_MIN_IMPULSE 			1
#define TD_SOLVER_PARAM_FRICTION 				2
#define TD_SOLVER_PARAM_RESTITUTION 			3
#define TD_SOLVER_PARAM_CONTACT_OFFSET 			4
#define TD_SOLVER_PARAM_STRETCH_DAMPING		 	5
#define TD_SOLVER_PARAM_STRETCH_STIFFNESS		6
#define TD_SOLVER_PARAM_BEND_DAMPING		 	7
#define TD_SOLVER_PARAM_BEND_STIFFNESS			8
#define TD_SOLVER_PARAM_FLUID_VISCO_ELASTICITY	9
#define TD_SOLVER_PARAM_FLUID_PARTICLE_SIZE		10
#define TD_SOLVER_PARAM_COUNT					11

//Max interacting fluid particles 
#define TD_MAX_FLUID_NEIGHBORS (32)

typedef struct 
{
	int flags;
	float invMass;
	tdVec invInertia;
	tdVec pos;
	tdQuat rot;
	tdVec linVel;
	tdVec angVel;
	int internal;
	float force;
} tdBody;

void tdBodySetMassAsSphere(tdBody* body, float mass, float radius);
void tdBodySetMassAsBox(tdBody* body, float mass, const tdVec* halfExtent);
void tdBodySetMassAsInfiniteInertia(tdBody* body, float mass);
//Maybe void tdBodyApplyImpulse(tdBody* body, const tdVec* pos, const tdVec* impulse);

typedef struct
{
	float invMass;
	tdVec pos;
	tdVec vel;
	int internal;
} tdParticle;

void tdParticleSetMass(tdParticle* particle, float mass);
//Maybe void tdParticleApplyImpulse(const tdParticlePos* pos, const tdVec* impulse);


typedef struct
{
	int count;
	tdParticle* particles[4];
	float weights[4];
} tdParticlePos;

void tdParticlePosInitWeights(tdParticlePos* particlePos, const tdVec* point);

typedef struct
{
	int status;
	float p0[3];
	float p1[3];
	float d0[3];
	float d1[3];
	float n0[3];
	float n1[3];
	//float imp[7];
	int count;
} tdConstraintPersistent;


// Constraints ////////////////////////////////////////////////

typedef struct 
{
	tdBody* bodies[2];
	tdContactManifold manifold;
	tdConstraintPersistent* persistent;
} tdConstraintContactBB;

typedef struct 
{
	tdBody* body;
	tdVec bodyPos;
	tdParticlePos particlePos;
	tdVec normal;
} tdConstraintContactBP;

typedef struct
{
	tdBody* body;
	tdVec bodyPos;
	tdParticlePos particlePos;
} tdConstraintJointBP;

typedef struct 
{
	tdParticlePos particlePos[2];
	tdVec normal;
} tdConstraintContactPP;

typedef struct 
{
	tdParticle* particles[3];
	float extents[3];
	float edges[3];
} tdConstraintParticleTriangle;

void tdConstraintParticleTriangleSetShape(tdConstraintParticleTriangle* triangle);

typedef struct 
{
	tdParticle* particles[4];
	float extents[4];
	float edges[6];
} tdConstraintParticleTetra;

typedef struct 
{
	tdParticle* particles[3];
	float distance[2];
} tdConstraintParticleString;

void tdConstraintParticleTetraSetShape(tdConstraintParticleTetra* tetra);

typedef struct 
{
	tdBody* body[2];
	tdVec bodyPos[2];
	float distance;
} tdConstraintDistanceBB;

typedef struct 
{
	tdBody* body;
	tdVec bodyPos;
	tdParticle* particlePos;
	float distance;
} tdConstraintDistanceBP;

typedef struct 
{
	tdParticle* particles[2];
	float distance;
} tdConstraintDistancePP;

typedef struct 
{
	tdParticle* particle;
	tdParticle* neighbors[TD_MAX_FLUID_NEIGHBORS];
	int neighborCount;
} tdConstraintFluid;

int tdSolverCreate();
void tdSolverDestroy(int solver);
void tdSolverSetIterationCount(int solver, int count);
int tdSolverGetIterationCount(int solver);

void tdSolverPushState(int solver);
void tdSolverPopState(int solver);
void tdSolverResetState(int solver);

void tdSolverSetLinAcc(int solver, const tdVec* linAcc);
void tdSolverSetAngAcc(int solver, const tdVec* angAcc);
void tdSolverGetLinAcc(int solver, tdVec* linAcc);
void tdSolverGetAngAcc(int solver, tdVec* angAcc);

void tdSolverSetParam(int solver, int param, float value);
float tdSolverGetParam(int solver, int param);

void tdSolverEnable(int solver, int feature);
void tdSolverDisable(int solver, int feature);
int tdSolverIsEnabled(int solver, int feature);

void tdSolverInsertBody(int solver, tdBody* body);
void tdSolverInsertParticle(int solver, tdParticle* particle);

void tdSolverInsertContactBB(int solver, const tdConstraintContactBB* constraint);
void tdSolverInsertContactBP(int solver, const tdConstraintContactBP* constraint);
void tdSolverInsertContactPP(int solver, const tdConstraintContactPP* constraint);

void tdSolverInsertDistanceBB(int solver, const tdConstraintDistanceBB* constraint);
void tdSolverInsertDistanceBP(int solver, const tdConstraintDistanceBP* constraint);
void tdSolverInsertDistancePP(int solver, const tdConstraintDistancePP* constraint);

void tdSolverInsertJointBP(int solver, const tdConstraintJointBP* constraint);

void tdSolverInsertParticleTriangle(int solver, const tdConstraintParticleTriangle* constraint);
void tdSolverInsertParticleTetra(int solver, const tdConstraintParticleTetra* constraint);
void tdSolverInsertParticleString(int solver, const tdConstraintParticleString* constraint);
void tdSolverInsertFluid(int solver, const tdConstraintFluid* constraint);

void tdSolverStep(int solver, float dt);

#ifdef __cplusplus
}
#endif

#endif

