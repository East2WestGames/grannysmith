/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */
/* Based on dbvt implementation from Bullet physics library, see below */

/*
Bullet Continuous Collision Detection and Physics Library
Copyright (c) 2003-2006 Erwin Coumans  http://continuousphysics.com/Bullet/

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.
Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it freely,
subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/
///btDbvt implementation by Nathanael Presson

#pragma once

#include "tdconfig.h"
#include "tdmath.h"

#include <vector>

inline bool btRayAabb2(const TdVec3& rayFrom,
								  const TdVec3& rayInvDirection,
								  const unsigned int raySign[3],
								  const TdVec3 bounds[2],
								  float& tmin,
								  float lambda_min,
								  float lambda_max)
{
	float tmax, tymin, tymax, tzmin, tzmax;
	tmin = (bounds[raySign[0]].x - rayFrom.x) * rayInvDirection.x;
	tmax = (bounds[1-raySign[0]].x - rayFrom.x) * rayInvDirection.x;
	tymin = (bounds[raySign[1]].y - rayFrom.y) * rayInvDirection.y;
	tymax = (bounds[1-raySign[1]].y - rayFrom.y) * rayInvDirection.y;

	if ( (tmin > tymax) || (tymin > tmax) )
		return false;

	if (tymin > tmin)
		tmin = tymin;

	if (tymax < tmax)
		tmax = tymax;

	tzmin = (bounds[raySign[2]].z - rayFrom.z) * rayInvDirection.z;
	tzmax = (bounds[1-raySign[2]].z - rayFrom.z) * rayInvDirection.z;

	if ( (tmin > tzmax) || (tzmin > tmax) )
		return false;
	if (tzmin > tmin)
		tmin = tzmin;
	if (tzmax < tmax)
		tmax = tzmax;
	return ( (tmin < lambda_max) && (tmax > lambda_min) );
}


/* btDbvtAabbMm			*/ 
struct	btDbvtAabbMm
{
	inline TdVec3 Center() const	{ return((mi+mx)/2); }
	inline TdVec3 Lengths() const { return(mx-mi); }
	inline TdVec3 Extents() const { return((mx-mi)/2); }
	inline const TdVec3&	Mins() const { return(mi); }
	inline const TdVec3&	Maxs() const { return(mx); }
	static inline btDbvtAabbMm		FromMM(const TdVec3& mi, const TdVec3& mx);
	inline void	Expand(	float eps );
	inline bool				Contain(const btDbvtAabbMm& a) const;
	inline friend bool		Intersect(const btDbvtAabbMm& a, const btDbvtAabbMm& b);
	
	inline friend bool		Intersect(	const btDbvtAabbMm& a, const TdVec3& b);
	inline friend float		Proximity(	const btDbvtAabbMm& a, const btDbvtAabbMm& b);
	inline friend int		Select(	const btDbvtAabbMm& o, const btDbvtAabbMm& a, const btDbvtAabbMm& b);
	inline friend void		Merge(	const btDbvtAabbMm& a, const btDbvtAabbMm& b, btDbvtAabbMm& r);
	inline friend bool		NotEqual( const btDbvtAabbMm& a, const btDbvtAabbMm& b);

	TdVec3	mi,mx;
};

// Types	
typedef	btDbvtAabbMm	btDbvtVolume;

/* btDbvtNode				*/ 
struct	btDbvtNode
{
	btDbvtVolume	volume;
	btDbvtNode*		parent;
	int				childCount;
	inline bool	isleaf() const		{ return(childs[1]==0); }
	inline bool	isinternal() const	{ return(!isleaf()); }
	union
	{
		btDbvtNode*	childs[2];
		void*	data;
		int		dataAsInt;
	};
};


struct	btDbvt
{
	/* Stack element	*/ 
	struct	sStkNN
	{
		const btDbvtNode*	a;
		const btDbvtNode*	b;
		sStkNN() {}
		sStkNN(const btDbvtNode* na,const btDbvtNode* nb) : a(na),b(nb) {}
	};

	// Policies/Interfaces

	/* ICollide	*/ 
	struct	ICollide
	{		
		virtual ~ICollide() {}
		virtual void Process(const btDbvtNode*,const btDbvtNode*)		{}
		virtual void Process(const btDbvtNode*)					{}
		virtual void Process(const btDbvtNode* n, float)			{ Process(n); }
		virtual bool Descent(const btDbvtNode*)					{ return(true); }
		virtual bool AllLeaves(const btDbvtNode*)					{ return(true); }
	};

	// Constants
	enum	{
		SIMPLE_STACKSIZE	=	64,
		DOUBLE_STACKSIZE	=	SIMPLE_STACKSIZE*2
	};

	// Fields
	btDbvtNode*		m_root;
	btDbvtNode*		m_free;
	int				m_lkhd;
	int				m_leaves;
	unsigned		m_opath;
	float 			m_eps;
	
	std::vector<sStkNN>	m_stkStack;


	// Methods
	btDbvt();
	~btDbvt();
	void			clear();
	bool			empty() const { return(0==m_root); }
	void			optimizeBottomUp();
	void			optimizeTopDown(int bu_treshold=128);
	void			optimizeIncremental(int passes);
	btDbvtNode*		insert(const btDbvtVolume& box, void* data);
	void			update(btDbvtNode* leaf, int lookahead=-1);
	void			update(btDbvtNode* leaf, btDbvtVolume& volume);
	void			remove(btDbvtNode* leaf);
	static int		maxdepth(const btDbvtNode* node);
	static int		countLeaves(const btDbvtNode* node);
	static void		extractLeaves(const btDbvtNode* node, std::vector<const btDbvtNode*>& leaves);

	static void		enumNodes(	const btDbvtNode* root,	ICollide& policy);
	static void		enumLeaves(	const btDbvtNode* root,	ICollide& policy);
	void collideTT(	const btDbvtNode* root0, const btDbvtNode* root1, ICollide& policy) const;
	void collideTTpersistentStack(const btDbvtNode* root0, const btDbvtNode* root1, ICollide& policy);
	void collideTV(	const btDbvtNode* root, const btDbvtVolume& volume, ICollide& policy);

	///rayTest is a re-entrant ray test, and can be called in parallel as long as the btAlignedAlloc is thread-safe (uses locking etc)
	///rayTest is slower than rayTestInternal, because it builds a local stack, using memory allocations, and it recomputes signs/rayDirectionInverses each time
	static void		rayTest(	const btDbvtNode* root, 
								const TdVec3& rayFrom, 
								const TdVec3& rayTo, 
								ICollide& policy);

	///rayTestInternal is faster than rayTest, because it uses a persistent stack (to reduce dynamic memory allocations to a minimum) and it uses precomputed signs/rayInverseDirections
	///rayTestInternal is used by btDbvtBroadphase to accelerate world ray casts
	void rayTestInternal(	const btDbvtNode* root,
							const TdVec3& rayFrom,
							const TdVec3& rayTo,
							const TdVec3& rayDirectionInverse,
							unsigned int signs[3],
							float lambda_max,
							const TdVec3& aabbMin,
							const TdVec3& aabbMax,
							ICollide& policy) const;

private:
	btDbvt(const btDbvt&)	{}	
};

//
// Inline's
//

//
inline btDbvtAabbMm			btDbvtAabbMm::FromMM(const TdVec3& mi,const TdVec3& mx)
{
	btDbvtAabbMm box;
	box.mi=mi;
	box.mx=mx;
	return(box);
}

//
inline bool		btDbvtAabbMm::Contain(const btDbvtAabbMm& a) const
{
	return(	(mi.x<=a.mi.x) &&
			(mi.y<=a.mi.y) &&
			(mi.z<=a.mi.z) &&
			(mx.x>=a.mx.x) &&
			(mx.y>=a.mx.y) &&
			(mx.z>=a.mx.z) );
}

//
inline bool		Intersect(	const btDbvtAabbMm& a, const btDbvtAabbMm& b)
{
	return(	(a.mi.x<=b.mx.x) &&
			(a.mx.x>=b.mi.x) &&
			(a.mi.y<=b.mx.y) &&
			(a.mx.y>=b.mi.y) &&
			(a.mi.z<=b.mx.z) &&		
			(a.mx.z>=b.mi.z) );
}



//
inline bool		Intersect(	const btDbvtAabbMm& a, const TdVec3& b)
{
	return(	(b.x>=a.mi.x) &&
			(b.y>=a.mi.y) &&
			(b.z>=a.mi.z) &&
			(b.x<=a.mx.x) &&
			(b.y<=a.mx.y) &&
			(b.z<=a.mx.z) );
}

inline void	btDbvtAabbMm::Expand(	float eps )
{
	mi.x -= eps;
	mi.y -= eps;
	mi.z -= eps;
	mx.x += eps;
	mx.y += eps;
	mx.z += eps;
}






//////////////////////////////////////


//
inline float Proximity(	const btDbvtAabbMm& a, const btDbvtAabbMm& b)
{
	const TdVec3 d=(a.mi+a.mx)-(b.mi+b.mx);
	return manhattanDistance(d);
}


//
inline int Select(	const btDbvtAabbMm& o, const btDbvtAabbMm& a, const btDbvtAabbMm& b)
{
	return(Proximity(o,a)<Proximity(o,b)?0:1);
}

//
inline void Merge(	const btDbvtAabbMm& a, const btDbvtAabbMm& b, btDbvtAabbMm& r)
{
	for(int i=0;i<3;++i)
	{
		r.mi[i] = a.mi[i]<b.mi[i] ? a.mi[i] : b.mi[i];
		r.mx[i] = a.mx[i]>b.mx[i] ? a.mx[i] : b.mx[i];
	}
}

//
inline bool NotEqual(const btDbvtAabbMm& a, const btDbvtAabbMm& b)
{
	return(	(a.mi.x!=b.mi.x)||
		(a.mi.y!=b.mi.y)||
		(a.mi.z!=b.mi.z)||
		(a.mx.x!=b.mx.x)||
		(a.mx.y!=b.mx.y)||
		(a.mx.z!=b.mx.z));
}

//
// Inline's
//

//
inline void		btDbvt::enumNodes(	const btDbvtNode* root, ICollide& policy)
{
	policy.Process(root);
	if(root->isinternal())
	{
		enumNodes(root->childs[0], policy);
		enumNodes(root->childs[1], policy);
	}
}

//
inline void		btDbvt::enumLeaves(	const btDbvtNode* root, ICollide& policy)
{
	if(root->isinternal())
	{
		enumLeaves(root->childs[0],policy);
		enumLeaves(root->childs[1],policy);
	}
	else
	{
		policy.Process(root);
	}
}

//
inline void		btDbvt::collideTT(	const btDbvtNode* root0, const btDbvtNode* root1, ICollide& policy) const
{
	if(root0 && root1)
	{
		int	depth=1;
		int	treshold=DOUBLE_STACKSIZE-4;
		std::vector<sStkNN>	stkStack;
		stkStack.resize(DOUBLE_STACKSIZE);
		stkStack[0]=sStkNN(root0,root1);
		do	
		{		
			sStkNN	p=stkStack[--depth];
			if(depth>treshold)
			{
				stkStack.resize(stkStack.size()*2);
				treshold=stkStack.size()-4;
			}

			if(p.a==p.b)
			{
				if(p.a->isinternal())
				{
					stkStack[depth++]=sStkNN(p.a->childs[0],p.a->childs[0]);
					stkStack[depth++]=sStkNN(p.a->childs[1],p.a->childs[1]);
					stkStack[depth++]=sStkNN(p.a->childs[0],p.a->childs[1]);
				}
			}
			else if(Intersect(p.a->volume,p.b->volume))
			{
				if(p.a->isinternal())
				{
					if(p.b->isinternal())
					{
						stkStack[depth++]=sStkNN(p.a->childs[0],p.b->childs[0]);
						stkStack[depth++]=sStkNN(p.a->childs[1],p.b->childs[0]);
						stkStack[depth++]=sStkNN(p.a->childs[0],p.b->childs[1]);
						stkStack[depth++]=sStkNN(p.a->childs[1],p.b->childs[1]);
					}
					else
					{
						stkStack[depth++]=sStkNN(p.a->childs[0],p.b);
						stkStack[depth++]=sStkNN(p.a->childs[1],p.b);
					}
				}
				else
				{
					if(p.b->isinternal())
					{
						stkStack[depth++]=sStkNN(p.a,p.b->childs[0]);
						stkStack[depth++]=sStkNN(p.a,p.b->childs[1]);
					}
					else
					{
						policy.Process(p.a,p.b);
					}
				}
			}
		} while(depth);
	}
}


inline void		btDbvt::collideTV(	const btDbvtNode* root, const btDbvtVolume& vol, ICollide& policy)
{
		if(root)
		{
			//ATTRIBUTE_ALIGNED16(btDbvtVolume)		volume(vol);
			btDbvtVolume volume(vol);
			std::vector<const btDbvtNode*> stack;
			stack.resize(0);
			stack.reserve(SIMPLE_STACKSIZE);
			stack.push_back(root);
			do	{
				const btDbvtNode*	n=stack[stack.size()-1];
				stack.pop_back();
				if(Intersect(n->volume,volume))
				{
					if(n->isinternal())
					{
						stack.push_back(n->childs[0]);
						stack.push_back(n->childs[1]);
					}
					else
					{
						policy.Process(n);
					}
				}
			} while(stack.size()>0);
		}
}

inline void		btDbvt::rayTestInternal(const btDbvtNode* root,
								const TdVec3& rayFrom,
								const TdVec3& rayTo,
								const TdVec3& rayDirectionInverse,
								unsigned int signs[3],
								float lambda_max,
								const TdVec3& aabbMin,
								const TdVec3& aabbMax,
								ICollide& policy) const
{
	if(root)
	{
		TdVec3 resultNormal;

		int								depth=1;
		int								treshold=DOUBLE_STACKSIZE-2;
		std::vector<const btDbvtNode*>	stack;
		stack.resize(DOUBLE_STACKSIZE);
		stack[0]=root;
		TdVec3 bounds[2];
		do	
		{
			const btDbvtNode*	node=stack[--depth];
			bounds[0] = node->volume.Mins()-aabbMax;
			bounds[1] = node->volume.Maxs()-aabbMin;
			float tmin=1.f,lambda_min=0.f;
			unsigned int result1=false;
			result1 = btRayAabb2(rayFrom,rayDirectionInverse,signs,bounds,tmin,lambda_min,lambda_max);
			if(result1)
			{
				if(node->isinternal())
				{
					if(depth>treshold)
					{
						stack.resize(stack.size()*2);
						treshold=stack.size()-2;
					}
					stack[depth++]=node->childs[0];
					stack[depth++]=node->childs[1];
				}
				else
				{
					policy.Process(node);
				}
			}
		} while(depth);
	}
}

//
inline void		btDbvt::rayTest(	const btDbvtNode* root,
								const TdVec3& rayFrom,
								const TdVec3& rayTo,
								ICollide& policy)
{
		if(root)
		{
			TdVec3 rayDir = normalize(rayTo-rayFrom);

			///what about division by zero? --> just set rayDirection[i] to INF/BT_LARGE_FLOAT
			TdVec3 rayDirectionInverse;
			rayDirectionInverse[0] = rayDir[0] == float(0.0) ? TD_FLOAT_MAX : 1.0f / rayDir[0];
			rayDirectionInverse[1] = rayDir[1] == float(0.0) ? TD_FLOAT_MAX : 1.0f / rayDir[1];
			rayDirectionInverse[2] = rayDir[2] == float(0.0) ? TD_FLOAT_MAX : 1.0f / rayDir[2];
			unsigned int signs[3] = { rayDirectionInverse[0] < 0.0f, rayDirectionInverse[1] < 0.0f, rayDirectionInverse[2] < 0.0f};

			float lambda_max = dot(rayDir, rayTo-rayFrom);

			TdVec3 resultNormal;

			std::vector<const btDbvtNode*>	stack;

			int								depth=1;
			int								treshold=DOUBLE_STACKSIZE-2;

			stack.resize(DOUBLE_STACKSIZE);
			stack[0]=root;
			TdVec3 bounds[2];
			do	{
				const btDbvtNode*	node=stack[--depth];

				bounds[0] = node->volume.Mins();
				bounds[1] = node->volume.Maxs();
				
				float tmin=1.f,lambda_min=0.f;
				unsigned int result1 = btRayAabb2(rayFrom,rayDirectionInverse,signs,bounds,tmin,lambda_min,lambda_max);

				if(result1)
				{
					if(node->isinternal())
					{
						if(depth>treshold)
						{
							stack.resize(stack.size()*2);
							treshold=stack.size()-2;
						}
						stack[depth++]=node->childs[0];
						stack[depth++]=node->childs[1];
					}
					else
					{
						policy.Process(node);
					}
				}
			} while(depth);

		}
}

