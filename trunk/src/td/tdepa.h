/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */
/* Based on dbvt implementation from Bullet physics library, see below */

/*
Bullet Continuous Collision Detection and Physics Library
Copyright (c) 2003-2008 Erwin Coumans  http://continuousphysics.com/Bullet/

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the
use of this software.
Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely,
subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software in a
product, an acknowledgment in the product documentation would be appreciated
but is not required.
2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

/*
GJK-EPA collision solver by Nathanael Presson, 2008
*/

#pragma once

#include "tdgjk.h"
#include "tddebug.h"

#define EPA_MAX_VERTICES	32
#define EPA_MAX_FACES		(EPA_MAX_VERTICES*2)
#define EPA_ACCURACY		(0.001f) //This is epsilon
#define EPA_PLANE_EPS		(0.001f)
#define EPA_INSIDE_EPS		(0.01f)

class TdEpa
{
public:
	/* Types		*/ 
	class sSimplex
	{
	public:
		TdSimplexPoint* c[4];
		float		p[4];
		int			rank;
	};
	
	struct	sFace
	{
		TdVec3		n;
		float 		d;
		float 		p;
		TdSimplexPoint* c[3];
		sFace*		f[3];
		sFace*		l[2];
		int			e[3];
		int			pass;
	};
	struct	sList
	{
		sFace*		root;
		int			count;
		sList() : root(0),count(0)	{}
	};
	struct	sHorizon
	{
		sFace*		cf;
		sFace*		ff;
		int			nf;
		sHorizon() : cf(0),ff(0),nf(0)	{}
	};
	enum eStatus
	{
		StatusValid,
		StatusTouching,
		StatusDegenerated,
		StatusNonConvex,
		StatusInvalidHull,
		StatusOutOfFaces,
		StatusOutOfVertices,
		StatusAccuraryReached,
		StatusMaxIterationsReached,
		StatusFallBack,
		StatusFailed
	};
	
	/* Fields		*/ 
	eStatus		m_status;
	sSimplex	m_result;
	TdVec3			m_normal;
	float			m_depth;
	TdSimplexPoint	m_sv_store[EPA_MAX_VERTICES];
	sFace			m_fc_store[EPA_MAX_FACES];
	int				m_nextsv;
	sList			m_hull;
	sList			m_stock;
	float			mDeltaDistance;
	sFace			mOuter;
	float 			mEps;
	int 			mMaxIterations;
	

	/* Methods		*/ 
	TdEpa()
	{
		Initialize();	
	}


	TdEpa(float tolerance, int maxIterations) : 
	mEps(tolerance), mMaxIterations(maxIterations) 
	{
		Initialize();	
	}


	static inline void	bind(sFace* fa,int ea,sFace* fb,int eb)
	{
		fa->e[ea]=eb;fa->f[ea]=fb;
		fb->e[eb]=ea;fb->f[eb]=fa;
	}
	static inline void		append(sList& list,sFace* face)
	{
		face->l[0]	=	0;
		face->l[1]	=	list.root;
		if(list.root) list.root->l[0]=face;
		list.root	=	face;
		++list.count;
	}
	static inline void		remove(sList& list,sFace* face)
	{
		if(face->l[1]) face->l[1]->l[0]=face->l[0];
		if(face->l[0]) face->l[0]->l[1]=face->l[1];
		if(face==list.root) list.root=face->l[1];
		--list.count;
	}

	void				Initialize()
	{
		m_status	=	StatusFailed;
		m_normal.set(0,0,0);
		m_depth		=	0;
		m_nextsv	=	0;
		for(int i=0;i<EPA_MAX_FACES;++i)
		{
			append(m_stock,&m_fc_store[EPA_MAX_FACES-i-1]);
		}
		mEps = 0.001f;
		mMaxIterations = 32;
	}
	eStatus Evaluate(TdGjk& gjk, float& distance, TdVec3& normal, TdVec3* point0, TdVec3* point1)
	{
		mDeltaDistance = 0.0f;
		int iterations = 0;
		{
			/* Clean up				*/ 
			while(m_hull.root)
			{
				sFace*	f = m_hull.root;
				remove(m_hull,f);
				append(m_stock,f);
			}
			m_status	=	StatusValid;
			m_nextsv	=	0;

			/* Build initial hull	*/ 
			sFace*	tetra[]={newface(&gjk.mSimplex[2],&gjk.mSimplex[1],&gjk.mSimplex[0],true),
				newface(&gjk.mSimplex[1],&gjk.mSimplex[2],&gjk.mSimplex[3],true),
				newface(&gjk.mSimplex[0],&gjk.mSimplex[1],&gjk.mSimplex[3],true),
				newface(&gjk.mSimplex[2],&gjk.mSimplex[0],&gjk.mSimplex[3],true)};
				
			if(m_hull.count==4)
			{
				sFace*		best=findbest();
				mOuter=*best;
				int			pass=0;
				bind(tetra[0],0,tetra[1],0);
				bind(tetra[0],1,tetra[2],0);
				bind(tetra[0],2,tetra[3],0);
				bind(tetra[1],1,tetra[3],2);
				bind(tetra[1],2,tetra[2],1);
				bind(tetra[2],2,tetra[3],1);
				m_status=StatusValid;
				for(;iterations<mMaxIterations;++iterations)
				{
					if(m_nextsv<EPA_MAX_VERTICES)
					{
						
						sHorizon		horizon;
						TdSimplexPoint*	w=&m_sv_store[m_nextsv++];
						bool			valid=true;					
						best->pass	=	++pass;
						gjk.mCso->getSupport(best->n,*w);

						TD_DEBUG_PUSH();
						float	wdist=dot(best->n, w->cs)-best->d;						
						mDeltaDistance = wdist;

						TD_DEBUG_LINE(w->cs, w->cs-best->n*wdist);
						TD_DEBUG_POINT(w->cs);
						TD_DEBUG_PAUSE();
						TD_DEBUG_POP();
						
						if(wdist>mEps)
						{							
							for(int j=0;(j<3)&&valid;++j)
							{
								valid&=expand(	pass, w, best->f[j],best->e[j], horizon);									
							}
							if(valid&&(horizon.nf>=3))
							{
								bind(horizon.cf,1,horizon.ff,2);
								remove(m_hull,best);
								append(m_stock,best);
								best=findbest();
								if(best->p>=mOuter.p)
									mOuter=*best;
							} 
							else
							{
								m_status=StatusInvalidHull;
								break;
							}
						}
						else
						{
							m_status=StatusAccuraryReached;
							break;
						}
					}
					else
					{
						m_status=StatusOutOfVertices;
						break;
					}
				}
				if (iterations == mMaxIterations)
					m_status = StatusMaxIterationsReached;

				//Resulting distance is distance to outer triangle
				//Negative, since it's penetration we're dealing with
				normal = mOuter.n;
				distance = -dot(normal, mOuter.c[0]->cs);

				//Compute local points only if we need to
				if (point0 || point1)
				{
					float dist0 = lengthSquared(mOuter.c[0]->cs - mOuter.c[1]->cs);
					float dist1 = lengthSquared(mOuter.c[0]->cs - mOuter.c[2]->cs);
					float dist2 = lengthSquared(mOuter.c[1]->cs - mOuter.c[2]->cs);
					if (dist0 > dist2*4 && dist1 > dist2*4)
					{
						TdSimplexPoint* tmp = mOuter.c[0];
						mOuter.c[0] = mOuter.c[1];
						mOuter.c[1] = mOuter.c[2];
						mOuter.c[2] = tmp;
					}

					float t0, t1;
					TdVec3 e0 = mOuter.c[1]->cs - mOuter.c[0]->cs;
					TdVec3 e1 = mOuter.c[2]->cs - mOuter.c[0]->cs;
					getTriangleHit(mOuter.c[0]->cs, e0, e1, t0, t1);

					if (point0)
					{
						TdVec3 e0 = mOuter.c[1]->p0 - mOuter.c[0]->p0;
						TdVec3 e1 = mOuter.c[2]->p0 - mOuter.c[0]->p0;
						*point0 = mOuter.c[0]->p0 + e0*t0 + e1*t1;
					}

					if (point1)
					{
						TdVec3 e0 = mOuter.c[1]->p1 - mOuter.c[0]->p1;
						TdVec3 e1 = mOuter.c[2]->p1 - mOuter.c[0]->p1;
						*point1 = mOuter.c[0]->p1 + e0*t0 + e1*t1;
					}
				}
				return m_status;
			}
			return StatusFailed;
		}
	}

	sFace*				newface(TdSimplexPoint* a,TdSimplexPoint* b,TdSimplexPoint* c,bool forced)
	{
		if(m_stock.root)
		{
			sFace*	face=m_stock.root;
			remove(m_stock,face);
			append(m_hull,face);
			face->pass	=	0;
			face->c[0]	=	a;
			face->c[1]	=	b;
			face->c[2]	=	c;
			face->n		=	cross(b->cs - a->cs, c->cs - a->cs);
			const float	l=length(face->n);
			const bool		v=l>EPA_ACCURACY;
			face->p		=	TdMin(TdMin(
				dot(a->cs, cross(face->n, a->cs - b->cs)),
				dot(b->cs, cross(face->n, b->cs - c->cs))),
				dot(c->cs, cross(face->n, c->cs - a->cs))) /
				(v?l:1);
			face->p = face->p>=-EPA_INSIDE_EPS?0:face->p;
			if(v)
			{
				face->d = dot(a->cs, face->n)/l;
				face->n /= l;
				if(forced||(face->d>=-EPA_PLANE_EPS))
					return(face);
				else
					m_status=StatusNonConvex;
			}
			else
				m_status=StatusDegenerated;
			remove(m_hull,face);
			append(m_stock,face);
			return(0);
		}
		m_status=m_stock.root?StatusOutOfVertices:StatusOutOfFaces;
		return(0);
	}
	sFace*				findbest()
	{
		sFace*		minf=m_hull.root;
		float		mind=minf->d*minf->d;
		float		maxp=minf->p;
		for(sFace* f=minf->l[1];f;f=f->l[1])
		{
			TD_DEBUG_LINE(f->c[0]->cs, f->c[1]->cs);
			TD_DEBUG_LINE(f->c[1]->cs, f->c[2]->cs);
			TD_DEBUG_LINE(f->c[2]->cs, f->c[0]->cs);
			float	sqd=f->d*f->d;
			if((f->p>=maxp)&&(sqd<mind))
			{
				minf=f;
				mind=sqd;
				maxp=f->p;
			}
		}
		return(minf);
	}
	bool				expand(int pass,TdSimplexPoint* w,sFace* f,int e,sHorizon& horizon)
	{
		static const int i1m3[]={1,2,0};
		static const int i2m3[]={2,0,1};
		if(f->pass!=pass)
		{
			const int e1=i1m3[e];
			if((dot(f->n, w->cs)-f->d)<-EPA_PLANE_EPS)
			{
				sFace*	nf=newface(f->c[e1],f->c[e],w,false);
				if(nf)
				{
					bind(nf,0,f,e);
					if(horizon.cf)
						bind(horizon.cf,1,nf,2);
					else
						horizon.ff=nf;
					horizon.cf=nf;
					++horizon.nf;
					return(true);
				}
			}
			else
			{
				const int e2=i2m3[e];
				f->pass = pass;
				if(	expand(pass,w,f->f[e1],f->e[e1],horizon)&&
					expand(pass,w,f->f[e2],f->e[e2],horizon))
				{
					remove(m_hull,f);
					append(m_stock,f);
					return(true);
				}
			}
		}
		return(false);
	}

};

