/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#pragma once

#include "td.h"
#include "tdcso.h"
#include "tdgjk.h"
#include "tdepa.h"
#include "tdmpr.h"


class TdContextInternal
{
public:
	tdMalloc customMalloc;
	tdFree customFree;
	int maxIterations;
	float tolerance;

	TdContextInternal();
	~TdContextInternal();
};

