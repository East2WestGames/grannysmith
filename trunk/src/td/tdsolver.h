/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#pragma once

#include "td.h"
#include "tdconfig.h"
#include "tdmath.h"
#include "tdconstrainttype.h"

extern tdBody gStaticBody;

class TdSolverBody
{
public:
	TdVec3 mLinVel;
	TdVec3 mAngVel;
	TdVec3 mLinAcc;
	TdVec3 mInvInertia;
	float mInvMass;
	
	inline float getAffection(const TdVec3& point, const TdVec3& dir)
	{
		TdVec3 t = multiplyPerElement(mInvInertia, cross(point, dir));
		return mInvMass + dot(dir, cross(t, point));
	}

	inline float getAngAffection(const TdVec3& dir)
	{
		return dot(dir, multiplyPerElement(mInvInertia, dir));
	}

	inline void applyImpulse(const TdVec3& pos, const TdVec3& impulse)
	{
		mLinVel += impulse * mInvMass;
		mAngVel += multiplyPerElement(mInvInertia, cross(pos, impulse));
	}

	inline void applyAngImpulse(const TdVec3& impulse)
	{
		mAngVel += multiplyPerElement(mInvInertia, impulse);
	}

	inline TdVec3 getPointVel(const TdVec3& point)
	{
		return mLinVel + cross(mAngVel, point);
	}

	inline float getPropagationFactor(const TdVec3& pointVel, const TdVec3& dir)
	{
		float factor = TdClamp(-dot(mLinAcc, dir), 0.0f, 1.0f);
		factor *= TdClamp(dot(mLinAcc, pointVel)*2.0f, 0.0f, 1.0f);
		return 1.0f + 1.0f*factor;
	}	
};

class TdSolverParticle
{
public:
	float mInvMass;
	TdVec3 mVel;
	float mDensity;

	inline void applyImpulse(const TdVec3& impulse)
	{
		mVel += impulse * mInvMass;
	}
};


class TdSolverBodyExtras
{
public:
	TdVec3 mAngVelMax;
	TdVec3 mAngVelMin;
	TdVec3 mAngAcc;
};


class TdSolverParticleExtras
{
public:
	TdVec3 mAcc;
};


class TdSolverState
{
public:
	TdVec3 linAcc;
	TdVec3 angAcc;
	float params[TD_SOLVER_PARAM_COUNT];
	bool features[TD_SOLVER_FEATURE_COUNT];

	inline float getMaxImpulse() const { return params[TD_SOLVER_PARAM_MAX_IMPULSE]; } 
	inline float getMinImpulse() const { return params[TD_SOLVER_PARAM_MIN_IMPULSE]; } 
	inline float getFriction() const { return params[TD_SOLVER_PARAM_FRICTION]; } 
	inline float getRestitution() const { return params[TD_SOLVER_PARAM_RESTITUTION]; } 
	inline float getContactOffset() const { return params[TD_SOLVER_PARAM_CONTACT_OFFSET]; } 
	inline float getStretchDamping() const { return params[TD_SOLVER_PARAM_STRETCH_DAMPING]; } 
	inline float getStretchStiffness() const { return params[TD_SOLVER_PARAM_STRETCH_STIFFNESS]; } 
	inline float getBendDamping() const { return params[TD_SOLVER_PARAM_BEND_DAMPING]; } 
	inline float getBendStiffness() const { return params[TD_SOLVER_PARAM_BEND_STIFFNESS]; } 
	inline float getFluidViscoElasticity() const { return params[TD_SOLVER_PARAM_FLUID_VISCO_ELASTICITY]; } 
	inline float getFluidParticleSize() const { return params[TD_SOLVER_PARAM_FLUID_PARTICLE_SIZE]; } 
	inline bool hasContactSticky() const { return features[TD_SOLVER_FEATURE_CONTACT_STICKY]; }
	inline float getContactMinImpulse() const { return hasContactSticky() ? getMinImpulse() : 0.0f; }
	inline float getMaxPenetration() const { return 0.02f; }

	inline bool clampAndAccumulateContactImpulse(float& imp, float& accImp) const 
	{
		if (accImp + imp < getContactMinImpulse())
		{
			imp = getContactMinImpulse() - accImp;
			accImp = getContactMinImpulse();
			return true;
		}
		else if (accImp + imp > getMaxImpulse())
		{
			imp = getMaxImpulse() - accImp;
			accImp = getMaxImpulse();
			return true;
		}
		accImp += imp;
		return false;
	}

	inline bool clampAndAccumulateFrictionImpulse(float& imp, float& accImp, float normalImpulse) const 
	{
		float fricMax = TdAbs(normalImpulse) * getFriction() - getContactMinImpulse();
		if (accImp + imp < -fricMax)
		{
			imp = -fricMax - accImp;
			accImp = -fricMax;
			return true;
		}
		else if (accImp + imp > fricMax)
		{
			imp = fricMax - accImp;
			accImp = fricMax;
			return true;
		}
		accImp += imp;
		return false;
	}

	inline bool clampAndAccumulateImpulse(float& imp, float& accImp) const 
	{
		if (accImp + imp < getMinImpulse())
		{
			imp = getMinImpulse() - accImp;
			accImp = getMinImpulse();
			return true;
		}
		else if (accImp + imp > getMaxImpulse())
		{
			imp = getMaxImpulse() - accImp;
			accImp = getMaxImpulse();
			return true;
		}
		accImp += imp;
		return false;
	}
};


class TdSolver
{
public:
	enum ItemType
	{
		ITEM_CONSTRAINT,
		ITEM_PARAM,
		ITEM_FEATURE
	};
	
	class Item
	{
	public:
		inline Item() {}

		inline Item(TdConstraintType::Type constraintType, int start, int count) : mType(ITEM_CONSTRAINT)
		{
			mConstraint.type = constraintType;
			mConstraint.start = start;
			mConstraint.count = count;
		}

		inline Item(int param, float value) : mType(ITEM_PARAM)
		{
			mParam.index = param;
			mParam.value = value;
		}

		inline Item(int feature, bool enabled) : mType(ITEM_FEATURE)
		{
			mFeature.index = feature;
			mFeature.enabled = enabled;
		}
		
		ItemType mType;
		union
		{
			struct
			{
				TdConstraintType::Type type;
				int start;
				int count;
			} mConstraint;

			struct
			{
				int index;
				float value;
			} mParam;
			
			struct
			{
				int index;
				bool enabled;
			} mFeature;
		};
	};
	
	TdSolver();
	~TdSolver();

	void setParam(int param, float value);
	void setEnabled(int feature, bool value);

	void reset();
	void resetState();

	void step(float dt);

	inline float getErp() const { return 0.8f / mDt; }
	inline float getDt() const { return mDt; }
	inline int getCurrentIteration() const { return mCurrentIteration; }

	TdSolverState mState;
	TdSolverState mInitialState;
	
	std::vector<TdSolverState> mStateStack;
	
	std::vector<TdSolverBody> mBodies;
	std::vector<TdSolverBodyExtras> mBodiesExtras;
	std::vector<tdBody*> mOriginalBodies;

	std::vector<TdSolverParticle> mParticles;
	std::vector<TdSolverParticleExtras> mParticlesExtras;
	std::vector<tdParticle*> mOriginalParticles;

	inline tdBody* getBody(int id) { return mOriginalBodies[id]; }
	inline TdSolverBody* getSolverBody(int id) { return &mBodies[id]; }
	inline TdSolverBodyExtras* getBodyExtras(int id) { return &mBodiesExtras[id]; }

	inline tdParticle* getParticle(int id) { return mOriginalParticles[id]; }
	inline TdSolverParticle* getSolverParticle(int id) { return &mParticles[id]; }

	std::vector<Item> mItems;
	TdConstraintType* mConstraintTypes[TdConstraintType::COUNT];
	void* addConstraint(TdConstraintType::Type type);

	tdTransform getBodyTransform(tdBody* body);
	tdTransform getBodyTransformDiff(tdBody* body0, tdBody* body1);

	float mDt;
	int mCurrentIteration;
	int mIterationCount;
	int mConstraintCount;
};
