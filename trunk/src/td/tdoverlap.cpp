/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#include "td.h"
#include "tdshape.h"
#include "tdgjk.h"
#include "tdepa.h"
#include "tdcontext.h"

#include "tddebug.h"

//Input query flags:
//NONE

//Output status flags:
//TD_RESULT_SEPARATION
//TD_RESULT_PENETRATION
//TD_RESULT_FAILED

void tdOverlap(int context, const tdOverlapQuery* query, tdOverlapResult* result)
{

	TD_DEBUG_CLEAR();
	TD_DEBUG_TEXT("Overlap query");
	TD_DEBUG_CSO();

	TdContextInternal* ctxt = (TdContextInternal*)context;

	//Setup GJK
	TdCso cso;
	cso.init(query->shape0, query->shape1, &query->transform);

	TdGjk gjk(ctxt->tolerance, ctxt->maxIterations);
	gjk.init(&cso);
	gjk.mMaxDistance = 0.0f;

	//Iterate
	if (query->flags & TD_QUERY_LOAD_SIMPLEX)
		gjk.computeDistance((GjkSimplex*)query->simplex);
	else
		gjk.computeDistance(NULL);
	if (query->flags & TD_QUERY_STORE_SIMPLEX)
		gjk.getSimplex((GjkSimplex&)*query->simplex);

	//Fill in result
	if (gjk.hasPenetration())
		result->status = TD_RESULT_PENETRATION;
	else
		result->status = TD_RESULT_SEPARATION;
}

