/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#pragma once

#include "td.h"
#include "tdconfig.h"
#include "tdmath.h"
#include "tdshape.h"


class TdSimplexPoint
{
public:
	TdVec3 cs;
	TdVec3 p0;
	TdVec3 p1;
};

class TdCso;

typedef void(*TdShapeSupportFunc)(const TdCso* cso, const TdVec3& dir, TdSimplexPoint& point);

class TdCso
{
public:
	const tdShape* mShape0;
	const tdShape* mShape1;
	const tdTransform* mTransform;
	TdVec3 mLinAdd;
	bool mUseLinAdd;
	TdShapeSupportFunc mFunc;

	inline TdCso() : 
	mShape0(NULL), mShape1(NULL), mTransform(NULL)
	{
	}


	void setSupportFunc();

	inline void init(const tdShape* shape0, const tdShape* shape1, const tdTransform* transform) 
	{
		mShape0 = shape0;
		mShape1 = shape1;
		mTransform = transform;
		mLinAdd.set(0.0f, 0.0f, 0.0f);
		mUseLinAdd = false;
		setSupportFunc();
	}


	inline void getSupport(const TdVec3& dir, TdSimplexPoint& point) const
	{
		mFunc(this, dir, point);
		if (mUseLinAdd && dot(dir, mLinAdd) > 0.0f)
			point.cs += mLinAdd;
	}
	

	//Does not take linAdd into account! Simply because it cant without the search direction
	//Or?
	inline TdVec3 getConfigurationSpacePoint(const TdVec3& p0, const TdVec3& p1) const
	{
		return p0-TdTransformPoint(mTransform, p1);
	}
	

	inline TdVec3 getMidPoint() const
	{
		TdVec3 p = TdShapeGetMidPoint(mShape0);
		p -= TdTransformPoint(mTransform, TdShapeGetMidPoint(mShape1));
		p += mLinAdd * 0.5f;
		return p;
	}
};


