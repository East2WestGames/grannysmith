/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#include "td.h"

void tdBodySetMassAsSphere(tdBody* body, float mass, float radius)
{
	tdVec x = {radius, radius, radius};
	tdBodySetMassAsBox(body, mass, &x);
}

void tdBodySetMassAsBox(tdBody* body, float mass, const tdVec* halfExtent)
{
	if (mass > 0.0f)
	{
		float x = 2.0f*halfExtent->x;
		float y = 2.0f*halfExtent->y;
		float z = 2.0f*halfExtent->z;
		body->invMass = 1.0f/mass;
		body->invInertia.x = 12.0f/(mass*(y*y + z*z));
		body->invInertia.y = 12.0f/(mass*(x*x + z*z));
		body->invInertia.z = 12.0f/(mass*(x*x + y*y));
	}
	else
	{
		body->invMass = 0.0f;
		body->invInertia.x = 0.0f;
		body->invInertia.y = 0.0f;
		body->invInertia.z = 0.0f;
	}
}

void tdBodySetMassAsInfiniteInertia(tdBody* body, float mass)
{
	if (mass > 0.0f)
		body->invMass = 1.0f/mass;
	else
		body->invMass = 0.0f;
	body->invInertia.x = 0.0f;
	body->invInertia.y = 0.0f;
	body->invInertia.z = 0.0f;
}


