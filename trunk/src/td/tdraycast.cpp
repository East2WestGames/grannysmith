/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#include "td.h"
#include "tdconfig.h"
#include "tdshape.h"
#include "tdgjk.h"
#include "tdepa.h"
#include "tdcontext.h"
#include "tddebug.h"

static tdShape pointShape;

void tdRaycast(int context, const tdRaycastQuery* query, tdRaycastResult* result)
{
	TD_DEBUG_CLEAR();
	TD_DEBUG_TEXT("Raycast query");
	TD_DEBUG_CSO();

	pointShape.type = TD_SHAPE_TYPE_POINT;
	tdShapecastQuery q;
	q.flags = query->flags;
	q.shape0 = query->shape;
	q.shape1 = &pointShape;
	tdTransformInitP(&q.transform, &query->point);
	q.maxDistance = query->maxDistance;
	q.direction = -((TdVec3&)query->direction);
	
	tdShapecastResult r;
	tdShapecast(context, &q, &r);
	
	result->status = r.status;
	result->distance = r.distance;
	result->normal = r.normal;
	result->point = r.point0;
}

