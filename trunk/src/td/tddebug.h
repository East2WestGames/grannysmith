/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#pragma once

#include "tdconfig.h"
#include "tdcso.h"

#include <vector>

class TdDebugItem
{
public:
	enum Type
	{
		PUSH,
		POP,
		PAUSE,
		TEXT,
		CSO,
		GFX
	};
	
	virtual ~TdDebugItem() {}
	virtual void draw() {}
	virtual const char* getText() { return ""; }
	virtual Type getType() = 0;
};


class TdDebugPush : public TdDebugItem
{
	Type getType()
	{
		return PUSH;
	}
};


class TdDebugPop : public TdDebugItem
{
	Type getType()
	{
		return POP;
	}
};


class TdDebugPause : public TdDebugItem
{
	Type getType()
	{
		return PAUSE;
	}
};


class TdDebugText : public TdDebugItem
{
public:
	char mText[256];

	TdDebugText(const char* text)
	{
		strncpy(mText, text, 256);
	}

	virtual const char* getText()
	{
		return mText;
	}
	
	virtual Type getType()
	{
		return TEXT;
	}
};


class TdDebugCso : public TdDebugItem
{
public:
	virtual Type getType()
	{
		return CSO;
	}	
};


class TdDebugSimplex : public TdDebugItem
{
public:
	TdSimplexPoint mPoints[4];
	int mCount;
	
	TdDebugSimplex(TdSimplexPoint* p, int count)
	{
		mCount = count;
		for(int i=0; i<count; i++)
			mPoints[i] = p[i];
	}
		
	virtual Type getType()
	{
		return GFX;
	}
	
	virtual void draw();
};


class TdDebugPoint : public TdDebugItem
{
public:
	TdVec3 mPoint;
	
	TdDebugPoint(const TdVec3& p) : mPoint(p)
	{
	}
		
	virtual Type getType()
	{
		return GFX;
	}
	
	virtual void draw();
};


class TdDebugLine : public TdDebugItem
{
public:
	TdVec3 mPoint0;
	TdVec3 mPoint1;
	
	TdDebugLine(const TdVec3& p0, const TdVec3& p1) : mPoint0(p0), mPoint1(p1) 
	{
	}
		
	virtual Type getType()
	{
		return GFX;
	}
	
	virtual void draw();
};


extern std::vector<TdDebugItem*> gDebugStack;

#ifdef TD_DEBUG_TRACE

#define TD_DEBUG_CLEAR() for(unsigned int i=0; i<gDebugStack.size(); i++){delete gDebugStack[i];} gDebugStack.clear();
#define TD_DEBUG_PUSH() gDebugStack.push_back(new TdDebugPush);
#define TD_DEBUG_POP() gDebugStack.push_back(new TdDebugPop);
#define TD_DEBUG_PAUSE() gDebugStack.push_back(new TdDebugPause);
#define TD_DEBUG_CSO() gDebugStack.push_back(new TdDebugCso);
#define TD_DEBUG_SIMPLEX(points, count) gDebugStack.push_back(new TdDebugSimplex(points, count));
#define TD_DEBUG_POINT(p) gDebugStack.push_back(new TdDebugPoint(p));
#define TD_DEBUG_LINE(p0, p1) gDebugStack.push_back(new TdDebugLine(p0, p1));
#ifdef WIN32
#define TD_DEBUG_TEXT(...) { char tmp[1024]; sprintf_s(tmp, __VA_ARGS__); gDebugStack.push_back(new TdDebugText(tmp)); }
#else
#define TD_DEBUG_TEXT(...) { char tmp[1024]; snprintf(tmp, 1024, __VA_ARGS__); gDebugStack.push_back(new TdDebugText(tmp)); }
#endif

class TdDebugScope
{
public:
	inline TdDebugScope() { TD_DEBUG_PUSH(); }
	inline ~TdDebugScope() { TD_DEBUG_POP(); }
};

#define TD_DEBUG_SCOPE() TdDebugScope __tdDebugScope;

#else

#define TD_DEBUG_CLEAR() (void(0))
#define TD_DEBUG_PUSH() (void(0))
#define TD_DEBUG_POP() (void(0))
#define TD_DEBUG_PAUSE() (void(0))
#define TD_DEBUG_TEXT(...) (void(0))
#define TD_DEBUG_CSO() (void(0))
#define TD_DEBUG_SIMPLEX(points, count) (void(0))
#define TD_DEBUG_POINT(p) (void(0))
#define TD_DEBUG_LINE(p0, p1) (void(0))
#define TD_DEBUG_SCOPE() (void(0))

#endif

