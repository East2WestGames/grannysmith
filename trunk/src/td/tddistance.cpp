/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#include "td.h"
#include "tdshape.h"
#include "tdgjk.h"
#include "tdepa.h"
#include "tdcontext.h"
#include "tddebug.h"

//Input query flags:
// TD_QUERY_COMPUTE_NORMAL
// TD_QUERY_COMPUTE_POINTS
// TD_QUERY_HANDLE_PENETRATION
// TD_QUERY_USE_MAX_DISTANCE

//Output status flags:
//TD_RESULT_HAS_DISTANCE
//TD_RESULT_HAS_NORMAL
//TD_RESULT_HAS_POINTS
//TD_RESULT_SEPARATION
//TD_RESULT_PENETRATION
//TD_RESULT_UNRELIABLE
//TD_RESULT_FAILED

inline static void tdDistanceInternal(int context, const tdDistanceQuery* query, tdDistanceResult* result)
{
	TdContextInternal* ctxt = (TdContextInternal*)context;

	TD_DEBUG_CLEAR();
	TD_DEBUG_TEXT("Distance query");
	TD_DEBUG_CSO();

	result->status = 0;

	TdCso cso;
	cso.init(query->shape0, query->shape1, &query->transform);

	TdGjk gjk(ctxt->tolerance, ctxt->maxIterations);
	gjk.init(&cso);
	
	if (query->flags & TD_QUERY_USE_MAX_DISTANCE)
		gjk.mMaxDistance = query->maxDistance;
	
	if (query->flags & TD_QUERY_LOAD_SIMPLEX)
		gjk.computeDistance((GjkSimplex*)query->simplex);
	else
		gjk.computeDistance(NULL);

	if (query->flags & TD_QUERY_STORE_SIMPLEX)
		gjk.getSimplex((GjkSimplex&)*query->simplex);

	if (gjk.mMaxDistanceReached)
	{
		result->distance = query->maxDistance;
		result->status |= TD_RESULT_MAX_DISTANCE_REACHED;
		return;
	}

	//We cannot use warm starting for EPA, since
	//all vertices must lie on the surface
	//Note that this can in rare cases switch to non-penetration
	//That's why hasPenetration has to be called again afterwards
	if (gjk.hasPenetration() && query->simplex)
		gjk.computeDistance(NULL);

	if (!gjk.hasPenetration())
	{
		TD_DEBUG_TEXT("Separation");

		result->status |= TD_RESULT_SEPARATION;
		gjk.getDistanceAndNormal(result->distance, (TdVec3&)result->normal);
		result->status |= TD_RESULT_HAS_DISTANCE;
		result->status |= TD_RESULT_HAS_NORMAL;

		TD_DEBUG_LINE(TdVec3(0,0,0), -((TdVec3&)result->normal)*result->distance);

		//Closest point
		if (query->flags & TD_QUERY_COMPUTE_POINTS)
		{
			gjk.getClosestPoints(NULL, (TdVec3*)&result->point0, (TdVec3*)&result->point1);
			result->status |= TD_RESULT_HAS_POINTS;
		}
		
		if (gjk.mMaxIterationsReached)
			result->status |= TD_RESULT_UNRELIABLE;
	}
	else
	{
		TD_DEBUG_TEXT("Penetration");
		result->status |= TD_RESULT_PENETRATION;
		if (query->flags & TD_QUERY_HANDLE_PENETRATION)
		{				
			if (!gjk.hasPenetration())
			{
				result->status |= TD_RESULT_FAILED;
				return;
			}
			
			TdVec3* p0 = NULL;
			TdVec3* p1 = NULL;
			if (query->flags & TD_QUERY_COMPUTE_POINTS)
			{
				p0 = (TdVec3*)&result->point0;
				p1 = (TdVec3*)&result->point1;
				result->status |= TD_RESULT_HAS_POINTS;
			}
			
			TD_DEBUG_PUSH();
			TdEpa epa(ctxt->tolerance, ctxt->maxIterations);
			TdEpa::eStatus status = epa.Evaluate(gjk, result->distance, (TdVec3&)result->normal, p0, p1);
			TD_DEBUG_POP();
			
			if (status != TdEpa::StatusFailed)
			{
				TD_DEBUG_TEXT("Epa succeeded");
				result->status |= TD_RESULT_HAS_DISTANCE;
				result->status |= TD_RESULT_HAS_NORMAL;

				//Mark as unreliable if EPA is 
				if (status != TdEpa::StatusValid && status != TdEpa::StatusAccuraryReached && status != TdEpa::StatusTouching)
					result->status |= TD_RESULT_UNRELIABLE;

				TD_DEBUG_TEXT("EPA Distance %f", result->distance);
				TD_DEBUG_LINE(TdVec3(0,0,0), result->normal);
				if (true) //TODO: Check context for refinement routine
				{
					//Refine EPA output using MPR. This is essential to find accurate penetration depth
					TdMpr mpr(ctxt->tolerance, ctxt->maxIterations);
					mpr.init(&cso, result->normal, *epa.mOuter.c[0], *epa.mOuter.c[1], *epa.mOuter.c[2]);
					mpr.estimatePenetrationDepth();
					//TODO: Check here if the portal actually changed before recomping everything
					float t0, t1;
					float res = mpr.getDistanceToPortal(t0, t1);
					result->normal = mpr.mDir;
					result->distance = -res;

					if (query->flags & TD_QUERY_COMPUTE_POINTS)
					{
						mpr.getPoints(t0, t1, NULL, (TdVec3*)&result->point0, (TdVec3*)&result->point1);
						result->status |= TD_RESULT_HAS_POINTS;
					}
				}
				TD_DEBUG_LINE(TdVec3(0,0,0), -TdVec3(result->normal)*result->distance);
			}
			else
			{
				TD_DEBUG_TEXT("Epa failed");
				//Backup routine, search from geometric center and refine using hit normal
				result->status |= TD_RESULT_UNRELIABLE;
				
				TdMpr mpr(ctxt->tolerance, ctxt->maxIterations);
				mpr.init(&cso);
				mpr.estimatePenetrationDepth();
				float t0, t1;
				float res = mpr.getDistanceToPortal(t0, t1);
				result->normal = mpr.mDir;
				result->distance = -res;
				result->status |= TD_RESULT_HAS_DISTANCE;
				result->status |= TD_RESULT_HAS_NORMAL;

				TD_DEBUG_LINE(TdVec3(0,0,0), -mpr.mDir*result->distance);

				//Fill in points
				if (query->flags & TD_QUERY_COMPUTE_POINTS)
				{
					TdVec3 cs(false);
					mpr.getPoints(t0, t1, &cs, (TdVec3*)&result->point0, (TdVec3*)&result->point1);
					result->status |= TD_RESULT_HAS_POINTS;

					TD_DEBUG_POINT(cs);
				}
			}
		}
	}
}

//This wrapper replaces sphere with points and capsules with lines and compensates the result
void tdDistance(int context, const tdDistanceQuery* query, tdDistanceResult* result)
{
	bool useProxy = query->shape0->type == TD_SHAPE_TYPE_SPHERE || 
					query->shape1->type == TD_SHAPE_TYPE_SPHERE ||
					query->shape0->type == TD_SHAPE_TYPE_CAPSULE || 
					query->shape1->type == TD_SHAPE_TYPE_CAPSULE;

	if (!useProxy)
	{
		tdDistanceInternal(context, query, result);
	}
	else
	{
		tdDistanceQuery q = *query;
		tdShape proxy0;
		tdShape proxy1;
		float add0 = 0.0f;
		float add1 = 0.0f;

		if (query->shape0->type == TD_SHAPE_TYPE_SPHERE)
		{
			add0 = query->shape0->sphere.radius;
			proxy0.type = TD_SHAPE_TYPE_POINT;
			q.shape0 = &proxy0;
			q.maxDistance += add0;
		}
		else if (query->shape0->type == TD_SHAPE_TYPE_CAPSULE)
		{
			add0 = query->shape0->capsule.radius;
			proxy0 = *query->shape0;
			proxy0.capsule.radius = 0;
			q.shape0 = &proxy0;
			q.maxDistance += add0;
		}
		
		if (query->shape1->type == TD_SHAPE_TYPE_SPHERE)
		{
			add1 = query->shape1->sphere.radius;
			proxy1.type = TD_SHAPE_TYPE_POINT;
			q.shape1 = &proxy1;
			if (q.transform.type == TD_TRANSFORM_TYPE_M)
			{
				//In the very special case of a sphere as second object
				//We can switch to the cheaper P transform since rotation
				//does not matter.
				q.transform.type = TD_TRANSFORM_TYPE_P;
				q.transform.m[0] = q.transform.m[12];
				q.transform.m[1] = q.transform.m[13];
				q.transform.m[2] = q.transform.m[14];
			}
			q.maxDistance += add1;
		}
		else if (query->shape1->type == TD_SHAPE_TYPE_CAPSULE)
		{
			add1 = query->shape1->capsule.radius;
			proxy1 = *query->shape1;
			proxy1.capsule.radius = 0;
			q.shape1 = &proxy1;
			q.maxDistance += add1;
		}
	
		tdDistanceInternal(context, &q, result);

		if (add0)
		{
			result->distance -= add0;
			if (result->status & TD_RESULT_HAS_POINTS)
				(TdVec3&)result->point0 += ((TdVec3&)result->normal) * add0;
		}
	
		if (add1)
		{
			result->distance -= add1;
			if (result->status & TD_RESULT_HAS_POINTS)
				(TdVec3&)result->point1 -= TdTransformInvVec(&query->transform, ((TdVec3&)result->normal) * add1);
		}
	
		if (result->distance < 0.0f)
		{
			result->status &= ~TD_RESULT_SEPARATION;
			result->status |= TD_RESULT_PENETRATION;
		}
	}
}

