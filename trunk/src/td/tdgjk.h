/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#pragma once

#include "td.h"
#include "tdconfig.h"
#include "tdmath.h"
#include "tdshape.h"
#include "tdcso.h"
#include "tddebug.h"


inline static void getTriangleHit(const TdVec3& p, const TdVec3& edge0, const TdVec3& edge1, float& t0, float& t1)
{
	float fA00 = edge0.lengthSquared();
	float fA01 = dot(edge0, edge1);
	float fA11 = edge1.lengthSquared();
	float fB0 = dot(p, edge0);
	float fB1 = dot(p, edge1);
	float fDet = TdAbs(fA00*fA11-fA01*fA01);
	t0 = fA01*fB1-fA11*fB0;
	t1 = fA01*fB0-fA00*fB1;
	float fInvDet = (1.0f)/fDet;
	t0 *= fInvDet;
	t1 *= fInvDet;
}

inline void getLineHit(const TdVec3& p0, const TdVec3& p1, float& t)  // VERIFY
{
	float l = length(p1-p0);
	TD_ASSERT(l != 0.0f, "Invalid line segment");
	TdVec3 d = (p1-p0)/l;
	t = -dot(d, p0)/l;
}

class GjkSimplex
{
public:
	inline GjkSimplex() : count(0) {}
	int count;
	TdVec3 points0[4];
	TdVec3 points1[4];
};

class TdGjk
{
public:
	const TdCso* mCso;
	TdVec3 mNormal;

	TdSimplexPoint mSimplex[4];
	int mSimplexSize;

	bool mPenetration;
	float mEps;
	int mMaxIterations;
	bool mMaxIterationsReached;
	bool mMaxDistanceReached;
	float mMaxDistance;
		
	inline TdGjk() : 
	mSimplexSize(0), mPenetration(false), mEps(0.001f), mMaxIterations(32), mMaxIterationsReached(false)
	{
	}
	
	inline TdGjk(float tolerance, int maxIterations) :
	mSimplexSize(0), mPenetration(false), mEps(tolerance), mMaxIterations(maxIterations), mMaxIterationsReached(false)
	{
	}

	inline void init(const TdCso* cso) 
	{
		mCso = cso;
		mMaxDistance = TD_FLOAT_MAX;
		mMaxDistanceReached = false;
	}
	
	inline bool hasPenetration() const
	{
		return mPenetration;
	}
	
	inline void getClosestPoints(TdVec3* cs, TdVec3* p0, TdVec3* p1)
	{
		switch(mSimplexSize)
		{
			case 1:
			{
				if (p0)
					*p0 = mSimplex[0].p0;
				if (p1)
					*p1 = mSimplex[0].p1;
				if (cs)
					*cs = mSimplex[0].cs;
			}
			break;

			case 2:
			{
				float t;
				getLineHit(mSimplex[0].cs, mSimplex[1].cs, t);
				if (p0)
					*p0 = mSimplex[0].p0*(1.0f-t) + mSimplex[1].p0*t;
				if (p1)
					*p1 = mSimplex[0].p1*(1.0f-t) + mSimplex[1].p1*t;
				if (cs)
					*cs = mSimplex[0].cs*(1.0f-t) + mSimplex[1].cs*t;
			}
			break;

			case 3:
			{
				float dist0 = lengthSquared(mSimplex[0].cs - mSimplex[1].cs);
				float dist1 = lengthSquared(mSimplex[0].cs - mSimplex[2].cs);
				float dist2 = lengthSquared(mSimplex[1].cs - mSimplex[2].cs);
				if (dist0 > dist2*4 && dist1 > dist2*4)
				{
					TdSimplexPoint tmp = mSimplex[0];
					mSimplex[0] = mSimplex[1];
					mSimplex[1] = mSimplex[2];
					mSimplex[2] = tmp;
				}
				
				float t0, t1;
				TdVec3 e0 = mSimplex[1].cs - mSimplex[0].cs;
				TdVec3 e1 = mSimplex[2].cs - mSimplex[0].cs;
				getTriangleHit(mSimplex[0].cs, e0, e1, t0, t1);
				if (cs)
					*cs = mSimplex[0].cs + e0*t0 + e1*t1;

				if (p0)
				{
					e0 = mSimplex[1].p0 - mSimplex[0].p0;
					e1 = mSimplex[2].p0 - mSimplex[0].p0;
					*p0 = mSimplex[0].p0 + e0*t0 + e1*t1;
				}
			
				if (p1)
				{
					e0 = mSimplex[1].p1 - mSimplex[0].p1;
	 				e1 = mSimplex[2].p1 - mSimplex[0].p1;
					*p1 = mSimplex[0].p1 + e0*t0 + e1*t1;
				}
			}
			break;
		}
	}
	
	inline bool doDistanceIteration()
	{
		switch(mSimplexSize)
		{
			case 0:
			{
				TdVec3 dir = -mCso->getMidPoint();
				mCso->getSupport(dir, mSimplex[0]);

				if (mMaxDistance < TD_FLOAT_MAX)
				{
					if (dot(dir, mSimplex[0].cs) < -mMaxDistance*manhattanDistance(dir))
					{
						TD_DEBUG_TEXT("Max Distance reached. Early out.");
						mMaxDistanceReached = true;
						return true;
					}
				}
				
				mSimplexSize = 1;
				return false;
			}
			break;
			case 1:
			{
				TdVec3 dir = -mSimplex[0].cs;
				mCso->getSupport(dir, mSimplex[1]);
				if (lengthSquared(mSimplex[0].cs - mSimplex[1].cs) < mEps*mEps)
				{
					TD_DEBUG_TEXT("Terminating at 1-simplex");
					TD_DEBUG_POINT(mSimplex[1].cs);
					return true;
				}

				if (mMaxDistance < TD_FLOAT_MAX)
				{
					if (dot(dir, mSimplex[1].cs) < -mMaxDistance*manhattanDistance(dir))
					{
						TD_DEBUG_TEXT("Max Distance reached. Early out.");
						mMaxDistanceReached = true;
						return true;
					}
				}

				//Check for degenerate case when origin lies on line
				if ( dot(mSimplex[0].cs, mSimplex[1].cs) < 0.0f)
				{
					if (lengthSquared(cross(mSimplex[0].cs, mSimplex[1].cs)) < mEps*mEps)
					{
						TdVec3 x = perpendicular(normalize(dir));
						mSimplex[1].cs += x*mEps;
					}
				}

				mSimplexSize = 2;
				return false;
			}
			break;
			case 2:
			{
				TdVec3 normEdge = normalize(mSimplex[1].cs - mSimplex[0].cs);
				TdVec3 dir = -(mSimplex[0].cs - normEdge*dot(normEdge, mSimplex[0].cs)); // VERIFY
				mCso->getSupport(dir, mSimplex[2]);

				TdVec3 toNew = mSimplex[2].cs - mSimplex[0].cs;
				if (lengthSquared(toNew-normEdge*dot(normEdge,toNew)) < mEps*mEps)
				{
					TD_DEBUG_TEXT("Terminating at 2-simplex");
					TD_DEBUG_POINT(mSimplex[2].cs);
					return true;
				}

				if (mMaxDistance < TD_FLOAT_MAX)
				{
					if (dot(dir, mSimplex[2].cs) < -mMaxDistance*manhattanDistance(dir))
					{
						TD_DEBUG_TEXT("Max Distance reached. Early out.");
						mMaxDistanceReached = true;
						return true;
					}
				}

				//Compute normal and flip triangle if not pointing at origin
				TdVec3 e0 = mSimplex[1].cs - mSimplex[0].cs;
				TdVec3 e1 = mSimplex[2].cs - mSimplex[1].cs;
				mNormal = cross(e0, e1);
				if (dot(mNormal, mSimplex[0].cs) > 0.0f)
				{
					TdSwap(mSimplex[0], mSimplex[1]);
					mNormal = -mNormal;
				}
				
				TD_DEBUG_LINE(TdVec3(0,0,0), mNormal);
				TD_DEBUG_PAUSE();
								
				mSimplexSize = 3;
				return false;
			}
			break;
			case 3:
			{
				TdVec3 dir = mNormal / manhattanDistance(mNormal);
				mCso->getSupport(dir, mSimplex[3]);

				if (dot(dir, mSimplex[3].cs-mSimplex[0].cs) < mEps)
				{
					TD_DEBUG_TEXT("Terminating at 3-simplex");
					TD_DEBUG_POINT(mSimplex[3].cs);
					return true;
				}

				if (mMaxDistance < TD_FLOAT_MAX)
				{
					if (dot(dir, mSimplex[3].cs) < -mMaxDistance)
					{
						TD_DEBUG_TEXT("Max Distance reached. Early out.");
						mMaxDistanceReached = true;
						return true;
					}
				}
				mSimplexSize = 4;
				return false;
			}
			break;
		}		
		return false;
	}

	//All regions need to be checked at the initial simplex reduction
	//but not while iterating GJK
	inline void reduceSimplex(bool checkAllRegions=false)
	{
		switch(mSimplexSize)
		{
			case 2:
			{
				//If origin is beyond simplex 1, reduce to point
				if (dot(mSimplex[1].cs, mSimplex[0].cs-mSimplex[1].cs) > 0.0f)
				{
					mSimplex[0] = mSimplex[1];
					mSimplexSize = 1;
					return;
				}
				if (checkAllRegions)
				{
					if (dot(mSimplex[0].cs, mSimplex[1].cs- mSimplex[0].cs) > 0.0f) //VERIFY
					{
						mSimplexSize = 1;
						return;
					}
				}
			}
			break;
			
			case 3:
			{
				TdVec3 e1 = mSimplex[2].cs - mSimplex[1].cs;
				TdVec3 e2 = mSimplex[0].cs - mSimplex[2].cs;
				bool outsideEdge1 = (dot(mSimplex[1].cs, cross(mNormal, e1)) > 0.0f);
				bool outsideEdge2 = (dot(mSimplex[2].cs, cross(mNormal, e2)) > 0.0f);
				
				if (outsideEdge1 && outsideEdge2)
				{
					bool w1 = (dot(e1, mSimplex[2].cs) < 0.0f);
					bool w2 = (dot(e2, mSimplex[2].cs) > 0.0f);
					if (w1 && w2)
					{
						mSimplex[0] = mSimplex[2];
						mSimplexSize = 1;
						return;
					}
					if (!w1)
					{
						if (checkAllRegions)
						{
							if (dot(e1, mSimplex[1].cs) > 0.0f)
							{
								mSimplex[0] = mSimplex[1];
								mSimplexSize = 1;
								return;
							}
						}
						mSimplex[0] = mSimplex[1];
						mSimplex[1] = mSimplex[2];
						mSimplexSize = 2;
						return;	
					}
					if (!w2)
					{
						if (checkAllRegions)
						{
							if (dot(e2, mSimplex[0].cs) < 0.0f)
							{
								mSimplex[0] = mSimplex[1];
								mSimplexSize = 1;
								return;
							}
						}
						mSimplex[1] = mSimplex[2];
						mSimplexSize = 2;
						return;	
					}
				}
				else if (outsideEdge1)
				{
					//Check voronoi regions of edge 1
					if (dot(e1, mSimplex[2].cs) < 0.0f)
					{
						mSimplex[0] = mSimplex[2];
						mSimplexSize = 1;
						return;
					}
					if (checkAllRegions)
					{
						if (dot(e1, mSimplex[1].cs) > 0.0f)
						{
							mSimplex[0] = mSimplex[1];
							mSimplexSize = 1;
							return;
						}
					}
					mSimplex[0] = mSimplex[1];
					mSimplex[1] = mSimplex[2];
					mSimplexSize = 2;
					return;	
				}
				else if (outsideEdge2)
				{
					//Check voronoi regions of edge 2
					if (dot(e2, mSimplex[2].cs) > 0.0f)
					{
						mSimplex[0] = mSimplex[2];
						mSimplexSize = 1;
						return;
					}
					if (checkAllRegions)
					{
						if (dot(e2, mSimplex[0].cs) < 0.0f)
						{
							mSimplexSize = 1;
							return;
						}
					}
					mSimplex[1] = mSimplex[2];
					mSimplexSize = 2;
					return;	
				}
				if (checkAllRegions)
				{
					//Check the voronoi regions of edge 0
					TdVec3 e0 = mSimplex[1].cs-mSimplex[0].cs;
					bool outsideEdge0 = (dot(mSimplex[0].cs, cross(mNormal, e0)) > 0.0f);
					if (outsideEdge0)
					{
						if (dot(e0, mSimplex[0].cs) > 0.0f)
						{
							mSimplexSize = 1;
							return;
						}
						else if (dot(e0, mSimplex[1].cs) < 0.0f)
						{
							mSimplex[0] = mSimplex[1];
							mSimplexSize = 1;
							return;
						}
						else
						{
							mSimplexSize = 2;
							return;
						}
					}
				}
			}
			break;

			case 4:
			{
				TdVec3 n0=cross(mSimplex[0].cs - mSimplex[3].cs, mSimplex[1].cs - mSimplex[3].cs);	//VERIFY
				TdVec3 n1=cross(mSimplex[1].cs - mSimplex[3].cs, mSimplex[2].cs - mSimplex[3].cs);
				TdVec3 n2=cross(mSimplex[2].cs - mSimplex[3].cs, mSimplex[0].cs - mSimplex[3].cs);
				bool onSide0 = (dot(mSimplex[3].cs, n0) < 0.0f);
				bool onSide1 = (dot(mSimplex[3].cs, n1) < 0.0f);
				bool onSide2 = (dot(mSimplex[3].cs, n2) < 0.0f);
				if (!onSide0 && !onSide1 && !onSide2)
				{
					mPenetration = true;
					return;
				}
				bool outsideEdge00 = true;
				bool outsideEdge01 = true;
				bool outsideEdge10 = true;
				bool outsideEdge11 = true;
				bool outsideEdge20 = true;
				bool outsideEdge21 = true;
				if (onSide0)
				{
					//Check if on voronoi of triangle 0
					TdVec3 edge0 = mSimplex[0].cs - mSimplex[3].cs;
					TdVec3 edge1 = mSimplex[3].cs - mSimplex[1].cs;
					outsideEdge00 = (dot(mSimplex[3].cs, cross(n0, edge0)) > 0.0f);
					outsideEdge01 = (dot(mSimplex[1].cs, cross(n0, edge1)) > 0.0f);
					if (!outsideEdge00 && !outsideEdge01)
					{
						mSimplex[2] = mSimplex[3];
						mNormal = n0;
						mSimplexSize = 3;
						return;
					}
				}
				if (onSide1)
				{
					//Check if on voronoi of triangle 1
					TdVec3 edge0 = mSimplex[1].cs - mSimplex[3].cs;
					TdVec3 edge1 = mSimplex[3].cs - mSimplex[2].cs;
					outsideEdge10 = (dot(mSimplex[3].cs, cross(n1, edge0)) > 0.0f);
					outsideEdge11 = (dot(mSimplex[2].cs, cross(n1, edge1)) > 0.0f);
					if (!outsideEdge10 && !outsideEdge11)
					{
						mSimplex[0] = mSimplex[1];
						mSimplex[1] = mSimplex[2];
						mSimplex[2] = mSimplex[3];
						mNormal = n1;
						mSimplexSize = 3;
						return;
					}
				}
				if (onSide2)
				{
					//Check if on voronoi of triangle 2
					TdVec3 edge0 = mSimplex[2].cs - mSimplex[3].cs;
					TdVec3 edge1 = mSimplex[3].cs - mSimplex[0].cs;
					outsideEdge20 = (dot(mSimplex[3].cs, cross(n2, edge0)) > 0.0f);
					outsideEdge21 = (dot(mSimplex[0].cs, cross(n2, edge1)) > 0.0f);
					if (!outsideEdge20 && !outsideEdge21)
					{
						mSimplex[1] = mSimplex[0];
						mSimplex[0] = mSimplex[2];
						mSimplex[2] = mSimplex[3];
						mNormal = n2;
						mSimplexSize = 3;
						return;
					}
				}
				
				//Check edges
				if (outsideEdge01 && outsideEdge10)
				{
					TdVec3 e = mSimplex[1].cs - mSimplex[3].cs;
					if (dot(mSimplex[3].cs, e) < 0.0f)
					{
						mSimplex[0] = mSimplex[1];
						mSimplex[1] = mSimplex[3];
						mSimplexSize = 2;
						return;
					}
				}
				if (outsideEdge11 && outsideEdge20)
				{
					TdVec3 e = mSimplex[2].cs - mSimplex[3].cs;
					if (dot(mSimplex[3].cs, e) < 0.0f)
					{
						mSimplex[0] = mSimplex[2];
						mSimplex[1] = mSimplex[3];
						mSimplexSize = 2;
						return;
					}
				}
				if (outsideEdge21 && outsideEdge00)
				{
					TdVec3 e = mSimplex[0].cs - mSimplex[3].cs;
					if (dot(mSimplex[3].cs, e) < 0.0f)
					{
						mSimplex[1] = mSimplex[3];
						mSimplexSize = 2;
						return;
					}
				}

				//It must be a vertex case if we get this far
				mSimplex[0] = mSimplex[3];
				mSimplexSize = 1;
				return;
			}
			break;
		}		
	}
	
	inline void getDistanceAndNormal(float& distance, TdVec3& normal)
	{
		switch(mSimplexSize)
		{
			case 1:
			normal = normalize(-mSimplex[0].cs, distance);
			break;

			case 2:	//VERIFY
			{
				TdVec3 edge = mSimplex[1].cs - mSimplex[0].cs;
				TdVec3 edgeNorm = normalize(edge);
				TdVec3 perp = mSimplex[0].cs - edgeNorm*dot(edgeNorm, mSimplex[0].cs);
				normal = -normalize(perp, distance);
				break;
			}

			case 3:
			{
				normal = normalize(mNormal);
				distance = dot(normal, -mSimplex[0].cs);
				break;
			}

			default:
			distance = 0.0f;
			normal.set(1.0f, 0.0f, 0.0f);
		}
	}

	inline void computeDistance(const GjkSimplex* lastSimplex = NULL)
	{
		mMaxDistanceReached = false;
		mPenetration = false;
		mSimplexSize = 0;
		mMaxIterationsReached = false;
		if (lastSimplex)
		{
			setInitialSimplex(*lastSimplex);
			if (mPenetration)
				return;
		}
		int i;
		for(i=0; i<mMaxIterations; i++)
		{
			TD_DEBUG_SCOPE();
			TD_DEBUG_TEXT("Iteration: " + i);
			
			{
				TD_DEBUG_SCOPE();
				bool xit = doDistanceIteration();
				TD_DEBUG_SIMPLEX(mSimplex, mSimplexSize);
				TD_DEBUG_TEXT("Simplex size: %", mSimplexSize);
				TD_DEBUG_PAUSE();
				if (xit)
					return;
			}

			{
				TD_DEBUG_SCOPE();
				reduceSimplex();
				TD_DEBUG_SIMPLEX(mSimplex, mSimplexSize);
				TD_DEBUG_TEXT("Simplex size: %i", mSimplexSize);
				
				#ifdef TD_DEBUG
				{
				TdVec3 n;
				float d;
				getDistanceAndNormal(d, n);
				TD_DEBUG_TEXT("Distance: %f", d);
				TD_DEBUG_LINE(TdVec3(0,0,0), -n*d);
				}
				#endif

				TD_DEBUG_PAUSE();
			}

			if (mPenetration)
				return;
		}
		mMaxIterationsReached = true;
	}

	inline void setInitialSimplex(const GjkSimplex& simplex)
	{
		TD_DEBUG_SCOPE();
		TD_DEBUG_TEXT("setInitialSimplex");
		mSimplexSize = simplex.count;
		for(int i=0; i<mSimplexSize; i++)
		{
			mSimplex[i].p0 = simplex.points0[i];
			mSimplex[i].p1 = simplex.points1[i];
			mSimplex[i].cs = mCso->getConfigurationSpacePoint(mSimplex[i].p0, mSimplex[i].p1);
		}

		TD_DEBUG_SIMPLEX(mSimplex, mSimplexSize);
		TD_DEBUG_PAUSE();

		switch(mSimplexSize)
		{
			case 4:
			{
				//Check for containment and redo from scratch if not on inside
				//TODO: Checking voronoi regions of the three bottom edges might be faster than doing this
				TdVec3 n0 = cross(mSimplex[0].cs - mSimplex[3].cs, mSimplex[1].cs - mSimplex[3].cs);
				TdVec3 n1 = cross(mSimplex[1].cs - mSimplex[3].cs, mSimplex[2].cs - mSimplex[3].cs);
				TdVec3 n2 = cross(mSimplex[2].cs - mSimplex[3].cs, mSimplex[0].cs - mSimplex[3].cs);
				TdVec3 n3 = cross(mSimplex[2].cs - mSimplex[0].cs, mSimplex[1].cs - mSimplex[0].cs);
				bool onSide0 = (dot(mSimplex[3].cs, n0) < 0.0f);
				bool onSide1 = (dot(mSimplex[3].cs, n1) < 0.0f);
				bool onSide2 = (dot(mSimplex[3].cs, n2) < 0.0f);
				bool onSide3 = (dot(mSimplex[0].cs, n3) < 0.0f);
				if (!onSide0 && !onSide1 && !onSide2 && !onSide3)
				{
					TD_DEBUG_TEXT("4-simplex in penetration");
					TD_DEBUG_PAUSE();
					mPenetration = true;
					return;
				}

				TD_DEBUG_TEXT("4-simplex not in penetration. Clearing simplex");
				TD_DEBUG_PAUSE();
				mSimplexSize = 3;
				
				//Note that there is intentionally no break here
				//We need to handle 4-simplex that turned into 3-simplex
			}
				
			case 3:
			{
				//Compute normal and flip triangle if not pointing at origin
				TdVec3 e0 = mSimplex[1].cs - mSimplex[0].cs;
				TdVec3 e1 = mSimplex[2].cs - mSimplex[1].cs;
				mNormal = cross(e0, e1);
				if (mNormal.lengthSquared() < mEps*mEps)
				{
					//Triangle has degenerated into line
					mSimplexSize = 1;
					return;
				}
				if (dot(mSimplex[0].cs, mNormal) > 0.0f)
				{
					TD_DEBUG_TEXT("Swapping 3-simplex");
					TD_DEBUG_PAUSE();
					TdSwap(mSimplex[0], mSimplex[1]);
					mNormal = -mNormal;
					break;
				}
			}
			break;

			case 2:
			//Check for degenerate case
			if (lengthSquared(mSimplex[1].cs - mSimplex[0].cs) < mEps*mEps)
			{
				mSimplexSize = 1;
				return;
			}
			break;
		}
		reduceSimplex(true);
		TD_DEBUG_TEXT("Simplex size after reduction: %i", mSimplexSize);
		TD_DEBUG_PAUSE();
	}
	
	inline bool getSimplex(GjkSimplex& simplex)
	{
		if (mMaxDistanceReached)
		{
			simplex.count = 0;
			return true;
		}
		else
		{
			simplex.count = mSimplexSize;
			for(int i=0; i<mSimplexSize; i++)
			{
				simplex.points0[i] = mSimplex[i].p0;
				simplex.points1[i] = mSimplex[i].p1;
			}
			return true;
		}
	}
	
	/*
	bool sweep2(const QiDir& dir, float* outDist, QiDir* outNormal, QiPoint* cs, QiPoint* p0, QiPoint* p1)
	{
		QiPoint m0 = tdShapeGetMidPoint(mShape0);
		QiPoint m1 = transform(mTransform, tdShapeGetMidPoint(mShape1));
		float d = m0.getDistanceTo(m1);

		mLinAdd = dir*d;

		computeDistance();
		if (!mPenetration)
		{
			mLinAdd.set(0,0,0);
			return false;
		}

		QiVec n;
		SimplexPoint p[3];
		getPortalFromSimplex(dir, p);
		trackSurface(p, dir, n);
		mLinAdd.set(0,0,0);

		float t0, t1;
		float result = getDistanceToPortal(p, dir, t0, t1);

		if (cs)
			*cs = p[0].cs + p[0].cs.getVectorTo(p[1].cs)*t0 + p[0].cs.getVectorTo(p[2].cs)*t1;
		if (p0)
			*p0 = p[0].p0 + p[0].p0.getVectorTo(p[1].p0)*t0 + p[0].p0.getVectorTo(p[2].p0)*t1;
		if (p1)
			*p1 = p[0].p1 + p[0].p1.getVectorTo(p[1].p1)*t0 + p[0].p1.getVectorTo(p[2].p1)*t1;

		if (outNormal)
			*outNormal = QiDir(n);
		if (outDist)
			*outDist = d-result;
		return true;
	}
	*/
};

