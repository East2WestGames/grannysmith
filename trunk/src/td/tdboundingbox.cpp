/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#include "td.h"
#include "tdconfig.h"
#include "tdmath.h"
#include "tdshape.h"

void tdBoundingBoxAabb(int context, const tdBoundingBoxQuery* query, tdAabb* aabb)
{
	const tdShape* shape = query->shape;

	const tdTransform* transform = &query->transform;

	if (shape->type == TD_SHAPE_TYPE_HULL)
	{
		int st = shape->hull.stride/4;
		const float* v = shape->hull.vertices;
		aabb->lower.x = TD_FLOAT_MAX;
		aabb->lower.y = TD_FLOAT_MAX;
		aabb->lower.z = TD_FLOAT_MAX;
		aabb->upper.x = -TD_FLOAT_MAX;
		aabb->upper.y = -TD_FLOAT_MAX;
		aabb->upper.z = -TD_FLOAT_MAX;
		for(int i=0; i<shape->hull.count; i++)
		{
			TdVec3 p = TdTransformPoint(transform, TdVec3(v[i*st+0], v[i*st+1], v[i*st+2]));
			aabb->lower.x = TdMin(aabb->lower.x, p.x);
			aabb->lower.y = TdMin(aabb->lower.y, p.y);
			aabb->lower.z = TdMin(aabb->lower.z, p.z);
			aabb->upper.x = TdMax(aabb->upper.x, p.x);
			aabb->upper.y = TdMax(aabb->upper.y, p.y);
			aabb->upper.z = TdMax(aabb->upper.z, p.z);
		}
	}
	else
	{
		TdVec3 x = TdTransformInvVec(transform, TdVec3(1,0,0));
		TdVec3 y = TdTransformInvVec(transform, TdVec3(0,1,0));
		TdVec3 z = TdTransformInvVec(transform, TdVec3(0,0,1));
		TdVec3 px = TdTransformPoint(transform, TdShapeGetSupport(shape, x));
		TdVec3 pnx = TdTransformPoint(transform, TdShapeGetSupport(shape, -x));
		TdVec3 py = TdTransformPoint(transform, TdShapeGetSupport(shape, y));
		TdVec3 pny = TdTransformPoint(transform, TdShapeGetSupport(shape, -y));
		TdVec3 pz = TdTransformPoint(transform, TdShapeGetSupport(shape, z));
		TdVec3 pnz = TdTransformPoint(transform, TdShapeGetSupport(shape, -z));	
		aabb->lower.x = pnx.x;
		aabb->lower.y = pny.y;
		aabb->lower.z = pnz.z;
		aabb->upper.x = px.x;
		aabb->upper.y = py.y;
		aabb->upper.z = pz.z;	
	}
	for(int i=0; i<3; i++)
	{
		(&aabb->lower.x)[i] += TdMin(0.0f, (&query->sweep.x)[i]) - query->padding;
		(&aabb->upper.x)[i] += TdMax(0.0f, (&query->sweep.x)[i]) + query->padding;
	}
}


