/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#include "td.h"
#include "tdsolver.h"

#include "tdconstraintcontact.h"

//#define GRAPH_IMPULSES

static const int gThreads = 4;

tdBody gStaticBody = {0};


TdSolver::TdSolver()
{
	gStaticBody.rot.w = 1.0f;
	mDt = 0.02f;
	mIterationCount = 8;
	mConstraintCount = 0;

	//Register constraint types
	memset(mConstraintTypes, 0, TdConstraintType::COUNT*sizeof(TdConstraintType*));
	mConstraintTypes[TdConstraintType::CONTACT] = new TdConstraintTypeContact();
	
	resetState();
	mInitialState = mState;
}

TdSolver::~TdSolver()
{
	//Delete all constraint types
	for(int i=0; i<TdConstraintType::COUNT; i++)
		delete mConstraintTypes[i];
	
	resetState();
}


void TdSolver::setParam(int param, float value)
{
	if (mState.params[param] == value)
		return;
	//TODO: Maybe check if last item was a param change of same type
	if (mItems.size() == 0)
		mInitialState = mState;
	mState.params[param] = value;
	mItems.push_back(TdSolver::Item(param, value));	
}


void TdSolver::setEnabled(int feature, bool value)
{
	if (mState.features[feature] == value)
		return;
	if (mItems.size() == 0)
		mInitialState = mState;
	//TODO: Maybe check if last item was a feature state change of same type
	mState.features[feature] = value;
	mItems.push_back(TdSolver::Item(feature, value));	
}


void TdSolver::resetState()
{
	mState.linAcc.set(0,-10,0);
	mState.angAcc.set(0,0,0);
	
	mState.params[TD_SOLVER_PARAM_MAX_IMPULSE] = TD_FLOAT_MAX;
	mState.params[TD_SOLVER_PARAM_MIN_IMPULSE] = -TD_FLOAT_MAX;
	mState.params[TD_SOLVER_PARAM_FRICTION] = 1.0f;
	mState.params[TD_SOLVER_PARAM_RESTITUTION] = 0.0f;
	mState.params[TD_SOLVER_PARAM_CONTACT_OFFSET] = 0.02f;
	mState.params[TD_SOLVER_PARAM_STRETCH_DAMPING] = 1.0f;
	mState.params[TD_SOLVER_PARAM_STRETCH_STIFFNESS] = 1.0f;
	mState.params[TD_SOLVER_PARAM_BEND_DAMPING] = 1.0f;
	mState.params[TD_SOLVER_PARAM_BEND_STIFFNESS] = 1.0f;
	mState.params[TD_SOLVER_PARAM_FLUID_VISCO_ELASTICITY] = 0.0f;
	mState.params[TD_SOLVER_PARAM_FLUID_PARTICLE_SIZE] = 1.0f;
	
	mState.features[TD_SOLVER_FEATURE_CONTACT_STICKY] = false;
}


#ifdef GRAPH_IMPULSES
#include "qi_file.h"
#endif

class TdTask
{
};

class ProcessConstraints : public TdTask
{
public:
	TdSolver* mSolver;
	TdSolverState mSolverState;
	int mStart;
	int mEnd;

	virtual bool onExecute()
	{
		TD_PROFILE_ZONE("Init constraints");
		for(int i=mStart; i<mEnd; i++)
		{
			TdSolver::Item& item = mSolver->mItems[i];
			switch(item.mType)
			{
				case TdSolver::ITEM_CONSTRAINT:
				mSolver->mConstraintTypes[item.mConstraint.type]->init( mSolver, &mSolverState, item.mConstraint.start, item.mConstraint.start+item.mConstraint.count);
				break;
				
				case TdSolver::ITEM_PARAM:
				mSolverState.params[item.mParam.index] = item.mParam.value;
				break;

				case TdSolver::ITEM_FEATURE:
				mSolverState.features[item.mFeature.index] = item.mFeature.enabled;
				break;

				default:
				break;
			}
		}
		
		return true;
	}
};


class SolverGroup
{
public:
	std::vector<int> mConstraints[gThreads];
};

class SolveGroupTask : public TdTask
{
public:
	TdSolver* mSolver;
	SolverGroup* mGroup;
	int mThread;

	virtual bool onExecute()
	{
		TD_PROFILE_ZONE("Solve group");
		TdConstraintTypeContact* t = (TdConstraintTypeContact*)mSolver->mConstraintTypes[TdConstraintType::CONTACT];
		for(int i=0; i<(int)mGroup->mConstraints[mThread].size(); i++)
			t->solve(mSolver, &mSolver->mState, mGroup->mConstraints[mThread][i], 1);
		return true;
	}
};

class SolveGroupTask2 : public TdTask
{
public:
	TdSolver* mSolver;
	TdSolverState mSolverState;
	int mStart;
	int mEnd;

	virtual bool onExecute()
	{
		TD_PROFILE_ZONE("Solve group");
		TdSolverState state = mSolverState;
		for(int i=mStart; i<mEnd; i++)
		{
			TdSolver::Item& item = mSolver->mItems[i];
			switch(item.mType)
			{
				case TdSolver::ITEM_CONSTRAINT:
				mSolver->mConstraintTypes[item.mConstraint.type]->solve( mSolver, &state, item.mConstraint.start, item.mConstraint.start+item.mConstraint.count);
				break;
				
				case TdSolver::ITEM_PARAM:
				state.params[item.mParam.index] = item.mParam.value;
				break;

				case TdSolver::ITEM_FEATURE:
				state.features[item.mFeature.index] = item.mFeature.enabled;
				break;

				default:
				break;
			}
		}
		
		return true;
	}
};

class ClampAngVelTask : public TdTask
{
public:
	TdSolver* mSolver;
	int mStart;
	int mEnd;

	virtual bool onExecute()
	{
		for(int i=mStart; i<mEnd; i++)
		{
			for(int j=0; j<3; j++)
				mSolver->mBodies[i].mAngVel[j] = TdClamp(mSolver->mBodies[i].mAngVel[j], mSolver->mBodiesExtras[i].mAngVelMin[j], mSolver->mBodiesExtras[i].mAngVelMax[j]);

			//Uncomment for 2d movement
			//mBodies[i].mLinVel[2] = 0.0f;
		}		
		return true;
	}
};

class IntegrateVelTask: public TdTask
{
public:
	TdSolver* mSolver;
	int mStart;
	int mEnd;
	virtual bool onExecute()
	{
		TD_PROFILE_ZONE("Integrate velocity task");
		for(int i=mStart; i<mEnd; i++)
		{
			TdVec3 linAdd = mSolver->mBodies[i].mLinAcc*mSolver->mDt;
			TdVec3 angAdd = mSolver->mBodiesExtras[i].mAngAcc*mSolver->mDt;

			tdTransform t;
			tdTransformInitPQ(&t, &mSolver->mOriginalBodies[i]->pos, &mSolver->mOriginalBodies[i]->rot);
			angAdd = TdTransformInvVecM(&t, angAdd);

			mSolver->mBodies[i].mLinVel += linAdd;
			mSolver->mBodies[i].mAngVel += angAdd;
			mSolver->mOriginalBodies[i]->force = 0.0f;
		}
		return true;
	}
};

class IntegrateTask : public TdTask
{
public:
	TdSolver* mSolver;
	int mThread;
	virtual bool onExecute()
	{
		//Write back velocity and integrate position
		TD_PROFILE_ZONE("Integrate");
		int start = 1 + (mThread*(mSolver->mBodies.size()-1)) / 4;
		int end = 1 + ((mThread+1)*(mSolver->mBodies.size()-1)) / 4;
		for(int i=start; i<end; i++)
		{
			tdBody* body = mSolver->mOriginalBodies[i];

			tdTransform t;
			tdTransformInitPQ(&t, &body->pos, &body->rot);
			mSolver->mBodies[i].mLinVel = TdTransformVecM(&t, mSolver->mBodies[i].mLinVel);
			mSolver->mBodies[i].mAngVel = TdTransformVecM(&t, mSolver->mBodies[i].mAngVel);

			(TdVec3&)body->linVel = mSolver->mBodies[i].mLinVel;
			(TdVec3&)body->angVel = mSolver->mBodies[i].mAngVel;
			(TdVec3&)body->pos += mSolver->mBodies[i].mLinVel*mSolver->mDt;
			if (mSolver->mBodies[i].mAngVel.lengthSquared() > 0.001f)
			{
				TdQuat& rot = (TdQuat&)body->rot;
				float angle = 0.0f;
				TdVec3 axis =normalize(mSolver->mBodies[i].mAngVel, angle);
				rot = TdQuat(axis, mSolver->mDt*angle)*rot;
				rot.normalize();
			}
		}
		start = (mThread*(mSolver->mParticles.size())) / 4;
		end = ((mThread+1)*(mSolver->mParticles.size())) / 4;
		for(int i=start; i<end; i++)
		{
			tdParticle* particles = mSolver->mOriginalParticles[i];
			(TdVec3&)particles->vel = (mSolver->mParticles[i].mVel + (TdVec3&)particles->vel)*0.5f;
			(TdVec3&)particles->pos += mSolver->mParticles[i].mVel*mSolver->mDt;
		}
		return true;
	}
};
//#define SYNC_ITERATION
inline void TdSolver::step(float dt)
{	
	mDt = dt;
	
	//Integrate velocity
	{
		TD_PROFILE_ZONE("Integrate velocity");
		IntegrateVelTask ivt;
		ivt.mStart = 0;
		ivt.mEnd = mBodies.size();
		ivt.mSolver = this;
		ivt.onExecute();
	}

	{
		TD_PROFILE_ZONE("Setup constriants");
		mState = mInitialState;

		ProcessConstraints pc;
		pc.mSolver = this;
		pc.mSolverState = mState;
		pc.mStart = 0;
		pc.mEnd = mItems.size();
		pc.onExecute();
	}

#if 0
	{
		SolveGroupTask2 sgt;
		mState = mInitialState;
		sgt.mSolver = this;
		sgt.mSolverState = mState;
		sgt.mStart = 0;
		sgt.mEnd = mItems.size();
		sgt.onExecute();

		//Clamp ang vel
		{
			TD_PROFILE_ZONE("Clamp angvel");
			ClampAngVelTask cavt;
			cavt.mSolver = this;
			cavt.mStart = 0;
			cavt.mEnd = mBodies.size();
			cavt.onExecute();
		}
	}

#endif
	#ifdef GRAPH_IMPULSES
	float impulses[1000][ITERATIONS];
	int ic = 0;
	#endif

	//Solve constraints
	for(mCurrentIteration=0; mCurrentIteration<mIterationCount; mCurrentIteration++)
	{
		mState = mInitialState;
		TD_PROFILE_ZONE("Solve iteration");
		for(int i=0; i<(int)mItems.size(); i++)
		{
			Item& item = mItems[i];
			switch(mItems[i].mType)
			{
				case ITEM_CONSTRAINT:
				mConstraintTypes[item.mConstraint.type]->solve(this, &mState, item.mConstraint.start, item.mConstraint.start+item.mConstraint.count);
				break;
				
				case ITEM_PARAM:
				mState.params[item.mParam.index] = item.mParam.value;
				break;

				case ITEM_FEATURE:
				mState.features[item.mFeature.index] = item.mFeature.enabled;
				break;

				default:
				break;
			}
		}
		
		#ifdef GRAPH_IMPULSES
		ic = 0;
		for(int i=0; i<mContacts.getCount(); i++)
		{
			for(int j=0; j<mContacts[i].mCount; j++)
			{
				if (ic < 1000)
				{
					TdSolverBody* sb0 = getSolverBody(mContacts[i].mBody0);
					TdSolverBody* sb1 = getSolverBody(mContacts[i].mBody1);
					TdConstraintContact::Point& point = mContacts[i].mPoints[j];
					QiVec pv0 = sb0->getPointVel(point.mPos0); 
					QiVec pv1 = sb1->getPointVel(point.mPos1); 
					float nRelVel = -mContacts[i].mNormal0.dot(pv0) - mContacts[i].mNormal1.dot(pv1);
					float v = nRelVel-point.mTarget;
					
					impulses[ic][mCurrentIteration] = mContacts[i].mPoints[j].mImpulse;
					//impulses[ic][mCurrentIteration] = v;
					ic++;
				}
			}
		}
		#endif
		
		//Clamp ang vel
		for(int i=0; i<(int)mBodies.size(); i++)
		{
			for(int j=0; j<3; j++)
				mBodies[i].mAngVel[j] = TdClamp(mBodies[i].mAngVel[j], mBodiesExtras[i].mAngVelMin[j], mBodiesExtras[i].mAngVelMax[j]);

			//Uncomment for 2d movement
			//mBodies[i].mLinVel[2] = 0.0f;
		}
	}
	
	#ifdef GRAPH_IMPULSES
	QiFileOutputStream fos;
	fos.open("impulses.csv");
	for(int i=0; i<ic; i++)
	{
		QiString row = QiString("Contact ") + i;
		row += "\t";
		for(int j=0; j<ITERATIONS; j++)
		{
			row += impulses[i][j];
			row += "\t";
		}
		fos.writeLine(row);
	}
	fos.close();
	#endif

	IntegrateTask it[4];
	for(int i=0; i<4; i++)
	{
		it[i].mSolver = this;
		it[i].mThread = i;
		it[i].onExecute();
	}

	{
		TD_PROFILE_ZONE("Post solve");
		for(int i=0; i<TdConstraintType::COUNT; i++)
			mConstraintTypes[i]->postSolve(this);
	}

	reset();
}


void* TdSolver::addConstraint(TdConstraintType::Type type)
{
	int pos;
	void* ptr = mConstraintTypes[type]->add(pos);

	if (mItems.size() && mItems.back().mType == ITEM_CONSTRAINT && mItems.back().mConstraint.type == type && mItems.back().mConstraint.count < 16)
		mItems.back().mConstraint.count++;
	else
		mItems.push_back(Item(type, pos, 1));

	mConstraintCount++;
	return ptr;
}


void TdSolver::reset()
{
	mConstraintCount = 0;
	mBodies.clear();
	mBodiesExtras.clear();
	mOriginalBodies.clear();
	mParticles.clear();
	mParticlesExtras.clear();
	mOriginalParticles.clear();
	mItems.clear();

	for(int i=0; i<TdConstraintType::COUNT; i++)
		mConstraintTypes[i]->clear();
	
	tdSolverPushState((int)this);
	mState.linAcc.set(0,0,0);
	tdSolverInsertBody((int)this, &gStaticBody);
	tdSolverPopState((int)this);

	mInitialState = mState;
}


/*************************************************************************
** 								API										**
*************************************************************************/


int tdSolverCreate()
{
	return (int)new TdSolver();
}


void tdSolverDestroy(int solver)
{
	TdSolver* s = (TdSolver*)solver;
	delete s;
}


void tdSolverPushState(int solver)
{
	TdSolver* s = (TdSolver*)solver;
	s->mStateStack.push_back(s->mState);
}


void tdSolverPopState(int solver)
{
	TdSolver* s = (TdSolver*)solver;
	TD_ASSERT(s->mStateStack.size() > 0, "State stack is empty");
	if (s->mStateStack.size() > 0)
	{
		TdSolverState& newState = s->mStateStack.back();
		for(int i=0; i<TD_SOLVER_PARAM_COUNT; i++)
			s->setParam(i, newState.params[i]);
		for(int i=0; i<TD_SOLVER_FEATURE_COUNT; i++)
			s->setEnabled(i, newState.features[i]);
		s->mState = newState;
		s->mStateStack.pop_back();
	}
}


void tdSolverInsertBody(int solver, tdBody* body)
{
	TdSolver* s = (TdSolver*)solver;
	int id = s->mBodies.size();
	TdSolverBody b;
	b.mLinVel = (TdVec3&)body->linVel;
	b.mAngVel = (TdVec3&)body->angVel;

	tdTransform t;
	tdTransformInitPQ(&t, &body->pos, &body->rot);
	b.mLinVel = TdTransformInvVecM(&t, b.mLinVel);
	b.mAngVel = TdTransformInvVecM(&t, b.mAngVel);

	b.mInvMass = body->invMass;
	b.mInvInertia = (TdVec3&)body->invInertia;
	b.mLinAcc = TdTransformInvVecM(&t, s->mState.linAcc);
	s->mBodies.push_back(b);
	s->mOriginalBodies.push_back(body);
	body->internal = id;
	
	float limit = 3.0f;
	TdSolverBodyExtras x;
	for(int i=0; i<3; i++)
	{
		x.mAngVelMin[i] = TdMin(b.mAngVel[i]-limit, -limit); 
		x.mAngVelMax[i] = TdMax(b.mAngVel[i]+limit, limit); 
	}
	x.mAngAcc = s->mState.angAcc;
	s->mBodiesExtras.push_back(x);
}


void tdSolverInsertParticle(int solver, tdParticle* particle)
{
	TdSolver* s = (TdSolver*)solver;
	int id = s->mParticles.size();
	TdSolverParticle p;
	p.mInvMass = particle->invMass;
	p.mVel = particle->vel;
	p.mDensity = 0.0f;
	s->mParticles.push_back(p);
	s->mOriginalParticles.push_back(particle);
	particle->internal = id;

	TdSolverParticleExtras x;
	x.mAcc = s->mState.linAcc;
	s->mParticlesExtras.push_back(x);
}



void tdSolverEnable(int solver, int feature)
{
	TdSolver* s = (TdSolver*)solver;
	s->setEnabled(feature, true);
}


void tdSolverDisable(int solver, int feature)
{
	TdSolver* s = (TdSolver*)solver;
	s->setEnabled(feature, false);
}


int tdSolverIsEnabled(int solver, int feature)
{
	TdSolver* s = (TdSolver*)solver;
	return s->mState.features[feature];

}


void tdSolverResetState(int solver)
{
	TdSolver* s = (TdSolver*)solver;
	s->resetState();
}


void tdSolverSetParam(int solver, int param, float value)
{
	TdSolver* s = (TdSolver*)solver;
	s->setParam(param, value);	
}


float tdSolverGetParam(int solver, int param)
{
	TdSolver* s = (TdSolver*)solver;
	return s->mState.params[param];
}


void tdSolverSetLinAcc(int solver, const tdVec* linAcc)
{
	TdSolver* s = (TdSolver*)solver;
	s->mState.linAcc = *linAcc;
}


void tdSolverSetAngAcc(int solver, const tdVec* angAcc)
{
	TdSolver* s = (TdSolver*)solver;
	s->mState.angAcc = *angAcc;
}


void tdSolverGetLinAcc(int solver, tdVec* linAcc)
{
	TdSolver* s = (TdSolver*)solver;
	*linAcc = s->mState.linAcc;
}


void tdSolverGetAngAcc(int solver, tdVec* angAcc)
{
	TdSolver* s = (TdSolver*)solver;
	*angAcc = s->mState.angAcc;
}


void tdSolverStep(int solver, float dt)
{
	TdSolver* s = (TdSolver*)solver;
	
	//Remember state, since solver program can include state changes
	tdSolverPushState(solver);
	s->step(dt);
	tdSolverPopState(solver);
}


void tdSolverSetIterationCount(int solver, int count)
{
	TdSolver* s = (TdSolver*)solver;
	s->mIterationCount = count;
}


int tdSolverGetIterationCount(int solver)
{
	TdSolver* s = (TdSolver*)solver;
	return s->mIterationCount;
}

tdTransform TdSolver::getBodyTransform(tdBody* body)
{
	tdTransform tmp;
	if (body == NULL)
		tdTransformInit(&tmp);
	else
		tdTransformInitPQ(&tmp, &body->pos, &body->rot);
	return tmp;	
}

tdTransform TdSolver::getBodyTransformDiff(tdBody* body0, tdBody* body1)
{
	TdVec3& wp0 = (TdVec3&)body0->pos;
	TdQuat& wq0 = (TdQuat&)body0->rot;
	TdVec3& wp1 = (TdVec3&)body1->pos;
	TdQuat& wq1 = (TdQuat&)body1->rot;

	TdVec3 p = wq0.rotateInv(wp1 - wp0);
	TdQuat q = wq0.getConjugate() * wq1;

	tdTransform tmp;
	tdTransformInitPQ(&tmp, (tdVec*)&p, (tdQuat*)&q);
	return tmp;
}

