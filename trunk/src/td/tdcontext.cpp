/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#include "td.h"
#include "tdcontext.h"

int DEFAULT_MAX_ITERATIONS = 32;
float DEFAULT_TOLERANCE = 0.001f;


TdContextInternal::TdContextInternal()
{
}


TdContextInternal::~TdContextInternal()
{
}

int tdContextCreate(tdMalloc customMalloc, tdFree customFree)
{
	if (!customMalloc)
		customMalloc = malloc;
	if (!customFree)
		customFree = free;
	TdContextInternal* ctxt = (TdContextInternal*)customMalloc(sizeof(TdContextInternal));
	new(ctxt) TdContextInternal;

	//Init context
	ctxt->customMalloc = customMalloc;
	ctxt->customFree = customFree;

	tdContextSetMaxIterations((int)ctxt, DEFAULT_MAX_ITERATIONS);
	tdContextSetTolerance((int)ctxt, DEFAULT_TOLERANCE);

	return (int)ctxt;
}


void tdContextSetMaxIterations(int context, int count)
{
	TdContextInternal* ctxt = (TdContextInternal*)context;
	ctxt->maxIterations = count;
}


void tdContextSetTolerance(int context, float tolerance)
{
	TdContextInternal* ctxt = (TdContextInternal*)context;
	ctxt->tolerance = tolerance;
}


void tdContextDestroy(int context)
{
	TdContextInternal* ctxt = (TdContextInternal*)context;
	ctxt->~TdContextInternal();
	ctxt->customFree((void*)context);
}



