/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#include "tdconstraintcontact.h"
#include "tdsolver.h"


void tdSolverInsertContactBB(int solver, const tdConstraintContactBB* constraint)
{
	TdSolver* s = (TdSolver*)solver;
	*(tdConstraintContactBB*)s->addConstraint(TdConstraintType::CONTACT) = *constraint;
}


void TdConstraintTypeContact::init(TdSolver* s, const class TdSolverState* state, int start, int end)
{	
	for(int i=start; i<end; i++)
	{
		tdConstraintContactBB* constraint = &mApiConstraints[i];
		TdConstraintContact& c = mConstraints[i];

		tdBody* body0 = constraint->bodies[0];
		tdBody* body1 = constraint->bodies[1];
		if (body0 == NULL)
			body0 = &gStaticBody;
		if (body1 == NULL)
			body1 = &gStaticBody;
	
		c.mBody0 = body0->internal;
		c.mBody1 = body1->internal;
	
		TdSolverBody* sb0 = s->getSolverBody(c.mBody0);
		TdSolverBody* sb1 = s->getSolverBody(c.mBody1);	
	
		//Normal and tangent dir
		c.mNormal0 = (TdVec3&)constraint->manifold.normal0;
		c.mPerp0[0] = perpendicular(c.mNormal0);
		c.mPerp0[1] = cross(c.mNormal0, c.mPerp0[0]);
	
		//Transform the contact frame
		tdTransform f1tof0 = s->getBodyTransformDiff(body0, body1);

		c.mNormal1 = TdTransformInvVecM(&f1tof0, c.mNormal0);
		c.mPerp1[0] = TdTransformInvVecM(&f1tof0, c.mPerp0[0]);
		c.mPerp1[1] = TdTransformInvVecM(&f1tof0, c.mPerp0[1]);

		c.mNormal0 = -c.mNormal0;
		c.mPerp0[0] = -c.mPerp0[0];
		c.mPerp0[1] = -c.mPerp0[1];

		//Transform and init points 
		c.mCount = constraint->manifold.count;
		for(int i=0; i<c.mCount; i++)
		{
			TdVec3& p0 = (TdVec3&)constraint->manifold.points0[i];
			TdVec3& p1 = (TdVec3&)constraint->manifold.points1[i];

			float relVel = -dot(c.mNormal0, sb0->getPointVel(p0)) - dot(c.mNormal1, sb1->getPointVel(p1));

			//p0 and p1 are now in RB local space for each body
			float distance = -dot(c.mNormal0, TdTransformPointM(&f1tof0, p1)-p0);
			distance -= state->getContactOffset(); // TODO: centralize this in solver
			distance = TdMax(distance, -state->getMaxPenetration());
			if (state->getRestitution() > 0.0f && relVel > state->getMaxPenetration()/s->mDt)
				distance = TdMin(distance, 0.0f); // zero positive distance to allow bounce
			c.mPoints[i].mPos0 = p0;
			c.mPoints[i].mPos1 = p1;
			c.mPoints[i].mTarget = -relVel*state->getRestitution() + distance * s->getErp();
			c.mPoints[i].mImpulse = 0.0f;
			c.mPoints[i].mAff0 = sb0->getAffection(c.mPoints[i].mPos0, c.mNormal0);
			c.mPoints[i].mAff1 = sb1->getAffection(c.mPoints[i].mPos1, c.mNormal1);
			c.mPoints[i].mAngImp0 = multiplyPerElement(sb0->mInvInertia, cross(p0, c.mNormal0));
			c.mPoints[i].mAngImp1 = multiplyPerElement(sb1->mInvInertia, cross(p1, c.mNormal1));
			c.mPos0 += p0/(float)c.mCount;
			c.mPos1 += p1/(float)c.mCount;
		}
	
		//Friction init
		c.mFricTarget[0] = 0.0f;
		c.mFricTarget[1] = 0.0f;
		c.mFricTarget[2] = 0.0f;
		c.mFricImpulse[0] = 0.0f;
		c.mFricImpulse[1] = 0.0f;
		c.mFricImpulse[2] = 0.0f;

		c.mFricAff0[0] = sb0->getAffection(c.mPos0, c.mPerp0[0]);
		c.mFricAff1[0] = sb1->getAffection(c.mPos1, c.mPerp1[0]);
		c.mFricAff0[1] = sb0->getAffection(c.mPos0, c.mPerp0[1]);
		c.mFricAff1[1] = sb1->getAffection(c.mPos1, c.mPerp1[1]);
		c.mFricAff0[2] = sb0->getAngAffection(c.mNormal0);
		c.mFricAff1[2] = sb1->getAngAffection(c.mNormal1);

		c.mPerpAngImp0[0] = multiplyPerElement(sb0->mInvInertia, cross(c.mPos0, c.mPerp0[0]));
		c.mPerpAngImp1[0] = multiplyPerElement(sb1->mInvInertia, cross(c.mPos1, c.mPerp1[0]));
		c.mPerpAngImp0[1] = multiplyPerElement(sb0->mInvInertia, cross(c.mPos0, c.mPerp0[1]));
		c.mPerpAngImp1[1] = multiplyPerElement(sb1->mInvInertia, cross(c.mPos1, c.mPerp1[1]));

		//Setup persistent data
		c.mPersistent = constraint->persistent;
		if (c.mPersistent)
		{
			if(c.mPersistent->status==1)
			{
				(TdVec3&)*(c.mPersistent->p0) = c.mPos0;
				(TdVec3&)*(c.mPersistent->p1) = TdTransformInvPointM(&f1tof0, c.mPos0);
				(TdVec3&)*(c.mPersistent->d0) = c.mPerp0[0];
				(TdVec3&)*(c.mPersistent->d1) = c.mPerp1[0];
				(TdVec3&)*(c.mPersistent->n0) = c.mNormal0;
				(TdVec3&)*(c.mPersistent->n1) = c.mNormal1;
				c.mPersistent->status = 2;
			}
			else if(c.mPersistent->status==2)
			{
				//Only apply friction drift if objects are aligned
				TdVec3 n0 = (TdVec3&)*(c.mPersistent->n0);
				TdVec3 n1 = (TdVec3&)*(c.mPersistent->n1);
				n1 = TdTransformVecM(&f1tof0, n1);
				if (dot(n0, n1) < -0.99f)
				{
					TdVec3 p0 = (TdVec3&)*(c.mPersistent->p0);
					TdVec3 p1 = (TdVec3&)*(c.mPersistent->p1);
					p1 = TdTransformPointM(&f1tof0, p1);
			
					c.mFricTarget[0] = -dot(c.mPerp0[0], p1-p0) * s->getErp();
					c.mFricTarget[1] = -dot(c.mPerp0[1], p1-p0) * s->getErp();
			
					TdVec3 d0 = (TdVec3&)*(c.mPersistent->d0);
					TdVec3 d1 = (TdVec3&)*(c.mPersistent->d1);
					d1 = TdTransformVecM(&f1tof0, d1);
					c.mFricTarget[2] = dot(c.mNormal0, cross(d0, d1)) * s->getErp(); //TODO: Compensate for object size
				}
				else
				{
					c.mPersistent->status = 0;
				}
			}
		}
		c.mTmp = 2;
	}
}


void TdConstraintTypeContact::solve(TdSolver* solver, const class TdSolverState* state, int start, int end)
{
//	float limit = 0.0f;
	for(int a=start; a<end; a++)
	{
		TdConstraintContact& c = mConstraints[a];

		if (c.mCount == 0)
			continue;
	
		TdSolverBody* sb0 = solver->getSolverBody(c.mBody0);
		TdSolverBody* sb1 = solver->getSolverBody(c.mBody1);

		TdVec3& linVel0 = sb0->mLinVel;
		TdVec3& angVel0 = sb0->mAngVel;
		TdVec3& linVel1 = sb1->mLinVel;
		TdVec3& angVel1 = sb1->mAngVel;

		//Solve points
		float accImp = 0.0f;

		//solver->mMut[c.mBody0].lock();
		//solver->mMut[c.mBody1].lock();

		for(int i=0; i<c.mCount; i++)
		{
			TdConstraintContact::Point& point = c.mPoints[i];
			TdVec3 pv0 = sb0->getPointVel(point.mPos0); 
			TdVec3 pv1 = sb1->getPointVel(point.mPos1); 
			float nRelVel = -dot(c.mNormal0, pv0) - dot(c.mNormal1, pv1);
//			if (nRelVel-point.mTarget > limit)
			{
//				c.mTmp = 2;
				float f0 = sb0->getPropagationFactor(pv0, c.mNormal0);
				float f1 = sb1->getPropagationFactor(pv1, c.mNormal1);
				float aff = point.mAff0*f0 + point.mAff1*f1;
				float imp = (nRelVel-point.mTarget) / aff;

				state->clampAndAccumulateContactImpulse(imp, point.mImpulse);

				//TODO: Investigate this. Friction needs to be handled differently when it's not resting
				//This is just a hack to bypass impulses caused by impacts
				if (solver->getCurrentIteration() == 0 && point.mTarget<0.0f)
					point.mImpulse = 0.0f;

/*
				if (imp < 0.0f)
					imp = 0.0f;
*/									
				//Apply impulse on body0
				linVel0 += c.mNormal0*(imp*f0*sb0->mInvMass);
				angVel0 += point.mAngImp0*(imp*f0);
		
				//Apply impulse on body1
				linVel1 += c.mNormal1*(imp*f1*sb1->mInvMass);
				angVel1 += point.mAngImp1*(imp*f1);
			}			
			accImp += point.mImpulse;
		}

		c.mSaturated = false;

		//Solve friction
		TdVec3 pv0 = sb0->getPointVel(c.mPos0); 
		TdVec3 pv1 = sb1->getPointVel(c.mPos1); 
//		if ((pv0-pv1).getMagnitudeSq() > limit*limit)
		{
//			c.mTmp = 2;
			for(int i=0; i<2; i++)
			{
				float nRelVel = -dot(c.mPerp0[i], pv0) - dot(c.mPerp1[i], pv1);
				float aff = c.mFricAff0[i] + c.mFricAff1[i];
				float imp = (nRelVel-c.mFricTarget[i]) / aff;

				c.mSaturated |= state->clampAndAccumulateFrictionImpulse(imp, c.mFricImpulse[i], accImp);

				//Apply impulse on body0
				linVel0 += c.mPerp0[i]*(imp*sb0->mInvMass);
				angVel0 += c.mPerpAngImp0[i]*imp;

				//Apply impulse on body1
				linVel1 += c.mPerp1[i]*(imp*sb1->mInvMass);
				angVel1 += c.mPerpAngImp1[i]*imp;
			}
		}

		float nRelVel = -dot(c.mNormal0, sb0->mAngVel) - dot(c.mNormal1, sb1->mAngVel);
//		if (TdAbs(nRelVel-c.mFricTarget[2]) > limit)
		{
			float aff0 = c.mFricAff0[2];
			float aff1 = c.mFricAff1[2];
			if (aff0+aff1 > 0.0f)
			{
				float imp = (nRelVel-c.mFricTarget[2]) / (aff0+aff1);

				state->clampAndAccumulateFrictionImpulse(imp, c.mFricImpulse[2], accImp);

				sb0->applyAngImpulse(c.mNormal0*imp);
				sb1->applyAngImpulse(c.mNormal1*imp);
				}
		}
		//solver->mMut[c.mBody0].unlock();
		//solver->mMut[c.mBody1].unlock();
	}
}


void TdConstraintTypeContact::postSolve(TdSolver* solver)
{
	for(int i=0; i<(int)mConstraints.size(); i++)
	{
		TdConstraintContact& c = mConstraints[i];
		if (c.mPersistent)
		{
			if (c.mSaturated)
			{
				if (c.mPersistent->status != 0)
					c.mPersistent->status = 0;
			}
			else
			{
				if (c.mPersistent->status == 0)
					c.mPersistent->status = 1;
			}
		
			//Store impulses
			/*
			c.mPersistent->count = c.mCount;
			for(int i=0; i<c.mCount; i++)
				c.mPersistent->imp[i] = c.mPoints[i].mImpulse;
			c.mPersistent->imp[4] = c.mFricImpulse[0];
			c.mPersistent->imp[5] = c.mFricImpulse[1];
			c.mPersistent->imp[6] = c.mFricImpulse[2];
			*/
		}

		for(int p=0; p<c.mCount; p++)
		{
			solver->mOriginalBodies[c.mBody0]->force += c.mPoints[p].mImpulse;
			solver->mOriginalBodies[c.mBody1]->force += c.mPoints[p].mImpulse;
		}
	}
}

