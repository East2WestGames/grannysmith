/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#pragma once

#include "td.h"

void tdSerWriteDistance(int context, const tdDistanceQuery* query);
void tdSerReadDistance(int context, tdDistanceQuery* query);

