/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#pragma once

#include <vector>

class TdConstraintType
{
public:
	enum Type
	{
		CONTACT,
		COUNT
	};
	
	virtual void* add(int& pos) = 0;
	virtual void clear() = 0;
	virtual void init(class TdSolver* solver, const class TdSolverState* state, int start, int end) = 0;
	virtual void preSolve(class TdSolver* solver) {}
	virtual void solve(class TdSolver* solver, const class TdSolverState* state, int start, int end) = 0;
	virtual void postSolve(class TdSolver* solver) {}
};

template <class ApiType, class ConstraintType>
class TdConstraintTypeBase : public TdConstraintType
{
public:
	std::vector<ApiType> mApiConstraints;
	std::vector<ConstraintType> mConstraints;

	virtual void* add(int& pos)
	{
		pos = (int)mConstraints.size();
		mConstraints.push_back(ConstraintType());
		mApiConstraints.push_back(ApiType());
		return &mApiConstraints.back();
	}

	virtual void clear()
	{
		mConstraints.clear();
		mApiConstraints.clear();
	}
};

