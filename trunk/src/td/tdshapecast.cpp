/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#include "td.h"
#include "tdconfig.h"
#include "tdshape.h"
#include "tdgjk.h"
#include "tdepa.h"
#include "tdcontext.h"


//Input query flags:
// TD_QUERY_COMPUTE_NORMAL
// TD_QUERY_COMPUTE_POINTS
// TD_QUERY_HANDLE_PENETRATION
// TD_QUERY_USE_MAX_DISTANCE

//Output status flags:
//TD_RESULT_HAS_DISTANCE
//TD_RESULT_HAS_NORMAL
//TD_RESULT_HAS_POINTS
//TD_RESULT_SEPARATION
//TD_RESULT_PENETRATION
//TD_RESULT_MAX_DISTANCE_REACHED
//TD_RESULT_MISS
//TD_RESULT_UNRELIABLE
//TD_RESULT_FAILED

void tdShapecast(int context, const tdShapecastQuery* query, tdShapecastResult* result)
{
	TD_DEBUG_CLEAR();
	TD_DEBUG_TEXT("Shapecast query");
	TD_DEBUG_CSO();

	TdContextInternal* ctxt = (TdContextInternal*)context;
	TdCso cso;
	cso.init(query->shape0, query->shape1, &query->transform);

	TdGjk gjk(ctxt->tolerance, ctxt->maxIterations);
	gjk.init(&cso);

	result->status = 0;
	
	TdVec3& dir = (TdVec3&)query->direction;
	float d = 2.0f*(cso.getMidPoint().length());
	cso.mLinAdd = dir*d;
	cso.mUseLinAdd = true;

	gjk.init(&cso);
	gjk.computeDistance();
	if (!gjk.mPenetration)
	{
		result->status |= TD_RESULT_SEPARATION;
		result->status |= TD_RESULT_MISS;
		return;
	}

	TdMpr mpr(ctxt->tolerance, ctxt->maxIterations);
	mpr.init(&gjk, dir);
	mpr.trackSurface();
	
	//Fill in normal
	result->normal = mpr.mDir;
	result->status |= TD_RESULT_HAS_NORMAL;

	float t0, t1;
	float res = mpr.getDistanceToPortal(t0, t1);

	result->distance = d-res;
	result->status |= TD_RESULT_HAS_DISTANCE;
	result->status |= result->distance < 0.0f ? TD_RESULT_PENETRATION : TD_RESULT_SEPARATION;

	//Early out if penetration should not be considered
	if (!(query->flags & TD_QUERY_HANDLE_PENETRATION) && result->distance < 0.0f)
		return;

	//Early out if max distance is reached
	if ((query->flags & TD_QUERY_USE_MAX_DISTANCE) && result->distance > query->maxDistance)
	{
		result->status |= TD_RESULT_MAX_DISTANCE_REACHED;
		return;
	}

	//Fill in points
	if (query->flags & TD_QUERY_COMPUTE_POINTS)
	{
		mpr.getPoints(t0, t1, NULL, (TdVec3*)&result->point0, (TdVec3*)&result->point1);
		result->status |= TD_RESULT_HAS_POINTS;
	}
}

