/* TD physics and geometry framework. Copyright 2009-2012, Dengu AB */

#include "tddebug.h"

#if TD_DEBUG_DRAW
#include "qi_gfx.h"

std::vector<TdDebugItem*> gDebugStack;

void TdDebugSimplex::draw()
{
	glColor4f(1,1,0,1);
	TdVec3 s0 = mPoints[0].cs;
	TdVec3 s1 = mPoints[1].cs;
	TdVec3 s2 = mPoints[2].cs;
	TdVec3 s3 = mPoints[3].cs;
	if (mCount==1)
	{
		glBegin(GL_POINTS);
		glVertex3f(s0.x, s0.y, s0.z);
		glEnd();
	}
	if (mCount==2)
	{
		glBegin(GL_LINES);
		glVertex3f(s0.x, s0.y, s0.z);
		glVertex3f(s1.x, s1.y, s1.z);
		glEnd();
		glBegin(GL_POINTS);
		glColor4f(1,0,0,1);
		glVertex3f(s0.x, s0.y, s0.z);
		glColor4f(0,1,0,1);
		glVertex3f(s1.x, s1.y, s1.z);
		glEnd();
	}
	if (mCount==3)
	{
		glBegin(GL_LINE_STRIP);
		glVertex3f(s0.x, s0.y, s0.z);
		glVertex3f(s1.x, s1.y, s1.z);
		glVertex3f(s2.x, s2.y, s2.z);
		glVertex3f(s0.x, s0.y, s0.z);
		glEnd();
		glBegin(GL_POINTS);
		glColor4f(1,0,0,1);
		glVertex3f(s0.x, s0.y, s0.z);
		glColor4f(0,1,0,1);
		glVertex3f(s1.x, s1.y, s1.z);
		glColor4f(0,0,1,1);
		glVertex3f(s2.x, s2.y, s2.z);
		glEnd();
	}
	if (mCount==4)
	{
		glBegin(GL_LINE_STRIP);
		glVertex3f(s0.x, s0.y, s0.z);
		glVertex3f(s1.x, s1.y, s1.z);
		glVertex3f(s2.x, s2.y, s2.z);
		glVertex3f(s0.x, s0.y, s0.z);
		glEnd();
		glBegin(GL_LINES);
		glVertex3f(s0.x, s0.y, s0.z);
		glVertex3f(s3.x, s3.y, s3.z);
		glVertex3f(s1.x, s1.y, s1.z);
		glVertex3f(s3.x, s3.y, s3.z);
		glVertex3f(s2.x, s2.y, s2.z);
		glVertex3f(s3.x, s3.y, s3.z);
		glEnd();
		glBegin(GL_POINTS);
		glColor4f(1,0,0,1);
		glVertex3f(s0.x, s0.y, s0.z);
		glColor4f(0,1,0,1);
		glVertex3f(s1.x, s1.y, s1.z);
		glColor4f(0,0,1,1);
		glVertex3f(s2.x, s2.y, s2.z);
		glEnd();
	}	
}


void TdDebugPoint::draw()
{
	glColor4f(1,0,0,1);
	glBegin(GL_POINTS);
	glVertex3f(mPoint.x, mPoint.y, mPoint.z);
	glEnd();
}


void TdDebugLine::draw()
{
	glColor4f(0,1,1,1);
	glBegin(GL_LINES);
	glVertex3f(mPoint0.x, mPoint0.y, mPoint0.z);
	glVertex3f(mPoint1.x, mPoint1.y, mPoint1.z);
	glEnd();
}
#endif

