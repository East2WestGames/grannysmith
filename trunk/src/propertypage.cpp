/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#if !defined(QI_IOS) && !defined(QI_ANDROID)

#include "propertypage.h"
#include "qi_app.h"

#if _MSC_VER
#pragma warning (disable: 4244)
#endif

bool PropertyPage::init(class QiViewport* viewport)
{
	mViewport = viewport;
	mFinished = false;
	mShow = false;

	mTextRenderer.setAlignment(QiTextRenderer::LEFT);
	mTextRenderer.setFont(QiTextRenderer::TAHOMA_11);
	mTextRenderer.setColor(0,0,0,1);
	mTextRenderer.flip(true);

	return true;
}


void PropertyPage::shutdown()
{
	clear();
}



void PropertyPage::clear()
{
	for(int i=0; i<mFields.getCount(); i++)
		QI_DELETE(mFields[i]);
	mFields.clear();
	mSelected = -1;
}

/*
void PropertyPage::addField(const QiString& name, const QiString& value)
{
	Field* f = QI_NEW Field();
	f->mName = name;
	f->mValue = value;
	mFields.add(f);
}
*/


void PropertyPage::setPropertyBag(PropertyBag* properties)
{
	clear();
	mProperties = properties;
	for(int i=0; i<mProperties->getPropertyCount(); i++)
	{
		Property* p = mProperties->getProperty(i);
		if ((p->mFlags & PROPERTY_INVISIBLE) == 0)
		{
			Field* f = QI_NEW Field();
			f->mName = p->mName;
			f->mValue = p->mValue;
			f->mDefault = (p->mInherited != "" ?  p->mInherited : p->mDefault);
			mFields.add(f);
		}
	}
}


void PropertyPage::show()
{
	mShow = true;
	mFinished = false;
}


void PropertyPage::beginFrame(class QiInput& input)
{
	mFinished = false;
	mInput.clear();
	mInput = input;

	if (!mShow)
		return;

	if (input.wasKeyPressed(QI_KEY_ESCAPE))
	{
		mShow = false;
		mFinished = true;
	}

	input.clear();
}


static void drawBox(const QiVec2& lower, const QiVec2& upper, bool bold=false)
{
	glColor4f(0.9f,0.9f,0.9f,1.0f);
	glBegin(GL_QUADS);
	glVertex2f(lower.x, lower.y);
	glVertex2f(upper.x, lower.y);
	glVertex2f(upper.x, upper.y);
	glVertex2f(lower.x, upper.y);
	glEnd();
	glColor4f(0.7f,0.7f,0.7f,1.0f);
	if (bold)
	{
		glColor4f(0.2f,0.2f,0.2f,1.0f);
		glLineWidth(2.0f);
	}
	glBegin(GL_LINE_LOOP);
	glVertex2f(lower.x, lower.y);
	glVertex2f(upper.x, lower.y);
	glVertex2f(upper.x, upper.y);
	glVertex2f(lower.x, upper.y);
	glEnd();
	glLineWidth(1.0f);
}


void PropertyPage::endFrame()
{
	if (!mShow)
		return;

	mViewport->push();
	mViewport->setModePixel();
	mViewport->apply();

	int x = 100;
	int y = 100;

	int w = 500;
	int h = 20 + 20 + 20 + 20 * mFields.getCount();

	glColor4f(1,1,1,0.8f);
	glBegin(GL_QUADS);
	glVertex2i(x, y);
	glVertex2i(x+w, y);
	glVertex2i(x+w, y+h);
	glVertex2i(x, y+h);
	glEnd();

	glColor4f(0,0,0,1);
	glBegin(GL_LINE_LOOP);
	glVertex2i(x, y);
	glVertex2i(x+w, y);
	glVertex2i(x+w, y+h);
	glVertex2i(x, y+h);
	glEnd();

	y+=20;
	x+=20;

	int picked = -1;
	for(int i=0; i<mFields.getCount(); i++)
	{
		bool sel = (i==mSelected);
		float off = 100;
		float w = 350;
		float h = 11;
		drawBox(QiVec2(x+off, y-3), QiVec2(x+off+w, y+h+3), sel);

		if (mViewport->pickRect(i, QiVec2(x+off, y-5), QiVec2(x+off+w, y+h+5)))
			picked = i;

		mTextRenderer.setColor(0, 0, 0, 1);
		mTextRenderer.setPosition(x, y);
		mTextRenderer.print(mFields[i]->mName);
		mTextRenderer.setPosition(x+off+10, y);

		if (mSelected == i)
		{
			mTextRenderer.print(mInputBox.getText());
			float w = mTextRenderer.getWidth(mInputBox.getText().substring(0, mInputBox.getCursosPos()));
			static int f = 0;
			f++;
			if ((f/30)%2==0)
			{
				glColor4f(0,0,0,1);
				glBegin(GL_LINES);
				glVertex2f(x+off+11+w, y-1);
				glVertex2f(x+off+11+w, y+h+1);
				glEnd();
			}
		}
		else
		{
			if (mFields[i]->mValue != "")
			{
				mTextRenderer.setColor(0, 0, 0, 1);
				mTextRenderer.print(mFields[i]->mValue);
			}
			else
			{
				mTextRenderer.setColor(0, 0, 0, 0.5f);
				mTextRenderer.print(mFields[i]->mDefault);
			}
		}
		y+=20;
	}

	y += 30;

	drawBox(QiVec2(x+w-120, y-30), QiVec2(x+w-50, y-10));
	mTextRenderer.setPosition(x+w-100, y-25);
	mTextRenderer.print("Ok");

	if (mInput.wasButtonPressed(QI_BUTTON_LEFT))
	{
		mSelected = picked;
		if (mSelected != -1)
		{
			int cx = mInput.getMousePosX() - (x+110);
			QiString tmp = "";
			QiString val = mFields[mSelected]->mValue;
			int c = 0;
			while(mTextRenderer.getWidth(tmp) < cx && c < val.getLength())
			{
				c++;
				tmp = val.substring(0, c);
			}
			mInputBox.setText(val);
			mInputBox.setCursosPos(c);
		}
	}

	if (mInput.wasKeyPressed(QI_KEY_RETURN))
	{
		mSelected = -1;
	}

	if (mSelected != -1)
		mFields[mSelected]->mValue = mInputBox.getText();

	if (mSelected != -1)
		mInputBox.processInput(mInput);


	if (mViewport->pickRect((int)this, QiVec2(x+w-120, y-30), QiVec2(x+w-50, y-10)) && mInput.wasButtonPressed(QI_BUTTON_LEFT))
	{
		//Write back to property bag
		for(int i=0; i<mFields.getCount(); i++)
		{
			Field* f = mFields[i];
			mProperties->setString(f->mName, f->mValue);
		}

		mShow = false;
		mFinished = true;
	}


	mViewport->pop();
	mViewport->apply();
}


QiString PropertyPage::getValue(const QiString& name) const
{
	for(int i=0; i<mFields.getCount(); i++)
	{
		if (mFields[i]->mName == name)
			return mFields[i]->mValue;
	}
	return QiString();
}

#endif
