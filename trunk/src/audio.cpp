/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "config.h"
#include "audio.h"
#include "game.h"
#include "display.h"
#include "resman.h"
#include "qi_snd.h"
#include "device.h"
#include "profiling.h"
#include "level.h"

Audio::Audio() :
mBackgroundMusicChannel(NULL), mForegroundMusicChannel(NULL), mSoundEnabled(true), mMusicEnabled(true), mForceBackground(false)
{
	mEngine.init(32);

	mProperties.add("musicEnabled", "1");
	mProperties.add("soundEnabled", "1");

	start();
}


Audio::~Audio()
{
	signalQuit();
	while(isRunning())
		QiThread::sleep(0.01f);
	mEngine.shutdown();
}


void Audio::load()
{
	mClickDown.load(gGame->mResMan, "snd/clickdown");
	mClickDown.mTimeInBetween = 0.0f;
	mClickUp.load(gGame->mResMan, "snd/clickup");
	mClickUp.mTimeInBetween = 0.0f;
	mBreakBrick.load(gGame->mResMan, "snd/breakable/brick");
	mBreakGlass.load(gGame->mResMan, "snd/breakable/glass");
	mBreakWood.load(gGame->mResMan, "snd/breakable/wood");
	mBreakMetal.load(gGame->mResMan, "snd/breakable/metal");
	mBreakBranch.load(gGame->mResMan, "snd/breakable/branch");

	mCaneHit.load(gGame->mResMan, "snd/character/cane_hit");
	mCaneSwoosh.load(gGame->mResMan, "snd/character/cane_swoosh");
	mCaneSwoosh.mTimeInBetween = 0.0f;
	mCaneSpeed.load(gGame->mResMan, "snd/character/cane_speed");
	mCaneSpeed.mTimeInBetween = 0.0f;
	mSkatesLand.load(gGame->mResMan, "snd/character/skates_land");
	mSkatesLeave.load(gGame->mResMan, "snd/character/skates_leave");
	mRotate.load(gGame->mResMan, "snd/character/rotate");
	mCoin.load(gGame->mResMan, "snd/coin");
	mCoin.mTimeInBetween = 0.05f;

	mObjectHit.load(gGame->mResMan, "snd/object/hit");
}


void Audio::unload()
{
	mClickDown.release();
	mClickUp.release();
	mBreakBrick.release();
	mBreakGlass.release();
	mBreakWood.release();
	mBreakMetal.release();
	mBreakBranch.release();

	mCaneHit.release();
	mCaneSwoosh.release();
	mCaneSpeed.release();
	mSkatesLand.release();
	mSkatesLeave.release();
	mRotate.release();
	mCoin.release();

	mObjectHit.release();
}


void Audio::run()
{
	while(!shouldQuit())
	{
		{
			PROFILE_ZONE("Stream");
			mStreamMutex.lock();
			mEngine.updateStreaming();
			mStreamMutex.unlock();
		}
		QiThread::sleep(0.02f);
	}
}


void Audio::update()
{
	mMusicEnabled = mProperties.getBool("musicEnabled");
	mSoundEnabled = mProperties.getBool("soundEnabled");

	if (mForegroundMusicChannel)
	{
		if (mForegroundMusicChannel->isPlaying())
		{
			//Mute the background music
			if (mBackgroundMusicChannel && mBackgroundMusicChannel->isPlaying())
				mBackgroundMusicChannel->setVolume(0.0f);
		}
		else
		{
			//Foreground music came to an end, switch to background
			stopForegroundMusic();
		}
	}

	//Play background music if there is any and there is no foreground music
	if ((isMusicEnabled() || mForceBackground) && mBackgroundMusicChannel && !mForegroundMusicChannel)
	{
		float v = mBackgroundMusicChannel->getVolume();
		if (v < 1.0f)
			mBackgroundMusicChannel->setVolume(v + 0.04f);
	}

	//Fade down and stop music if we shouldn't play music
	if (!isMusicEnabled() && !mForceBackground)
	{
		if (mBackgroundMusicChannel)
		{
			float v = mBackgroundMusicChannel->getVolume();
			if (v > 0.0f)
				mBackgroundMusicChannel->setVolume(v - 0.04f);
			else
				stopBackgroundMusic();
		}
		if (mForegroundMusicChannel)
		{
			float v = mForegroundMusicChannel->getVolume();
			if (v > 0.0f)
				mForegroundMusicChannel->setVolume(v - 0.04f);
			else
				stopForegroundMusic();
		}
	}
}


void Audio::playBackgroundMusic(const QiString& path, bool force)
{
	mForceBackground = force;
	
	if (!force && !isMusicEnabled())
		return;

	if (mBackgroundMusicChannel && mBackgroundMusicPath == path)
		return;

	stopBackgroundMusic();

	QiMutexZone lock(mStreamMutex);

	mBackgroundMusicChannel = mEngine.acquireChannel();
	if (!mBackgroundMusicChannel)
		return;

	mBackgroundMusicPath = path;
	
	QiString assetPath = gGame->mDevice->getAssetPath() + "/" + mBackgroundMusicPath;
#ifdef QI_ANDROID
	assetPath = mBackgroundMusicPath + ".mp3";
#endif
	QiAudioFileStreamDecoder<QiVorbisDecoder>* dec = QI_NEW QiAudioFileStreamDecoder<QiVorbisDecoder>();
	if (dec->init(assetPath, true))
	{
		mBackgroundMusicStream = dec;
		mBackgroundMusicBuffer = mEngine.createStreamingBuffer(mBackgroundMusicStream, dec->getFrequency(), dec->getChannelCount(), dec->getBitsPerSample());
		mBackgroundMusicChannel->setBuffer(mBackgroundMusicBuffer);
		mBackgroundMusicChannel->play();
	}
	else
	{
		QI_DELETE(dec);
		mEngine.releaseChannel(mBackgroundMusicChannel);
		mBackgroundMusicChannel = NULL;
	}
}


void Audio::stopBackgroundMusic()
{
	if (mBackgroundMusicChannel)
	{
		QiMutexZone lock(mStreamMutex);
		mEngine.releaseChannel(mBackgroundMusicChannel);
		mBackgroundMusicChannel = NULL;
		mEngine.destroyBuffer(mBackgroundMusicBuffer);
		mBackgroundMusicBuffer = NULL;
		QI_DELETE(mBackgroundMusicStream);
		mBackgroundMusicStream = NULL;
	}
}


void Audio::playForegroundMusic(const QiString& path)
{
	if (!isMusicEnabled())
		return;

	if (mForegroundMusicChannel && mForegroundMusicPath == path)
		return;

	stopForegroundMusic();

	QiMutexZone lock(mStreamMutex);

	mForegroundMusicChannel = mEngine.acquireChannel();
	if (!mForegroundMusicChannel)
		return;
	
	mForegroundMusicPath = path;

	QiString assetPath = gGame->mDevice->getAssetPath() + "/" + mForegroundMusicPath;
#ifdef QI_ANDROID
	assetPath = mForegroundMusicPath + ".mp3";
#endif
	QiAudioFileStreamDecoder<QiVorbisDecoder>* dec = QI_NEW QiAudioFileStreamDecoder<QiVorbisDecoder>();
	if (dec->init(assetPath, false))
	{
		mForegroundMusicStream = dec;
		mForegroundMusicBuffer = mEngine.createStreamingBuffer(mForegroundMusicStream, dec->getFrequency(), dec->getChannelCount(), dec->getBitsPerSample());
		mForegroundMusicChannel->setBuffer(mForegroundMusicBuffer);
		mForegroundMusicChannel->play();
	}
	else
	{
		QI_DELETE(dec);
		mEngine.releaseChannel(mForegroundMusicChannel);
		mForegroundMusicChannel = NULL;
	}
}


void Audio::stopForegroundMusic()
{
	if (mForegroundMusicChannel)
	{
		QiMutexZone lock(mStreamMutex);
		mEngine.releaseChannel(mForegroundMusicChannel);
		mForegroundMusicChannel = NULL;
		mEngine.destroyBuffer(mForegroundMusicBuffer);
		mForegroundMusicBuffer = NULL;
		QI_DELETE(mForegroundMusicStream);
		mForegroundMusicStream = NULL;
	}
}


bool Audio::isMusicEnabled()
{
	return mMusicEnabled && !gGame->mDevice->isMusicPlaying();
}


bool Audio::isSoundEnabled()
{
	return mSoundEnabled;
}


QiAudioChannel* Audio::acquireChannel()
{
	return mEngine.acquireChannel();
}


void Audio::releaseChannel(QiAudioChannel* channel)
{
	mEngine.releaseChannel(channel);
}


void Audio::playSound(const QiAudioBuffer* buffer, float volume, float pitch)
{
	if (isSoundEnabled())
	{
		if (gGame->mLevel->mReplay)
			volume *= 0.5f;
		mEngine.play(buffer, volume, pitch);
	}
}


void Audio::playSound(const QiAudioBuffer* buffer, const QiVec2& pos, float volume, float pitch)
{
	if (buffer == NULL)
		return;
	float d = length(pos - QiVec2(gGame->mDisplay->mCameraPos.x, gGame->mDisplay->mCameraPos.y));
	float v = 1.0f / (1.0f + d*d*0.01f);
	playSound(buffer, volume*v, pitch);
}


void Audio::setSoundChannelVolume(QiAudioChannel* channel, float volume)
{
	if (gGame->mLevel->mReplay)
		volume *= 0.5f;
	if (channel)
		channel->setVolume(isSoundEnabled() ? volume : 0.0f);
}


void Audio::setSoundChannelVolume(QiAudioChannel* channel, const QiVec2& pos, float volume)
{
	float d = length(pos - QiVec2(gGame->mDisplay->mCameraPos.x, gGame->mDisplay->mCameraPos.y));
	float v = 1.0f / (1.0f + d*d*0.01f);
	setSoundChannelVolume(channel, volume*v);
}


void Audio::attach()
{
}


void Audio::detach()
{
}


void SoundBank::load(ResMan* resMan, const QiString& path)
{
	mLast = 0;
	release();
	for(int i=0; i<10; i++)
	{
		QiString p = path + i + QiString(".ogg");
		Resource r = resMan->acquireSound(p);
		if (r.getSound())
			mSounds.add(r);
		else
			break;
	}
	if (mSounds.getCount() == 0)
		gGame->logE("Couldn't find sound bank " + path);
}

void SoundBank::release()
{
	mSounds.clear();
}

QiAudioBuffer* SoundBank::next()
{
	if (mSounds.getCount() == 0 || mTimer.getTime() < mTimeInBetween)
		return NULL;
	else
	{
		int s;
		do
		{
			s = QiRnd(0, mSounds.getCount());
		}
		while(s == mLast && mSounds.getCount() > 1);
		mLast = s;
		mTimer.reset();
		return mSounds[s].getSound();
	}
}

