/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "gfx.h"
#include "game.h"
#include "particlesystem.h"
#include "level.h"

ParticleSystem::ParticleSystem() 
{
	mTextureRows = 1;
	mTextureCols = 1;
	mMaxParticleCount = 0;

	mMinLife = 1.0f;
	mMaxLife = 2.0f;
	mMinAngVel = -10.0f;
	mMaxAngVel = 10.0f;
	mStartRadius = 0.0f;
	mEndRadius = 1.0f;
	mStartAlpha = 1.0f;
	mEndAlpha= 0.0f;
	mGravity = -10.0f;
}

ParticleSystem::~ParticleSystem()
{
}


void ParticleSystem::init(const QiVec2& texMin, const QiVec2& texMax, int rows, int cols, int maxParticles)
{
	mMaxParticleCount = maxParticles;
	mParticles.redim(maxParticles);
	mParticles.clear();

	mTextureRows = rows;
	mTextureCols = cols;

	QiTexture* t = gGame->mLevel->mParticleTexture.getTexture();
	if (t)
	{
		float w = (float)t->getWidth();
		float h = (float)t->getHeight();
		mTexMin.set(texMin.x/w, texMin.y/h);
		mTexMax.set(texMax.x/w, texMax.y/h);
	}
}


bool ParticleSystem::load(const QiString& name)
{
	QiMemoryStream<256> tmp;
	gGame->mResMan->load("effects/default.xml", tmp);
	QiXmlParser xml;
	if (!xml.parse(tmp, tmp.getSize()) || xml.getName() != "effects")
	{
		gGame->logE("Could not parse effect file");
		return false;
	}
	xml.enter();
	while(xml.isValid())
	{
		if (xml.getName()=="effect")
		{
			if (xml.getAttribute("name") == name)
			{
				QiVec2 texMin;
				QiVec2 texMax;

				QiString coords = xml.getAttribute("coords");
				texMin.x = coords.getWord(0).toFloat();
				texMin.y = coords.getWord(1).toFloat();
				texMax.x = coords.getWord(2).toFloat();
				texMax.y = coords.getWord(3).toFloat();
				int rows = xml.getAttribute("rows").toInt();
				int cols = xml.getAttribute("cols").toInt();
				int maxParticles = xml.getAttribute("max").toInt();
				mGravity = xml.getAttribute("gravity").toFloat();
				mMinLife = xml.getAttribute("life").getWord(0).toFloat();
				mMaxLife = xml.getAttribute("life").getWord(1).toFloat();
				mMinAngVel = xml.getAttribute("rot").getWord(0).toFloat();
				mMaxAngVel = xml.getAttribute("rot").getWord(1).toFloat();
				mStartAlpha = xml.getAttribute("alpha").getWord(0).toFloat();
				mEndAlpha = xml.getAttribute("alpha").getWord(1).toFloat();
				mStartRadius = xml.getAttribute("size").getWord(0).toFloat();
				mEndRadius = xml.getAttribute("size").getWord(1).toFloat();
				init(texMin, texMax, rows, cols, maxParticles);
				return true;
			}
		}
		xml.next();
	}
	xml.leave();
	gGame->logE("Couldn't find effect " + name);
	return false;
}



void ParticleSystem::spawn(const QiVec3& pos, const QiVec3& vel, int texture)
{
	if (mMaxParticleCount == 0)
		return;

	if (texture == -1)
		texture = QiRnd(0, mTextureRows*mTextureCols);

	Particle p;
	p.pos = pos;
	p.vel = vel;
	p.angle = QiRnd(0.0f, QI_TWO_PI);
	p.angVel = QiRndNormal(mMinAngVel, mMaxAngVel);
	p.lifeTime = QiRndNormal(mMinLife, mMaxLife);
	p.age = 0.0f;
	p.texture = texture;

	if (mParticles.getCount() < mMaxParticleCount)
		mParticles.add(p);
	else
	{
		int best = 0;
		for(int i=0; i<mParticles.getCount(); i++)
		{
			if (mParticles[i].age > mParticles[best].age)
				best = i;
		}
		mParticles[best] = p;
	}
}

void ParticleSystem::update()
{
	int c=0;
	float g = mGravity * gGame->mTimeStep;
	for(int i=0; i<mParticles.getCount(); i++)
	{
		Particle& p = mParticles[i];
		if (p.age < p.lifeTime)
		{
			p.vel.y += g;
			p.age += gGame->mTimeStep;
			p.pos += p.vel * gGame->mTimeStep;
			p.angle += p.angVel * gGame->mTimeStep;
			//TODO: Collisions
			if (c != i)
				mParticles[c] = p;
			c++;
		}
	}
	mParticles.redim(c);
}

void ParticleSystem::render()
{
	QiVertexBuffer& vb = gGame->mLevel->mParticleVb;
	QiIndexBuffer& ib = gGame->mLevel->mParticleIb;
	float a[4] = {0.0f, QI_HALF_PI, QI_PI, QI_PI+QI_HALF_PI};
	for(int i=0; i<mParticles.getCount(); i++)
	{
		Particle& p = mParticles[i];
		float t = QiClamp(mParticles[i].age / mParticles[i].lifeTime, 0.0f, 1.0f);
		float r = QiInterpolateLinear(mStartRadius, mEndRadius, t);
		float ang = p.angle;
		QiUInt8 alpha = (QiUInt8)QiInterpolateLinear(255.0f*mStartAlpha, 255.0f*mEndAlpha, t);
		QiVec2 texCoords[4];
		QiTexture::getTiledTexCoords(mTexMin, mTexMax, mTextureRows, mTextureCols, p.texture, texCoords);
	
		int c = vb.getCount();
		ib.quad(c+0, c+1, c+2, c+3);
		for(int j=0; j<4; j++)
		{
			QiVec3 pos(p.pos.x + QiCos(ang+a[j])*r, p.pos.y + QiSin(ang+a[j])*r, p.pos.z);
			vb.vertex();
			vb.addFast(pos);
			vb.addFast(texCoords[j]);
			vb.addFast(alpha);
		}
	}
}

