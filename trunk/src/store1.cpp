/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "game.h"
#include "store.h"

//#if !defined(QI_IOS) && !defined(QI_ANDROID)

QiTimer gPurchaseTimer;
void storeInit() {}
void storeShutdown() {}
void storeInitPurchase(const char* product) { gPurchaseTimer.reset(); }
int storeGetPurchaseStatus() { return gPurchaseTimer.getTime() > 2.0f ? STORE_SUCCEEDED : STORE_WAITING;}
int storeEnabled() { return 1; }

void storeInitRestore() { gPurchaseTimer.reset(); }
int storeGetRestoreStatus() { return gPurchaseTimer.getTime() > 2.0f ? STORE_SUCCEEDED : STORE_WAITING; }
int storeIsRestored(const char* product) { return 0; }
void storeRestoreReset() {}

void storeInitAlert(const char* message) {}
void storeInitDialog(const char* message, const char* yes, const char* no) {}
int storeGetDialogStatus() { return 0; }
void storeResetDialog() {}
void ShowAd()
{};
//#endif

