/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "propertybag.h"


void PropertyBag::clear()
{
	mProperties.clear();
}


int PropertyBag::getPropertyCount()
{
	return mProperties.getCount();
}


const Property* PropertyBag::getProperty(int index) const
{
	return &mProperties[index];
}


Property* PropertyBag::getProperty(int index)
{
	return &mProperties[index];
}


const Property* PropertyBag::getProperty(const QiString& name) const
{
	for(int i=0; i<mProperties.getCount(); i++)
	{
		if (mProperties[i].mName == name)
			return &mProperties[i];
	}
	return NULL;
}


Property* PropertyBag::getProperty(const QiString& name)
{
	for(int i=0; i<mProperties.getCount(); i++)
	{
		if (mProperties[i].mName == name)
			return &mProperties[i];
	}
	return NULL;
}


QiString PropertyBag::getString(const QiString& name) const
{
	const Property* p = getProperty(name);
	if (p)
	{
		if (p->mValue != "")
			return p->mValue;
		else if (p->mInherited != "")
			return p->mInherited;
		else
			return p->mDefault;
	}
	else
		return QiString();
}


void PropertyBag::add(const QiString& name, const QiString& value, int flags)
{
	Property* p = getProperty(name);
	if (p)
		p->mDefault = "";
	else
	{
		Property &p = mProperties.add();
		p.mName = name;
		p.mDefault = value;
		p.mValue = "";
		p.mFlags = flags;
		
	}
}


void PropertyBag::setString(const QiString& name, const QiString& value)
{
	Property* p = getProperty(name);
	if (p)
		p->mValue = value;
}


void PropertyBag::readXml(QiXmlParser& xml, bool create)
{
	for(int i=0; i<xml.getAttributeCount(); i++)
	{
		if (create)
			add(xml.getAttributeName(i), xml.getAttribute(i));
		setString(xml.getAttributeName(i), xml.getAttribute(i));
	}
}


void PropertyBag::writeXml(QiXmlWriter& xml)
{
	for(int i=0; i<mProperties.getCount(); i++)
	{
		if (mProperties[i].mValue != "" && (mProperties[i].mFlags & PROPERTY_VOLATILE) == 0)
			xml.setAttribute(mProperties[i].mName, mProperties[i].mValue);
	}
}


void PropertyBag::operator=(const PropertyBag& other)
{
	clear();
	for(int i=0; i<other.mProperties.getCount(); i++)
	{
		Property& p = mProperties.add();
		p.mName = other.mProperties[i].mName;
		p.mDefault = other.mProperties[i].mDefault;
		p.mValue = other.mProperties[i].mValue;
		p.mInherited = other.mProperties[i].mInherited;
		p.mFlags = other.mProperties[i].mFlags;
	}
}


void PropertyBag::inheritFrom(const PropertyBag& parent)
{
	for(int i=0; i<mProperties.getCount(); i++)
		mProperties[i].mInherited = "";

	for(int i=0; i<parent.mProperties.getCount(); i++)
	{
		Property* p = getProperty(parent.mProperties[i].mName);
		if (p)
			p->mInherited = parent.mProperties[i].mValue;
	}
}


QiColor PropertyBag::getColor(const QiString& name) const
{
	QiString c = getString(name);
	QiColor ret;
	ret.r = c.getWord(0).toFloat();
	ret.g = c.getWord(1).toFloat();
	ret.b = c.getWord(2).toFloat();
	ret.a = 1.0f;
	if (c.getWordCount() > 3)
		ret.a = c.getWord(3).toFloat();
	return ret;
}

void PropertyBag::reset()
{
	for(int i=0; i<mProperties.getCount(); i++)
		mProperties[i].mValue = "";	
}

