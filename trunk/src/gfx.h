/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "qi_gfx.h"
#include "resman.h"

class Gfx
{
public:
	Gfx(class ResMan* resMan);
	~Gfx();

	void drawFullScreen();
	void drawFullScreenSolid(const QiColor& color);
	void drawRectangle(const QiMatrix4& mat, int res=0);
	void drawRectangle(const QiVec2& p0, const QiVec2& p1, const QiColor& color);

	void drawLine(const QiVec3& p0, const QiVec3& p1, const QiColor& col=QiColor(1,1,1,1), float width=1.0f);
	void drawLine(const QiVec2& p0, const QiVec2& p1, const QiColor& col=QiColor(1,1,1,1), float width=1.0f) { drawLine(QiVec3(p0.x, p0.y, 0.0f), QiVec3(p1.x, p1.y, 0.0f), col, width); }

	QiVertexFormat mFmt;
	QiVertexBuffer mVbo[5];
	QiIndexBuffer mIbo[5];

	Resource m2DShader;
	Resource m2DTexShader;
	Resource mFontShader;
	Resource mBodyShader;
	Resource mBodyShaderTex;
	Resource mWaterShader;
	Resource mShadowShader;
	Resource mFoliageShader;
	Resource mDecalShader;
	Resource mGlassShader;
	Resource mGlassTexShader;
	Resource mWireShader;
	Resource mSepiaShader;

	QiTexture mGaussBlob;
};


inline bool LineIntersect(const QiVec2& p1, const QiVec2& p2, const QiVec2& p3, const QiVec2& p4, QiVec2& result, float* t = NULL)
{
	float mua,mub;
	float denom,numera,numerb;

	denom  = (p4.y-p3.y) * (p2.x-p1.x) - (p4.x-p3.x) * (p2.y-p1.y);
	numera = (p4.x-p3.x) * (p1.y-p3.y) - (p4.y-p3.y) * (p1.x-p3.x);
	numerb = (p2.x-p1.x) * (p1.y-p3.y) - (p2.y-p1.y) * (p1.x-p3.x);

	// Are the line coincident? 
	if (QiAbs(numera) < QI_FLOAT_EPSILON && QiAbs(numerb) < QI_FLOAT_EPSILON && QiAbs(denom) < QI_FLOAT_EPSILON) {
		result.x = (p1.x + p2.x) / 2;
		result.y = (p1.y + p2.y) / 2;
		if (t)
			*t = 0.5f;
		return true;
	}

	// Are the line parallel 
	if (QiAbs(denom) < QI_FLOAT_EPSILON) {
		result.x = 0;
		result.y = 0;
		return false;
	}

	/* Is the intersection along the the segments */
	mua = numera / denom;
	mub = numerb / denom;
	if (mua < 0 || mua > 1 || mub < 0 || mub > 1)
	{
		result.x = 0;
		result.y = 0;
		return false;
	}
	result.x = p1.x + mua * (p2.x - p1.x);
	result.y = p1.y + mua * (p2.y - p1.y);
	if (t)
		*t = mua;
	return true;
}


