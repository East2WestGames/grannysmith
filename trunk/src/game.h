/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "propertybag.h"
#include "tweak.h"

class Game
{
public:
	enum State
	{
		LOADING,
		MENU,
		MOVIE,
		PLAY,
		EDIT
	};

	Game(class Device* device);
	~Game();

	void showAds(const QiString& cmd);
	void hideAds();

	void init(int frame);
	void loadConfig();
	void saveConfig();
	
	void loadAttractLevel();

	void frame();
	void update();
	void draw();
	void draw_MM();

	void stopLevel();
	void startLevel();

	void drawFbo(const QiMatrix4& mat);

	QiString getProperty(const QiString& prop);
	void setProperty(const QiString& prop, const QiString& value);
	PropertyBag* getPropertyBag(const QiString& object);

	QiString handleCommand(const QiString& cmd);

	void setState(State state);
	inline void requestState(State state) { mNextState = state; } 
	inline State getState() { return mCurrentState; }

	void confirmPurchase(const QiString& product);

	void onEnterState(State state);
	void onLeaveState(State state);
	
	bool extraEffects();
	void die();
	int getDieCount();

	class Device* mDevice;
	class QiInput* mInput;
	class Display* mDisplay;
	class QiRenderer* mRenderer;
	class ResMan* mResMan;
	class Audio* mAudio;
	class Debug* mDebug;
	class Gfx* mGfx;

	class Scene* mMenuScene;
	class Scene* mMovieScene;
	class Scene* mHudScene;
	class Level* mLevel;
	class Editor* mEditor;
	class Player* mPlayer;

	class HttpThread* mHttpThread;

	PropertyBag mProperties;

	float mTimeStep;
	int mFrame;
	float mFrameTime;
	QiTimer mGameTimer;
	QiTimer mFrameTimer;

	State mCurrentState;
	State mNextState;
	float mStateFade;

	QiString mMoviePath;

	QiString mNextLevel;
	QiString mLevelName;

	void logI(const char* msg);
	void logW(const char* msg);
	void logE(const char* msg);

	bool isHard() const;

	bool mPaused;
	bool mReplay;
	bool mTrigHudReload;
	class QiInputStream* mReplayStream;

	QiTimer mWallTime;
	float mUpdatedTime;
	bool mTvDrawn;

	QiString mPurchase;
	float mStoreTimer;
	bool mInPurchase;

	QiVec2 mFboTexMax;

	bool mTrigBackButton;
	bool mTrigMenuButton;
	bool mTrigBackFromResume;
	
	bool mGetGift;

	int mDieAccount;
	int mReplayAccount;


	class AsyncThread* mAsyncThread;
};


extern Game* gGame;

