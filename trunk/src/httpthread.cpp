/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "game.h"
#include "player.h"
#include "httpthread.h"
#include "qi_net.h"
#include "qi_file.h"


HttpThread::HttpThread() : mNewAds(false), mAdsRevision(0), mAdsFront(false), mAdsPlus(false)
{
}


//Report statistics and check for new banners
bool HttpThread::reportStats()
{
	QI_PRINT("Reporting stats");

	QiMemoryStream<256> tmp;
	if (!mResMan.load("user://granny-progression.xml", tmp))
		return false;

	gGame->mPlayer->decrypt(tmp.getData(), tmp.getSize());

	QiMemoryStream<4096> compressedReport;
	QiCompress c;
	c.init(0, 0, -1, true);
	c.process(tmp, compressedReport, tmp.getSize(), true);
	c.shutdown();

	QiString url = QiString("http://grannysmithgame.com/stats/stats.php?product=") + QiString(PRODUCT_STRING) + QiString("&platform=") + QiString(PLATFORM_STRING) + QiString("&version=") + QiString(VERSION_STRING);
	if (mResMan.httpPost(url, compressedReport.getData(), compressedReport.getSize()))
	{
		QI_PRINT("Stats successfully reported");
		return true;
	}
	return false;
}



bool HttpThread::downloadFile(const QiString url, const QiString& filename)
{
	QI_PRINT("Downloading " + filename);
	QiHttpRequest http;
	QiMemoryStream<8> out;
	if (http.get(url, out) && out.getSize() > 0)
	{
		if (gGame->mResMan->save("user://" + filename, out.getData(), out.getSize()))
			return true;
		else
			QI_PRINT("Error writing local file");
	}
	else
		QI_PRINT("Failed to download");
	return false;
}

void HttpThread::checkBanners()
{
	QiHttpRequest http;
	QiMemoryStream<8> out;
	QiString baseUrl = QiString("http://grannysmithgame.com/ads/");
	int currentRev = gGame->mPlayer->mProperties.getInt("adsrevision");
	QiString url = baseUrl + QiString("ads.php?product=") + QiString(PRODUCT_STRING) + QiString("&platform=") + QiString(PLATFORM_STRING) + QiString("&version=") + QiString(VERSION_STRING) + QiString("&rev=") + currentRev;
	QI_PRINT("Checking " + url);
	if (http.get(url, out))
	{
		QiXmlParser xml;
		if (xml.parse(out, out.getSize()))
		{
			int rev = xml.getAttribute("revision").toInt();
			int showFront = xml.getAttribute("showfront").toInt();
			int showPlus = xml.getAttribute("showplus").toInt();
			QiString folder = xml.getAttribute("folder");

			QI_PRINT("Content revision " + rev);
			if (rev > currentRev)
			{
				QiString url = baseUrl + folder + "/";
				if (!downloadFile(url + "ads-icon.png", "ads-icon.png"))
					return;
				if (!downloadFile(url + "ads-front.png", "ads-front.png"))
					return;
				if (!downloadFile(url + "ads.xml", "ads.xml"))
					return;

				QI_PRINT("New ad revision ready");
				mAdsRevision = rev;
				mAdsFront = (showFront==1);
				mAdsPlus = (showPlus==1);
				mNewAds = true;
			}
			else if (rev == 0)
			{
				mAdsRevision = 0;
				mAdsFront = false;
				mAdsPlus = false;
				mNewAds = true;
			}
		}
	}
}


void HttpThread::run()
{
#if ENABLE_NET_STATS
	QiUInt64 current = QiSystem::getCurrentDateTime();
	QiUInt64 lastReported = gGame->mPlayer->mLastReportedStats;
	if (current > lastReported+86400)
	{
		if (reportStats())
			gGame->mPlayer->mLastReportedStats = current;
	}
#endif
#if ENABLE_NET_ADS
	if (gGame->mPlayer->mProperties.getInt("timesstarted") > 2)
		checkBanners();
#endif
}

