/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "config.h"
#include "game.h"
#include "resman.h"
#include "player.h"
#include "scene.h"
#include "device.h"

#include "qi_base.h"
#include "qi_math.h"

#ifdef QI_IOS
#include "gamecenter.h"
#endif


class PlayerLevel
{
public:
	PlayerLevel() : mPlayCount(0), mClearCount(0), mDieCount(0), mRestartCount(0), mScore(0), mScoreHard(0), mApples(0), mApplesHard(0), mSkipped(false), mSkippedHard(false), mBonus(false), mWorld(NULL)
	{
	}

	QiString mName;
	QiString mPath;
	QiString mTitle;
	int mPlayCount;
	int mClearCount;
	int mDieCount;
	QiVec2 mDiePos;
	int mRestartCount;
	QiVec2 mRestartPos;
	int mScore;
	int mScoreHard;
	int mApples;
	int mApplesHard;
	bool mSkipped;
	bool mSkippedHard;
	bool mBonus;
	QiString mRequire;
	class PlayerWorld* mWorld;
	
	int getScore()
	{
		if (gGame->isHard())
			return mScoreHard;
		else
			return mScore;
	}

	int getApples()
	{
		if (gGame->isHard())
			return mApplesHard;
		else
			return mApples;
	}
	
	bool isSkipped()
	{
		if (gGame->isHard())
			return mSkippedHard;
		else
			return mSkipped;
	}
};


class PlayerWorld
{
public:
	QiArray<PlayerLevel*> mLevels;
	
	QiString mName;
	QiString mMusicPath;
};


Player::Player()
{
	mCurrentLevel = NULL;

	mProperties.add("uid", "0");
	mProperties.add("coins", "0");
	mProperties.add("level", "");
	mProperties.add("baseball", "0");
	mProperties.add("banana", "0");
	mProperties.add("helmet", "0");
	mProperties.add("unlockall", "0");
	mProperties.add("unlockchar", "1");
	mProperties.add("character", "granny");
	mProperties.add("iapcoins1000","0");
	mProperties.add("iapcoins3000","0");
	mProperties.add("iapunlockall","0");
	mProperties.add("rated", "0");
	mProperties.add("hard", "0");
	mProperties.add("adsrevision", "0");
	mProperties.add("adsfront", "0");
	mProperties.add("adsplus", "0");
	mProperties.add("adsshown", "0");
	mProperties.add("timesstarted", "0");

	QiRandomize();
	mProperties.setInt("uid", rand());

	mLastReportedStats = 0;
}


Player::~Player()
{
	cleanup();
}


void Player::cleanup()
{
	for(int i=0; i<mWorlds.getCount(); i++)
		QI_DELETE(mWorlds[i]);
	mWorlds.clear();
	for(int i=0; i<mLevels.getCount(); i++)
		QI_DELETE(mLevels[i]);
	mLevels.clear();
}


void Player::init()
{
	cleanup();
	QiMemoryStream<256> tmp;
	gGame->mResMan->load("game.xml", tmp);
	QiXmlParser xml(tmp, tmp.getSize());
	if (xml.getName() == "game")
	{
		xml.enter();
		while(xml.isValid())
		{
			if (xml.getName() == "world")
			{
				PlayerWorld* w = QI_NEW PlayerWorld();
				w->mName = xml.getAttribute("name");
				w->mMusicPath = xml.getAttribute("music");
				mWorlds.add(w);

				xml.enter();
				while(xml.isValid())
				{
					if (xml.getName() == "level")
					{
						PlayerLevel* l = QI_NEW PlayerLevel();
						l->mName = xml.getAttribute("name");
						l->mTitle = xml.getAttribute("title");
						l->mPath = xml.getAttribute("path");
						l->mRequire = xml.getAttribute("require");
						if (xml.getAttribute("bonus") == "true")
							l->mBonus = true;
						l->mWorld = w;
						w->mLevels.add(l);
						mLevels.add(l);
					}
					xml.next();
				}
				xml.leave();
			}
			xml.next();
		}
		xml.leave();
	}
}


void Player::reset()
{
	init();
	mProperties.reset();
}


void Player::save()
{
	QiMemoryStream<256> tmp;
	save(tmp);
	encrypt(tmp.getData(), tmp.getSize());
	gGame->mResMan->save("user://granny-progression.xml", tmp.getData(), tmp.getSize());
}


const char* encryptionKey = "gr4nny0n5k47es";

void Player::encrypt(char* data, int size)
{
	QiString tmp = encryptionKey;
	for(int i=0; i<size; i++)
		data[i] += (char)tmp[i%tmp.getLength()] + (char)size;
}


void Player::decrypt(char* data, int size)
{
	QiString tmp = encryptionKey;
	for(int i=0; i<size; i++)
		data[i] -= (char)tmp[i%tmp.getLength()] + (char)size;
}



void Player::load()
{
	QiMemoryStream<256> tmp;
	if (gGame->mResMan->load("user://granny-progression.xml", tmp))
	{
		decrypt(tmp.getData(), tmp.getSize());
		load(tmp, tmp.getSize(), false);
	}
	checkForUnlockedCharacters(false);
	mProperties.setInt("timesstarted", mProperties.getInt("timesstarted")+1);
	save();
}


void Player::load(QiInputStream& stream, int size, bool merge)
{
	QiXmlParser xml(stream, size);
	if (xml.getName() == "granny")
	{
		mProperties.readXml(xml);
		mLastReportedStats = xml.getAttribute("laststats").toInt64();
		xml.enter();
		while(xml.isValid())
		{
			if (xml.getName() == "level")
			{
				QiString name = xml.getAttribute("name");
				PlayerLevel* l = getLevel(name);
				if (l)
				{
					l->mScore = QiMax(l->mScore, xml.getAttribute("score").toInt());
					l->mScoreHard = QiMax(l->mScoreHard, xml.getAttribute("scorehard").toInt());
					l->mApples = QiMax(l->mApples, xml.getAttribute("apples").toInt());
					l->mApplesHard = QiMax(l->mApplesHard, xml.getAttribute("appleshard").toInt());
					l->mPlayCount = QiMax(l->mPlayCount, xml.getAttribute("playcount").toInt());
					l->mClearCount = QiMax(l->mClearCount, xml.getAttribute("clearcount").toInt());
					l->mDieCount = QiMax(l->mDieCount, xml.getAttribute("diecount").toInt());
					QiString dp = xml.getAttribute("diepos");
					l->mDiePos.set(dp.getWord(0).toFloat(), dp.getWord(1).toFloat());
					l->mRestartCount = QiMax(l->mRestartCount, xml.getAttribute("restartcount").toInt());
					QiString rp = xml.getAttribute("restartpos");
					l->mRestartPos.set(rp.getWord(0).toFloat(), rp.getWord(1).toFloat());
					l->mSkipped = (xml.getAttribute("skipped") == "true");
					l->mSkippedHard = (xml.getAttribute("skippedhard") == "true");
				}
			}
			xml.next();
		}
		xml.leave();
	}
}


void Player::save(QiOutputStream& stream)
{
	QiXmlWriter xml;
	xml.enter("granny");
	mProperties.writeXml(xml);
	xml.setAttribute("version", VERSION_STRING);
	xml.setAttribute("platform", PLATFORM_STRING);
	xml.setAttribute("model", gGame->mDevice->getModelName());
	xml.setAttribute("laststats", QiString() + (long long)mLastReportedStats);

	for(int i=0; i<mLevels.getCount(); i++)
	{
		PlayerLevel* l = mLevels[i];
		if (l->mPlayCount > 0 || l->mSkipped || l->mSkippedHard)
		{
			xml.enter("level");
			xml.setAttribute("name", l->mName);
			xml.setAttribute("score", QiString() + l->mScore);
			xml.setAttribute("scorehard", QiString() + l->mScoreHard);
			xml.setAttribute("apples", QiString() + l->mApples);
			xml.setAttribute("appleshard", QiString() + l->mApplesHard);
			xml.setAttribute("playcount", QiString() + l->mPlayCount);
			xml.setAttribute("clearcount", QiString() + l->mClearCount);
			xml.setAttribute("diecount", QiString() + l->mDieCount);
			xml.setAttribute("diepos", QiString() + int(l->mDiePos.x) + QiString(" ") + int(l->mDiePos.y));
			xml.setAttribute("restartcount", QiString() + l->mRestartCount);
			xml.setAttribute("restartpos", QiString() + int(l->mRestartPos.x) + QiString(" ") + int(l->mRestartPos.y));
			if (l->mSkipped)
				xml.setAttribute("skipped", "true");
			if (l->mSkippedHard)
				xml.setAttribute("skippedhard", "true");
			xml.leave();
		}
	}
	xml.leave();
	xml.write(stream);
}


int Player::getCoinCount()
{
	return mProperties.getInt("coins");
}


void Player::addCoins(int coins)
{
	mProperties.setInt("coins", getCoinCount() + coins);
}


PlayerLevel* Player::getLevel(const QiString& name)
{
	for(int i=0; i<mLevels.getCount(); i++)
		if (mLevels[i]->mName == name)
			return mLevels[i];
	QI_ERROR("Level not found " + name);
	return NULL;
}


#define GET_LEVEL_OR_RETURN PlayerLevel* level = getLevel(levelName); if (!level) return


QiString Player::getLevelPath()
{
	if (mCurrentLevel)
		return mCurrentLevel->mPath;
	else
		return "";
}


void Player::skipLevel(const QiString& levelName)
{
	GET_LEVEL_OR_RETURN;
	if (gGame->isHard())
		level->mSkippedHard = true;
	else
		level->mSkipped = true;
}


bool Player::isSkipped(const QiString& levelName)
{
	GET_LEVEL_OR_RETURN false;
	if (mProperties.getBool("unlockall"))
		return true;
	return level->isSkipped();
}


QiString Player::getLevelTitle(const QiString& levelName)
{
	GET_LEVEL_OR_RETURN "";
	return level->mTitle;
}

void Player::registerPlayed()
{
	mCurrentLevel->mPlayCount++;
}


void Player::checkForUnlockedCharacters(bool notify)
{
//	for(int j=0; j<mWorlds.getCount(); j++)
//	{
//		PlayerWorld* w = mWorlds[j];
//		bool ok = true;
//		for(int i=0; i<w->mLevels.getCount(); i++)
//		{
//			if (w->mLevels[i]->mClearCount == 0 && !w->mLevels[i]->mBonus)
//			{
//				ok = false;
//				break;
//			}
//		}
//		if (ok)
//		{
//			if (w->mName == "farm")
//			{
//				if (!isCharacterAvailable("scruffy"))
//				{
//					unlockCharacter("scruffy");
//					save();
//					if (notify)
//						gGame->mMenuScene->mScript.handleCommand("unlockedcharacter scruffy");
//				}
//			}
//			if (w->mName == "city")
//			{
//				if (!isCharacterAvailable("stanley"))
//				{
//					unlockCharacter("stanley");
//					save();
//					if (notify)
//						gGame->mMenuScene->mScript.handleCommand("unlockedcharacter stanley");
//				}
//			}
//		}
//	}
//	bool threeApples = true;
//	for(int j=0; j<mWorlds.getCount(); j++)
//	{
//		PlayerWorld* w = mWorlds[j];
//		for(int i=0; i<w->mLevels.getCount(); i++)
//		{
//			if (w->mLevels[i]->getApples() < 3)
//			{
//				threeApples = false;
//				break;
//			}
//		}
//	}
//	if (threeApples)
//	{
//		if (!isCharacterAvailable("ouie"))
//		{
//			unlockCharacter("ouie");
//			save();
//			if (notify)
//				gGame->mMenuScene->mScript.handleCommand("unlockedcharacter ouie");
//		}
//	}
}


void Player::registerCleared(int score, int apples)
{
	mCurrentLevel->mClearCount++;
	if (gGame->isHard())
	{
		mCurrentLevel->mScoreHard = QiMax(mCurrentLevel->mScoreHard, score);
		mCurrentLevel->mApplesHard = QiMax(mCurrentLevel->mApplesHard, apples);
	}
	else
	{
		mCurrentLevel->mScore = QiMax(mCurrentLevel->mScore, score);
		mCurrentLevel->mApples = QiMax(mCurrentLevel->mApples, apples);
	}

	checkForUnlockedCharacters(true);
	
	updateLeaderboard(mCurrentLevel->mWorld->mName);
}


void Player::registerDie(const QiVec2& pos)
{
	mCurrentLevel->mDieCount++;
	mCurrentLevel->mDiePos = pos;
}


void Player::registerRestart(const QiVec2& pos)
{
	mCurrentLevel->mRestartCount++;
	mCurrentLevel->mRestartPos = pos;
}


int Player::getScore(const QiString& levelName)
{
	GET_LEVEL_OR_RETURN 0;
	return level->getScore();
}


int Player::getApples(const QiString& levelName)
{
	GET_LEVEL_OR_RETURN 0;
	return level->getApples();
}


bool Player::isWorldAvailable(const QiString& worldName)
{
	return true;
}


bool Player::isLevelAvailable(const QiString& levelName)
{
	GET_LEVEL_OR_RETURN false;
	if (mProperties.getBool("unlockall"))
		return true;
	if (level->isSkipped())
		return true;
	if (gGame->isHard())
	{
		if (level->getApples() > 0)
			return true;
	}
	else
	{
		if (level->getScore() > 0)
			return true;
	}
	if (level->mRequire != "")
	{
		int c = level->mRequire.getWordCount();
		for(int i=0; i<c; i++)
		{
			QiString name = level->mRequire.getWord(i);
			PlayerLevel* l = getLevel(name);
			if (l)
			{
				if (gGame->isHard())
				{
					if (l->getApples()>0 || l->isSkipped())
						return true;
				}
				else
				{
					if (l->getScore()>0 || l->isSkipped())
						return true;
				}
			}
		}
		return false;
	}
	return true;
}


void Player::setActiveWorld(const QiString& worldName)
{
}


void Player::setActiveLevel(const QiString& levelName)
{
	mProperties.setString("level", levelName);
	mCurrentLevel = getLevel(levelName);
}


void Player::registerAchievement(const QiString& achievement)
{
}


QiString Player::getActiveWorld() const
{
    return mCurrentLevel->mWorld->mName;
}


QiString Player::getActiveLevel() const
{
    return mCurrentLevel->mName;
}


void Player::addPowerUp(const QiString& type, int count)
{
	mProperties.setInt(type, getPowerUpCount(type) + count);
}


void Player::consumePowerUp(const QiString& type)
{
	addPowerUp(type, -1);
}


int Player::getPowerUpCount(const QiString& type)
{
	return mProperties.getInt(type);
}


void Player::unlockAll()
{
	mProperties.setBool("unlockall", true);
}

bool Player::hasUnlockAll()
{
	return mProperties.getBool("unlockall");
}


QiString Player::getLevelMusicPath()
{
	return mCurrentLevel->mWorld->mMusicPath;
}


QiString Player::getCharacter()
{
	return mProperties.getString("character");
}


void Player::selectCharacter(const QiString& name)
{
	if (isCharacterAvailable(name))
		mProperties.setString("character", name);
}


bool Player::isCharacterAvailable(const QiString& name)
{
	int c = mProperties.getInt("unlockchar");
	if (name == "granny" && (c & 1))
		return true;
	if (name == "scruffy" && (c & 2))
		return true;
	if (name == "stanley" && (c & 4))
		return true;
	if (name == "ouie" && (c & 8))
		return true;	
	return false;
}


void Player::unlockCharacter(const QiString& name)
{
	int c = mProperties.getInt("unlockchar");
	if (name == "granny")
		c |= 1;
	if (name == "scruffy")
		c |= 2;
	if (name == "stanley")
		c |= 4;
	if (name == "ouie")
		c |= 8;
	mProperties.setInt("unlockchar", c);
}


void Player::unlockAllCharacters()
{
	mProperties.setInt("unlockchar", 255);
}


int Player::getWorldScore(const QiString& worldName)
{
	for(int i=0; i<mWorlds.getCount(); i++)
	{
		PlayerWorld* w = mWorlds[i];
		if (w->mName == worldName)
		{
			int score = 0;
			for(int j=0; j<w->mLevels.getCount(); j++)
			{
				PlayerLevel* l = w->mLevels[j];
				score += l->getScore();
			}
			return score;
		}
	}
	return 0;
}


void Player::updateLeaderboard(const QiString& worldName)
{
#ifdef QI_IOS
	int totalScore = 0;
	for(int i=0; i<mLevels.getCount(); i++)
	{
		PlayerLevel* l = mLevels[i];
		totalScore += l->getScore();
	}

	int score = getWorldScore(worldName);
	if (gGame->isHard())
	{
		gameCenterReportScore(worldName + "hard", score);
		gameCenterReportScore("totalhard", totalScore);
	}
	else
	{
		gameCenterReportScore(worldName, score);
		gameCenterReportScore("total", totalScore);
	}
#endif		
}

