/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "tweak.h"

#ifdef ENABLE_TWEAK

#include "qi_base.h"
#include "qi_file.h"

static const int MAX_TWEAKS = 256;

static float gTweakValues[MAX_TWEAKS];
static QiString gTweakNames[MAX_TWEAKS];
static int gTweakCount = 0;


void Tweak::init(const char* file)
{
	gTweakCount = 0;
	QiFileInputStream fin;
	if (fin.open(file))
	{
		char str[256];
		while(fin.readLine(str, 256))
		{
			QiString s(str);
			QiString n = s.getWord(0);
			float v = s.getWord(1).toFloat();		
			QI_ASSERT(gTweakCount < MAX_TWEAKS, "Too many tweaks");
			if (gTweakCount < MAX_TWEAKS)
			{
				gTweakNames[gTweakCount] = n;
				gTweakValues[gTweakCount] = v;
				gTweakCount++;
			}
		}
	}
}


float Tweak::getFloat(const char* name, float value)
{
	QiString n(name);
	int c = gTweakCount;
	for(int i=0; i<c; i++)
	{
		if (n == gTweakNames[i])
			return gTweakValues[i];
	}
	return value;
}


int Tweak::getInt(const char* name, int value)
{
	return (int)getFloat(name, (float)value);
}


bool Tweak::getBool(const char* name, bool value)
{
	return getFloat(name, value ? 1.0f : 0.0f) != 0.0f;
}

#endif
