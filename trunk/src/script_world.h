/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "level.h"
#include "body.h"
#include "Box2D.h"
#include "sensor.h"
#include "joint.h"
#include "dude.h"
#include "breakable.h"

static Body* findBody(const QiString& name)
{
	Entity* e = gGame->mLevel->findEntity(name);
	if (e && e->getType() == Entity::BODY)
		return (Body*)e;

	gGame->logW(QiString("Script: Body not found: ") + name);
	return NULL;
}


static void mgGetPose(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	float x=0, y=0, r=0;
	QiString name = args.asString(0);
	Entity* e = gGame->mLevel->findEntity(name);
	if (e)
	{
		x = e->mTransform.pos.x;
		y = e->mTransform.pos.y;
		r = e->mTransform.rot;
	}
	else
		gGame->logW(QiString("Script: Entity not found: ") + name);
	ret.addFloat(x);
	ret.addFloat(y);
	ret.addFloat(r);
}


static void mgSetPose(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	QiString name = args.asString(0);
	float x = args.asFloat(1);
	float y = args.asFloat(2);
	float r = args.asFloat(3);
	Entity* e = gGame->mLevel->findEntity(name);
	if (e)
		e->setTransform(QiTransform2(QiVec2(x, y), r));
	else
		gGame->logW(QiString("Script: Entity not found: ") + name);
}


static void mgGetVelocity(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	float x=0, y=0, r=0;
	Body* b = findBody(args.asString(0));
	if (b && b->mPhysBody)
	{
		x = b->mPhysBody->GetLinearVelocity().x;
		y = b->mPhysBody->GetLinearVelocity().y;
		r = b->mPhysBody->GetAngularVelocity();
	}
	ret.addFloat(x);
	ret.addFloat(y);
	ret.addFloat(r);
}


static void mgSetVelocity(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Body* b = findBody(args.asString(0));
	if (b && b->mPhysBody)
	{
		float x = args.asFloat(1);
		float y = args.asFloat(2);
		float r = args.asFloat(3);
		b->mPhysBody->SetLinearVelocity(b2Vec2(x, y));
		b->mPhysBody->SetAngularVelocity(r);
	}
}


static void mgSetFilter(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Body* b = findBody(args.asString(0));
	if (b && b->mPhysBody)
	{
		int category = args.asInt(1);
		int mask = args.asInt(2);
		b2Fixture* f = b->mPhysBody->GetFixtureList();
		while(f)
		{
			b2Filter filter = f->GetFilterData();
			filter.categoryBits = category;
			filter.maskBits = mask;
			f->SetFilterData(filter);
			f = f->GetNext();
		}
	}
}


static void mgSetDynamic(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Body* b = findBody(args.asString(0));
	if (b && b->mPhysBody)
	{
		bool dynamic = args.asBool(1);
		if (b->mPhysBody)
			b->mPhysBody->SetType(dynamic ? b2_dynamicBody : b2_staticBody);
	}
}


static void mgMove(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Body* b = findBody(args.asString(0));
	if (b)
	{
		if (b->mPhysBody)
			b->mPhysBody->SetAwake(true);
		b->mMoveTarget.x = args.asFloat(1);
		b->mMoveTarget.y = args.asFloat(2);
		b->mMoveStrength = args.asFloat(3);
		if (args.getCount() > 4)
			b->mMoveMaxSpeed = args.asFloat(4);
		else
			b->mMoveMaxSpeed = QI_FLOAT_MAX;
	}
}


static void mgRotate(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Body* b = findBody(args.asString(0));
	if (b)
	{
		if (b->mPhysBody)
			b->mPhysBody->SetAwake(true);
		b->mRotateTarget = args.asFloat(1);
		b->mRotateStrength = args.asFloat(2);
		if (args.getCount() > 3)
			b->mRotateMaxSpeed = args.asFloat(3);
		else
			b->mRotateMaxSpeed = QI_FLOAT_MAX;
	}
}


static void mgGetDistanceBetweenBodies(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Body* b0 = findBody(args.asString(0));
	Body* b1 = findBody(args.asString(1));
	float d = 0.0f;
	if (b0 && b1)
		d = gGame->mLevel->getDistance(b0, b1);
	ret.addFloat(d);
}


static void mgGetDistanceToPoint(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Body* b = findBody(args.asString(0));
	float d = 0.0f;
	if (b)
	{
		QiVec2 p(args.asFloat(1), args.asFloat(2));
		d = gGame->mLevel->getDistance(b, p);
	}
	ret.addFloat(d);
}


static void mgInSensor(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	QiString sName = args.asString(0);
	Entity* e = gGame->mLevel->findEntity(sName);
	if (!e)
	{
		gGame->logE("Script: " + sName + " not found");
		return;
	}
	if (e->getType() != Entity::SENSOR)
	{
		gGame->logE("Script: " + sName + " not sensor");
		return;
	}
	Sensor* sensor = (Sensor*)e;

	QiString eName = args.asString(1);
	Entity* e2 = gGame->mLevel->findEntity(eName);
	if (!e2)
	{
		gGame->logE("Script: " + eName + " not found");
		return;
	}
	ret.addBool(sensor->testPoint(e2->mTransform.pos));
}


static void mgBreakJoint(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	QiString sName = args.asString(0);
	Entity* e = gGame->mLevel->findEntity(sName);
	if (!e)
	{
		gGame->logE("Script: " + sName + " not found");
		return;
	}
	if (e->getType() != Entity::JOINT)
	{
		gGame->logE("Script: " + sName + " not joint");
		return;
	}
	Joint* joint= (Joint*)e;
	joint->breakJoint();
}


static void mgIsGrabbed(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	QiString sName = args.asString(0);
	Entity* e = gGame->mLevel->findEntity(sName);
	if (!e)
	{
		gGame->logE("Script: " + sName + " not found");
		return;
	}
	Dude* d = gGame->mLevel->mDude;
	if (args.asBool(1))
		d = gGame->mLevel->mBadGuy;

	if (e->getType() == Entity::HANDLE)
	{
		ret.addBool((Entity*)d->mGrabbedHandle == e);
	}
	else if (e->getType() == Entity::WIRE)
	{
		ret.addBool((Entity*)d->mGrabbedWire == e);
	}
	else if (e->getType() == Entity::BODY)
	{
		ret.addBool((Entity*)d->mRailBody == e);
	}
	else
	{
		gGame->logE("Script: " + sName + " not handle, wire or body");
	}
}


static void mgIsBroken(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	QiString sName = args.asString(0);
	Entity* e = gGame->mLevel->findEntity(sName);
	if (!e)
	{
		gGame->logE("Script: " + sName + " not found");
		return;
	}
	if (e->getType() != Entity::BREAKABLE)
	{
		gGame->logE("Script: " + sName + " not breakable");
		return;
	}
	Breakable* b = (Breakable*)e;
	ret.addBool(b->mFragments.getCount() > 1);
}

static void mgCreateEffect(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{

	Script* s = (Script*)script->userData;
	QiString name = args.asString(0);
	Script::Effect* effect= QI_NEW Script::Effect(s, name);

	int index = 0;
	s->mObjects.add(effect, &index);
	ret.addInt(index);

}

static void mgParticle(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Effect* effect = s->getEffect(args.asInt(0));
	if (!effect)
		return;

	if (effect->mParticleSystem)
	{
		QiVec3 pos, vel;
		pos.x = args.asFloat(1);
		pos.y = args.asFloat(2);
		pos.z = args.asFloat(3);
		vel.x = args.asFloat(4);
		vel.y = args.asFloat(5);
		vel.z = args.asFloat(6);
		int texture = -1;
		if (args.getCount() > 7)
			texture = args.asInt(7);

		effect->mParticleSystem->spawn(pos, vel, texture);
	}
}


static void mgRnd(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	ret.addFloat(QiRndNormal(args.asFloat(0), args.asFloat(1)));
}


static void mgParticlePoof(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Effect* effect = s->getEffect(args.asInt(0));
	if (!effect)
		return;

	if (effect->mParticleSystem)
	{
		QiVec3 pos, vel;
		pos.x = args.asFloat(1);
		pos.y = args.asFloat(2);
		vel.x = args.asFloat(3);
		vel.y = args.asFloat(4);
		float posRadius = args.asFloat(5);
		float velRadius = args.asFloat(6);
		int count = args.asInt(7);
		for(int i=0; i<count; i++)
		{
			QiVec3 p(QiRndNormal(-posRadius, posRadius), QiRndNormal(-posRadius, posRadius), 0.0f);
			QiVec3 v(QiRndNormal(-velRadius, velRadius), QiRndNormal(-velRadius, velRadius), 0.0f);
			effect->mParticleSystem->spawn(pos+p, vel+v, -1);
		}
	}	
}


static void mgSetSensorEnabled(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	QiString sName = args.asString(0);
	Entity* e = gGame->mLevel->findEntity(sName);
	if (!e)
	{
		gGame->logE("Script: " + sName + " not found");
		return;
	}
	if (e->getType() != Entity::SENSOR)
	{
		gGame->logE("Script: " + sName + " not sensor");
		return;
	}
	Sensor* s = (Sensor*)e;
	s->mEnabled = args.asBool(1);
}


static void mgCameraShake(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	float amount = args.asFloat(0);
	gGame->mDisplay->mShakeTimer = 0.5f * QiClamp(amount, 0.0f, 1.0f);	
}

