/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "config.h"
#include "game.h"
#include "resman.h"
#include "file/qifileinputstream.h"
#include "file/qifileoutputstream.h"
#include "net/qihttprequest.h"
#include "snd/qiwavdecoder.h"
#include "snd/qivorbisdecoder.h"
#include "gfx/qijpegdecoder.h"
#include "gfx/qipngdecoder.h"
#include "gfx/qimtx.h"
#include "gfx/qigl.h"
#include "audio.h"
#include "device.h"
#include "net/qitcpsocket.h"
#include "profiling.h"

extern bool gTegraAvailable;
QiTcpSocket* ResMan::sAssetSocket = NULL;


ResMan::ResMan() : mPreventUnload(false)
{
	mAssetPath = gGame->mDevice->getAssetPath();
	if (mAssetPath.endsWith("/"))
		mAssetPath = mAssetPath.substring(0, mAssetPath.getLength()-1);

	mUserPath = gGame->mDevice->getUserPath();
	if (mUserPath.endsWith("/"))
		mUserPath = mUserPath.substring(0, mUserPath.getLength()-1);
}


ResMan::~ResMan()
{
	mPreventUnload = false;
	clear();
}


void ResMan::clear()
{
	if (mPreventUnload)
		return;

	while(mResources.getCount() > 0)
	{
		QiHashTable<Res>::Iterator iter = mResources.getIterator();
		Res& r = iter.next();
		r.release();
		mResources.remove(r);
	}
}


void ResMan::clearUnused()
{
	bool pu = mPreventUnload;
	mPreventUnload = false;	
	bool more = true;
	do
	{
		more = false;
		QiHashTable<Res>::Iterator iter = mResources.getIterator();
		while(iter.hasNext())
		{
			Res& r = iter.next();
			if (r.mRefCount == 0)
			{
				r.release();
				mResources.remove(r);
				more = true;
				break;
			}
		}
	} while(more);
	mPreventUnload = pu;	
}


void ResMan::setAdditionalPath(const QiString& path)
{
	mAdditionalPath = path;
	if (mAdditionalPath.endsWith("/"))
		mAdditionalPath = mAdditionalPath.substring(0, mAdditionalPath.getLength()-1);
}


bool ResMan::load(const QiString& path, QiOutputStream& outStream)
{
	PROFILE_ZONE("ResMan::load");
	PROFILE_PRINTF("%s", path.c_str());

	QiString protocol;
	QiString relativePath = path;
	int i = path.getIndexOf("://");
	if (i != -1)
	{
		protocol = path.substring(0, i);
		relativePath = path.substring(i+3);
	}
	if (protocol == "http")
	{
		QiHttpRequest http;
		return http.get(path, outStream);
	}
	else
	{
		QiString p;
		if (protocol == "user")
			p = mUserPath + "/";
		else
			p = mAssetPath + "/";

		if (sAssetSocket && protocol != "user")
		{
			if (sAssetSocket->isOpen())
			{
				if (getFileFromAssetServer(relativePath, outStream))
					return true;
				else if (getFileFromAssetServer(mAdditionalPath + "/" + relativePath, outStream))
					return true;
				else
					return false;
			}
			else
				disconnectAssetServer();
		}
		
		QiFileInputStream fin;

#ifdef QI_ANDROID
		if (protocol == "user")
		{
			if (fin.openLeanAndMean(p + relativePath))
				return fin.readBuffer(outStream, fin.getSize());
			else
				return false;
		}
		else
		{
			relativePath = relativePath + ".mp3";
			p = "";

			QiString compressedPath = relativePath.replace(".mp3", ".gz.mp3");
			if (fin.open(p + compressedPath))
			{
				QiMemoryStream<8192> compressed;
				if (!fin.readBuffer(compressed, fin.getSize()))
					return false;

				QiDecompress dec;
				dec.init();
				dec.process(compressed, outStream, compressed.getSize());
				dec.shutdown();
				return true;
		}
			else if (fin.open(p + mAdditionalPath + "/" + compressedPath))
			{
				QiMemoryStream<8192> compressed;
				if (!fin.readBuffer(compressed, fin.getSize()))
					return false;

				QiDecompress dec;
				dec.init();
				dec.process(compressed, outStream, compressed.getSize());
				dec.shutdown();
				return true;
			}
		}
#endif
	
		if (fin.open(p + relativePath))
			return fin.readBuffer(outStream, fin.getSize());
		else if (fin.open(p + mAdditionalPath + "/" + relativePath))
			return fin.readBuffer(outStream, fin.getSize());
		else
			return false;

	}
	return false;
}


bool ResMan::save(const QiString& path, const void* data, int size)
{
	QiString protocol;
	QiString relativePath = path;
	int i = path.getIndexOf("://");
	if (i != -1)
	{
		protocol = path.substring(0, i);
		relativePath = path.substring(i+3);
	}
	QiString p;
	if (protocol == "user")
		p = mUserPath;
	else
		p = mAssetPath;

	QiFileOutputStream fout;
	if (fout.open(p + "/" + relativePath))
		return fout.writeBuffer(data, size);
	
	return false;
}


Resource ResMan::acquireTexture(const QiString& path)
{
	PROFILE_ZONE("ResMan::acquireTexture");
	Res* res = mResources.getPointer(Res(path));
	if (res)
	{
		res->mRefCount++;
		return Resource(this, path, res->mPointer, Resource::TEXTURE);
	}

	QiTexture* tex = QI_NEW QiTexture();
	tex->enableMipMap(true);

	QiMemoryStream<256> in;

    bool loaded = false;

#if defined(QI_ANDROID)
	bool hasDdsSupport = gTegraAvailable;
	hasDdsSupport = false;
	if (hasDdsSupport && load(path + ".dds", in))
	{
		int size = in.getSize();
		QiMemoryStream<8> unzipped;
		QiDecompress dec;
		dec.init();
		dec.process(in, unzipped, size);
		dec.shutdown();
		size = unzipped.getSize();
		void* compressed = QiAlloc(size);
		tex->init(16, 16, GL_RGB, false);
		tex->uploadDXT(unzipped.getData(), size);
		tex->releaseMemory();
		QiFree(compressed);
        loaded = true;
	}
#elif defined(QI_IOS)
    if (gGame->mDevice->getModelName() == "iPad3")
    {
        QiString retinaPath = path;
        retinaPath = retinaPath.replace(".png", "_retina.png");
        retinaPath = retinaPath.replace(".jpg", "_retina.jpg");
        if (!loaded && load(retinaPath + ".pvrtc", in))
        {
            void* compressed = NULL;
            int size = in.getSize();
            compressed = QiAlloc(size);
            in.readBuffer(compressed, size);
            tex->init(16,16, GL_RGB, false);
            tex->uploadPVRTC(compressed, size);
            tex->releaseMemory();
            tex->markRetina();
            QiFree(compressed);
            loaded = true;
        }
        if (!loaded && load(retinaPath + ".mtx", in))
        {
            int size = in.getSize();
			QiMtxDecoder dec(in, size);
			tex->init(dec.getWidth(), dec.getHeight(), GL_RGBA);
			dec.decode((unsigned char*)tex->getData());
			tex->upload();
            tex->releaseMemory();
            tex->markRetina();
			loaded = true;
		}
        if (!loaded && load(retinaPath, in))
        {
            int size = in.getSize();
            if (QiString(path).contains(".png"))
            {
                QiPngDecoder dec(in, size);
                tex->init(dec.getWidth(), dec.getHeight(), GL_RGBA);
                {
                    PROFILE_ZONE("Decode png")
                    dec.decode((unsigned char*)tex->getData(), false);
                }
                {
                    PROFILE_ZONE("Upload png")
                    tex->upload();
                }
            }
            else
            {
                QiJpegDecoder dec(in, size);
                tex->init(dec.getWidth(), dec.getHeight(), GL_RGB);
                {
                    PROFILE_ZONE("Decode jpg")
                    dec.decode((unsigned char*)tex->getData(), false);
                }
                {
                    PROFILE_ZONE("Upload jpg")
                    tex->upload();
                }
            }
            tex->releaseMemory();
            tex->markRetina();
            loaded = true;
        }
    }
    
    if (!loaded && load(path + ".pvrtc", in))
	{
		void* compressed = NULL;
		int size = in.getSize();
		compressed = QiAlloc(size);
		in.readBuffer(compressed, size);
        tex->init(16,16, GL_RGB, false);
        tex->uploadPVRTC(compressed, size);
		tex->releaseMemory();
		QiFree(compressed);
        loaded = true;
	}
#endif
    
    if (!loaded && load(path + ".mtx", in))
    {
        int size = in.getSize();
		QiMtxDecoder dec(in, size);
		tex->init(dec.getWidth(), dec.getHeight(), GL_RGBA);
		dec.decode((unsigned char*)tex->getData());
		tex->upload();
        tex->releaseMemory();
		loaded = true;
	}
	if (!loaded && load(path, in))
	{
		int size = in.getSize();
		if (QiString(path).contains(".png"))
		{
			QiPngDecoder dec(in, size);
			tex->init(dec.getWidth(), dec.getHeight(), GL_RGBA);
			{
				PROFILE_ZONE("Decode png")
				dec.decode((unsigned char*)tex->getData(), false);
			}
			{
				PROFILE_ZONE("Upload png")
				tex->upload();
			}
		}
		else
		{
			QiJpegDecoder dec(in, size);
			tex->init(dec.getWidth(), dec.getHeight(), GL_RGB);
			{
				PROFILE_ZONE("Decode jpg")
				dec.decode((unsigned char*)tex->getData(), false);
			}
			{
				PROFILE_ZONE("Upload jpg")
				tex->upload();
			}
		}
		tex->releaseMemory();
        loaded = true;
	}

    if (!loaded)
	{
		tex->init(32,32,GL_RGBA);
		tex->loadDefault();
	}
	
	mResources.put(Res(path, tex));
	return Resource(this, path, tex, Resource::TEXTURE);
}


Resource ResMan::acquireSound(const QiString& path)
{
	PROFILE_ZONE("ResMan::acquireSound");
	Res* res = mResources.getPointer(Res(path));
	if (res)
	{
		res->mRefCount++;
		return Resource(this, path, res->mPointer, Resource::SOUND);
	}

	QiAudioBuffer* sound = NULL;
	QiMemoryStream<256> in;
	if (load(path, in))
	{
		if (path.contains(".wav"))
		{
			QiWavDecoder dec;
			if (dec.init(in, in.getSize()))
			{
				QiMemoryStream<8> mem;
				dec.decodeAll(mem);
				if (dec.getChannelCount() > 1)
					gGame->logW("Multi-channel sound: " + path);
				sound = gGame->mAudio->mEngine.createBuffer((short*)mem.getData(), mem.getSize(), dec.getFrequency(), dec.getChannelCount(), dec.getBitsPerSample());
			}
			else
			{
				gGame->logE("Couldn't decode WAV sound " + path);
				return Resource();
			}
		}
		else if (path.contains(".ogg"))
		{
			QiVorbisDecoder dec;
			if (dec.init(in, in.getSize()))
			{
				QiMemoryStream<8> mem;
				dec.decodeAll(mem);
				if (mem.getSize() == 0)
					return Resource();
				if (dec.getChannelCount() > 1)
					gGame->logW("Multi-channel sound: " + path);
				sound = gGame->mAudio->mEngine.createBuffer((short*)mem.getData(), mem.getSize(), dec.getFrequency(), dec.getChannelCount(), dec.getBitsPerSample());
			}
			else
			{
				gGame->logE("Couldn't decode Vorbis sound " + path);
				return Resource();
			}
		}
		else
		{
			gGame->logE("Unknown sound format: " + path);
			return Resource();
		}
	}
	else
	{
		//gGame->logE("Couldn't find sound " + path);
		return Resource();
	}

	if (sound)
		mResources.put(Res(path, sound));

	return Resource(this, path, sound, Resource::SOUND);
}


Resource ResMan::acquireShader(const QiString& path)
{
	PROFILE_ZONE("ResMan::acquireShader");
	Res* res = mResources.getPointer(Res(path));
	if (res)
	{
		res->mRefCount++;
		return Resource(this, path, res->mPointer, Resource::SHADER);
	}

	QiMemoryStream<256> tmp;
	if (load(path, tmp))
	{
		QiShader* shader = QI_NEW QiShader();
		if (shader->loadMemory(tmp.getData(), tmp.getSize(), tmp.getData(), tmp.getSize()))
		{
			mResources.put(Res(path, shader));
			return Resource(this, path, shader, Resource::SHADER);
		}
		else
			QI_DELETE(shader);
	}
	return Resource();
}


void ResMan::Res::release()
{
	switch(mType)
	{
	case Resource::TEXTURE:
		QI_DELETE((QiTexture*)mPointer);
		break;
	case Resource::SOUND:
		gGame->mAudio->mEngine.destroyBuffer((QiAudioBuffer*)mPointer);
		break;
	case Resource::SHADER:
		QI_DELETE((QiShader*)mPointer);
		break;
	default:
		QI_ASSERT(false, "Unkonwn resource type!");
	}
}


void ResMan::decRef(Res* res)
{
	res->mRefCount--;
	if (res->mRefCount == 0 && !mPreventUnload)
	{
		res->release();
		mResources.remove(*res);
	}
}


Resource::Resource() : mResMan(NULL), mPointer(NULL), mType(UNKNOWN)
{
}


Resource::Resource(ResMan* resMan, const QiString& path, void* pointer, Type type) : 
mResMan(resMan), mPath(path), mPointer(pointer), mType(type)
{
}


Resource::Resource(const Resource& other)
{
	*this = other;
}


Resource::~Resource()
{
	release();
}


void Resource::release()
{
	if (mPath != "")
	{
		ResMan::Res* res = mResMan->mResources.getPointer(ResMan::Res(mPath));
		if (res)
			mResMan->decRef(res);			
	}

	mResMan = NULL;
	mType = UNKNOWN;
	mPath = "";
	mPointer = NULL;
}


void Resource::operator=(const Resource& other)
{
	mResMan = other.mResMan;
	mPointer = other.mPointer;
	mType = other.mType;

	if (mPath == other.mPath)
		return;

	if (mPath != "")
	{
		ResMan::Res* res = mResMan->mResources.getPointer(ResMan::Res(mPath));
		if (res)
			mResMan->decRef(res);
	}

	mPath = other.mPath;

	ResMan::Res* res = mResMan->mResources.getPointer(ResMan::Res(mPath));
	if (res)
		res->mRefCount++;
}


QiTexture* Resource::getTexture()
{
	if (mType == Resource::TEXTURE)
		return (QiTexture*)mPointer;
	else
		return NULL;
}


QiAudioBuffer* Resource::getSound()
{
	if (mType == Resource::SOUND)
		return (QiAudioBuffer*)mPointer;
	else
		return NULL;
}


QiShader* Resource::getShader()
{
	if (mType == Resource::SHADER)
		return (QiShader*)mPointer;
	else
		return NULL;
}


void ResMan::debugPrint()
{
	QiHashTable<Res>::Iterator it = mResources.getIterator();
	int gfxMem = 0;
	int sndMem = 0;
	while(it.hasNext())
	{
		Res& res = it.next();
		QiString type = "unkonwn";
		QiString info = "";
		if (res.mType == Resource::TEXTURE)
		{
			type = "TEXTURE";
			if (res.mPointer)
			{
				QiTexture* t = (QiTexture*)res.mPointer;
				info = QiString() + t->getWidth() + QiString("x") + t->getHeight() + ": " + t->getMemSize() + " bytes";
				gfxMem += t->getMemSize();
			}
		}
		else if (res.mType == Resource::SOUND)
		{
			type = "SOUND";
			if (res.mPointer)
			{
				QiAudioBuffer* b = (QiAudioBuffer*)res.mPointer;
				info = QiString() + b->getSize() + QiString(" bytes");
				sndMem += b->getSize();
			}
		}
		else if (res.mType == Resource::SHADER)
		{
			type = "SHADER";
		}
		QI_PRINT(type + ": " +  res.mRequestPath + " " + info);
	}
	QI_PRINT("SUMMARY --------------------------------");
	QI_PRINT("Texture :" + (gfxMem/1024) + " kb");
	QI_PRINT("Sound :" + (sndMem/1024) + " kb");
}


bool ResMan::httpPost(const QiString& url, const void* data, int size)
{
	QiHttpRequest http;
	QiMemoryStream<8> out;
	bool p = http.post(url, data, size, out);
	if (p)
	{
		QI_PRINT("Posted " + size + " bytes to " + url);
		return true;
	}
	else
		return false;
}


bool ResMan::connectAssetServer(const QiString& host, float timeOut)
{
	sAssetSocket = QI_NEW QiTcpSocket();
	if (!sAssetSocket->connect(QiInetAddress(host, 24555), timeOut))
	{
		disconnectAssetServer();
		return false;
	}
	
	int cookie = 0;
	if (!sAssetSocket->readInt32(cookie) || cookie != 0xfa1afe1)
	{
		disconnectAssetServer();
		return false;
	}
	
	return true;
}


void ResMan::disconnectAssetServer()
{
	if (sAssetSocket)
	{
		sAssetSocket->close();
		QI_DELETE(sAssetSocket);
        sAssetSocket = NULL;
	}
}


bool ResMan::getFileFromAssetServer(const QiString& path, QiOutputStream& stream)
{
	if (!sAssetSocket)
		return false;

	if (!sAssetSocket->isOpen())
		return false;

	sAssetSocket->writeString(path);
	sAssetSocket->flush();

	bool exists = false;
	if (!sAssetSocket->readBool(exists))
		return false;

	if (exists)
	{
		QiInt32 size = 0;
		if (!sAssetSocket->readInt32(size))
			return false;

		return stream.writeBuffer(*sAssetSocket, size);
	}
	return false;
}


