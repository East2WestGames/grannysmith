/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "replay.h"
#include "dude.h"

void Replay::load(QiInputStream& motion)
{
	mFrames.clear();

	while(motion.hasMoreData())
	{
		Frame frame;

		QiUInt32 flags = 0;
		if (!motion.readUInt32(flags))
			break;
		bool correction = ((flags & 0x10000) != 0);
			motion.readUInt16(frame.hints);
		if (correction)
		{
			motion.readFloat32(frame.pos.x);
			motion.readFloat32(frame.pos.y);
			motion.readFloat32(frame.ang);
			motion.readFloat32(frame.vel.x);
			motion.readFloat32(frame.vel.y);
			motion.readFloat32(frame.angVel);
		}

		mFrames.add(frame);
	}

	if (mFrames.getCount() == 0)
		return;

	//Interpolate in between values
	int last = 0;
	for(int i=1; i<mFrames.getCount(); i++)
	{
		while(i < mFrames.getCount() && lengthSquared(mFrames[i].pos) == 0.0f)
			i++;
		if (i==mFrames.getCount())
		{
			mFrames[i-1] = mFrames[last];
			i--;
		}
		for(int j=last+1; j<i; j++)
		{
			float t = (j-last) / (float)(i-last);
			mFrames[j].pos.x = QiInterpolateLinear(mFrames[last].pos.x, mFrames[i].pos.x, t);
			mFrames[j].pos.y = QiInterpolateLinear(mFrames[last].pos.y, mFrames[i].pos.y, t);
			mFrames[j].ang = QiInterpolateLinear(mFrames[last].ang, mFrames[i].ang, t);
			mFrames[j].angVel = QiInterpolateLinear(mFrames[last].angVel, mFrames[i].angVel, t);
			mFrames[j].vel.x = QiInterpolateLinear(mFrames[last].vel.x, mFrames[i].vel.x, t);
			mFrames[j].vel.y = QiInterpolateLinear(mFrames[last].vel.y, mFrames[i].vel.y, t);
		}
		last = i;
	}

	//Select camera

	//First look for die
	for(int i=0; i<mFrames.getCount(); i++)
	{
		if ((mFrames[i].hints & HINT_DIE))
		{
			int mi = QiMax(i-10, 0);
			int ma = QiMin(i+40, mFrames.getCount());
			for(int j=mi; j<ma; j++)
			{
				mFrames[j].camera = 8;
				mFrames[j].timeScale = 0.3f;
			}

			//Don't look for new breakables for a while
			i+= 250;
		}
	}

	//Look for breakables
	for(int i=0; i<mFrames.getCount(); i++)
	{
		if ((mFrames[i].hints & HINT_BREAK))
		{
			int mi = QiMax(i-10, 0);
			int ma = QiMin(i+40, mFrames.getCount());
			int c = QiRnd(6, 8);
			float ts = (QiRnd(0,2)==0 ? 0.2f : 0.4f);
			for(int j=mi; j<ma; j++)
			{
				mFrames[j].camera = c;
				mFrames[j].cameraPos = mFrames[i].pos.vec3() + QiVec3(6, 3, 16);
				mFrames[j].timeScale = ts;
			}

			//Don't look for new breakables for a while
			i+= 250;
		}
	}

#if 0
	//Win
	for(int i=0; i<mFrames.getCount(); i++)
	{
		if ((mFrames[i].hints & HINT_WIN))
		{
			int mi = QiMax(i-30, 0);
			int ma = QiMin(i+30, mFrames.getCount());
			QiVec3 p(mFrames[i].pos.x, mFrames[i].pos.y, 20);
			for(int j=mi; j<ma; j++)
			{
				mFrames[j].camera = 1;
				mFrames[j].cameraPos = p;
				mFrames[j].timeScale = 0.5f;
			}
		}
	}

	//Apple
	for(int i=0; i<mFrames.getCount(); i++)
	{
		if ((mFrames[i].hints & HINT_WIN))
		{
			int mi = QiMax(i-20, 0);
			int ma = mFrames.getCount();
			QiVec3 p(mFrames[i].pos.x, mFrames[i].pos.y, 20);
			int c = QiRnd(7,9);
			for(int j=mi; j<ma; j++)
			{
				mFrames[j].camera = c;
				mFrames[j].cameraPos = p;
				mFrames[j].timeScale = 0.5f;
			}
		}
	}

	//Look for handles
	for(int i=0; i<mFrames.getCount(); i++)
	{
		if ((mFrames[i].hints & HINT_HANDLE))
		{
			int mi =i;
			int ma;
			for(ma=i+1; ma<mFrames.getCount(); ma++)
				if ((mFrames[ma].hints & HINT_RELEASE))
					break;
			QiVec2 p2;
			for(int j=mi; j<ma; j++)
				p2 += mFrames[j].pos;
			p2 /= (float)(ma-mi);
			QiVec3 p(p2.x, p2.y, 12.0f);
			mi = QiMax(mi-30, 0);
			ma = QiMin(ma+30, mFrames.getCount());
			int c = QiRnd(7, 10);
			float s = (QiRnd(0.0f, 1.0f) > 0.5f ? 1.0f : 0.5f);
			for(int j=mi; j<ma; j++)
			{
				mFrames[j].camera = c;
				mFrames[j].cameraPos = p;
				mFrames[j].timeScale = s;
			}
		}
	}
#endif


	//Look for jumps
	for(int i=0; i<mFrames.getCount(); i++)
	{
		if ((mFrames[i].hints & HINT_JUMP))
		{
			int mi = i;
			int ma;
			for(ma=i+1; ma<mFrames.getCount(); ma++)
				if ((mFrames[ma].hints & HINT_LAND) || (mFrames[ma].hints & HINT_WIRE) || (mFrames[ma].hints & HINT_RAIL))
					break;

			bool ok = true;

			//Bad landing doesn't count
			if (ma<mFrames.getCount() && (mFrames[ma].hints & HINT_LAND) && !(mFrames[ma].hints & HINT_LAND_GOOD))
				ok = false;

			for(int j=mi; j<ma; j++)
			{
				if (mFrames[j].camera != -1)
				{
					ok = false;
					break;
				}
			}
			if (ok && ma-mi > 100)
			{
				QiVec3 p0(mFrames[mi].pos.x, mFrames[mi].pos.y+4, 20);
				QiVec3 p1(mFrames[ma-1].pos.x, mFrames[ma-1].pos.y+4, 20);
				int c;
				if (ma-mi < 150)
					c = QiRnd(1,6);
				else
					c = QiRnd(1,5);
				for(int j=mi; j<ma; j++)
				{
					mFrames[j].camera = c;
					mFrames[j].cameraPos = (p0+p1)*0.5f;
					mFrames[j].timeScale = 0.4f;
				}
			}
			i = ma + 200;
		}
	}

	//Fill in holes
	for(int i=0; i<mFrames.getCount(); i++)
	{
		if (mFrames[i].camera == -1)
		{
			int mi = i;
			int ma;
			for(ma=i+1; ma<mFrames.getCount(); ma++)
				if (mFrames[ma].camera != -1)
					break;

			int clipLength = 200;
			int t = QiMax(1, (ma-mi) / clipLength);
			int c = 0;
			int i0 = mi;
			for(int i=0; i<t; i++)
			{
				int i1 = mi + ((ma-mi)*(i+1))/t;

				int newC;
				do
				{
					newC = QiRnd(1,6);
					if (newC == c)
						newC = 0;
					if ((c == 1 || c == 2) && (newC == 3 || newC == 4))
						newC = 0;
					if (c == 3 && newC == 4)
						newC = 0;
					if (c == 4 && newC == 3)
						newC = 0;
					if (newC == 5 && ma-mi > 150 && i==t-1)
						newC = 0;
				} while(!newC);
				c = newC;

				if (c == 5 && i1 > i0+150)
					i1 = i0+150;

				QiVec3 p0(mFrames[i0].pos.x, mFrames[i0].pos.y+2, 20);
				QiVec3 p1(mFrames[i1-1].pos.x, mFrames[i1-1].pos.y+2, 20);

				for(int j=i0; j<i1; j++)
				{
					mFrames[j].camera = c;
					mFrames[j].cameraPos = (p0+p1)*0.5f;
				}

				i0 = i1;
			}
		}
	}

#if 0
	//Remove small gaps (there shouldn't be any)
	for(int i=0; i<mFrames.getCount(); i++)
	{
		int mi = i;
		int ma;
		for(ma=i+1; ma<mFrames.getCount(); ma++)
			if (mFrames[ma].camera != -1)
				break;

		//Close gaps that are too small
		if (ma-mi < 60 && mi > 0)
		{
			for(int j=mi; j<ma; j++)
			{
				mFrames[j].camera = mFrames[mi-1].camera;
				mFrames[j].cameraPos = mFrames[mi-1].cameraPos;
				mFrames[j].timeScale = mFrames[mi-1].timeScale;
			}
			continue;
		}
	}
#endif
}



