/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "propertybag.h"
#include "snd/qiaudio.h"
#include "base/qithread.h"
#include "base/qisync.h"
#include "resman.h"

class SoundBank
{
public:
	SoundBank() : mTimeInBetween(0.5f), mLast(0) {}
	SoundBank(ResMan* resMan, const QiString& path) : mTimeInBetween(0.5f), mLast(0) { load(resMan, path); }

	void load(ResMan* resMan, const QiString& path);
	void release();

	QiAudioBuffer* next();

	float mTimeInBetween;

protected:
	QiArrayInplace<Resource, 8> mSounds;
	int mLast;
	QiTimer mTimer;
};

class Audio : public QiThread
{
public:
	Audio();
	~Audio();

	void load();
	void unload();

	void update();
	virtual void run();  //Thread method

	void playBackgroundMusic(const QiString& path, bool force=false);
	void stopBackgroundMusic();

	void playForegroundMusic(const QiString& path);
	void stopForegroundMusic();

	QiAudioChannel* acquireChannel();
	void releaseChannel(QiAudioChannel* channel);

	void playSound(const QiAudioBuffer* buffer, float volume=1.0f, float pitch=1.0f);
	void playSound(const QiAudioBuffer* buffer, const QiVec2& pos, float volume=1.0f, float pitch=1.0f);
	void setSoundChannelVolume(QiAudioChannel* channel, float volume);
	void setSoundChannelVolume(QiAudioChannel* channel, const QiVec2& pos, float volume);

	void attach();
	void detach();

	bool isMusicEnabled();
	bool isSoundEnabled();

	PropertyBag mProperties;
	QiAudio mEngine;

	QiString mBackgroundMusicPath;
	QiString mForegroundMusicPath;
	QiAudioChannel* mBackgroundMusicChannel;
	QiAudioChannel* mForegroundMusicChannel;
	QiAudioStream* mBackgroundMusicStream;
	QiAudioBuffer* mBackgroundMusicBuffer;
	QiAudioStream* mForegroundMusicStream;
	QiAudioBuffer* mForegroundMusicBuffer;

	SoundBank mClickDown;
	SoundBank mClickUp;
	SoundBank mBreakBrick;
	SoundBank mBreakGlass;
	SoundBank mBreakWood;
	SoundBank mBreakMetal;
	SoundBank mBreakBranch;
	SoundBank mCaneHit;
	SoundBank mCaneSwoosh;
	SoundBank mCaneSpeed;
	SoundBank mSkatesLand;
	SoundBank mSkatesLeave;
	SoundBank mObjectHit;
	SoundBank mRotate;
	SoundBank mCoin;

	QiMutex mStreamMutex;

	bool mSoundEnabled;
	bool mMusicEnabled;
	bool mForceBackground;
};

