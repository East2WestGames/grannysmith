/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "app/qiinput.h"
#include "display.h"
#include "player.h"
#include "scene.h"
#include "device.h"
extern bool gCaptureMode;


static void mgCreateImage(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	QiString path = args.asString(0);
	Script::Image* image = QI_NEW Script::Image(s);
	image->mResource = s->mResMan->acquireTexture(path);
	image->mTexture = image->mResource.getTexture();
	if (args.getCount() > 1)
	{
		float w = (float)image->mTexture->getWidth();
		float h = (float)image->mTexture->getHeight();
		if (w > 0 && h > 0)
		{
			image->mTexLow.set(args.asFloat(1)/w, args.asFloat(2)/h);
			image->mTexHigh.set(args.asFloat(3)/w, args.asFloat(4)/h);
		}
	}
	image->mDesc = QiString("Image ") + image->mTexture->getWidth() + QiString("x") + image->mTexture->getHeight() + QiString(": ") + QiPath::getFilePart(path);
	int index = 0;
	s->mObjects.add(image, &index);
	ret.addInt(index);
}


static void mgCreateUi(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	QiString path = args.asString(0);
	Script::Ui* ui = QI_NEW Script::Ui(s);
	
	QiMemoryStream<256> tmp;
	if (s->mResMan->load(path, tmp))
		ui->mGuiBox.init(tmp, tmp.getSize(), s->mResMan);

	ui->mDesc = QiString("UI: ") + QiPath::getFilePart(path);

	int index = 0;
	s->mObjects.add(ui, &index);
	ret.addInt(index);
}


static void mgCreateText(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	QiString name = args.asString(0);
	bool numbers = false; 
	if (args.getCount() > 1)
		numbers = args.asBool(1);
	Script::Text* text = QI_NEW Script::Text(s, name, numbers);
	text->mDesc = QiString("Font: ") + name;

	int index = 0;
	s->mObjects.add(text, &index);
	ret.addInt(index);
}


static void mgSetPos(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Visual* visual = s->getVisual(args.asInt(0));
	if (!visual)
		return;

	visual->mPos.set(QiVec2(args.asFloat(1), args.asFloat(2)), args.asString(3), args.asFloat(4));
}


static void mgSetRot(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Visual* visual = s->getVisual(args.asInt(0));
	if (!visual)
		return;

	visual->mRot.set(args.asFloat(1), args.asString(2), args.asFloat(3));
}


static void mgSetScale(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Visual* visual = s->getVisual(args.asInt(0));
	if (!visual)
		return;

	float x = args.asFloat(1);
	float y = args.getCount() > 2 ? args.asFloat(2) : x;

	visual->mScale.set(QiVec2(x, y), args.asString(3), args.asFloat(4));
}


static void mgSetAlpha(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Visual* visual = s->getVisual(args.asInt(0));
	if (!visual)
		return;

	visual->mAlpha.set(args.asFloat(1), args.asString(2), args.asFloat(3));
}


static void mgSetColor(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Visual* visual = s->getVisual(args.asInt(0));
	if (!visual)
		return;

	visual->mColor.set(QiVec3(args.asFloat(1), args.asFloat(2), args.asFloat(3)), args.asString(4), args.asFloat(5));
}


static void mgSetOrigo(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Visual* visual = s->getVisual(args.asInt(0));
	if (!visual)
		return;
	QiString origo = args.asString(1);
	if (origo == "center")
		visual->mOrigo.set(visual->getWidth()*0.5f, visual->getHeight()*0.5f);
	else if (origo == "topleft")
		visual->mOrigo.set(0, 0);
	else if (origo == "topright")
		visual->mOrigo.set((float)visual->getWidth(), 0);
	else if (origo == "bottomleft")
		visual->mOrigo.set(0.0f, (float)visual->getHeight());
	else if (origo == "bottomright")
		visual->mOrigo.set((float)visual->getWidth(), (float)visual->getHeight());
	else if (origo == "pixel")
		visual->mOrigo.set(args.asFloat(2), args.asFloat(3));
}


static void mgSetCrop(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Image* image = s->getImage(args.asInt(0));
	if (!image)
		return;
	if (args.getCount() > 1)
	{
		float w = (float)image->mTexture->getWidth();
		float h = (float)image->mTexture->getHeight();
		if (w > 0 && h > 0)
		{
			image->mTexLow.set(args.asFloat(1)/w, args.asFloat(2)/h);
			image->mTexHigh.set(args.asFloat(3)/w, args.asFloat(4)/h);
		}
	}	
}

static void mgGetPos(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Visual* visual = s->getVisual(args.asInt(0));
	if (!visual)
		return;

	QiVec2 pos = visual->mPos.get();
	ret.addFloat(pos.x);
	ret.addFloat(pos.y);
}


static void mgGetRot(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Visual* visual = s->getVisual(args.asInt(0));
	if (!visual)
		return;

	ret.addFloat(visual->mRot.get());
}


static void mgGetScale(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Visual* visual = s->getVisual(args.asInt(0));
	if (!visual)
		return;

	QiVec2 scale = visual->mScale.get();
	ret.addFloat(scale.x);
	ret.addFloat(scale.y);
}


static void mgGetAlpha(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Visual* visual = s->getVisual(args.asInt(0));
	if (!visual)
		return;

	ret.addFloat(visual->mAlpha.get());
}


static void mgGetColor(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Visual* visual = s->getVisual(args.asInt(0));
	if (!visual)
		return;

	QiVec3 col = visual->mColor.get();
	ret.addFloat(col.x);
	ret.addFloat(col.y);
	ret.addFloat(col.y);
}



static void mgIsVisible(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Visual* visual = s->getVisual(args.asInt(0));
	if (!visual)
		return;

	ret.addBool(visual->isVisible());
}


static void mgDraw(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Visual* visual = s->getVisual(args.asInt(0));
	if (!visual)
		return;

	if (visual->isVisible())
		visual->draw();
}


static void mgGet(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	QiString obj = args.asString(0);
	QiString result = gGame->getProperty(args.asString(0));
	ret.addString(result);
}


static void mgSet(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	gGame->setProperty(args.asString(0), args.asString(1));
}


static void mgCommand(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	ret.addString(gGame->handleCommand(args.asString(0)));
}


static void mgRadioSelect(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Ui* ui = s->getUi(args.asInt(0));
	if (!ui)
		return;
	ui->mGuiBox.selectRadio(args.asString(1));
}


static void mgSetUiEnabled(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Ui* ui = s->getUi(args.asInt(0));
	if (!ui)
		return;
	ui->mGuiBox.setEnabled(args.asBool(1));
}


static void mgSetUiModal(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Ui* ui = s->getUi(args.asInt(0));
	if (!ui)
		return;
	ui->mGuiBox.setModal(args.asBool(1));
}


static void mgScriptTime(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	if (gCaptureMode)
		ret.addFloat(s->mSimTime);
	else
		ret.addFloat(s->mTimer.getTime());
}


static void mgScriptFrame(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	ret.addInt(s->mFrame);
}


static void mgFullScreenColor(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	float r = args.asFloat(0);
	float g = args.asFloat(1);
	float b = args.asFloat(2);
	float a = args.asFloat(3);
	if (a > 0.0f)
		gGame->mGfx->drawFullScreenSolid(QiColor(r,g,b,a));
}


static void mgCreateSound(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	QiString path = args.asString(0);
	Script::Sound* sound = QI_NEW Script::Sound(s);
	
	sound->mResource = s->mResMan->acquireSound(path);
	sound->mDelay = args.asFloat(1);

	sound->mDesc = QiString("Sound: ") + QiPath::getFilePart(path);

	int index = 0;
	s->mObjects.add(sound, &index);
	ret.addInt(index);
}


static void mgPlaySound(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Sound* sound = s->getSound(args.asInt(0));
	if (!sound)
		return;

	float volume = QiClamp(args.getCount() > 1 ? args.asFloat(1) : 1.0f, 0.0f, 1.0f);
	float pitch = QiClamp(args.getCount() > 2 ? args.asFloat(2) : 1.0f, 0.01f, 100.0f);

	if (sound->mTimer.getTime() > sound->mDelay || sound->mFirst)
	{
		if (sound->mResource.getSound())
			gGame->mAudio->playSound(sound->mResource.getSound(), volume, pitch);

		sound->mFirst = false;
		sound->mTimer.reset();

		if (sound->mDelay < 0.0f)
			sound->mDelay = QI_FLOAT_MAX;
	}
}


static void mgSetText(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Text* text = s->getText(args.asInt(0));
	QiString str = args.asString(1);
	if (!text)
		return;
	text->mFont.setText(str);
}


inline int getTouch2(const QiVec2& point, float radius)
{
	for(int i=0; i<QI_MAX_TOUCH; i++)
	{
		if (gGame->mInput->hasTouch(i) || gGame->mInput->wasTouchReleased(i))
		{
			QiVec2 t = gGame->mInput->getTouchPos(i);
			t.x = 1024.f * t.x/(float)gGame->mDisplay->mWidth;
			t.y = 768.f * t.y/(float)gGame->mDisplay->mHeight;
			if (lengthSquared(t-point) < radius*radius)
				return i;
		}
	}
	return -1;
}


static void mgIsTouched(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	float x = args.asFloat(0);
	float y = args.asFloat(1);
	float r = args.asFloat(2);
	int m = args.asInt(3);
	int t = getTouch2(QiVec2(x, y), r);
	bool q = (t != -1);
	if (q)
	{
		if (m == 1 && !gGame->mInput->wasTouchPressed(t))
			q = false;
		if (m == 2 && !gGame->mInput->wasTouchReleased(t))
			q = false;
	}
	ret.addBool(q);
}

static void mgIsAdRemoved(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	ret.addBool(!gGame->mDevice->showAds());
}


static void mgGetLastTouch(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	ret.addFloat(s->mLastTouch.x);
	ret.addFloat(s->mLastTouch.y);
}



static void mgGetScreenCoord(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	float x = args.asFloat(0);
	float y = args.asFloat(1);
	gGame->mDisplay->enterLevel();
	QiVec2 p = gGame->mDisplay->worldToPixel(QiVec2(x, y));
	gGame->mDisplay->leaveLevel();
	p = gGame->mDisplay->pixelToWorld(QiVec2(p.x, p.y));
	ret.addFloat(p.x);
	ret.addFloat(p.y);
}


static void mgCreateCanvas(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Canvas* frame = QI_NEW Script::Canvas(s, args.asFloat(0), args.asFloat(1));
	frame->mDesc = QiString("Frame");
	int index = 0;
	s->mObjects.add(frame, &index);
	ret.addInt(index);
}


static void mgPushCanvas(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Canvas* canvas= s->getCanvas(args.asInt(0));
	if (canvas)
		s->pushCanvas(canvas);
}


static void mgPopCanvas(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	s->popCanvas();
}


static void mgSetCanvasEnabled(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Canvas* canvas = s->getCanvas(args.asInt(0));
	if (canvas)
		canvas->mEnabled = args.asBool(1);
}


static void mgSetCanvasWindow(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Canvas* canvas = s->getCanvas(args.asInt(0));
	if (canvas)
	{
		canvas->mWindow[0] = args.asInt(1);
		canvas->mWindow[1] = args.asInt(2);
		canvas->mWindow[2] = args.asInt(3);
		canvas->mWindow[3] = args.asInt(4);
		canvas->mUseWindow = (canvas->mWindow[0] != 0 || canvas->mWindow[1] != 0 || canvas->mWindow[2] != 0 || canvas->mWindow[3] != 0);
	}
}


static void mgSetCanvasMovable(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Canvas* canvas = s->getCanvas(args.asInt(0));
	if (canvas)
	{
		QiString m = args.asString(1);
		if (m=="x")
			canvas->mMovable = 1;
		else if (m=="y")
			canvas->mMovable = 2;
		else
			canvas->mMovable = 3;
	}
}


static void mgGetUiSelectionCount(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Ui* ui = s->getUi(args.asInt(0));
	ret.addInt(ui->mGuiBox.mSelections.getCount());
}


static void mgGetUiSelectionInfo(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	Script* s = (Script*)script->userData;
	Script::Ui* ui = s->getUi(args.asInt(0));
	int i = args.asInt(1);
	ret.addString(ui->mGuiBox.mSelections[i].command);
	ret.addInt((int)ui->mGuiBox.mSelections[i].lower.x);
	ret.addInt((int)ui->mGuiBox.mSelections[i].lower.y);
	ret.addInt((int)ui->mGuiBox.mSelections[i].upper.x);
	ret.addInt((int)ui->mGuiBox.mSelections[i].upper.y);
}


static void mgGetLevelInfo(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	QiString level  = args.asString(0);
	ret.addBool(gGame->mPlayer->isLevelAvailable(level));
	ret.addInt(gGame->mPlayer->getScore(level));
	ret.addBool(gGame->mPlayer->isSkipped(level));
	ret.addInt(gGame->mPlayer->getApples(level));
	ret.addString(gGame->mPlayer->getLevelTitle(level));
}


static void mgIsCharacterAvailable(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	QiString c  = args.asString(0);
	ret.addBool(gGame->mPlayer->isCharacterAvailable(c));
}


static void mgDrawReplay(QiScript* script, const QiScriptArgs& args, QiScriptRet& ret)
{
	float x0 = args.asFloat(0);
	float y0 = args.asFloat(1);
	float x1 = args.asFloat(2);
	float y1 = args.asFloat(3);

	QiMatrix4 mat;
	mat.m[12] = x0;
	mat.m[13] = y1;
	mat.m[0] = x1-x0;
	mat.m[5] = y0-y1;

	gGame->drawFbo(gGame->mMenuScene->mScript.mCanvasMatrix * mat);
	gGame->mTvDrawn = true;	
}


