/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "transition.h"
#include "guibox.h"
#include "resman.h"
#include "font.h"

#include "qi_base.h"
#include "qi_math.h"
#include "qi_script.h"

class Script
{
public:
	class Object
	{
	public:
		enum Type
		{
			UNKNOWN,
			IMAGE,
			UI,
			TEXT,
			SOUND,
			CANVAS,
			EFFECT
		};

		Object(Script* script, Type type) : mScript(script), mType(type) {}
		virtual ~Object() {}
		virtual bool isVisual() const { return false; }
		Script* mScript;
		Type mType;
		QiString mDesc;
	};

	class Visual : public Object
	{
	public:
		Visual(Script* script, Type type) : Object(script, type), mRot(0.0f), mScale(QiVec2(1.0f, 1.0f)), mAlpha(1.0f), mColor(QiVec3(1.0f, 1.0f, 1.0f)) {}
		virtual bool isVisual() const { return true; }
		virtual float getWidth() const = 0;
		virtual float getHeight() const = 0;
		virtual bool isVisible() const;
		virtual void draw() = 0;
		QiMatrix4 getTransform() const;
		QiColor getColor() const;
		QiVec2 mOrigo;
		Transition<QiVec2> mPos;
		Transition<float> mRot;
		Transition<QiVec2> mScale;
		Transition<float> mAlpha;
		Transition<QiVec3> mColor;
	};

	class Image : public Visual
	{
	public:
		Image(Script* script) : Visual(script, IMAGE), mTexHigh(1.0f, 1.0f) { }
		virtual float getWidth() const;
		virtual float getHeight() const;
		virtual void draw();
		Resource mResource;
		class QiTexture* mTexture;
		QiVec2 mTexLow;
		QiVec2 mTexHigh;
	};

	class Ui : public Visual
	{
	public:
		Ui(Script* script) : Visual(script, UI) { }
		virtual float getWidth() const;
		virtual float getHeight() const;
		virtual void draw();
		GuiBox mGuiBox;
	};

	class Text : public Visual
	{
	public:
		Text(Script* script, const char* name, bool numbers) : Visual(script, TEXT), mFont(name, numbers) { }
		virtual float getWidth() const;
		virtual float getHeight() const;
		virtual void draw();
		Font mFont;
	};

	class Sound : public Object
	{
	public:
		Sound(Script* script) : Object(script, SOUND), mDelay(0.0f), mFirst(true) { }
		Resource mResource;
		float mDelay;
		bool mFirst;
		QiTimer mTimer;
	};

	class Canvas : public Visual
	{
	public:
		Canvas(Script* script, float w, float h) : Visual(script, CANVAS), mWidth(w), mHeight(h), mEnabled(true), mMovable(0), mUseWindow(false) { }
		virtual float getWidth() const { return mWidth; }
		virtual float getHeight() const { return mHeight; }
		virtual QiMatrix4 getTransform();
		virtual void draw() {}
		float mWidth;
		float mHeight;
		bool mEnabled;
		int mMovable;
		bool mUseWindow;
		int mWindow[4];
		QiMatrix4 mMatrix;
		QiVec2 mDragVel;
	};

	class Effect : public Object
	{
	public:
		Effect(Script* script, const QiString& name);
		~Effect();

		class ParticleSystem* mParticleSystem;
	};


public:
	Script(class ResMan* resMan);
	~Script() { unload(); }

	bool load(const class QiString& path);
	void unload();

	void tick(bool callFrameFunction=true);
	void draw();
	void drawEffects();
	void handleCommand(const QiString& cmd);

public:
	Image* getImage(int i);
	Visual* getVisual(int i);
	Ui* getUi(int i);
	Text* getText(int i);
	Sound* getSound(int i);
	Canvas* getCanvas(int i);
	Effect* getEffect(int i);

	QiScript mScript;
	QiArray<Object*> mObjects;
	class ResMan* mResMan;
	bool mHasScript;
	QiTimer mTimer;
	float mSimTime;
	int mFrame;
	float mInitTime;
	float mTickTime;
	float mDrawTime;

	void pushCanvas(Canvas* frame);
	void popCanvas();
	void updateCanvasStack();
	Canvas* mCurrentCanvas;
	QiMatrix4 mCanvasMatrix;
	QiColor mCanvasColor;
	bool mCanvasEnabled;
	QiArray<Canvas*> mCanvasStack;

	int mDragTouch;
	Canvas* mDragCanvas;
	QiVec2 mDragOrgPos;
	QiVec2 mDragPivot;

	QiVec2 mLastTouch;
};

