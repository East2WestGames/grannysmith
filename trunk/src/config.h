/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "base/qiconfig.h"

#define DEPLOY 1

//#define DATE_CHECK 2012, 8, 23
#define ENABLE_PROFILING 0

#define ENABLE_NET_STATS 1
#define ENABLE_NET_ADS 1

#if defined(QI_IOS) || defined(QI_ANDROID)
#define ENABLE_ASSET_SERVER 0
#else
#define ENABLE_ASSET_SERVER 0
#endif

#define PRODUCT_STRING "full"
#define VERSION_STRING "1.2.0"

#if defined(QI_WIN32)
#define PLATFORM_STRING "win32"
#elif defined(QI_MACOS)
#define PLATFORM_STRING "osx"
#elif defined(QI_IOS)
#define PLATFORM_STRING "ios"
#elif defined(QI_ANDROID)
#define PLATFORM_STRING "android"
#else
#define PLATFORM_STRING "unknown"
#endif

#if defined(QI_IOS) || defined(QI_ANDROID)
#define ENABLE_EDITOR 0
#else
#define ENABLE_EDITOR 1
#endif

