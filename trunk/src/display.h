/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "propertybag.h"
#include "gfx/qiviewport.h"
#include "gfx/qitextrenderer.h"
#include "transition.h"
#include "replay.h"

class Display
{
public:
	Display();

	void setGeometry(int w, int h, bool letterBox=true);

	QiVec2 pixelToWorld(const QiVec2& pixel);
	QiVec2 worldToPixel(const QiVec2& worldCoord);

	float getMaxVisibleY();
	float getMinVisibleY();

	void enterLevel();
	void leaveLevel();

	void enterFbo();
	void leaveFbo();

	void reset();
	void update();
	void postDraw();
	
	bool isVisible(const QiVec2& boundsMin, const QiVec2& boundsMax, float z);

	int mWidth, mHeight;
	QiViewport mViewport;
	PropertyBag mProperties;

	bool mPanelMode;
	float mPanelFade;

	class Debug* mDebug;
	
	float mEditZoom;
	QiVec2 mEditPan;	
	QiVec3 mCameraPos;
	QiQuat mCameraRot;
	Transition<float> mCameraFov;
	float mCameraDistance;
	float mCameraDistanceSpeed;
	float mCameraDistance2;
	float mCameraDistanceSpeed2;
	int mEdit3D;

	QiVec3 mFrustumDir0;
	QiVec3 mFrustumDir1;
	QiVec3 mFrustumDir2;
	QiVec3 mFrustumDir3;

	int mCameraSensor;
	int mCameraSensorOld;
	int mCameraSensorRot;
	int mCameraSensorRotOld;
	float mCameraSensorTime;

	int mReplayCameraMode;
	float mReplayCameraTimer;
	int mReplayCameraFrame;
	QiVec3 mReplayCameraFixedPos;

	Replay mReplay;
	Transition<float> mRotY;

	class Sensor* mCamera2Sensor;
	class Sensor* mCamera2SensorOld;
	QiVec3 mCamera2Offset;
	float mCamera2Rot;
	Transition<QiVec3> mCamera2OffsetTrans;
	Transition<float> mCamera2RotTrans;
	float mCamera2SensorTime;
	Transition<float> mCamera2SensorUsage;

	QiFbo mFbo;
	float mShakeTimer;
};

