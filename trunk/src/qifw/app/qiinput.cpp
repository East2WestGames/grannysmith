/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "app/qiinput.h"
#include "base/qistream.h"

#if  defined(QI_ANDROID)
#include <android/log.h>
#	define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, "native-activity", __VA_ARGS__))
#endif

QiInput::QiInput()
{
	mLastMouseX = 0;
	mLastMouseY = 0;
	mMouseX = 0;
	mMouseY = 0;

	clear();
	mOutputStream = NULL;
	mInputStream = NULL;
	mPlaybackScaleX = 1.0f;
	mPlaybackScaleY = 1.0f;
}


QiInput::~QiInput()
{
}


void QiInput::record(QiOutputStream& stream)
{
	stop();
	mOutputStream = &stream;
}


void QiInput::playback(QiInputStream& stream)
{
	stop();
	mInputStream = &stream;
}


void QiInput::stop()
{
	mInputStream = NULL;
	mOutputStream = NULL;
}


//Query interface
int QiInput::getMousePosX() const
{
	return mMouseX;
}


int QiInput::getMousePosY() const
{
	return mMouseY;
}


int QiInput::getMouseDiffX() const
{
	return mMouseX-mLastMouseX;
}


int QiInput::getMouseDiffY() const
{
	return mMouseY-mLastMouseY;
}


bool QiInput::wasButtonPressed(int button) const
{
#if defined(QI_ANDROID)
	LOGD("wasButtonPressed----------%d",button);
#endif
	return mButtonPressed[button];
}


bool QiInput::wasButtonReleased(int button) const
{
//#if defined(QI_ANDROID)
//	LOGD("wasButtonReleased----------%d",button);
//#endif
	return mButtonReleased[button];
}


bool QiInput::isButtonDown(int button) const
{
#if defined(QI_ANDROID)
	LOGD("isButtonDown----------%d",button);
#endif
	return mButtonDown[button];
}


bool QiInput::wasKeyPressed(int key) const
{
//#if defined(QI_ANDROID)
//	LOGD("wasKeyPressed----------%d",key);
//#endif
	if (key >=65 && key<=90)
		key+=32;
	return mKeyPressed[key];
}


bool QiInput::wasKeyReleased(int key) const
{
//#if defined(QI_ANDROID)
//	LOGD("wasKeyReleased----------%d",key);
//#endif
	if (key >=65 && key<=90)
		key+=32;
	return mKeyReleased[key];
}


bool QiInput::isKeyDown(int key) const
{
//#if defined(QI_ANDROID)
//	LOGD("isKeyDown----------%d",key);
//#endif
	if (key >=65 && key<=90)
		key+=32;
	return mKeyDown[key];
}


int QiInput::getEventCount() const
{
	return mEvents.getCount();
}


const QiInput::Event& QiInput::getEvent(int i) const
{
	return mEvents[i];
}


void QiInput::clear()
{
	for(int i=0; i<QI_MAX_KEYS; i++)
	{
		mKeyDown[i] = false;
		mKeyPressed[i] = false;
		mKeyReleased[i] = false;
	}
	for(int i=0; i<QI_MAX_BUTTONS; i++)
	{
		mButtonPressed[i] = false;
		mButtonReleased[i] = false;
		mButtonDown[i] = false;
	}
	for(int i=0; i<QI_MAX_TOUCH; i++)
	{
		mTouchId[i] = 0;
		mTouchPressed[i] = false;
		mTouchReleased[i] = false;
	}
}

void QiInput::registerBegin()
{
	mEvents.clear();
	for(int i=0; i<QI_MAX_KEYS; i++)
	{
		mKeyPressed[i] = false;
		mKeyReleased[i] = false;
	}
	for(int i=0; i<QI_MAX_BUTTONS; i++)
	{
		mButtonPressed[i] = false;
		mButtonReleased[i] = false;
	}
	for(int i=0; i<QI_MAX_TOUCH; i++)
	{
		mTouchPressed[i] = false;
		mTouchReleased[i] = false;
	}

	mLastMouseX = mMouseX;
	mLastMouseY = mMouseY;
	for(int i=0; i<QI_MAX_TOUCH; i++)
	{
		mLastTouchPos[i][0] = mTouchPos[i][0];
		mLastTouchPos[i][1] = mTouchPos[i][1];
	}

	if (mInputStream)
	{
		//Set input stream to NULL, otherwise register functions will be bypassed
		QiInputStream* tmp = mInputStream;
		mInputStream = NULL;
		int c=0;
		if (tmp->readInt32(c))
		{
			for(int i=0; i<c; i++)
			{
				bool s = true;
				QiInput::Event e;
				s &= tmp->readInt32((int&)e.type);
				s &= tmp->readInt32(e.id);
				s &= tmp->readInt32(e.x);
				s &= tmp->readInt32(e.y);
				if (s)
				{
					e.x = (int)(mPlaybackScaleX * (float)e.x);
					e.y = (int)(mPlaybackScaleY * (float)e.y);
					registerEvent(e);
				}
			}
		}
		mInputStream = tmp;
	}

}


void QiInput::registerEvent(const Event& e)
{
	if (e.type == KEY_DOWN)
		registerKeyDown(e.id);
	else if (e.type == KEY_UP)
		registerKeyUp(e.id);
	else if (e.type == MOUSE_MOVE)
		registerMousePos(e.x, e.y);
	else if (e.type == BUTTON_DOWN)
		registerButtonDown(e.id);
	else if (e.type == BUTTON_UP)
		registerButtonUp(e.id);
	else if (e.type == TOUCH_BEGIN)
		registerTouchBegin(e.id, e.x, e.y);
	else if (e.type == TOUCH_MOVE)
		registerTouchPos(e.id, e.x, e.y);
	else if (e.type == TOUCH_END)
		registerTouchEnd(e.id);
	else if (e.type == CHARACTER)
		registerCharacter(e.id);
		
}


void QiInput::registerEnd()
{
	if (mOutputStream)
	{
		mOutputStream->writeInt32(mEvents.getCount());
		for(int i=0; i<mEvents.getCount(); i++)
		{
			QiInput::Event& e=mEvents[i];
			mOutputStream->writeInt32(e.type);
			mOutputStream->writeInt32(e.id);
			mOutputStream->writeInt32(e.x);
			mOutputStream->writeInt32(e.y);
		}
		mOutputStream->flush();
	}
}


void QiInput::registerKeyDown(int key)
{
	if (mInputStream)
		return;
	if (key >= 'A' && key <= 'Z')
		key += 32;
	QiInput::Event e={0};
	e.type = QiInput::KEY_DOWN;
	e.id = key;
	mEvents.add(e);
	if (!mKeyDown[key])
		mKeyPressed[key] = true;
	mKeyDown[key] = true;
	if (key == QI_KEY_LSHIFT || key == QI_KEY_RSHIFT)
		registerKeyDown(QI_KEY_SHIFT);
}


void QiInput::registerKeyUp(int key)
{
	if (mInputStream)
		return;
	QiInput::Event e={0};
	e.type = QiInput::KEY_UP;
	e.id = key;
	mEvents.add(e);
	mKeyDown[key] = false;
	mKeyReleased[key] = true;
	if (key == QI_KEY_LSHIFT || key == QI_KEY_RSHIFT)
		registerKeyUp(QI_KEY_SHIFT);
}


void QiInput::registerMousePos(int x, int y)
{
	if (mInputStream)
		return;
	QiInput::Event e={0};
	e.type = QiInput::MOUSE_MOVE;
	e.x = x;
	e.y = y;
	mEvents.add(e);
	mMouseX = x;
	mMouseY = y;

	if (mButtonDown[QI_BUTTON_LEFT])
	{
		registerTouchPos(1, mMouseX, mMouseY);
		mEvents.removeLast();
	}
}


void QiInput::registerButtonDown(int button)
{
	if (mInputStream)
		return;
	QiInput::Event e={0};
	e.type = QiInput::BUTTON_DOWN;
	e.id = button;
	mEvents.add(e);
	mButtonDown[button] = true;
	mButtonPressed[button] = true;

	if (button == QI_BUTTON_LEFT)
	{
		registerTouchBegin(1, mMouseX, mMouseY);
		mEvents.removeLast();
	}
}


void QiInput::registerButtonUp(int button)
{
	if (mInputStream)
		return;
	QiInput::Event e={0};
	e.type = QiInput::BUTTON_UP;
	e.id = button;
	mEvents.add(e);
	mButtonDown[button] = false;
	mButtonReleased[button] = true;

	if (button == QI_BUTTON_LEFT)
	{
		registerTouchEnd(1);
		mEvents.removeLast();
	}
}


void QiInput::registerCharacter(int c)
{
	if (mInputStream)
		return;
	QiInput::Event e={0};
	e.type = QiInput::CHARACTER;
	e.id = c;
	mEvents.add(e);
}


void QiInput::registerTouchBegin(int id, int x, int y)
{
	if (mInputStream)
		return;
	QiInput::Event e={0};
	e.type = QiInput::TOUCH_BEGIN;
	e.id = id;
	e.x = x;
	e.y = y;
	mEvents.add(e);

	int t = -1;
	for(int i=0; i<QI_MAX_TOUCH; i++)
	{
		if (mTouchId[i] == 0)
		{
			t = i;
			break;
		}
	}
	if (t!=-1)
	{
		mTouchId[t] = id;
		mTouchPressed[t] = true;
		mTouchPos[t][0] = x;
		mTouchPos[t][1] = y;
		mLastTouchPos[t][0] = x;
		mLastTouchPos[t][1] = y;
	}
}


void QiInput::registerTouchPos(int id, int x, int y)
{
	if (mInputStream)
		return;
	QiInput::Event e={0};
	e.type = QiInput::TOUCH_MOVE;
	e.id = id;
	e.x = x;
	e.y = y;
	mEvents.add(e);

	for(int i=0; i<QI_MAX_TOUCH; i++)
	{
		if (mTouchId[i] == id)
		{
			mTouchPos[i][0] = x;
			mTouchPos[i][1] = y;
			break;
		}
	}
}


void QiInput::registerTouchEnd(int id)
{
	if (mInputStream)
		return;
	QiInput::Event e={0};
	e.type = QiInput::TOUCH_END;
	e.id = id;
	mEvents.add(e);

	for(int i=0; i<QI_MAX_TOUCH; i++)
	{
		if (mTouchId[i] == id)
		{
			mTouchId[i] = 0;
			mTouchReleased[i] = true;
			break;
		}
	}
}


int QiInput::getTouchCount() const
{
	int c=0;
	for(int i=0; i<QI_MAX_TOUCH; i++)
	{
		if (mTouchId[i])
			c++;
	}
	return c;
}


int QiInput::getTouch(int i) const
{
	int c=0;
	for(int i=0; i<QI_MAX_TOUCH; i++)
	{
		if (mTouchId[i])
		{
			if (c++ == i)
				return i;
		}
	}
	return -1;
}


int QiInput::getTouchId(int touch) const
{
	QI_ASSERT(touch < QI_MAX_TOUCH, "Invalid touch");
	if (touch < QI_MAX_TOUCH)
		return mTouchId[touch];
	else
		return 0;
}



int QiInput::getTouchPosX(int touch) const
{
	QI_ASSERT(touch < QI_MAX_TOUCH, "Invalid touch");
	if (touch < QI_MAX_TOUCH)
		return mTouchPos[touch][0];
	else
		return 0;
}


int QiInput::getTouchPosY(int touch) const
{
	QI_ASSERT(touch < QI_MAX_TOUCH, "Invalid touch");
	if (touch < QI_MAX_TOUCH)
		return mTouchPos[touch][1];
	else
		return 0;
}


int QiInput::getTouchDiffX(int touch) const
{
	QI_ASSERT(touch < QI_MAX_TOUCH, "Invalid touch");
	if (touch < QI_MAX_TOUCH)
		return mTouchPos[touch][0] - mLastTouchPos[touch][0];
	else
		return 0;
}


int QiInput::getTouchDiffY(int touch) const
{
	QI_ASSERT(touch < QI_MAX_TOUCH, "Invalid touch");
	if (touch < QI_MAX_TOUCH)
		return mTouchPos[touch][1] - mLastTouchPos[touch][1];
	else
		return 0;
}


bool QiInput::hasTouch(int touch) const
{
	QI_ASSERT(touch < QI_MAX_TOUCH, "Invalid touch");
	if (touch < QI_MAX_TOUCH)
		return mTouchId[touch] != 0;
	else
		return false;
}


bool QiInput::wasTouchPressed(int touch) const
{
	QI_ASSERT(touch < QI_MAX_TOUCH, "Invalid touch");
//#if defined(QI_ANDROID)
//	LOGD("wasTouchPressed----------%d",touch);
//#endif
	if (touch < QI_MAX_TOUCH)
		return mTouchPressed[touch];
	else
		return false;
}


bool QiInput::wasTouchReleased(int touch) const
{
	QI_ASSERT(touch < QI_MAX_TOUCH, "Invalid touch");
//#if defined(QI_ANDROID)
//	LOGD("wasTouchReleased----------%d",touch);
//#endif
	if (touch < QI_MAX_TOUCH)
		return mTouchReleased[touch];
	else
		return false;
}


bool QiInput::isTouched(const QiVec2& pos, float radius, int* touch) const
{
	for(int i=0; i<QI_MAX_TOUCH; i++)
	{
		if (mTouchId[i])
		{
			if (lengthSquared(QiVec2((float)mTouchPos[i][0], (float)mTouchPos[i][1]) - pos) < radius*radius)
            {
                if (touch)
                    *touch = i;
				return true;
            }
		}
	}
	return false;
}


bool QiInput::wasTouched(const QiVec2& pos, float radius, int* touch) const
{
	for(int i=0; i<QI_MAX_TOUCH; i++)
	{
		if (mTouchId[i] && mTouchPressed[i])
		{
			if (lengthSquared(QiVec2((float)mTouchPos[i][0], (float)mTouchPos[i][1]) - pos) < radius*radius)
            {
                if (touch)
                    *touch = i;
				return true;
            }
		}
	}
	return false;
}

