#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiarray.h"
#include "math/qivec2.h"

const int QI_MAX_KEYS = 1024;
const int QI_MAX_BUTTONS = 4;
const int QI_MAX_TOUCH = 32;

enum QiKey
{
	QI_KEY_UNDEFINED	= 0,
	QI_KEY_SPACE		= 32,
	QI_KEY_RETURN		= 256,
	QI_KEY_BACKSPACE	= 257,
	QI_KEY_DELETE		= 258,
	QI_KEY_ESCAPE		= 259,
	QI_KEY_SHIFT		= 260,
	QI_KEY_LSHIFT		= 261,
	QI_KEY_RSHIFT		= 262,
	QI_KEY_UP			= 263,
	QI_KEY_DOWN			= 264,
	QI_KEY_LEFT			= 265,
	QI_KEY_RIGHT		= 266,
	QI_KEY_CTRL			= 267,
	QI_KEY_HOME			= 268,
	QI_KEY_END			= 269,
	QI_KEY_PGUP			= 270,
	QI_KEY_PGDOWN		= 271,
	QI_KEY_INSERT		= 272,
	QI_KEY_TAB			= 273,
	QI_KEY_F1			= 274,
	QI_KEY_F2			= 275,
	QI_KEY_F3			= 276,
	QI_KEY_F4			= 277,
	QI_KEY_F5			= 278,
	QI_KEY_F6			= 279,
	QI_KEY_F7			= 280,
	QI_KEY_F8			= 281,
	QI_KEY_F9			= 282,
	QI_KEY_F10			= 283,
	QI_KEY_F11			= 284,
	QI_KEY_F12			= 285
};

enum QiButton
{
	QI_BUTTON_UNDEFINED = 0,
	QI_BUTTON_LEFT		= 1,
	QI_BUTTON_MIDDLE	= 2,
	QI_BUTTON_RIGHT		= 3
};

class QiInput
{
public:
	enum EventType
	{
		KEY_DOWN,
		KEY_UP,
		BUTTON_DOWN,
		BUTTON_UP,
		MOUSE_MOVE,
		TOUCH_BEGIN,
		TOUCH_END,
		TOUCH_MOVE,
		CHARACTER
	};

	struct Event
	{
		int type;
		int id;
		int x;
		int y;
	};

	QiInput();
	~QiInput();

	void record(class QiOutputStream& stream);
	void playback(class QiInputStream& stream);
	void stop();

	//Query interface
	int getMousePosX() const;
	int getMousePosY() const;
	int getMouseDiffX() const;
	int getMouseDiffY() const;
	bool wasButtonPressed(int button) const;
	bool wasButtonReleased(int button) const;
	bool isButtonDown(int button) const;

	bool wasKeyPressed(int key) const;
	bool wasKeyReleased(int key) const;
	bool isKeyDown(int key) const;

	int getTouchCount() const;
	int getTouch(int i) const;

	int getTouchId(int touch) const;
	int getTouchPosX(int touch) const;
	int getTouchPosY(int touch) const;
	int getTouchDiffX(int touch) const;
	int getTouchDiffY(int touch) const;
	inline QiVec2 getTouchPos(int touch) const { return QiVec2((float)getTouchPosX(touch), (float)getTouchPosY(touch)); }
	inline QiVec2 getTouchDiff(int touch) const { return QiVec2((float)getTouchDiffX(touch), (float)getTouchDiffY(touch)); }
	bool hasTouch(int touch) const;
	bool wasTouchPressed(int touch) const;
	bool wasTouchReleased(int touch) const;
	bool isTouched(const QiVec2& pos, float radius, int* touch=NULL) const;
	bool wasTouched(const QiVec2& pos, float radius, int* touch=NULL) const;

	int getEventCount() const;
	const Event& getEvent(int i) const;

	//Used by event producer
	void registerBegin();
	void registerKeyDown(int key);
	void registerKeyUp(int key);
	void registerMousePos(int x, int y);
	void registerButtonDown(int button);
	void registerButtonUp(int button);	
	void registerCharacter(int c);
	void registerTouchBegin(int id, int x, int y);
	void registerTouchPos(int id, int x, int y);
	void registerTouchEnd(int id);
	void registerEvent(const Event& e);
	void registerEnd();

	void clear();
	
	inline void setPlaybackScale(float x, float y) { mPlaybackScaleX=x; mPlaybackScaleY=y;}

private:
	bool mKeyDown[QI_MAX_KEYS];
	bool mKeyPressed[QI_MAX_KEYS];
	bool mKeyReleased[QI_MAX_KEYS];

	int mLastMouseX;
	int mLastMouseY;
	int mMouseX;
	int mMouseY;
	
	float mPlaybackScaleX;
	float mPlaybackScaleY;

	bool mButtonPressed[QI_MAX_BUTTONS];
	bool mButtonReleased[QI_MAX_BUTTONS];
	bool mButtonDown[QI_MAX_BUTTONS];

	int mTouchId[QI_MAX_TOUCH];
	int mLastTouchPos[QI_MAX_TOUCH][2];
	int mTouchPos[QI_MAX_TOUCH][2];
	bool mTouchPressed[QI_MAX_TOUCH];
	bool mTouchReleased[QI_MAX_TOUCH];

	class QiInputStream* mInputStream;
	class QiOutputStream* mOutputStream;
	QiArrayInplace<Event, 64> mEvents;
};
