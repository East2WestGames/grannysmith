#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiarray.h"
#include "base/qimemorystream.h"

class QiUndoImplementation
{
public:
	virtual ~QiUndoImplementation() {}
	virtual void loadUndoState(QiInputStream& stream, int size) = 0;
	virtual void saveUndoState(QiOutputStream& stream) = 0;
};

class QiUndo
{
public:
	QiUndo(QiUndoImplementation* impl, int maxSteps);
	~QiUndo();

	void checkpoint();
	void reset();

	void undo();
	void redo();

private:
	void load(int pos);

	typedef QiMemoryStream<4096> UndoState; 

	QiUndoImplementation* mImplementation;
	QiArray<UndoState*> mUndoStates;
	int mMax;
	int mPos;
};

