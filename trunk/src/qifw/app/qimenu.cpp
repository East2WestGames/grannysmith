/* Qi Framework. Copyright 2007-2012 Dengu AB */

#ifdef QI_USE_OPENGL

#include "base/qistring.h"
#include "app/qimenu.h"
#include "app/qiinput.h"
#include "gfx/qiviewport.h"
#include "gfx/qigl.h"

#if _MSC_VER
#pragma warning (disable: 4244)
#endif


QiMenu::QiMenu() : mOpen(false), mId(-1)
{
}


QiMenu::QiMenu(QiViewport& viewport) : mOpen(false), mId(-1)
{
	init(viewport);
}


void QiMenu::init(QiViewport& viewport)
{
	mViewport = &viewport;
	mTextRenderer.init();
	mTextRenderer.setFont(QiTextRenderer::TAHOMA_14);
	mTextRenderer.setAlignment(QiTextRenderer::LEFT);
	mTextRenderer.flip(true);
}


void QiMenu::beginFrame(QiInput& input)
{
	if (mOpen)
	{
		mInput = input;
		input.clear();
		mItems.clear();
	}
	else
	{
		x = input.getMousePosX();
		y = input.getMousePosY();
	}
}


void QiMenu::endFrame()
{
	mPickedItem = "";
	if (mOpen)
	{
		mViewport->pickClear();
		int w = 0;
		int h = 0;

		for(int i=0; i<mItems.getCount(); i++)
		{
			w = QiMax(w, mTextRenderer.getWidth(mItems[i]));
			h += mTextRenderer.getHeight(mItems[i]);
		}

		glDisable(GL_DEPTH_TEST);
		mViewport->push();
		mViewport->setModePixel();
		mViewport->apply();
		int py = y;
		glColor4f(0.8f,0.8f,0.8f,0.8f);
		glBegin(GL_QUADS);
		glVertex2f(x-5, y-5);
		glVertex2f(x-5, y+h+5);
		glVertex2f(x+w+5, y+h+5);
		glVertex2f(x+w+5, y-5);
		glEnd();
		glColor4f(0.0f,0.0f,0.0f,1);
		glBegin(GL_LINE_LOOP);
		glVertex2f(x-5, y-5);
		glVertex2f(x-5, y+h+5);
		glVertex2f(x+w+5, y+h+5);
		glVertex2f(x+w+5, y-5);
		glEnd();
		
		for(int i=0; i<mItems.getCount(); i++)
		{
			int h = mTextRenderer.getHeight(mItems[i]);
			mTextRenderer.setColor(0,0,0,1);
			if (mViewport->pickRect(i, QiVec2(x-4, py+h), QiVec2(x+w+4, py)))
			{
				glColor4f(0,0,0,0.5f);
				glBegin(GL_QUADS);
				glVertex2f(x-4, py);
				glVertex2f(x-4, py+h);
				glVertex2f(x+w+4, py+h);
				glVertex2f(x+w+4, py);
				glEnd();
				mTextRenderer.setColor(1,1,1,1);
				if (mInput.wasButtonReleased(QI_BUTTON_LEFT))
					mPickedItem = mItems[i];
			}
			mTextRenderer.setPosition(x, py);
			mTextRenderer.print(mItems[i]);
			py += h;
		}
		
		mViewport->pop();
		mViewport->apply();

		if (mInput.wasButtonReleased(QI_BUTTON_LEFT) || mInput.wasButtonReleased(QI_BUTTON_RIGHT))
			mOpen = false;
		mInput.clear();
	}
	else
	{
		mId = -1;
	}
}


void QiMenu::open(int id)
{
	if (mId != -1)
		return;
	mOpen = true;
	mId = id;
}


bool QiMenu::isOpen(int id)
{
	return mId==id;
}


bool QiMenu::isOpen()
{
	return mOpen;
}


bool QiMenu::item(int id, const QiString& text)
{
	QI_ASSERT(id != -1, "Id -1 is reserved");
	if (mId != id)
		return false;

	if (mPickedItem == text)
		return true;
	else
	{
		mItems.add(text);
		return false;
	}
}


#endif

