/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "app/qiinputbox.h"

QiInputBox::QiInputBox() :
mCursorPos(0), mSelectionStart(0), mSelectionEnd(0)
{
}


void QiInputBox::setText(const QiString& text)
{
	mText = text;
	mCursorPos = text.getLength()-1;
	mSelectionStart = mSelectionEnd=0;
}


void QiInputBox::processInput(const QiInput& input)
{
	for(int i=0; i<input.getEventCount(); i++)
	{
		const QiInput::Event& e = input.getEvent(i);
		if (e.type == QiInput::KEY_DOWN)
		{
			if (e.id == QI_KEY_LEFT)
			{
				if (input.isKeyDown(QI_KEY_CTRL))
					mCursorPos = 0;
				else
					mCursorPos = QiMax(mCursorPos-1, 0);
			}
			else if (e.id == QI_KEY_RIGHT)
			{
				if (input.isKeyDown(QI_KEY_CTRL))
					mCursorPos = mText.getLength();
				else
					mCursorPos = QiMin(mCursorPos+1, mText.getLength());
			}
			else if (e.id == QI_KEY_HOME)
				mCursorPos = 0;
			else if (e.id == QI_KEY_END)
				mCursorPos = mText.getLength();
			else if (e.id == 'a' && input.isKeyDown(QI_KEY_CTRL))
				mCursorPos = 0;
			else if (e.id == 'e' && input.isKeyDown(QI_KEY_CTRL))
				mCursorPos = mText.getLength();
			else if (e.id == 'k' && input.isKeyDown(QI_KEY_CTRL))
				mText = mText.substring(0, mCursorPos);
			else if (e.id == QI_KEY_END)
				mCursorPos = mCursorPos = mText.getLength();
			else if (e.id == QI_KEY_BACKSPACE && mCursorPos > 0)
			{
				mText = mText.substring(0, mCursorPos-1) + mText.substring(mCursorPos, mText.getLength());
				mCursorPos--;
			}
			else if (e.id == QI_KEY_DELETE && mCursorPos < mText.getLength())
			{
				mText = mText.substring(0, mCursorPos) + mText.substring(mCursorPos+1, mText.getLength());
			}
			else if (e.id == QI_KEY_RETURN)
			{
			}
		}
		else if (e.type == QiInput::CHARACTER && e.id>=' ' && e.id<='z')
		{
			mText = mText.substring(0, mCursorPos) + (char)e.id + mText.substring(mCursorPos, mText.getLength());
			mCursorPos++;
		}
	}
}

