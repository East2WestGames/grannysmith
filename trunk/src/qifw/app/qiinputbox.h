#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qistring.h"
#include "app/qiinput.h"

class QiInputBox
{
public:
	QiInputBox();

	inline const QiString& getText() const { return mText; }
	void setText(const QiString& text);

	inline int getCursosPos() const { return mCursorPos; }
	inline void setCursosPos(int i) { mCursorPos = i; }

	inline int getSelectionStart() const { return mSelectionStart; }
	inline int getSelectionEnd() const { return mSelectionEnd; }
	inline void setSelection(int start, int end) { mSelectionStart=start; mSelectionEnd=end; }

	void processInput(const QiInput& input);

protected:
	int mCursorPos;
	int mSelectionStart;
	int mSelectionEnd;
	QiString mText;
};

