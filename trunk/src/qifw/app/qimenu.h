#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

	void undoCheckpoint();
	void resetUndo();
	void loadUndo(int pos);
#include "base/qistring.h"
#include "base/qiarray.h"
#include "gfx/qitextrenderer.h"
#include "app/qiinput.h"

class QiViewport;

class QiMenu
{
public:
	QiMenu();
	QiMenu(QiViewport& viewport);
	void init(QiViewport& viewport);

	void beginFrame(class QiInput& input);
	void endFrame();
	void open(int id);
	bool isOpen(int id);
	bool isOpen();
	bool item(int id, const QiString& text);

public:
	QiViewport* mViewport;
	QiString mPickedItem;
	QiArray<QiString> mItems;
	QiTextRenderer mTextRenderer;
	QiInput mInput;
	bool mOpen;
	int x, y;
	int mId;
};

