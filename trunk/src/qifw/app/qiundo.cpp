/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "qiundo.h"


QiUndo::QiUndo(QiUndoImplementation* impl, int maxSteps) : mImplementation(impl), mMax(maxSteps), mPos(0)
{
}


QiUndo::~QiUndo()
{
	reset();
}


void QiUndo::checkpoint()
{
	if (mPos < mUndoStates.getCount())
	{
		for(int i=mPos; i<mUndoStates.getCount(); i++)
			QI_DELETE(mUndoStates[i]);
		mUndoStates.redim(mPos);
	}
	if (mUndoStates.getCount() > mMax)
	{
		QI_DELETE(mUndoStates[0]);
		for(int i=1; i<mUndoStates.getCount(); i++)
			mUndoStates[i-1] = mUndoStates[i];
		mUndoStates.redim(mUndoStates.getCount()-1);
	}
	UndoState* state = QI_NEW UndoState();
	mImplementation->saveUndoState(*state);
	mUndoStates.add(state);
	mPos = mUndoStates.getCount();
}


void QiUndo::reset()
{
	for(int i=0; i<mUndoStates.getCount(); i++)
		QI_DELETE(mUndoStates[i]);
	mUndoStates.clear();
	mPos = 0;
	checkpoint();
}


void QiUndo::undo()
{
	if (mPos > 0)
	{
		if (mPos == mUndoStates.getCount())
		{
			checkpoint();
			mPos--;
		}
		mPos--;
		load(mPos);
	}
}


void QiUndo::redo()
{
	if (mPos+1 < mUndoStates.getCount())
	{
		mPos++;
		load(mPos);
	}
}


void QiUndo::load(int pos)
{
	mUndoStates[pos]->seekRead(0);
	mImplementation->loadUndoState(*mUndoStates[pos], mUndoStates[pos]->getSize());
}


