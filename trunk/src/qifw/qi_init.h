#ifndef QI_INIT_H
#define QI_INIT_H

#include "base/qidebug.h"
#include "base/qiallocator.h"
#include <stdio.h>

class QiDefaultDebugStream : public QiDebugStream
{
	virtual void print(const char* msg)
	{
		printf("%s", msg);
	}
};

static QiDefaultDebugStream gDebugStream;

inline void QiInit()
{
	//Init debug streams
	QiDebug::setPrintStream(&gDebugStream);
	QiDebug::setWarningStream(&gDebugStream);
	QiDebug::setErrorStream(&gDebugStream);
}

inline void QiShutdown()
{
	//Shutdown debug streams
	QiDebug::setPrintStream(NULL);
	QiDebug::setWarningStream(NULL);
	QiDebug::setErrorStream(NULL);
}

#endif

