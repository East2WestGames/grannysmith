#ifndef QI_HEADERS_MATH_H
#define QI_HEADERS_MATH_H

#include "math/qigauss.h"
#include "math/qiinterpolate.h"
#include "math/qimatrix3.h"
#include "math/qimatrix4.h"
#include "math/qiperlinnoise.h"
#include "math/qiquat.h"
#include "math/qirnd.h"
#include "math/qitransform2.h"
#include "math/qitransform3.h"
#include "math/qivec2.h"
#include "math/qivec3.h"

#endif
