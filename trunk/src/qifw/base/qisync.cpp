/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qisync.h"
#include "base/qimem.h"

/////////////////////////////////////////////////////////////
////////////////////////// WIN32 ////////////////////////////
/////////////////////////////////////////////////////////////
#ifdef QI_WIN32

#include <windows.h>
typedef CRITICAL_SECTION QiMutexHandle;
typedef HANDLE QiRwLockHandle;
typedef HANDLE QiConditionHandle;
typedef HANDLE QiSemaphoreHandle;

class QiMutexImpl { public: QiMutexHandle mHandle; };
class QiConditionImpl { public: QiConditionHandle mHandle; };
class QiRwLockImpl { public: QiRwLockHandle mHandle; };
class QiSemaphoreImpl { public: QiSemaphoreHandle mHandle; };

QiMutex::QiMutex()
{
	mImpl = QI_NEW QiMutexImpl;
	InitializeCriticalSectionAndSpinCount(&mImpl->mHandle, 4000);
}

QiMutex::~QiMutex()
{
	DeleteCriticalSection(&mImpl->mHandle);
	QI_DELETE(mImpl);
}
	
bool QiMutex::tryLock()
{
	//TODO: Fixme
	return true;
}

void QiMutex::lock()
{
	EnterCriticalSection(&mImpl->mHandle);
}

void QiMutex::unlock()
{
	LeaveCriticalSection(&mImpl->mHandle);
}

QiCondition::QiCondition()
{
	mImpl = QI_NEW QiConditionImpl;
	mImpl->mHandle = CreateEvent(NULL, FALSE, FALSE, NULL);
}

QiCondition::~QiCondition()
{
	CloseHandle(mImpl->mHandle);
	QI_DELETE(mImpl);
}
	
void QiCondition::wait()
{
	WaitForSingleObject(mImpl->mHandle, INFINITE);
}

void QiCondition::signal()
{
	SetEvent(mImpl->mHandle);
}

void QiCondition::reset()
{
	ResetEvent(mImpl->mHandle);
}



QiSemaphore::QiSemaphore()
{
	mImpl = QI_NEW QiSemaphoreImpl;
}

QiSemaphore::QiSemaphore(int count, int maxCount)
{
	mImpl = QI_NEW QiSemaphoreImpl;
	init(count, maxCount);
}

QiSemaphore::~QiSemaphore()
{
	::CloseHandle(mImpl->mHandle);
	QI_DELETE(mImpl->mHandle);
}

void QiSemaphore::init(int count, int maxCount)
{
	mImpl->mHandle = ::CreateSemaphoreA(NULL, count, maxCount, NULL);
}

void QiSemaphore::release(int count)
{
	::ReleaseSemaphore(mImpl->mHandle, count, NULL);
}

void QiSemaphore::wait()
{
	::WaitForSingleObject(mImpl->mHandle, INFINITE);
}

/////////////////////////////////////////////////////////////
////////////////////////// POSIX ////////////////////////////
/////////////////////////////////////////////////////////////
#else

#include <pthread.h>
#include <semaphore.h>
typedef pthread_mutex_t QiMutexHandle;
typedef pthread_rwlock_t QiRwLockHandle;
typedef pthread_cond_t QiConditionHandle;
typedef sem_t QiSemaphoreHandle;

class QiMutexImpl { public: QiMutexHandle mHandle; };
class QiConditionImpl { public: QiConditionHandle mConditionHandle; QiMutexHandle mMutexHandle; bool mSignaled; };
class QiRwLockImpl { public: QiRwLockHandle mHandle; };
class QiSemaphoreImpl { public: QiSemaphoreHandle mHandle; };

QiMutex::QiMutex()
{
	mImpl = QI_NEW QiMutexImpl;
	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
	pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init((pthread_mutex_t*)&mImpl->mHandle, &attr);
}

QiMutex::~QiMutex()
{
	pthread_mutex_destroy((pthread_mutex_t*)&mImpl->mHandle);
	QI_DELETE(mImpl);
}
	
bool QiMutex::tryLock()
{
	return pthread_mutex_trylock((pthread_mutex_t*)&mImpl->mHandle) == 0;
}

void QiMutex::lock()
{
	pthread_mutex_lock((pthread_mutex_t*)&mImpl->mHandle);
}

void QiMutex::unlock()
{
	pthread_mutex_unlock((pthread_mutex_t*)&mImpl->mHandle);
}


#ifndef QI_ANDROID

QiRwLock::QiRwLock() :
mState(UNLOCKED)
{
	mImpl = QI_NEW QiRwLockImpl;
	pthread_rwlock_init((pthread_rwlock_t*)&mImpl->mHandle, NULL);
}

QiRwLock::~QiRwLock()
{
	pthread_rwlock_destroy((pthread_rwlock_t*)&mImpl->mHandle);
	QI_DELETE(mImpl);
}
	
bool QiRwLock::tryLockRead()
{
	return pthread_rwlock_tryrdlock((pthread_rwlock_t*)&mImpl->mHandle) == 0;
}

void QiRwLock::lockRead()
{
	pthread_rwlock_rdlock((pthread_rwlock_t*)&mImpl->mHandle);
	mState = READ_LOCKED;
}

bool QiRwLock::tryLockWrite()
{
	return pthread_rwlock_trywrlock((pthread_rwlock_t*)&mImpl->mHandle) == 0;
}

void QiRwLock::lockWrite()
{
	pthread_rwlock_wrlock((pthread_rwlock_t*)&mImpl->mHandle);
	mState = WRITE_LOCKED;
}

void QiRwLock::unlock()
{
	pthread_rwlock_unlock((pthread_rwlock_t*)&mImpl->mHandle);
	mState = UNLOCKED;
}

#endif


QiCondition::QiCondition()
{
	mImpl = QI_NEW QiConditionImpl;
	mImpl->mSignaled = false;
	pthread_cond_init((pthread_cond_t*)&mImpl->mConditionHandle, NULL);
	pthread_mutex_init((pthread_mutex_t*)&mImpl->mMutexHandle, NULL);
}

QiCondition::~QiCondition()
{
	pthread_mutex_destroy((pthread_mutex_t*)&mImpl->mMutexHandle);
	pthread_cond_destroy((pthread_cond_t*)&mImpl->mConditionHandle);
	QI_DELETE(mImpl);
}

void QiCondition::wait()
{
	pthread_mutex_lock((pthread_mutex_t*)&mImpl->mMutexHandle);
	while(!mImpl->mSignaled)
		pthread_cond_wait((pthread_cond_t*)&mImpl->mConditionHandle, &mImpl->mMutexHandle);
	mImpl->mSignaled = false;
	pthread_mutex_unlock((pthread_mutex_t*)&mImpl->mMutexHandle);
}

void QiCondition::signal()
{
	pthread_mutex_lock((pthread_mutex_t*)&mImpl->mMutexHandle);
	mImpl->mSignaled = true;
	pthread_cond_broadcast((pthread_cond_t*)&mImpl->mConditionHandle);
	pthread_mutex_unlock((pthread_mutex_t*)&mImpl->mMutexHandle);
}


QiSemaphore::QiSemaphore()
{
	mImpl = QI_NEW QiSemaphoreImpl;
}

QiSemaphore::QiSemaphore(int count, int maxCount)
{
	mImpl = QI_NEW QiSemaphoreImpl;
	init(count, maxCount);
}

QiSemaphore::~QiSemaphore()
{
	sem_destroy(&mImpl->mHandle);
	QI_DELETE(mImpl);
}

void QiSemaphore::init(int count, int maxCount)
{
	sem_init(&mImpl->mHandle, 0, count);
}

void QiSemaphore::release(int count)
{
	for(int i=0; i<count; i++)
		sem_post(&mImpl->mHandle);
}

void QiSemaphore::wait()
{
	sem_wait(&mImpl->mHandle);
}


#endif

