#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"
#include "base/qimem.h"
#include "base/qidebug.h"
#include "base/qisort.h"

#include <new>
#include <string.h>

template <class T>
class QiArray
{
public:
	class Iterator
	{
	public:
		Iterator(const QiArray& array) :
		mArray(array), mPos(0)
		{
		}
		
		inline bool hasNext() const
		{
			return mPos < mArray.getCount();
		}

		inline const T& next()
		{
			QI_ASSERT(hasNext(), "Reached end of array");
			return mArray[mPos++];
		}
		
		inline const T& getCurrent() const
		{
			QI_ASSERT(hasNext(), "Reached end of array");
			return mArray[mPos];
		}
		
		inline int getCount() const
		{
			return mArray.getCount();
		}
		
		inline void reset()
		{
			mPos = 0;
		}
		
	protected:
		const QiArray<T>& mArray;
		int mPos;
	};
	

	inline QiArray() : 
	mCount(0), mSize(0), mData(NULL)
	{
	}

	
	inline QiArray(const QiArray& other) :
	mCount(0), mSize(0), mData(NULL)
	{
		*this = other;
	}


	inline ~QiArray() 
	{
		redim(0);
		if (mData && (char*)mData != ((char*)this)+sizeof(QiArray<T>))
			QiFree(mData);
	}


	inline void reserve(int count)
	{
		grow(count);
	}


	inline void redim(int count)
	{
		if (count > mCount)
		{
			grow(count);
			for(int i=mCount; i<count; i++)
				new (mData+i) T;
			mCount = count;
		}
		else if (count < mCount)
		{
			for(int i=count; i<mCount; i++)
				(mData+i)->~T();
			mCount = count;
			grow(count);
		}
	}

	inline QiArray& operator=(const QiArray& other)
	{
		clear();
		addAll(other.getIterator());
		return *this;
	}

	inline const T& operator[] (int i) const 
	{
		QI_ASSERT(i>=0 && i<mCount, "Index out of bound");
		return mData[i];
	}


	inline T& operator[] (int i) 
	{
		QI_ASSERT(i>=0 && i<mCount, "Index out of bound");
		return mData[i];
	}

	inline T& first()
	{
		QI_ASSERT(mCount>0, "Index out of bound");
		return mData[0];
	}

	inline const T& first() const
	{
		QI_ASSERT(mCount>0, "Index out of bound");
		return mData[0];
	}

	inline T& last()
	{
		QI_ASSERT(mCount>0, "Index out of bound");
		return mData[mCount-1];
	}	

	inline const T& last() const
	{
		QI_ASSERT(mCount>0, "Index out of bound");
		return mData[mCount-1];
	}
	
	inline int getCount() const 
	{
		return mCount;
	}


	inline bool isEmpty() const 
	{
		return mCount==0;
	}


	inline void clear() 
	{
		redim(0);
	}


	inline int getIndexOf(const T &a) const 
	{
		int i;
		for(i=0; i<mCount; i++)
		{
			if(mData[i]==a) 
				return i;
		}
		return -1;
	}

	inline bool contains(const T& a) const
	{
		return getIndexOf(a)!=-1;
	}

	inline T& add() 
	{
		if (mSize<=mCount)
			grow(mSize*2+1);
		redim(mCount+1);
		return mData[mCount-1];
	}

	inline void insertAt(int pos, const T& a) 
	{
		if (pos > mCount)
			return;
		if (mSize<=mCount)
			grow(mSize*2+1);
		int c = mCount-pos;
		redim(mCount+1);
		memmove(mData+pos+1, mData+pos, sizeof(T)*c);
		mData[pos] = a;
	}

	inline void add(const T &a, int* index = NULL) 
	{
		if (mSize<=mCount)
			grow(mSize*2+1);
		if (index) 
			*index = mCount;
		redim(mCount+1);
		mData[mCount-1]=a;
	}

	inline void addChunk(const T* a, int count) 
	{
		if (mSize<mCount+count)
			grow((mCount+count)*2+1);
		memcpy(mData+mCount, a, sizeof(T)*count);
		mCount+=count;
	}

	template <class Iterator>
	inline void addAll(Iterator iterator)
	{
		int c = mCount;
		redim(mCount + iterator.getCount());
		while(iterator.hasNext())
			mData[c++] = iterator.next();
	}


	inline T removeElementAt(int i, bool preserveOrder=false) 
	{
		QI_ASSERT(i<mCount, "Index out of bound");
		T tmp=mData[i];
		if (preserveOrder)
			memmove(mData+i, mData+i+1, sizeof(T)*(mCount-i-1));
		else
			mData[i]=mData[mCount-1];
		redim(mCount-1);
		return tmp;
	}


	inline T removeLast() 
	{
		return removeElementAt(mCount-1);
	}

	inline bool removeAll(const T &a) 
	{
		int i;
		int low=0;
		bool ret=false;
		do
		{
			for(i=low; i<mCount; i++) 
			{
				if(mData[i]==a) 
					break;
			}
			if (i<mCount)
			{
				removeElementAt(i);
				ret=true;
				low=i;
			}
		} while(i<mCount);
		return ret;
	}

	
	inline Iterator getIterator() const
	{
		return Iterator(*this);
	}

	inline T* getData()
	{
		return mData;
	}

	const inline T* getData() const
	{
		return mData;
	}

	void sort()
	{
		QiSort(mData, mCount, QiComparator<T>());
	}

protected:
	int mCount;
	int mSize;
	T* mData;

	inline void grow(int size) {
		if(size > mSize) 
		{
			if (mData == NULL)
			{
				mData = (T*)QiAlloc(size*sizeof(T), "QiArray::Data");
			}
			else if ((char*)mData == ((char*)this)+sizeof(QiArray<T>))
			{
				//This is quite ugly. Detect wether the allocation is immediately after
				//this object. In that case, this is an inplace array and initial data
				//should not be freed.
				T* newData = (T*)QiAlloc(size*sizeof(T), "QiArray::Data");
				if (newData)
					memcpy(newData, mData, mCount * sizeof(T));
				mData = newData;
			}
			else
			{
				mData = (T*)QiRealloc(mData, size*sizeof(T));
			}
			QI_ASSERT(mData!=NULL, "Out of memory");
			mSize = size;
		}
	}

	inline int getArraySize() {
		return mSize;
	}
};


template <class T, int size>
class QiArrayInplace : public QiArray<T>
{
public:
	inline QiArrayInplace() : QiArray<T>()
	{
		QiArray<T>::mData = (T*)mArr;
		QiArray<T>::mSize = size;
	}

	inline QiArrayInplace(int count) : QiArray<T>()
	{
		QiArray<T>::mData = (T*)mArr;
		QiArray<T>::mSize = size;
		QiArray<T>::redim(count);
	}
	
	inline QiArrayInplace(const QiArray<T>& other) : QiArray<T>()
	{
		QiArray<T>::mData = (T*)mArr;
		QiArray<T>::mSize = size;
		*this = other;
	}
	
	inline ~QiArrayInplace()
	{
	}

protected:
	char mArr[size * sizeof(T)];
};
