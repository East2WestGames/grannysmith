/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qimem.h"
#include "base/qidebug.h"
#include "base/qiallocator.h"

#include <stdlib.h>

//#define QI_USE_DRESSCODE
#define USE_ALLOCATOR 1

#ifdef QI_USE_DRESSCODE
#include "dresscode/dresscode.h"
#endif

static int sAllocatedBytes = 0;
static int sAllocationCount = 0;

#ifdef QI_WIN32
#include "malloc.h"
#endif

void* QiAlloc(size_t size, const char* desc)
{
#if USE_ALLOCATOR
	return QiAllocator::alloc(size);
#else
	void* ptr = ::malloc(size);
	//printf("alloc %i = %i\n", size, ptr);
	#ifdef QI_USE_DRESSCODE
	dcAlloc(ptr, size);
	#endif
	if (ptr)
	{
		sAllocationCount++;
		#ifdef QI_WIN32
		sAllocatedBytes += size;
		#endif
	}
	return ptr;
#endif
}

void* QiRealloc(void* ptr, size_t size)
{
#if USE_ALLOCATOR
	return QiAllocator::realloc(ptr, size);
#else
	if (ptr)
	{
		sAllocationCount--;
		#ifdef QI_WIN32
		sAllocatedBytes -= _msize(ptr);
		#endif
	}
	void* nptr = ::realloc(ptr, size);
	#ifdef QI_USE_DRESSCODE
	//dcRealloc(ptr, nptr, size);
	dcFree(ptr);
	dcAlloc(nptr, size);
	#endif
	//printf("realloc %i %i = %i\n", (int)ptr, size, nptr);
	if (nptr)
	{
		sAllocationCount++;
		#ifdef QI_WIN32
		sAllocatedBytes += size;
		#endif
	}
	return nptr;
#endif
}


void QiFree(void* ptr)
{
#if USE_ALLOCATOR
	QiAllocator::free(ptr);
#else
	if (ptr)
	{
		sAllocationCount--;
		#ifdef QI_WIN32
		sAllocatedBytes -= _msize(ptr);
		#endif
	}
	#ifdef QI_USE_DRESSCODE
	dcFree(ptr);
	#endif
	//printf("free %i\n", ptr);
	::free(ptr);
#endif
}


void* operator new (size_t size, QiMemType dummy)
{
	return QiAlloc(size);
}

void operator delete (void* ptr, QiMemType dummy)
{
	QiFree(ptr);
}

void* operator new[] (size_t size, QiMemType dummy)
{
	QI_ERROR("ARRAYS NOT SUPPORTED");
	return QiAlloc(size);
}

void operator delete[] (void* ptr, QiMemType dummy)
{
	QI_ERROR("ARRAYS NOT SUPPORTED");
	QiFree(ptr);
}

int QiGetAllocatedBytes()
{
	return sAllocatedBytes;
}

int QiGetAllocationCount()
{
	return sAllocationCount;
}

