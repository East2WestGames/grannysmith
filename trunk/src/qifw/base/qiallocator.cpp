/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiallocator.h"
#include "base/qidebug.h"
#include "base/qisync.h"
#include "base/qimath.h"

#include <stdlib.h>
#include <stdio.h>

static QiMutex gMutex;

static bool gInitialized = false;
static const int COUNT = 7;
static const int MAX_SMALL_ALLOC = 128;
static const int gSizes[COUNT] = { 8, 16, 32, 48, 64, 96, 128};
static const int gCount[COUNT] = { 4096, 2048, 4096, 12800, 6400, 2048, 256};
static int gOffset[COUNT];
static void* gMem = NULL;
static int gTotalSize = 0;
static QiUInt32* gAllocated[COUNT];
static QiUInt32 gLowestFree[COUNT];
static QiUInt32 gLowestFreeBit[COUNT];
static char gBankMap[MAX_SMALL_ALLOC+1];
static int gSystemAllocations = 0;
static int gFailedBankAllocations[COUNT];


inline static int getBankFromSize(int size)
{
	if (size > MAX_SMALL_ALLOC)
		return -1;
	else
		return gBankMap[size];
}


inline static int getBankFromAllocation(void* ptr)
{
	if ((QiUInt32)ptr >= (QiUInt32)gMem && (QiUInt32)ptr < ((QiUInt32)gMem)+gTotalSize)
	{
		int p = (QiUInt32)ptr - (QiUInt32)gMem;
		for(int i=0; i<COUNT-1; i++)
		{
			if (p < gOffset[i+1])
				return i;
		}
		return COUNT-1;
	}
	else
		return -1;
}


inline static void markAsAllocated(int bank, int i)
{
	QiUInt32 e = (i >> 5);
	QiUInt32 bit = (i & 31);
	QiUInt32 mask = (1U << bit);
	gAllocated[bank][e] |= mask;
	gLowestFree[bank] = e;
	if (gAllocated[bank][e] == 0xFFFFFFFFU)
		gLowestFreeBit[bank] = 0;
}


inline static void markAsFree(int bank, int i)
{
	QiUInt32 e = (i >> 5);
	QiUInt32 bit = (i & 31);
	QiUInt32 mask = (1U << bit);
	gAllocated[bank][e] &= ~mask;
	gLowestFree[bank] = QiMin(gLowestFree[bank], e);
	gLowestFreeBit[bank] = QiMin(gLowestFreeBit[bank], bit);
}


inline static int findNextFree(int bank)
{
	int c = gCount[bank];
	int e = (c >> 5);
	for(int i=gLowestFree[bank]; i<e; i++)
	{
		QiUInt32 t = gAllocated[bank][i];
		if (t != 0xFFFFFFFFU)
		{
			for(int bit=gLowestFreeBit[bank]; bit<32; bit++)
			{
				QiUInt32 mask = (1U << bit);
				if ((t & mask) == 0)
					return (i<<5)+bit;
			}
		}
	}
	return -1;
}


void QiAllocator::init()
{
	QiMutexZone lock(gMutex);
	QI_ASSERT(!gInitialized, "Already initialized");
	gTotalSize = 0;
	int size = 0;
	for(int i=0; i<COUNT; i++)
	{
		for(int j=size+1; j<=gSizes[i]; j++)
			gBankMap[j] = i;
		size = gSizes[i];
		gOffset[i] = gTotalSize;
		gTotalSize += gSizes[i] * gCount[i];
		gAllocated[i] = (QiUInt32*)::malloc((gCount[i]>>5)*sizeof(QiUInt32));
		memset(gAllocated[i], 0, (gCount[i]>>5)*sizeof(QiUInt32));
		gLowestFree[i] = 0;
		gLowestFreeBit[i] = 0;
		gFailedBankAllocations[i] = 0;
	}
	gMem = ::malloc(gTotalSize);
	QI_PRINT("Reserved for small allocations: " + gTotalSize);
	gInitialized = true;
}


void QiAllocator::shutdown()
{
	QiMutexZone lock(gMutex);
	QI_ASSERT(gInitialized, "Not yet initialized");
	for(int i=0; i<COUNT; i++)
		::free(gAllocated[i]);
	::free(gMem);
	gMem = NULL;
	gTotalSize = 0;
	gInitialized = false;
}


void* QiAllocator::alloc(int size)
{
	if (!gInitialized)
	{
		gSystemAllocations++;
		return ::malloc(size);
	}

	int bank = getBankFromSize(size);
	if (bank != -1)
	{
		QiMutexZone lock(gMutex);
		int p = findNextFree(bank);
		if (p != -1)
		{
			//printf("Small alloc: %i (%i)\n", size, p);
			markAsAllocated(bank, p);
			return ((char*)gMem) + gOffset[bank] + p*gSizes[bank];
		}
		else
		{
			gFailedBankAllocations[bank]++;
		}
	}
	//printf("System alloc: %i\n", size);
	gSystemAllocations++;
	return ::malloc(size);
}


void* QiAllocator::realloc(void* ptr, int size)
{
	if (!gInitialized)
		return ::realloc(ptr, size);

	if (ptr == 0)
		return QiAllocator::alloc(size);

	int b = getBankFromAllocation(ptr);
	if (b != -1)
	{
		QiMutexZone lock(gMutex);
		int oldSize = gSizes[b];
		if (size <= oldSize)
			return ptr;
		else
		{
			void* newPtr = QiAllocator::alloc(size);
			memcpy(newPtr, ptr, QiMin(size, oldSize));
			QiAllocator::free(ptr);
			return newPtr;
		}
	}
	else
		return ::realloc(ptr, size);
}


void QiAllocator::free(void* ptr)
{
	if (ptr == NULL)
		return;

	if (!gInitialized)
	{
		gSystemAllocations--;
		::free(ptr);
		return;
	}

	int b = getBankFromAllocation(ptr);
	if (b != -1)
	{
		int p = ((QiUInt32)ptr - ((QiUInt32)gMem+gOffset[b])) / gSizes[b];
		QiMutexZone lock(gMutex);
		markAsFree(b, p);
	}
	else
	{
		gSystemAllocations--;
		::free(ptr);
	}
}


void QiAllocator::printStats()
{
	int total = 0;
	int totalMax = 0;
	for(int bank=0; bank<COUNT; bank++)
	{
		int c = gCount[bank]>>5;
		int bits = 0;
		for(int e=0; e<c; e++)
		{
			printf("%c", QiPopCount(gAllocated[bank][e]) == 0 ? '-' : 'o');
			bits += QiPopCount(gAllocated[bank][e]);
		}
		QI_PRINT("\nBank " + gSizes[bank] + ": \t" + bits + "/" + gCount[bank] + " - Failed: " + gFailedBankAllocations[bank]);
		total += bits;
		totalMax += gCount[bank];		
	}
	QI_PRINT("Current bank allocations: " + total + " / " + totalMax);
	QI_PRINT("System allocations: " + gSystemAllocations);
}


