#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#ifndef WIN32
#include <stddef.h>
#endif

#ifndef NULL
#define NULL 0L
#endif

#ifdef DEBUG
#ifndef QI_DEBUG
#define QI_DEBUG
#endif
#endif

//Disable certain visual studio warnings
#if _MSC_VER
#pragma warning (disable: 4996)
#pragma warning (disable: 4530)
#define QI_WIN32
#endif

typedef int QiInt32;
typedef unsigned int QiUInt32;
typedef short QiInt16;
typedef unsigned short QiUInt16;
typedef char QiInt8;
typedef unsigned char QiUInt8;
typedef float QiFloat32;
typedef double QiFloat64;
typedef long long QiInt64;
typedef unsigned long long QiUInt64;

typedef int QiHash;

#ifdef QI_64_BIT
typedef QiInt64 QiNativeInt;
typedef QiUInt64 QiNativeUInt;
#else
typedef QiInt32 QiNativeInt;
typedef QiUInt32 QiNativeUInt;
#endif

enum QiUninitialized { QI_UNINITIALIZED };


template<class T> inline T QiMin(T a, T b)
{
	return a < b ? a : b; 
}


template<class T> inline T QiMax(T a, T b)
{
		return a > b ? a : b; 
}


template<class T> inline T QiMin(T a, T b, T c)
{
	return QiMin(QiMin(a, b), c);
}


template<class T> inline T QiMax(T a, T b, T c)
{
	return QiMax(QiMax(a, b), c);
}


template<class T> inline T QiClamp(T value, T minValue, T maxValue)
{
	return QiMin(QiMax(value, minValue), maxValue); 
}


template<class T> inline void QiSwap(T& v0, T& v1)
{
	T tmp = v1;
	v1 = v0;
	v0 = tmp;
}


#if defined(_MSC_VER)
#define QI_ALIGN_16 __declspec(align(16))
#define QI_ALIGN_8 __declspec(align(8))
#else
#define QI_ALIGN_16 __align(16)
#define QI_ALIGN_8 __align(8)
#endif

#define QI_SSE QI_ALIGN_16

