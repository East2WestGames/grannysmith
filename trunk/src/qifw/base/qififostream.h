#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qibyteorder.h"
#include "base/qistream.h" 

template <size_t stackSize = 256>
class QiFifoStream : public QiInputStream, public QiOutputStream
{
public:
	inline QiFifoStream(QiByteOrder byteOrder = QI_HOST_BYTE_ORDER, size_t initialBufferSize = stackSize) :
	QiInputStream(byteOrder), QiOutputStream(byteOrder),
	mBuffer(mArr), mBufferSize(stackSize), mReadPos(0), mWritePos(0), 
	mByteCount(0)
	{
		if (initialBufferSize > stackSize)
		{
			mBuffer = (char*)QiAlloc(initialBufferSize);
			mBufferSize = initialBufferSize;
		}
	}
	
	inline ~QiFifoStream()
	{
		if (mBuffer != mArr)
			QiFree(mBuffer);
	}

	inline size_t getSize() const
	{
		return mByteCount;
	}

	inline void clear()
	{
		mByteCount = 0;
		mReadPos = 0;
		mWritePos = 0;
		resetInputStream();
		resetOutputStream();
	}

	inline virtual bool hasMoreData()
	{
		return mReadPos != mWritePos;
	}
	
	inline bool peek(char* buffer, size_t bytes) const
	{
		if (bytes > mByteCount)
			return false;
		for(size_t p=0; p<bytes; p++)
			buffer[p] = mBuffer[(mReadPos+p) % mBufferSize];
		return true;
	}

private:
	inline virtual bool readInternal(char* buffer, size_t bytes)
	{
		if (bytes > mByteCount)
			return false;
		size_t first = QiMin(mBufferSize-mReadPos, bytes);
		if (first > 0)
		{
			memcpy(buffer, mBuffer+mReadPos, first);
			mReadPos += bytes;
		}
		if (first < bytes)
		{
			memcpy(buffer+first, mBuffer, bytes-first);
			mReadPos = bytes-first;
		}
		mByteCount -= bytes;
		return true;
	}
	
	inline virtual bool writeInternal(const char* buffer, size_t bytes)
	{
		if (mByteCount + bytes > mBufferSize)
		{
			//Allocate new buffer
			size_t newBufferSize = (mByteCount + bytes) * 2 + 32;
			char* newBuffer = (char*)QiAlloc(newBufferSize);

			//Copy the old data in two chunks
			size_t first = QiMin(mBufferSize-mReadPos, mByteCount);
			if (first > 0)
				memcpy(newBuffer, mBuffer+mReadPos, first);
			if (first < mByteCount)
				memcpy(newBuffer+first, mBuffer, mByteCount-first);

			//Trash old buffer
			if (mBuffer != mArr)
				QiFree(mBuffer);

			//Set new props
			mBuffer = newBuffer;
			mBufferSize = newBufferSize;
			mReadPos = 0;
			mWritePos = mByteCount;
		}
		//Append data to fifo and wrap around if necessary
		size_t first = QiMin(mBufferSize-mWritePos, bytes);
		if (first > 0)
		{
			memcpy(mBuffer+mWritePos, buffer, first);
			mWritePos += bytes;
		}
		if (first < bytes)
		{
			memcpy(mBuffer, buffer+first, bytes-first);
			mWritePos = bytes-first;
		}
		mByteCount += bytes;
		return true;
	}

	char mArr[stackSize];
	char* mBuffer;
	size_t mBufferSize;
	size_t mReadPos;
	size_t mWritePos;
	size_t mByteCount;
};


