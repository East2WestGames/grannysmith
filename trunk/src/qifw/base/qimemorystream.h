#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qibyteorder.h"
#include "base/qistream.h" 
#include "base/qimem.h" 

#include <string.h>

template <size_t stackSize = 256>
class QiMemoryStream : public QiInputStream, public QiOutputStream
{
public:
	inline QiMemoryStream(QiByteOrder byteOrder = QI_HOST_BYTE_ORDER) :
	QiInputStream(byteOrder), QiOutputStream(byteOrder),
	mBuffer(mArr), mBufferSize(stackSize), mSeekPosRead(0), mSeekPosWrite(0), mByteCount(0)
	{
	}
	
	inline ~QiMemoryStream()
	{
		if (mBuffer != mArr)
			QiFree(mBuffer);
	}

	inline size_t tellRead() const
	{
		return mSeekPosRead;
	}
	
	inline size_t tellWrite() const
	{
		return mSeekPosWrite;
	}
	
	inline void seekRead(size_t pos)
	{
		mSeekPosRead = pos;
	}

	inline void seekWrite(size_t pos)
	{
		mSeekPosWrite = pos;
	}

	inline size_t getSize() const
	{
		return mByteCount;
	}
	
	inline void clear()
	{
		mSeekPosRead = 0;
		mSeekPosWrite = 0;
		mByteCount = 0;
		resetInputStream();
		resetOutputStream();
	}

	inline char* getData()
	{
		return mBuffer;
	}

	inline virtual bool hasMoreData()
	{
		return mSeekPosRead < mByteCount;
	}

	inline void redim(size_t size)
	{
		if (size > mBufferSize)
		{
			mBufferSize = size;
			if (mBuffer == mArr)
			{
				mBuffer = (char*)QiAlloc(mBufferSize);
				memcpy(mBuffer, mArr, mByteCount);
			}
			else
			{
				mBuffer = (char*)QiRealloc(mBuffer, mBufferSize);
			}
		}
		mByteCount=size;
		mSeekPosWrite=size;
	}

private:
	inline virtual bool readInternal(char* buffer, size_t bytes)
	{
		if (mSeekPosRead + bytes > mByteCount)
			return false;
		memcpy(buffer, mBuffer+mSeekPosRead, bytes);
		mSeekPosRead += bytes;
		return true;
	}
	
	inline virtual bool writeInternal(const char* buffer, size_t bytes)
	{
		if (mSeekPosWrite + bytes > mBufferSize)
		{
			mBufferSize = (mSeekPosWrite + bytes) * 2 + 32;
			if (mBuffer == mArr)
			{
				mBuffer = (char*)QiAlloc(mBufferSize);
				memcpy(mBuffer, mArr, mByteCount);
			}
			else
			{
				mBuffer = (char*)QiRealloc(mBuffer, mBufferSize);
			}
		}
		memcpy(mBuffer+mSeekPosWrite, buffer, bytes);
		mByteCount += bytes;
		mSeekPosWrite += bytes;
		return true;
	}

	char* mBuffer;
	char mArr[stackSize];
	size_t mBufferSize;
	size_t mSeekPosRead;
	size_t mSeekPosWrite;
	size_t mByteCount;
};



