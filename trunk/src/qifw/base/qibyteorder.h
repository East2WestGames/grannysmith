#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"
 
enum QiByteOrder
{
	QI_BIG_ENDIAN,
	QI_LITTLE_ENDIAN
};

#ifdef QI_ARCH_PPC
const QiByteOrder QI_HOST_BYTE_ORDER = QI_BIG_ENDIAN;
const QiByteOrder QI_NOT_HOST_BYTE_ORDER = QI_LITTLE_ENDIAN;
#else
const QiByteOrder QI_HOST_BYTE_ORDER = QI_LITTLE_ENDIAN;
const QiByteOrder QI_NOT_HOST_BYTE_ORDER = QI_BIG_ENDIAN;
#endif

const QiByteOrder QI_NETWORK_BYTE_ORDER = QI_BIG_ENDIAN;

//Generic function for swapping endianess of arbitrary type
template<class T> 
inline T volatile QiSwapByteOrder(T volatile value) 
{
	T volatile tmp;
	for (size_t i=0; i<sizeof(T); i++) 
		((volatile char*)&tmp)[i] = ((volatile char*)&value)[sizeof(T)-1-i];
	return tmp;
}

