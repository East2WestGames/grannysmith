/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qistring.h"
#include "base/qimem.h"
#include "base/qimath.h"
#include "base/qidebug.h"

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

inline static bool isWhiteSpace(char c)
{
	return c == ' ' || c == '\t' || c == '\n';
}

inline void QiString::grow(int maxLength)
{
	if (maxLength+1 <= mSize) 
		return;

	//New size
	mSize=maxLength * 2 + 1;

	//If already heap allocator, realloc
	if (mData)
	{
		mData = (char*)QiRealloc(mData, mSize);
		QI_ASSERT(mData, "Out of memory");
	}
	else
	{
		mData = (char*)QiAlloc(mSize, "QiString::Data");
		QI_ASSERT(mData, "Out of memory");
		strcpy(mData, mInternalData);
	}
}

void QiString::assign(const char * s)
{
	if(s==NULL) 
		s="";
	mLength = (int)strlen(s);
	grow(mLength);
	strcpy(getData(), s);
}

void QiString::assign(const QiString& s)
{
	mLength = s.mLength;
	grow(mLength);
	strcpy(getData(), s.c_str());
}

QiString::QiString()
{
	mSize = QI_INTERNAL_STRING_SIZE;
	mData = NULL;
	mInternalData[0] = '\0';
	mLength = 0;
}

QiString::QiString(const QiString& string)
{
	mSize = QI_INTERNAL_STRING_SIZE;
	mData = NULL;
	mInternalData[0] = '\0';
	mLength = 0;
	assign(string);
}

QiString::QiString(const char * string)
{
	mSize = QI_INTERNAL_STRING_SIZE;
	mData = NULL;
	mInternalData[0] = '\0';
	mLength = 0;
	assign(string);
}

QiString::QiString(char ch)
{
	mSize = QI_INTERNAL_STRING_SIZE;
	mData = NULL;
	grow(1);
	mInternalData[0]=ch;
	mInternalData[1]='\0';
	mLength = 1;
}

QiString::~QiString()
{
	if(mData) 
		QiFree(mData);
}

QiString& QiString::operator=(const char* string)
{
	assign(string);
	return *this;
}

QiString& QiString::operator=(const QiString& string)
{
	assign(string);
	return *this;
}

QiString QiString::operator+(const QiString& string) const
{
	QiString tmp(*this);
	tmp+=string.c_str();
	return tmp;
}

QiString QiString::operator+(const char* string) const
{
	QiString tmp(*this);
	tmp+=string;
	return tmp;
}

QiString QiString::operator+(char ch) const
{
	QiString tmp(*this);
	tmp+=ch;
	return tmp;
}

QiString QiString::operator+(int i) const
{
	char tmp[16];
	sprintf(tmp, "%i", i);
	return *this + QiString(tmp);
}

QiString QiString::operator+(long long i) const
{
	char tmp[32];
#ifdef QI_WIN32
	sprintf(tmp, "%I64d", i);
#else
	sprintf(tmp, "%lld", i);
#endif
	return *this + QiString(tmp);
}

QiString QiString::operator+(float f) const
{
	return *this + formatFloat(f);
}

QiString QiString::operator+(void* ptr) const
{
	char tmp[16];
	sprintf(tmp, "%p", ptr);
	return *this + QiString(tmp);
}

QiString& QiString::operator+=(const QiString& string)
{
	mLength += string.mLength;
	grow(mLength);
	strcat(getData(), string.c_str());
	return *this;
}

QiString& QiString::operator+=(const char* string)
{
	mLength += (int)strlen(string);
	grow(mLength);
	strcat(getData(), string);
	return *this;
}

QiString& QiString::operator+=(char ch)
{
	grow(mLength+1);
	char* data = getData();
	data[mLength]=ch;
	data[mLength+1]='\0';
	mLength++;
	return *this;
}

QiString& QiString::operator+=(int i)
{
	char tmp[32];
	sprintf(tmp, "%i", i);
	return operator+=(tmp);
}

QiString& QiString::operator+=(long long i)
{
	char tmp[32];
#ifdef QI_WIN32
	sprintf(tmp, "%I64d", i);
#else
	sprintf(tmp, "%lld", i);
#endif
	return operator+=(tmp);
}

QiString& QiString::operator+=(float f)
{
	return operator+=(formatFloat(f));
}

QiString& QiString::operator+=(void* ptr)
{
	char tmp[32];
	sprintf(tmp, "%p", ptr);
	return operator+=(tmp);
}

void QiString::trim()
{
	char* data = getData();

	//Find beginning
	int i = 0;
	while(isWhiteSpace(data[i]) && i < mLength) 
		i++;
	//Find end
	int j = mLength-1;
	while(isWhiteSpace(data[j]) && j>i && j>0) 
		j--;
	//Terminate at end
	data[j+1]='\0';
	//Create tmp string and assign
	assign(data+i);
}


int QiString::compareTo(const QiString& string, bool caseSensitive) const 
{
	return compareTo(string.c_str(), caseSensitive);
}

int QiString::compareTo(const char* string, bool caseSensitive) const 
{
	const char* data = getData();
	if (caseSensitive)
	{
		return strcmp(data,string);
	}
	else
	{
		const char *first = data;
		const char *second = string;
		while (*first && *second) 
		{ 
			if (toupper((int)*first) != toupper((int)*second)) 
				break; 
			first++; 
			second++; 
		} 
		return toupper((int)*first) - toupper((int)*second); 
	}
}

bool QiString::startsWith(const QiString& string, bool caseSensitive) const 
{
	return startsWith(string.c_str(), caseSensitive);
}

bool QiString::startsWith(const char* string, bool caseSensitive) const 
{
	return getIndexOf(string, 0, caseSensitive) == 0;
}

bool QiString::endsWith(const QiString& string, bool caseSensitive) const 
{
	return endsWith(string.c_str(), caseSensitive);
}

bool QiString::endsWith(const char* string, bool caseSensitive) const 
{
	int p = mLength - strlen(string);
	if (p<0)
		return false;
	return getLastIndexOf(string, caseSensitive) == p;
}

bool QiString::contains(const QiString& string, bool caseSensitive) const 
{
	return contains(string.c_str(), caseSensitive);
}

bool QiString::contains(const char* string, bool caseSensitive) const 
{
	return getIndexOf(string, 0, caseSensitive) != -1;
}

int QiString::getIndexOf(const QiString& string, int start, bool caseSensitive) const
{
	return getIndexOf(string.c_str(), start, caseSensitive);
}

int QiString::getIndexOf(const char* string, int start, bool caseSensitive) const
{
	const char* data = getData();
	int strLength2 = strlen(string);
	for(int i=start; i<=mLength-strLength2; i++)
	{
		int j;
		for(j=0; j<strLength2; j++)
		{
			char c1 = data[i+j];
			char c2 = string[j];
			if ((caseSensitive && c1 != c2) || (!caseSensitive && tolower(c1) != tolower(c2)))
				break;
		}
		if (j==strLength2)
			return i;
	}
	return -1;
}

int QiString::getLastIndexOf(const QiString& string, bool caseSensitive) const
{
	return getLastIndexOf(string.c_str(), caseSensitive);
}

int QiString::getLastIndexOf(const char* string, bool caseSensitive) const
{
	const char* data = getData();
	int strLength2 = strlen(string);
	for(int i=mLength-strLength2; i>=0; i--)
	{
		int j;
		for(j=0; j<strLength2; j++)
		{
			char c1 = data[i+j];
			char c2 = string[j];
			if ((caseSensitive && c1 != c2) || (!caseSensitive && tolower(c1) != tolower(c2)))
				break;
		}
		if (j==strLength2)
			return i;
	}
	return -1;
}


QiString QiString::substring(int start) const
{
	return substring(start, mLength);
}


QiString QiString::substring(int start, int end) const
{
	QiString tmp(*this);
	if (start>end)
		return QiString();
	char* tmpData = tmp.getData();
	memmove(tmpData, tmpData+start, end-start);
	tmpData[end-start]=0;
	tmp.mLength = end-start;
	return tmp;
}


QiString QiString::replace(const char* search, const char* replace, bool caseSensitive) const
{
	QiString result;

	int pos=0, lastPos=0;
	int l = (int)strlen(search);

	while( (pos=getIndexOf(search, lastPos, caseSensitive)) >= 0) 
	{
		//append data before
		if(pos != lastPos)
			result += substring(lastPos, pos);

		//replace and update lastPos
		result += replace;
		lastPos = pos + l;
	}
	if (lastPos < mLength)
		result += substring(lastPos, mLength);

	return result;
}

bool QiString::isNumerical() const
{
	if (mLength == 0)
		return false;
	const char* data = getData();
	for(int i=0; i<mLength; i++)
	{
		if (data[i] < '0' || data[i] > '9')
			return false;
	}
	return true;
}


QiString QiString::toLowerCase() const
{
	QiString tmp(*this);
	char* tmpData = tmp.getData();
	for(int i=0; i<tmp.mLength; i++)
		tmpData[i] = tolower(tmpData[i]);
	return tmp;
}


QiString QiString::toUpperCase() const
{
	QiString tmp(*this);
	char* tmpData = tmp.getData();
	for(int i=0; i<tmp.mLength; i++)
		tmpData[i] = toupper(tmpData[i]);
	return tmp;
}


int QiString::getWordCount() const
{
	int wordCount = 0;
	int begin = 0;
	int end = 0;
	while(end < mLength)
	{
		//Skip until next whitespace
		begin = end;
		while(begin < mLength && isWhiteSpace((*this)[begin]))
			begin++;
			
		if (begin == mLength)
			break;
		
		//Read until next whitespace
		end = begin;
		while(end < mLength && !isWhiteSpace((*this)[end]))
			end++;

		wordCount++;
	}
	return wordCount;			
}	

QiString QiString::getWord(int index) const
{
	//Skip initial whitespace
	int begin = 0;
	while(begin < mLength && isWhiteSpace((*this)[begin]))
		begin++;
	
	if (begin == mLength)
		return "";

	int end;
	for(int i=0; i<=index; i++)
	{
		//Read until next whitespace
		end = begin;
		while(end < mLength && !isWhiteSpace((*this)[end]))
			end++;
			
		//This is the word we're looking fo
		if (i == index && begin < mLength && end <= mLength)
			return substring(begin, end);
			
		//Skip until next whitespace
		begin = end;
		while(begin < mLength && isWhiteSpace((*this)[begin]))
			begin++;
	}
	return "";
}	


char QiString::toChar() const
{
	return (char)atoi(getData());
}

short QiString::toShort() const
{
	return (short)atoi(getData());
}

int QiString::toInt() const
{
	return (int)atoi(getData());
}

QiInt64 QiString::toInt64() const
{
#ifdef QI_WIN32
	return _atoi64(getData());
#else
	return atoll(getData());
#endif
}

float QiString::toFloat() const
{
	return (float)atof(getData());
}


QiHash QiString::getHash() const
{
	const char* data = getData();
	int c = 0;
	QiHash hash = 0;
	while(data[c]!=0)
	{
		hash = (data[c]*hash)^hash + data[c] * (c+59) + data[c] * (hash * 3);
		c++;
	}
	return hash;
}


QiString QiString::formatFloat(float number, int maxDecimals, bool allowScientific)
{
	if (number == 0.0f)
		return QiString("0.0");
	if (QiAbs(number) < 0.000001f || QiAbs(number) > 10000000)
	{
		char tmp[100];
		sprintf(tmp, "%e", number);
		return QiString(tmp);
	}

	if (number != 0.0f)
		maxDecimals -= QiClamp((int)QiLog10(QiAbs(number)), -8, 0);
	char format[100];
	sprintf(format, "%%.%if", maxDecimals);
	char tmp[100];
	sprintf(tmp, format, number);
	QiString result(tmp);
	int i = result.getLength()-1;
	while(i>1 && result[i]=='0' && result[i-1]!='.')
		i--;
	return result.substring(0,i+1);
}


QiString operator+(const char* string1, const QiString& string2)
{
	return QiString(string1)+=string2;
}


bool QiString::matchExpression(const QiString& expression, bool caseSensitive)
{
	QiString s = *this;
	QiString e = expression;
	if (!caseSensitive)
	{
		s = s.toLowerCase();
		e = e.toLowerCase();
	}

	int pe = 0;
	while (s.getLength() > 0 || e.getLength() > 0)
	{
		if (e == "*")
			return true;
		int index = e.getIndexOf("*", pe);
		if (index == -1)
			return s == e;
		else if (index == 0)
		{
			int end = e.getIndexOf("*", pe+1);
			if (end == -1)
				end = e.getLength();
			QiString part = e.substring(index+1, end);
			int si = s.getIndexOf(part);
			if (si == -1)
				return false;
			s = s.substring(si);
			e = e.substring(1);
		}
		else
		{
			QiString ss = s.substring(0, index);
			QiString se = e.substring(0, index);
			if (ss != se)
				return false;
			s = s.substring(index);
			e = e.substring(index);
		}
	}
	return true;
}

