#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"
#include "base/qidebug.h"
#include "base/qimem.h"
#include "base/qiarray.h"
#include "base/qisort.h"

#include <string.h>

template <class T>
class QiSet
{
public:
	class Iterator
	{
	public:
		Iterator(const QiSet<T>& set) :
		mSet(set), mPos(0)
		{
		}
		
		inline bool hasNext() const
		{
			return mPos < mSet.getCount();
		}

		inline const T& next()
		{
			QI_ASSERT(hasNext(), "Reached end of set");
			return mSet[mPos++];
		}
		
		inline const T& getCurrent() const
		{
			QI_ASSERT(hasNext(), "Reached end of set");
			return mSet[mPos];
		}
		
		inline int getCount() const
		{
			return mSet.getCount();
		}

		inline void reset()
		{
			mPos = 0;
		}
		
	protected:
		const QiSet<T>& mSet;
		int mPos;
	};

	inline QiSet() :
	mCount(0), mSize(0), mData(NULL)
	{
	}

	inline QiSet(const T* arr, int count, bool sortedAndUnique = false) :
	mCount(count), mSize(0), mData(NULL)
	{
		addAll(arr, count, sortedAndUnique);
	}

	template <class Iterator>
	inline QiSet(Iterator iterator, bool sortedAndUnique = false) :
	mCount(0), mSize(0), mData(NULL)
	{
		addAll(iterator, sortedAndUnique);
	}
	
	inline ~QiSet()
	{
		if (mData)
			QiFree(mData);
	}
	
	inline void makeSet()
	{
		QiSort(mData, mCount, QiComparator<T>());
		if (mCount > 0)
		{
			int c=0;
			for(int i=1; i<mCount; i++)
			{
				if (mData[c] != mData[i])
				{
					c++;
					if (c != i)
						mData[c] = mData[i];
				}
			}
			mCount = c+1;
		}
	}

	inline const T& operator[](int i) const
	{
		return mData[i];
	}

	inline T& operator[](int i)
	{
		return mData[i];
	}
	
	inline T& first()
	{
		QI_ASSERT(mCount>0, "Index out of bound");
		return mData[0];
	}

	inline const T& first() const
	{
		QI_ASSERT(mCount>0, "Index out of bound");
		return mData[0];
	}

	inline T& last()
	{
		QI_ASSERT(mCount>0, "Index out of bound");
		return mData[mCount-1];
	}	

	inline const T& last() const
	{
		QI_ASSERT(mCount>0, "Index out of bound");
		return mData[mCount-1];
	}	

	inline void add(const T& object)
	{
		if (contains(object))
			return;
		if (mSize<=mCount) 
			grow(mSize*2+1);
		mData[mCount++] = object;
		makeSet();
	}

	template <class Iterator>
	inline void addAll(const Iterator iterator, bool sortedAndUnique = false)
	{
		grow(mCount + iterator.getCount());
		while(iterator.hasNext())
			mData[mCount++] = iterator.next();
		if (!sortedAndUnique)
			makeSet();
	}
	
	inline void addAll(const T* arr, int count, bool sortedAndUnique = false)
	{
		grow(count);
		memcpy(mData, arr, sizeof(T)*count);
		mCount = count;
		if (!sortedAndUnique)
			makeSet();
	}

	template <class SetIterator>
	inline void intersect(SetIterator iterator)
	{
		int count = 0;
		int i = 0;
		while(iterator.hasNext() && i < mCount)
		{
			const T& next = iterator.next();
			while(i < mCount && mData[i] < next)
				i++;
			if (i < mCount && mData[i] == next)
				mData[count++] = next;
		}
		mCount = count;
	}
	
	template <class SetIterator>
	inline void subtract(SetIterator iterator)
	{
		int count = 0;
		for(int i = 0; i<mCount; i++)
		{		
			while(iterator.hasNext() && iterator.getCurrent()<mData[i])
				iterator.next();
			if (!iterator.hasNext() || iterator.getCurrent() != mData[i])
				mData[count++] = mData[i];
		}
		mCount = count;
	}

	inline bool contains(const T& object) const
	{
		return getIndexOf(object)!=-1;
	}
	
	inline int getIndexOf(const T& object) const
	{
		int lower=0;
		int upper=mCount-1;
		while(lower <= upper)
		{
			int piv = (lower+upper)/2;
			if (mData[piv] == object)
				return piv;
			if (mData[piv] < object)
				lower = piv+1;
			else
				upper = piv-1;
		}
		return -1;
	}
	
	inline int getCount() const
	{
		return mCount;
	}
	
	inline void clear()
	{
		mCount = 0;
	}
	
	inline Iterator getIterator() const
	{
		return Iterator(*this);
	}
	
	inline bool isEmpty() const
	{
		return mCount == 0;
	}

protected:	
	inline void grow(int size)
	{
		if (size > mSize)
		{
			if (mData == NULL)
			{
				mData = (T*)QiAlloc(size*sizeof(T), "QiSet::Data");
			}
			else if ((char*)mData == ((char*)this)+sizeof(QiArray<T>))
			{
				//This is quite ugly. Detect wether the allocation is immediately after
				//this object. In that case, this is an inplace array and initial data
				//should not be freed.
				T* newData = (T*)QiAlloc(size*sizeof(T), "QiSet::Data");
				if (newData)
					memcpy(newData, mData, mCount * sizeof(T));
				mData = newData;
			}
			else
			{
				mData = (T*)QiRealloc(mData, size*sizeof(T));
			}
			QI_ASSERT(mData!=NULL, "Out of memory");
			mSize = size;
		}
	}

	int mCount;
	int mSize;
	T* mData;
};


template <class T, int size>
class QiSetInplace : public QiSet<T>
{
public:
	inline QiSetInplace() : QiSet<T>()
	{
		QiSet<T>::mData = (T*)mArr;
		QiSet<T>::mSize = size;
	}

	inline QiSetInplace(const T* arr, int count, bool sortedAndUnique = false) :
	QiSet<T>()
	{
		QiSet<T>::mData = (T*)mArr;
		QiSet<T>::mSize = size;
		addAll(arr, count, sortedAndUnique);
	}

	template <class Iterator>
	inline QiSetInplace(const Iterator iterator, bool sortedAndUnique = false) :
	QiSet<T>()
	{
		QiSet<T>::mData = (T*)mArr;
		QiSet<T>::mSize = size;
		addAll(iterator, sortedAndUnique);
	}
	
	inline ~QiSetInplace()
	{
		if (QiSet<T>::mData == (T*)mArr)
			QiSet<T>::mData = NULL;
	}

protected:
	char mArr[size * sizeof(T)];
};


