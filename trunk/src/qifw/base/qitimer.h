#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"

class QiTimeStamp
{
public:
	QiUInt64 sec;
	QiUInt64 usec;
};

class QiTimer
{
public:
	QiTimer();
	void reset();
	long long getTicks() const;
	static long long getTicksPerSecond();
	float getTime() const;

public:
	QiTimeStamp mStart;
};



