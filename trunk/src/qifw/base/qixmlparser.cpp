/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qixmlparser.h"
#include "base/qimemorystream.h"
#include "base/qidebug.h"
#include "base/qiarray.h"

#include "base/tinyxml/tinyxml.h"

class QiXmlParserImpl
{
public:
	QiXmlParserImpl() : mNode(NULL)
	{
	}

	TiXmlDocument mDoc;
	TiXmlElement* mNode;
	QiArray<TiXmlElement*> mStack;
	QiString mError;
};

QiXmlParser::QiXmlParser()
{
	mImpl = QI_NEW QiXmlParserImpl();
}

QiXmlParser::QiXmlParser(QiInputStream& stream, int length)
{
	mImpl = QI_NEW QiXmlParserImpl();
	parse(stream, length);
}

QiXmlParser::~QiXmlParser()
{
	QI_DELETE(mImpl);
}

bool QiXmlParser::parse(QiInputStream& stream, int length)
{
	mImpl->mNode = NULL;
	mImpl->mDoc.Clear();
	mImpl->mDoc.ClearError();

	QiMemoryStream<4096> mem;
	mem.writeBuffer(stream, length);
	mem.writeInt8(0);
	mImpl->mDoc.Parse(mem.getData());
	if (mImpl->mDoc.Error())
	{
		mImpl->mError = mImpl->mDoc.ErrorDesc();
		return false;
	}
	
	reset();
	return true;
}

void QiXmlParser::reset()
{
	mImpl->mStack.clear();
	mImpl->mNode = mImpl->mDoc.FirstChildElement();
}

bool QiXmlParser::isValid()
{
	return mImpl->mNode != NULL;
}

bool QiXmlParser::next()
{
	mImpl->mNode = mImpl->mNode->NextSiblingElement();
	return mImpl->mNode != NULL;
}

bool QiXmlParser::enter()
{
	if (mImpl->mNode == NULL)
		return false;
	push();
	mImpl->mNode = mImpl->mNode->FirstChildElement();
	return true;
}

bool QiXmlParser::leave()
{
	return pop();
}

bool QiXmlParser::select(const QiString& name)
{
	TiXmlElement* curr = NULL;
	if (mImpl->mNode == NULL)
		curr = mImpl->mDoc.FirstChildElement();
	else
		curr = mImpl->mNode->Parent()->FirstChildElement();
	while(curr)
	{
		if (name == curr->Value())
		{
			mImpl->mNode = curr;
			return true;
		}
		curr = curr->NextSiblingElement();
	}
	return false;
}

bool QiXmlParser::push()
{
	if (mImpl->mNode == NULL)
		return false;
	mImpl->mStack.add(mImpl->mNode);
	return true;
}

bool QiXmlParser::pop()
{
	if (mImpl->mStack.isEmpty())
		return false;
	mImpl->mNode = mImpl->mStack.last();
	mImpl->mStack.removeElementAt(mImpl->mStack.getCount()-1);
	return true;
}

QiString QiXmlParser::getName()
{
	if (mImpl->mNode)
		return mImpl->mNode->Value();
	return "";
}

QiString QiXmlParser::getValue()
{
	if (mImpl->mNode)
	{
		const char* text = mImpl->mNode->GetText();
		if (text)
			return QiString(text);
	}
	return "";
}

int QiXmlParser::getAttributeCount()
{
	if (!mImpl->mNode)
		return 0;
	const TiXmlAttribute* attrib = mImpl->mNode->FirstAttribute();
	int count = 0;
	while(attrib)
	{
		count++;
		attrib = attrib->Next();
	}
	return count;
}

QiString QiXmlParser::getAttributeName(int index)
{
	if (!mImpl->mNode)
		return "";
	const TiXmlAttribute* attrib = mImpl->mNode->FirstAttribute();
	int count = 0;
	while(attrib)
	{
		if (count == index)
			return attrib->Name();
		count++;
		attrib = attrib->Next();
	}
	return "";
}

QiString QiXmlParser::getAttribute(int index)
{
	if (!mImpl->mNode)
		return "";
	const TiXmlAttribute* attrib = mImpl->mNode->FirstAttribute();
	int count = 0;
	while(attrib)
	{
		if (count == index)
			return attrib->Value();
		count++;
		attrib = attrib->Next();
	}
	return "";
}

QiString QiXmlParser::getAttribute(const QiString& name)
{
	if (!mImpl->mNode)
		return "";
	const char* value = mImpl->mNode->Attribute(name);
	if (value)
		return value;
	return "";
}

QiString QiXmlParser::getError()
{
	return mImpl->mError;
}

