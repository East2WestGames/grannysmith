#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qidebug.h"

template <int bitCount>
class QiBitfield
{
public:
	inline QiBitfield()
	{
		for(int i=0; i<bitCount; i++)
			mBitfield[i] = 0;
	}
	
	inline bool operator==(const QiBitfield& other) const
	{
		for(int i=0; i<(int)sizeof(mBitfield); i++)
			if (mBitfield[i] != other.mBitfield[i])
				return false;
		return true;
	}

	inline bool operator!=(const QiBitfield& other) const
	{
		for(int i=0; i<(int)sizeof(mBitfield); i++)
			if (mBitfield[i] != other.mBitfield[i])
				return true;
		return false;
	}

	inline void set(int bit)
	{
		QI_ASSERT(bit>=0 && bit < bitCount, "Index out of bounds");
		mBitfield[bit/8] |= 1 << (bit%8);
	}

	inline void clear(int bit)
	{
		QI_ASSERT(bit>=0 && bit < bitCount, "Index out of bounds");
		mBitfield[bit/8] &= ~(1 << (bit%8));
	}

	inline bool has(int bit) const
	{
		QI_ASSERT(bit>=0 && bit < bitCount, "Index out of bounds");
		return ((mBitfield[bit/8] >> (bit%8)) & 1) == 1;
	}
	
	inline bool operator[](int bit) const
	{
		QI_ASSERT(bit>=0 && bit < bitCount, "Index out of bounds");
		return has(bit);
	}

protected:		
	char mBitfield[bitCount/8 + (bitCount%8 != 0 ? 1 : 0)];
};
