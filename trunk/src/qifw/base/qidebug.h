#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"
#include "base/qistring.h"

void QiAssert();


class QiDebugStream
{
public:
	virtual ~QiDebugStream()
	{
	}
	
	inline void print(const QiString& str)
	{
		print(str.c_str()); 
	}

protected:
	virtual void print(const char *string) = 0;
};


class QiDebug
{
public:
	static void setPrintStream(QiDebugStream* stream);
	static QiDebugStream* getPrintStream();
	
	static void setWarningStream(QiDebugStream* stream);
	static QiDebugStream* getWarningStream();
	
	static void setErrorStream(QiDebugStream* stream);
	static QiDebugStream* getErrorStream();
	
private:
	static QiDebugStream* sPrintStream;
	static QiDebugStream* sWarningStream;
	static QiDebugStream* sErrorStream;
};

#define QI_PRINT(msg) {if (QiDebug::getPrintStream()) QiDebug::getPrintStream()->print(QiString() + msg + "\n");}

#ifdef QI_DEBUG
#define QI_WARNING(msg) {if (QiDebug::getWarningStream()) QiDebug::getWarningStream()->print(QiString() + __FILE__ + ":" + __LINE__ + ": "  + msg + "\n");}
#else
#define QI_WARNING(msg) (void(0))
#endif

#ifdef QI_DEBUG
#define QI_ERROR(msg) {if (QiDebug::getErrorStream()) QiDebug::getErrorStream()->print(QiString() + __FILE__ + ":" + __LINE__ + ": " + msg + "\n");}
#else
#define QI_ERROR(msg) (void(0))
#endif

#ifdef QI_DEBUG
#define QI_ASSERT(cond, msg) {if(!(cond)){QI_ERROR(QiString() + msg); QiAssert();}}
#else
#define QI_ASSERT(cond, msg) (void(0))
#endif

