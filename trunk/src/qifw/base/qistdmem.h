/* Qi Framework. Copyright 2007-2012 Dengu AB */

#pragma  once

#include <stdlib.h>

void* QiStdAlloc(size_t size);
void* QiStdCAlloc(size_t count, size_t size);
void* QiStdRealloc(void* ptr, size_t size);
void QiStdFree(void* ptr);

