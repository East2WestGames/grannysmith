#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiarray.h"
#include "base/qithread.h"
#include "base/qisync.h"

const int QI_THREAD_POOL_MAX_JOBS = 512;

class QiTask;
class QiJob
{
public:
	QiTask* mTask;
	unsigned int mJobId;
};

class QiTaskDispatcher
{
	friend class QiTask;

protected:
	class Worker : public QiThread
	{
		friend class QiTaskDispatcher;
	public:
		Worker(QiTaskDispatcher* dispatcher);
		virtual ~Worker();

	protected:
		virtual void run();
		void notify() { mCondition.signal(); }

		QiTask* mTask;
		QiTaskDispatcher* mDispatcher;
		QiCondition mCondition;
		volatile unsigned int mWaiting;
		volatile unsigned int mCurrentJobId;
	};

public:
	QiTaskDispatcher();
	QiTaskDispatcher(int threadCount);
	virtual ~QiTaskDispatcher();

	bool init(int threadCount);
	void shutdown();

	int add(QiTask* task);
	void clear();
	
	void waitForJob(int job);
	void waitForAll();
	bool isFinished();

	void setPerformanceMode(bool enabled);

protected:
	virtual Worker* createWorkerThread();
	virtual void destroyWorkerThread(Worker* thread);
	void signal() { mCondition.signal(); }

	void processJobOrSleep();
	bool getNextJob(QiJob& job);

	QiMutex mMutex;
	QiCondition mCondition;
	QiCondition mDone;
	int mRunningTasks;

	QiTask* mTasks[QI_THREAD_POOL_MAX_JOBS];
	QiJob mJobs[QI_THREAD_POOL_MAX_JOBS];
	QiArray<Worker*> mWorkers;

	QiSemaphore mJobAvailable;
	bool mInited;
	bool mPerformanceMode;
	int mDummy;
	int mSpinWaitCount;
	unsigned long int mNextJobId;
	volatile unsigned long mCurrentJob;
	volatile unsigned long mJobCount;
	volatile unsigned long mRun;
	volatile unsigned long mCompletedJobs;
};



class QiTask
{
	friend class QiTaskDispatcher;
	friend class QiTaskDispatcher::Worker;
public:
	enum State
	{
		DETACHED,
		SCHEDULED,
		RUNNING,
		FINISHED,
		ABORTED,
		FAILED
	};

	QiTask();	
	virtual ~QiTask();

	bool isProcessed();
	State getState();
	void abort();
	void wait();
	void setPriority(float priority) { mPriority = priority; }
	float getPriority() { return mPriority; }

protected:
	virtual bool onExecute() = 0;
	virtual void onAbort() {}
	virtual void onProcessed() {}

	bool isAborted();

	void schedule(QiTaskDispatcher* dispatcher);
	void execute();
	void detach();
	QiTaskDispatcher::Worker* getWorker();

private:
	State mState;
	QiTaskDispatcher* mDispatcher;
	QiTaskDispatcher::Worker* mWorker;
	bool mAborted;
	float mPriority;
	bool mWaiting;
};


