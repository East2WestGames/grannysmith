/* Qi Framework. Copyright 2007-2012 Dengu AB */


#include "base/qitaskdispatcher.h"
#include "base/qimath.h"
#include "base/qisystem.h"


//Increment addr and wrap around at count
inline unsigned int InterlockedIncrementWrap(volatile unsigned long* addr, unsigned int count)
{
	volatile unsigned long current;
	volatile unsigned long next;
	do
	{
		current = *addr;
		next = (*addr+1) % count;
	} while(QiInterlockedCompareExchange(addr, next, current) != current);
	return current;
}


QiTask::QiTask() : mState(DETACHED), mDispatcher(NULL), mWorker(NULL), mAborted(false), mPriority(0.0f), mWaiting(false)
{
}


QiTask::~QiTask()
{
}


bool QiTask::isProcessed()
{
	return mState == FINISHED || mState == ABORTED || mState == FAILED;
}


QiTask::State QiTask::getState()
{
	return mState;
}


void QiTask::abort()
{
	mAborted = true;
	onAbort();
}


void QiTask::wait()
{
	mWaiting = true;
	while(!isProcessed())
		QiThread::yield();
	mWaiting = false;
}


bool QiTask::isAborted()
{
	return mAborted;
}


void QiTask::schedule(QiTaskDispatcher* dispatcher)
{
	mState = SCHEDULED;
	mDispatcher = dispatcher;
}


//Execute task. This method is always called by a worker thread.
void QiTask::execute()
{
	bool status = onExecute();

	if (mAborted)
		mState = ABORTED;
	else
		mState = status ? FINISHED : FAILED;
	mWorker = NULL;

	onProcessed();
}


//Detach task from dispatcher. This method is always called by the dispatcher.
void QiTask::detach()
{
	mDispatcher = NULL;
	mState = DETACHED;
	mAborted = false;
}


//Detach task from dispatcher. This method is always called by the dispatcher.
QiTaskDispatcher::Worker* QiTask::getWorker()
{
	return mWorker;
}


////////////////////////////////////////////////////////////////////////////////////


QiTaskDispatcher::Worker::Worker(QiTaskDispatcher* dispatcher) : 
mTask(NULL), mDispatcher(dispatcher), mWaiting(0), mCurrentJobId(0)
{
}


QiTaskDispatcher::Worker::~Worker()
{
}

void QiTaskDispatcher::Worker::run()
{
	QiSystem::setFpu();

	while(!shouldQuit())
	{
		if (mDispatcher->mCurrentJob == mDispatcher->mJobCount)
		{
			//No more work to do, wait for signal from dispatcher
			mWaiting = 1;
			QiMemoryBarrier();
			int spinWaitCount = 0;
			while(mDispatcher->mCurrentJob == mDispatcher->mJobCount)
			{
				//Update current job id in case someone is waiting
				mCurrentJobId = mDispatcher->mCompletedJobs;
				QiMemoryBarrier();
				if (spinWaitCount++ < 512)
				{
					QiPause();
				}
				else
				{
					QiThread::yield();
					spinWaitCount = 0;
				}
				if (!mDispatcher->mPerformanceMode)
				{
					mDispatcher->mJobAvailable.wait();
				}
			}
			QiMemoryBarrier();
			mWaiting = 0;
		}

		//Get the best available task. This will also mark the task as running
		//and assign this worker thread.
		//Execute task or wait for signal
		QiJob job;
		if (mDispatcher->getNextJob(job))
		{
			mCurrentJobId = job.mJobId;
			QiMemoryBarrier();
			job.mTask->onExecute();
			QiMemoryBarrier();
			QiInterlockedIncrement(&mDispatcher->mCompletedJobs);
		}
	}
}


///////////////////////////////////////////////////////////////////////////////////


QiTaskDispatcher::QiTaskDispatcher() : mInited(false), mCurrentJob(0), mJobCount(0), mCompletedJobs(0)
{
}


QiTaskDispatcher::QiTaskDispatcher(int threadCount)  : mInited(false)
{
	init(threadCount);
}


QiTaskDispatcher::~QiTaskDispatcher()
{
	if (mInited)
		shutdown();
}


bool QiTaskDispatcher::init(int threadCount)
{
	QiMemoryBarrier();

	for(int i=0; i<threadCount; i++)
	{
		Worker* w = createWorkerThread();
		mWorkers.add(w);
		w->start();
	}
	mJobAvailable.init(0, 65535);
	mInited = true;
	mJobCount = 0;
	mCurrentJob = 0;
	mPerformanceMode = false;
	mNextJobId = 0;
	mSpinWaitCount = 0;

	QiMemoryBarrier();
	mRun = 0;
	return true;
}


void QiTaskDispatcher::shutdown()
{
}


void QiTaskDispatcher::setPerformanceMode(bool enabled)
{
	mPerformanceMode = enabled;
	if (mPerformanceMode)
	{
		QiMemoryBarrier();
		mJobAvailable.release(mWorkers.getCount());
	}
}


int QiTaskDispatcher::add(QiTask* task)
{
	//Check to see if the ring buffer is filling up
	while((mJobCount+1) % QI_THREAD_POOL_MAX_JOBS == mCurrentJob)
		processJobOrSleep();

	int jobId = mNextJobId++;
	mJobs[mJobCount].mJobId = jobId;
	mJobs[mJobCount].mTask = task;

	QiMemoryBarrier();
	InterlockedIncrementWrap(&mJobCount, QI_THREAD_POOL_MAX_JOBS);

	return jobId;
}

void QiTaskDispatcher::clear()
{
	//Make sure all workers enter wait state
	//This is important!
	bool done;
	do
	{
		done = true;
		for(int i=0; i<mWorkers.getCount(); i++)
		{
			if (!mWorkers[i]->mWaiting)
				done = false;
		}
	} while(!done);

	//Reset job counter to avoid wrapping issues
	mNextJobId = 0;
	mCompletedJobs = 0;
}


void QiTaskDispatcher::waitForJob(int job)
{
	bool done;
	do
	{
		done = true;
		if (job < (int)mCompletedJobs)
		{
			//Need to check all workers, to make sure they
			//are past the job we're asking for
			for(int i=0; i<mWorkers.getCount(); i++)
			{
				if ((int)mWorkers[i]->mCurrentJobId <= job)
					done = false;
			}
		}
		else
			done = false;
		//Crunch a job or get some sleep while waiting
		if (!done)
			processJobOrSleep();
	} while(!done);
}


void QiTaskDispatcher::processJobOrSleep()
{
	QiJob job;
	if (getNextJob(job))
	{
		job.mTask->onExecute();
		QiMemoryBarrier();
		QiInterlockedIncrement(&mCompletedJobs);
		mSpinWaitCount = 0;
	}
	else
	{
		//Be a good boy
		if (mSpinWaitCount++ < 512)
		{
			QiPause();
		}
		else
		{
			QiThread::yield();
			mSpinWaitCount = 0;
		}
	}
}


void QiTaskDispatcher::waitForAll()
{
	while(mCompletedJobs < mNextJobId)
	{
		processJobOrSleep();
	}
}


QiTaskDispatcher::Worker* QiTaskDispatcher::createWorkerThread()
{
	return QI_NEW QiTaskDispatcher::Worker(this);
}


void QiTaskDispatcher::destroyWorkerThread(Worker* thread)
{
	QI_DELETE(thread);
}

//Find best available task and mark that as running. This method is always called
//from a worker thread.
bool QiTaskDispatcher::getNextJob(QiJob& job)
{
	//Select the next job interlocked
	unsigned long currentJob;
	unsigned long nextJob;
	do
	{
		currentJob = mCurrentJob;

		//We've reached the end of the ring buffer. No more jobs.
		if (currentJob == mJobCount)
			return false;

		//Copy job here, this is important, since the add can overwrite this position
		job = mJobs[currentJob];

		nextJob = (mCurrentJob+1) % QI_THREAD_POOL_MAX_JOBS;
	} while(QiInterlockedCompareExchange(&mCurrentJob, nextJob, currentJob) != currentJob);
	return true;
}



