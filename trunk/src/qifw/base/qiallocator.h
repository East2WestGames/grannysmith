#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"

class QiAllocator
{
public:
	static void	init();
	static void shutdown();
	static void* alloc(int size);
	static void* realloc(void* ptr, int size);
	static void free(void* ptr);
	
	static void printStats();
};

