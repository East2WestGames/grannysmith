#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

class QiXmlWriterImpl;
class QiXmlWriter
{
public:
	QiXmlWriter();
	~QiXmlWriter();

	//Control
	bool write(class QiOutputStream& stream);

	//Clear
	void reset();

	//Element name and value
	bool enter(const class QiString& name);
	bool setValue(const class QiString& value);
	bool leave();

	//Attributes
	bool setAttribute(const class QiString& name, const class QiString& value);
	bool removeAttribute(const class QiString& name);

protected:
	QiXmlWriterImpl* mImpl;
};



