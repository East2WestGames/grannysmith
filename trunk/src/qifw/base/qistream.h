#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"
#include "base/qibyteorder.h"

class QiOutputStream;
class QiInputStream
{
public:
	QiInputStream(QiByteOrder byteOrder = QI_HOST_BYTE_ORDER);
	virtual ~QiInputStream();

	void resetInputStream();

	void setInputByteOrder(QiByteOrder byteOrder);
	QiByteOrder getInputByteOrder() const;

	bool readBool(bool& val);

	bool readInt8(QiInt8& val);
	inline bool readUInt8(QiUInt8& val) {return readInt8((QiInt8&)val);}
	bool readInt16(QiInt16& val);
	inline bool readUInt16(QiUInt16& val) {return readInt16((QiInt16&)val);}
	bool readInt32(QiInt32& val);
	inline bool readUInt32(QiUInt32& val) {return readInt32((QiInt32&)val);}
	bool readInt64(QiInt64& val);
	inline bool readUInt64(QiUInt64& val) {return readInt64((QiInt64&)val);}

	bool readFloat32(QiFloat32& val);
	bool readFloat64(QiFloat64& val);

	bool readString(char* string, int maxLength);
	bool readLine(char* string, int maxLength);

	bool readBuffer(void* buffer, size_t bytes);
	bool readBuffer(QiOutputStream& buffer, size_t bytes);

	int getInputByteCount() const { return mInputByteCount; }
	
	virtual bool hasMoreData() = 0;

protected:
	inline bool readInternalCount(char* buffer, size_t bytes)
	{
		if (readInternal(buffer, bytes))
		{
			mInputByteCount += bytes;
			return true;
		}
		return false;
	}

private:
	virtual bool readInternal(char* buffer, size_t bytes) = 0;

	QiByteOrder mInputByteOrder;
	int mInputByteCount;
};


class QiOutputStream
{
public:
	QiOutputStream(QiByteOrder byteOrder = QI_HOST_BYTE_ORDER);
	virtual ~QiOutputStream();

	void resetOutputStream();

	void setOutputByteOrder(QiByteOrder byteOrder);
	QiByteOrder getOutputByteOrder() const;

	bool writeBool(bool val);
	
	bool writeInt8(QiInt8 val);
	inline bool writeUInt8(QiUInt8 val) {return writeInt8((QiInt8)val);}
	bool writeInt16(QiInt16 val);
	inline bool writeUInt16(QiUInt16 val) {return writeInt16((QiInt16)val);}
	bool writeInt32(QiInt32 val);
	inline bool writeUInt32(QiUInt32 val) {return writeInt32((QiInt32)val);}
	bool writeInt64(QiInt64 val);
	inline bool writeUInt64(QiUInt64 val) {return writeInt64((QiInt64)val);}

	bool writeFloat32(QiFloat32 val);
	bool writeFloat64(QiFloat64 val);
	
	bool writeString(const char* string);
	bool writeLine(const char* string);

	bool writeBuffer(const void* buffer, size_t bytes);

	bool writeBuffer(QiInputStream& in, size_t bytes);
	
	int getOutputByteCount() const { return mOutputByteCount; }

	virtual bool flush();

protected:
	inline bool writeInternalCount(const char* buffer, size_t bytes)
	{
		if (writeInternal(buffer, bytes))
		{
			mOutputByteCount += bytes;
			return true;
		}
		return false;
	}

private:
	virtual bool writeInternal(const char* buffer, size_t bytes) = 0;

	QiByteOrder mOutputByteOrder;
	int mOutputByteCount;
};



