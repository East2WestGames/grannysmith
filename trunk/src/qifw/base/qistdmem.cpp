/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qimem.h"

#include <string.h>

extern "C"
{
#include "base/qistdmem.h"
}


void* QiStdAlloc(size_t size)
{
	return QiAlloc(size);
}


void* QiStdCAlloc(size_t count, size_t size)
{
	void* mem = QiAlloc(count*size);
	memset(mem, 0, count*size);
	return mem;
}


void* QiStdRealloc(void* ptr, size_t size)
{
	return QiRealloc(ptr, size);
}


void QiStdFree(void* ptr)
{
	QiFree(ptr);
}


