#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"
#include "base/qimem.h"
#include "base/qidebug.h"
#include "base/qihashtable.h"
/*
template <class Key, class Value>
class QiMap
{
protected:
	class Entry
	{
	public:
		Key key;
		Value value;

		inline Entry() {}
		inline Entry(const Key& key) : key(key) {}
		inline Entry(const Key& key, const Value& value) : key(key), value(value) {}

		inline QiHash getHash() const
		{
			return key.getHash();
		}

		inline bool operator==(const Entry& other) const
		{
			return key == other.key;
		}
	};

	QiHashTable<Entry> mHashTable;

public:
	
	QiMap()
	{
	}
	
	~QiMap()
	{
	}
	
	void put(const Key& key, const Value& value)
	{
		mHashTable.put(Entry(key, value));
	}

	bool remove(const Key& key)
	{
		return mHashTable.remove(Entry(key));
	}

	bool removeByValue(const Value& value)
	{
		bool removedAny = false;
		bool removed;
		do
		{
			removed = false;
			QiHashTable<Entry>::Iterator iter = mHashTable.getIterator();
			while(iter.hasNext())
			{
				Entry& e = iter.next();
				if (e.value == value)
				{
					mHashTable.remove(e);
					removed = true;
					removedAny = true;
					break;
				}
			}
		} while(removed);

		return removedAny;
	}

	bool contains(const Key& key) const
	{
		return mHashTable.contains(Entry(key));
	}
	
	bool get(const Key& key, Value& value) const
	{
		Entry e(key);
		if (mHashTable.get(e))
		{
			value = e.value;
			return true;
		}
		return false;
	}

	Value* getPointer(const Key& key)
	{
		Entry* e = mHashTable.getPointer(Entry(key));
		if (e)
			return &e->value;
		else
			return NULL;
	}
	
	inline int getCount() const
	{
		return mHashTable.getCount();
	}
	
	inline void clear()
	{
		mHashTable.clear();
	}
	
	void getKeys(QiArray<Key>& keys)
	{
		QiHashTable<Entry>::Iterator iter = mHashTable.getIterator();
		while(iter.hasNext())
		{
			Entry& e = iter.next();
			keys.add(e.key);
		}
	}
};

*/