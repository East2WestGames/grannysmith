/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"
#include "base/qisystem.h"
#include "base/qistring.h"

#ifdef QI_MACOS
#include <unistd.h>
#include <stdio.h>
#include <assert.h>
#include <sys/sysctl.h>
#include <pwd.h>
#endif

#if defined(QI_IOS) || defined(QI_ANDROID)
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#endif

#ifdef QI_LINUX
#include <unistd.h>
#include <pwd.h>
#endif

#ifdef QI_WIN32
#include <windows.h>
#include <shlobj.h>
#include <time.h>
#include <stdio.h>
#include <float.h>
#endif

#if defined(QI_ARCH_INTEL)
#include <emmintrin.h>
#include <xmmintrin.h>
#endif


int QiSystem::getCpuCount()
{
#ifdef QI_MACOS
	size_t two = 2;
	size_t len = sizeof(int);
	int mib[2];
	int val;
	sysctlnametomib( "hw.ncpu", mib, &two );
	if ( sysctl( mib, two, &val, &len, NULL, 0) != -1 && len > 0 )
		return val;
	else
		return 1;
#endif
#ifdef QI_LINUX
	return sysconf(_SC_NPROCESSORS_ONLN);
#endif
#ifdef QI_WIN32
	static SYSTEM_INFO	si;
	GetSystemInfo(&si);
	return si.dwNumberOfProcessors;
#endif
	return 1;
}


QiString QiSystem::getArchitecture()
{
#ifdef QI_MACOS
#ifdef QI_ARCH_PPC
	return QiString("PPC/MacOS X");
#else
	return QiString("Intel/MacOS X");
#endif
#endif
#ifdef QI_WIN32
	return QiString("Intel/Win32");
#endif
	return QiString("Unknown/Unknown");
}


QiString QiSystem::getExecutablePath()
{
#ifdef QI_WIN32
	char tmp[MAX_PATH+1];
	tmp[0] = '\0';
	if (GetModuleFileName(NULL, tmp, sizeof(tmp)))
		return QiString(tmp);
	else
		return QiString();
#else
	return QiString();
#endif
}


QiString QiSystem::getHomeDir()
{
#if defined(QI_WIN32)
	char dir[MAX_PATH+1];
	dir[0] = '\0';
	SHGetSpecialFolderPath(NULL, dir, CSIDL_PROFILE, 0);
	if (dir)
		return QiString(dir);
	else
		return QiString(".");
#elif defined(QI_IOS) || defined(QI_ANDROID)
	return QiString();
#else
	return QiString(getpwuid(getuid())->pw_dir);
#endif
}


QiString QiSystem::getComputerName()
{
#ifdef QI_WIN32
	char tmp[MAX_COMPUTERNAME_LENGTH+1];
	tmp[0] = '\0';
	DWORD size = sizeof(tmp);
	if (GetComputerName(tmp, &size))
		return QiString(tmp);
	else
		return QiString();
#else
	return QiString();
#endif
}


QiString QiSystem::getUserName()
{
#ifdef QI_WIN32
	char tmp[256];
	tmp[0] = '\0';
	DWORD size = sizeof(tmp);
	if (GetUserName(tmp, &size))
		return QiString(tmp);
	else
		return QiString();
#else
	return QiString();
#endif
}
#include "base/qidebug.h"


QiInt64 QiSystem::getCurrentDateTime()
{
#ifdef WIN32
	__time64_t long_time;
	_time64( &long_time );
	return long_time;
#else
	time_t t;
	struct tm *tmp;
	t = time(NULL);
	tmp = localtime(&t);
	if (tmp == NULL)
		return 0;
	else
		return mktime(tmp);
#endif
}

QiInt64 QiSystem::getDateTime(int year, int month, int day, int hour, int minute, int second)
{
#ifdef WIN32
	struct tm t;
	t.tm_year = year-1900;
	t.tm_mon = month-1;
	t.tm_mday = day;
	t.tm_hour = hour;
	t.tm_min = minute;
	t.tm_sec = second;
	t.tm_isdst = 0;
	return _mktime64(&t);
#else
	struct tm t;
	t.tm_year = year-1900;
	t.tm_mon = month-1;
	t.tm_mday = day;
	t.tm_hour = hour;
	t.tm_min = minute;
	t.tm_sec = second;
	t.tm_isdst = 0;
	return mktime(&t);
#endif
}


QiString QiSystem::getDateTimeString(QiInt64 dateTime)
{
#ifdef WIN32
	__time64_t long_time = (__time64_t)dateTime;
	struct tm *newtime;
	newtime = _localtime64( &long_time );
	if (newtime)
	{
		char buff[256];
		buff[0] = '\0';
		asctime_s( buff, sizeof(buff), newtime );
		return QiString(buff);
	}
	else
		return QiString();
#else
	char outstr[256];
	struct tm *tmp;
	time_t t = dateTime;
	tmp = localtime(&t);
    if (strftime(outstr, sizeof(outstr), "%m", tmp) == 0)
		return "";
	else
		return QiString(outstr);
#endif
}


QiInt32 QiSystem::getProcessId()
{
#ifdef WIN32
	return GetCurrentProcessId();
#else
	return 0;
#endif
}


bool QiSystem::isVista()
{
#ifdef WIN32
	static bool vista = false;
	static bool initialized = false;

	if( initialized )
		return vista;

	OSVERSIONINFO version_info;
	ZeroMemory( &version_info, sizeof( OSVERSIONINFO ) );
	version_info.dwOSVersionInfoSize = sizeof( OSVERSIONINFO );
	if( GetVersionEx( &version_info ) )
	{
		initialized = true;
		vista = version_info.dwMajorVersion >= 6;
		return vista;
	}
	return false;
#else
	return false;
#endif
}

void QiSystem::setFpu(FpuPrecision precision, bool interuptDivisionByZero)
{
#ifdef WIN32
	int p = _PC_64;
	switch(precision)
	{
		case FPU_32_BITS:
		p = _PC_24;
		break;

		case FPU_64_BITS:
		p = _PC_53;
		break;
	}
	unsigned int mask = 0xFFFFFFFF;
	if (interuptDivisionByZero)
	{
		mask ^= _EM_ZERODIVIDE;
		mask ^= _EM_INVALID;
		mask ^= _EM_OVERFLOW;
	}

	_control87(p, _MCW_PC);
	_control87(mask, _MCW_EM);
	_control87(_DN_FLUSH, _MCW_DN);
#else
#endif
}


volatile unsigned long QiInterlockedCompareExchange(volatile unsigned long* dest, volatile unsigned long exchange, volatile unsigned long comparand)
{
#ifdef QI_WIN32
	return ::InterlockedCompareExchange(dest, exchange, comparand);
#else
	return __sync_val_compare_and_swap(dest, comparand, exchange);
#endif
}


void QiInterlockedIncrement(volatile unsigned long* dest)
{
#ifdef QI_WIN32
	::InterlockedIncrement(dest);
#else
	__sync_fetch_and_add(dest, 1);
#endif
}

void QiMemoryBarrier()
{
#ifdef QI_WIN32
	::MemoryBarrier();
#else
	__sync_synchronize();
#endif
}

void QiPause()
{
#if !defined(QI_IOS) && !defined(QI_ANDROID)
	_mm_pause();
#endif
}

void QiSystem::exit()
{
	::exit(0);
}


void QiSystem::openUrl(const QiString& url)
{
#ifdef QI_WIN32
	ShellExecute(GetForegroundWindow(), "open", url.c_str(), NULL, NULL, SW_SHOWNORMAL);
#endif
}

