/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qimem.h"
#include "base/qistream.h"
#include "base/qicompress.h"
#include "base/zlib/zlib.h"

#include "base/qidebug.h"

static void* zQiAlloc(voidpf opaque, uInt items, uInt size)
{
	return QiAlloc(items * size);
}

static void zQiFree(voidpf opaque, voidpf address)
{
	QiFree(address);
}

class QiCompressImpl
{
public:
	z_stream strm;
};

QiCompress::QiCompress(int dataType, int strategy, int level)
{
	mImpl = NULL;
	init(dataType, strategy, level);
}

QiCompress::~QiCompress()
{
	shutdown();
}

void QiCompress::init(int dataType, int strategy, int level, bool gzipFormat)
{
	mDataType = dataType;
	mStrategy = strategy;
	mLevel = level;
	mImpl = QI_NEW QiCompressImpl;
	mImpl->strm.zalloc = zQiAlloc;
    mImpl->strm.zfree = zQiFree;
    mImpl->strm.opaque = Z_NULL;
	mImpl->strm.data_type = dataType;
	deflateInit2(&mImpl->strm, Z_DEFAULT_COMPRESSION, Z_DEFLATED, gzipFormat ? 31 : 15, 8, Z_DEFAULT_STRATEGY);
}

void QiCompress::shutdown()
{
	if (mImpl)
	{
		deflateEnd(&mImpl->strm);
		QI_DELETE(mImpl);
		mImpl = NULL;
	}
}

bool QiCompress::process(QiInputStream& in, QiOutputStream& out, int bytes, bool finish)
{
	const int chunkSize = 8192;
	char inBuf[chunkSize];
	char outBuf[chunkSize];
	while(bytes > 0)
	{
		int currChunk = QiMin(bytes, chunkSize);
		if (!in.readBuffer(inBuf, currChunk))
			return false;
		mImpl->strm.next_in = (Bytef*)inBuf;
		mImpl->strm.avail_in = currChunk;
		bool last = (bytes - currChunk == 0);
		mImpl->strm.avail_out = 0;
		while(mImpl->strm.avail_in > 0 && mImpl->strm.avail_out == 0)
		{
			mImpl->strm.next_out = (Bytef*)outBuf;
			mImpl->strm.avail_out = chunkSize;
			int st = (last ? (finish ? Z_FINISH : Z_SYNC_FLUSH) : Z_SYNC_FLUSH);
			int status = deflate(&mImpl->strm, st);
			if (status != Z_OK && status != Z_STREAM_END)
				return false;
			int written = chunkSize - mImpl->strm.avail_out;
			if (!out.writeBuffer(outBuf, written))
				return false;
		}
		bytes -= currChunk;
	}

	return true;
}


void QiCompress::reset()
{	
	shutdown();
	init(mDataType, mStrategy, mLevel);
}

