#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qisync.h"

#ifdef QI_WIN32

//Use win32 threads
#include <windows.h>
typedef HANDLE QiThreadHandle;
typedef DWORD QiThreadId;

DWORD WINAPI QiThreadEntryPoint(LPVOID thread);

#else

//Use posix threads
#include <pthread.h>
typedef pthread_t QiThreadHandle;
typedef int QiThreadId;

void *QiThreadEntryPoint(void* thread);

#endif


class QiThread
{
#ifdef QI_WIN32
	friend DWORD WINAPI QiThreadEntryPoint(LPVOID thread);
#else
	friend void* QiThreadEntryPoint(void*);
#endif
public:
	QiThread();
	virtual ~QiThread();

	void start(size_t stackSize = 1024*1024);
	void stop();
	void wait();
	void setPriority(int p);

	void signalQuit();	
	bool isRunning() const;

	static void yield();
	static void sleep(float time);
	static QiThreadId getCurrentThreadId();

protected:
	virtual void run() = 0;
	bool shouldQuit() const;

private:
	QiThreadHandle mThread;
	volatile bool mQuitFlag;
	volatile bool mRunning;
};



