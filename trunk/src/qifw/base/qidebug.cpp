/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qidebug.h"
#include "base/qimem.h"

#include <stdio.h>
#include <assert.h>
#include <string.h>

QiDebugStream* QiDebug::sPrintStream = NULL;
QiDebugStream* QiDebug::sWarningStream = NULL;
QiDebugStream* QiDebug::sErrorStream = NULL;

void QiAssert()
{	
	//stack.ShowCallstack();
	assert(false);
}

void QiDebug::setPrintStream(QiDebugStream* stream){
	sPrintStream = stream;
}


QiDebugStream* QiDebug::getPrintStream(){
	return sPrintStream;
}


void QiDebug::setWarningStream(QiDebugStream* stream){
	sWarningStream = stream;
}


QiDebugStream* QiDebug::getWarningStream(){
	return sWarningStream;
}


QiDebugStream* QiDebug::getErrorStream(){
	return sErrorStream;
}


void QiDebug::setErrorStream(QiDebugStream* stream){
	sErrorStream = stream;
}

