#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

class QiXmlParserImpl;
class QiXmlParser
{
public:
	QiXmlParser();
	QiXmlParser(class QiInputStream& stream, int length);
	~QiXmlParser();

	//Parse control
	bool parse(class QiInputStream& stream, int length);
	class QiString getError();

	//General flow
	void reset();
	bool isValid();
	bool next();
	bool enter();
	bool leave();

	//Jump to specific node
	bool select(const class QiString& name);
	bool push();
	bool pop();

	//Element name and value
	class QiString getName();
	class QiString getValue();

	//Attribute retrieval
	int getAttributeCount();
	class QiString getAttributeName(int index);
	class QiString getAttribute(int index);
	class QiString getAttribute(const class QiString& name);

protected:
	QiXmlParserImpl* mImpl;
};



