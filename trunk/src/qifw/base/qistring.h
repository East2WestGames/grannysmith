#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */
/* QiString is relocateable */

#include "base/qiconfig.h"
#include <string.h>

const int QI_INTERNAL_STRING_SIZE = 16;

class QiString {
public:
	QiString();
	QiString(const QiString& string);
	QiString(const char * string);
	explicit QiString(char ch);
	~QiString();

	//Assignment
	QiString& operator=(const char* string);
	QiString& operator=(const QiString& string);
 
	//Compare (fastest comparison is always two QiStrings)
	inline bool operator==(const QiString& string) const { return string.mLength==mLength && strcmp(getData(), string.getData())==0; }
	bool operator==(const char* string) const { return strcmp(getData(), string)==0; }
	bool operator!=(const QiString& string) const { return string.mLength!=mLength || strcmp(getData(), string.getData())!=0; }
	bool operator!=(const char* string) const { return strcmp(getData(), string)!=0; }
	int compareTo(const QiString& string, bool caseSensitive = true) const;
	int compareTo(const char* string, bool caseSensitive = true) const;
	bool matchExpression(const QiString& expression, bool caseSensitive = true);

	//Concatenate
	QiString operator+(const QiString& string) const;
	QiString operator+(const char* string) const;
	inline QiString operator+(const unsigned char* string) const { return operator+((const char*)string); };
	QiString& operator+=(const QiString& string);
	QiString& operator+=(const char* string);
	inline QiString& operator+=(const unsigned char* string) { return operator+=((const char*)string); }

	QiString operator+(char ch) const;
	inline QiString operator+(unsigned char ch) const { return operator+((char)ch); }
	inline QiString operator+(short int i) const { return operator+((int)i); }
	inline QiString operator+(unsigned short int i) const { return operator+((int)i); }
	QiString operator+(int i) const;
	inline QiString operator+(unsigned int i) const { return operator+((int)i); }
	QiString operator+(long long i) const;
	inline QiString operator+(unsigned long long i) const { return operator+((long long)i); }
	QiString& operator+=(char ch);
	inline QiString& operator+=(unsigned char ch) { return operator+=((char)ch); }
	inline QiString& operator+=(short int i) { return operator+=((int)i); }
	inline QiString& operator+=(unsigned short int i) { return operator+=((int)i); }
	QiString& operator+=(int i);
	inline QiString& operator+=(unsigned int i) { return operator+=((int)i); }
	QiString& operator+=(long long i);
	inline QiString& operator+=(unsigned long long i) { return operator+=((long long)i); }

	QiString operator+(float f) const;
	inline QiString operator+(double d) const { return operator+((float)d); }
	inline QiString operator+(bool b) const { return operator+(b ? "true" : "false"); }
	QiString operator+(void *ptr) const;
	QiString& operator+=(float f);
	inline QiString& operator+=(double d) { return operator+=((float)d); }
	inline QiString& operator+=(bool b) { return operator+=(b ? "true" : "false"); }
	QiString& operator+=(void *ptr);

	//Array access
	inline char* c_str() { return mData ? mData : mInternalData; }
	inline const char* c_str() const { return mData ? mData : mInternalData; }
	inline operator const char* () const { return c_str(); }
	inline char& operator[](int n) { return getData()[n]; }
	inline char operator[](int n) const { return getData()[n]; }

	//Length
	inline int getLength() const { return mLength; }
	inline bool isEmpty() const { return mLength==0; }

	//Search and replace
	bool startsWith(const char* string, bool caseSensitive = true) const;
	bool startsWith(const QiString& string, bool caseSensitive = true) const;
	bool endsWith(const char* string, bool caseSensitive = true) const;
	bool endsWith(const QiString& string, bool caseSensitive = true) const;
	bool contains(const QiString& string, bool caseSensitive = true) const;
	bool contains(const char* string, bool caseSensitive = true) const;
	int getIndexOf(const QiString& string, int start = 0, bool caseSensitive = true) const;
	int getIndexOf(const char* string, int start = 0, bool caseSensitive = true) const;
	int getLastIndexOf(const QiString& string, bool caseSensitive = true) const;
	int getLastIndexOf(const char* string, bool caseSensitive = true) const;
	QiString replace(const char* search, const char* replace, bool caseSensitive = true) const;

	//Modify
	void trim();
	QiString toLowerCase() const;
	QiString toUpperCase() const;
	QiString substring(int start) const;
	QiString substring(int start, int end) const;

	//Word tokenizer
	int getWordCount() const;
	QiString getWord(int index) const;

	//Numeric
	bool isNumerical() const;
	char toChar() const;
	short toShort() const;
	int toInt() const;
	float toFloat() const;
	QiInt64 toInt64() const;
	
	//Hash
	QiHash getHash() const;
	
	static QiString formatFloat(float number, int maxDecimals = 5, bool allowScientific = true);
	
protected:
	inline char* getData()
	{
		return mData ? mData : mInternalData;
	}

	inline const char* getData() const
	{
		return mData ? mData : mInternalData;
	}

	char* mData;
	int mSize;
	int mLength;
	char mInternalData[QI_INTERNAL_STRING_SIZE];

	void grow(int size);
	void assign(const char * s);
	void assign(const QiString& s);
};

QiString operator+(const char* string1, const QiString& string2);
	

