#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

template <class T>
class QiComparator 
{
public:
	int compare(const T& a, const T& b) const
	{
		if (a < b)
			return -1;
		else if (b < a)
			return 1;
		else
			return 0;
	}
};


template <class T, class Comparator>
void QiSort(T* arr, int count, const Comparator& comparator)
{
	const int MAX_LEVELS = 300;
	int beg[MAX_LEVELS];
	int end[MAX_LEVELS];
	int i=0;
	int L;
	int R;
	int swap;
	T piv;

	beg[0]=0;
	end[0]=count;
	while (i>=0) 
	{
		QI_ASSERT(i < MAX_LEVELS-1, "Too many levels during sort");
		L=beg[i]; 
		R=end[i]-1;
		if (L < R) 
		{
			piv=arr[L];
			while (L < R)
			{
	       		while (comparator.compare(arr[R], piv) != -1 && L < R) 
	       			R--; 
	       		if (L < R) 
	       			arr[L++] = arr[R];
	       		while (comparator.compare(arr[L], piv) != 1 && L < R) 
	       			L++; 
	       		if (L < R) 
	       			arr[R--] = arr[L];
	       	}
			arr[L] = piv; 
			beg[i+1] = L+1; 
			end[i+1] = end[i]; 
			end[i++] = L;
			if (end[i]-beg[i] > end[i-1]-beg[i-1])
			{
				swap = beg[i]; 
				beg[i] = beg[i-1]; 
				beg[i-1] = swap;
				swap = end[i]; 
				end[i] = end[i-1]; 
				end[i-1] = swap; 
			}
	   	}
		else 
		{
	   		i--; 
		}
	}
}


template <class T, class Iterator, class Comparator>
int QiTopEntries(Iterator& iter, T* results, int maxResults, const Comparator& comp, bool descending = true)
{
	if (maxResults > iter.getCount())
		maxResults = iter.getCount();
	for(int i=0; i<maxResults; i++)
		results[i].val = descending ? -2000000000 : 2000000000;
	while(iter.hasNext())
	{
		T& current = iter.next();
		int i;
		//Find where to put this entry
		for(i=(int)maxResults-1; i>=0; i--)
		{
			if ((descending && comp.compare(current, results[i]) < 1) || 
				(!descending && comp.compare(current, results[i]) > -1)) 
				break;
		}
		i++;
		if (i < (int)maxResults)
		{
			//Push down maxResults below
			for(int j=(int)maxResults-1; j>i; j--)
				results[j] = results[j-1];
			//Insert at pos i
			results[i] = current;
		}
	}
	return maxResults;
}	



