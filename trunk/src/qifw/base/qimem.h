#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"

void* QiAlloc(size_t size, const char* desc = NULL);
void* QiRealloc(void* ptr, size_t size);
void QiFree(void* ptr);

int QiGetAllocatedBytes();
int QiGetAllocationCount();

enum QiMemType { QI_MEM_DUMMY };
void* operator new (size_t size, QiMemType dummy);
void operator delete (void* ptr, QiMemType dummy);
void* operator new[] (size_t size, QiMemType dummy);
void operator delete[] (void* ptr, QiMemType dummy);

#define QI_NEW new( QI_MEM_DUMMY ) 
template <class T> void QI_DELETE(T* ptr)
{
	if (ptr)
	{
		ptr->~T();
		QiFree(ptr);
	}
}

