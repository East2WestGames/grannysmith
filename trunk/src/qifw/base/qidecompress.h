#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

class QiDecompress
{
public:
	QiDecompress();
	~QiDecompress();

	void init();
	void shutdown();
	void reset();
	bool process(class QiInputStream& in, class QiOutputStream& out, int bytes);

protected:
	class QiDecompressImpl* mImpl;
};



