#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"


class QiMutex
{
public:
	QiMutex();
	~QiMutex();
	
	bool tryLock();
	void lock();
	void unlock();

protected:
	class QiMutexImpl* mImpl;
};


class QiMutexZone
{
public:
	inline QiMutexZone(QiMutex& mutex) : mMutex(mutex)
	{
		mMutex.lock(); 
	}

	inline ~QiMutexZone()
	{
		mMutex.unlock();
	}
	
protected:
	QiMutex& mMutex;
};


class QiRwLock
{
public:
	QiRwLock();
	~QiRwLock();
	
	bool tryLockRead();
	void lockRead();

	bool tryLockWrite();
	void lockWrite();

	void unlock();

protected:
	enum State
	{
		UNLOCKED,
		READ_LOCKED,
		WRITE_LOCKED
	};
	
	State mState;
	class QiRwLockImpl* mImpl;
	
};


class QiCondition
{
public:
	QiCondition();
	~QiCondition();
	
	//Wait until this condition is in signaled state. If condition is already signaled, return immediately.
	void wait();

	//Set condition in signaled state.
	void signal();

	//Set condition in unsignaled state. This is the initial state.
	void reset();

protected:
	class QiConditionImpl* mImpl;
};

class QiSemaphore
{
public:
	QiSemaphore();
	QiSemaphore(int count, int maxCount);
	~QiSemaphore();

	void init(int count, int maxCount);

	//Increment counter
	void release(int count=1);

	//Wait until counter is above zero and decrement
	void wait();

protected:
	class QiSemaphoreImpl* mImpl;
};




