/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qicommandline.h"

QiCommandLine::QiCommandLine()
{
}

QiCommandLine::QiCommandLine(int argc, char** argv)
{
	set(argc, argv);
}


void QiCommandLine::set(int argc, char** argv)
{
	for(int i=1; i<argc; i++)
	{
		mCmdLine += argv[i];
		mCmdLine += " ";
	}
}


bool QiCommandLine::has(const QiString& option) const
{
	return mCmdLine.contains(option);
}


QiString QiCommandLine::get(const QiString& option, int index) const
{
	int i = mCmdLine.getIndexOf(option);
	if (i != -1)
		return mCmdLine.substring(i + option.getLength()).getWord(index);
	else
		return "";
}

void QiCommandLine::consume(const QiString& option, int argumentCount)
{
	int i = mCmdLine.getIndexOf(option);
	if (i != -1)
	{
		QiString first = mCmdLine.substring(0, i);
		QiString second = mCmdLine.substring(i + option.getLength());
		second.trim();
		for(int i=0; i<argumentCount; i++)
		{
			QiString w = second.getWord(0);
			second = second.substring(w.getLength());
			second.trim();
		}
		mCmdLine = first + " " + second;
	}
}

