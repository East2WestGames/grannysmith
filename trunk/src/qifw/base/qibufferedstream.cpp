/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "qibufferedstream.h"
#include "string.h"

QiBufferedOutputStream::QiBufferedOutputStream() :
mCount(0)
{
}

QiBufferedOutputStream::~QiBufferedOutputStream()
{
	flush();
}

bool QiBufferedOutputStream::flush()
{
	if (mCount > 0)
	{
		bool ret = writeBuffered(mBuffer, mCount);
		mCount = 0;
		return ret;
	}
	return true;
}

bool QiBufferedOutputStream::writeInternal(const char* buffer, size_t bytes)
{
	if (bytes+mCount < QI_BUFFERED_STREAM_SIZE)
	{
		//Add to internal buffer
		memcpy(mBuffer+mCount, buffer, bytes);
		mCount+=bytes;
		return true;
	}
	else
	{
		bool ret = flush();
		//Try add to internal buffer, otherwise write directly
		if (bytes+mCount < QI_BUFFERED_STREAM_SIZE)
		{
			memcpy(mBuffer+mCount, buffer, bytes);
			mCount+=bytes;
			return ret;
		}
		else
			return writeBuffered(buffer, bytes) && ret;
	}
}

