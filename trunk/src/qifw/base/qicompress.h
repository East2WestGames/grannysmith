#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

class QiCompress
{
public:
	QiCompress(int dataType=0, int strategy=0, int level=-1);
	~QiCompress();

	void init(int dataType=0, int strategy=0, int level=-1, bool gzipFormat=false);
	void shutdown();
	void reset();
	bool process(class QiInputStream& in, class QiOutputStream& out, int bytes, bool finish=false);

protected:
	class QiCompressImpl* mImpl;
	int mDataType;
	int mStrategy;
	int mLevel;
};

