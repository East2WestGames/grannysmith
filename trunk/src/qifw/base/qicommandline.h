#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qistring.h"

class QiCommandLine
{
public:
	QiCommandLine();

	QiCommandLine(int argc, char** argv);
	
	void set(int argc, char** argv);

	bool has(const QiString& option) const;

	QiString get(const QiString& option, int index = 0) const;

	void consume(const QiString& option, int argumentCount = 0);
	
	inline const QiString& getString() const
	{
		return mCmdLine;
	}
	
protected:
	QiString mCmdLine;
};

