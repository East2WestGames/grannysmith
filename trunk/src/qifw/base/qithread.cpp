/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"
#include "base/qidebug.h"
#include "base/qithread.h"
#include "base/qisystem.h"

/////////////////////////////////////////////////////////////
////////////////////////// WIN32 ////////////////////////////
/////////////////////////////////////////////////////////////
#ifdef QI_WIN32

QiThread::QiThread() :
mThread(0), mQuitFlag(false), mRunning(false)
{
}

QiThread::~QiThread()
{
}

static DWORD WINAPI QiThreadEntryPoint(LPVOID thread)
{
	((QiThread*)thread)->run();
	((QiThread*)thread)->mRunning = false;
	MemoryBarrier();
	return NULL;
}

void QiThread::start(size_t stackSize)
{
	mQuitFlag = false;
	mRunning = true;
	MemoryBarrier();
	mThread = CreateThread(NULL, stackSize, (LPTHREAD_START_ROUTINE)QiThreadEntryPoint, (LPVOID) this, 0, 0);
}

void QiThread::stop()
{
	TerminateThread(mThread, 0);
}

QiThreadId QiThread::getCurrentThreadId()
{
	return (QiThreadId)GetCurrentThreadId();
}


void QiThread::sleep(float time)
{
	::Sleep((int)(time*1000.0f));
}


void QiThread::yield()
{
	::Sleep(0);
}


void QiThread::setPriority(int p)
{
	int prio = THREAD_PRIORITY_NORMAL;
	switch(p)
	{
	case -2:
		prio = THREAD_PRIORITY_LOWEST;
		break;
	case -1:
		prio = THREAD_PRIORITY_BELOW_NORMAL;
		break;
	case 1:
		prio = THREAD_PRIORITY_ABOVE_NORMAL;
		break;
	case 2:
		prio = THREAD_PRIORITY_HIGHEST;
		break;
	}
	::SetThreadPriority(mThread, prio);
}


/////////////////////////////////////////////////////////////
////////////////////////// POSIX ////////////////////////////
/////////////////////////////////////////////////////////////
#else

#include <unistd.h>
#include <sched.h>
#include <pthread.h>
#include <signal.h>

void QiThreadCancelHandler(void* thread)
{
	QI_PRINT("Thread " + thread + "canceled");
}

void *QiThreadEntryPoint(void* thread)
{
	pthread_cleanup_push(QiThreadCancelHandler, thread);
	((QiThread*)thread)->run();
	((QiThread*)thread)->mRunning = false;
	pthread_cleanup_pop(0);
	return NULL;
}


QiThread::QiThread() :
mThread(0), mQuitFlag(false), mRunning(false)
{
}

QiThread::~QiThread()
{
}

QiThreadId QiThread::getCurrentThreadId()
{
	return (QiThreadId)pthread_self();
}

void QiThread::start(size_t stackSize)
{
	mQuitFlag = false;
	mRunning = true;
	pthread_attr_t tattr;
	pthread_attr_init(&tattr);
	pthread_attr_setdetachstate(&tattr, PTHREAD_CREATE_DETACHED);
	pthread_attr_setstacksize(&tattr, stackSize);
	pthread_create((pthread_t*)&mThread, &tattr, QiThreadEntryPoint, this);
}

void QiThread::stop()
{
	QI_ASSERT(false, "NOT FUNCTIONAL");
	//if (mThread)
	//	pthread_cancel((pthread_t)mThread);
	mThread = 0;
}

void QiThread::yield()
{
	sched_yield();
	sleep(0);
}

void QiThread::sleep(float time)
{
	usleep((unsigned int)(time*1000000.0f));
}

#endif

void QiThread::wait()
{
	while(isRunning())
		yield();
}

void QiThread::signalQuit()
{
	mQuitFlag = true;
	QiMemoryBarrier();
}

bool QiThread::isRunning() const
{
	return mRunning;
}

bool QiThread::shouldQuit() const
{
	return mQuitFlag;
}
