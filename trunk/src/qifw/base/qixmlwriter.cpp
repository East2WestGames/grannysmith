/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"
#include "base/qistream.h"
#include "base/qistring.h"
#include "base/qixmlwriter.h"
#include "base/qimemorystream.h"
#include "base/qidebug.h"
#include "base/qimem.h"

#include "base/tinyxml/tinyxml.h"

#include <iostream>
#include <sstream>


class QiXmlWriterImpl
{
public:
	QiXmlWriterImpl() : mNode(NULL)
	{
	}

	TiXmlDocument mDoc;
	TiXmlElement* mNode;
};


QiXmlWriter::QiXmlWriter()
{
	mImpl = QI_NEW QiXmlWriterImpl();
}


QiXmlWriter::~QiXmlWriter()
{
	QI_DELETE(mImpl);
}


bool QiXmlWriter::write(QiOutputStream& stream)
{
	std::ostringstream output;
	output << mImpl->mDoc;
	output.flush();
	std::stringbuf *pbuf = output.rdbuf();
	std::string tmp = pbuf->str();
	stream.writeBuffer(tmp.c_str(), tmp.size());
	return true;
}


void QiXmlWriter::reset()
{
	mImpl->mDoc.Clear();
}


bool QiXmlWriter::enter(const QiString& name)
{
	TiXmlElement toAdd(name);
	if (mImpl->mNode == NULL)
		mImpl->mNode = mImpl->mDoc.InsertEndChild(toAdd)->ToElement();
	else
		mImpl->mNode = mImpl->mNode->InsertEndChild(toAdd)->ToElement();
	return true;
}


bool QiXmlWriter::leave()
{
	if (!mImpl->mNode)
		return false;
	mImpl->mNode = (TiXmlElement*)mImpl->mNode->Parent()->ToElement();
	return true;
}


bool QiXmlWriter::setValue(const QiString& value)
{
	if (!mImpl->mNode)
		return false;
	TiXmlText toAdd(value);
	mImpl->mNode->InsertEndChild(toAdd);
	return true;
}


bool QiXmlWriter::setAttribute(const QiString& name, const QiString& value)
{
	if (!mImpl->mNode)
		return false;
	mImpl->mNode->SetAttribute(name, value);
	return true;
}


bool QiXmlWriter::removeAttribute(const QiString& name)
{
	if (!mImpl->mNode)
		return false;
	mImpl->mNode->RemoveAttribute(name);
	return true;
}



