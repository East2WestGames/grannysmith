/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qidecompress.h"
#include "base/qimem.h"
#include "base/qistream.h"
#include "base/zlib/zlib.h"

static void* zQiAlloc(voidpf opaque, uInt items, uInt size)
{
	return QiAlloc(items * size);
}

static void zQiFree(voidpf opaque, voidpf address)
{
	QiFree(address);
}

class QiDecompressImpl
{
public:
	z_stream strm;
};

QiDecompress::QiDecompress() : mImpl(NULL)
{
	init();
}

QiDecompress::~QiDecompress()
{
	shutdown();
}

#include "base/qidebug.h"

void QiDecompress::init()
{
	if (mImpl)
		shutdown();
	mImpl = QI_NEW QiDecompressImpl;
	mImpl->strm.zalloc = zQiAlloc;
    mImpl->strm.zfree = zQiFree;
    mImpl->strm.opaque = Z_NULL;
	mImpl->strm.next_in = Z_NULL;
	mImpl->strm.avail_in = 0;
	inflateInit2(&mImpl->strm, 32+15);
}

void QiDecompress::shutdown()
{
	if (mImpl)
	{
		inflateEnd(&mImpl->strm);
		QI_DELETE(mImpl);
		mImpl = NULL;
	}
}

bool QiDecompress::process(QiInputStream& in, QiOutputStream& out, int bytes)
{
	QI_ASSERT(mImpl, "Not initialized");
	const int chunkSize = 8192;
	char inBuf[chunkSize];
	char outBuf[chunkSize];
	while(bytes > 0)
	{
		int currChunk = QiMin(bytes, chunkSize);
		if (!in.readBuffer(inBuf, currChunk))
			return false;
		mImpl->strm.next_in = (Bytef*)inBuf;
		mImpl->strm.avail_in = currChunk;
		while(mImpl->strm.avail_in > 0)
		{
			mImpl->strm.next_out = (Bytef*)outBuf;
			mImpl->strm.avail_out = chunkSize;
			int status = inflate(&mImpl->strm, Z_BLOCK);
			if (status != Z_OK)
				return false;
			int written = chunkSize - mImpl->strm.avail_out;
			if (!out.writeBuffer(outBuf, written))
				return false;
		}
		bytes -= currChunk;
	}
	return true;
}


void QiDecompress::reset()
{	
	shutdown();
	init();
}

