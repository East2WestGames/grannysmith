#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"
#include "base/qimem.h"
#include "base/qidebug.h"

template <class T>
class QiHashTable
{
protected:
	enum Status
	{
		FREE,
		USED,
		DELETED
	};
	
	class Entry
	{
	public:
		Status status;
		T object;
	};

public:
	class Iterator
	{
		friend class QiHashTable<T>;
	public:
		inline bool hasNext()
		{
			return mCount < mTable.mCount;
		}
		
		inline T& next()
		{
			QI_ASSERT(hasNext(), "Reached end of hash table");
			while(mTable.mEntries[mIndex].status != USED)
				mIndex++;
			mCount++;
			return mTable.mEntries[mIndex++].object;
		}
		
		inline T& getCurrent()
		{
			QI_ASSERT(hasNext(), "Reached end of hash table");
			while(mTable.mEntries[mIndex].status != USED)
				mIndex++;
			return mTable.mEntries[mIndex].object;
		}
		
		inline int getCount() const
		{
			return mTable.getCount();
		}

		inline void reset() 
		{
			mIndex = 0;
			mCount = 0;
		}
	private:
		Iterator(const QiHashTable<T>& hashTable) : 
		mTable(hashTable), mIndex(0), mCount(0)
		{
		}
		
		const QiHashTable<T>& mTable;
		int mIndex;
		int mCount;
	};
	
	friend class Iterator;
	
	QiHashTable() : 
	mCapacity(0), mCount(0), mEntries(NULL)
	{
	}
	
	QiHashTable(int capacity) :
	mCapacity(0), mCount(0), mEntries(NULL)
	{
		grow(capacity);
	}
	
	~QiHashTable()
	{
		if (mEntries)
			QiFree(mEntries);
	}
	
	void put(const T& object)
	{
		if ((mCount+1)*2 > mCapacity)
			grow((mCount+1)*3);
		QiHash hash = object.getHash();
		int startPos = hashToIndex(hash);
		int pos = startPos;
		while(mEntries[pos].status == USED)
		{
			if (mEntries[pos].object == object)
				break;
			pos = (pos+1) % mCapacity;
		}
		if (mEntries[pos].status != USED)
			mCount++;
		mEntries[pos].status = USED;
		mEntries[pos].object = object;
	}

	bool remove(const T& object)
	{
		if (mEntries == NULL)
			return false;
		QiHash hash = object.getHash();
		int startPos = hashToIndex(hash);
		int pos = startPos;
		while(mEntries[pos].status != FREE && !(mEntries[pos].object == object))
		{
			pos=(pos+1)%mCapacity;
			if (pos == startPos)
				return false;
		}
		if (mEntries[pos].status == FREE)
			return false;
		mEntries[pos].status = DELETED;
		mCount--;
		return true;
	}

	bool contains(const T& object) const
	{
		if (mEntries == NULL)
			return false;
		QiHash hash = object.getHash();
		int startPos = hashToIndex(hash);
		int pos = startPos;
		int jumps = 0;
		while(mEntries[pos].status != FREE)
		{
			if (jumps++ == 100)
			{
				QI_WARNING("More than 100 jumps in hash table search");
			}

			if (mEntries[pos].status == USED && mEntries[pos].object == object)
				return true;

			pos=(pos+1)%mCapacity;
			if (pos == startPos)
				return false;
		}
		return false;
	}
	
	bool get(T& object) const
	{
		if (mEntries == NULL)
			return false;
		QiHash hash = object.getHash();
		int startPos = hashToIndex(hash);
		int pos = startPos;
		int jumps = 0;
		while(mEntries[pos].status != FREE)
		{
			if (jumps++ == 100)
			{
				QI_WARNING("More than 100 jumps in hash table search");
			}

			if (mEntries[pos].status == USED && mEntries[pos].object == object)
			{
				object = mEntries[pos].object;
				return true;
			}

			pos=(pos+1)%mCapacity;
			if (pos == startPos)
				return false;
		}
		return false;
	}

	T* getPointer(const T& object)
	{
		if (mEntries == NULL)
			return NULL;
		QiHash hash = object.getHash();
		unsigned int startPos = hashToIndex(hash);
		unsigned int pos = startPos;
		int jumps = 0;
		while(mEntries[pos].status != FREE)
		{
			if (jumps++ == 100)
			{
				QI_WARNING("More than 100 jumps in hash table search " + mCount + "/" + mCapacity);
			}

			if (mEntries[pos].status == USED && mEntries[pos].object == object)
				return &mEntries[pos].object;

			pos=(pos+1)%(unsigned int)mCapacity;
			if (pos == startPos)
				return NULL;
		}
		return NULL;
	}
	
	inline int getCount() const
	{
		return mCount;
	}
	
	inline void clear()
	{
		mCount = 0;
		for(int i=0; i<mCapacity; i++)
			mEntries[i].status = FREE;
	}
	
	Iterator getIterator() const
	{
		return Iterator(*this);
	}
	
protected:
	inline unsigned int hashToIndex(QiHash hash) const
	{
		return hash%((unsigned int)(mCapacity-1));
	}

	inline bool grow(int newCapacity)
	{
		if (newCapacity < mCount)
			return false;

		Entry* newEntries = (Entry*)QiAlloc(newCapacity*sizeof(Entry));
		if (!newEntries)
			return false;

		for(int i=0; i<newCapacity; i++)
		{
			newEntries[i].status = FREE;
			new (&newEntries[i].object) T;
		}

		mCount = 0;
		for(int i=0; i<mCapacity; i++)
		{
			if (mEntries[i].status == USED)
			{
				QiHash hash = mEntries[i].object.getHash();
				unsigned int startPos = hash%((unsigned int)(newCapacity-1));
				unsigned int pos = startPos;
				while(newEntries[pos].status == USED)
					pos = (pos+1) % (unsigned int)newCapacity;
				newEntries[pos].status = USED;
				newEntries[pos].object = mEntries[i].object;
				mCount++;
			}
		}

		//Free old memory if not allocated inplace
		if (mEntries != NULL && (char*)mEntries != ((char*)this)+sizeof(QiHashTable<T>))
			QiFree(mEntries);

		mEntries = newEntries;
		mCapacity = newCapacity;
		return true;
	}

	int mCapacity;
	int mCount;
	Entry* mEntries;
};

template <class T, int capacity>
class QiHashTableInplace : public QiHashTable<T>
{
public:
	inline QiHashTableInplace() : QiHashTable<T>()
	{
		QiHashTable<T>::mEntries = (class QiHashTable<T>::Entry*)this->mArr;
		QiHashTable<T>::mCapacity = capacity;
		for(int i=0; i<QiHashTable<T>::mCapacity; i++)
			QiHashTable<T>::mEntries[i].status = QiHashTable<T>::FREE;
	}

	inline QiHashTableInplace(int count) : QiHashTable<T>()
	{
		QiHashTable<T>::mEntries = (class QiHashTable<T>::Entry*)mArr;
		QiHashTable<T>::mCapacity = capacity;
		for(int i=0; i<QiHashTable<T>::mCapacity; i++)
			QiHashTable<T>::mEntries[i].status = QiHashTable<T>::FREE;
		QiHashTable<T>::grow(count);
	}
	
	inline ~QiHashTableInplace()
	{
		if (QiHashTable<T>::mEntries == (class QiHashTable<T>::Entry*)mArr)
			QiHashTable<T>::mEntries = NULL;
	}

protected:
	char mArr[capacity * sizeof(class QiHashTable<T>::Entry)];
};

