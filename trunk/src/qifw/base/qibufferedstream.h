#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qistream.h"

const size_t QI_BUFFERED_STREAM_SIZE = 8192;

class QiBufferedOutputStream : public QiOutputStream
{
public:
	QiBufferedOutputStream();
	virtual ~QiBufferedOutputStream();

	virtual bool flush();

protected:
	virtual bool writeInternal(const char* buffer, size_t bytes);

	virtual bool writeBuffered(const char* buffer, size_t bytes) = 0;
	
	char mBuffer[QI_BUFFERED_STREAM_SIZE];
	int mCount;
};



