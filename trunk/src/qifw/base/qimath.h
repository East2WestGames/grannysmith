#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"
#include "base/qidebug.h"

#include <stdlib.h>
#include <math.h>
#include <float.h>

#if defined(QI_WIN32) || defined(QI_MACOS)
#include <xmmintrin.h>
#endif

const float QI_PI = 3.14159265358979323846f;
const float QI_TWO_PI = QI_PI * 2.0f;
const float QI_HALF_PI = QI_PI * 0.5f;
const float QI_E =  2.71828182845904523536f;
const float QI_FLOAT_MAX = FLT_MAX;
const float QI_FLOAT_MIN = FLT_MIN;
const int QI_INT_MAX = 0x7fffffff;
const float QI_FLOAT_EPSILON = FLT_EPSILON;
const float QI_ORTHONORMAL_TOLERANCE = 0.001f;
const float QI_LN2 = 0.6931471805599453f;


inline int QiAbs(int i)
{
	return ::abs(i);
}


inline long long QiAbs(long long l)
{
	if (l < 0) 
		return -l;
	else
		return l;
}


inline int QiCeil(float f)
{
	return int(::ceilf(f));
}


inline int QiFloor(float f)
{
	return int(::floorf(f));
}


inline int QiRound(float a)
{
	return int(::floorf(a + 0.5f));
}


inline float QiSin(float arg)
{
	return ::sinf(arg);
}


inline float QiCos(float arg)
{
	return ::cosf(arg);
}


inline float QiTan(float arg)
{
	return ::tanf(arg);
}


inline float QiASin(float arg)
{
	return ::asinf(QiClamp(arg, -1.0f, 1.0f));
}


inline float QiACos(float arg)
{
	return ::acosf(QiClamp(arg, -1.0f, 1.0f));
}


inline float QiATan(float arg)
{
	return ::atanf(arg);
}


inline float QiATan2(float y, float x)
{
	return ::atan2f(y, x);
}


inline float QiAbs(float f)
{
	return ::fabsf(f);
}


inline float QiSqrt(float arg)
{
	QI_ASSERT(arg >= 0, "Square root on negative value");
#if defined(QI_WIN32) || defined(QI_MACOS)
	float out;
	_mm_store_ss( &out, _mm_sqrt_ss( _mm_load_ss( &arg ) ) );
	return out;
#else
	return ::sqrtf(arg);
#endif	
}


inline float QiRSqrt(float arg)
{
	QI_ASSERT(arg >= 0, "Square root on negative value");
#if defined(QI_WIN32) || defined(QI_MACOS)
	float out;
	_mm_store_ss( &out, _mm_sqrt_ss( _mm_load_ss( &arg ) ) );
	return 1.0f/out;
#else
return 1.0f/::sqrtf(arg);
#endif
}


inline float QiRSqrtFast(float arg)
{
	QI_ASSERT(arg >= 0, "Square root on negative value");
#if defined(QI_WIN32) || defined(QI_MACOS)
	float out;
	_mm_store_ss( &out, _mm_rsqrt_ss( _mm_load_ss( &arg ) ) );
	return out;
#else
return 1.0f/::sqrtf(arg);
#endif
}


inline float QiSqrtCarmack(float x)
{
	QI_ASSERT(x >= 0, "Square root on negative value");
    float xhalf = 0.5f*x;
    int i = *(int*)&x;
    i = 0x5f3759df - (i>>1);
    x = *(float*)&i;
    x = x*(1.5f - xhalf*x*x);
    return 1.0f/x;
}


inline float QiRSqrtCarmack(float x)
{
	QI_ASSERT(x >= 0, "Square root on negative value");
    float xhalf = 0.5f*x;
    int i = *(int*)&x;
    i = 0x5f3759df - (i>>1);
    x = *(float*)&i;
    x = x*(1.5f - xhalf*x*x);
    return x;
}


inline float QiPow(float arg, float exp)
{
	return ::powf(arg, exp);
}


inline float QiMod(float a, float b)
{
	return ::fmodf(a, b);
}


inline float QiExp(float arg)
{
	return ::expf(arg);
}

inline float QiLog10(float arg)
{
	return ::log10f(arg);
}

inline float QiLog2(float arg)
{
	return ::logf(arg)/QI_LN2;
}

inline float QiLog(float arg)
{
	return ::logf(arg);
}

inline float QiSign(float arg)
{
	if (arg == 0.0f)
		return 0.0f;
	else
		return (arg < 0.0f ? -1.0f : 1.0f);
}

inline int QiSign(int arg)
{
	if (arg == 0)
		return 0;
	else
		return (arg < 0 ? -1 : 1);
}


inline bool QiIsPowerOfTwo(unsigned int arg)
{
	return (arg & (arg - 1)) == 0; 
}


inline unsigned int QiIsAligned(unsigned int arg, unsigned int alignment)
{
	QI_ASSERT(QiIsPowerOfTwo(alignment), "Alignment must be power of two");
	return (arg & (alignment-1)) == 0;
}


inline unsigned int QiRoundUpToAlignment(unsigned int arg, unsigned int alignment)
{
	QI_ASSERT(QiIsPowerOfTwo(alignment), "Alignment must be power of two");
	return (arg + (alignment-1)) & ~(alignment-1);
}

inline int QiPopCount(QiUInt32 i)
{
    i = i - ((i >> 1) & 0x55555555U);
    i = (i & 0x33333333U) + ((i >> 2) & 0x33333333U);
    return (((i + (i >> 4)) & 0x0F0F0F0FU) * 0x01010101U) >> 24;
}

//Signed shortest rotation in radians between two angles 
inline float QiAngle(float ang1, float ang2)
{
	return QiATan2(QiSin(ang1-ang2), QiCos(ang1-ang2));
}
