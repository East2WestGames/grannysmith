#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"

class QiSystem
{
public:
	enum FpuPrecision
	{
		FPU_32_BITS,
		FPU_64_BITS,
		FPU_80_BITS
	};

	static int getCpuCount();
	static class QiString getArchitecture();
	static class QiString getExecutablePath();
	static class QiString getHomeDir();
	static class QiString getComputerName();
	static class QiString getUserName();
	static QiInt64 getCurrentDateTime();
	static class QiString getDateTimeString(QiInt64 dateTime);
	static QiInt64 getDateTime(int year, int month, int day, int hour, int minute, int second);
	static QiInt32 getProcessId();
	static bool isVista();
	static void setFpu(FpuPrecision precision=FPU_80_BITS, bool interuptDivisionByZero=false);
	static void exit();
	static void openUrl(const QiString& url);
};

volatile unsigned long QiInterlockedCompareExchange(volatile unsigned long* dest, volatile unsigned long exchange, volatile unsigned long comparand);
void QiInterlockedIncrement(volatile unsigned long* dest);
void QiMemoryBarrier();
void QiPause();

