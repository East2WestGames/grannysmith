/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qitimer.h"

#ifdef QI_WIN32
#include "windows.h"

QiTimer::QiTimer()
{
	reset();
}

void QiTimer::reset()
{
	LARGE_INTEGER i;
	QueryPerformanceCounter(&i);
	mStart.usec = (long long)i.QuadPart;
}

long long QiTimer::getTicksPerSecond()
{
	LARGE_INTEGER frequency;
	QueryPerformanceFrequency(&frequency);
	return (long long)frequency.QuadPart;
}

long long QiTimer::getTicks() const
{
	LARGE_INTEGER finish;
	QueryPerformanceCounter(&finish);
	return ((long long)finish.QuadPart)-mStart.usec;
}

#else

#include <sys/time.h>
#include <time.h>

QiTimer::QiTimer()
{
	reset();
}

void QiTimer::reset()
{
	struct timeval tp;
	::gettimeofday(&tp, NULL);
	mStart.sec = tp.tv_sec;
	mStart.usec = tp.tv_usec;
}

long long QiTimer::getTicksPerSecond()
{
	return 1000000;
}

long long QiTimer::getTicks() const
{
	struct timeval tp;
	::gettimeofday(&tp, NULL);
	QiUInt64 sec = tp.tv_sec;
	QiUInt64 usec = tp.tv_usec;
	return (sec-mStart.sec)*(QiUInt64)1000000 + (usec-mStart.usec);
}

#endif

float QiTimer::getTime() const
{
	return (float)((double)getTicks() / (double)getTicksPerSecond());
}

