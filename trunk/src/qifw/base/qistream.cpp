/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qistream.h"
#include "base/qistring.h"

#include <string.h>


// -------- QiInputStream ----------

QiInputStream::QiInputStream(QiByteOrder byteOrder) :
mInputByteOrder(byteOrder), mInputByteCount(0)
{
}


QiInputStream::~QiInputStream()
{
}


void QiInputStream::resetInputStream()
{
	mInputByteCount = 0;
}



void QiInputStream::setInputByteOrder(QiByteOrder byteOrder)
{
mInputByteOrder = byteOrder;
}


QiByteOrder QiInputStream::getInputByteOrder() const
{
	return mInputByteOrder;
}


bool QiInputStream::readBool(bool& val)
{
	QiInt8 a;
	if (!readInternalCount(&a, sizeof(a)))
		return false;
	val = (a != 0);
	return true;
}


bool QiInputStream::readInt8(QiInt8& val)
{
	return readInternalCount(&val, sizeof(val));
}


bool QiInputStream::readInt16(QiInt16& val)
{
	if (!readInternalCount((char*)&val, sizeof(val)))
		return false;
	if (mInputByteOrder!=QI_HOST_BYTE_ORDER)
		val = QiSwapByteOrder(val);
	return true;
}


bool QiInputStream::readInt32(QiInt32& val)
{
	if (!readInternalCount((char*)&val, sizeof(val)))
		return false;
	if (mInputByteOrder!=QI_HOST_BYTE_ORDER)
		val = QiSwapByteOrder(val);
	return true;
}


bool QiInputStream::readInt64(QiInt64& val)
{
	if (!readInternalCount((char*)&val, sizeof(val)))
		return false;
	if (mInputByteOrder!=QI_HOST_BYTE_ORDER)
		val = QiSwapByteOrder(val);
	return true;
}


bool QiInputStream::readFloat32(QiFloat32& val)
{
	if (!readInternalCount((char*)&val, sizeof(val)))
		return false;
	return true;
}


bool QiInputStream::readFloat64(QiFloat64& val)
{
	if (!readInternalCount((char*)&val, sizeof(val)))
		return false;
	return true;
}


bool QiInputStream::readString(char* string, int maxLen)
{
	QiInt32 l;
	string[0] = 0; //Just in case
	if (!readInt32(l))
		return false;

	int readLen = l > maxLen-1 ? maxLen-1 : l;
	
	if (!readBuffer(string, readLen))
		return false;
		
	string[readLen] = 0;

	//Read the remainder to flush stream
	QiUInt8 tmp;
	for(int i=readLen; i<l; i++)
		if (!readUInt8(tmp))
			return false;

	return true;
}


bool QiInputStream::readLine(char* string, int maxLength)
{
	string[0] = 0;
	int count = 0;
	while(hasMoreData())
	{
		char c;
		if (!readInt8(c))
			return false;
		if (c == 13)
			continue;
		if (c == '\n')
			return true;
		if (count+1 < maxLength)
		{
			string[count] = c;
			string[count+1] = '\0';
			count++;
		}
	}	
	return true;
}


bool QiInputStream::readBuffer(void* buffer, size_t bytes)
{
	return readInternalCount((char*)buffer, bytes);
}


bool QiInputStream::readBuffer(QiOutputStream& out, size_t bytes)
{
	char data[4096];
	while(bytes > 0)
	{
		size_t z = bytes < 4096 ? bytes : 4096;
		if (!readInternalCount(data, z))
			return false;
		if (!out.writeBuffer(data, z))
			return false;
		bytes -= z;
	}
	return true;
}


// -------- QiOutputStream ----------


QiOutputStream::QiOutputStream(QiByteOrder byteOrder) :
mOutputByteOrder(byteOrder), mOutputByteCount(0)
{
}


QiOutputStream::~QiOutputStream()
{
}


void QiOutputStream::resetOutputStream()
{
	mOutputByteCount = 0;
}


void QiOutputStream::setOutputByteOrder(QiByteOrder byteOrder)
{
	mOutputByteOrder = byteOrder;
}


QiByteOrder QiOutputStream::getOutputByteOrder() const
{
	return mOutputByteOrder;
}


bool QiOutputStream::writeBool(bool val)
{
	QiInt8 a = (QiInt8)val;
	return writeInternalCount(&a, sizeof(a));
}


bool QiOutputStream::writeInt8(QiInt8 val)
{
	return writeInternalCount(&val, sizeof(val));
}


bool QiOutputStream::writeInt16(QiInt16 val)
{
	if (mOutputByteOrder != QI_HOST_BYTE_ORDER)
		val = QiSwapByteOrder(val);
	return writeInternalCount((char*)&val, sizeof(val));
}


bool QiOutputStream::writeInt32(QiInt32 val)
{
	if (mOutputByteOrder != QI_HOST_BYTE_ORDER)
		val = QiSwapByteOrder(val);
	return writeInternalCount((char*)&val, sizeof(val));
}


bool QiOutputStream::writeInt64(QiInt64 val)
{
	if (mOutputByteOrder != QI_HOST_BYTE_ORDER)
		val = QiSwapByteOrder(val);
	return writeInternalCount((char*)&val, sizeof(val));
}


bool QiOutputStream::writeFloat32(QiFloat32 val)
{
	return writeInternalCount((char*)&val, sizeof(val));
}


bool QiOutputStream::writeFloat64(QiFloat64 val)
{
	return writeInternalCount((char*)&val, sizeof(val));
}


bool QiOutputStream::writeString(const char* string)
{
	QiInt32 l = strlen(string);
	if (l < 0)
		l = 0;
	if (!writeInt32(l))
		return false;
	if (!writeBuffer(string, l))
		return false;
	return true;
}


bool QiOutputStream::writeLine(const char* string)
{
	QiString tmp = QiString(string) + "\n";
	return writeBuffer(tmp.c_str(), tmp.getLength());
}


bool QiOutputStream::writeBuffer(const void* buffer, size_t bytes)
{
	return writeInternalCount((const char*)buffer, bytes);
}


bool QiOutputStream::writeBuffer(QiInputStream& in, size_t bytes)
{
	char data[4096];
	while(bytes > 0)
	{
		size_t z = bytes < 4096 ? bytes : 4096;
		if (!in.readBuffer(data, z))
			return false;
		if (!writeInternalCount(data, z))
			return false;
		bytes -= z;
	}
	return true;
}


bool QiOutputStream::flush()
{
	return true;
}

