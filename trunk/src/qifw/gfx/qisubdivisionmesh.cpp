/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "gfx/qisubdivisionmesh.h"

QiSubdivisionMesh::QiSubdivisionMesh() :
mControlMesh(NULL)
{
}

QiSubdivisionMesh::QiSubdivisionMesh(const QiMesh& controlMesh)
{
	setControlMesh(controlMesh);
}

QiSubdivisionMesh::QiSubdivisionMesh(const QiSubdivisionMesh& controlMesh)
{
	setControlMesh(controlMesh);
}

void QiSubdivisionMesh::setControlMesh(const QiMesh& controlMesh)
{
	mControlMesh = &controlMesh;
	mVertices.clear();

	//Add existing vertices
	for(int i=0; i<mControlMesh->getVertexCount(); i++)
	{
		const QiVec3& p = mControlMesh->getVertex(i).point;
		mVertices.add(Vertex(p));
	}

	//Edge vertices
	for(int i=0; i<mControlMesh->getEdgeCount(); i++)
	{
		const QiVec3& p0 = mControlMesh->getVertex(mControlMesh->getEdge(i).vertices[0]).point;
		const QiVec3& p1 = mControlMesh->getVertex(mControlMesh->getEdge(i).vertices[1]).point;
		mVertices.add(Vertex((p0+p1)*0.5f));
	}
	
	mFaces.clear();

	//Add faces, four per original face
	int edgeStart = mControlMesh->getVertexCount();
	int v0,v1,v2;
	for(int i=0; i<mControlMesh->getFaceCount(); i++)
	{
		const Face& face = mControlMesh->getFace(i);
		v0 = face.vertices[0];
		v1 = edgeStart + face.edges[0];
		v2 = edgeStart + face.edges[2];
		mFaces.add(Face(v0, v1, v2));		
		v0 = face.vertices[1];
		v1 = edgeStart + face.edges[1];
		v2 = edgeStart + face.edges[0];
		mFaces.add(Face(v0, v1, v2));		
		v0 = face.vertices[2];
		v1 = edgeStart + face.edges[2];
		v2 = edgeStart + face.edges[1];
		mFaces.add(Face(v0, v1, v2));		
		v0 = edgeStart + face.edges[0];
		v1 = edgeStart + face.edges[1];
		v2 = edgeStart + face.edges[2];
		mFaces.add(Face(v0, v1, v2));		
	}
	
	computeConnectivity();	
}

void QiSubdivisionMesh::update()
{
	int edgeStart = mControlMesh->getVertexCount();
	//Set new vertex positions
	for(int i=0; i<mControlMesh->getVertexCount(); i++)
		mVertices[i].point = mControlMesh->getVertex(i).point;
	for(int i=0; i<mControlMesh->getEdgeCount(); i++)
	{
		const QiVec3& p0 = mControlMesh->getVertex(mControlMesh->getEdge(i).vertices[0]).point;
		const QiVec3& p1 = mControlMesh->getVertex(mControlMesh->getEdge(i).vertices[1]).point;
		mVertices[edgeStart+i].point = (p0+p1)*0.5f;
	}
	
	QiArray<QiVec3> tmp;
	tmp.redim(mVertices.getCount());

	float alpha = 0.5f;
	//Average and normalize
	for(int i=0; i<mVertices.getCount(); i++)
	{
		QiVec3 acc;
		int count = getVertexEdgeCount(i);
		for(int j=0; j<count; j++)
		{
			int v = mEdges[getVertexEdgeIndex(i, j)].getOtherVertex(i);
			acc += mVertices[v].point;
		}
		if (count > 0)
			tmp[i] = QiVec3(mVertices[i].point*alpha + acc/(float)count*(1.0f-alpha));
		else 
			tmp[i] = mVertices[i].point;
	}	
	
	for(int i=0; i<mVertices.getCount(); i++)
	{
		mVertices[i].point = tmp[i];
	}
}

void QiSubdivisionMesh::wrinkle(float amount)
{
	//Wrinkle only vertices. These should have exactly two edges, always.
	for(int i=mControlMesh->getVertexCount(); i<mVertices.getCount(); i++)
	{
		//Original edge
		int e = i - mControlMesh->getVertexCount();
		int v0 = mControlMesh->getEdge(e).vertices[0];
		int v1 = mControlMesh->getEdge(e).vertices[1];
				
		const QiVec3& n0 = mControlMesh->getVertex(v0).normal;
		const QiVec3& n1 = mControlMesh->getVertex(v1).normal;
		QiVec3 n = normalize(n0+n1);
		//Current edge length
		float l = length(mControlMesh->getVertex(v0).point - mControlMesh->getVertex(v1).point);
		//Old edge in controlmesh
		float f = mControlMesh->getEdge(e).getOriginalLength() / l - 1.0f;
		//f -= 0.1f;
		float d = amount * QiClamp(f, 0.0f, 3.0f);
		mVertices[i].point -= n*d;
	}
}
