/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "gfx/qimtx.h"
#include "base/qistream.h"
#include "base/qimemorystream.h"
#include "base/qicompress.h"
#include "base/qidecompress.h"
#include "gfx/qipngencoder.h"
#include "gfx/qipngdecoder.h"
#include "gfx/qijpegencoder.h"
#include "gfx/qijpegdecoder.h"


void QiMtxEncoder::init(QiOutputStream& out, int w, int h)
{
	mStream = &out;
	mWidth = w;
	mHeight = h;
}


bool QiMtxEncoder::encode(unsigned char* rgbaData)
{
	if (!mStream)
		return false;
		
	unsigned char* rgbData = (unsigned char*)QiAlloc(mWidth*mHeight*3);
	unsigned char* aData = (unsigned char*)QiAlloc(mWidth*mHeight);
	for(int i=0; i<mWidth*mHeight; i++)
	{
		rgbData[i*3+0] = rgbaData[i*4+0];
		rgbData[i*3+1] = rgbaData[i*4+1];
		rgbData[i*3+2] = rgbaData[i*4+2];
		aData[i] = rgbaData[i*4+3];
	}
	
	QiMemoryStream<8> rgbCompressed;
	QiJpegEncoder jpg(rgbCompressed, mWidth, mHeight, 80);
	jpg.encode(rgbData);
	
	QiMemoryStream<8> aDataStream;
	aDataStream.writeBuffer(aData, mWidth*mHeight);
	QiMemoryStream<8> aCompressed;
	QiCompress z;
	z.process(aDataStream, aCompressed, aDataStream.getSize(), true);
	
	mStream->writeUInt32(MTX_RGBA8);
	mStream->writeUInt32((unsigned int)mWidth);
	mStream->writeUInt32((unsigned int)mHeight);
	mStream->writeUInt32((int)rgbCompressed.getSize());
	mStream->writeBuffer(rgbCompressed, rgbCompressed.getSize());
	mStream->writeUInt32((int)aCompressed.getSize());
	mStream->writeBuffer(aCompressed, aCompressed.getSize());
	
	QiFree(rgbData);
	QiFree(aData);
	
	return true;
}


void QiMtxDecoder::init(QiInputStream& in, int size)
{
	mStream = &in;
	mSize = size;

	QiUInt32 type = 0;
	mStream->readUInt32(type);
	if (type != MTX_RGBA8)
		return;

	mStream->readUInt32((unsigned int&)mWidth);
	mStream->readUInt32((unsigned int&)mHeight);
}


bool QiMtxDecoder::decode(unsigned char* rgbaData)
{
	if (!mStream || !mWidth || !mHeight)
		return false;

	QiUInt32 rgbSize = 0;
	mStream->readUInt32(rgbSize);
		
	unsigned char* rgbData = (unsigned char*)QiAlloc(mWidth*mHeight*3);
	QiJpegDecoder dec(*mStream, rgbSize);
	if (dec.getWidth() != mWidth || dec.getHeight() != mHeight)
		return false;
	dec.decode(rgbData);
	
	QiUInt32 aSize = 0;
	mStream->readUInt32(aSize);
	
	QiMemoryStream<8> aDataStream;
	QiDecompress z;
	z.process(*mStream, aDataStream, aSize);
	if (aDataStream.getSize() != mWidth * mHeight)
		return false;
	unsigned char* aData = (unsigned char*)aDataStream.getData();

	for(int i=0; i<mWidth*mHeight; i++)
	{
		rgbaData[i*4+0] = rgbData[i*3+0];
		rgbaData[i*4+1] = rgbData[i*3+1];
		rgbaData[i*4+2] = rgbData[i*3+2];
		rgbaData[i*4+3] = aData[i];
	}
	
	return true;
}

