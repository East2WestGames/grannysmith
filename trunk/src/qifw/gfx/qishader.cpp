/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qimem.h"
#include "file/qifileinputstream.h"
#include "gfx/qishader.h"
#include "gfx/qigl.h"
#include "math/qivec3.h"
#include <string.h>

QiShader::QiShader() :
mVertexShader(0), mFragmentShader(0), mProgram(0)
{
}


QiShader::QiShader(const char* vertexShader, const char* fragmentShader) :
mVertexShader(0), mFragmentShader(0), mProgram(0)
{
	load(vertexShader, fragmentShader);
}


QiShader::~QiShader()
{
	unload();
}

#if defined(QI_USE_OPENGL) || defined(QI_IOS)
	
bool QiShader::loadString(const char* vertexShader, const char* fragmentShader)
{
	mProgram = glCreateProgram();

	GLint ok;
	mVertexShader = glCreateShader(GL_VERTEX_SHADER);
	QiString vs = vertexShader;
#if !defined(QI_IOS) && !defined(QI_ANDROID)
	vs = vs.replace("lowp", "");
	vs = vs.replace("mediump", "");
#endif
	#if defined(QI_IOS) || defined(QI_ANDROID)
	vs = QiString("precision highp float;\n") + vs;	
	#endif
	vs = vs.replace("#ifdef VERTEX", "#if 1");
	vs = vs.replace("#ifdef FRAGMENT", "#if 0");
	const char* vsp = vs.c_str();
	glShaderSource(mVertexShader, 1, (const char**)&vsp, NULL);
	glCompileShader(mVertexShader);
	glGetShaderiv(mVertexShader, GL_COMPILE_STATUS, &ok);
	if (!ok)
	{
		QI_PRINT("Failed to compile vertex shader " + vertexShader);
		GLint len;
		char msg[1000];
		glGetShaderInfoLog(mVertexShader, 1000, &len, msg);
		QI_PRINT(msg);
		return false;
	}
	glAttachShader(mProgram, mVertexShader);

	mFragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	QiString fs = fragmentShader;
#if !defined(QI_IOS) && !defined(QI_ANDROID)
	fs = fs.replace("lowp", "");
	fs = fs.replace("mediump", "");
#endif
#if defined(QI_IOS) 
	fs = QiString("precision lowp float;\n") + fs;	
#elif defined(QI_ANDROID)
	fs = QiString("precision mediump float;\n") + fs;
#endif
	fs = fs.replace("#ifdef VERTEX", "#if 0");
	fs = fs.replace("#ifdef FRAGMENT", "#if 1");
	fs = fs.replace("#ifdef VERTEX", "#if 0");
	const char* fsp = fs.c_str();
	glShaderSource(mFragmentShader, 1, (const char**)&fsp, NULL);
	glCompileShader(mFragmentShader);
	glGetShaderiv(mFragmentShader, GL_COMPILE_STATUS, &ok);
	if (!ok)
	{
		QI_PRINT("Failed to compile fragment shader " + fs);
		GLint len;
		char msg[1000];
		glGetShaderInfoLog(mFragmentShader, 1000, &len, msg);
		QI_PRINT(msg);
		return false;
	}
	glAttachShader(mProgram, mFragmentShader);
	
	glLinkProgram(mProgram);

	cacheLocations();
	
	return true;
}

bool QiShader::load(const char* combinedShader)
{
	return load(combinedShader, combinedShader);
}

bool QiShader::load(const char* vertexShader, const char* fragmentShader)
{
	QiFileInputStream file;
	if (!file.open(vertexShader))
	{
		QI_PRINT("Could not find vertex shader " + vertexShader);
		return false;
	}
	int fz = file.getSize();
	char* vsSource = (char*)QiAlloc(fz+1);
	file.readBuffer(vsSource, fz);
	file.close();
	vsSource[fz] = 0;

	file.open(fragmentShader);
	if (!file.open(fragmentShader))
	{
		QI_PRINT("Could not find fragment shader " + fragmentShader);
		return false;
	}
	fz = file.getSize();
	char* fsSource = (char*)QiAlloc(fz+1);
	file.readBuffer(fsSource, fz);
	file.close();
	fsSource[fz] = 0;

	bool ret = loadString(vsSource, fsSource);

	QiFree(vsSource);
	QiFree(fsSource);

	return ret;
}


bool QiShader::loadMemory(const char* vertexShader, int vSize, const char* fragmentShader, int fSize)
{
	char* vsSource = (char*)QiAlloc(vSize+1);
	::memcpy(vsSource, vertexShader, vSize);
	vsSource[vSize] = 0;

	char* fsSource = (char*)QiAlloc(fSize+1);
	::memcpy(fsSource, fragmentShader, fSize);
	fsSource[fSize] = 0;

	bool ret = loadString(vsSource, fsSource);

	QiFree(vsSource);
	QiFree(fsSource);

	return ret;
}


void QiShader::cacheLocations()
{
	mMvpId = glGetUniformLocation(mProgram, "uMvpMatrix");
	mProjectionMatrixId = glGetUniformLocation(mProgram, "uProjectionMatrix");
	mModelViewMatrixId = glGetUniformLocation(mProgram, "uModelViewMatrix");
	mObjectMatrixId = glGetUniformLocation(mProgram, "uObjectMatrix");
	mColorId = glGetUniformLocation(mProgram, "uColor");
	mTexScaleId = glGetUniformLocation(mProgram, "uTexScale0");
	mTexOffsetId = glGetUniformLocation(mProgram, "uTexOffset0");

	aPosition = glGetAttribLocation(mProgram, "aPosition");
	aNormal = glGetAttribLocation(mProgram, "aNormal");
	aTexCoord = glGetAttribLocation(mProgram, "aTexCoord");
	aAlpha = glGetAttribLocation(mProgram, "aAlpha");
}


void QiShader::unload()
{
	if (mProgram && mVertexShader)
		glDetachShader(mProgram, mVertexShader);
	if (mProgram && mFragmentShader)
		glDetachShader(mProgram, mFragmentShader);
	if (mVertexShader)
		glDeleteShader(mVertexShader);
	if (mFragmentShader)
		glDeleteShader(mFragmentShader);
	if (mProgram)
		glDeleteProgram(mProgram);
}

	
void QiShader::use()
{
	glUseProgram(mProgram);
}

	
void QiShader::useDefault()
{
	glUseProgram(0);
}


void QiShader::setUniform(const char* name, int value)
{
	glUniform1i(glGetUniformLocation(mProgram, name), value);
}


void QiShader::setUniform(const char* name, float value)
{
	glUniform1f(glGetUniformLocation(mProgram, name), value);
}


void QiShader::setUniform(const char* name, const QiVec3& value)
{
	glUniform3fv(glGetUniformLocation(mProgram, name), 1, &value.x);
}


void QiShader::setUniform(const char* name, float x, float y, float z, float w)
{
	glUniform4f(glGetUniformLocation(mProgram, name), x, y, z, w);
}


void QiShader::setUniform2(const char* name, float* arr)
{
	glUniform2fv(glGetUniformLocation(mProgram, name), 1, arr);
}


void QiShader::setUniform4(const char* name, float* arr)
{
	glUniform4fv(glGetUniformLocation(mProgram, name), 1, arr);
}


void QiShader::setUniform4x4(const char* name, float* arr)
{
	glUniformMatrix4fv(glGetUniformLocation(mProgram, name), 1, 0, arr);
}




void QiShader::setUniform(int location, int value)
{
	glUniform1i(location, value);
}


void QiShader::setUniform(int location, float value)
{
	glUniform1f(location, value);
}


void QiShader::setUniform(int location, const QiVec3& value)
{
	glUniform3fv(location, 1, &value.x);
}


void QiShader::setUniform(int location, float x, float y, float z, float w)
{
	glUniform4f(location, x, y, z, w);
}


void QiShader::setUniform2(int location, float* arr)
{
	glUniform2fv(location, 1, arr);
}


void QiShader::setUniform4(int location, float* arr)
{
	glUniform4fv(location, 1, arr);
}


void QiShader::setUniform4x4(int location, float* arr)
{
	glUniformMatrix4fv(location, 1, 0, arr);
}




int QiShader::getAttributeLocation(const char* name)
{
	return glGetAttribLocation(mProgram, name);
}


#endif