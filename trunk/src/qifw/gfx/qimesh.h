#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiarray.h"
#include "base/qihashtable.h"
#include "math/qivec3.h"
#include "geometry/qiplane.h"

class QiMesh
{
public:
	class Vertex
	{
	public:
		inline Vertex() 
		{
			edgeCount = 0;
			faceCount = 0;
		}
		inline Vertex(const QiVec3& point) : point(point)
		{
			edgeCount = 0;
			faceCount = 0;
		}
		inline Vertex(const QiVec3& point, const QiVec3& normal) : point(point), normal(normal)
		{
			edgeCount = 0;
			faceCount = 0;
		}
		QiVec3 point;
		QiVec3 normal;
		int edgeCount;
		int edgeStart;
		int faceCount;
		int faceStart;
	};
	
	class Face
	{
	public:
		inline Face()
		{
		}
		inline Face(int v0, int v1, int v2)
		{
			vertices[0] = v0;
			vertices[1] = v1;
			vertices[2] = v2;
		}
		int vertices[3];
		int edges[3];
		QiVec3 normal;
	};

	class Edge
	{
	public:
		inline Edge()
		{
			vertices[0] = -1;
			vertices[1] = -1;
			faces[0] = -1;
			faces[1] = -1;
		}
		inline Edge(int v0, int v1)
		{
			vertices[0] = v0;
			vertices[1] = v1;
			faces[0] = -1;
			faces[1] = -1;
		}
		inline int getOtherVertex(int vertex) const
		{
			if (vertices[0] != vertex)
				return vertices[0];
			else
				return vertices[1];
		}
		inline void setOriginalLength(float l)
		{
			originalLength = l;
		}
		inline float getOriginalLength() const
		{
			return originalLength;
		}
		int vertices[2];
		int faces[2];
		float originalLength;
	};

	class EdgeIndex
	{
	public:
		inline EdgeIndex()
		{
		}

		inline EdgeIndex(int v0, int v1)
		{
			set(v0, v1, 0);
		}

		inline EdgeIndex(int v0, int v1, int index)
		{
			set(v0, v1, index);
		}

		inline void set(int v0, int v1, int index)
		{
			if (v0 < v1)
			{
				vertices[0] = v0;
				vertices[1] = v1;
			}
			else
			{
				vertices[0] = v1;
				vertices[1] = v0;
			}
			this->index = index;
		}

		inline bool operator==(const EdgeIndex& other) const
		{
			return vertices[0]==other.vertices[0] && vertices[1]==other.vertices[1];
		}
		
		inline QiHash getHash() const
		{
			return (vertices[1]<<16) + vertices[0];
		}

		int vertices[2];
		int index;
	};

public:
	QiMesh();
	~QiMesh();

	void set(const QiVec3* points, int vertexCount, const int* faces, int faceCount);
	void computeNormals();
	void computeConnectivity();
	void setOriginalEdgeLength();
	void split(const QiPlane& plane, QiMesh* out0, QiMesh* out1, float eps = 0.0f);

	inline void updateVertex(int index, const QiVec3& point)
	{
		mVertices[index].point = point;
	}

	inline int getVertexCount() const
	{
		return mVertices.getCount();
	}

	inline const Vertex& getVertex(int index) const
	{
		return mVertices[index];
	}

	inline Vertex& getVertex(int index)
	{
		return mVertices[index];
	}

	inline int getFaceCount() const
	{
		return mFaces.getCount();
	}

	inline const Face& getFace(int index) const
	{
		return mFaces[index];
	}

	inline int getEdgeCount() const
	{
		return mEdges.getCount();
	}

	inline const Edge& getEdge(int index) const
	{
		return mEdges[index];
	}

	inline int getEdgeIndex(int v0, int v1) const
	{
		EdgeIndex ei(v0, v1);
		if (mEdgeIndex.get(ei))
			return (int)ei.index;
		else
			return -1;
	}
	
	inline int getVertexEdgeCount(int vertex) const
	{
		return mVertices[vertex].edgeCount;
	}

	inline int getVertexEdgeIndex(int vertex, int edge) const
	{
		return mVertexEdgeList[mVertices[vertex].edgeStart+edge];
	}

	inline int getVertexFaceCount(int vertex) const
	{
		return mVertices[vertex].faceCount;
	}

	inline int getVertexFaceIndex(int vertex, int face) const
	{
		return mVertexFaceList[mVertices[vertex].faceStart+face];
	}
	
protected:
	QiArray<Vertex> mVertices;
	QiArray<Face> mFaces;
	QiArray<Edge> mEdges;
	QiHashTable<EdgeIndex> mEdgeIndex;
	QiArray<int> mVertexEdgeList;
	QiArray<int> mVertexFaceList;
};


