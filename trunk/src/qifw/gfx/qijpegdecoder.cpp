/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include <stdio.h>
#include <string.h>

#include "base/qiconfig.h"
#include "base/qimem.h"
#include "base/qistream.h"

//#ifdef QI_WIN32
extern "C"
{
//#endif
#include "gfx/jpeg/jpeglib.h"
//#ifdef QI_WIN32
}
//#endif

#include "base/qidebug.h"
#include "gfx/qijpegdecoder.h"

#include <setjmp.h>

static const int INPUT_BUF_SIZE = 8192;


struct my_src_mgr
{
        struct jpeg_source_mgr pub;
        JOCTET eoi_buffer[2];
		unsigned char buffer[INPUT_BUF_SIZE];
		QiInputStream * inStream;
		int current;
		int size;
};


static void init_source(j_decompress_ptr cinfo)
{
    struct my_src_mgr *src = (struct my_src_mgr *)cinfo->src;
	src->pub.next_input_byte = src->buffer;
	src->pub.bytes_in_buffer = 0;
	src->current = 0;
}


static int fill_input_buffer(j_decompress_ptr cinfo)
{
    struct my_src_mgr *src = (struct my_src_mgr *)cinfo->src;
	int size = src->size - src->current;
	if (size > INPUT_BUF_SIZE)
		size = INPUT_BUF_SIZE;
	src->inStream->readBuffer((char*)src->buffer, size);
	src->current += size;
	src->pub.next_input_byte = src->buffer;
	src->pub.bytes_in_buffer = size;
	return 1;
}


static void skip_input_data(j_decompress_ptr cinfo, long num_bytes)
{
    struct my_src_mgr *src = (struct my_src_mgr *)cinfo->src;
	if (num_bytes > 0)
	{
		while (num_bytes > (long)src->pub.bytes_in_buffer)
		{
			num_bytes -= (long)src->pub.bytes_in_buffer;
			fill_input_buffer(cinfo);
		}
	}
	src->pub.next_input_byte += num_bytes;
	src->pub.bytes_in_buffer -= num_bytes;
}


static void term_source(j_decompress_ptr cinfo)
{
}


static void dummyExit(j_common_ptr err);


class QiJpegDecoderImpl
{
public:
	QiJpegDecoderImpl()
	{	
		cinfo.err = jpeg_std_error(&jerr);
		cinfo.client_data = this;
		jerr.error_exit = dummyExit;
		jpeg_create_decompress(&cinfo);
		cinfo.src = (jpeg_source_mgr*)&srcMgr;
		srcMgr.pub.init_source = init_source;
	    srcMgr.pub.fill_input_buffer = fill_input_buffer;
	    srcMgr.pub.skip_input_data = skip_input_data;
	    srcMgr.pub.resync_to_restart = jpeg_resync_to_restart;
	    srcMgr.pub.term_source = term_source;
	}
	
	~QiJpegDecoderImpl()
	{
		jpeg_destroy_decompress(&cinfo);
	}
			
	struct jpeg_error_mgr jerr;
	struct jpeg_decompress_struct cinfo;
	struct my_src_mgr srcMgr;
	jmp_buf jmpbuf;
};


static void dummyExit(j_common_ptr err)
{
	QiJpegDecoderImpl* impl = (QiJpegDecoderImpl*)err->client_data;
	::longjmp(impl->jmpbuf, 1);
}


QiJpegDecoder::QiJpegDecoder()
{
	mImpl = QI_NEW QiJpegDecoderImpl();
}


QiJpegDecoder::QiJpegDecoder(QiInputStream& inStream, size_t size)
{
	mImpl = QI_NEW QiJpegDecoderImpl();
	init(inStream, size);
}


QiJpegDecoder::~QiJpegDecoder()
{
	QI_DELETE(mImpl);
}


bool QiJpegDecoder::init(QiInputStream& inStream, size_t size)
{
	struct my_src_mgr *src = (struct my_src_mgr *)mImpl->cinfo.src;
	src->inStream = &inStream;
	src->size = size;
	
	int jmp = ::setjmp(mImpl->jmpbuf);
	if (jmp != 0)
		return false;

	if (jpeg_read_header(&mImpl->cinfo, TRUE) == JPEG_HEADER_OK)
	{
		mImpl->cinfo.buffered_image = FALSE;
		mImpl->cinfo.out_color_space = JCS_RGB;
		mImpl->cinfo.out_color_components = 3;
		jpeg_start_decompress(&mImpl->cinfo); 
		return true;
	}
	else
	{
		return false;
	}
}


int QiJpegDecoder::getWidth() const
{
	return mImpl->cinfo.output_width;
}


int QiJpegDecoder::getHeight() const
{
	return mImpl->cinfo.output_height;
}


bool QiJpegDecoder::decode(unsigned char* pixels, bool flip)
{
	int jmp = ::setjmp(mImpl->jmpbuf);
	if (jmp != 0)
		return false;

	int row_stride = mImpl->cinfo.output_width * mImpl->cinfo.output_components;
	
	while(mImpl->cinfo.output_scanline < mImpl->cinfo.output_height)
	{
		unsigned char* tmp;
		if (flip)
			tmp = pixels+(mImpl->cinfo.output_height-mImpl->cinfo.output_scanline-1)*row_stride;
		else
			tmp = pixels+mImpl->cinfo.output_scanline*row_stride;
		jpeg_read_scanlines(&mImpl->cinfo, (JSAMPARRAY)&tmp, 1);
	}

	jpeg_finish_decompress(&mImpl->cinfo);
	return true;
}
