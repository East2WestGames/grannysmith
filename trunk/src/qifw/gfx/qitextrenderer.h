#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiarray.h"
#include "base/qistring.h"
#include "math/qivec3.h"
#include "gfx/qitexture.h"
#include "gfx/qicolor.h"
#include "gfx/qirenderer.h"

class QiTextRenderer
{
public:
	enum Font
	{
		TAHOMA_8,
		TAHOMA_9,
		TAHOMA_10,
		TAHOMA_11,
		TAHOMA_12,
		TAHOMA_14,
		TAHOMA_16,
		TAHOMA_18		
	};

	enum Alignment
	{
		LEFT,
		CENTER,
		RIGHT
	};
	
	QiTextRenderer();
	~QiTextRenderer();

	void init(class QiRenderer* renderer = NULL, QiShader* shader = NULL);
	bool isInitialized() const { return mInitialized; }
	
	void flip(bool enable) { mFlip = enable; }
	void setFont(Font font);
	void setAlignment(Alignment alignment);
	void setPosition(int x, int y);
	void setColor(float r, float g, float b, float a);

	int getMaxFittingChars(const QiString& str, int width) const;
	void getSize(int& width, int& height, const QiString& str, int wrapWidth = QI_INT_MAX) const;
	int getWidth(const QiString& str, int wrapWidth = QI_INT_MAX) const;
	int getHeight(const QiString& str, int wrapWidth = QI_INT_MAX) const;
	void getBounds(const QiString& str, int& minX, int& minY, int& maxX, int& maxY, int wrapWidth = QI_INT_MAX);

	void print(const QiString& str, bool autoFlush=true, int wrapWidth = QI_INT_MAX);
	void flush();
	
protected:
	void printLine(const QiString& str, int line);

	Font mFont;
	Alignment mAlignment;
	QiArray<QiVec3> mVerts;
	QiArray<QiVec2> mTexCoords;
	QiVec3 mPos;
	QiColor mColor;
	class QiRenderer* mRenderer;

	bool mInitialized;
	int mFontFirstChar;
	int mFontCharCount;
	int mFontTotalWidth;
	int mFontHeight;
	int* mFontCharWidth;
	int* mFontCharStart;
	char* mFontData;
	bool mFlip;
	
	QiTexture mTexture;
	QiVertexFormat mFmt;
	QiVertexBuffer mVb;
	QiIndexBuffer mIb;
	QiShader* mShader;
};


