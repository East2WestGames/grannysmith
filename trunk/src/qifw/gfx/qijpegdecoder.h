#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

class QiJpegDecoder
{
public:
	QiJpegDecoder();
	QiJpegDecoder(class QiInputStream& inStream, size_t size);
	~QiJpegDecoder();

	bool init(class QiInputStream& inStream, size_t size);
	int getWidth() const;
	int getHeight() const;
	bool decode(unsigned char* pixels, bool flip=false);

protected:
	class QiJpegDecoderImpl* mImpl;
};



