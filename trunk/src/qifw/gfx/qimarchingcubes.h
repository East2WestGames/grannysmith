#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiarray.h"
#include "math/qivec3.h"

class QiMarchingCubes
{
public:
	QiMarchingCubes();
	
	void init(float* data, int xCount, int yCount, int zCount);
	void setOutputScaling(const QiVec3& offset, const QiVec3& cellSize);
	void setStride(int xStride, int yStride, int zStride);
	void compute(float target);

	int getVertexCount() const;
	int getFaceCount() const;
	const QiVec3* getVertices() const;
	const QiVec3* getNormals() const;
	const int* getFaces() const;
	
protected:
	float getValue(int x, int y, int z);
	float getValue(float x, float y, float z);
	QiVec3 vGetNormal(float a, float b, float c);

	QiArray<QiVec3> mVertices;
	QiArray<QiVec3> mNormals;
	QiArray<int> mFaces;
	
	float* mData;
	int mXSize;
	int mYSize;
	int mZSize;
	int mXStride;
	int mYStride;
	int mZStride;
	QiVec3 mOffset;
	QiVec3 mCellSize;
};


