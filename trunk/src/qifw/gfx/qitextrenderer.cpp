/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "gfx/qitextrenderer.h"
#include "gfx/fonts/tahoma8.h"
#include "gfx/fonts/tahoma9.h"
#include "gfx/fonts/tahoma10.h"
#include "gfx/fonts/tahoma11.h"
#include "gfx/fonts/tahoma12.h"
#include "gfx/fonts/tahoma14.h"
#include "gfx/fonts/tahoma16.h"
#include "gfx/fonts/tahoma18.h"

#include "gfx/qigl.h"

const int MAX_CHARS = 1024;

inline static bool isWhiteSpace(char c)
{
	return c == ' ' || c == '\t' || c == '\n';
}


QiTextRenderer::QiTextRenderer() : mInitialized(false), mFlip(false)
{
	mRenderer = NULL;
	mShader = NULL;
}


QiTextRenderer::~QiTextRenderer()
{
}


void QiTextRenderer::init(QiRenderer* renderer, QiShader* shader)
{
	mRenderer = renderer;
	mShader = shader;
	mFmt.clear();
	mFmt.addField("aPosition", QiVertexFormat::FLOAT32, 3);
	mFmt.addField("aTexCoord", QiVertexFormat::FLOAT32, 2);
	mVb.init(mFmt, MAX_CHARS*4);
	mVb.clear();
	mIb.init(MAX_CHARS*6);
	mIb.clear();
	for(int i=0; i<MAX_CHARS; i++)
		mIb.quad(i*4+0, i*4+1, i*4+2, i*4+3);

	setFont(TAHOMA_9);
	setAlignment(LEFT);
	setColor(1,1,1,1);
	mInitialized = true;
}


void QiTextRenderer::setFont(Font font)
{
	mFont = font;
	switch(mFont)
	{
		case QiTextRenderer::TAHOMA_8:
		mFontFirstChar = font_tahoma8_firstChar;
		mFontCharCount = font_tahoma8_charCount;
		mFontTotalWidth = font_tahoma8_totalWidth;
		mFontHeight = font_tahoma8_height;
		mFontCharWidth = font_tahoma8_charWidth;
		mFontCharStart = font_tahoma8_charStart;
		mFontData = (char*)font_tahoma8_data;
		break;	
		case QiTextRenderer::TAHOMA_9:
		mFontFirstChar = font_tahoma9_firstChar;
		mFontCharCount = font_tahoma9_charCount;
		mFontTotalWidth = font_tahoma9_totalWidth;
		mFontHeight = font_tahoma9_height;
		mFontCharWidth = font_tahoma9_charWidth;
		mFontCharStart = font_tahoma9_charStart;
		mFontData = (char*)font_tahoma9_data;
		break;	
		case QiTextRenderer::TAHOMA_10:
		mFontFirstChar = font_tahoma10_firstChar;
		mFontCharCount = font_tahoma10_charCount;
		mFontTotalWidth = font_tahoma10_totalWidth;
		mFontHeight = font_tahoma10_height;
		mFontCharWidth = font_tahoma10_charWidth;
		mFontCharStart = font_tahoma10_charStart;
		mFontData = (char*)font_tahoma10_data;
		break;	
		case QiTextRenderer::TAHOMA_11:
		mFontFirstChar = font_tahoma11_firstChar;
		mFontCharCount = font_tahoma11_charCount;
		mFontTotalWidth = font_tahoma11_totalWidth;
		mFontHeight = font_tahoma11_height;
		mFontCharWidth = font_tahoma11_charWidth;
		mFontCharStart = font_tahoma11_charStart;
		mFontData = (char*)font_tahoma11_data;
		break;	
		case QiTextRenderer::TAHOMA_12:
		mFontFirstChar = font_tahoma12_firstChar;
		mFontCharCount = font_tahoma12_charCount;
		mFontTotalWidth = font_tahoma12_totalWidth;
		mFontHeight = font_tahoma12_height;
		mFontCharWidth = font_tahoma12_charWidth;
		mFontCharStart = font_tahoma12_charStart;
		mFontData = (char*)font_tahoma12_data;
		break;
		case QiTextRenderer::TAHOMA_14:
		mFontFirstChar = font_tahoma14_firstChar;
		mFontCharCount = font_tahoma14_charCount;
		mFontTotalWidth = font_tahoma14_totalWidth;
		mFontHeight = font_tahoma14_height;
		mFontCharWidth = font_tahoma14_charWidth;
		mFontCharStart = font_tahoma14_charStart;
		mFontData = (char*)font_tahoma14_data;
		break;
		case QiTextRenderer::TAHOMA_16:
		mFontFirstChar = font_tahoma16_firstChar;
		mFontCharCount = font_tahoma16_charCount;
		mFontTotalWidth = font_tahoma16_totalWidth;
		mFontHeight = font_tahoma16_height;
		mFontCharWidth = font_tahoma16_charWidth;
		mFontCharStart = font_tahoma16_charStart;
		mFontData = (char*)font_tahoma16_data;
		break;
		case QiTextRenderer::TAHOMA_18:
		mFontFirstChar = font_tahoma18_firstChar;
		mFontCharCount = font_tahoma18_charCount;
		mFontTotalWidth = font_tahoma18_totalWidth;
		mFontHeight = font_tahoma18_height;
		mFontCharWidth = font_tahoma18_charWidth;
		mFontCharStart = font_tahoma18_charStart;
		mFontData = (char*)font_tahoma18_data;
		break;
		default:
		break;
	}
	int tw = 2048;
	int th = 32;
	int w = mFontTotalWidth;
	int h = mFontHeight;
	mTexture.init(tw, th, GL_ALPHA);
	char* data = (char*)mTexture.getData();
	memset(data, 0, tw*th);
	for(int y=0; y<h; y++)
	{
		for(int x=0; x<w; x++)
			data[y*tw+x] = mFontData[(h-y-1)*w+x];
	}
	mTexture.upload();	
}


void QiTextRenderer::setAlignment(Alignment alignment)
{
	mAlignment = alignment;
}


void QiTextRenderer::setPosition(int x, int y)
{
	mPos.x = (float)x;
	mPos.y = (float)y;
}

int QiTextRenderer::getMaxFittingChars(const QiString& str, int width) const
{
	int w = 0;
	int chars = 0;
	for(const char* cc=str.c_str(); *cc!='\0'; cc++)
	{
		char c = *cc;
		if (c == '\n')
		{
			w = 0;
		}
		int index = (int)c-mFontFirstChar;
		if (index < 0 || index >= mFontCharCount)
			continue;
		w += mFontCharWidth[index];

		if (w>width)
			return chars;
		chars++;
	}
	return chars;
}

void QiTextRenderer::getSize(int& width, int& height, const QiString& str, int wrapWidth) const
{
	if( wrapWidth <= 0 )
	{
		// Our wrap width won't fit anything... Bolting out.
		width = 0;
		height = 0;
		return;
	}

	height = mFontHeight;
	width = 0;
	int w = 0;
	for(const char* cc=str.c_str(); *cc!='\0'; cc++)
	{	
		char c = *cc;
		if (c == '\n')
		{
			w = 0;
			height += mFontHeight;
		}
		int index = (int)c-mFontFirstChar;
		if (index < 0 || index >= mFontCharCount)
			continue;
		w += mFontCharWidth[index];
		// If this char overrun our wrap width, we back up a char and start on the next line
		if (w > wrapWidth)
		{
			w -= mFontCharWidth[index];
			width = QiMax(w, width);
			if( w == 0 )
			{
				// Our wrap width won't even fit this char alone... Bolting out.
				width = 0;
				height = 0;
				return;
			}
			cc--;
			w = 0;
			height += mFontHeight;
		} else {
			width = QiMax(w, width);
		}
	}
}

int QiTextRenderer::getWidth(const QiString& str, int wrapWidth) const
{
	int width, height;
	getSize(width, height, str, wrapWidth);
	return width;
}


int QiTextRenderer::getHeight(const QiString& str, int wrapWidth) const
{
	int width, height;
	getSize(width, height, str, wrapWidth);
	return height;
}


void QiTextRenderer::print(const QiString& str, bool autoFlush, int wrapWidth)
{
	int line=0;
	//Print all lines
	int c = -1;
	QiString tmp = str;
	while(true)
	{
		c = tmp.getIndexOf("\n");
		QiString lineStr = c != -1 ? tmp.substring(0, c) : tmp;
		int fittingChars;
		while ((fittingChars = getMaxFittingChars(lineStr, wrapWidth)) > 0 )
		{
			if (getWidth(lineStr) < wrapWidth || fittingChars == lineStr.getLength())
			{
				printLine(lineStr, line++);
				break;
			} else {
				// Search for the last whitespace that fit into our wrap width
				int lastSpace = fittingChars-1;
				while (lastSpace > 0 && !isWhiteSpace(lineStr[lastSpace]))
					lastSpace--;
				// If we didn't find a whitespace, we just break at the exact char limit
				if( lastSpace <= 0 )
					lastSpace = fittingChars;

				printLine(lineStr.substring(0, lastSpace), line++);
				if (lastSpace != fittingChars)
					lineStr = lineStr.substring(lastSpace + 1);
				else
					lineStr = lineStr.substring(lastSpace);
			}
		}
		if (c==-1)
			break;
		tmp = tmp.substring(c+1);
	}
	if (autoFlush)
		flush();
}


void QiTextRenderer::printLine(const QiString& str, int line)
{
	float flip = mFlip ? -1.0f : 1.0f;
	float hx = mPos.x;
	if (mAlignment == QiTextRenderer::CENTER)
		hx -= getWidth(str)/2;
	else if (mAlignment == QiTextRenderer::RIGHT)
		hx -= getWidth(str);
	float hy = mPos.y - flip*mFontHeight*line;
	hx += 0.5f;
	for(const char* cc=str.c_str(); *cc!='\0'; cc++)
	{	
		char c = *cc;
		int index = (int)c-mFontFirstChar;
		if (index < 0 || index >= mFontCharCount)
			continue;
		float x = (mFontCharStart[index]-0.5f) / (float)mTexture.getWidth();
		float w = (mFontCharWidth[index]+1.0f) / (float)mTexture.getWidth();
		float h = mFontHeight / (float)mTexture.getHeight();
		float hw = (float)mFontCharWidth[index];
		float hh = (float)flip*mFontHeight;
		mTexCoords.add(QiVec2(x, 0));
		mVerts.add(QiVec3(hx, hy-hh, mPos.z));
		mTexCoords.add(QiVec2(x+w, 0));
		mVerts.add(QiVec3(hx+hw+1.0f, hy-hh, mPos.z));
		mTexCoords.add(QiVec2(x+w, h));
		mVerts.add(QiVec3(hx+hw+1.0f, hy, mPos.z));
		mTexCoords.add(QiVec2(x, h));
		mVerts.add(QiVec3(hx, hy, mPos.z));
		hx += hw;
	}	
}


void QiTextRenderer::flush()
{
	if (mRenderer)
	{
		mVb.clear();
		for(int i=0; i<mVerts.getCount() && i<MAX_CHARS*4; i++)
		{
			mVb.vertex();
			mVb.addFast(mVerts[i]);
			mVb.addFast(mTexCoords[i]);
		}
		QiRenderState state;
		state.color = mColor;
		state.blendMode = state.BLEND;
		state.shader = mShader;
		state.texture[0] = &mTexture;
		mRenderer->setState(state);
		mRenderer->drawTriangles(&mVb, &mIb, mVb.getCount()/4*6);
	}
	else
	{
#ifdef QI_USE_OPENGL
#ifndef QI_ANDROID
		mTexture.use();
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	
		glColor4f(mColor.r, mColor.g, mColor.b, mColor.a);
		glPushAttrib(GL_TEXTURE_BIT | GL_DEPTH_BUFFER_BIT);
		glDepthMask(GL_FALSE);
		glEnable(GL_BLEND);
		glEnable(GL_TEXTURE_2D);

		glVertexPointer(3, GL_FLOAT, sizeof(QiVec3), mVerts.getData());
		glEnableClientState(GL_VERTEX_ARRAY);
		glTexCoordPointer(2, GL_FLOAT, sizeof(QiVec2), mTexCoords.getData());
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glDrawArrays(GL_QUADS, 0, mVerts.getCount());
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);

		glPopAttrib();
#endif
#endif
	}
	mVerts.clear();
	mTexCoords.clear();
}


void QiTextRenderer::setColor(float r, float g, float b, float a)
{
	mColor.set(r, g, b, a);
}


void QiTextRenderer::getBounds(const QiString& str, int& minX, int& minY, int& maxX, int& maxY, int wrapWidth)
{
	int width, height;
	getSize(width, height, str, wrapWidth);
	if (mAlignment == CENTER)
	{
		minX = (int)mPos.x - width/2;
		maxX = (int)mPos.x + width/2;
	}
	else if (mAlignment == RIGHT)
	{
		minX = (int)mPos.x - width;
		maxX = (int)mPos.x;
	}
	else
	{
		minX = (int)mPos.x;
		maxX = (int)mPos.x + width;
	}
	if (mFlip)
	{
		minY = (int)mPos.y;
		maxY = (int)mPos.y + height;
	}
	else
	{
		minY = (int)mPos.y - height;
		maxY = (int)mPos.y;
	}
}

