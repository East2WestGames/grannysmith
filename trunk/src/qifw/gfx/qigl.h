#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#if defined(QI_MACOS)
//	#include "gfx/glew/glew.h"
	#include <OpenGL/gl.h>
	#include <OpenGL/glu.h>
	#ifdef QI_USE_GLUT
		#include <glut/glut.h>
	#endif
#elif defined(QI_IOS)
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#define GL_WRITE_ONLY GL_WRITE_ONLY_OES
#define GL_READ_ONLY GL_READ_ONLY_OES
#define glMapBuffer glMapBufferOES
#define glUnmapBuffer glUnmapBufferOES
#elif defined(QI_ANDROID)
#define GL_GLEXT_PROTOTYPES
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#define GL_WRITE_ONLY GL_WRITE_ONLY_OES
#define GL_READ_ONLY GL_READ_ONLY_OES
#define glMapBuffer glMapBufferOES
#define glUnmapBuffer glUnmapBufferOES
#else
	#include <windows.h>
	#include "gfx/glew/glew.h"
	#include <GL/gl.h>
	#include <GL/glu.h>
	#ifdef QI_USE_GLUT
		#include <glut.h>
	#endif
#endif

