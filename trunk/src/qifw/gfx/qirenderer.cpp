/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "gfx/qirenderer.h"
#include "gfx/qiviewport.h"
#include "gfx/qishader.h"
#include "gfx/qitexture.h"

#include "gfx/qigl.h"

#if defined(QI_USE_OPENGL) || defined(QI_IOS)

#define USE_MAP_BUFFER

int QiVertexFormat::getFieldIndex(const QiString& name) const
{
	for(int i=0; i<mCount; i++)
	{
		if (mFields[i].name == name)
			return i;
	}
	return -1;
}


void QiVertexFormat::addField(const QiString& name, Type type, int dim)
{
	QI_ASSERT(mCount < MAX_FIELD_COUNT-1, "Reached MAX_FIELD_COUNT");
	mFields[mCount].name = name;
	mFields[mCount].type = type;
	mFields[mCount].dim = dim;
	if (mCount == 0)
		mFields[mCount].offset = 0;
	else
	{
		mFields[mCount].offset = mFields[mCount-1].offset + QI_RENDER_TYPE_SIZE[mFields[mCount-1].type]*mFields[mCount-1].dim;
		mFields[mCount].offset = QiRoundUpToAlignment(mFields[mCount].offset, QI_RENDER_TYPE_SIZE[type]);
	}
	mSize = mFields[mCount].offset + QI_RENDER_TYPE_SIZE[type]*dim;
	mSize = QiRoundUpToAlignment(mSize, 4);
	mCount++;
}



QiVertexBuffer::QiVertexBuffer() : mFormat(NULL), mData(0), mBase(0), mCurrent(0), mVertexCount(0), mMaxVertexCount(0), mVertexSize(0), mVboId(0)
{
}

QiVertexBuffer::QiVertexBuffer(const QiVertexFormat& format, int size) : mFormat(NULL), mData(0), mBase(0), mCurrent(0), mVertexCount(0), mMaxVertexCount(0), mVertexSize(0), mVboId(0)
{
	init(format, size);
}

QiVertexBuffer::~QiVertexBuffer()
{
	shutdown();
}

void QiVertexBuffer::init(const QiVertexFormat& format, int size)
{
	mFormat = &format;
	mMaxVertexCount = size;
	mVertexSize = format.mSize;
	mVertexCount = 0;
	if (mData)
		QiFree(mData);
	mData = (char*)QiAlloc(mVertexSize*mMaxVertexCount);
	mBase = mData;
	mCurrentField = -1;
}


void QiVertexBuffer::redim(int size)
{
	if (size > mMaxVertexCount || size < mMaxVertexCount - 256)
	{
		mMaxVertexCount = size;
		mData = (char*)QiRealloc(mData, mVertexSize*mMaxVertexCount);
	}
}


void QiVertexBuffer::shutdown()
{
	if (mData)
	{
		QiFree(mData);
		mData = NULL;
	}
	if (mVboId)
	{
		glDeleteBuffers(1, (const GLuint*)&mVboId);
		mVboId = 0;
	}
}


void QiVertexBuffer::clear()
{
	mVertexCount = 0;
	mCurrentField = -1;
}


void QiVertexBuffer::makeVbo()
{
	if (!mVboId)
		glGenBuffers(1, (GLuint*)&mVboId);
	glBindBuffer(GL_ARRAY_BUFFER, mVboId);
	glBufferData(GL_ARRAY_BUFFER, mVertexCount*mVertexSize, mData, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}


void QiVertexBuffer::makeDynamicVbo()
{
	if (mData)
	{
#ifdef USE_MAP_BUFFER
		QiFree(mData);
		mData = NULL;
#endif
	}
	if (mVboId)
		glDeleteBuffers(1, (const GLuint*)&mVboId);
	glGenBuffers(1, (GLuint*)&mVboId);
	glBindBuffer(GL_ARRAY_BUFFER, mVboId);
	glBufferData(GL_ARRAY_BUFFER, mMaxVertexCount*mVertexSize, NULL, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}


void QiVertexBuffer::unlock()
{
#ifdef USE_MAP_BUFFER
	if (mVboId)
	{
		glBindBuffer(GL_ARRAY_BUFFER, mVboId);
		mData = (char*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
	}
#endif
}


void QiVertexBuffer::lock()
{
	if (mVboId)
	{
		glBindBuffer(GL_ARRAY_BUFFER, mVboId);
#ifdef USE_MAP_BUFFER
		glUnmapBuffer(GL_ARRAY_BUFFER);
		mData = NULL;
#else
		glBufferSubData(GL_ARRAY_BUFFER, 0, mVertexCount*mVertexSize, mData);
#endif
	}
}



QiIndexBuffer::QiIndexBuffer() : mCount(0), mData(NULL), mIboId(0), mSize(0)
{
	init(0);
}


QiIndexBuffer::QiIndexBuffer(int size) : mCount(0), mData(NULL), mIboId(0), mSize(0)
{
	init(size);
}


QiIndexBuffer::~QiIndexBuffer()
{
	shutdown();
}


void QiIndexBuffer::init(int size)
{
	shutdown();
	mCount = 0;
	if (size)
	{
		mData = (unsigned short*)QiAlloc(sizeof(unsigned short)*size);
		mSize = size;
	}
	clear();
}

void QiIndexBuffer::redim(int size)
{
	if (mSize != size)
	{
		mSize = size;
		mData = (unsigned short*)QiRealloc(mData, sizeof(unsigned short)*size);
	}
}

void QiIndexBuffer::clear()
{
	mCount = 0;
}

void QiIndexBuffer::shutdown()
{
	if (mIboId)
	{
		glDeleteBuffers(1, (const GLuint*)&mIboId);
		mIboId = 0;
	}
	if (mData)
	{
		QiFree(mData);
		mData = NULL;
		mSize = 0;
	}
	mCount = 0;
}

void QiIndexBuffer::makeIbo()
{
	if (!mIboId)
		glGenBuffers(1, (GLuint*)&mIboId);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIboId);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned short)*mCount, mData, GL_STATIC_DRAW);
}


void QiIndexBuffer::triangle(int i0, int i1, int i2)
{
	if (mCount+3 > mSize)
		redim(mSize * 2 + 128);
	QI_ASSERT(i0 < 65536 && i1 < 65536 && i2 < 65536, "Vertex indices must be < 65536");
	mData[mCount++] = i0;
	mData[mCount++] = i1;
	mData[mCount++] = i2;
}


void QiIndexBuffer::quad(int i0, int i1, int i2, int i3)
{
	if (mCount+6 > mSize)
		redim(mSize * 2 + 128);
	QI_ASSERT(i0 < 65536 && i1 < 65536 && i2 < 65536 && i3 < 65536, "Vertex indices must be < 65536");
	mData[mCount++] = i0;
	mData[mCount++] = i1;
	mData[mCount++] = i2;
	mData[mCount++] = i2;
	mData[mCount++] = i3;
	mData[mCount++] = i0;
}


QiRenderer::QiRenderer() : mDrawCallCount(0), mStateChangeCount(0)
{
	mRectFormat.addField("aPosition", QiVertexFormat::FLOAT32, 2);
	mRectFormat.addField("aTexCoord", QiVertexFormat::FLOAT32, 2);
	mRectVb.init(mRectFormat, 4);
	mRectIb.init(6);
	mRectIb.quad(0, 1, 2, 3);

	mLineFormat.addField("aPosition", QiVertexFormat::FLOAT32, 3);
	mLineVb.init(mLineFormat, 2);
}


void QiRenderer::setViewport(const QiViewport& viewport)
{
	int x0, y0, x1, y1;
	viewport.getBounds(x0, y0, x1, y1);
	glViewport(x0, y0, x1-x0, y1-y0);
	
	if (viewport.mScissorEnabled)
	{
		glEnable(GL_SCISSOR_TEST);
		glScissor(viewport.mScissor[0], viewport.mScissor[1], viewport.mScissor[2], viewport.mScissor[3]);
	}
	else
	{
		glDisable(GL_SCISSOR_TEST);
	}

	mProjectionMatrix = viewport.getProjectionMatrix();
	mModelViewMatrix = viewport.getModelviewMatrix();
}


void QiRenderer::clear(bool color, bool depth)
{
	bool s = glIsEnabled(GL_SCISSOR_TEST) != 0 ? true : false;
	if (s)
		glDisable(GL_SCISSOR_TEST);
	glClear((color ? GL_COLOR_BUFFER_BIT : 0) | (depth ? GL_DEPTH_BUFFER_BIT : 0));
	if (s)
		glEnable(GL_SCISSOR_TEST);

	mDrawCallCount = 0;
	mStateChangeCount = 0;
}


inline GLuint glType(QiVertexFormat::Type type)
{
	switch(type)
	{
		case QiVertexFormat::FLOAT32:
		return GL_FLOAT;
		case QiVertexFormat::INT16:
		return GL_SHORT;
		case QiVertexFormat::UINT16:
		return GL_UNSIGNED_SHORT;
		case QiVertexFormat::INT8:
		return GL_BYTE;
		case QiVertexFormat::UINT8:
		return GL_UNSIGNED_BYTE;
	}
	return GL_FLOAT;
}

void QiRenderer::preDraw(const QiVertexBuffer* vb)
{
	glBindBuffer(GL_ARRAY_BUFFER, vb->mVboId);
	const QiVertexFormat& fmt = *vb->mFormat;
	for(int i=0; i<fmt.mCount; i++)
	{
		int location;
		if (fmt.mFields[i].name == "aPosition")
			location = mCurrentState.shader->aPosition;
		else if (fmt.mFields[i].name == "aTexCoord")
			location = mCurrentState.shader->aTexCoord;
		else if (fmt.mFields[i].name == "aNormal")
			location = mCurrentState.shader->aNormal;
		else if (fmt.mFields[i].name == "aAlpha")
			location = mCurrentState.shader->aAlpha;
		else
			location = mCurrentState.shader->getAttributeLocation(fmt.mFields[i].name);
		glEnableVertexAttribArray(location);
		bool norm = (fmt.mFields[i].type!=QiVertexFormat::FLOAT32);
		if (vb->mVboId)
			glVertexAttribPointer(location, fmt.mFields[i].dim, glType(fmt.mFields[i].type), norm, fmt.mSize, (void*)(fmt.mFields[i].offset));
		else
			glVertexAttribPointer(location, fmt.mFields[i].dim, glType(fmt.mFields[i].type), norm, fmt.mSize, vb->mData+fmt.mFields[i].offset);
	}
	QiMatrix4 mvp = mProjectionMatrix * mModelViewMatrix * mObjectMatrix;
	if (mCurrentState.shader->mMvpId != -1)
		mCurrentState.shader->setUniform4x4(mCurrentState.shader->mMvpId, mvp.m);
	if (mCurrentState.shader->mProjectionMatrixId != -1)
		mCurrentState.shader->setUniform4x4(mCurrentState.shader->mProjectionMatrixId, mProjectionMatrix.m);
	if (mCurrentState.shader->mModelViewMatrixId != -1)
		mCurrentState.shader->setUniform4x4(mCurrentState.shader->mModelViewMatrixId, mModelViewMatrix.m);
	if (mCurrentState.shader->mObjectMatrixId != -1)
		mCurrentState.shader->setUniform4x4(mCurrentState.shader->mObjectMatrixId, mObjectMatrix.m);
	if (mCurrentState.shader->mColorId != -1)
		mCurrentState.shader->setUniform4(mCurrentState.shader->mColorId, &mCurrentState.color.r);
	for(int i=0; i<QI_RENDER_TEXTURE_COUNT; i++)
	{
        if (mCurrentState.shader->mTexScaleId != -1)
            mCurrentState.shader->setUniform2(mCurrentState.shader->mTexScaleId, &mCurrentState.texScale[i].x);
        if (mCurrentState.shader->mTexOffsetId != -1)
            mCurrentState.shader->setUniform2(mCurrentState.shader->mTexOffsetId, &mCurrentState.texOffset[i].x);
	}
}


void QiRenderer::postDraw(const QiVertexBuffer* vb)
{
	const QiVertexFormat& fmt = *vb->mFormat;
	for(int i=0; i<fmt.mCount; i++)
	{
		int location;
		if (fmt.mFields[i].name == "aPosition")
			location = mCurrentState.shader->aPosition;
		else if (fmt.mFields[i].name == "aTexCoord")
			location = mCurrentState.shader->aTexCoord;
		else if (fmt.mFields[i].name == "aNormal")
			location = mCurrentState.shader->aNormal;
		else if (fmt.mFields[i].name == "aAlpha")
			location = mCurrentState.shader->aAlpha;
		else
			location = mCurrentState.shader->getAttributeLocation(fmt.mFields[i].name);
		glDisableVertexAttribArray(location);
	}
}



void QiRenderer::setState(QiRenderState& renderState)
{
	if (renderState.blendMode != mCurrentState.blendMode)
	{
		switch(renderState.blendMode)
		{
			case QiRenderState::NONE:
			glDisable(GL_BLEND);
			break;

			case QiRenderState::BLEND:
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			break;

			case QiRenderState::ADDITIVE:
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE);
			break;
		}
	}

	if (renderState.shader != mCurrentState.shader)
	{
		if (renderState.shader)
		{
			renderState.shader->use();
			for(int i=0; i<QI_RENDER_TEXTURE_COUNT; i++)
				renderState.shader->setUniform(QiString("uTexture")+i, i);
		}
		else
			glUseProgram(0);
	}

	for(int i=0; i<QI_RENDER_TEXTURE_COUNT; i++)
	{
		if (renderState.texture[i] != mCurrentState.texture[i])
		{
			glActiveTexture(GL_TEXTURE0+i);
			if (renderState.texture[i])
				renderState.texture[i]->use();
		}
	}

	if (renderState.depthTest != mCurrentState.depthTest)
	{
		if (renderState.depthTest)
			glEnable(GL_DEPTH_TEST);
		else
			glDisable(GL_DEPTH_TEST);
	}

	if (renderState.depthMask != mCurrentState.depthMask)
	{
		if (renderState.depthMask)
			glDepthMask(GL_TRUE);
		else
			glDepthMask(GL_FALSE);
	}

	if (renderState.colorMask != mCurrentState.colorMask)
	{
		if (renderState.colorMask)
			glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
		else
			glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
	}

	if (renderState.cullFace != mCurrentState.cullFace)
	{
		if (renderState.cullFace)
			glEnable(GL_CULL_FACE);
		else
			glDisable(GL_CULL_FACE);
	}

	mCurrentState = renderState;
	mStateChangeCount++;
}



void QiRenderer::drawPoints(const QiVertexBuffer* vb, int count, int offset)
{
	if (count==-1)
		count = vb->mVertexCount;
	
	if (count == 0)
		return;

	preDraw(vb);
	glDrawArrays(GL_POINTS, offset, count);
	postDraw(vb);
	mDrawCallCount++;
}


void QiRenderer::drawLines(const QiVertexBuffer* vb, int count, int offset)
{
	if (count==-1)
		count = vb->mVertexCount;
	
	if (count == 0)
		return;

	preDraw(vb);
	glDrawArrays(GL_LINES, offset, count);
	postDraw(vb);
	mDrawCallCount++;
}


void QiRenderer::drawLines(const QiVertexBuffer* vb, const QiIndexBuffer* ib, int count, int offset)
{
	if (count==-1)
		count = ib->mCount;

	if (count == 0)
		return;

	preDraw(vb);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib->mIboId);
	if (ib->mIboId)
		glDrawElements(GL_LINES, count, GL_UNSIGNED_SHORT, (void*)(2*offset));
	else
		glDrawElements(GL_LINES, count, GL_UNSIGNED_SHORT, ib->mData+offset);
	postDraw(vb);
	mDrawCallCount++;
}


void QiRenderer::drawTriangles(const QiVertexBuffer* vb, int count, int offset)
{
	if (count==-1)
		count = vb->mVertexCount;
	
	if (count == 0)
		return;

	preDraw(vb);
	glDrawArrays(GL_TRIANGLES, offset, count);
	postDraw(vb);
	mDrawCallCount++;
}

void QiRenderer::drawTriangles(const QiVertexBuffer* vb, const QiIndexBuffer* ib, int count, int offset)
{
	if (count==-1)
		count = ib->mCount;

	if (count == 0)
		return;

	preDraw(vb);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib->mIboId);
	if (ib->mIboId)
		glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_SHORT, (void*)(2*offset));
	else
		glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_SHORT, ib->mData+offset);
	postDraw(vb);
	mDrawCallCount++;
}


void QiRenderer::drawTriangles(const QiMatrix4& transform, const QiVertexBuffer* vb, int count, int offset)
{
	mObjectMatrix = transform;
	drawTriangles(vb, count, offset);
	mObjectMatrix.setIdentity();
}


void QiRenderer::drawTriangles(const QiMatrix4& transform, const QiVertexBuffer* vb, const QiIndexBuffer* ib, int count, int offset)
{
	mObjectMatrix = transform;
	drawTriangles(vb, ib, count, offset);
	mObjectMatrix.setIdentity();
}


void QiRenderer::drawRect(const QiVec2& lower, const QiVec2& upper)
{
	drawRect(lower, upper, QiVec2(0,0), QiVec2(1,1));
}


void QiRenderer::drawRect(const QiVec2& lower, const QiVec2& upper, const QiVec2& texLower, const QiVec2& texUpper)
{
	mRectVb.clear();
	mRectVb.vertex();
	mRectVb.addFast(QiVec2(lower.x, lower.y));
	mRectVb.addFast(QiVec2(texLower.x, texLower.y));
	mRectVb.vertex();
	mRectVb.addFast(QiVec2(upper.x, lower.y));
	mRectVb.addFast(QiVec2(texUpper.x, texLower.y));
	mRectVb.vertex();
	mRectVb.addFast(QiVec2(upper.x, upper.y));
	mRectVb.addFast(QiVec2(texUpper.x, texUpper.y));
	mRectVb.vertex();
	mRectVb.addFast(QiVec2(lower.x, upper.y));
	mRectVb.addFast(QiVec2(texLower.x, texUpper.y));
	drawTriangles(&mRectVb, &mRectIb);
}


void QiRenderer::drawRectRot(const QiVec2& center, float size, float angle, const QiVec2& texLower, const QiVec2& texUpper)
{
	QiVec2 p[4];
	float a = angle-QI_PI/4;
	float ad = QI_PI/2;
	for(int i=0; i<4; i++)
		p[i] = center + QiVec2(QiCos(a+ad*i)*size, QiSin(a+ad*i)*size);
	mRectVb.clear();
	mRectVb.vertex();
	mRectVb.addFast(p[0]);
	mRectVb.addFast(QiVec2(texLower.x, texLower.y));
	mRectVb.vertex();
	mRectVb.addFast(p[1]);
	mRectVb.addFast(QiVec2(texUpper.x, texLower.y));
	mRectVb.vertex();
	mRectVb.addFast(p[2]);
	mRectVb.addFast(QiVec2(texUpper.x, texUpper.y));
	mRectVb.vertex();
	mRectVb.addFast(p[3]);
	mRectVb.addFast(QiVec2(texLower.x, texUpper.y));
	drawTriangles(&mRectVb, &mRectIb);
}


void QiRenderer::drawLine(const QiVec3& p0, const QiVec3& p1)
{
	mLineVb.clear();
	mLineVb.vertex();
	mLineVb.addFast(p0);
	mLineVb.vertex();
	mLineVb.addFast(p1);
	drawLines(&mLineVb);
}


void QiRenderer::drawPoint(const QiVec3& p)
{
	mLineVb.clear();
	mLineVb.vertex();
	mLineVb.addFast(p);
	drawPoints(&mLineVb);
}


void QiRenderer::resetState()
{
	mCurrentState = QiRenderState();
	glDisable(GL_SCISSOR_TEST);
	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glDisable(GL_BLEND);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glClearColor(0,0,0,0);
	glUseProgram(0);
	glActiveTexture(GL_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

#endif
