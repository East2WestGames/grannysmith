/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "qiviewport.h"
#include "geometry/qidistance.h"
#include "math/qitransform3.h"

QiViewport::QiViewport()
{
	mState.fixedAspect = 0.0f;
	mState.mode = MODE_ORTHO;
	mState.isoAmount = 0.0f;
	mState.isoAngle = QI_PI/4;
	mScissorEnabled = false;
}


QiViewport::QiViewport(int x0, int y0, int x1, int y1)
{
	mState.fixedAspect = 0.0f;
	mState.mode = MODE_ORTHO;
	mState.isoAmount = 0.0f;
	mState.isoAngle = QI_PI/4;
	mScissorEnabled = false;
	setBounds(x0, y0, x1, y1);
}


void QiViewport::init(int x0, int y0, int x1, int y1)
{
	setBounds(x0, y0, x1, y1);
	mScissorEnabled = false;
}


void QiViewport::setBounds(int x0, int y0, int x1, int y1)
{
	mState.bounds[0] = x0;
	mState.bounds[1] = y0;
	mState.bounds[2] = x1;
	mState.bounds[3] = y1;
	updateProjection();
}


void QiViewport::getBounds(int& x0, int& y0, int& x1, int& y1) const
{
	x0 = mState.bounds[0];
	y0 = mState.bounds[1];
	x1 = mState.bounds[2];
	y1 = mState.bounds[3];
}


void QiViewport::enableScissor(int x0, int y0, int x1, int y1)
{
	mScissor[0] = x0;
	mScissor[1] = getHeight()-y1;
	mScissor[2] = x1-x0;
	mScissor[3] = y1-y0;
	mScissorEnabled = true;
}


void QiViewport::disableScissor()
{
	mScissorEnabled = false;
}

void QiViewport::push()
{
	mStack.add(mState);
}


void QiViewport::pop()
{
	mState = mStack.removeLast();
}


void QiViewport::setModeOrtho()

{
	mState.mode = MODE_ORTHO;
	updateProjection();
	updateModelview();
}


void QiViewport::setModePixel(float near, float far)
{
	mState.pNear = near;
	mState.pFar = far;
	mState.mode = MODE_PIXEL;
	updateProjection();
	updateModelview();
}


void QiViewport::setModePixelFlipped(float near, float far)
{
	mState.pNear = near;
	mState.pFar = far;
	mState.mode = MODE_PIXEL_FLIPPED;
	updateProjection();
	updateModelview();
}


void QiViewport::setMode2D(float fovInUnits, float near, float far)
{
	mState.mode = MODE_2D;
	mState.pFov = fovInUnits;
	mState.pNear = near;
	mState.pFar = far;
	updateProjection();
	updateModelview();
}


void QiViewport::setMode3D(float fovInDegrees, float near, float far)
{
	mState.mode = MODE_3D;
	mState.pFov = fovInDegrees;
	mState.pNear = near;
	mState.pFar = far;
	updateProjection();
	updateModelview();
}


void QiViewport::setCameraPos(const QiVec3& pos)
{
	mState.cameraPos = pos;
	updateModelview();
}


QiVec3 QiViewport::getCameraPos() const
{
	return mState.cameraPos;
}


void QiViewport::setCameraRot(const QiQuat& rot)
{
	mState.cameraRot = rot;
	updateModelview();
}


QiQuat QiViewport::getCameraRot() const
{
	return mState.cameraRot;
}


void QiViewport::lookAt(const QiVec3& point, const QiVec3& cameraPos, const QiVec3& cameraUp)
{
}


QiVec3 QiViewport::project(const QiVec3& inPoint) const
{
	QiVec3 point = inPoint;
	float fTempo[8];
	const float* m = mState.modelviewMatrix.m;
	const float* p = mState.projectionMatrix.m;
	fTempo[0]=m[0]*point.x+m[4]*point.y+m[8]*point.z+m[12];
	fTempo[1]=m[1]*point.x+m[5]*point.y+m[9]*point.z+m[13];
	fTempo[2]=m[2]*point.x+m[6]*point.y+m[10]*point.z+m[14];
	fTempo[3]=m[3]*point.x+m[7]*point.y+m[11]*point.z+m[15];
	fTempo[4]=p[0]*fTempo[0]+p[4]*fTempo[1]+p[8]*fTempo[2]+p[12]*fTempo[3];
	fTempo[5]=p[1]*fTempo[0]+p[5]*fTempo[1]+p[9]*fTempo[2]+p[13]*fTempo[3];
	fTempo[6]=p[2]*fTempo[0]+p[6]*fTempo[1]+p[10]*fTempo[2]+p[14]*fTempo[3];
	fTempo[7]=-fTempo[2];

	if (mState.mode == MODE_3D)
	{
		//Perspective
		if(fTempo[7]==0.0f)
			return QiVec3();
		fTempo[7]=1.0f/fTempo[7];
		fTempo[4]*=fTempo[7];
		fTempo[5]*=fTempo[7];
		fTempo[6]*=fTempo[7];
	}

	//Window coordinates
	QiVec3 result;
	result.x=(fTempo[4]*0.5f+0.5f)*getWidth()+mState.bounds[0];
	result.y=getHeight()-((fTempo[5]*0.5f+0.5f)*getHeight()+mState.bounds[1]);
	//Only correct when glDepthRange(0.0, 1.0)
	result.z=(1.0f+fTempo[6])*0.5f;	//Between 0 and 1
	return result;
}


QiVec3 QiViewport::unproject(const QiVec3& pixel) const
{
	QiMatrix4 tmp = mState.projectionMatrix * mState.modelviewMatrix;
	if (!tmp.invert())
		return QiVec3();

	//Transformation of normalized coordinates between -1 and 1
	QiVec4 in(QI_UNINITIALIZED);
	in.x=(pixel.x-(float)mState.bounds[0])/(float)getWidth()*2.0f-1.0f;
	in.y=(getHeight()-(pixel.y+(float)mState.bounds[1]))/(float)getHeight()*2.0f-1.0f;
	if (mState.mode == MODE_3D)
		in.z=2.0f*(1.0f-mState.pNear/pixel.z)-1.0f;
	else
		in.z=0.0f;
	in.w=1.0f;
	//Objects coordinates
	QiVec4 out = tmp * in;

	if(out.w==0.0f)
		return QiVec3();
	out.w=1.0f/out.w;
	QiVec3 result;
	result.x=out.x*out.w;
	result.y=out.y*out.w;
	result.z=out.z*out.w;
	return result;
}


QiVec3 QiViewport::getPixelDirection(const QiVec2& pixel) const
{
	if (mState.mode == MODE_3D)
	{
		if (getWidth() == 0 || getHeight() == 0)
			return -QiVec3::Z;
		float h = 1.0f/getAspectRatio();
		float xx = pixel.x/(float)getWidth()*2.0f-1.0f;
		float yy = -pixel.y/(float)getHeight()*2.0f*h+h;
		QiTransform3 t(mState.cameraPos, mState.cameraRot);
		return t.toParentVec(normalize(QiVec3(xx, yy, -1.72f)));		// WHAT THE FUCK!?!?? 1.72?
	}
	else
		return -QiVec3::Z;
}


void QiViewport::updateProjection()
{
	switch(mState.mode)
	{
		case MODE_PIXEL:
		{
			float w = (float)(mState.bounds[2]-mState.bounds[0]);
			float h = (float)(mState.bounds[3]-mState.bounds[1]);
			mState.projectionMatrix.m[0] = 2.0f/w;
			mState.projectionMatrix.m[1] = 0.0f;
			mState.projectionMatrix.m[2] = 0.0f;
			mState.projectionMatrix.m[3] = 0.0f;
			mState.projectionMatrix.m[4] = 0.0f;
			mState.projectionMatrix.m[5] = -2.0f/h;
			mState.projectionMatrix.m[6] = 0.0f;
			mState.projectionMatrix.m[7] = 0.0f;
			mState.projectionMatrix.m[8] = mState.isoAmount*QiCos(mState.isoAngle)*2.0f/w;
			mState.projectionMatrix.m[9] = mState.isoAmount*QiSin(mState.isoAngle)*2.0f/h;
			mState.projectionMatrix.m[10] = 1.0f;//(-mState.pFar - mState.pNear) /  (mState.pFar - mState.pNear);
			mState.projectionMatrix.m[11] = 0.0f;
			mState.projectionMatrix.m[12] = -1.0f;
			mState.projectionMatrix.m[13] = 1.0f;
			mState.projectionMatrix.m[14] = 0.0f;
			mState.projectionMatrix.m[15] = 1.0f;
			break;
		}
		case MODE_PIXEL_FLIPPED:
		{
			float w = (float)(mState.bounds[2]-mState.bounds[0]);
			float h = (float)(mState.bounds[3]-mState.bounds[1]);
			mState.projectionMatrix.m[0] = 2.0f/w;
			mState.projectionMatrix.m[1] = 0.0f;
			mState.projectionMatrix.m[2] = 0.0f;
			mState.projectionMatrix.m[3] = 0.0f;
			mState.projectionMatrix.m[4] = 0.0f;
			mState.projectionMatrix.m[5] = 2.0f/h;
			mState.projectionMatrix.m[6] = 0.0f;
			mState.projectionMatrix.m[7] = 0.0f;
			mState.projectionMatrix.m[8] = mState.isoAmount*QiCos(mState.isoAngle)*2.0f/w;
			mState.projectionMatrix.m[9] = -mState.isoAmount*QiSin(mState.isoAngle)*2.0f/h;
			mState.projectionMatrix.m[10] = 1.0f;//(-mState.pFar - mState.pNear) /  (mState.pFar - mState.pNear);
			mState.projectionMatrix.m[11] = 0.0f;
			mState.projectionMatrix.m[12] = -1.0f;
			mState.projectionMatrix.m[13] = -1.0f;
			mState.projectionMatrix.m[14] = 0.0f;
			mState.projectionMatrix.m[15] = 1.0f;
			break;
		}
		case MODE_ORTHO:
		{
			mState.projectionMatrix.m[0] = 1.0f;
			mState.projectionMatrix.m[1] = 0.0f;
			mState.projectionMatrix.m[2] = 0.0f;
			mState.projectionMatrix.m[3] = 0.0f;
			mState.projectionMatrix.m[4] = 0.0f;
			mState.projectionMatrix.m[5] = 1.0f;
			mState.projectionMatrix.m[6] = 0.0f;
			mState.projectionMatrix.m[7] = 0.0f;
			mState.projectionMatrix.m[8] = mState.isoAmount*QiCos(mState.isoAngle);
			mState.projectionMatrix.m[9] = -mState.isoAmount*QiSin(mState.isoAngle);
			mState.projectionMatrix.m[10] = 1.0f;
			mState.projectionMatrix.m[11] = 0.0f;
			mState.projectionMatrix.m[12] = 0.0f;
			mState.projectionMatrix.m[13] = 0.0f;
			mState.projectionMatrix.m[14] = 0.0f;
			mState.projectionMatrix.m[15] = 1.0f;
			break;
		}
		case MODE_2D:
		{
			float halfWidth = mState.pFov/2;
			float halfHeight = halfWidth / getAspectRatio(); 
			mState.projectionMatrix.m[0] = 1.0f / halfWidth;
			mState.projectionMatrix.m[1] = 0.0f;
			mState.projectionMatrix.m[2] = 0.0f;
			mState.projectionMatrix.m[3] = 0.0f;
			mState.projectionMatrix.m[4] = 0.0f;
			mState.projectionMatrix.m[5] = 1.0f / halfHeight;
			mState.projectionMatrix.m[6] = 0.0f;
			mState.projectionMatrix.m[7] = 0.0f;
			mState.projectionMatrix.m[8] = mState.isoAmount*QiCos(mState.isoAngle);
			mState.projectionMatrix.m[9] = -mState.isoAmount*QiSin(mState.isoAngle);
			mState.projectionMatrix.m[10] = -1.0f / (mState.pFar - mState.pNear);
			mState.projectionMatrix.m[11] = 0.0f;
			mState.projectionMatrix.m[12] = 0.0f;
			mState.projectionMatrix.m[13] = 0.0f;
			mState.projectionMatrix.m[14] = 0.0f;
			mState.projectionMatrix.m[15] = 1.0f;
			break;
		}
		case MODE_3D:
		{
			float halfWidth = mState.pNear * QiTan(mState.pFov * QI_PI / 360.0f);
			float halfHeight = halfWidth / getAspectRatio();
			float left = -halfWidth;
			float right = halfWidth;
			float bottom = -halfHeight;
			float top = halfHeight;
			float temp, temp2, temp3, temp4;
			temp = 2.0f * mState.pNear;
			temp2 = right - left;
			temp3 = top - bottom;
			temp4 = mState.pFar - mState.pNear;
			mState.projectionMatrix.m[0] = temp / temp2;
			mState.projectionMatrix.m[1] = 0.0f;
			mState.projectionMatrix.m[2] = 0.0f;
			mState.projectionMatrix.m[3] = 0.0f;
			mState.projectionMatrix.m[4] = 0.0f;
			mState.projectionMatrix.m[5] = temp / temp3;
			mState.projectionMatrix.m[6] = 0.0f;
			mState.projectionMatrix.m[7] = 0.0f;
			mState.projectionMatrix.m[8] = (right + left) / temp2;
			mState.projectionMatrix.m[9] = (top + bottom) / temp3;
			mState.projectionMatrix.m[10] = (-mState.pFar - mState.pNear) / temp4;
			mState.projectionMatrix.m[11] = -1.0f;
			mState.projectionMatrix.m[12] = 0.0f;
			mState.projectionMatrix.m[13] = 0.0f;
			mState.projectionMatrix.m[14] = (-temp * mState.pFar) / temp4;
			mState.projectionMatrix.m[15] = 0.0f;
			break;
		}
	}
}


void QiViewport::updateModelview()
{
	switch(mState.mode)
	{
		case MODE_2D:
		{
			mState.modelviewMatrix.m[0] = 1.0f;
			mState.modelviewMatrix.m[1] = 0.0f;
			mState.modelviewMatrix.m[2] = 0.0f;
			mState.modelviewMatrix.m[3] = 0.0f;
			mState.modelviewMatrix.m[4] = 0.0f;
			mState.modelviewMatrix.m[5] = 1.0f;
			mState.modelviewMatrix.m[6] = 0.0f;
			mState.modelviewMatrix.m[7] = 0.0f;
			mState.modelviewMatrix.m[8] = 0.0f;
			mState.modelviewMatrix.m[9] = 0.0f;
			mState.modelviewMatrix.m[10] = 1.0f;
			mState.modelviewMatrix.m[11] = 0.0f;
			mState.modelviewMatrix.m[12] = -mState.cameraPos.x;
			mState.modelviewMatrix.m[13] = -mState.cameraPos.y;
			mState.modelviewMatrix.m[14] = 0.0f;
			mState.modelviewMatrix.m[15] = 1.0f;
			break;
		}
		case MODE_3D:
		{
			QiVec3 base0 = (-mState.cameraRot).getBase(0);
			QiVec3 base1 = (-mState.cameraRot).getBase(1);
			QiVec3 base2 = (-mState.cameraRot).getBase(2);
			QiVec3 pos = mState.cameraRot.rotateInv(mState.cameraPos);
			mState.modelviewMatrix.m[0] = base0.x;
			mState.modelviewMatrix.m[1] = base1.x;
			mState.modelviewMatrix.m[2] = base2.x;
			mState.modelviewMatrix.m[3] = 0.0f;
			mState.modelviewMatrix.m[4] = base0.y;
			mState.modelviewMatrix.m[5] = base1.y;
			mState.modelviewMatrix.m[6] = base2.y;
			mState.modelviewMatrix.m[7] = 0.0f;
			mState.modelviewMatrix.m[8] = base0.z;
			mState.modelviewMatrix.m[9] = base1.z;
			mState.modelviewMatrix.m[10] = base2.z;
			mState.modelviewMatrix.m[11] = 0.0f;
			mState.modelviewMatrix.m[12] = -pos.x;
			mState.modelviewMatrix.m[13] = -pos.y;
			mState.modelviewMatrix.m[14] = -pos.z;
			mState.modelviewMatrix.m[15] = 1.0f;
			break;
		}
		default:
		{
			mState.modelviewMatrix.m[0] = 1.0f;
			mState.modelviewMatrix.m[1] = 0.0f;
			mState.modelviewMatrix.m[2] = 0.0f;
			mState.modelviewMatrix.m[3] = 0.0f;
			mState.modelviewMatrix.m[4] = 0.0f;
			mState.modelviewMatrix.m[5] = 1.0f;
			mState.modelviewMatrix.m[6] = 0.0f;
			mState.modelviewMatrix.m[7] = 0.0f;
			mState.modelviewMatrix.m[8] = 0.0f;
			mState.modelviewMatrix.m[9] = 0.0f;
			mState.modelviewMatrix.m[10] = 1.0f;
			mState.modelviewMatrix.m[11] = 0.0f;
			mState.modelviewMatrix.m[12] = 0.0f;
			mState.modelviewMatrix.m[13] = 0.0f;
			mState.modelviewMatrix.m[14] = 0.0f;
			mState.modelviewMatrix.m[15] = 1.0f;
			break;
		}
	}
}


void QiViewport::translate(const QiVec3& v)
{
	mState.modelviewMatrix *= QiMatrix4(v, QiQuat());
}


void QiViewport::rotate(const QiQuat& q)
{
	mState.modelviewMatrix *= QiMatrix4(QiVec3(), q);
}


void QiViewport::scale(const QiVec3& v)
{
	QiMatrix4 m;
	m.m[0] = v.x;
	m.m[5] = v.y;
	m.m[10] = v.z;
	mState.modelviewMatrix *= m;
}


void QiViewport::transform(const QiTransform3& t)
{
	mState.modelviewMatrix *= QiMatrix4(t);
}


void QiViewport::transform(const QiMatrix4& m)
{
	mState.modelviewMatrix *= m;
}


#if !defined(QI_IOS) && !defined(QI_ANDROID)
#include "gfx/qigl.h"
void QiViewport::apply()
{
	int w = mState.bounds[2]-mState.bounds[0];
	int h = mState.bounds[3]-mState.bounds[1];
	glViewport(mState.bounds[0], mState.bounds[1], w, h); 

	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(mState.projectionMatrix.m);

	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixf(mState.modelviewMatrix.m);
}
#endif



inline bool QiViewport::isCurrentPicked() const
{
	if (mPickResult.getCount() != mBestPick.getCount())
		return false;
	for(int i=0; i<mPickResult.getCount(); i++)
	{
		if (mPickResult[i] != mBestPick[i])
			return false;
	}
	return true;
}


void QiViewport::pick(int x, int y, float r)
{
	//Copy pick results from last frame
	mPickResult.clear();
	mPickResult.addAll(mBestPick.getIterator());

	//Prepare new frame
	mBestPick.clear();
	mPickStack.clear();
	mPickDepth = QI_FLOAT_MAX;
	mPickPos.set((float)x, (float)y);
}

void QiViewport::pickClear()
{
	mBestPick.clear();
	mPickStack.clear();
	mPickDepth = QI_FLOAT_MAX;
}

void QiViewport::pickPush(int id)
{
	mPickStack.add(id);
}

void QiViewport::pickPop()
{
	if (mPickStack.getCount() > 0)
		mPickStack.removeLast();
}

bool QiViewport::pickPoint(int id, const QiVec3& p, float tolerance)
{
	QiVec3 sp = project(p);
	float dd = lengthSquared(QiVec2(sp.x, sp.y)-mPickPos);
if (dd < tolerance*tolerance)
	return true;
	if (dd < tolerance*tolerance && sp.z<mPickDepth)
	{
		mPickDepth = sp.z;
		mBestPick.clear();
		mBestPick.addAll(mPickStack.getIterator());
		mBestPick.add(id);
		return isCurrentPicked();
	}
	return false;
}

bool QiViewport::pickLine(int id, const QiVec3& p0, const QiVec3& p1, float tolerance)
{
	QiVec3 sp0 = project(p0);
	QiVec3 sp1 = project(p1);
	//Check 2D distance to line in screen space
	float t;
	float sqDist = QiDistanceSq(mPickPos, QiVec2(sp0.x, sp0.y), QiVec2(sp1.x, sp1.y), &t);

if (sqDist < tolerance*tolerance)
	return true;
	//Depth to intersection point
	float depth = sp0.z + (sp1.z-sp0.z)*t;
	if (sqDist < tolerance*tolerance && depth<mPickDepth)
	{
		mPickDepth = depth;
		mBestPick.clear();
		mBestPick.addAll(mPickStack.getIterator());
		mBestPick.add(id);
		return isCurrentPicked();
	}
	return false;
}

bool QiViewport::pickTri(int id, const QiVec3& p0, const QiVec3& p1, const QiVec3& p2)
{
	//Swizzle order, since y is flipped
	QiVec3 sp0 = project(p0);
	QiVec3 sp1 = project(p2);
	QiVec3 sp2 = project(p1);

	//Early out with inside/outside test
	QiVec2 e0(sp1.x-sp0.x, sp1.y-sp0.y);
	QiVec2 e1(sp2.x-sp1.x, sp2.y-sp1.y);
	QiVec2 e2(sp0.x-sp2.x, sp0.y-sp2.y);
	QiVec2 n0(-e0.y, e0.x);
	QiVec2 n1(-e1.y, e1.x);
	QiVec2 n2(-e2.y, e2.x);
	if (dot(n0, mPickPos-QiVec2(sp0.x, sp0.y)) < 0.0f)
		return false;
	if (dot(n1, mPickPos-QiVec2(sp1.x, sp1.y)) < 0.0f)
		return false;
	if (dot(n2, mPickPos-QiVec2(sp2.x, sp2.y)) < 0.0f)
		return false;
	
	//Compute barycentric
	float a = (sp2.x - sp1.x)*(mPickPos.y - sp1.y) - (mPickPos.x - sp1.x)*(sp2.y - sp1.y);
	float b = (sp0.x - sp2.x)*(mPickPos.y - sp2.y) - (mPickPos.x - sp2.x)*(sp0.y - sp2.y);
	float c = (sp1.x - sp0.x)*(mPickPos.y - sp0.y) - (mPickPos.x - sp0.x)*(sp1.y - sp0.y);
	float s = a+b+c;
	a /= s;
	b /= s;
	c /= s;
return true;
	//Compute depth
	float depth = sp0.z*a + sp1.z*b + sp2.z*c;
	if (depth<=mPickDepth)
	{
		mPickDepth = depth;
		mBestPick.clear();
		mBestPick.addAll(mPickStack.getIterator());
		mBestPick.add(id);
		return isCurrentPicked();
	}
	return false;
}

bool QiViewport::pickRect(int id, const QiVec3& p0, const QiVec3& p1, const QiVec3& p2, const QiVec3& p3)
{
	return pickTri(id, p0, p1, p2) || pickTri(id, p2, p3, p0);
}

bool QiViewport::pickRect(int id, const QiVec2& lower, const QiVec2& upper, float tolerance)
{
	QiVec3 p0(lower.x-tolerance, lower.y-tolerance, 0);
	QiVec3 p1(upper.x+tolerance, lower.y-tolerance, 0);
	QiVec3 p2(upper.x+tolerance, upper.y+tolerance, 0);
	QiVec3 p3(lower.x-tolerance, upper.y+tolerance, 0);
	return pickTri(id, p0, p1, p2) || pickTri(id, p2, p3, p0) || pickTri(id, p0, p2, p1) || pickTri(id, p2, p0, p3);
}

bool QiViewport::isPicked(int i0)
{
	return (mPickResult.getCount() > 0 && mPickResult[0] == i0);
}

bool QiViewport::isPicked(int i0, int i1)
{
	return (mPickResult.getCount() > 1 && mPickResult[0] == i0 && mPickResult[1] == i1);
}

bool QiViewport::isPicked(int i0, int i1, int i2)
{
	return (mPickResult.getCount() > 2 && mPickResult[0] == i0 && mPickResult[1] == i1 && mPickResult[2] == i2);
}

bool QiViewport::isPicked(int i0, int i1, int i2, int i3)
{
	return (mPickResult.getCount() > 3 && mPickResult[0] == i0 && mPickResult[1] == i1 && mPickResult[2] == i2 && mPickResult[3] == i3);
}

