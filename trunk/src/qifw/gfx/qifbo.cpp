/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "gfx/qifbo.h"
#include "base/qidebug.h"
#include "gfx/qigl.h"

#ifdef QI_WIN32
#define GL_FRAMEBUFFER_BINDING GL_FRAMEBUFFER_BINDING_EXT
#define glGenFramebuffers glGenFramebuffersEXT
#define GL_FRAMEBUFFER GL_FRAMEBUFFER_EXT
#define glBindFramebuffer glBindFramebufferEXT
#define glFramebufferTexture2D glFramebufferTexture2DEXT
#define GL_COLOR_ATTACHMENT0 GL_COLOR_ATTACHMENT0_EXT
#define glGenRenderbuffers glGenRenderbuffersEXT
#define glBindRenderbuffer glBindRenderbufferEXT
#define GL_RENDERBUFFER GL_RENDERBUFFER_EXT
#define GL_DEPTH_ATTACHMENT GL_DEPTH_ATTACHMENT_EXT
#define glRenderbufferStorage glRenderbufferStorageEXT
#define glFramebufferRenderbuffer glFramebufferRenderbufferEXT
#define glCheckFramebufferStatus glCheckFramebufferStatusEXT
#define GL_FRAMEBUFFER_COMPLETE GL_FRAMEBUFFER_COMPLETE_EXT
#endif

//static GLenum buffers[] = { GL_COLOR_ATTACHMENT0 };

int QiFbo::sStack[QI_FBO_MAX_STACK_DEPTH];
int QiFbo::sStackBuffers[QI_FBO_MAX_STACK_DEPTH];
int QiFbo::sStackPtr = 0;

QiFbo::QiFbo() : 
mFbo(0), mDepth(0), mAttachmentCount(0), mWidth(0), mHeight(0)
{
	for(int i=0; i<QI_FBO_MAX_ATTACHMENTS; i++)
		mColor[i] = 0;
}


QiFbo::QiFbo(int width, int height) :
mFbo(0), mDepth(0), mAttachmentCount(0)
{
	for(int i=0; i<QI_FBO_MAX_ATTACHMENTS; i++)
		mColor[i] = 0;
	init(width, height, GL_RGBA, 1);
}


QiFbo::QiFbo(int width, int height, int format) :
mFbo(0), mDepth(0), mAttachmentCount(0)
{
	for(int i=0; i<QI_FBO_MAX_ATTACHMENTS; i++)
		mColor[i] = 0;
	init(width, height, format, 1);
}


QiFbo::QiFbo(int width, int height, int format, int attachmentCount) :
mFbo(0), mDepth(0), mAttachmentCount(0)
{
	for(int i=0; i<QI_FBO_MAX_ATTACHMENTS; i++)
		mColor[i] = 0;
	init(width, height, format, attachmentCount);
}


QiFbo::~QiFbo()
{
}

void QiFbo::init(int width, int height)
{
	init(width, height, GL_RGBA, 1);
}

static int gDefaultFbo = 0;

void QiFbo::init(int width, int height, int format, int attachmentCount)
{
    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &gDefaultFbo);
    
	mAttachmentCount = attachmentCount;
	mWidth = width;
	mHeight = height;	
	glGenFramebuffers(1, (GLuint*)&mFbo);
	glBindFramebuffer(GL_FRAMEBUFFER, mFbo);
	
	glGenTextures(mAttachmentCount, (GLuint*)mColor);
	for(int i=0; i<mAttachmentCount; i++)
	{
		glBindTexture(GL_TEXTURE_2D, mColor[i]);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,  mWidth, mHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0+i, GL_TEXTURE_2D, mColor[i], 0);
		glBindTexture(GL_TEXTURE_2D, 0);
	}

    glGenRenderbuffers(1, (GLuint*)&mDepth);
	glBindRenderbuffer(GL_RENDERBUFFER, mDepth);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, mWidth, mHeight);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, mDepth);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);    
    
    // check the frame buffer
    GLuint status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if ( status != GL_FRAMEBUFFER_COMPLETE)
    {
        QI_PRINT("Frame buffer cannot be generated! Status: " + status);
    }
    
 
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


void QiFbo::shutdown()
{
}


void QiFbo::update(int width, int height)
{
	if (width != mWidth || height != mHeight)
	{
		shutdown();
		init(width, height);
	}
}


void QiFbo::useAsTarget()
{
	glBindFramebuffer(GL_FRAMEBUFFER, mFbo);
	//glDrawBuffers(mAttachmentCount, buffers);
}


void QiFbo::useAsViewport()
{
	glViewport(0, 0, mWidth, mHeight);
}


void QiFbo::useAsTexture(int attachment, int texUnit)
{
	glActiveTexture(GL_TEXTURE0 + texUnit);
	glBindTexture(GL_TEXTURE_2D, mColor[attachment]);
}


void QiFbo::useDefaultTarget()
{
	glBindFramebuffer(GL_FRAMEBUFFER, gDefaultFbo);
	//glDrawBuffers(1, buffers);
}


int QiFbo::getTextureId(int attachment)
{
	return mColor[attachment];
}


