/* Qi Framework. Copyright 2007-2012 Dengu AB */

#ifdef QI_USE_OPENGL

#include "gfx/qigfxutil.h"
#include "gfx/qigl.h"
#include "math/qitransform3.h"


int QiGfxUtil::sLists[QI_GFX_UTIL_MAX_LISTS];


void QiGfxUtil::getBillboardBase(QiVec3& x, QiVec3& y, QiVec3& z)
{
	float modelview[16];
	glGetFloatv(GL_MODELVIEW_MATRIX, modelview);
	x.set(modelview[0], modelview[4], modelview[8]);
	y.set(modelview[1], modelview[5], modelview[9]);
	z.set(modelview[2], modelview[6], modelview[10]);
}


void QiGfxUtil::enterOrthoMode()
{
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(-1, 1, -1, 1, -1, 1);
}


void QiGfxUtil::leaveOrthoMode()
{
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}


void QiGfxUtil::enterPixelMode()
{
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0, 1, 0, 1, -1, 1);
	GLint vp[4];
	glGetIntegerv(GL_VIEWPORT, vp);
	glScalef(1.0f/(vp[2]-vp[0]), 1.0f/(vp[3]-vp[1]), 1.0f);
}


void QiGfxUtil::leavePixelMode()
{
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}


void QiGfxUtil::newList(int list)
{
	QI_ASSERT(list < QI_GFX_UTIL_MAX_LISTS, "Only " + QI_GFX_UTIL_MAX_LISTS + " allowed");
	sLists[list] = glGenLists(1);
	glNewList(sLists[list], GL_COMPILE);
}


void QiGfxUtil::endList()
{
	glEndList();
}


void QiGfxUtil::callList(int list)
{
	QI_ASSERT(list < QI_GFX_UTIL_MAX_LISTS, "Only " + QI_GFX_UTIL_MAX_LISTS + " allowed");
	glCallList(sLists[list]);
}


void QiGfxUtil::deleteList(int list)
{
	QI_ASSERT(list < QI_GFX_UTIL_MAX_LISTS, "Only " + QI_GFX_UTIL_MAX_LISTS + " allowed");
	glDeleteLists(sLists[list], 1);
}


void QiGfxUtil::clear()
{
	glClearColor(0, 0, 0, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}


void QiGfxUtil::drawQuad(const QiVec3& x, const QiVec3& z)
{
	QiVec3 p0 = -x-z;
	QiVec3 p1 = x-z;
	QiVec3 p2 = x+z;
	QiVec3 p3 = -x+z;
	glBegin(GL_QUADS);
	glVertex3fv(&p0.x);
	glVertex3fv(&p1.x);
	glVertex3fv(&p2.x);
	glVertex3fv(&p3.x);
	glEnd();
}


void QiGfxUtil::drawTexQuad(const QiVec3& x, const QiVec3& y)
{
	glEnable(GL_TEXTURE_2D);
	QiVec3 p0 = -x-y;
	QiVec3 p1 = x-y;
	QiVec3 p2 = x+y;
	QiVec3 p3 = -x+y;
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);
	glVertex3fv(&p0.x);
	glTexCoord2f(1, 0);
	glVertex3fv(&p1.x);
	glTexCoord2f(1, 1);
	glVertex3fv(&p2.x);
	glTexCoord2f(0, 1);
	glVertex3fv(&p3.x);
	glEnd();
	glDisable(GL_TEXTURE_2D);
}


void QiGfxUtil::scaleImage(const unsigned char* in, int inWidth, int inHeight, int inChannels, unsigned char* out, int outWidth, int outHeight, int outChannels)
{
	int channels = QiMin(inChannels, outChannels);
	for(int outY=0; outY < outHeight; outY++)
	{
		for(int outX=0; outX < outWidth; outX++)
		{
			int outIndex = outY * outWidth + outX;
			int inX = (outX * inWidth) / outWidth;
			int inY = (outY * inHeight) / outHeight;
			int inIndex = inY * inWidth + inX;
			if (inChannels == 2 && outChannels == 4)
			{
				out[outIndex*outChannels+0] = in[inIndex*inChannels+0];
				out[outIndex*outChannels+1] = in[inIndex*inChannels+0];
				out[outIndex*outChannels+2] = in[inIndex*inChannels+0];
				out[outIndex*outChannels+3] = in[inIndex*inChannels+1];
			}
			else if (inChannels == 1 && outChannels == 4)
			{
				out[outIndex*outChannels+0] = in[inIndex*inChannels+0];
				out[outIndex*outChannels+1] = in[inIndex*inChannels+0];
				out[outIndex*outChannels+2] = in[inIndex*inChannels+0];
				out[outIndex*outChannels+3] = 255;
			}
			else
			{
				for(int c=0; c < channels; c++)
					out[outIndex*outChannels+c] = in[inIndex*inChannels+c];
				for(int c=inChannels; c < outChannels; c++)
					out[outIndex*outChannels+c] = 255;
			}
		}
	}
}

void QiGfxUtil::enterFrame(const QiTransform3& frame)
{
	glPushMatrix();
	QiVec3 base0 = frame.rot.getBase(0);
	QiVec3 base1 = frame.rot.getBase(1);
	QiVec3 base2 = frame.rot.getBase(2);

	GLfloat f[16];

	f[0] = base0.x;
	f[1] = base0.y;
	f[2] = base0.z;
	f[3] = 0;

	f[4] = base1.x;
	f[5] = base1.y;
	f[6] = base1.z;
	f[7] = 0;

	f[8] = base2.x;
	f[9] = base2.y;
	f[10] = base2.z;
	f[11] = 0;

	f[12] = frame.pos.x;
	f[13] = frame.pos.y;
	f[14] = frame.pos.z;
	f[15] = 1;

	glMultMatrixf(f);
}

void QiGfxUtil::leaveFrame()
{
	glPopMatrix();
}

void QiGfxUtil::drawFullScreenQuad()
{
	enterOrthoMode();
	drawQuad();
	leaveOrthoMode();
}

void QiGfxUtil::drawFullScreenTexQuad()
{
	enterOrthoMode();
	drawTexQuad();
	leaveOrthoMode();
}

#endif
