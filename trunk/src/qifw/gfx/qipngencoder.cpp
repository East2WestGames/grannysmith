/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include <stdio.h>
#include "base/qiconfig.h"
#include "base/qimem.h"
#include "base/qistream.h"

#ifdef QI_WIN32
extern "C"
{
#endif
#include "gfx/png/png.h"
#ifdef QI_WIN32
}
#endif

#include "base/qidebug.h"
#include "gfx/qipngencoder.h"

static png_voidp pngQiAlloc(png_structp, png_size_t size)
{
	return (png_voidp*)QiAlloc(size);
}

static void pngQiFree(png_structp, png_voidp ptr)
{
	QiFree(ptr);
}

void user_write_data(png_structp png_ptr, png_bytep data, png_size_t length)
{
	QiOutputStream* out = (QiOutputStream*)png_get_io_ptr(png_ptr);
	if (out)
		out->writeBuffer((const char*)data, length);
}


void user_flush_data(png_structp png_ptr)
{
	QiOutputStream* out = (QiOutputStream*)png_get_io_ptr(png_ptr);
	if (out)
		out->flush();
}


class QiPngEncoderImpl
{
public:
	QiPngEncoderImpl() : img_width(0), img_height(0), img_hasAlpha(false)
	{
		png_ptr = NULL;
		info_ptr = NULL;
	}
	
	~QiPngEncoderImpl()
	{
		if (png_ptr && info_ptr)
			png_destroy_write_struct(&png_ptr, &info_ptr);
	}

	bool init(QiOutputStream& stream, int width, int height, bool hasAlpha)
	{
		img_width = width;
		img_height = height;
		img_hasAlpha = hasAlpha;
		png_ptr = png_create_write_struct_2(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL, NULL, pngQiAlloc, pngQiFree);

		if (!png_ptr)
		{
			png_destroy_write_struct(&png_ptr, NULL);		
			return false;
		}

		info_ptr = png_create_info_struct(png_ptr);
		if (!info_ptr)
			return false;

		png_set_write_fn(png_ptr, (void *)&stream, user_write_data, user_flush_data);

		int color_type = hasAlpha ? PNG_COLOR_TYPE_RGBA : PNG_COLOR_TYPE_RGB;
		png_set_IHDR(png_ptr, info_ptr, width, height,
				 8, color_type, PNG_INTERLACE_NONE,
				 PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

		png_write_info(png_ptr, info_ptr);

		return true;
	}

	bool write(unsigned char* pixels, bool flip)
	{
		int bitsPerPixel = img_hasAlpha ? 4 : 3;
		png_bytep* row_pointers = (png_bytep*) QiAlloc(sizeof(png_bytep) * img_height);
		if (flip)
		{
			for(int i=0; i<img_height; i++)
				row_pointers[img_height-i-1] = pixels + i*img_width*bitsPerPixel;
		}
		else
		{
			for(int i=0; i<img_height; i++)
				row_pointers[i] = pixels + i*img_width*bitsPerPixel;
		}
		png_write_image(png_ptr, row_pointers);
		png_write_end(png_ptr, NULL);
		QiFree(row_pointers);
		return true;
	}

	png_structp png_ptr;
	png_infop info_ptr;
	int img_width;
	int img_height;
	bool img_hasAlpha;
};


QiPngEncoder::QiPngEncoder()
{
	mImpl = QI_NEW QiPngEncoderImpl();
}


QiPngEncoder::QiPngEncoder(QiOutputStream& outStream, int width, int height, bool hasAlpha)
{
	mImpl = QI_NEW QiPngEncoderImpl();
	init(outStream, width, height, hasAlpha);
}


QiPngEncoder::~QiPngEncoder()
{
	QI_DELETE(mImpl);
}


bool QiPngEncoder::init(QiOutputStream& outStream, int width, int height, bool hasAlpha)
{
	return mImpl->init(outStream, width, height, hasAlpha);
}


bool QiPngEncoder::encode(unsigned char* pixels, bool flip)
{
	return mImpl->write(pixels, flip);
}


