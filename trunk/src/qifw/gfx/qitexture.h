#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "math/qivec2.h"

class QiTexture
{
public:
	QiTexture();
	QiTexture(int width, int height);
	QiTexture(int width, int height, int format);
	~QiTexture();

	void init(int width, int height);
	void init(int width, int height, int format, bool allocMem=true);
	void loadDefault();
	void loadBlob();
	void upload();
	void upload(unsigned char* data, int mipLevel, int w, int h);
	void upload(unsigned char* data, int mipLevel, int w, int h, int offset);
    void uploadPVRTC(void* data, int size);
	void uploadDXT(void* data, int dataSize);
	void use();	
	void free();
	void releaseMemory();
	inline void enableMipMap(bool enabled) { mMipMap = enabled; }
	void enableRepeat(bool enabled);
	inline bool hasMipMaps() const { return mMipMap; }
	int detach();

	inline int getId() const { return mId; }
	inline int getWidth() const { return mWidth; }
	inline int getHeight() const { return mHeight; }
	inline int getFormat() const { return mFormat; }
	inline void* getData() { return mData; }

	int getMemSize() const { return mMemSize; }

	static int getAllocatedBytes();
	static void getTiledTexCoords(int rows, int cols, int tile, QiVec2 t[4]);
	static void getTiledTexCoords(const QiVec2& texMin, const QiVec2& texMax, int rows, int cols, int tile, QiVec2 t[4]);

    void markRetina();
    
protected:
	int mId;
	int mWidth;
	int mHeight;
	int mFormat;
	void* mData;
	bool mMipMap;
	int mMemSize;
	bool mRepeat;
};

