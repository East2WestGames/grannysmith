/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qistream.h"
#include "base/qimem.h"
#include "base/qidebug.h"
#include "gfx/qipngdecoder.h"

#include <stdio.h>
#include <string.h>
#include <setjmp.h>

#ifdef QI_WIN32
extern "C"
{
#endif
#include "gfx/png/png.h"
#ifdef QI_WIN32
}
#endif

static png_voidp pngQiAlloc(png_structp, png_size_t size)
{
	return (png_voidp*)QiAlloc(size);
}

static void pngQiFree(png_structp, png_voidp ptr)
{
	QiFree(ptr);
}

void user_read_data(png_structp png_ptr, png_bytep data, png_size_t length)
{
	QiInputStream* stream = (QiInputStream*)png_get_io_ptr(png_ptr);
	if (stream)
		stream->readBuffer((char*)data, length);
}

void user_error_fn(png_structp png_ptr, png_const_charp error_msg);

class QiPngDecoderImpl
{
public:
	QiPngDecoderImpl() : img_width(0), img_height(0), img_hasAlpha(false), inited(false),
	bitDepth(0), channelCount(0)
	{
		png_ptr = NULL;
	}
	
	~QiPngDecoderImpl()
	{
		if (png_ptr && info_ptr)
			png_destroy_read_struct(&png_ptr, &info_ptr, NULL);	
	}

	bool init(QiInputStream& inStream, size_t size)
	{
		//Verify it's a PNG image
		char header[8];
		if (!inStream.readBuffer(header, 8))
			return false;
		if (png_sig_cmp((png_bytep)header, 0, 8))
			return false;

		png_ptr = png_create_read_struct_2(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL, NULL, pngQiAlloc, pngQiFree);

		if (!png_ptr)
			return false;

		png_set_error_fn(png_ptr, this, user_error_fn, NULL);
		if (::setjmp(jmpbuf) != 0)
			return false;

		info_ptr = png_create_info_struct(png_ptr);
		if (!info_ptr)
			return false;

		png_set_read_fn(png_ptr, &inStream, user_read_data);

		png_set_sig_bytes(png_ptr, 8);

		png_read_info(png_ptr, info_ptr);
		if (info_ptr->color_type == PNG_COLOR_TYPE_PALETTE)
		    png_set_palette_to_rgb(png_ptr);
		if (info_ptr->color_type == PNG_COLOR_TYPE_GRAY && info_ptr->bit_depth < 8)
		    png_set_gray_1_2_4_to_8(png_ptr);
		if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
		    png_set_tRNS_to_alpha(png_ptr);
	    if (info_ptr->color_type == PNG_COLOR_TYPE_GRAY || info_ptr->color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
	        png_set_gray_to_rgb(png_ptr);
		if (info_ptr->interlace_type != PNG_INTERLACE_NONE)
			png_set_interlace_handling(png_ptr);
		png_read_update_info(png_ptr, info_ptr);

		img_width = info_ptr->width;
		img_height = info_ptr->height;
		img_hasAlpha = (info_ptr->color_type == PNG_COLOR_TYPE_RGBA);
		bitDepth = info_ptr->bit_depth;
		channelCount = info_ptr->channels;

		inited = true;
		return true;
	}

	bool decode(unsigned char* pixels, bool flip)
	{
		if (!inited)
			return false;

		if (::setjmp(jmpbuf) != 0)
			return false;

		int bytesPerPixel = (bitDepth/8) * channelCount;
		png_bytep* rowpp = (png_bytep*)QiAlloc(img_height*sizeof(png_bytep*));
		for(int y=0; y<img_height; y++)
		{
			int realy = flip ? img_height-y-1 : y;
			png_bytep rowp = (png_bytep)(pixels+img_width*realy*bytesPerPixel);
			rowpp[y] = rowp;
		}
		png_read_image(png_ptr, rowpp);
		QiFree(rowpp);
		return true;
	}

	png_structp png_ptr;
	png_infop info_ptr;
	int img_width;
	int img_height;
	bool img_hasAlpha;
	bool inited;
	jmp_buf jmpbuf;
	int bitDepth;
	int channelCount;
};


void user_error_fn(png_structp png_ptr, png_const_charp error_msg)
{
	QiPngDecoderImpl* impl = (QiPngDecoderImpl*)png_get_error_ptr(png_ptr);
	::longjmp(impl->jmpbuf, 1);
}


QiPngDecoder::QiPngDecoder()
{
	mImpl = QI_NEW QiPngDecoderImpl();
}


QiPngDecoder::QiPngDecoder(QiInputStream& inStream, size_t size)
{
	mImpl = QI_NEW QiPngDecoderImpl();
	init(inStream, size);
}


QiPngDecoder::~QiPngDecoder()
{
	QI_DELETE(mImpl);
}


bool QiPngDecoder::init(QiInputStream& inStream, size_t size)
{
	return mImpl->init(inStream, size);
}


int QiPngDecoder::getWidth() const
{
	return mImpl->img_width;
}


int QiPngDecoder::getHeight() const
{
	return mImpl->img_height;
}


bool QiPngDecoder::hasAlpha() const
{
	return mImpl->img_hasAlpha;
}


bool QiPngDecoder::decode(unsigned char* pixels, bool flip)
{
	return mImpl->decode(pixels, flip);
}


int QiPngDecoder::getBitDepth() const
{
	return mImpl->bitDepth;
}


int QiPngDecoder::getChannelCount() const
{
	return mImpl->channelCount;
}

