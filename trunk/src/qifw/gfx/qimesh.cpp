/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "gfx/qimesh.h"
#include "geometry/qilinesegment.h"
#include "geometry/qiintersection.h"

QiMesh::QiMesh()
{
}

QiMesh::~QiMesh()
{
}

void QiMesh::set(const QiVec3* points, int vertexCount, const int* faces, int faceCount)
{
	mVertices.clear();
	for(int i=0; i<vertexCount; i++)
		mVertices.add(Vertex(points[i]));

	mFaces.clear();
	for(int i=0; i<faceCount; i++)
	{
		int f0 = faces[i*3+0];
		int f1 = faces[i*3+1];
		int f2 = faces[i*3+2];
		QI_ASSERT(f0 >= 0 && f0 < vertexCount, "Bad Input mesh: Face points to invalid vertex");
		QI_ASSERT(f1 >= 0 && f1 < vertexCount, "Bad Input mesh: Face points to invalid vertex");
		QI_ASSERT(f2 >= 0 && f2 < vertexCount, "Bad Input mesh: Face points to invalid vertex");
		mFaces.add(Face(f0, f1, f2));
	}

	computeConnectivity();
}

void QiMesh::computeNormals()
{
	//Compute face normals
	for(int i=0; i<mFaces.getCount(); i++)
	{
		QiVec3 e0 = mVertices[mFaces[i].vertices[1]].point - mVertices[mFaces[i].vertices[0]].point;
		QiVec3 e1 = mVertices[mFaces[i].vertices[2]].point - mVertices[mFaces[i].vertices[0]].point;
		QiVec3 n = cross(e0, e1);		
		mFaces[i].normal = normalize(n);
	}	
	//Average and normalize
	for(int i=0; i<mVertices.getCount(); i++)
	{
		QiVec3 acc;
		int count = getVertexFaceCount(i);
		for(int j=0; j<count; j++)
		{
			QiVec3 n = mFaces[getVertexFaceIndex(i, j)].normal;
			acc += n/(float)count;
		}
		mVertices[i].normal = normalize(acc);
	}	
}

//Use vertex and face structure only to build up full connectivity
void QiMesh::computeConnectivity()
{
	for(int i=0; i<mVertices.getCount(); i++)
	{
		mVertices[i].edgeCount = 0;
		mVertices[i].edgeStart = 0;
	}

	mEdges.clear();
	mEdgeIndex.clear();
	for(int i=0; i<mFaces.getCount(); i++)
	{
		for(int v=0; v<3; v++)
		{
			int v0 = mFaces[i].vertices[v];
			int v1 = mFaces[i].vertices[(v+1)%3];
			QI_ASSERT(v0 >= 0 && v0 <= mVertices.getCount(), "Error");
			QI_ASSERT(v1 >= 0 && v1 <= mVertices.getCount(), "Error");
			EdgeIndex ei(v0, v1);
			if (mEdgeIndex.get(ei))
			{
				if (mEdges[ei.index].faces[0] == -1) 
					mEdges[ei.index].faces[0] = i;
				else
					mEdges[ei.index].faces[1] = i;
			}
			else
			{
				ei.index = mEdges.getCount();
				mEdges.add(Edge(v0, v1));
				mEdgeIndex.put(ei);
			}
			mFaces[i].edges[v] = ei.index;
		}
	}

	mVertexEdgeList.redim(mEdges.getCount()*2);

	//Count
	for(int i=0; i<mEdges.getCount(); i++)
	{
		for(int v=0; v<2; v++)
		{
			if (mEdges[i].vertices[v] != -1)
				mVertices[mEdges[i].vertices[v]].edgeCount++;
		}
	}

	//Accumulate
	int start = 0;
	for(int i=0; i<mVertices.getCount(); i++)
	{
		mVertices[i].edgeStart = start;
		start += mVertices[i].edgeCount;
		mVertices[i].edgeCount = 0;
	}	

	//Fill in
	for(int i=0; i<mEdges.getCount(); i++)
	{
		for(int j=0; j<2; j++)
		{
			Vertex& v = mVertices[mEdges[i].vertices[j]];
			mVertexEdgeList[v.edgeStart+v.edgeCount] = i;
			v.edgeCount++;
		}
	}	
		
	//Count
	for(int i=0; i<mFaces.getCount(); i++)
	{
		mVertices[mFaces[i].vertices[0]].faceCount++;
		mVertices[mFaces[i].vertices[1]].faceCount++;
		mVertices[mFaces[i].vertices[2]].faceCount++;
	}

	//Accumulate
	start = 0;
	for(int i=0; i<mVertices.getCount(); i++)
	{
		mVertices[i].faceStart = start;
		start += mVertices[i].faceCount;
		mVertices[i].faceCount = 0;
	}	

	mVertexFaceList.redim(start);

	//Fill in
	for(int i=0; i<mFaces.getCount(); i++)
	{
		for(int j=0; j<3; j++)
		{
			Vertex& v = mVertices[mFaces[i].vertices[j]];
			mVertexFaceList[v.faceStart+v.faceCount] = i;
			v.faceCount++;
		}
	}	
}

void QiMesh::setOriginalEdgeLength()
{
	//Fill in
	for(int i=0; i<mEdges.getCount(); i++)
	{
		int v0 = mEdges[i].vertices[0];
		int v1 = mEdges[i].vertices[1];
		float l = length(mVertices[v0].point - mVertices[v1].point);
		mEdges[i].setOriginalLength(l);
	}	
}

#include "geometry/qiconvexhull.h"

void QiMesh::split(const QiPlane& plane, QiMesh* out0, QiMesh* out1, float eps)
{
	//Put vertices in two distinct sets
	QiArray<int> vertSide;

	for(int i=0; i<mVertices.getCount(); i++)
	{
		if (plane.isAbove(mVertices[i].point))
			vertSide.add(0);
		else
			vertSide.add(1);
	}
	
	//Find edges with verts in both sets
	QiArray<int> splitEdges;
	QiArray<QiVec3> splitPoints;
	for(int i=0; i<mEdges.getCount(); i++)
	{
		int v0 = mEdges[i].vertices[0];
		int v1 = mEdges[i].vertices[1];
		if (vertSide[v0] != vertSide[v1])
		{
			splitEdges.add(i);

			float t;
			QiLineSegment ls(mVertices[v0].point, mVertices[v1].point);
			QiIntersection(ls, plane, &t);
			QiVec3 p = ls.p + ls.v*t;
			splitPoints.add(p);
		}
	}
	
	QiArray<QiVec3> points0;
	QiArray<QiVec3> points1;
	for(int i=0; i<mVertices.getCount(); i++)
	{
		if (vertSide[i] == 0)
			points0.add(mVertices[i].point);
		else
			points1.add(mVertices[i].point);
	}
	for(int i=0; i<splitPoints.getCount(); i++)
	{
		points0.add(splitPoints[i]);
		points1.add(splitPoints[i]);
	}

	for(int i=0; i<points0.getCount(); i++)
	{
		float d = plane.getSignedDistance(points0[i]);
		if (d < eps)
		{
			QiVec3 n = plane.getNormal();
			points0[i] += n * (eps-d);
		}
	}
	
	for(int i=0; i<points1.getCount(); i++)
	{
		float d = -plane.getSignedDistance(points1[i]);
		if (d < eps)
		{
			QiVec3 n = plane.getNormal();
			points1[i] -= n * (eps-d);
		}
	}

	QiConvexHull hull0(points0);
	if (hull0.getFaceCount() > 3)
		out0->set(hull0.getPoints(), hull0.getPointCount(), hull0.getFaces(), hull0.getFaceCount());

	QiConvexHull hull1(points1);
	if (hull1.getFaceCount() > 3)
		out1->set(hull1.getPoints(), hull1.getPointCount(), hull1.getFaces(), hull1.getFaceCount());

	//Compute and associate a new vertex with each such edge
	
	/*
	//Init the output meshes
	//Iterate over all triangles
	//If all verts are inside -> include
	//If all verts are outside -> exclude
	//If one vert is outside -> clip
	//If two verts are outside -> clip	
	
	//Compute and triangulate the cap
	*/

	//or...
	//Use convex hull generator to compute new mesh
}


