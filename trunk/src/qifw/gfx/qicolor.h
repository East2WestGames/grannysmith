#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

// r, g, b = [0..1]
// h = [0..360], s = [0..1], v = [0..1]

void QiRGBToHSV(float r, float g, float b, float &h, float &s, float &v);
void QiHSVToRGB(float h, float s, float v, float& r, float& g, float& b);

class QiColor
{
public:
	inline QiColor() : r(0), g(0), b(0), a(0) {}
	inline QiColor(float r, float g, float b, float a) : r(r), g(g), b(b), a(a) {}

	inline void set(float r, float g, float b, float a) { this->r=r; this->g=g; this->b=b; this->a=a; }

	inline bool operator==(const QiColor& other) const { return r==other.r && g==other.g && b==other.b && a==other.a; }
	inline bool operator!=(const QiColor& other) const { return r!=other.r || g!=other.g || b!=other.b || a!=other.a; }

	inline QiColor operator*(float c) const { return QiColor(r, g, b, a*c); }
	inline void operator*=(float c) { a*=c; }

	inline QiColor operator*(const QiColor& c) const { return QiColor(r*c.r, g*c.g, b*c.b, a*c.a); }
	inline void operator*=(const QiColor& c) { r*=c.r; g*=c.g; b*=c.b; a*=c.a; }

	float r,g,b,a;
};
