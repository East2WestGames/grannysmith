#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "math/qivec3.h"

const int QI_GFX_UTIL_MAX_LISTS = 256;
class QiGfxUtil
{
public:
	static void getBillboardBase(QiVec3& x, QiVec3& y, QiVec3& z);

	static void enterOrthoMode();
	static void leaveOrthoMode();

	static void enterPixelMode();
	static void leavePixelMode();

	static void enterFrame(const class QiTransform3& frame);
	static void leaveFrame();

	static void newList(int list);
	static void endList();
	static void callList(int list);
	static void deleteList(int list);

	static void clear();
	
	static void drawQuad(const QiVec3& x = QiVec3::X, const QiVec3& y = QiVec3::Y);
	static void drawTexQuad(const QiVec3& x = QiVec3::X, const QiVec3& y = QiVec3::Y);
	static void drawFullScreenQuad();
	static void drawFullScreenTexQuad();
	
	static void scaleImage(const unsigned char* in, int inWidth, int inHeight, int inChannels, unsigned char* out, int outWidth, int outHeight, int outChannels);

protected:
	static int sLists[QI_GFX_UTIL_MAX_LISTS];
};


