#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

class QiPngEncoder
{
public:
	QiPngEncoder();
	QiPngEncoder(class QiOutputStream& outStream, int width, int height, bool hasAlpha);
	~QiPngEncoder();

	bool init(class QiOutputStream& outStream, int width, int height, bool hasAlpha);

	//Encode 8-bit RGB(A) image to binary output stream
	bool encode(unsigned char* pixels, bool flip=false);

protected:
	class QiPngEncoderImpl* mImpl;
};



