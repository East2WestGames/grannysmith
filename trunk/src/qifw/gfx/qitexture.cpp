/* Qi Framework. Copyright 2007-2012 Dengu AB */


#include "base/qimem.h"
#include "base/qimath.h"
#include "gfx/qitexture.h"
#include "gfx/qigl.h"

static void downSample(unsigned char* src, unsigned char* dst, int w, int h, int channels)
{
	for(int y=0; y<h; y+=2)
	{
		for(int x=0; x<w; x+=2)
		{
			//Offsets
			int offset0 = (y*w+x)*channels;
			int offset1 = (y*w+QiMin(x+1,w-1))*channels;
			int offset2 = (QiMin(y+1,h-1)*w+x)*channels;
			int offset3 = (QiMin(y+1,h-1)*w+QiMin(x+1,w-1))*channels;
			//Average pixels
			unsigned char rgba[4];
			for(int i=0; i<channels; i++)
			{
				int sum = (int)src[offset0+i] + (int)src[offset1+i] + (int)src[offset2+i] + (int)src[offset3+i];
				rgba[i] = (unsigned char)(sum/4);
			}
			//Write result
			for(int i=0; i<channels; i++)
				dst[((y/2)*QiMax(w/2, 1)+x/2)*channels+i] = rgba[i];
		}
	}
}

#if !defined(QI_IOS) && !defined(QI_ANDROID) 

int gTextureCount = 0;
int gTextureMem = 0;

static inline float W(float q)
{
	if (q > 1.0f)
		return 0.0f;
	float tmp = (1.0f-q*q);
	return 315.0f/(64.0f*QI_PI) * (tmp*tmp*tmp) * 0.5f;	
}

QiTexture::QiTexture() : 
mId(0), mWidth(0), mHeight(0), mFormat(0), mData(NULL), mMipMap(true), mMemSize(0), mRepeat(false)
{
}


QiTexture::QiTexture(int width, int height) :
mId(0), mWidth(0), mHeight(0), mFormat(0), mData(NULL), mMipMap(true), mMemSize(0), mRepeat(false)
{
	init(width, height);
}


QiTexture::QiTexture(int width, int height, int format) :
mId(0), mWidth(0), mHeight(0), mFormat(0), mData(NULL), mMipMap(true), mMemSize(0), mRepeat(false)
{
	init(width, height, format);
}


QiTexture::~QiTexture()
{
	free();
}


void QiTexture::init(int width, int height)
{
	init(width, height, GL_RGBA);
}


void QiTexture::init(int width, int height, int format, bool allocMem)
{
	free();
	mWidth = width;
	mHeight = height;
	mFormat = format;
	GLuint id;
	glGenTextures(1, &id);
	mId = id;
	if (allocMem)
	{
		if (mFormat == GL_RGBA)
			mData = QiAlloc(4 * width * height, "QiTexture::data");
		else if (mFormat == GL_RGB)
			mData = QiAlloc(3 * width * height, "QiTexture::data");
		else
			mData = QiAlloc(width * height, "QiTexture::data");
	}
	else
		mData = NULL;
}


void QiTexture::loadDefault()
{
	unsigned char* data = (unsigned char*)mData;
	for(int y=0; y<mHeight; y++)
	{
		for(int x=0; x<mWidth; x++)
		{
			int c = ((((y*8)/mHeight)+((x*8)/mWidth))%2) * 255;
			if (mFormat == GL_RGBA)
			{
				data[4*(y*mWidth+x) + 0] = c;
				data[4*(y*mWidth+x) + 1] = c;
				data[4*(y*mWidth+x) + 2] = c;
				data[4*(y*mWidth+x) + 3] = 255;
			}
			else if (mFormat == GL_RGB)
			{
				data[3*(y*mWidth+x) + 0] = c;
				data[3*(y*mWidth+x) + 1] = c;
				data[3*(y*mWidth+x) + 2] = c;
			}
			else if (mFormat == GL_ALPHA)
			{
				data[y*mWidth+x] = c;
			}
		}
	}
	upload();
}


void QiTexture::releaseMemory()
{
	if (mData)
	{
		QiFree(mData);
		mData = NULL;
	}
}


void QiTexture::loadBlob()
{
	unsigned char* data = (unsigned char*)mData;
	for(int y=0; y<mHeight; y++)
	{
		for(int x=0; x<mWidth; x++)
		{
			//Normalized dist
			float dx = (x-mWidth/2) / (float)(mWidth/2);
			float dy = (y-mHeight/2) / (float)(mHeight/2);
			float d = QiSqrt(dx*dx + dy*dy);
			//Compute weight
			int c = int(QiClamp(W(d), 0.0f, 1.0f) * 255);
			if (mFormat == GL_RGBA)
			{
				data[4*(y*mWidth+x) + 0] = c;
				data[4*(y*mWidth+x) + 1] = c;
				data[4*(y*mWidth+x) + 2] = c;
				data[4*(y*mWidth+x) + 3] = 255;
			}
			else if (mFormat == GL_RGB)
			{
				data[3*(y*mWidth+x) + 0] = c;
				data[3*(y*mWidth+x) + 1] = c;
				data[3*(y*mWidth+x) + 2] = c;
			}
			else if (mFormat == GL_ALPHA)
			{
				data[y*mWidth+x] = c;
			}
		}
	}
	upload();
}


void QiTexture::free()
{
	if (mId)
	{
		GLuint id = mId;
		glDeleteTextures(1, &id);
		mId = 0;
		gTextureCount--;
		gTextureMem -= mMemSize;
	}
	if (mData)
	{
		QiFree(mData);
		mData = NULL;
	}
	mWidth = 0;
	mHeight = 0;
}

/*
void QiTexture::upload()
{
	int type = GL_UNSIGNED_BYTE;
	glBindTexture(GL_TEXTURE_2D, mId);
	if (mFormat != GL_RGB || ((mWidth*3)&0x3) == 0)
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	else
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	if (mMipMap)
		glTexParameterf(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
	else
		glTexParameterf(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);
	glTexImage2D(GL_TEXTURE_2D, 0, mFormat, mWidth, mHeight, 0, mFormat, type, mData);
	mMemSize = mWidth*mHeight*(mFormat==GL_RGB?3:4);
	gTextureCount++;
	gTextureMem += mMemSize;
}

*/

void QiTexture::upload()
{
	glBindTexture(GL_TEXTURE_2D, mId);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	bool doMipMaps = (QiIsPowerOfTwo(mWidth) && QiIsPowerOfTwo(mHeight));

	bool scaleDown = false;
	if (scaleDown)
	{
		mWidth /= 2;
		mHeight /= 2;
		unsigned char* cdata = (unsigned char*)mData;
		for(int y=0; y<mHeight; y++)
		{
			for(int x=0; x<mWidth; x++)
			{
				if (mFormat == GL_RGB)
				{
					cdata[(y*mWidth+x)*3+0] = cdata[(y*mWidth*4+x*2)*3+0];
					cdata[(y*mWidth+x)*3+1] = cdata[(y*mWidth*4+x*2)*3+1];
					cdata[(y*mWidth+x)*3+2] = cdata[(y*mWidth*4+x*2)*3+2];
				}
				else
				{
					cdata[(y*mWidth+x)*4+0] = cdata[(y*mWidth*4+x*2)*4+0];
					cdata[(y*mWidth+x)*4+1] = cdata[(y*mWidth*4+x*2)*4+1];
					cdata[(y*mWidth+x)*4+2] = cdata[(y*mWidth*4+x*2)*4+2];
					cdata[(y*mWidth+x)*4+3] = cdata[(y*mWidth*4+x*2)*4+3];
				}
			}
		}
	}

	mMemSize = 0;
	if (mFormat == GL_RGB)
	{
		//Three channels
		unsigned char* cdata = (unsigned char*)mData;
		unsigned short* sdata = (unsigned short*)QiAlloc(mWidth*mHeight*4);
		int w = mWidth;
		int h = mHeight;
		int mip = 0;
		
		while(true)
		{
			for(int y=0; y<h; y++)
			{
				for(int x=0; x<w; x++)
				{
					int pix = y*w+x;
					unsigned short r = cdata[pix*3+0] >> 3;
					unsigned short g = cdata[pix*3+1] >> 2;
					unsigned short b = cdata[pix*3+2] >> 3;
					sdata[pix] = (r<<11) | (g<<5) | b;
				}
			}
			glTexImage2D(GL_TEXTURE_2D, mip, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_SHORT_5_6_5, sdata);
			mMemSize += w*h*2;
			if (!doMipMaps || (w == 1 && h == 1))
				break;
			downSample(cdata, cdata, w, h, 3);
			w = QiMax(w/2, 1);
			h = QiMax(h/2, 1);
			mip++;
		}
		QiFree(sdata);
	}
	else if (mFormat == GL_RGBA)
	{
		//Four channels
		unsigned char* cdata = (unsigned char*)mData;
		int w = mWidth;
		int h = mHeight;
		int mip = 0;
		while(true)
		{
			glTexImage2D(GL_TEXTURE_2D, mip, mFormat, w, h, 0, mFormat, GL_UNSIGNED_BYTE, cdata);
			mMemSize += w*h*4;
			if (!doMipMaps || (w == 1 && h == 1))
				break;
			downSample(cdata, cdata, w, h, 4);
			w = QiMax(w/2, 1);
			h = QiMax(h/2, 1);
			mip++;
		}
	}
	else
	{
		//One channel
		unsigned char* cdata = (unsigned char*)mData;
		int w = mWidth;
		int h = mHeight;
		int mip = 0;
		while(true)
		{
			glTexImage2D(GL_TEXTURE_2D, mip, mFormat, w, h, 0, mFormat, GL_UNSIGNED_BYTE, cdata);
			mMemSize += w*h*1;
			if (!doMipMaps || (w == 1 && h == 1))
				break;
			downSample(cdata, cdata, w, h, 1);
			w = QiMax(w/2, 1);
			h = QiMax(h/2, 1);
			mip++;
		}
	}
	
	gTextureCount++;
	gTextureMem += mMemSize;

	if (scaleDown)
	{
		mWidth *= 2;
		mHeight *= 2;
	}

	if (mRepeat)
	{
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	
	}
	else
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	if (doMipMaps)
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); 
	else
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
	mMipMap = doMipMaps;
}


void QiTexture::upload(unsigned char* data, int mip, int w, int h)
{
	glBindTexture(GL_TEXTURE_2D, mId);
	if (mFormat != GL_RGB || ((w*3)&0x3) == 0)
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	else
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexParameterf(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);
	glTexImage2D(GL_TEXTURE_2D, mip, mFormat, w, h, 0, mFormat, GL_UNSIGNED_BYTE, data);
//	glTexImage2D(GL_TEXTURE_2D, mip, GL_RGBA8, w, h, 0, GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV, data);

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	if (mMipMap)
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	else
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	if (mRepeat)
	{
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	
	}
	else
	{
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);	
	}
}


void QiTexture::enableRepeat(bool enabled)
{
	mRepeat = enabled;
	glBindTexture(GL_TEXTURE_2D, mId);
	if (mRepeat)
	{
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	
	}
	else
	{
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);	
	}
}


void QiTexture::upload(unsigned char* data, int mip, int w, int h, int offset)
{
	int type = GL_UNSIGNED_BYTE;
	glBindTexture(GL_TEXTURE_2D, mId);
	if (mFormat != GL_RGB || ((w*3)&0x3) == 0)
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	else
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexParameterf(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);
	glTexSubImage2D(GL_TEXTURE_2D, mip, 0, offset, w, h, mFormat, type, data);
//	glTexSubImage2D(GL_TEXTURE_2D, mip, 0, offset, w, h, GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV, data);
}


void QiTexture::use()
{
	glBindTexture(GL_TEXTURE_2D, mId);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	if (mMipMap)
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	else
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	if (mRepeat)
	{
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	
	}
	else
	{
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);	
	}
}


int QiTexture::detach()
{
	int ret = mId;
	mId = 0;
	return ret;
}


int QiTexture::getAllocatedBytes()
{
	return gTextureMem;
}



/*
void QiTexture::uploadDXT5(void* data, int w, int h, int bpp, bool hasAlpha)
{
    GLenum format;
    GLsizei size = w * h;
	if (hasAlpha)
		format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
	else
		format = GL_COMPRESSED_RGB_S3TC_DXT1_EXT;

	glBindTexture(GL_TEXTURE_2D, mId);
    glCompressedTexImage2D(GL_TEXTURE_2D, 0, format, w, h, 0, size, data);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	
}
*/



#else

#include "base/qimem.h"
#include "base/qimath.h"
#include "gfx/qitexture.h"
#include "gfx/qigl.h"

int gTextureCount = 0;
int gTextureMem = 0;


static inline float W(float q)
{
	if (q > 1.0f)
		return 0.0f;
	float tmp = (1.0f-q*q);
	return 315.0f/(64.0f*QI_PI) * (tmp*tmp*tmp) * 0.5f;	
}

QiTexture::QiTexture() : 
mId(0), mWidth(0), mHeight(0), mFormat(0), mData(NULL), mMipMap(false), mMemSize(0), mRepeat(false)
{
}


QiTexture::QiTexture(int width, int height) :
mId(0), mWidth(0), mHeight(0), mFormat(0), mData(NULL), mMipMap(false), mMemSize(0), mRepeat(false)
{
	init(width, height);
}


QiTexture::QiTexture(int width, int height, int format) :
mId(0), mWidth(0), mHeight(0), mFormat(0), mData(NULL), mMipMap(false), mMemSize(0), mRepeat(false)
{
	init(width, height, format);
}


QiTexture::~QiTexture()
{
	free();
}


void QiTexture::init(int width, int height)
{
	init(width, height, GL_RGBA);
}


void QiTexture::init(int width, int height, int format, bool allocMem)
{
	free();
	mWidth = width;
	mHeight = height;
	mFormat = format;
	GLuint id;
	glGenTextures(1, &id);
	mId = id;
	if (allocMem)
	{
		if (mFormat == GL_RGBA)
			mData = QiAlloc(4 * width * height, "QiTexture::data");
		else if (mFormat == GL_RGB)
			mData = QiAlloc(3 * width * height, "QiTexture::data");
		else
			mData = QiAlloc(width * height, "QiTexture::data");
	}
	else
		mData = NULL;
}


void QiTexture::loadDefault()
{
	unsigned char* data = (unsigned char*)mData;
	for(int y=0; y<mHeight; y++)
	{
		for(int x=0; x<mWidth; x++)
		{
			int c = ((((y*8)/mHeight)+((x*8)/mWidth))%2) * 255;
			if (mFormat == GL_RGBA)
			{
				data[4*(y*mWidth+x) + 0] = c;
				data[4*(y*mWidth+x) + 1] = c;
				data[4*(y*mWidth+x) + 2] = c;
				data[4*(y*mWidth+x) + 3] = 255;
			}
			else if (mFormat == GL_RGB)
			{
				data[3*(y*mWidth+x) + 0] = c;
				data[3*(y*mWidth+x) + 1] = c;
				data[3*(y*mWidth+x) + 2] = c;
			}
			else if (mFormat == GL_ALPHA)
			{
				data[y*mWidth+x] = c;
			}
		}
	}
	upload();
}


void QiTexture::loadBlob()
{
	unsigned char* data = (unsigned char*)mData;
	for(int y=0; y<mHeight; y++)
	{
		for(int x=0; x<mWidth; x++)
		{
			//Normalized dist
			float dx = (x-mWidth/2) / (float)(mWidth/2);
			float dy = (y-mHeight/2) / (float)(mHeight/2);
			float d = QiSqrt(dx*dx + dy*dy);
			//Compute weight
			int c = QiClamp(W(d), 0.0f, 1.0f) * 255;
			if (mFormat == GL_RGBA)
			{
				data[4*(y*mWidth+x) + 0] = c;
				data[4*(y*mWidth+x) + 1] = c;
				data[4*(y*mWidth+x) + 2] = c;
				data[4*(y*mWidth+x) + 3] = 255;
			}
			else if (mFormat == GL_RGB)
			{
				data[3*(y*mWidth+x) + 0] = c;
				data[3*(y*mWidth+x) + 1] = c;
				data[3*(y*mWidth+x) + 2] = c;
			}
			else if (mFormat == GL_ALPHA)
			{
				data[y*mWidth+x] = c;
			}
		}
	}
	upload();
}


void QiTexture::free()
{
	if (mId)
	{
		GLuint id = mId;
		glDeleteTextures(1, &id);
		mId = 0;
		gTextureCount--;
		gTextureMem -= mMemSize;
	}
	if (mData)
	{
		QiFree(mData);
		mData = NULL;
	}
	mWidth = 0;
	mHeight = 0;
	mMemSize = 0;
}


void QiTexture::releaseMemory()
{
	if (mData)
	{
		QiFree(mData);
		mData = NULL;
	}
}



void QiTexture::upload()
{
	int type = GL_UNSIGNED_BYTE;
	glBindTexture(GL_TEXTURE_2D, mId);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	int internalFormat = mFormat;

	bool doMipMaps = (QiIsPowerOfTwo(mWidth) && QiIsPowerOfTwo(mHeight));

	bool scaleDown = false;
	if (scaleDown)
	{
		mWidth /= 2;
		mHeight /= 2;
		unsigned char* cdata = (unsigned char*)mData;
		for(int y=0; y<mHeight; y++)
		{
			for(int x=0; x<mWidth; x++)
			{
				if (mFormat == GL_RGB)
				{
					cdata[(y*mWidth+x)*3+0] = cdata[(y*mWidth*4+x*2)*3+0];
					cdata[(y*mWidth+x)*3+1] = cdata[(y*mWidth*4+x*2)*3+1];
					cdata[(y*mWidth+x)*3+2] = cdata[(y*mWidth*4+x*2)*3+2];
				}
				else
				{
					cdata[(y*mWidth+x)*4+0] = cdata[(y*mWidth*4+x*2)*4+0];
					cdata[(y*mWidth+x)*4+1] = cdata[(y*mWidth*4+x*2)*4+1];
					cdata[(y*mWidth+x)*4+2] = cdata[(y*mWidth*4+x*2)*4+2];
					cdata[(y*mWidth+x)*4+3] = cdata[(y*mWidth*4+x*2)*4+3];
				}
			}
		}
	}

	mMemSize = 0;
	
	if (mFormat == GL_RGB)
	{
		//Three channels
		unsigned char* cdata = (unsigned char*)mData;
		unsigned short* sdata = (unsigned short*)QiAlloc(mWidth*mHeight*4);
		int w = mWidth;
		int h = mHeight;
		int mip = 0;
		while(true)
		{
			for(int y=0; y<h; y++)
			{
				for(int x=0; x<w; x++)
				{
					int pix = y*w+x;
					unsigned short r = cdata[pix*3+0] >> 3;
					unsigned short g = cdata[pix*3+1] >> 2;
					unsigned short b = cdata[pix*3+2] >> 3;
					sdata[pix] = (r<<11) | (g<<5) | b;
				}
			}
			glTexImage2D(GL_TEXTURE_2D, mip, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_SHORT_5_6_5, sdata);
			mMemSize += w*h*2;
			if (!doMipMaps || (w == 1 && h == 1))
				break;
			downSample(cdata, cdata, w, h, 3);
			w = QiMax(w/2, 1);
			h = QiMax(h/2, 1);
			mip++;
		}
		QiFree(sdata);
	}
	else if (mFormat == GL_RGBA)
	{
		//Four channels
		unsigned char* cdata = (unsigned char*)mData;
		int w = mWidth;
		int h = mHeight;
		int mip = 0;
		while(true)
		{
			glTexImage2D(GL_TEXTURE_2D, mip, mFormat, w, h, 0, mFormat, GL_UNSIGNED_BYTE, cdata);
			mMemSize += w*h*4;
			if (!doMipMaps || (w == 1 && h == 1))
				break;
			downSample(cdata, cdata, w, h, 4);
			w = QiMax(w/2, 1);
			h = QiMax(h/2, 1);
			mip++;
		}
	}
	else
	{
		//One channel
		unsigned char* cdata = (unsigned char*)mData;
		int w = mWidth;
		int h = mHeight;
		int mip = 0;
		while(true)
		{
			glTexImage2D(GL_TEXTURE_2D, mip, mFormat, w, h, 0, mFormat, GL_UNSIGNED_BYTE, cdata);
			mMemSize += w*h*1;
			if (!doMipMaps || (w == 1 && h == 1))
				break;
			downSample(cdata, cdata, w, h, 1);
			w = QiMax(w/2, 1);
			h = QiMax(h/2, 1);
			mip++;
		}
	}
	gTextureCount++;
	gTextureMem += mMemSize;

	if (scaleDown)
	{
		mWidth *= 2;
		mHeight *= 2;
	}

	if (mRepeat)
	{
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	
	}
	else
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	if (doMipMaps)
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); 
	else
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
}


void QiTexture::upload(unsigned char* data, int mip, int w, int h, int offset)
{
	int type = GL_UNSIGNED_BYTE;
	glBindTexture(GL_TEXTURE_2D, mId);
	if (mFormat != GL_RGB || ((w*3)&0x3) == 0)
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	else
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexSubImage2D(GL_TEXTURE_2D, mip, 0, offset, w, h, mFormat, type, data);
}


void QiTexture::enableRepeat(bool enabled)
{
	glBindTexture(GL_TEXTURE_2D, mId);
	mRepeat = enabled;
	if (mRepeat)
	{
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	
	}
	else
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}
}



struct PVRTexHeader
{
    uint32_t headerLength;
    uint32_t height;
    uint32_t width;
    uint32_t numMipmaps;
    uint32_t flags;
    uint32_t dataLength;
    uint32_t bpp;
    uint32_t bitmaskRed;
    uint32_t bitmaskGreen;
    uint32_t bitmaskBlue;
    uint32_t bitmaskAlpha;
    uint32_t pvrTag;
    uint32_t numSurfs;
};

void QiTexture::uploadPVRTC(void* data, int size)
{
	PVRTexHeader* header = (PVRTexHeader*)data;
	char* pixels = ((char*)data)+header->headerLength;
	
	mWidth = header->width;
	mHeight = header->height;
	mMipMap = (header->numMipmaps > 0);
	int bpp = header->bpp;
	bool hasAlpha = (header->bitmaskAlpha != 0);

    GLenum format;
    if(hasAlpha)
	{
        format = (bpp == 4) ? GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG : GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG;
    } else {
        format = (bpp == 4) ? GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG : GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG;
    }
	int blockSize = (4*4*bpp)/8;

    if(size < 32)
        size = 32;

	mMemSize = size;
	gTextureCount++;
	gTextureMem += mMemSize;

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glBindTexture(GL_TEXTURE_2D, mId);

	int w = mWidth;
	int h = mHeight;

    int m = 0;
    int widthBlocks, heightBlocks;    
    int dataOffset = 0;
    int dataLength = header->dataLength;
    while (dataOffset < dataLength)
    {
        if (bpp == 4)
        {
            blockSize = 4 * 4; // Pixel by pixel block size for 4bpp
            widthBlocks = w / 4;
            heightBlocks = h / 4;
            bpp = 4;
        }
        else
        {
            blockSize = 8 * 4; // Pixel by pixel block size for 2bpp
            widthBlocks = w / 8;
            heightBlocks = h / 4;
            bpp = 2;
        }
        
        // Clamp to minimum number of blocks
        if (widthBlocks < 2)
            widthBlocks = 2;
        if (heightBlocks < 2)
            heightBlocks = 2;
        
        int dataSize = widthBlocks * heightBlocks * ((blockSize  * bpp) / 8);
        
        glCompressedTexImage2D(GL_TEXTURE_2D, m++, format, w, h, 0, dataSize, pixels+dataOffset);

        dataOffset += dataSize;
        w = QiMax(w >> 1, 1);
        h = QiMax(h >> 1, 1);
    }

	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
}

void QiTexture::use()
{
	glBindTexture(GL_TEXTURE_2D, mId);
}


int QiTexture::detach()
{
	int ret = mId;
	mId = 0;
	return ret;
}


int QiTexture::getAllocatedBytes()
{
	return gTextureMem;
}

#endif


typedef struct {
  QiInt32 dwSize;
  QiInt32 dwFlags;
  QiInt32 dwFourCC;
  QiInt32 dwRGBBitCount;
  QiInt32 dwRBitMask;
  QiInt32 dwGBitMask;
  QiInt32 dwBBitMask;
  QiInt32 dwABitMask;
} QiDdsPixelFormat;

typedef struct {
  QiInt32 magic;
  QiInt32 dwSize;
  QiInt32 dwFlags;
  QiInt32 dwHeight;
  QiInt32 dwWidth;
  QiInt32 dwPitchOrLinearSize;
  QiInt32 dwDepth;
  QiInt32 dwMipMapCount;
  QiInt32 dwReserved1[11];
  QiDdsPixelFormat ddspf;
  QiInt32 dwCaps;
  QiInt32 dwCaps2;
  QiInt32 dwCaps3;
  QiInt32 dwCaps4;
  QiInt32 dwReserved2;
} QiDdsHeader;

void QiTexture::uploadDXT(void* data, int dataSize)
{
	QiDdsHeader* header = (QiDdsHeader*)data;
	mWidth = header->dwWidth;
	mHeight = header->dwHeight;
	int size = dataSize-128;

	char* pixels = ((char*)data)+(header->dwSize+4);

    GLenum format = 0;
	char* fmt = (char*)(&header->ddspf.dwFourCC);

	bool hasAlpha = ((header->ddspf.dwFlags & 1) == 1);
	
	mFormat = hasAlpha ? GL_RGBA : GL_RGB; 
	mMipMap = (header->dwMipMapCount > 0) | true;

	int blockSize = 16;

	if (strncmp(fmt, "DXT1", 4) == 0 && !hasAlpha)
	{
		format = 0x83F0; //DXT1 RGB
		blockSize = 8;
	}
	else if (strncmp(fmt, "DXT1", 4) == 0 && hasAlpha)
		format = 0x83F1; //DXT1 RGBA
	else if (strncmp(fmt, "DXT3", 4) == 0)
		format = 0x83F2; //DXT3 RGBA
	else if (strncmp(fmt, "DXT5", 4) == 0)
		format = 0x83F3; //DXT5 RGBA		

	mMemSize = size;
	gTextureCount++;
	gTextureMem += mMemSize;

	//TODO: Something is wrong with. Crashes with non power-of-two textures.
	glBindTexture(GL_TEXTURE_2D, mId);
	int w = mWidth;
	int h = mHeight;
	int offset = 0;
	for(int i=0; i<header->dwMipMapCount; i++)
	{
		QI_PRINT(i);
		int s = ((w+3)/4) * ((h+3)/4) * blockSize;
    	glCompressedTexImage2D(GL_TEXTURE_2D, i, format, w, h, 0, s, pixels+offset);
		w = QiMax(w/2, 1);
		h = QiMax(h/2, 1);
		offset += s;
	}
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	

	if (!QiIsPowerOfTwo(mHeight) || !QiIsPowerOfTwo(mWidth))
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}
}


void QiTexture::getTiledTexCoords(int rows, int cols, int tile, QiVec2 t[4])
{
	getTiledTexCoords(QiVec2(0.0f, 0.0f), QiVec2(1.0f, 1.0f), rows, cols, tile, t);
}


void QiTexture::getTiledTexCoords(const QiVec2& texMin, const QiVec2& texMax, int rows, int cols, int tile, QiVec2 t[4])
{
	int r = tile/cols;
	int c = tile%cols;
	float w = texMax.x-texMin.x;
	float h = texMax.y-texMin.y;
	t[0] = texMin + QiVec2(w * c / (float)cols, h * (r+1) / (float)rows);
	t[1] = texMin + QiVec2(w * (c+1) / (float)cols, h * (r+1) / (float)rows);
	t[2] = texMin + QiVec2(w * (c+1) / (float)cols, h * r / (float)rows);
	t[3] = texMin + QiVec2(w * c / (float)cols, h * r / (float)rows);
}


void QiTexture::markRetina()
{
    mWidth /= 2;
    mHeight /= 2;
}
