#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

class QiPngDecoder
{
public:
	QiPngDecoder();
	QiPngDecoder(class QiInputStream& inStream, size_t size);
	~QiPngDecoder();

	bool init(class QiInputStream& inStream, size_t size);
	int getWidth() const;
	int getHeight() const;
	bool hasAlpha() const;
	int getBitDepth() const;
	int getChannelCount() const;
	bool decode(unsigned char* pixels, bool flip=false);

protected:
	class QiPngDecoderImpl* mImpl;
};



