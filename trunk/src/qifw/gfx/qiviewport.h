#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiarray.h"
#include "math/qivec2.h"
#include "math/qivec3.h"
#include "math/qiquat.h"
#include "math/qimatrix4.h"


class QiViewport
{
	friend class QiRenderer;

public:
	
	enum Mode
	{
		MODE_PIXEL,
		MODE_PIXEL_FLIPPED,
		MODE_ORTHO,
		MODE_2D,
		MODE_3D
	};

	QiViewport();
	QiViewport(int x0, int y0, int x1, int y1);

	void init(int x0, int y0, int x1, int y1);
	void shutdown();

	void enableScissor(int x0, int y0, int x1, int y1);
	void disableScissor();
	inline bool isScissorEnabled() const { return mScissorEnabled; }
	inline void getScissor(int& x0, int& y0, int& x1, int& y1) const { x0 = mScissor[0]; y0 = mScissor[1]; x1 = x0+mScissor[2]; y1 = y0+mScissor[3]; }

	void setBounds(int x0, int y0, int x1, int y1);
	void getBounds(int& x0, int& y0, int& x1, int& y1) const;

	void push();
	void pop();

	void setIsoAmount(float amount) { mState.isoAmount = amount; updateProjection(); }
	void setIsoAngle(float angle) { mState.isoAngle = angle; updateProjection(); }

	void setModeOrtho();
	void setModePixel(float near=-1.0f, float far=1.0f);
	void setModePixelFlipped(float near=-1.0f, float far=1.0f);
	void setMode2D(float hFovInUnits, float near=-1.0f, float far=1.0f);
	void setMode3D(float hFovInDegrees, float near, float far);
	inline Mode getMode() { return mState.mode; }

	void setCameraPos(const QiVec3& point);
	QiVec3 getCameraPos() const;

	void setCameraRot(const QiQuat& rot);
	QiQuat getCameraRot() const;

	void lookAt(const QiVec3& point, const QiVec3& cameraPos, const QiVec3& cameraUp);

	//X, Y in pixel coords
	QiVec3 project(const QiVec3& point) const;
	QiVec3 unproject(const QiVec3& pixel) const;

	QiVec3 getPixelDirection(const QiVec2& pixel) const;

	inline int getWidth() const { return mState.bounds[2]-mState.bounds[0]; }
	inline int getHeight() const { return mState.bounds[3]-mState.bounds[1]; }
	inline void setFixedAspectRatio(float aspectRatio=0.0f) { mState.fixedAspect = aspectRatio; }
	inline float getFixedAspectRatio() const { return mState.fixedAspect; }
	inline float getAspectRatio() const { return mState.fixedAspect ? mState.fixedAspect : getWidth()/(float)getHeight(); }

	void translate(const QiVec3& v);
	void rotate(const QiQuat& q);
	void scale(const QiVec3& v);
	inline void scale(float s) { scale(QiVec3(s, s, s)); }
	void transform(const QiTransform3& f);
	void transform(const QiMatrix4& m);

	inline const QiMatrix4& getProjectionMatrix() const { return mState.projectionMatrix; }
	inline const QiMatrix4& getModelviewMatrix() const { return mState.modelviewMatrix; }

	void apply();

private:
	void updateProjection();
	void updateModelview();

	struct State
	{
		Mode mode;
		int bounds[4];

		float isoAmount;
		float isoAngle;
		float fixedAspect;
		float pFov;
		float pNear;
		float pFar;

		QiVec3 cameraPos;
		QiQuat cameraRot;

		QiMatrix4 projectionMatrix;
		QiMatrix4 modelviewMatrix;
	};

	State mState;
	QiArray<State> mStack;

public:
	void pick(int x, int y, float r);
	void pickClear();

	void pickPush(int id);
	void pickPop();

	bool pickPoint(int id, const QiVec3& p, float tolerance);
	bool pickLine(int id, const QiVec3& p0, const QiVec3& p1, float tolerance);
	bool pickTri(int id, const QiVec3& p0, const QiVec3& p1, const QiVec3& p2);
	bool pickRect(int id, const QiVec3& p0, const QiVec3& p1, const QiVec3& p2, const QiVec3& p3);
	bool pickRect(int id, const QiVec2& lower, const QiVec2& upper, float tolerance=0.0f);

	bool isPicked(int i0);
	bool isPicked(int i0, int i1);
	bool isPicked(int i0, int i1, int i2);
	bool isPicked(int i0, int i1, int i2, int i3);

private:
	bool isCurrentPicked() const;
	QiVec2 mPickPos;
	float mPickDepth;
	QiArrayInplace<int, 8> mPickStack;
	QiArrayInplace<int, 8> mBestPick;
	QiArrayInplace<int, 8> mPickResult;
	bool mScissorEnabled;
	int mScissor[4];
};

