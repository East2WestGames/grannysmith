#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

class QiJpegEncoder
{
public:
	QiJpegEncoder();
	QiJpegEncoder(class QiOutputStream& outStream, int width, int height, int quality=85);
	~QiJpegEncoder();

	bool init(class QiOutputStream& outStream, int width, int height, int quality=85);

	//Encode 8-bit RGB image to binary output stream
	bool encode(unsigned char* pixels, bool flip=false);
	
protected:
	class QiJpegEncoderImpl* mImpl;
};



