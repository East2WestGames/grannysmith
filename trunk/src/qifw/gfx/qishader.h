#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

class QiShader
{
public:
	QiShader();
	QiShader(const char* vertexShader, const char* fragmentShader);
	~QiShader();
	
	bool load(const char* combinedShader);
	bool load(const char* vertexShader, const char* fragmentShader);
	bool loadString(const char* vertexShader, const char* fragmentShader);
	bool loadMemory(const char* vertexShader, int vSize, const char* fragmentShader, int fSize);
	void unload();
	
	void cacheLocations();
	void use();
	static void useDefault();
	
	void setUniform(const char* name, int value);
	void setUniform(const char* name, float value);
	void setUniform(const char* name, const class QiVec3& value);
	void setUniform(const char* name, float x, float y, float z, float w);
	void setUniform2(const char* name, float* arr);
	void setUniform4(const char* name, float* arr);
	void setUniform4x4(const char* name, float* arr);
	
	void setUniform(int location, int value);
	void setUniform(int location, float value);
	void setUniform(int location, const class QiVec3& value);
	void setUniform(int location, float x, float y, float z, float w);
	void setUniform2(int location, float* arr);
	void setUniform4(int location, float* arr);
	void setUniform4x4(int location, float* arr);
	
	int getAttributeLocation(const char* name);

	int mMvpId;
	int mProjectionMatrixId;
	int mModelViewMatrixId;
	int mObjectMatrixId;
	int mColorId;
	int mTexScaleId;
	int mTexOffsetId;
	
	int aPosition;
	int aNormal;
	int aTexCoord;
	int aAlpha;
	
protected:
	int mVertexShader;
	int mFragmentShader;
	int mProgram;
};


