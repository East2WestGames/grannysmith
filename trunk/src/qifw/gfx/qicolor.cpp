/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "gfx/qicolor.h"
#include "qi_math.h"

void QiRGBToHSV(float r, float g, float b, float &h, float &s, float &v)
{
	float minVal, maxVal, delta;

	minVal = QiMin(r, g, b);
	maxVal = QiMax(r, g, b);
	v = maxVal;

	delta = maxVal - minVal;

	if (maxVal != 0)
	{
		s = delta / maxVal;
	}
	else 
	{
		// r = g = b = 0
		// s = 0, v is undefined, but we set it to 0
		h = 0;
		s = 0;
		v = 0;
		return;
	}

	if (r == maxVal)
		h = (g - b) / delta;		// between yellow & magenta
	else if (g == maxVal)
		h = 2 + (b - r) / delta;	// between cyan & yellow
	else
		h = 4 + (r - g) / delta;	// between magenta & cyan

	h *= 60;				// degrees
	if( h < 0 )
		h += 360;
}

void QiHSVToRGB(float h, float s, float v, float& r, float& g, float& b)
{
	int i;
	float f, p, q, t;

	if( s == 0 ) {
		// Gray
		r = g = b = v;
		return;
	}


	h /= 60;			// Sector 0 to 5
	i = (int)floor(h);
	f = h - i;			// Factorial part of h
	p = v * (1 - s);
	q = v * (1 - s * f);
	t = v * (1 - s * (1 - f) );

	switch( i ) {
		case 0:
			r = v;
			g = t;
			b = p;
			break;
		case 1:
			r = q;
			g = v;
			b = p;
			break;
		case 2:
			r = p;
			g = v;
			b = t;
			break;
		case 3:
			r = p;
			g = q;
			b = v;
			break;
		case 4:
			r = t;
			g = p;
			b = v;
			break;
		default:		// case 5:
			r = v;
			g = p;
			b = q;
			break;
	}
}
