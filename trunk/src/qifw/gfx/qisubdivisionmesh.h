#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "gfx/qimesh.h"

class QiSubdivisionMesh : public QiMesh
{
public:
	QiSubdivisionMesh();
	QiSubdivisionMesh(const QiMesh& controlMesh);
	QiSubdivisionMesh(const QiSubdivisionMesh& controlMesh);
	void setControlMesh(const QiMesh& controlMesh);
	void update();
	void wrinkle(float amount);

protected:
	const QiMesh* mControlMesh;
};



