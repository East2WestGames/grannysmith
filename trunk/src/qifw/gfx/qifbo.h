#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

const int QI_FBO_MAX_ATTACHMENTS = 4;
const int QI_FBO_MAX_STACK_DEPTH = 16;

class QiFbo
{
public:
	QiFbo();
	QiFbo(int width, int height);
	QiFbo(int width, int height, int format);
	QiFbo(int width, int height, int format, int attachmentCount);
	~QiFbo();

	inline int getWidth() const { return mWidth; }
	inline int getHeight() const { return mHeight; }

	void init(int width, int height);
	void init(int width, int height, int format, int attachmentCount);
	void update(int width, int height);
	void shutdown();
		
	void useAsTarget();
	void useAsViewport();
	void useAsTexture(int attachment = 0, int texUnit = 0);
	int getTextureId(int attachment = 0);
	
	static void useDefaultTarget();
	static void pushTarget();
	static void popTarget();

protected:
	unsigned int mFbo;
	unsigned int mDepth;
	unsigned int mColor[QI_FBO_MAX_ATTACHMENTS];
	int mWidth;
	int mHeight;
	int mAttachmentCount;

	static int sStack[QI_FBO_MAX_STACK_DEPTH];
	static int sStackBuffers[QI_FBO_MAX_STACK_DEPTH];
	static int sStackPtr;
};

