#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiarray.h"
#include "base/qihashtable.h"
#include "math/qivec3.h"

class QiParticleSurface
{
public:
	QiParticleSurface();
	QiParticleSurface(float particleSize, float gridCellSize);
	
	void init(float particleSize, float gridCellSize);

	void clear();
	void addParticle(const QiVec3& position);
	void addParticles(const QiVec3* positions, int count, int stride);

	void compute(float iso=1.0f);
	
	int getVertexCount() const;
	int getFaceCount() const;
	const QiVec3* getVertices() const;
	const QiVec3* getNormals() const;
	const int* getFaces() const;

	void debugDraw();

protected:
	static const int CELL_SAMPLES = 16;
	float mParticleSize;
	float mSampleSize;
	float mCellSize;

	class Cell
	{
	public:
		float mValues[(CELL_SAMPLES+1) * (CELL_SAMPLES+1) * (CELL_SAMPLES+1)];
		QiVec3 mLower, mUpper;
		int mX, mY, mZ;
		int mMinX, mMinY, mMinZ;
		int mMaxX, mMaxY, mMaxZ;
		int mNeighbors[3][3][3];
	};
	
	class CellPos
	{
	public:
		int x, y, z;
		int index;
		
		inline CellPos() {}
		inline CellPos(int x, int y, int z) : x(x), y(y), z(z) {}
		inline CellPos(int x, int y, int z, int index) : x(x), y(y), z(z), index(index) {}
		inline bool operator==(const CellPos& other) { return x==other.x && y==other.y && z==other.z; }
		inline QiHash getHash() const { return x+1000*y+1000000*z; }
	};

	void getCellIndexFromPoint(const QiVec3& point, int& x, int& y, int& z);
	int getCellIndex(int x, int y, int z);
	Cell& getCell(int x, int y, int z);
	int getCellIndex(const QiVec3& point);
	Cell* getCellIfExists(int x, int y, int z);
	Cell& getCellNeighbor(int cellId, int dx, int dy, int dz);
	void splat(Cell& cell, const QiVec3& point);
	
	QiArray<Cell> mCells;
	QiHashTable<CellPos> mCellHash;

	QiArray<QiVec3> mVertices;
	QiArray<QiVec3> mNormals;
	QiArray<int> mFaces;
};


