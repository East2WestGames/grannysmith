#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

const unsigned int MTX_RGBA8 = 1;

class QiMtxEncoder
{
public:
	QiMtxEncoder() : mStream(0), mWidth(0), mHeight(0) {}
	QiMtxEncoder(class QiOutputStream& out, int w, int h) { init(out, w, h); }

	void init(class QiOutputStream& out, int w, int h);
	bool encode(unsigned char* rgbaData);

private:
	class QiOutputStream* mStream;
	int mWidth;
	int mHeight;
};

class QiMtxDecoder
{
public:
	QiMtxDecoder() : mStream(0), mSize(0), mWidth(0), mHeight(0) {}
	QiMtxDecoder(class QiInputStream& in, int size) { init(in, size); }

	void init(class QiInputStream& in, int size);
	int getWidth() { return mWidth; }
	int getHeight() { return mHeight; }
	bool decode(unsigned char* rgbaData);

private:
	class QiInputStream* mStream;
	int mSize;
	int mWidth;
	int mHeight;
};



