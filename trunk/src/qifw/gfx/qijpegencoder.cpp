/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include <stdio.h>
#include "base/qiconfig.h"
#include "base/qimem.h"
#include "base/qistream.h"

//#ifdef QI_WIN32
extern "C"
{
//#endif
#include "gfx/jpeg/jpeglib.h"
//#ifdef QI_WIN32
}
//#endif

#include "base/qidebug.h"
#include "gfx/qijpegencoder.h"

static const int OUTPUT_BUF_SIZE = 8192;
static const int DEFAULT_QUALITY = 85;

typedef struct
{
	struct jpeg_destination_mgr pub;
	QiOutputStream * outStream;
	JOCTET * buffer;
	int error;
} my_destination_mgr;

typedef my_destination_mgr * my_dest_ptr;

static void init_destination (j_compress_ptr cinfo)
{
	my_dest_ptr dest = (my_dest_ptr) cinfo->dest;
	dest->buffer =  (JOCTET *)(*cinfo->mem->alloc_small)
					((j_common_ptr)cinfo, JPOOL_IMAGE,
					OUTPUT_BUF_SIZE * sizeof(JOCTET) );

	if( !dest->buffer ) {
		dest->error = TRUE;
	} else {
		dest->error = FALSE;
	}
	dest->pub.next_output_byte = dest->buffer;
	dest->pub.free_in_buffer   = OUTPUT_BUF_SIZE;
}

static boolean empty_output_buffer (j_compress_ptr cinfo)
{
	my_dest_ptr dest = (my_dest_ptr) cinfo->dest;
	if (!dest->error)
	{
		dest->outStream->writeBuffer((const char*)dest->buffer, (size_t)OUTPUT_BUF_SIZE);
		dest->pub.next_output_byte = dest->buffer;
		dest->pub.free_in_buffer   = OUTPUT_BUF_SIZE;
	} 
	else 
	{
		dest->error    = TRUE;
	}
	return TRUE;
}

static void term_destination (j_compress_ptr cinfo)
{
	my_dest_ptr dest = (my_dest_ptr) cinfo->dest;
	size_t datacount = OUTPUT_BUF_SIZE - dest->pub.free_in_buffer;
	if (!dest->error)
	{
		dest->outStream->writeBuffer((const char*)dest->buffer, (size_t)datacount);
	}
	else 
	{
		dest->error = TRUE;
	}
}

class QiJpegEncoderImpl
{
public:
	QiJpegEncoderImpl()
	{
		cinfo.err = jpeg_std_error(&jerr);
		jpeg_create_compress(&cinfo);
		cinfo.dest = (struct jpeg_destination_mgr *)
						(*cinfo.mem->alloc_small)
						((j_common_ptr)&cinfo,
						JPOOL_PERMANENT,
						sizeof(my_destination_mgr));
		my_dest_ptr dest = (my_dest_ptr)cinfo.dest;
		dest->pub.init_destination    = init_destination;
		dest->pub.empty_output_buffer = empty_output_buffer;
		dest->pub.term_destination = term_destination;
		cinfo.input_components = 3;
		cinfo.in_color_space = JCS_RGB;
		jpeg_set_defaults(&cinfo);
		jpeg_set_quality(&cinfo, DEFAULT_QUALITY, TRUE);	
	}
	
	~QiJpegEncoderImpl()
	{
		jpeg_destroy_compress(&cinfo);
	}
			
	struct jpeg_error_mgr jerr;
	struct jpeg_compress_struct cinfo;
	
};

QiJpegEncoder::QiJpegEncoder()
{
	mImpl = QI_NEW QiJpegEncoderImpl();
}

QiJpegEncoder::QiJpegEncoder(QiOutputStream& outStream, int width, int height, int quality)
{
	mImpl = QI_NEW QiJpegEncoderImpl();
	init(outStream, width, height, quality);
}

QiJpegEncoder::~QiJpegEncoder()
{
	QI_DELETE(mImpl);
}

bool QiJpegEncoder::init(QiOutputStream& outStream, int width, int height, int quality)
{
	my_dest_ptr dest = (my_dest_ptr)mImpl->cinfo.dest;
	dest->outStream  = &outStream;

	mImpl->cinfo.image_width = width;
	mImpl->cinfo.image_height = height;

	jpeg_set_quality(&mImpl->cinfo, quality, TRUE);	
	return true;
}

bool QiJpegEncoder::encode(unsigned char* pixels, bool flip)
{
	// Start compressor
	jpeg_start_compress(&mImpl->cinfo, TRUE);
	int row_stride = mImpl->cinfo.image_width * mImpl->cinfo.input_components;
	
	while (mImpl->cinfo.next_scanline < mImpl->cinfo.image_height)
	{
		unsigned char* tmp;
		if (flip)
			tmp = pixels+(mImpl->cinfo.image_height-mImpl->cinfo.next_scanline-1)*row_stride;
		else
			tmp = pixels+mImpl->cinfo.next_scanline*row_stride;
	    (void) jpeg_write_scanlines(&mImpl->cinfo, &tmp, 1);
	}

	// Complete compression and close output file
	jpeg_finish_compress(&mImpl->cinfo);
	return true;
}

