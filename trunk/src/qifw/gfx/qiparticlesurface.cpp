/* Qi Framework. Copyright 2007-2012 Dengu AB */

#ifdef QI_USE_OPENGL

#include "base/qimath.h"
#include "gfx/qiparticlesurface.h"
#include "qi_gfx.h"
#include "base/qitimer.h"

#define IDX(x, y, z) ((x) + (y)*(CELL_SAMPLES+1) + (z)*(CELL_SAMPLES+1)*(CELL_SAMPLES+1))

static const int PRECALC_SIZE=256;
float precalc[PRECALC_SIZE];

static inline float W(float q)
{	
	if (q > 1.0f)
		return 0.0f;
	float tmp = (1.0f-q*q);
	return 315.0f/(64.0f*QI_PI) * (tmp*tmp*tmp);	
}


QiParticleSurface::QiParticleSurface()
{
	clear();
}


QiParticleSurface::QiParticleSurface(float particleSize, float gridCellSize)
{
	init(particleSize, gridCellSize);
	clear();
}


void QiParticleSurface::init(float particleSize, float gridCellSize)
{
	mParticleSize = particleSize;
	mSampleSize = gridCellSize;
	mCellSize = gridCellSize*CELL_SAMPLES;
	
	for(int i=0; i<PRECALC_SIZE; i++)
	{
		float d2overh2 = i/(float)PRECALC_SIZE;
		float d2 = d2overh2 * (mParticleSize*mParticleSize);
		float d = QiSqrt(d2);
		float q = d/mParticleSize;
		precalc[i] = W(q);
	}
}


void QiParticleSurface::clear()
{
	mCells.clear();
	mCellHash.clear();
	mVertices.clear();
	mNormals.clear();
	mFaces.clear();
}

void QiParticleSurface::addParticle(const QiVec3& position)
{
	int cellId = getCellIndex(position);
	//splat(mCells[cellId], position); return;	
	QiVec3& lower = mCells[cellId].mLower;
	QiVec3& upper = mCells[cellId].mUpper;
	
	int x0=(position.x-mParticleSize < lower.x ? -1 : 0);
	int x1=(position.x+mParticleSize > upper.x ? 1 : 0);	
	int y0=(position.y-mParticleSize < lower.y ? -1 : 0);	
	int y1=(position.y+mParticleSize > upper.y ? 1 : 0);	
	int z0=(position.z-mParticleSize < lower.z ? -1 : 0);	
	int z1=(position.z+mParticleSize > upper.z ? 1 : 0);
	for(int x=x0; x<=x1; x++)
	{
		for(int y=y0; y<=y1; y++)
		{
			for(int z=z0; z<=z1; z++)
			{
				splat(getCellNeighbor(cellId, x, y, z), position);
			}
		}
	}
}


void QiParticleSurface::addParticles(const QiVec3* positions, int count, int stride)
{
	//NOT IMPLEMENTED
}


void QiParticleSurface::compute(float iso)
{
	mVertices.clear();
	mNormals.clear();
	mFaces.clear();
			
	QiMarchingCubes mc;
	QiTimer t;
	for(int i=0; i<mCells.getCount(); i++)
	{
		int offset = mVertices.getCount();

		Cell& c = mCells[i];

		//Copy edges
		Cell* c2;
		if ((c2 = getCellIfExists(c.mX+1, c.mY, c.mZ)))
		{
			for(int y=0; y<CELL_SAMPLES; y++)
			{
				for(int z=0; z<CELL_SAMPLES; z++)
				{
					c.mValues[IDX(CELL_SAMPLES, y, z)] = c2->mValues[IDX(0, y, z)];
				}
			}
		}
		if ((c2 = getCellIfExists(c.mX, c.mY+1, c.mZ)))
		{
			for(int x=0; x<CELL_SAMPLES; x++)
			{
				for(int z=0; z<CELL_SAMPLES; z++)
				{
					c.mValues[IDX(x, CELL_SAMPLES, z)] = c2->mValues[IDX(x, 0, z)];
				}
			}
		}
		if ((c2 = getCellIfExists(c.mX, c.mY, c.mZ+1)))
		{
			for(int x=0; x<CELL_SAMPLES; x++)
			{
				for(int y=0; y<CELL_SAMPLES; y++)
				{
					c.mValues[IDX(x, y, CELL_SAMPLES)] = c2->mValues[IDX(x, y, 0)];
				}
			}
		}
		if ((c2=getCellIfExists(c.mX+1, c.mY+1, c.mZ)))
		{
			for(int z=0; z<CELL_SAMPLES; z++)
				c.mValues[IDX(CELL_SAMPLES, CELL_SAMPLES, z)] = c2->mValues[IDX(0, 0, z)];
		}
		if ((c2=getCellIfExists(c.mX+1, c.mY, c.mZ+1)))
		{
			for(int y=0; y<CELL_SAMPLES; y++)
				c.mValues[IDX(CELL_SAMPLES, y, CELL_SAMPLES)] = c2->mValues[IDX(0, y, 0)];
		}
		if ((c2=getCellIfExists(c.mX, c.mY+1, c.mZ+1)))
		{
			for(int x=0; x<CELL_SAMPLES; x++)
				c.mValues[IDX(x, CELL_SAMPLES, CELL_SAMPLES)] = c2->mValues[IDX(x, 0, 0)];
		}	
		if ((c2=getCellIfExists(c.mX+1, c.mY+1, c.mZ+1)))
		{
			c.mValues[IDX(CELL_SAMPLES, CELL_SAMPLES, CELL_SAMPLES)] = c2->mValues[IDX(0, 0, 0)];
		}

		c.mMaxX+=2;
		c.mMaxY+=2;
		c.mMaxZ+=2;
		//c.mMinX=c.mMinY=c.mMinZ=0;
		//c.mMaxX=c.mMaxY=c.mMaxZ=CELL_SAMPLES+1;
		int tmp = c.mMinX + c.mMinY*(CELL_SAMPLES+1) + c.mMinZ*(CELL_SAMPLES+1)*(CELL_SAMPLES+1);
		QiVec3 off = c.mLower + QiVec3(c.mMinX*mSampleSize, c.mMinY*mSampleSize, c.mMinZ*mSampleSize);
		mc.init(c.mValues+tmp, c.mMaxX-c.mMinX, c.mMaxY-c.mMinY, c.mMaxZ-c.mMinZ);
		mc.setOutputScaling(off, QiVec3(mSampleSize, mSampleSize, mSampleSize));
		mc.setStride(4, (CELL_SAMPLES+1)*4, (CELL_SAMPLES+1)*(CELL_SAMPLES+1)*4);
		mc.compute(iso);
		const QiVec3* v = mc.getVertices();
		const QiVec3* n = mc.getNormals();	
		const int* f = mc.getFaces();
		for(int i=0; i<mc.getVertexCount(); i++)
		{
			mVertices.add(v[i]);
			mNormals.add(n[i]);
		}
		for(int i=0; i<mc.getFaceCount(); i++)
		{
			mFaces.add(f[i*3+0]+offset);
			mFaces.add(f[i*3+1]+offset);
			mFaces.add(f[i*3+2]+offset);
		}
	//	break;
	}
}


int QiParticleSurface::getVertexCount() const
{
	return mVertices.getCount();
}


int QiParticleSurface::getFaceCount() const
{
	return mFaces.getCount()/3;
}


const QiVec3* QiParticleSurface::getVertices() const
{
	return mVertices.getData();
}


const QiVec3* QiParticleSurface::getNormals() const
{
	return mNormals.getData();
}


const int* QiParticleSurface::getFaces() const
{
	return mFaces.getData();
}


inline int QiParticleSurface::getCellIndex(int x, int y, int z)
{
	CellPos* tmp = mCellHash.getPointer(CellPos(x, y, z));
	if (tmp)
		return tmp->index;
	
	int id = mCells.getCount();
	mCells.redim(id+1);
	Cell& c = mCells.last();
	memset(c.mValues, 0, sizeof(float) * (CELL_SAMPLES+1) * (CELL_SAMPLES+1) * (CELL_SAMPLES+1));
	c.mX = x;
	c.mY = y;
	c.mZ = z;
	c.mLower.set(x*mCellSize, y*mCellSize, z*mCellSize);
	c.mUpper.set((x+1)*mCellSize, (y+1)*mCellSize, (z+1)*mCellSize);
	c.mMinX = CELL_SAMPLES;
	c.mMinY = CELL_SAMPLES;
	c.mMinZ = CELL_SAMPLES;
	c.mMaxX = 0;
	c.mMaxY = 0;
	c.mMaxZ = 0;
	for(int i=0; i<3; i++)
		for(int j=0; j<3; j++)
			for(int k=0; k<3; k++)
				c.mNeighbors[i][j][k] = -2;

	mCellHash.put(CellPos(x, y, z, id));
	return id;
}


inline QiParticleSurface::Cell& QiParticleSurface::getCell(int x, int y, int z)
{
	return mCells[getCellIndex(x, y, z)];
}


inline int QiParticleSurface::getCellIndex(const QiVec3& point)
{
	int x,y,z;
	getCellIndexFromPoint(point, x, y, z);
	return getCellIndex(x, y, z);
}


inline void QiParticleSurface::getCellIndexFromPoint(const QiVec3& point, int& x, int& y, int& z)
{
	x = int(point.x/mCellSize);
	y = int(point.y/mCellSize);
	z = int(point.z/mCellSize);
	if (point.x < 0.0f)
		x--;
	if (point.y < 0.0f)
		y--;
	if (point.z < 0.0f)
		z--;
}


void QiParticleSurface::splat(Cell& cell, const QiVec3& point)
{
	QiVec3 p = point-cell.mLower;
	int x0 = QiClamp(int((p.x-mParticleSize)/mSampleSize), 0, CELL_SAMPLES-1);
	int x1 = QiClamp(int((p.x+mParticleSize)/mSampleSize), 0, CELL_SAMPLES-1);
	int y0 = QiClamp(int((p.y-mParticleSize)/mSampleSize), 0, CELL_SAMPLES-1);
	int y1 = QiClamp(int((p.y+mParticleSize)/mSampleSize), 0, CELL_SAMPLES-1);
	int z0 = QiClamp(int((p.z-mParticleSize)/mSampleSize), 0, CELL_SAMPLES-1);
	int z1 = QiClamp(int((p.z+mParticleSize)/mSampleSize), 0, CELL_SAMPLES-1);
	
	cell.mMinX = QiMin(cell.mMinX, x0);
	cell.mMaxX = QiMax(cell.mMaxX, x1);
	cell.mMinY = QiMin(cell.mMinY, y0);
	cell.mMaxY = QiMax(cell.mMaxY, y1);
	cell.mMinZ = QiMin(cell.mMinZ, z0);
	cell.mMaxZ = QiMax(cell.mMaxZ, z1);
	
	QiVec3 cp;
	float h2Inv = 1.0f/(mParticleSize*mParticleSize);
	for(int z=z0; z<=z1; z++)
	{
		cp.z = z*mSampleSize;
		for(int y=y0; y<=y1; y++)
		{
			cp.y = y*mSampleSize;
			int baseIndex = z*(CELL_SAMPLES+1)*(CELL_SAMPLES+1) + y*(CELL_SAMPLES+1);
			for(int x=x0; x<=x1; x++)
			{
				cp.x = x*mSampleSize;
				#if 0
				float d = cp.getDistanceTo(p);
				float q = d/mParticleSize;
				cell.mValues[baseIndex + x] += W(q);
				#else
				float d2overh2 = lengthSquared(cp-p)*h2Inv;
				int index = int(d2overh2*PRECALC_SIZE);
				if (index < PRECALC_SIZE)
					cell.mValues[baseIndex + x] += precalc[index];
				#endif
			}
		}
	}
}


void QiParticleSurface::debugDraw()
{
	for(int i=0; i<mCells.getCount(); i++)
	{
		Cell& c = mCells[i];
		glDisable(GL_LIGHTING);
		glColor4f(0.5f,0.0f,0.0f,1.0f);
		glBegin(GL_LINES);
		QiVec3 l = c.mLower;
		QiVec3 u = c.mUpper;
		c.mLower += QiVec3((float)c.mMinX, (float)c.mMinY, (float)c.mMinZ)*mSampleSize;
		c.mUpper -= QiVec3(float(CELL_SAMPLES-c.mMaxX), float(CELL_SAMPLES-c.mMaxY), float(CELL_SAMPLES-c.mMaxZ))*mSampleSize;
		glVertex3f(c.mLower.x, c.mLower.y, c.mLower.z);
		glVertex3f(c.mUpper.x, c.mLower.y, c.mLower.z);

		glVertex3f(c.mUpper.x, c.mLower.y, c.mLower.z);
		glVertex3f(c.mUpper.x, c.mUpper.y, c.mLower.z);

		glVertex3f(c.mUpper.x, c.mUpper.y, c.mLower.z);
		glVertex3f(c.mLower.x, c.mUpper.y, c.mLower.z);

		glVertex3f(c.mLower.x, c.mUpper.y, c.mLower.z);
		glVertex3f(c.mLower.x, c.mLower.y, c.mLower.z);


		glVertex3f(c.mLower.x, c.mLower.y, c.mUpper.z);
		glVertex3f(c.mUpper.x, c.mLower.y, c.mUpper.z);

		glVertex3f(c.mUpper.x, c.mLower.y, c.mUpper.z);
		glVertex3f(c.mUpper.x, c.mUpper.y, c.mUpper.z);

		glVertex3f(c.mUpper.x, c.mUpper.y, c.mUpper.z);
		glVertex3f(c.mLower.x, c.mUpper.y, c.mUpper.z);

		glVertex3f(c.mLower.x, c.mUpper.y, c.mUpper.z);
		glVertex3f(c.mLower.x, c.mLower.y, c.mUpper.z);


		glVertex3f(c.mLower.x, c.mLower.y, c.mLower.z);
		glVertex3f(c.mLower.x, c.mLower.y, c.mUpper.z);

		glVertex3f(c.mLower.x, c.mUpper.y, c.mLower.z);
		glVertex3f(c.mLower.x, c.mUpper.y, c.mUpper.z);

		glVertex3f(c.mUpper.x, c.mLower.y, c.mLower.z);
		glVertex3f(c.mUpper.x, c.mLower.y, c.mUpper.z);

		glVertex3f(c.mUpper.x, c.mUpper.y, c.mLower.z);
		glVertex3f(c.mUpper.x, c.mUpper.y, c.mUpper.z);
		glEnd();

		glBegin(GL_POINTS);
		for(int z=0; z<CELL_SAMPLES; z++)
		{
			for(int y=0; y<CELL_SAMPLES; y++)
			{
				int baseIndex = z*(CELL_SAMPLES+1)*(CELL_SAMPLES+1) + y*(CELL_SAMPLES+1);
				for(int x=0; x<CELL_SAMPLES; x++)
				{
					float v = c.mValues[baseIndex + x];
					glColor4f(1,1,1,v);
					glVertex3f(c.mLower.x+mSampleSize*x, c.mLower.y+mSampleSize*y, c.mLower.z+mSampleSize*z);					
				}
			}
		}
		glEnd();

		glEnable(GL_LIGHTING);
	}
}


inline QiParticleSurface::Cell* QiParticleSurface::getCellIfExists(int x, int y, int z)
{
	CellPos* tmp = mCellHash.getPointer(CellPos(x, y, z));
	if (tmp)
		return &mCells[tmp->index];
	return NULL;
}


QiParticleSurface::Cell& QiParticleSurface::getCellNeighbor(int cellId, int dx, int dy, int dz)
{
	if (dx==0 && dy==0 && dz==0)
		return mCells[cellId];
	else
	{
		int lookup = mCells[cellId].mNeighbors[dx+1][dy+1][dz+1];
		if (lookup == -2)
		{
		 	lookup = getCellIndex(mCells[cellId].mX+dx, mCells[cellId].mY+dy, mCells[cellId].mZ+dz);
			mCells[cellId].mNeighbors[dx+1][dy+1][dz+1] = lookup;
		}
		return mCells[lookup];
	}
}

#endif
