/* Qi Framework. Copyright 2007-2012 Dengu AB */

#pragma once

#include "base/qiarray.h"
#include "math/qivec2.h"
#include "math/qivec3.h"
#include "math/qimatrix4.h"
#include "gfx/qicolor.h"

class QiViewport;
class QiShader;
class QiTexture;

static const unsigned int QI_RENDER_TYPE_SIZE[] = {sizeof(float), sizeof(short int), sizeof(char), sizeof(unsigned short int), sizeof(unsigned char)};
static const int QI_RENDER_TEXTURE_COUNT = 1;

class QiVertexFormat
{
public:
	static const int MAX_FIELD_COUNT = 16;

	enum Type
	{
		FLOAT32 = 0,
		INT16 = 1,
		INT8 = 2,
		UINT16 = 3,
		UINT8 = 4
	};

	struct Field
	{
		QiString name;
		Type type;
		int dim;
		int offset;
	};

	inline QiVertexFormat() : mCount(0), mSize(0) {}
	inline void clear() { mCount=0; mSize=0; }

	void addField(const QiString& name, Type type, int dim);
	int getFieldIndex(const QiString& name) const;

	Field mFields[MAX_FIELD_COUNT];
	int mCount;
	int mSize;
};

class QiVertexBuffer
{
	friend class QiRenderer;
public:
	QiVertexBuffer();
	QiVertexBuffer(const QiVertexFormat& format, int size);
	~QiVertexBuffer();

	void init(const QiVertexFormat& format, int size);
	void redim(int size);
	void shutdown();
	void makeVbo();
	void makeDynamicVbo();

	void unlock();
	void lock();

	void clear();

	inline void vertex()
	{
		if (mVertexCount+1 > mMaxVertexCount)
			redim(mMaxVertexCount * 2 + 128);
		mBase = mData + mVertexSize*mVertexCount;
		mCurrent = mBase;
		mCurrentField = 0;
		mVertexCount++;
	}

	inline void add(float x, float y, float z, float w)
	{
		QI_ASSERT(mFormat->mFields[mCurrentField].dim == 4, "Dimension mismatch");
		addGeneric(x);
		addGeneric(y);
		addGeneric(z);
		addGeneric(w);
		advanceField();
	}

	inline void add(float x, float y, float z)
	{
		QI_ASSERT(mFormat->mFields[mCurrentField].dim == 3, "Dimension mismatch");
		addGeneric(x);
		addGeneric(y);
		addGeneric(z);
		advanceField();
	}

	inline void add(float x, float y)
	{
		QI_ASSERT(mFormat->mFields[mCurrentField].dim == 2, "Dimension mismatch");
		addGeneric(x);
		addGeneric(y);
		advanceField();
	}

	inline void add(float x)
	{
		QI_ASSERT(mFormat->mFields[mCurrentField].dim == 1, "Dimension mismatch");
		addGeneric(x);
		advanceField();
	}

	inline void add(const QiVec3& vec)
	{
		add(vec.x, vec.y, vec.z);
	}

	inline void add(const QiVec2& vec)
	{
		add(vec.x, vec.y);
	}

	template <class T>
	inline void addFast(T x, T y, T z, T w)
	{
		QI_ASSERT(QI_RENDER_TYPE_SIZE[mFormat->mFields[mCurrentField].type]*mFormat->mFields[mCurrentField].dim == 4*sizeof(T), "Type mismatch");
		((T*)mCurrent)[0] = x;
		((T*)mCurrent)[1] = y;
		((T*)mCurrent)[2] = z;
		((T*)mCurrent)[3] = w;
		advanceField();
	}

	template <class T>
	inline void addFast(T x, T y, T z)
	{
		QI_ASSERT(QI_RENDER_TYPE_SIZE[mFormat->mFields[mCurrentField].type]*mFormat->mFields[mCurrentField].dim == 3*sizeof(T), "Type mismatch");
		((T*)mCurrent)[0] = x;
		((T*)mCurrent)[1] = y;
		((T*)mCurrent)[2] = z;
		advanceField();
	}

	template <class T>
	inline void addFast(T x, T y)
	{
		QI_ASSERT(QI_RENDER_TYPE_SIZE[mFormat->mFields[mCurrentField].type]*mFormat->mFields[mCurrentField].dim == 2*sizeof(T), "Type mismatch");
		((T*)mCurrent)[0] = x;
		((T*)mCurrent)[1] = y;
		advanceField();
	}

	template <class T>
	inline void addFast(T x)
	{
		QI_ASSERT(QI_RENDER_TYPE_SIZE[mFormat->mFields[mCurrentField].type]*mFormat->mFields[mCurrentField].dim == 1*sizeof(T), "Type mismatch");
		((T*)mCurrent)[0] = x;
		advanceField();
	}
	
	inline int getCount() const { return mVertexCount; }

protected:
	inline void advanceField()
	{
		QI_ASSERT(mCurrentField < mFormat->mCount, "No more fields in Vertex");
		mCurrentField++;
		mCurrent = mBase + mFormat->mFields[mCurrentField].offset;
	}

	inline void addGeneric(float x)
	{
		switch(mFormat->mFields[mCurrentField].type)
		{
		case QiVertexFormat::FLOAT32:
			((float*)mCurrent)[0] = x;
			mCurrent += sizeof(float);
			break;
		case QiVertexFormat::INT16:
			((short int*)mCurrent)[0] = (short int)(QiClamp(x, -1.0f, 1.0f)*32767.0f);
			mCurrent += sizeof(short int);
			break;
		case QiVertexFormat::UINT16:
			QI_ASSERT(x >= 0.0f, "Cannot add negative float to unsigned");
			((unsigned short int*)mCurrent)[0] = (unsigned short int)(QiClamp(x, 0.0f, 1.0f)*65535.0f);
			mCurrent += sizeof(unsigned short int);
			break;
		case QiVertexFormat::INT8:
			((char*)mCurrent)[0] = (char)(QiClamp(x, -1.0f, 1.0f)*127.0f);
			mCurrent += sizeof(char);
			break;
		case QiVertexFormat::UINT8:
			QI_ASSERT(x >= 0.0f, "Cannot add negative float to unsigned");
			((unsigned char*)mCurrent)[0] = (unsigned char)(QiClamp(x, 0.0f, 1.0f)*255.0f);
			mCurrent += sizeof(unsigned char);
			break;
		}
	}

	const QiVertexFormat* mFormat;
	char* mData;
	char* mBase;
	char* mCurrent;
	int mVertexCount;
	int mMaxVertexCount;
	int mVertexSize;
	int mCurrentField;
	unsigned int mVboId;
};


class QiIndexBuffer
{
	friend class QiRenderer;
public:
	QiIndexBuffer();
	QiIndexBuffer(int size);
	~QiIndexBuffer();

	void init(int size);
	void redim(int size);
	void shutdown();
	void clear();
	void makeIbo();
	inline int getCount() const { return mCount; }

	void triangle(int i0, int i1, int i2);
	void quad(int i0, int i1, int i2, int i3);

protected:
	int mCount;
	unsigned short int* mData;
	unsigned int mIboId;
	int mSize;
};


class QiRenderState
{
public:
	enum BlendMode
	{
		NONE,
		BLEND,
		ADDITIVE
	};

	inline QiRenderState() : color(1,1,1,1), blendMode(NONE), 
	shader(NULL), depthMask(true), colorMask(true), depthTest(false), cullFace(false)
	{
		memset(texture, 0, sizeof(QiTexture*)*QI_RENDER_TEXTURE_COUNT);
		for(int i=0; i<QI_RENDER_TEXTURE_COUNT; i++)
			texScale[i].set(1.0f, 1.0f);
	}

	QiColor color;
	BlendMode blendMode;
	QiTexture* texture[QI_RENDER_TEXTURE_COUNT];
	QiShader* shader;
	QiVec2 texScale[QI_RENDER_TEXTURE_COUNT];
	QiVec2 texOffset[QI_RENDER_TEXTURE_COUNT];
	bool depthMask;
	bool colorMask;
	bool depthTest;
	bool cullFace;
};


class QiRenderer
{
public:
	QiRenderer();
	void init();
	void setViewport(const QiViewport& viewport);

	void resetState();

	void clear(bool color=true, bool depth=true);

	void setState(QiRenderState& renderState);

	void drawPoints(const QiVertexBuffer* vb, int count=-1, int offset=0);
	void drawLines(const QiVertexBuffer* vb, int count=-1, int offset=0);
	void drawLines(const QiVertexBuffer* vb, const QiIndexBuffer* ib, int count=-1, int offset=0);
	void drawTriangles(const QiVertexBuffer* vb, int count=-1, int offset=0);
	void drawTriangles(const QiVertexBuffer* vb, const QiIndexBuffer* ib, int count=-1, int offset=0);
	void drawTriangles(const QiMatrix4& transform, const QiVertexBuffer* vb, int count=-1, int offset=0);
	void drawTriangles(const QiMatrix4& transform, const QiVertexBuffer* vb, const QiIndexBuffer* ib, int count=-1, int offset=0);

	void drawRect(const QiVec2& lower, const QiVec2& upper);
	void drawRect(const QiVec2& lower, const QiVec2& upper, const QiVec2& texLower, const QiVec2& texUpper);
	void drawRectRot(const QiVec2& center, float size, float angle, const QiVec2& texLower, const QiVec2& texUpper);
	void drawLine(const QiVec3& p0, const QiVec3& p1);
	void drawPoint(const QiVec3& p);

	int getDrawCallCount() const { return mDrawCallCount; }
	int getStateChangeCount() const { return mStateChangeCount; }

protected:
	void preDraw(const QiVertexBuffer* vb);
	void postDraw(const QiVertexBuffer* vb);

	QiMatrix4 mProjectionMatrix;
	QiMatrix4 mModelViewMatrix;
	QiMatrix4 mObjectMatrix;

	QiVertexFormat mRectFormat;
	QiVertexBuffer mRectVb;
	QiIndexBuffer mRectIb;

	QiVertexFormat mLineFormat;
	QiVertexBuffer mLineVb;

	QiRenderState mCurrentState;

	int mDrawCallCount;
	int mStateChangeCount;
};

