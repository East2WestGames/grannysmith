#ifndef QI_HEADERS_GEOMETRY_H
#define QI_HEADERS_GEOMETRY_H

#include "geometry/qiconvexhull.h"
#include "geometry/qidistance.h"
#include "geometry/qiintersection.h"
#include "geometry/qilinesegment.h"
#include "geometry/qiplane.h"
#include "geometry/qiray.h"
#include "geometry/qisphere.h"
#include "geometry/qitriangle.h"
#include "geometry/qivertprimitives.h"

#endif
