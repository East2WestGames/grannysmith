/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "script/qiscript.h"
#include "base/qidebug.h"
#include "base/qiarray.h"
#include "base/qidebug.h"
#include "base/qistream.h"
#include "base/qistring.h"

#include "script/lua/lua.hpp"


int scriptProxy(lua_State* state)
{
	QiScriptFunc func = (QiScriptFunc)lua_topointer(state, lua_upvalueindex(1));
	QiScript* script = (QiScript*)lua_topointer(state, lua_upvalueindex(2));
	QiScriptArgs args(state);
	QiScriptRet ret(state);
	func(script, args, ret);
	return ret.getCount();
}


static void* luaQiFRealloc(void *, void *ptr, size_t osize, size_t nsize)
{
	if (nsize == 0)
	{
		QiFree(ptr);
		return NULL;
	}
	else if (osize == 0 || ptr == 0)
	{
		return QiAlloc(nsize);
	}
	else
		return QiRealloc(ptr, nsize);
}


static int luaPanic (lua_State *L)
{
	QI_ERROR("LUA PANIC: unprotected error in call to Lua API: " + lua_tostring(L, -1));
	return 0;
}



struct ScriptFunc
{
	QiString name;
	QiScriptFunc func;
};


struct QiScriptImpl
{
	lua_State* L;
	QiArray<ScriptFunc> funcs;
};


QiScript::QiScript() : userData(NULL)
{
	mImpl = QI_NEW QiScriptImpl();
	mImpl->L = NULL;
}


QiScript::~QiScript()
{
	shutdown();
	QI_DELETE(mImpl);
}


bool QiScript::init()
{
	//Shutdown previous state
	if (mImpl->L)
		shutdown();

	//Create new state
  	mImpl->L = lua_newstate(luaQiFRealloc, NULL);
	if (!mImpl->L)
		return false;

	//Register panic function
	lua_atpanic(mImpl->L, &luaPanic);

	//Init state
	luaL_openlibs(mImpl->L);

	return true;
}


void QiScript::shutdown()
{
	if (mImpl->L)
	{
		lua_close(mImpl->L);
		mImpl->L = NULL;
	}
}


void QiScript::registerFunction(const QiString& name, QiScriptFunc func)
{
    lua_pushlightuserdata(mImpl->L, (void*)func);
    lua_pushlightuserdata(mImpl->L, this);
    lua_pushcclosure(mImpl->L, scriptProxy, 2);
    lua_setglobal(mImpl->L, name);
}


void QiScript::clear()
{
	shutdown();
	init();
	for(int i=0; i<mImpl->funcs.getCount(); i++)
		lua_register(mImpl->L, mImpl->funcs[i].name.c_str(), (lua_CFunction)mImpl->funcs[i].func);
}


bool QiScript::load(const QiString& name, const void* buffer, int size)
{
	if (luaL_loadbuffer(mImpl->L, (const char*)buffer, size, name.c_str()) || lua_pcall(mImpl->L, 0, 0, 0))
	{
		//Error
		print(lua_tostring(mImpl->L, -1));
        lua_pop(mImpl->L, 1);
		return false;
	}
	return true;
}


bool QiScript::load(const QiString& name, QiInputStream& stream, int size)
{
	void* buffer = QiAlloc(size);
	stream.readBuffer(buffer, size);
	bool ret = load(name, buffer, size);
	QiFree(buffer);
	return ret;
}


bool QiScript::load(const QiString& name, const QiString& string)
{
	return load(name, (const void*)string.c_str(), string.getLength());
}


bool QiScript::execute(const QiString& code)
{
	if (luaL_dostring(mImpl->L, code.c_str()))
	{
		//Error
		print(lua_tostring(mImpl->L, -1));
        lua_pop(mImpl->L, 1);
		return false;
	}
	return true;
}


bool QiScript::call(const QiString& funcName)
{
	lua_getglobal(mImpl->L, funcName);
	if (!lua_isfunction(mImpl->L, -1))
	{
		lua_pop(mImpl->L, -1);
		return false;
	}
	return (lua_pcall(mImpl->L, 0, 0, 0) == 0);
}


bool QiScript::call(const QiString& funcName, const QiString& args)
{
	lua_getglobal(mImpl->L, funcName);
	if (!lua_isfunction(mImpl->L, -1))
	{
		lua_pop(mImpl->L, -1);
		return false;
	}
	lua_pushstring(mImpl->L, args.c_str());
	return (lua_pcall(mImpl->L, 1, 0, 0) == 0);
}


bool QiScript::hasFunction(const QiString& name)
{
	lua_getglobal(mImpl->L, name.c_str());
	bool ret = lua_isfunction(mImpl->L, -1) ? true : false;
	lua_pop(mImpl->L, 1);
	return ret;
}


QiString QiScript::getGlobalString(const QiString& name)
{
	QiString ret;
	lua_getglobal(mImpl->L, name.c_str());
	if (lua_isstring(mImpl->L, lua_gettop(mImpl->L)))
		ret = QiString(lua_tostring(mImpl->L, -1));
	lua_pop(mImpl->L, 1);
	return ret;
}


float QiScript::getGlobalFloat(const QiString& name)
{
	float ret = 0.0f;
	lua_getglobal(mImpl->L, name.c_str());
	if (lua_isstring(mImpl->L, lua_gettop(mImpl->L)))
		ret = (float)lua_tonumber(mImpl->L, -1);
	lua_pop(mImpl->L, 1);
	return ret;
}


int QiScript::getGlobalInt(const QiString& name)
{
	int ret = 0;
	lua_getglobal(mImpl->L, name.c_str());
	if (lua_isstring(mImpl->L, lua_gettop(mImpl->L)))
		ret = lua_tointeger(mImpl->L, -1);
	lua_pop(mImpl->L, 1);
	return ret;
}


bool QiScript::getGlobalBool(const QiString& name)
{
	bool ret = false;
	lua_getglobal(mImpl->L, name.c_str());
	if (lua_isstring(mImpl->L, lua_gettop(mImpl->L)))
		ret = lua_toboolean(mImpl->L, -1) ? true : false;
	lua_pop(mImpl->L, 1);
	return ret;
}


void* QiScript::getGlobalPointer(const QiString& name)
{
	return (void*)getGlobalInt(name);
}


void QiScript::setGlobalString(const QiString& name, const QiString& value)
{
	lua_pushstring(mImpl->L, value.c_str());
	lua_setglobal(mImpl->L, name.c_str());
}


void QiScript::setGlobalFloat(const QiString& name, float value)
{
	lua_pushnumber(mImpl->L, (lua_Number)value);
	lua_setglobal(mImpl->L, name.c_str());
}


void QiScript::setGlobalInt(const QiString& name, int value)
{
	lua_pushinteger(mImpl->L, value);
	lua_setglobal(mImpl->L, name.c_str());
}


void QiScript::setGlobalBool(const QiString& name, bool value)
{
	lua_pushboolean(mImpl->L, value ? 1 : 0);
	lua_setglobal(mImpl->L, name.c_str());
}


void QiScript::setGlobalPointer(const QiString& name, void* pointer)
{
	setGlobalInt(name, (int)pointer);
}



void QiScript::print(const char* str)
{
	QI_PRINT(str);
}



QiScriptArgs::QiScriptArgs(void* context) : mContext(context)
{
	mCount = lua_gettop((lua_State*)mContext);
}


QiString QiScriptArgs::asString(int arg) const
{
	if (arg < mCount)
		return QiString(lua_tostring((lua_State*)mContext, arg+1));
	else
		return QiString();
}

float QiScriptArgs::asFloat(int arg) const
{
	if (arg < mCount)
		return (float)lua_tonumber((lua_State*)mContext, arg+1);
	else
		return 0.0f;
}

int QiScriptArgs::asInt(int arg) const
{
	if (arg < mCount)
		return lua_tointeger((lua_State*)mContext, arg+1);
	else
		return 0;
}

bool QiScriptArgs::asBool(int arg) const
{
	if (arg < mCount)
		return lua_toboolean((lua_State*)mContext, arg+1) == 1 ? true : false;
	else
		return 0;
}

void* QiScriptArgs::asPointer(int arg) const
{
	if (arg < mCount)
		return (void*)lua_tointeger((lua_State*)mContext, arg+1);
	else
		return 0;
}


QiScriptRet::QiScriptRet(void* context) : mContext(context), mCount(0)
{
}


void QiScriptRet::addString(const QiString& string)
{
	lua_pushstring((lua_State*)mContext, string.c_str());
	mCount++;
}


void QiScriptRet::addFloat(float value)
{
	lua_pushnumber((lua_State*)mContext, (double)value);
	mCount++;
}


void QiScriptRet::addInt(int value)
{
	lua_pushinteger((lua_State*)mContext, value);
	mCount++;
}


void QiScriptRet::addBool(bool value)
{
	lua_pushboolean((lua_State*)mContext, value ? 1 : 0);
	mCount++;
}


void QiScriptRet::addPointer(void* pointer)
{
	lua_pushinteger((lua_State*)mContext, (int)pointer);
	mCount++;
}



