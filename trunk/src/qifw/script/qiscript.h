#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

class QiScriptArgs
{
public:
	QiScriptArgs(void* context);

	inline int getCount() const { return mCount; }
	class QiString asString(int arg) const;
	float asFloat(int arg) const;
	int asInt(int arg) const;
	bool asBool(int arg) const;
	void* asPointer(int arg) const;

protected:
	void* mContext;
	int mCount;
};


class QiScriptRet
{
public:
	QiScriptRet(void* context);

	inline int getCount() const { return mCount; }
	void addString(const QiString& string);
	void addFloat(float value);
	void addInt(int value);
	void addBool(bool value);
	void addPointer(void* pointer);
protected:
	void* mContext;
	int mCount;
};


typedef void (*QiScriptFunc)(class QiScript* script, const QiScriptArgs& args, QiScriptRet& ret);

class QiScript
{
public:
	QiScript();
	~QiScript();

	bool init();
	void shutdown();

	void registerFunction(const QiString& name, QiScriptFunc func);

	void clear();
	bool load(const QiString& name, const void* buffer, int size);
	bool load(const QiString& name, class QiInputStream& stream, int size);
	bool load(const QiString& name, const QiString& string);
	bool execute(const QiString& code);
	bool call(const QiString& funcName);
	bool call(const QiString& funcName, const QiString& args);

	bool hasFunction(const QiString& name);

	class QiString getGlobalString(const QiString& name);
	float getGlobalFloat(const QiString& name);
	int getGlobalInt(const QiString& name);
	bool getGlobalBool(const QiString& name);
	void* getGlobalPointer(const QiString& name);

	void setGlobalString(const QiString& name, const QiString& value);
	void setGlobalFloat(const QiString& name, float value);
	void setGlobalInt(const QiString& name, int value);
	void setGlobalBool(const QiString& name, bool value);
	void setGlobalPointer(const QiString& name, void* pointer);

	void* userData;

protected:
	struct QiScriptImpl* mImpl;
	void print(const char* str);
};


