#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "math/qivec3.h"

class QiSphere
{
public:
	inline QiSphere() : r(0.0f)
	{
	}

	inline QiSphere(float radius) : r(radius)
	{
	}

	inline QiSphere(const QiVec3& origin, float radius) : p(origin), r(radius)
	{
	}

	//Phi = latitude (north->south) from 0.0 to QI_PI
	//Theta = longitude (west->east) from 0.0 to 2.0*QI_PI
	//Y axis = north pole
	void convertToCartesian(float phi, float theta, QiVec3& point) const;

	//Phi = latitude (north->south) from 0.0 to QI_PI
	//Theta = longitude (west->east) from 0.0 to 2.0*QI_PI
	//Y axis = north pole
	void convertToSpherical(const QiVec3& dir, float& phi, float& theta) const;

	QiVec3 p;
	float r;
};


