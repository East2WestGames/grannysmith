#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiarray.h"
#include "math/qivec3.h"

class QiConvexHull
{
public:
	QiConvexHull();
	QiConvexHull(const QiArray<QiVec3>& points);
	QiConvexHull(const QiVec3* points, int pointCount);
	~QiConvexHull();

	void generate(const QiArray<QiVec3>& points);
	void generate(const QiVec3* points, int pointCount);

	const QiVec3* getPoints();
	int getPointCount();

	const int* getFaces();
	int getFaceCount();

private:
	QiArray<QiVec3> mOutputPoints;
	QiArray<int> mOutputIndices;
};




