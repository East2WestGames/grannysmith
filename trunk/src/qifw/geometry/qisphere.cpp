/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "geometry/qisphere.h"

void QiSphere::convertToCartesian(float phi, float theta, QiVec3& point) const
{
	//From http://www.math.montana.edu/frankw/ccp/multiworld/multipleIVP/spherical/body.htm
	point.x = r*QiSin(phi)*QiCos(theta);
	point.z = r*QiSin(phi)*QiSin(theta);
	point.y = r*QiCos(phi);
}

void QiSphere::convertToSpherical(const QiVec3& dir, float& phi, float& theta) const
{
	//From http://www.math.montana.edu/frankw/ccp/multiworld/multipleIVP/spherical/body.htm
	float S = QiSqrt(dir.x*dir.x + dir.z*dir.z);
	phi = QiACos(dir.y);
	theta = dir.x < 0 ? QI_PI - QiASin(dir.z/S) : QiASin(dir.z/S);
}

