#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "math/qivec3.h"

class QiRay;
class QiLineSegment;

class QiTriangle
{
public:
	QiVec3 p;
	QiVec3 e0, e1;
	
	inline QiTriangle()
	{
	}

	//TODO: Make sure no one assumes p0, p1, p2 here
	inline QiTriangle(const QiVec3& p, const QiVec3& e0, const QiVec3& e1) :
	p(p), e0(e0), e1(e1)
	{
	}

	inline QiVec3 getPoint(float t0, float t1) const
	{
		return p + e0*t0 + e1*t1;
	}
	
	inline QiVec3 getPoint(int i) const
	{
		switch(i)
		{
			case 0:
			return p;
			case 1:
			return p+e0;
			case 2:
			return p+e1;
			default:
			return QiVec3();
		}
	}
	
	inline QiVec3 getNormal() const
	{
		return normalize( cross(e0, e1) );
	}
	
	inline QiVec3 getNormalVector() const
	{
		return cross(e0, e1);
	}
	
	inline float getArea() const
	{
		//TODO check!
		return length( cross(e0, e1) ) * 0.5f;
	}
};




