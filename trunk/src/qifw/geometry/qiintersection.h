#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "geometry/qilinesegment.h"
#include "geometry/qiray.h"
#include "geometry/qitriangle.h"
#include "geometry/qisphere.h"
#include "geometry/qiplane.h"

bool QiIntersection(const QiLineSegment& lineSegment, const QiTriangle& triangle, float* t, float* e0, float* e1);
bool QiIntersection(const QiRay& ray, const QiTriangle& triangle, float* t, float* e0, float* e1);
bool QiIntersection(const QiRay& ray, const QiSphere& sphere, float* t, QiVec3* point);
bool QiIntersection(const QiLineSegment& lineSegment, const QiPlane& plane, float* t);


