/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "geometry/qidistance.h"

float QiDistanceSq(const QiVec3& point, const QiLineSegment& lineSegment, float* t)
{
	QiVec3 v = point - lineSegment.p;
	float length;
	QiVec3 d = normalize(lineSegment.v, length);
	if (length==0.0f)
		return lengthSquared(point - lineSegment.p);
	float tt = dot(d, v);
	if (tt <= 0.0f)
	{
		if (t)
			*t = 0.0f;
		return lengthSquared(point - lineSegment.p);
	}
	else if (tt >= length)
	{
		if (t)
			*t = 1.0f;
		return lengthSquared((lineSegment.p+lineSegment.v) - point);
	}
	else
	{
		if (t)
			*t = tt/length;
		return lengthSquared(v-d*dot(v,d));
	}
	
}


float QiDistanceSq(const QiVec3& point, const QiRay& ray, float* t)
{
	QI_ASSERT(false, "NOT IMPLEMENTED");
	return 0.0f;
}


float QiDistanceSq(const QiVec3& point, const QiTriangle& triangle, float* e0, float* e1)
{
	const QiVec3& m_origin = triangle.p;
	const QiVec3& m_edge0 = triangle.e0;
	const QiVec3& m_edge1 = triangle.e1;
	QiVec3 kDiff = m_origin - point;
	float fA00 = lengthSquared(m_edge0);
	float fA01 = dot(m_edge0, m_edge1);
	float fA11 = lengthSquared(m_edge1);
	float fB0 = dot(kDiff, m_edge0);
	float fB1 = dot(kDiff, m_edge1);
	float fC = lengthSquared(kDiff);
	float fDet = QiAbs(fA00*fA11-fA01*fA01);
	float fS = fA01*fB1-fA11*fB0;
	float fT = fA01*fB0-fA00*fB1;
	float fSqrDist;
	if ( fS + fT <= fDet )
	{
		if ( fS < 0.0f )
		{
			if ( fT < 0.0f )  // region 4
			{
				if ( fB0 < 0.0f )
				{
					fT = 0.0f;
					if ( -fB0 >= fA00 )
					{
						fS = 1.0f;
						fSqrDist = fA00+(2.0f)*fB0+fC;
					}
					else
					{
						fS = -fB0/fA00;
						fSqrDist = fB0*fS+fC;
					}
				}
				else
				{
					fS = 0.0f;
					if ( fB1 >= 0.0f )
					{
						fT = 0.0f;
						fSqrDist = fC;
					}
					else if ( -fB1 >= fA11 )
					{
						fT = 1.0f;
						fSqrDist = fA11+(2.0f)*fB1+fC;
					}
					else
					{
						fT = -fB1/fA11;
						fSqrDist = fB1*fT+fC;
					}
				}
			}
			else  // region 3
			{
				fS = 0.0f;
				if ( fB1 >= 0.0f )
				{
					fT = 0.0f;
					fSqrDist = fC;
				}
				else if ( -fB1 >= fA11 )
				{
					fT = 1.0f;
					fSqrDist = fA11+(2.0f)*fB1+fC;
				}
				else
				{
					fT = -fB1/fA11;
					fSqrDist = fB1*fT+fC;
				}
			}
		}
		else if ( fT < 0.0f )  // region 5
		{
			fT = 0.0f;
			if ( fB0 >= 0.0f )
			{
				fS = 0.0f;
				fSqrDist = fC;
			}
			else if ( -fB0 >= fA00 )
			{
				fS = 1.0f;
				fSqrDist = fA00+(2.0f)*fB0+fC;
			}
			else
			{
				fS = -fB0/fA00;
				fSqrDist = fB0*fS+fC;
			}
		}
		else  // region 0
		{
			// minimum at interior point
			float fInvDet = (1.0f)/fDet;
			fS *= fInvDet;
			fT *= fInvDet;
			fSqrDist = fS*(fA00*fS+fA01*fT+(2.0f)*fB0) +
				fT*(fA01*fS+fA11*fT+(2.0f)*fB1)+fC;
		}
	}
	else
	{
		float fTmp0, fTmp1, fNumer, fDenom;

		if ( fS < 0.0f )  // region 2
		{
			fTmp0 = fA01 + fB0;
			fTmp1 = fA11 + fB1;
			if ( fTmp1 > fTmp0 )
			{
				fNumer = fTmp1 - fTmp0;
				fDenom = fA00-2.0f*fA01+fA11;
				if ( fNumer >= fDenom )
				{
					fS = 1.0f;
					fT = 0.0f;
					fSqrDist = fA00+(2.0f)*fB0+fC;
				}
				else
				{
					fS = fNumer/fDenom;
					fT = 1.0f - fS;
					fSqrDist = fS*(fA00*fS+fA01*fT+2.0f*fB0) +
						fT*(fA01*fS+fA11*fT+(2.0f)*fB1)+fC;
				}
			}
			else
			{
				fS = 0.0f;
				if ( fTmp1 <= 0.0f )
				{
					fT = 1.0f;
					fSqrDist = fA11+(2.0f)*fB1+fC;
				}
				else if ( fB1 >= 0.0f )
				{
					fT = 0.0f;
					fSqrDist = fC;
				}
				else
				{
					fT = -fB1/fA11;
					fSqrDist = fB1*fT+fC;
				}
			}
		}
		else if ( fT < 0.0f )  // region 6
		{
			fTmp0 = fA01 + fB1;
			fTmp1 = fA00 + fB0;
			if ( fTmp1 > fTmp0 )
			{
				fNumer = fTmp1 - fTmp0;
				fDenom = fA00-(2.0f)*fA01+fA11;
				if ( fNumer >= fDenom )
				{
					fT = 1.0f;
					fS = 0.0f;
					fSqrDist = fA11+(2.0f)*fB1+fC;
				}
				else
				{
					fT = fNumer/fDenom;
					fS = 1.0f - fT;
					fSqrDist = fS*(fA00*fS+fA01*fT+(2.0f)*fB0) +
						fT*(fA01*fS+fA11*fT+(2.0f)*fB1)+fC;
				}
			}
			else
			{
				fT = 0.0f;
				if ( fTmp1 <= 0.0f )
				{
					fS = 1.0f;
					fSqrDist = fA00+(2.0f)*fB0+fC;
				}
				else if ( fB0 >= 0.0f )
				{
					fS = 0.0f;
					fSqrDist = fC;
				}
				else
				{
					fS = -fB0/fA00;
					fSqrDist = fB0*fS+fC;
				}
			}
		}
		else  // region 1
		{
			fNumer = fA11 + fB1 - fA01 - fB0;
			if ( fNumer <= 0.0f )
			{
				fS = 0.0f;
				fT = 1.0f;
				fSqrDist = fA11+(2.0f)*fB1+fC;
			}
			else
			{
				fDenom = fA00-2.0f*fA01+fA11;
				if ( fNumer >= fDenom )
				{
					fS = 1.0f;
					fT = 0.0f;
					fSqrDist = fA00+(2.0f)*fB0+fC;
				}
				else
				{
					fS = fNumer/fDenom;
					fT = 1.0f - fS;
					fSqrDist = fS*(fA00*fS+fA01*fT+(2.0f)*fB0) +
						fT*(fA01*fS+fA11*fT+(2.0f)*fB1)+fC;
				}
			}
		}
	}

	if ( e0 )
		*e0 = fS;

	if ( e1 )
		*e1 = fT;

	return QiAbs(fSqrDist);
}


float QiDistanceSq(const QiLineSegment& line0, const QiLineSegment& line1, float* t0, float* t1)
{
	QiVec3 kDiff = line0.p - line1.p;
	float fA00 = lengthSquared(line0.v);
	float fA01 = -dot(line0.v, line1.v);
	float fA11 = lengthSquared(line1.v);
	float fB0 = dot(kDiff, line0.v);
	float fC = lengthSquared(kDiff);
	float fDet = QiAbs(fA00*fA11-fA01*fA01);
	float fB1, fS, fT, fSqrDist, fTmp;

	if ( fDet >= QI_FLOAT_EPSILON )
	{
		// line segments are not parallel
		fB1 = -dot(kDiff, line1.v);
		fS = fA01*fB1-fA11*fB0;
		fT = fA01*fB0-fA00*fB1;

		if ( fS >= 0.0f )
		{
			if ( fS <= fDet )
			{
				if ( fT >= 0.0f )
				{
					if ( fT <= fDet )  // region 0 (interior)
					{
						// minimum at two interior points of 3D lines
						float fInvDet = (1.0f)/fDet;
						fS *= fInvDet;
						fT *= fInvDet;
						fSqrDist = fS*(fA00*fS+fA01*fT+(2.0f)*fB0) +
							fT*(fA01*fS+fA11*fT+(2.0f)*fB1)+fC;
					}
					else  // region 3 (side)
					{
						fT = 1.0f;
						fTmp = fA01+fB0;
						if ( fTmp >= 0.0f )
						{
							fS = 0.0f;
							fSqrDist = fA11+(2.0f)*fB1+fC;
						}
						else if ( -fTmp >= fA00 )
						{
							fS = 1.0f;
							fSqrDist = fA00+fA11+fC+(2.0f)*(fB1+fTmp);
						}
						else
						{
							fS = -fTmp/fA00;
							fSqrDist = fTmp*fS+fA11+(2.0f)*fB1+fC;
						}
					}
				}
				else  // region 7 (side)
				{
					fT = 0.0f;
					if ( fB0 >= 0.0f )
					{
						fS = 0.0f;
						fSqrDist = fC;
					}
					else if ( -fB0 >= fA00 )
					{
						fS = 1.0f;
						fSqrDist = fA00+(2.0f)*fB0+fC;
					}
					else
					{
						fS = -fB0/fA00;
						fSqrDist = fB0*fS+fC;
					}
				}
			}
			else
			{
				if ( fT >= 0.0f )
				{
					if ( fT <= fDet )  // region 1 (side)
					{
						fS = 1.0f;
						fTmp = fA01+fB1;
						if ( fTmp >= 0.0f )
						{
							fT = 0.0f;
							fSqrDist = fA00+(2.0f)*fB0+fC;
						}
						else if ( -fTmp >= fA11 )
						{
							fT = 1.0f;
							fSqrDist = fA00+fA11+fC+(2.0f)*(fB0+fTmp);
						}
						else
						{
							fT = -fTmp/fA11;
							fSqrDist = fTmp*fT+fA00+(2.0f)*fB0+fC;
						}
					}
					else  // region 2 (corner)
					{
						fTmp = fA01+fB0;
						if ( -fTmp <= fA00 )
						{
							fT = 1.0f;
							if ( fTmp >= 0.0f )
							{
								fS = 0.0f;
								fSqrDist = fA11+(2.0f)*fB1+fC;
							}
							else
							{
								fS = -fTmp/fA00;
								fSqrDist = fTmp*fS+fA11+(2.0f)*fB1+fC;
							}
						}
						else
						{
							fS = 1.0f;
							fTmp = fA01+fB1;
							if ( fTmp >= 0.0f )
							{
								fT = 0.0f;
								fSqrDist = fA00+(2.0f)*fB0+fC;
							}
							else if ( -fTmp >= fA11 )
							{
								fT = 1.0f;
								fSqrDist = fA00+fA11+fC+
									(2.0f)*(fB0+fTmp);
							}
							else
							{
								fT = -fTmp/fA11;
								fSqrDist = fTmp*fT+fA00+(2.0f)*fB0+fC;
							}
						}
					}
				}
				else  // region 8 (corner)
				{
					if ( -fB0 < fA00 )
					{
						fT = 0.0f;
						if ( fB0 >= 0.0f )
						{
							fS = 0.0f;
							fSqrDist = fC;
						}
						else
						{
							fS = -fB0/fA00;
							fSqrDist = fB0*fS+fC;
						}
					}
					else
					{
						fS = 1.0f;
						fTmp = fA01+fB1;
						if ( fTmp >= 0.0f )
						{
							fT = 0.0f;
							fSqrDist = fA00+(2.0f)*fB0+fC;
						}
						else if ( -fTmp >= fA11 )
						{
							fT = 1.0f;
							fSqrDist = fA00+fA11+fC+(2.0f)*(fB0+fTmp);
						}
						else
						{
							fT = -fTmp/fA11;
							fSqrDist = fTmp*fT+fA00+(2.0f)*fB0+fC;
						}
					}
				}
			}
		}
		else 
		{
			if ( fT >= 0.0f )
			{
				if ( fT <= fDet )  // region 5 (side)
				{
					fS = 0.0f;
					if ( fB1 >= 0.0f )
					{
						fT = 0.0f;
						fSqrDist = fC;
					}
					else if ( -fB1 >= fA11 )
					{
						fT = 1.0f;
						fSqrDist = fA11+(2.0f)*fB1+fC;
					}
					else
					{
						fT = -fB1/fA11;
						fSqrDist = fB1*fT+fC;
					}
				}
				else  // region 4 (corner)
				{
					fTmp = fA01+fB0;
					if ( fTmp < 0.0f )
					{
						fT = 1.0f;
						if ( -fTmp >= fA00 )
						{
							fS = 1.0f;
							fSqrDist = fA00+fA11+fC+(2.0f)*(fB1+fTmp);
						}
						else
						{
							fS = -fTmp/fA00;
							fSqrDist = fTmp*fS+fA11+(2.0f)*fB1+fC;
						}
					}
					else
					{
						fS = 0.0f;
						if ( fB1 >= 0.0f )
						{
							fT = 0.0f;
							fSqrDist = fC;
						}
						else if ( -fB1 >= fA11 )
						{
							fT = 1.0f;
							fSqrDist = fA11+(2.0f)*fB1+fC;
						}
						else
						{
							fT = -fB1/fA11;
							fSqrDist = fB1*fT+fC;
						}
					}
				}
			}
			else   // region 6 (corner)
			{
				if ( fB0 < 0.0f )
				{
					fT = 0.0f;
					if ( -fB0 >= fA00 )
					{
						fS = 1.0f;
						fSqrDist = fA00+(2.0f)*fB0+fC;
					}
					else
					{
						fS = -fB0/fA00;
						fSqrDist = fB0*fS+fC;
					}
				}
				else
				{
					fS = 0.0f;
					if ( fB1 >= 0.0f )
					{
						fT = 0.0f;
						fSqrDist = fC;
					}
					else if ( -fB1 >= fA11 )
					{
						fT = 1.0f;
						fSqrDist = fA11+(2.0f)*fB1+fC;
					}
					else
					{
						fT = -fB1/fA11;
						fSqrDist = fB1*fT+fC;
					}
				}
			}
		}
	}
	else
	{
		// line segments are parallel
		if ( fA01 > 0.0f )
		{
			// direction vectors form an obtuse angle
			if ( fB0 >= 0.0f )
			{
				fS = 0.0f;
				fT = 0.0f;
				fSqrDist = fC;
			}
			else if ( -fB0 <= fA00 )
			{
				fS = -fB0/fA00;
				fT = 0.0f;
				fSqrDist = fB0*fS+fC;
			}
			else
			{
				fB1 = -dot(kDiff, line1.v);
				fS = 1.0f;
				fTmp = fA00+fB0;
				if ( -fTmp >= fA01 )
				{
					fT = 1.0f;
					fSqrDist = fA00+fA11+fC+(2.0f)*(fA01+fB0+fB1);
				}
				else
				{
					fT = -fTmp/fA01;
					fSqrDist = fA00+(2.0f)*fB0+fC+fT*(fA11*fT+
						(2.0f)*(fA01+fB1));
				}
			}
		}
		else
		{
			// direction vectors form an acute angle
			if ( -fB0 >= fA00 )
			{
				fS = 1.0f;
				fT = 0.0f;
				fSqrDist = fA00+(2.0f)*fB0+fC;
			}
			else if ( fB0 <= 0.0f )
			{
				fS = -fB0/fA00;
				fT = 0.0f;
				fSqrDist = fB0*fS+fC;
			}
			else
			{
				fB1 = -dot(kDiff, line1.v);
				fS = 0.0f;
				if ( fB0 >= -fA01 )
				{
					fT = 1.0f;
					fSqrDist = fA11+(2.0f)*fB1+fC;
				}
				else
				{
					fT = -fB0/fA01;
					fSqrDist = fC+fT*((2.0f)*fB1+fA11*fT);
				}
			}
		}
	}

	if ( t0 )
		*t0 = fS;

	if ( t1 )
		*t1 = fT;

	return QiAbs(fSqrDist);
}


float QiDistanceSq(const QiLineSegment& line, const QiRay& ray, float* tl, float* tr)
{
	QI_ASSERT(false, "NOT IMPLEMENTED");
	return 0.0f;
}


float QiDistanceSq(const QiLineSegment& lineSegment, const QiTriangle& triangle, float* t, float* e0, float* e1)
{
	const QiVec3& m_origin = triangle.p;
	const QiVec3& m_edge0 = triangle.e0;
	const QiVec3& m_edge1 = triangle.e1;
	const QiVec3& rkSegOrigin = lineSegment.p;
	const QiVec3& rkSegDirection = lineSegment.v;
	QiVec3 kDiff = m_origin - rkSegOrigin;
	float fA00 = lengthSquared(rkSegDirection);
	float fA01 = -dot(rkSegDirection, m_edge0);
	float fA02 = -dot(rkSegDirection, m_edge1);
	float fA11 = lengthSquared(m_edge0);
	float fA12 = dot(m_edge0, m_edge1);
	float fA22 = dot(m_edge1, m_edge1);
	float fB0  = -dot(kDiff, rkSegDirection);
	float fB1  = dot(kDiff, m_edge0);
	float fB2  = dot(kDiff, m_edge1);

	//Point kPt;
	float fSqrDist, fSqrDist0, fS, fT, fR0, fS0, fT0;
	float fR = 0.0f;

	// Set up for a relative error test on the angle between ray direction
	// and triangle normal to determine parallel/nonparallel status.
	QiVec3 kN = cross(m_edge0, m_edge1);
	float fNSqrLen = lengthSquared(kN);
	float fDot = dot(rkSegDirection, kN);

	bool bAccurate;
	bool bNotParallel = (fDot*fDot >= 0.0004f*fA00*fNSqrLen);

	if ( bNotParallel )
	{
		float fCof00 = fA11*fA22-fA12*fA12;
		float fCof01 = fA02*fA12-fA01*fA22;
		float fCof02 = fA01*fA12-fA02*fA11;
		float fCof11 = fA00*fA22-fA02*fA02;
		float fCof12 = fA02*fA01-fA00*fA12;
		float fCof22 = fA00*fA11-fA01*fA01;
		float fInvDet = (1.0f)/(fA00*fCof00+fA01*fCof01+fA02*fCof02);
		float fRhs0 = -fB0*fInvDet;
		float fRhs1 = -fB1*fInvDet;
		float fRhs2 = -fB2*fInvDet;

		fR = fCof00*fRhs0+fCof01*fRhs1+fCof02*fRhs2;
		fS = fCof01*fRhs0+fCof11*fRhs1+fCof12*fRhs2;
		fT = fCof02*fRhs0+fCof12*fRhs1+fCof22*fRhs2;

		QiVec3 rkError = kDiff + m_edge0*fS + m_edge1*fT - rkSegDirection*fR;

		bAccurate = (lengthSquared(rkError) <= 0.0001f*fNSqrLen);

		if ( bAccurate )
		{
			if ( fR < 0.0f )
			{
				if ( fS+fT <= 1.0f )
				{
					if ( fS < 0.0f )
					{
						if ( fT < 0.0f )  // region 4m
						{
							// min on face s=0 or t=0 or r=0
							fSqrDist = QiDistanceSq(lineSegment, QiLineSegment(m_origin, m_edge1),&fR,&fT);
							fS = 0.0f;
							fSqrDist0 = QiDistanceSq(lineSegment, QiLineSegment(m_origin, m_edge0),&fR0,&fS0);
							fT0 = 0.0f;
							if ( fSqrDist0 < fSqrDist )
							{
								fSqrDist = fSqrDist0;
								fR = fR0;
								fS = fS0;
								fT = fT0;
							}
							fSqrDist0 = QiDistanceSq(rkSegOrigin, triangle, &fS0,&fT0);
							fR0 = 0.0f;
							if ( fSqrDist0 < fSqrDist )
							{
								fSqrDist = fSqrDist0;
								fR = fR0;
								fS = fS0;
								fT = fT0;
							}
						}
						else  // region 3m
						{
							// min on face s=0 or r=0
							fSqrDist = QiDistanceSq(lineSegment, QiLineSegment(m_origin, m_edge1),&fR,&fT);
							fS = 0.0f;
							fSqrDist0 = QiDistanceSq(rkSegOrigin, triangle, &fS0, &fT0);
							fR0 = 0.0f;
							if ( fSqrDist0 < fSqrDist )
							{
								fSqrDist = fSqrDist0;
								fR = fR0;
								fS = fS0;
								fT = fT0;
							}
						}
					}
					else if ( fT < 0.0f )  // region 5m
					{
						// min on face t=0 or r=0
						fSqrDist = QiDistanceSq(lineSegment, QiLineSegment(m_origin, m_edge0), &fR, &fS);
						fT = 0.0f;
						fSqrDist0 = QiDistanceSq(rkSegOrigin, triangle, &fS0, &fT0);
						fR0 = 0.0f;
						if ( fSqrDist0 < fSqrDist )
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
					}
					else  // region 0m
					{
						// min on face r=0
						fSqrDist = QiDistanceSq(rkSegOrigin, triangle, &fS, &fT);
						fR = 0.0f;
					}
				}
				else
				{
					if ( fS < 0.0f )  // region 2m
					{
						// min on face s=0 or s+t=1 or r=0
						fSqrDist = QiDistanceSq(lineSegment, QiLineSegment(m_origin, m_edge1),&fR,&fT);
						fS = 0.0f;
						fSqrDist0 = QiDistanceSq(lineSegment, QiLineSegment(m_origin, m_edge0),&fR0,&fT0);
						fS0 = 1.0f-fT0;
						if ( fSqrDist0 < fSqrDist )
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
						fSqrDist0 = QiDistanceSq(rkSegOrigin, triangle,&fS0,&fT0);
						fR0 = 0.0f;
						if ( fSqrDist0 < fSqrDist )
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
					}
					else if ( fT < 0.0f )  // region 6m
					{
						// min on face t=0 or s+t=1 or r=0
						fSqrDist = QiDistanceSq(lineSegment, QiLineSegment(m_origin, m_edge0), &fR, &fS);
						fT = 0.0f;
						fSqrDist0 = QiDistanceSq(lineSegment, QiLineSegment(m_origin+m_edge0, m_edge1-m_edge0), &fR0, &fT0);
						fS0 = 1.0f-fT0;
						if ( fSqrDist0 < fSqrDist )
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
						fSqrDist0 = QiDistanceSq(rkSegOrigin, triangle, &fS0, &fT0);
						fR0 = 0.0f;
						if ( fSqrDist0 < fSqrDist )
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
					}
					else  // region 1m
					{
						// min on face s+t=1 or r=0
						fSqrDist = QiDistanceSq(lineSegment, QiLineSegment(m_origin+m_edge0, m_edge1-m_edge0),&fR,&fT);
						fS = 1.0f-fT;
						fSqrDist0 = QiDistanceSq(rkSegOrigin, triangle, &fS0, &fT0);
						fR0 = 0.0f;
						if ( fSqrDist0 < fSqrDist )
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
					}
				}
			}
			else if ( fR <= 1.0f )
			{
				if ( fS+fT <= 1.0f )
				{
					if ( fS < 0.0f )
					{
						if ( fT < 0.0f )  // region 4
						{
							// min on face s=0 or t=0
							fSqrDist = QiDistanceSq(lineSegment, QiLineSegment(m_origin, m_edge1), &fR,&fT);
							fS = 0.0f;
							fSqrDist0 = QiDistanceSq(lineSegment, QiLineSegment(m_origin, m_edge0), &fR0, &fS0);
							fT0 = 0.0f;
							if ( fSqrDist0 < fSqrDist )
							{
								fSqrDist = fSqrDist0;
								fR = fR0;
								fS = fS0;
								fT = fT0;
							}
						}
						else  // region 3
						{
							// min on face s=0
							fSqrDist = QiDistanceSq(lineSegment, QiLineSegment(m_origin, m_edge1), &fR, &fT);
							fS = 0.0f;
						}
					}
					else if ( fT < 0.0f )  // region 5
					{
						// min on face t=0
						fSqrDist = QiDistanceSq(lineSegment, QiLineSegment(m_origin, m_edge0), &fR, &fS);
						fT = 0.0f;
					}
					else  // region 0
					{
						// global minimum is interior, done
						fSqrDist = 0.0f;
					}
				}
				else
				{
					if ( fS < 0.0f )  // region 2
					{
						// min on face s=0 or s+t=1
						fSqrDist = QiDistanceSq(lineSegment, QiLineSegment(m_origin, m_edge1), &fR, &fT);
						fS = 0.0f;
						fSqrDist0 = QiDistanceSq(lineSegment, QiLineSegment(m_origin+m_edge0, m_edge1-m_edge0), &fR0, &fT0);
						fS0 = 1.0f-fT0;
						if ( fSqrDist0 < fSqrDist )
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
					}
					else if ( fT < 0.0f )  // region 6
					{
						// min on face t=0 or s+t=1
						fSqrDist = QiDistanceSq(lineSegment, QiLineSegment(m_origin, m_edge0), &fR, &fS);
						fT = 0.0f;
						fSqrDist0 = QiDistanceSq(lineSegment, QiLineSegment(m_origin+m_edge0, m_edge1-m_edge0), &fR0, &fT0);
						fS0 = 1.0f-fT0;
						if ( fSqrDist0 < fSqrDist )
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
					}
					else  // region 1
					{
						// min on face s+t=1
						fSqrDist = QiDistanceSq(lineSegment, QiLineSegment(m_origin+m_edge0, m_edge1-m_edge0), &fR, &fT);
						fS = 1.0f-fT;
					}
				}
			}
			else  // fR > 1
			{
				if ( fS+fT <= 1.0f )
				{
					if ( fS < 0.0f )
					{
						if ( fT < 0.0f )  // region 4p
						{
							// min on face s=0 or t=0 or r=1
							fSqrDist = QiDistanceSq(lineSegment, QiLineSegment(m_origin, m_edge1), &fR, &fT);
							fS = 0.0f;
							fSqrDist0 = QiDistanceSq(lineSegment, QiLineSegment(m_origin, m_edge0), &fR0, &fS0);
							fT0 = 0.0f;
							if ( fSqrDist0 < fSqrDist )
							{
								fSqrDist = fSqrDist0;
								fR = fR0;
								fS = fS0;
								fT = fT0;
							}
							fSqrDist0 = QiDistanceSq(rkSegOrigin+rkSegDirection, triangle, &fS0, &fT0);
							fR0 = 1.0f;
							if ( fSqrDist0 < fSqrDist )
							{
								fSqrDist = fSqrDist0;
								fR = fR0;
								fS = fS0;
								fT = fT0;
							}
						}
						else  // region 3p
						{
							// min on face s=0 or r=1
							fSqrDist = QiDistanceSq(lineSegment, QiLineSegment(m_origin, m_edge1), &fR, &fT);
							fS = 0.0f;
							fSqrDist0 = QiDistanceSq(rkSegOrigin+rkSegDirection, triangle, &fS0,&fT0);
							fR0 = 1.0f;
							if ( fSqrDist0 < fSqrDist )
							{
								fSqrDist = fSqrDist0;
								fR = fR0;
								fS = fS0;
								fT = fT0;
							}
						}
					}
					else if ( fT < 0.0f )  // region 5p
					{
						// min on face t=0 or r=1
						fSqrDist = QiDistanceSq(lineSegment, QiLineSegment(m_origin, m_edge0), &fR, &fS);
						fT = 0.0f;
						fSqrDist0 = QiDistanceSq(rkSegOrigin+rkSegDirection, triangle, &fS0, &fT0);
						fR0 = 1.0f;
						if ( fSqrDist0 < fSqrDist )
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
					}
					else  // region 0p
					{
						// min face on r=1
						fSqrDist = QiDistanceSq(rkSegOrigin+rkSegDirection, triangle, &fS, &fT);
						fR = 1.0f;
					}
				}
				else
				{
					if ( fS < 0.0f )  // region 2p
					{
						// min on face s=0 or s+t=1 or r=1
						fSqrDist = QiDistanceSq(lineSegment, QiLineSegment(m_origin, m_edge1), &fR, &fT);
						fS = 0.0f;
						fSqrDist0 = QiDistanceSq(lineSegment, QiLineSegment(m_origin+m_edge0, m_edge1-m_edge0), &fR0, &fT0);
						fS0 = 1.0f-fT0;
						if ( fSqrDist0 < fSqrDist )
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
						fSqrDist0 = QiDistanceSq(rkSegOrigin+rkSegDirection, triangle, &fS0, &fT0);
						fR0 = 1.0f;
						if ( fSqrDist0 < fSqrDist )
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
					}
					else if ( fT < 0.0f )  // region 6p
					{
						// min on face t=0 or s+t=1 or r=1
						fSqrDist = QiDistanceSq(lineSegment, QiLineSegment(m_origin, m_edge0), &fR, &fS);
						fT = 0.0f;
						fSqrDist0 = QiDistanceSq(lineSegment, QiLineSegment(m_origin+m_edge0, m_edge1-m_edge0), &fR0, &fT0);
						fS0 = 1.0f-fT0;
						if ( fSqrDist0 < fSqrDist )
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
						fSqrDist0 = QiDistanceSq(rkSegOrigin+rkSegDirection, triangle, &fS0, &fT0);
						fR0 = 1.0f;
						if ( fSqrDist0 < fSqrDist )
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
					}
					else  // region 1p
					{
						// min on face s+t=1 or r=1
						fSqrDist = QiDistanceSq(lineSegment, QiLineSegment(m_origin+m_edge0, m_edge1-m_edge0), &fR, &fT);
						fS = 1.0f-fT;
						fSqrDist0 = QiDistanceSq(rkSegOrigin+rkSegDirection, triangle, &fS0, &fT0);
						fR0 = 1.0f;
						if ( fSqrDist0 < fSqrDist )
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
					}
				}
			}
		}
	}
	if ( !bNotParallel || !bAccurate ) 
	{
		// segment and triangle are parallel
		fSqrDist = QiDistanceSq(lineSegment, QiLineSegment(m_origin, m_edge0), &fR, &fS);
		fT = 0.0f;

		fSqrDist0 = QiDistanceSq(lineSegment, QiLineSegment(m_origin, m_edge1), &fR0, &fT0);
		fS0 = 0.0f;
		if ( fSqrDist0 < fSqrDist )
		{
			fSqrDist = fSqrDist0;
			fR = fR0;
			fS = fS0;
			fT = fT0;
		}

		fSqrDist0 = QiDistanceSq(lineSegment, QiLineSegment(m_origin + m_edge0, m_edge1 - m_edge0), &fR0, &fT0);
		fS0 = 1.0f-fT0;
		if ( fSqrDist0 < fSqrDist )
		{
			fSqrDist = fSqrDist0;
			fR = fR0;
			fS = fS0;
			fT = fT0;
		}

		fSqrDist0 = QiDistanceSq(rkSegOrigin, triangle, &fS0, &fT0);
		fR0 = 0.0f;
		if ( fSqrDist0 < fSqrDist )
		{
			fSqrDist = fSqrDist0;
			fR = fR0;
			fS = fS0;
			fT = fT0;
		}

		fSqrDist0 = QiDistanceSq(rkSegOrigin+rkSegDirection, triangle, &fS0, &fT0);
		fR0 = 1.0f;
		if ( fSqrDist0 < fSqrDist )
		{
			fSqrDist = fSqrDist0;
			fR = fR0;
			fS = fS0;
			fT = fT0;
		}
	}

	if ( t ) 
		*t = fR;

	if ( e0 )
		*e0 = fS;

	if ( e1 )
		*e1 = fT;

	return fSqrDist;
}


float QiDistanceSq(const QiRay& ray0, const QiRay& ray1, float* t0, float* t1)
{
	QI_ASSERT(false, "NOT IMPLEMENTED");
	return 0.0f;
}


float QiDistanceSq(const QiRay& ray, const QiTriangle& triangle, float* t, float* e0, float* e1)
{
	QI_ASSERT(false, "NOT IMPLEMENTED");
	return 0.0f;
}


float QiDistanceSq(const QiTriangle& triangle0, const QiTriangle& triangle1, float* e00, float* e01, float* e10, float* e11)
{
	QI_ASSERT(false, "NOT IMPLEMENTED");
	return 0.0f;
}


float QiDistanceSq(const QiVec2& point, const QiVec2& lineStart, const QiVec2& lineEnd, float* t)
{
	const float& cx = point.x;
	const float& cy = point.y;
	const float& ax = lineStart.x;
	const float& ay = lineStart.y;
	const float& bx = lineEnd.x;
	const float& by = lineEnd.y;
	float num = (cx-ax)*(bx-ax) + (cy-ay)*(by-ay);
	float den = (bx-ax)*(bx-ax) + (by-ay)*(by-ay);
	if (den == 0.0f)
	{
		if (t)
			*t = 0.0f;
		return (cx-ax)*(cx-ax) + (cy-ay)*(cy-ay);
	}

	float r = num / den;
	if (r < 0.0f)
	{
		if (t)
			*t = 0.0f;
		return (cx-ax)*(cx-ax) + (cy-ay)*(cy-ay);
	}
	else if (r > 1.0f)
	{
		if (t)
			*t = 1.0f;
		return (cx-bx)*(cx-bx) + (cy-by)*(cy-by);
	}
	else
	{
		if (t)
			*t = r;
		float dx = cx-ax-(bx-ax)*r;
		float dy = cy-ay-(by-ay)*r;
		return dx*dx+dy*dy;
	}
}

