/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "geometry/qivertprimitives.h"

void QiVertPrimitives::getCube(QiVec3 v[8], int f[36])
{
	if (v)
	{
		v[0].set(-1,-1,-1);
		v[1].set(1,-1,-1);
		v[2].set(1,1,-1);
		v[3].set(-1,1,-1);
		v[4].set(-1,-1,1);
		v[5].set(1,-1,1);
		v[6].set(1,1,1);
		v[7].set(-1,1,1);
	}

	if (f)
	{
		f[0] = 2;
		f[1] = 1;
		f[2] = 0;
		f[3] = 0;
		f[4] = 3;
		f[5] = 2;

		f[6] = 4;
		f[7] = 5;
		f[8] = 6;
		f[9] = 6;
		f[10] = 7;
		f[11] = 4;

		f[12] = 0;
		f[13] = 1;
		f[14] = 5;
		f[15] = 5;
		f[16] = 4;
		f[17] = 0;

		f[18] = 2;
		f[19] = 3;
		f[20] = 7;
		f[21] = 7;
		f[22] = 6;
		f[23] = 2;
	
		f[24] = 4;
		f[25] = 3;
		f[26] = 0;
		f[27] = 7;
		f[28] = 3;
		f[29] = 4;

		f[30] = 1;
		f[31] = 2;
		f[32] = 5;
		f[33] = 5;
		f[34] = 2;
		f[35] = 6;
	}
}
