#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "math/qivec2.h"
#include "geometry/qilinesegment.h"
#include "geometry/qiray.h"
#include "geometry/qitriangle.h"

float QiDistanceSq(const QiVec3& point, const QiLineSegment& lineSegment, float* t);
float QiDistanceSq(const QiVec3& point, const QiRay& ray, float* t);
float QiDistanceSq(const QiVec3& point, const QiTriangle& triangle, float* e0, float* e1);

float QiDistanceSq(const QiLineSegment& line0, const QiLineSegment& line1, float* t0, float* t1);
float QiDistanceSq(const QiLineSegment& line, const QiRay& ray, float* tl, float* tr);
float QiDistanceSq(const QiLineSegment& lineSegment, const QiTriangle& triangle, float* t, float* e0, float* e1);

float QiDistanceSq(const QiRay& ray0, const QiRay& ray1, float* t0, float* t1);
float QiDistanceSq(const QiRay& ray, const QiTriangle& triangle, float* t, float* e0, float* e1);

float QiDistanceSq(const QiTriangle& triangle0, const QiTriangle& triangle1, float* e00, float* e01, float* e10, float* e11);

float QiDistanceSq(const QiVec2& point, const QiVec2& lineStart, const QiVec2& lineEnd, float* t); 


