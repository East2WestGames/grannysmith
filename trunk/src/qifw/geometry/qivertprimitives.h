#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "math/qivec3.h"

class QiVertPrimitives
{
public:
	static void getCube(QiVec3 v[8], int f[36]);
};


