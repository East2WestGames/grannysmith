#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "math/qivec3.h"

class QiRay;

class QiLineSegment
{
public:
	QiVec3 p;
	QiVec3 v;

	inline QiLineSegment()
	{
	}

	//TODO: Verify no tries to call the point -> point version!
	inline QiLineSegment(const QiVec3& p, const QiVec3& v) :
	p(p), v(v)
	{
	}
};

