#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "math/qivec3.h"

class QiRay
{
public:
	QiVec3 p;
	QiVec3 d;

	inline QiRay()
	{
	}

	inline QiRay(const QiVec3& p, const QiVec3& d) :
	p(p), d(d)
	{
	}
};


