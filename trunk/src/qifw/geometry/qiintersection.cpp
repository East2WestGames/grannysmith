/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "geometry/qiintersection.h"

bool QiIntersection(const QiLineSegment& lineSegment, const QiTriangle& triangle, float* t, float* e0, float* e1)
{
	const QiVec3& edge0 = triangle.e0;
	const QiVec3& edge1 = triangle.e1;
	QiVec3 N = cross(edge0, edge1);
	const QiVec3& R = lineSegment.v; 
	float det = -dot(R, N);
	if (det > QI_FLOAT_EPSILON)
	{
		QiVec3 B = lineSegment.p - triangle.p;
		float l = dot(B, N);
		if (0.0f <= l && l <= det)
		{
			float oneOverDet = 1.0f / det;
			l *= oneOverDet;
			QiVec3 BxR = cross(B, R);
			float u1 = dot(edge1, BxR);
			if (0.0f <= u1)
			{
				u1 *= oneOverDet;
				float u2 = -dot(edge0, BxR);
				if (0.0f <= u2)
				{
					u2 *= oneOverDet;
					if (u1+u2 <= 1.0f) 
					{
						if (t)
							*t=l;
						if (e0)
							*e0 = u1;
						if (e1)
							*e1 = u2;
						return true;
					}
				}
			}
		}
	} 
	else if (det < -QI_FLOAT_EPSILON) 
	{
		QI_PRINT("<");
		QiVec3 B = lineSegment.p - triangle.p;
		float l = dot(B, N);
		if (0.0f >= l && l >= det) 
		{
			float oneOverDet = 1.0f / det;
			l *= oneOverDet;
			QiVec3 BxR = cross(B, R);
			float u1 = dot(edge1, BxR);
			if (0.0f >= u1) 
			{
				u1 *= oneOverDet;
				float u2 = -dot(edge0, BxR);
				if (0.0f >= u2) {
					u2 *= oneOverDet;
					if (u1+u2 <= 1.0f)
					{
						if (t)
							*t=l;
						if (e0)
							*e0=u1;
						if (e1)
							*e1=u2;
						return true;
					}
				}
			}
		}
	}
	return false;
}


bool QiIntersection(const QiRay& ray, const QiTriangle& triangle, float& t, float& e0, float& e1)
{
	QI_ASSERT(false, "NOT IMPLEMENTED");
	return false;
}


bool QiIntersection(const QiRay& ray, const QiSphere& sphere, float* t, QiVec3* point)
{
	//Algorithm from http://wiki.cgsociety.org/index.php/Ray_Sphere_Intersection
	//Compute A, B and C coefficients
	QiVec3 rayOrigin = ray.p-sphere.p;
    float a = dot(ray.d, ray.d);
    float b = 2 * dot(rayOrigin, ray.d);
    float c = dot(rayOrigin, rayOrigin) - (sphere.r * sphere.r);

    //Find discriminant
    float disc = b * b - 4 * a * c;
    
    // if discriminant is negative there are no real roots, so return 
    // false as ray misses sphere
    if (disc < 0)
        return false;

    // compute q as described above
    float distSqrt = QiSqrt(disc);
    float q;
    if (b < 0)
        q = (-b - distSqrt)/2.0f;
    else
        q = (-b + distSqrt)/2.0f;

    // compute t0 and t1
    float t0 = q / a;
    float t1 = c / q;

    // make sure t0 is smaller than t1
    if (t0 > t1)
    {
        // if t0 is bigger than t1 swap them around
        float temp = t0;
        t0 = t1;
        t1 = temp;
    }

    // if t1 is less than zero, the object is in the ray's negative direction
    // and consequently the ray misses the sphere
    if (t1 < 0)
        return false;

    // if t0 is less than zero, the intersection point is at t1
    if (t0 < 0)
    {
        if (t)
			*t = t1;
		if (point)
			*point = rayOrigin + ray.d*t1;
        return true;
    }
    // else the intersection point is at t0
    else
    {
		if (t)
			*t = t0;
		if (point)
			*point = rayOrigin + ray.d*t0;
        return true;
    }	
}

bool QiIntersection(const QiLineSegment& ls, const QiPlane& plane, float* t)
{
	//Plane equation: px*x + py*y + pz*z + t*(dx*x+dy*y+dz*z) + d = 0	
	float num = -ls.p.x*plane.x - ls.p.y*plane.y - ls.p.z*plane.z - plane.d;
	float den = ls.v.x*plane.x + ls.v.y*plane.y + ls.v.z*plane.z;
	if (den == 0.0f)
	{
		*t = 0.0f;
		return plane.getSignedDistance(ls.p) == 0.0f;
	}
	else
	{
		*t = num/den;
		return *t >= 0.0f && *t < 1.0f;
	}
}

