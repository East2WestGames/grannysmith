#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "math/qivec3.h"

class QiPlane
{
public:
	inline QiPlane() : x(0.0f), y(1.0f), z(0.0f), d(0.0f)
	{
	}

	inline QiPlane(float x, float y, float z, float d) : x(x), y(y), z(z), d(d)
	{
	}

	inline QiPlane(const QiVec3& point, const QiVec3& normal) : x(normal.x), y(normal.y), z(normal.z)
	{
		d = -normal.x*point.x-normal.y*point.y-normal.z*point.z;
	}
	
	inline QiVec3 getNormal() const
	{
		return QiVec3(x, y, z);
	}
	
	void fit(const QiVec3* points, int count);
	
	inline float getSignedDistance(const QiVec3& point) const
	{
		return point.x*x + point.y*y + point.z*z + d;
	}

	inline bool isAbove(const QiVec3& point) const
	{
		return getSignedDistance(point) >= 0.0f;
	}

	inline bool isBelow(const QiVec3& point) const
	{
		return getSignedDistance(point) < 0.0f;
	}

	float x, y, z, d;
};


