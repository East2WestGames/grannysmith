/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "math/qirnd.h"
#include "base/qimath.h"

#include <time.h>
#include <stdlib.h>

void QiRandomize()
{
	::srand((unsigned int)time(NULL));
}

void QiRandomize(int seed)
{
	::srand(seed);
}

int QiRnd(int low, int high)
{
	return QiClamp(low + (int)((::rand()/(float)(RAND_MAX-1))*(high-low)), low, high-1);
}

unsigned int QiRnd(unsigned int low, unsigned int high)
{
	return QiClamp(low + (unsigned int)((::rand()/(float)(RAND_MAX-1))*(high-low)), low, high-1);
}

float QiRnd(float low, float high)
{
	return low + (::rand()/(float)(RAND_MAX-1))*(high-low);
}

float QiRndNormal(float low, float high)
{
	float r = ::rand()/(float)(RAND_MAX-1);
	r = 2*r - 1.0f;
	r = QiSign(r)*r*r;
	return (low+high)/2 + r*(high-low)/2;
}

