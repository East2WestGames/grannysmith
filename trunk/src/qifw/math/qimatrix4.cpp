/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "math/qimatrix3.h"
#include "math/qimatrix4.h"
#include "math/qitransform2.h"
#include "math/qitransform3.h"


QiMatrix4::QiMatrix4(const QiTransform2& t)
{
	setPos(QiVec3(t.pos.x, t.pos.y, 0.0f));
	setRot(QiQuat(QiVec3::Z, t.rot));
	m[3] = 0.0f;
	m[7] = 0.0f;
	m[11] = 0.0f;
	m[15] = 1.0f;
}


QiMatrix4::QiMatrix4(const QiTransform3& t)
{
	setPos(t.pos);
	setRot(t.rot);
	m[3] = 0.0f;
	m[7] = 0.0f;
	m[11] = 0.0f;
	m[15] = 1.0f;
}


// Algorithm using cramer's rule, implementation from Intel:
// ftp://download.intel.com/design/PentiumIII/sml/24504301.pdf
static bool invert(const float mat[16], float dst[16])
{
	float tmp[12];
	float src[16];
	float det;

	/* transpose matrix */
	for (int i = 0; i<4; i++) 
	{
		src[i] = mat[i*4];
		src[i + 4] = mat[i*4 + 1];
		src[i + 8] = mat[i*4 + 2]; 
		src[i + 12] = mat[i*4 + 3];
	}

	/* calculate pairs for first 8 elements (cofactors) */
	tmp[0] = src[10] * src[15]; 
	tmp[1] = src[11] * src[14]; 
	tmp[2] = src[9] * src[15]; 
	tmp[3] = src[11] * src[13]; 
	tmp[4] = src[9] * src[14]; 
	tmp[5] = src[10] * src[13]; 
	tmp[6] = src[8] * src[15]; 
	tmp[7] = src[11] * src[12]; 
	tmp[8] = src[8] * src[14]; 
	tmp[9] = src[10] * src[12]; 
	tmp[10] = src[8] * src[13]; 
	tmp[11] = src[9] * src[12];

	/* calculate first 8 elements (cofactors) */
	dst[0] = tmp[0]*src[5] + tmp[3]*src[6] + tmp[4]*src[7]; 
	dst[0] -= tmp[1]*src[5] + tmp[2]*src[6] + tmp[5]*src[7];
	dst[1] = tmp[1]*src[4] + tmp[6]*src[6] + tmp[9]*src[7];
	dst[1] -= tmp[0]*src[4] + tmp[7]*src[6] + tmp[8]*src[7];
	dst[2] = tmp[2]*src[4] + tmp[7]*src[5] + tmp[10]*src[7];
	dst[2] -= tmp[3]*src[4] + tmp[6]*src[5] + tmp[11]*src[7];
	dst[3] = tmp[5]*src[4] + tmp[8]*src[5] + tmp[11]*src[6];
	dst[3] -= tmp[4]*src[4] + tmp[9]*src[5] + tmp[10]*src[6];
	dst[4] = tmp[1]*src[1] + tmp[2]*src[2] + tmp[5]*src[3];
	dst[4] -= tmp[0]*src[1] + tmp[3]*src[2] + tmp[4]*src[3];
	dst[5] = tmp[0]*src[0] + tmp[7]*src[2] + tmp[8]*src[3];
	dst[5] -= tmp[1]*src[0] + tmp[6]*src[2] + tmp[9]*src[3];
	dst[6] = tmp[3]*src[0] + tmp[6]*src[1] + tmp[11]*src[3];
	dst[6] -= tmp[2]*src[0] + tmp[7]*src[1] + tmp[10]*src[3];
	dst[7] = tmp[4]*src[0] + tmp[9]*src[1] + tmp[10]*src[2];
	dst[7] -= tmp[5]*src[0] + tmp[8]*src[1] + tmp[11]*src[2];

	/* calculate pairs for second 8 elements (cofactors) */
	tmp[0] = src[2]*src[7];
	tmp[1] = src[3]*src[6];
	tmp[2] = src[1]*src[7];
	tmp[3] = src[3]*src[5];
	tmp[4] = src[1]*src[6];
	tmp[5] = src[2]*src[5];
	tmp[6] = src[0]*src[7]; 
	tmp[7] = src[3]*src[4];
	tmp[8] = src[0]*src[6];
	tmp[9] = src[2]*src[4];
	tmp[10] = src[0]*src[5];
	tmp[11] = src[1]*src[4];
	
	/* calculate second 8 elements (cofactors) */
	dst[8] = tmp[0]*src[13] + tmp[3]*src[14] + tmp[4]*src[15];
	dst[8] -= tmp[1]*src[13] + tmp[2]*src[14] + tmp[5]*src[15];
	dst[9] = tmp[1]*src[12] + tmp[6]*src[14] + tmp[9]*src[15];
	dst[9] -= tmp[0]*src[12] + tmp[7]*src[14] + tmp[8]*src[15];
	dst[10] = tmp[2]*src[12] + tmp[7]*src[13] + tmp[10]*src[15];
	dst[10]-= tmp[3]*src[12] + tmp[6]*src[13] + tmp[11]*src[15];
	dst[11] = tmp[5]*src[12] + tmp[8]*src[13] + tmp[11]*src[14];
	dst[11]-= tmp[4]*src[12] + tmp[9]*src[13] + tmp[10]*src[14];
	dst[12] = tmp[2]*src[10] + tmp[5]*src[11] + tmp[1]*src[9];
	dst[12]-= tmp[4]*src[11] + tmp[0]*src[9] + tmp[3]*src[10];
	dst[13] = tmp[8]*src[11] + tmp[0]*src[8] + tmp[7]*src[10];
	dst[13]-= tmp[6]*src[10] + tmp[9]*src[11] + tmp[1]*src[8];
	dst[14] = tmp[6]*src[9] + tmp[11]*src[11] + tmp[3]*src[8];
	dst[14]-= tmp[10]*src[11] + tmp[2]*src[8] + tmp[7]*src[9];
	dst[15] = tmp[10]*src[10] + tmp[4]*src[8] + tmp[9]*src[9];
	dst[15]-= tmp[8]*src[9] + tmp[11]*src[10] + tmp[5]*src[8];
	
	/* calculate determinant */
	det=src[0]*dst[0]+src[1]*dst[1]+src[2]*dst[2]+src[3]*dst[3];

	/* sanity check */
	if (det == 0.0f)
		return false;

	/* calculate matrix inverse */
	det = 1.0f/det; 
	for (int j = 0; j < 16; j++)
		dst[j] *= det;
		
	return true;
}


float QiMatrix4::getDeterminant() const
{
	QI_ERROR("Not implemented!");
	return 0.0f;
}


bool QiMatrix4::invert()
{
	return ::invert(m, m);
}


QiMatrix4 QiMatrix4::operator*(const QiMatrix4& other) const
{
	QiMatrix4 result;
    result.m[0]=m[0]*other.m[0]+m[4]*other.m[1]+m[8]*other.m[2]+m[12]*other.m[3];
    result.m[4]=m[0]*other.m[4]+m[4]*other.m[5]+m[8]*other.m[6]+m[12]*other.m[7];
    result.m[8]=m[0]*other.m[8]+m[4]*other.m[9]+m[8]*other.m[10]+m[12]*other.m[11];
    result.m[12]=m[0]*other.m[12]+m[4]*other.m[13]+m[8]*other.m[14]+m[12]*other.m[15];
    result.m[1]=m[1]*other.m[0]+m[5]*other.m[1]+m[9]*other.m[2]+m[13]*other.m[3];
    result.m[5]=m[1]*other.m[4]+m[5]*other.m[5]+m[9]*other.m[6]+m[13]*other.m[7];
    result.m[9]=m[1]*other.m[8]+m[5]*other.m[9]+m[9]*other.m[10]+m[13]*other.m[11];
    result.m[13]=m[1]*other.m[12]+m[5]*other.m[13]+m[9]*other.m[14]+m[13]*other.m[15];
    result.m[2]=m[2]*other.m[0]+m[6]*other.m[1]+m[10]*other.m[2]+m[14]*other.m[3];
    result.m[6]=m[2]*other.m[4]+m[6]*other.m[5]+m[10]*other.m[6]+m[14]*other.m[7];
    result.m[10]=m[2]*other.m[8]+m[6]*other.m[9]+m[10]*other.m[10]+m[14]*other.m[11];
    result.m[14]=m[2]*other.m[12]+m[6]*other.m[13]+m[10]*other.m[14]+m[14]*other.m[15];
    result.m[3]=m[3]*other.m[0]+m[7]*other.m[1]+m[11]*other.m[2]+m[15]*other.m[3];
    result.m[7]=m[3]*other.m[4]+m[7]*other.m[5]+m[11]*other.m[6]+m[15]*other.m[7];
    result.m[11]=m[3]*other.m[8]+m[7]*other.m[9]+m[11]*other.m[10]+m[15]*other.m[11];
    result.m[15]=m[3]*other.m[12]+m[7]*other.m[13]+m[11]*other.m[14]+m[15]*other.m[15];
	return result;
}


bool QiMatrix4::isOrthoNormal() const
{
	const float eps = QI_ORTHONORMAL_TOLERANCE;
	const float epsSq = eps * eps;
	QiVec3 x(m[0], m[1], m[2]);
	QiVec3 y(m[4], m[5], m[6]);
	QiVec3 z(m[8], m[9], m[10]);
	if (QiAbs(lengthSquared(x)-1.0f) > epsSq || 
		QiAbs(lengthSquared(y)-1.0f) > epsSq ||
		QiAbs(lengthSquared(z)-1.0f) > epsSq)
		return false;
	if (QiAbs(dot(x, y)) > eps)
		return false;
	if (QiAbs(dot(x, z)) > eps)
		return false;
	if (QiAbs(dot(y, z)) > eps)
		return false;
	if (QiAbs(m[15]-1.0f) > eps)
		return false;
	if (QiAbs(m[3]) > eps || QiAbs(m[7]) > eps || QiAbs(m[11]) > eps)
		return false;
	return true;
}


QiMatrix3 QiMatrix4::getRot() const
{
	return QiMatrix3(QiVec3(m[0], m[1], m[2]), 
					  QiVec3(m[4], m[5], m[6]), 
					  QiVec3(m[8], m[9], m[10]));
}

