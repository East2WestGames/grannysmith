#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"

class QiVec4
{
public:
	float x,y,z,w;

	inline QiVec4() { x=y=z=w=0.0f; }
	explicit inline QiVec4(QiUninitialized) { }
	inline QiVec4(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) { }
	explicit inline QiVec4(const float e[4]) { x=e[0]; y=e[1]; z=e[2]; w=e[3]; }
	explicit QiVec4(const class QiVec2& vec2, float z, float w);
	explicit QiVec4(const class QiVec3& vec3, float w);

	inline void set(float x, float y, float z, float w) { this->x = x; this->y = y; this->z = z; this->w = w; }
	inline float operator[](int i) const { return (&x)[i]; }
	inline float& operator[](int i) { return (&x)[i]; }

	inline bool operator==(const QiVec4& vec) const { return x==vec.x && y==vec.y && z==vec.z && w==vec.w; }
	inline bool operator!=(const QiVec4& vec) const { return x!=vec.x || y!=vec.y || z!=vec.z || w!=vec.w; }

	operator class QiVec2() const;
	operator class QiVec3() const;
	operator class QiString() const;
};

