#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "math/qiquat.h"
#include "math/qivec2.h"
#include "math/qivec3.h"

class QiMatrix3
{
public:
	float m[9];

	inline QiMatrix3()
	{
		setIdentity();
	}
	
	explicit inline QiMatrix3(QiUninitialized)
	{
	}
	
	explicit QiMatrix3(const class QiTransform2& t);

	inline void setIdentity()
	{
		m[0] = 1.0f; m[1] = 0.0f; m[2] = 0.0f;
		m[3] = 0.0f; m[4] = 1.0f; m[5] = 0.0f;
		m[6] = 0.0f; m[7] = 0.0f; m[8] = 1.0f;
	}

	inline QiMatrix3(const QiVec3& x, const QiVec3& y, const QiVec3& z)
	{
		m[0] = x.x; m[1] = x.y; m[2] = x.z;
		m[3] = y.x; m[4] = y.y; m[5] = y.z;
		m[6] = z.x; m[7] = z.y; m[8] = z.z;
	}

	inline QiMatrix3(const QiQuat& q)
	{
		m[0] = 1.0f - 2.0f*(q.y*q.y + q.z*q.z); // x.x
		m[1] = 2.0f*(q.x*q.y + q.z*q.w);		// x.y
		m[2] = 2.0f*(q.x*q.z - q.y*q.w);		// x.z
		m[3] = 2.0f*(q.x*q.y - q.z*q.w);		// y.x
		m[4] = 1.0f - 2.0f*(q.x*q.x + q.z*q.z); // y.y
		m[5] = 2.0f*(q.y*q.z + q.x*q.w);		// y.z
		m[6] = 2.0f*(q.x*q.z + q.y*q.w);		// z.x
		m[7] = 2.0f*(q.y*q.z - q.x*q.w);		// z.y
		m[8] = 1.0f - 2.0f*(q.x*q.x + q.y*q.y); // z.z
	}

	inline void setBase(int i, const QiVec3& dir)
	{
		m[i*3+0] = dir.x;
		m[i*3+1] = dir.y;
		m[i*3+2] = dir.z;
	}
	
	inline QiVec3 getBase(int i) const
	{
		return QiVec3(m[i*3+0], m[i*3+1], m[i*3+2]);
	}
	
	inline QiVec3 getX() const
	{
		return QiVec3(m[0], m[1], m[2]);
	}

	inline QiVec3 getY() const
	{
		return QiVec3(m[3], m[4], m[5]);
	}

	inline QiVec3 getZ() const
	{
		return QiVec3(m[6], m[7], m[8]);
	}

	inline QiVec2 transformVec(const QiVec2& vec) const
	{
		return QiVec2(vec.x*m[0] + vec.y*m[3],
					  vec.x*m[1] + vec.y*m[4]);
	}

	inline QiVec2 transformVecInv(const QiVec2& vec) const
	{
		return QiVec2(vec.x*m[0] + vec.y*m[1],
					  vec.x*m[3] + vec.y*m[4]);
	}

	inline QiVec2 transformPoint(const QiVec2& p) const
	{
		return QiVec2(p.x*m[0] + p.y*m[3] + m[6],
					  p.x*m[1] + p.y*m[4] + m[8]);
	}

	inline QiVec2 transformPointInv(const QiVec2& p, bool orthoNormal=false) const
	{
		if (orthoNormal)
		{
			QiVec2 tmp = p - QiVec2(m[6], m[7]);
			return QiVec2(tmp.x*m[0] + tmp.y*m[3],
						  tmp.x*m[1] + tmp.y*m[4]);
		}
		else
		{
			QiMatrix3 tmp = *this;
			tmp.invert();
			return tmp.transformPoint(p);
		}
	}
	
	bool invert();

	float getDeterminant() const;

	inline QiVec3 operator*(const QiVec3& v) const
	{
		return QiVec3(m[0]*v.x + m[3]*v.y + m[6]*v.z, 
					  m[1]*v.x + m[4]*v.y + m[7]*v.z,
					  m[2]*v.x + m[5]*v.y + m[8]*v.z);
	}
	
	QiMatrix3 operator*(const QiMatrix3& other) const;

	void operator*=(const QiMatrix3& other) { *this = *this * other; }
	
};

