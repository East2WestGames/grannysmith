/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "math/qitransform2.h"
#include "base/qistring.h"


QiTransform2::operator QiString() const
{
	return QiString("{") + pos.x + ", " + pos.y + QiString(" / ") + rot + "}";
}

