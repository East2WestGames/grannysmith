#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "math/qivec3.h"
#include "math/qiquat.h"

class QiTransform3
{
public:
	QiVec3 pos;
	QiQuat rot;

	inline QiTransform3() { }
 	inline explicit QiTransform3(QiUninitialized) : pos(QI_UNINITIALIZED), rot(QI_UNINITIALIZED) { }
	inline explicit QiTransform3(const QiVec3& pos) : pos(pos) { }
	inline explicit QiTransform3(const QiQuat& rot) : rot(rot) { }
	inline QiTransform3(const QiVec3& pos, const QiQuat& rot) : pos(pos), rot(rot) { }
	explicit QiTransform3(const class QiTransform2& transform2);

	inline QiVec3 toParentVec(const QiVec3& vec) const
	{
		return rot.rotate(vec);
	}

	inline QiVec3 toLocalVec(const QiVec3& vec) const
	{
		return rot.rotateInv(vec);
	}

	inline QiVec3 toParentPoint(const QiVec3& point) const
	{
		return pos + rot.rotate(point);
	}

	inline QiVec3 toLocalPoint(const QiVec3& point) const
	{
		return rot.rotateInv(point-pos);
	}

	inline QiQuat toParent(const QiQuat& quat) const
	{
		return rot.rotate(quat);
	}

	inline QiQuat toLocal(const QiQuat& quat) const
	{
		return rot.rotateInv(quat);
	}

	inline QiTransform3 toParent(const QiTransform3& transform) const
	{
		return QiTransform3(pos + rot.rotate(transform.pos), rot.rotate(transform.rot));
	}

	inline QiTransform3 toLocal(const QiTransform3& transform) const
	{
		return QiTransform3(rot.rotateInv(transform.pos - pos), rot.rotateInv(transform.rot));
	}

	operator QiString() const;
};

