#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qimath.h"

inline float QiGaussFilter(float x, float cutOff=1.0f)
{
	return 1.0f/cutOff * QiExp(-QI_PI*QiPow(x/cutOff, 2.0f));
}

inline float QiGaussFilter2D(float x, float y, float cutOff=1.0f)
{
	return QiGaussFilter(x, cutOff) * QiGaussFilter(y, cutOff);
}

void QiGaussMap2D(QiUInt8* data, int width, int height, float cutOff=1.0f);

void QiGaussMap2D(float* data, int width, int height, float cutOff=1.0f);



