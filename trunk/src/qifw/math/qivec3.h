#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"
#include "base/qimath.h"

#define QI_ASSERT_VEC3_NORMALIZED(vec) { QI_ASSERT(vec.lengthSquared() > 0.999f && vec.lengthSquared() < 1.001f, "Vector not normalized");  }

class QiVec3
{
public:
	float x,y,z;

	inline QiVec3() { x=y=z=0.0f; }
	explicit inline QiVec3(QiUninitialized) { }
	inline QiVec3(float x, float y, float z) : x(x), y(y), z(z) { }
	explicit inline QiVec3(const float e[3]) { x=e[0]; y=e[1]; z=e[2]; }
	inline void set(float x, float y, float z) { this->x = x; this->y = y; this->z = z; }
	inline float operator[](int i) const { return (&x)[i]; }
	inline float& operator[](int i) { return (&x)[i]; }

	inline QiVec3 operator+(const QiVec3& v) const { return QiVec3(x+v.x, y+v.y, z+v.z); }
	inline QiVec3 operator-(const QiVec3& v) const { return QiVec3(x-v.x, y-v.y, z-v.z); }
	inline void operator+=(const QiVec3& v) { x+=v.x; y+=v.y; z+=v.z; }
	inline void operator-=(const QiVec3& v) { x-=v.x; y-=v.y; z-=v.z; }
	inline QiVec3 operator-() const { return QiVec3(-x, -y, -z); }

	inline void operator*=(float f) { x*=f; y*=f; z*=f; } 
	inline void operator/=(float f) { x/=f; y/=f; z/=f; } 	
	inline QiVec3 operator*(float f) const { return QiVec3(x*f, y*f, z*f); }
	inline QiVec3 operator/(float f) const { return QiVec3(x/f, y/f, z/f); }

	inline bool operator==(const QiVec3& vec) const { return x==vec.x && y==vec.y && z==vec.z; }
	inline bool operator!=(const QiVec3 &vec) const { return x!=vec.x || y!=vec.y || z!=vec.z; }

	inline bool equals(const QiVec3& vec, float epsilon) const { return QiAbs(x-vec.x)<=epsilon && QiAbs(y-vec.y)<=epsilon && QiAbs(z-vec.z)<=epsilon; }

	inline float length() const { return QiSqrt(x*x + y*y + z*z); }
	inline float lengthSquared() const { return x*x + y*y + z*z; }

	class QiVec2 vec2() const;

	static QiVec3 random(float length=1.0f);
	static QiVec3 X;
	static QiVec3 Y;
	static QiVec3 Z;

	operator class QiString() const;
};


inline QiVec3 operator*(float f, const QiVec3& vec)
{
	return QiVec3(vec.x*f, vec.y*f, vec.z*f);
}


inline QiVec3 operator/(float f, const QiVec3& vec)
{
	return QiVec3(vec.x/f, vec.y/f, vec.z/f);
}


inline float length(const QiVec3& vec)
{
	return QiSqrt(vec.x*vec.x + vec.y*vec.y + vec.z*vec.z);
}


inline float lengthSquared(const QiVec3& vec)
{
	return vec.x*vec.x + vec.y*vec.y + vec.z*vec.z;
}


inline float manhattanDistance(const QiVec3& vec)
{
	return QiAbs(vec.x)+QiAbs(vec.y)+QiAbs(vec.z);
}


inline QiVec3 normalize(const QiVec3& vec)
{
	float l = length(vec);
	if (l > 0.0f)
		return vec / l;
	else
		return QiVec3(1.0f, 0.0f, 0.0f);
}


inline QiVec3 normalize(const QiVec3& vec, float& oldLength)
{
	oldLength = length(vec);
	if (oldLength > 0.0f)
		return vec / oldLength;
	else
		return QiVec3(1.0f, 0.0f, 0.0f);
}


inline QiVec3 normalizeFast(const QiVec3& vec)
{
	float rec = QiRSqrtFast(vec.x*vec.x + vec.y*vec.y + vec.z*vec.z);
	return QiVec3(vec.x*rec, vec.y*rec, vec.z*rec);
}


inline QiVec3 normalizeCarmack(const QiVec3& vec)
{
	float rec = QiRSqrtCarmack(vec.x*vec.x + vec.y*vec.y + vec.z*vec.z);
	return QiVec3(vec.x*rec, vec.y*rec, vec.z*rec);
}


inline QiVec3 multiplyPerElement(const QiVec3& a, const QiVec3& b)
{
	return QiVec3(a.x*b.x, a.y*b.y, a.z*b.z);
}


inline QiVec3 cross(const QiVec3& a, const QiVec3& b) 
{
	return QiVec3(a.y*b.z - b.y*a.z, a.z*b.x - b.z*a.x, a.x*b.y - b.x*a.y);
}


inline float dot(const QiVec3 &a, const QiVec3& b)
{
	return a.x*b.x + a.y*b.y + a.z*b.z;
}
	
	
inline QiVec3 project(const QiVec3& normal, const QiVec3& vec)
{
	QI_ASSERT_VEC3_NORMALIZED(normal);
	return normal*dot(normal, vec);
}


inline float angle(const QiVec3& normal0, const QiVec3& normal1)
{
	QI_ASSERT_VEC3_NORMALIZED(normal0);
	QI_ASSERT_VEC3_NORMALIZED(normal1);
	return QiACos(QiClamp(dot(normal0, normal1), -1.0f, 1.0f));
}


inline QiVec3 perpendicular(const QiVec3& vec)
{
	if(QiAbs(vec.z) > 0.7f)
		return normalize(QiVec3(0.0f, -vec.z, vec.y));
	else
		return normalize(QiVec3(-vec.y, vec.x, 0.0f));
}


//Compute the normal to a triangle defined by points p0, p1, p2 in CCW order 
inline QiVec3 triangleNormal(const QiVec3& p0, const QiVec3& p1, const QiVec3& p2)
{
	return normalize(cross(p1-p0, p2-p0));
}

