#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"

float QiPerlinNoise(float x, float y, float frequency = 1.0f, float amplitude = 1.0f, int octaves = 5);

void QiPerlinNoise(float* data, int width, int height, float frequency = 1.0f, float amplitude = 1.0f, int octaves = 5);

void QiPerlinNoise(QiUInt8* data, int width, int height, float frequency = 1.0f, float amplitude = 1.0f, int octaves = 5);


