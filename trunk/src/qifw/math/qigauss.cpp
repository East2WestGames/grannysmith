/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qimath.h"
#include "math/qigauss.h"

void QiGaussMap2D(QiUInt8* data, int width, int height, float cutOff)
{
	for(int y=0; y<height; y++)
	{
		for(int x=0; x<width; x++)
		{			
			float g = QiGaussFilter2D(2.0f*x/(float)width-1.0f, 2.0f*y/(float)height-1.0f, cutOff);  
			g = QiClamp(g, 0.0f, 1.0f);
			data[y*width + x] = (QiUInt8)(g*255.0f);
		}
	}
}

void QiGaussMap2D(float* data, int width, int height, float cutOff)
{
	for(int y=0; y<height; y++)
	{
		for(int x=0; x<width; x++)
		{
			float g = QiGaussFilter2D(2.0f*x/(float)width-1.0f, 2.0f*y/(float)height-1.0f, cutOff);
			g = QiClamp(g, 0.0f, 1.0f);
			data[y*width + x] = g;
		}
	}
}
