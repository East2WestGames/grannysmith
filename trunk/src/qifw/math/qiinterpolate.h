#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qimath.h"

inline float QiInterpolateLinear(float v0, float v1, float t)
{
	return v0*(1.0f-t) + v1*t;
}

inline float QiInterpolateCosine(float v0, float v1, float t)
{
	return QiInterpolateLinear(v0, v1, (1.0f - QiCos(t*QI_PI)) * 0.5f);
}

inline float QiInterpolateCubic(float v00, float v0, float v1, float v11, float t)
{
	float p = (v11 - v1) - (v00 - v0);
	float q = (v00 - v0) - p;
	float r = v1 - v00;
	float s = v0;
	return p*(t*t*t) + q*(t*t) + r*t + s;
}

inline float QiInterpolateHermite(float p0, float p1, float m0, float m1, float t, float t0=0.0f, float t1=1.0f)
{
	float h = t1 - t0;
	if (h == 0.0f)
		t = 0.0f;
	else
		t = (t - t0) / h;
	float tt = t*t;
	float ttt = tt*t;
	float h00 = 2*ttt - 3*tt + 1;
	float h10 = ttt - 2*tt + t;
	float h01 = -2*ttt + 3*tt;
	float h11 = ttt - tt;
	return h00*p0 + h10*m0 + h01*p1 + h11*m1;
}

