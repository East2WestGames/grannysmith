#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "math/qivec3.h"
#include "math/qivec4.h"
#include "math/qiquat.h"

class QiMatrix4
{
public:
	float m[16];


	inline QiMatrix4()
	{
		setIdentity();
	}
	
	
	explicit inline QiMatrix4(QiUninitialized)
	{
	}


	inline void setIdentity()
	{
		m[0 ] = 1.0f; m[1 ] = 0.0f; m[2 ] = 0.0f; m[3 ] = 0.0f;
		m[4 ] = 0.0f; m[5 ] = 1.0f; m[6 ] = 0.0f; m[7 ] = 0.0f;
		m[8 ] = 0.0f; m[9 ] = 0.0f; m[10] = 1.0f; m[11] = 0.0f;
		m[12] = 0.0f; m[13] = 0.0f; m[14] = 0.0f; m[15] = 1.0f;
	}


	inline QiMatrix4(const float m[16])
	{
		memcpy(this->m, m, sizeof(float)*16);
	}


	inline QiMatrix4(const QiVec3& pos)
	{
		setIdentity();
		setPos(pos);
	}

	inline QiMatrix4(const QiQuat& rot)
	{
		setIdentity();
		setRot(rot);
	}

	inline QiMatrix4(const QiVec3& pos, const QiQuat& rot)
	{
		setPos(pos);
		setRot(rot);
		m[3] = 0.0f;
		m[7] = 0.0f;
		m[11] = 0.0f;
		m[15] = 1.0f;
	}

	QiMatrix4(const class QiTransform2& t);

	QiMatrix4(const class QiTransform3& t);

	inline void setRot(const QiQuat& q)
	{
		m[0] = 1.0f - 2.0f*(q.y*q.y + q.z*q.z); // x.x
		m[1] = 2.0f*(q.x*q.y + q.z*q.w);		// x.y
		m[2] = 2.0f*(q.x*q.z - q.y*q.w);		// x.z
		m[4] = 2.0f*(q.x*q.y - q.z*q.w);		// y.x
		m[5] = 1.0f - 2.0f*(q.x*q.x + q.z*q.z); // y.y
		m[6] = 2.0f*(q.y*q.z + q.x*q.w);		// y.z
		m[8] = 2.0f*(q.x*q.z + q.y*q.w);		// z.x
		m[9] = 2.0f*(q.y*q.z - q.x*q.w);		// z.y
		m[10] = 1.0f - 2.0f*(q.x*q.x + q.y*q.y);// z.z
	}


	inline void setPos(const QiVec3& pos)
	{
		m[12] = pos.x;
		m[13] = pos.y;
		m[14] = pos.z;
	}
	
	bool isOrthoNormal() const;
	
	inline bool isRightHanded() const
	{
		QiVec3 x(m[0], m[1], m[2]);
		QiVec3 y(m[4], m[5], m[6]);
		QiVec3 z(m[8], m[9], m[10]);
		return dot(cross(x, y), z) > 0.0f;
	}

	inline void transposeRotation()
	{
		QiSwap(m[1], m[4]);
		QiSwap(m[2], m[8]);
		QiSwap(m[6], m[9]);
	}

	inline QiMatrix3 getRot() const;

	inline QiVec3 getPos() const
	{
		return QiVec3(m[12], m[13], m[14]);
	}


	inline QiVec3 transformVec(const QiVec3& vec) const
	{
		return QiVec3(vec.x*m[0] + vec.y*m[4] + vec.z*m[8],
					vec.x*m[1] + vec.y*m[5] + vec.z*m[9],
					vec.x*m[2] + vec.y*m[6] + vec.z*m[10]);
	}

	inline QiVec3 transformVecInv(const QiVec3& vec) const
	{
		return QiVec3(vec.x*m[0] + vec.y*m[1] + vec.z*m[2],
					vec.x*m[4] + vec.y*m[5] + vec.z*m[6],
					vec.x*m[8] + vec.y*m[9] + vec.z*m[10]);
	}

	inline QiVec3 transformPoint(const QiVec3& p) const
	{
		return QiVec3(p.x*m[0] + p.y*m[4] + p.z*m[8] + m[12],
					p.x*m[1] + p.y*m[5] + p.z*m[9] + m[13],
					p.x*m[2] + p.y*m[6] + p.z*m[10] + m[14]);
	}

	//Note that this is expensive for non-orthonormal tranforms
	//since it involves a 4x4 matrix inverse. If transforming many
	//points, consider creating an inverted transform explicitly.
	inline QiVec3 transformPointInv(const QiVec3& p, bool orthoNormal=false) const
	{
		if (orthoNormal)
		{
			QiVec3 tmp = p - QiVec3(m[12], m[13], m[14]);
			return QiVec3(tmp.x*m[0] + tmp.y*m[1] + tmp.z*m[2],
						tmp.x*m[4] + tmp.y*m[5] + tmp.z*m[6],
						tmp.x*m[8] + tmp.y*m[9] + tmp.z*m[10]);
		}
		else
		{
			QiMatrix4 tmp = *this;
			tmp.invert();
			return tmp.transformPoint(p);
		}
	}

	bool invert();
	
	float getDeterminant() const;

	inline QiVec4 operator*(const QiVec4& v) const
	{ 
		return QiVec4(v.x*m[0] + v.y*m[4] + v.z*m[8] + v.w*m[12],
					  v.x*m[1] + v.y*m[5] + v.z*m[9] + v.w*m[13],
					  v.x*m[2] + v.y*m[6] + v.z*m[10] + v.w*m[14],
					  v.x*m[3] + v.y*m[7] + v.z*m[11] + v.w*m[15]);
	}	

	QiMatrix4 operator*(const QiMatrix4& other) const;

	void operator*=(const QiMatrix4& other) { *this = *this * other; }
};

