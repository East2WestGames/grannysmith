/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qistring.h"

#include "math/qivec2.h"
#include "math/qivec3.h"
#include "math/qivec4.h"


QiVec4::QiVec4(const QiVec2& vec2, float z, float w) : x(vec2.x), y(vec2.y), z(z), w(w)
{
}


QiVec4::QiVec4(const QiVec3& vec3, float w) : x(vec3.x), y(vec3.y), z(vec3.z), w(w)
{
}


QiVec4::operator QiVec2() const
{
	return QiVec2(x, y);
}


QiVec4::operator QiVec3() const
{
	return QiVec3(x, y, z);
}


QiVec4::operator QiString() const
{
	return QiString("{") + x + ", " + y + ", " + z + ", " + w + "}";
}

