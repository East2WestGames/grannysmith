#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"

void QiRandomize();
void QiRandomize(int seed);
int QiRnd(int low, int high);
unsigned int QiRnd(unsigned int low, unsigned int high);
float QiRnd(float low, float high);
float QiRndNormal(float low, float high);


