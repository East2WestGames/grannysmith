#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "math/qivec2.h"

class QiTransform2
{
public:
	QiVec2 pos;
	float rot;

	inline QiTransform2() : rot(0.0f) { }
	inline explicit QiTransform2(QiUninitialized) : pos(QI_UNINITIALIZED) { }
	inline explicit QiTransform2(const QiVec2& pos) : pos(pos), rot(0.0f) { }
	inline explicit QiTransform2(float rot) : rot(rot) { }
	inline QiTransform2(const QiVec2& pos, float rot) : pos(pos), rot(rot) { }

	inline QiVec2 toParentVec(const QiVec2& vec) const
	{
		return rotate(vec, rot);
	}

	inline QiVec2 toLocalVec(const QiVec2& vec) const
	{
		return rotate(vec, -rot);
	}

	inline QiVec2 toParentPoint(const QiVec2& point) const
	{
		return pos + rotate(point, rot);
	}

	inline QiVec2 toLocalPoint(const QiVec2& point) const
	{
		return rotate(point-pos, -rot);
	}

	inline QiTransform2 toParent(const QiTransform2& transform) const
	{
		return QiTransform2(pos + rotate(transform.pos, rot), rot+transform.rot);
	}

	inline QiTransform2 toLocal(const QiTransform2& transform) const
	{
		return QiTransform2(rotate(transform.pos - pos, -rot), transform.rot-rot);
	}

	operator QiString() const;
};

