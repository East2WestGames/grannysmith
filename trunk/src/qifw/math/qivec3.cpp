/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "math/qivec2.h"
#include "math/qivec3.h"
#include "math/qirnd.h"

QiVec3 QiVec3::X(1.0f, 0.0f, 0.0f);
QiVec3 QiVec3::Y(0.0f, 1.0f, 0.0f);
QiVec3 QiVec3::Z(0.0f, 0.0f, 1.0f);


QiVec3 QiVec3::random(float length)
{
	return normalize(QiVec3(QiRnd(-1.0f, 1.0f), QiRnd(-1.0f, 1.0f), QiRnd(-1.0f, 1.0f)))*length;
}


QiVec2 QiVec3::vec2() const
{
	return QiVec2(x, y);
}


QiVec3::operator QiString() const
{
	return QiString("{") + x + ", " + y + ", " + z + "}";
}


