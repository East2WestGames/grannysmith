/* Qi Framework. Copyright 2007-2012 Dengu AB */

#pragma once

#include "math/qivec2.h"

class QiMatrix2
{
public:
	float m[4];

	inline QiMatrix2()
	{
		setIdentity();
	}
	
	explicit inline QiMatrix2(QiUninitialized)
	{
	}
	
	inline void setRotation(float angle)
	{
		m[0] = QiCos(angle);
		m[1] = QiSin(angle);
		m[2] = -m[1];
		m[3] = m[0];
	}

	inline void setIdentity()
	{
		m[0] = 1.0f; m[1] = 0.0f;
		m[2] = 0.0f; m[3] = 1.0f;
	}

	inline QiMatrix2(const QiVec2& x, const QiVec2& y)
	{
		m[0] = x.x;
		m[1] = x.y;
		m[2] = y.x;
		m[3] = y.y;
	}

	inline void setBase(int i, const QiVec2& dir)
	{
		m[i*2+0] = dir.x;
		m[i*2+1] = dir.y;
	}
	
	inline QiVec2 getBase(int i) const
	{
		return QiVec2(m[i*2+0], m[i*2+1]);
	}
	
	inline QiVec2 getX() const
	{
		return QiVec2(m[0], m[1]);
	}

	inline QiVec2 getY() const
	{
		return QiVec2(m[2], m[3]);
	}
	
	bool invert();
	
	inline float getAngle() const
	{
		return QiATan2(m[1], m[0]);
	}

	inline float getDeterminant() const
	{
		return m[0]*m[3] - m[2]*m[1];	
	}

	inline QiVec2 operator*(const QiVec2& v) const
	{
		return QiVec2(m[0]*v.x + m[2]*v.y, 
					  m[1]*v.x + m[3]*v.y);
	}
	
	QiMatrix2 operator*(const QiMatrix2& other) const;

	void operator*=(const QiMatrix2& other) { *this = *this * other; }
	
};

