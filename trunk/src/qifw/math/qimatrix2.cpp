/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "math/qimatrix2.h"


bool QiMatrix2::invert()
{
	float det = getDeterminant();
	if (det != 0.0f)
		det = 1.0f / det;
	else
		return false;
	float tmp[4];
	tmp[0] =  det * m[3];
	tmp[1] = -det * m[1];
	tmp[2] = -det * m[2];
	tmp[3] =  det * m[0];
	memcpy(m, tmp, sizeof(float)*4);
	return true;
}


QiMatrix2 QiMatrix2::operator*(const QiMatrix2& other) const
{
	QiMatrix2 result;
	result.m[0]=m[0]*other.m[0] + m[2]*other.m[1];
	result.m[2]=m[0]*other.m[2] + m[2]*other.m[3];
	result.m[1]=m[1]*other.m[0] + m[3]*other.m[1];
	result.m[3]=m[1]*other.m[2] + m[3]*other.m[3];
	return result;	
}


