/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "math/qivec2.h"
#include "math/qivec3.h"
#include "math/qirnd.h"
#include "base/qistring.h"

QiVec2 QiVec2::X(1.0f, 0.0f);
QiVec2 QiVec2::Y(0.0f, 1.0f);


QiVec2 QiVec2::random(float length)
{
	return normalize(QiVec2(QiRnd(-1.0f,1.0f), QiRnd(-1.0f,1.0f)))*length;
}


QiVec2::operator QiString() const
{
	return QiString("{") + x + ", " + y + "}";
}


QiVec3 QiVec2::vec3() const
{
	return QiVec3(x, y, 0.0f);
}
	
