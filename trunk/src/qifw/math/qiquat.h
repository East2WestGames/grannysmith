#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "math/qivec3.h"

class QiQuat
{
public:
	float x,y,z,w;

	inline QiQuat() : x(0.0f), y(0.0f), z(0.0f), w(1.0f)
	{
	}
	
	explicit inline QiQuat(QiUninitialized)
	{
	}

	inline QiQuat(float x, float y, float z, float w) : x(x), y(y), z(z), w(w)
	{
	}
	
	explicit inline QiQuat(const float f[4])
	{
		x=f[0]; 
		y=f[1]; 
		z=f[2];
		w=f[3];
	}
	
	inline QiQuat(const QiVec3& axis, float angle)
	{
		setAxisAngle(axis, angle);
	}
	
	inline QiQuat(const class QiMatrix3& matrix)
	{
		set(matrix);
	}

	inline void set(float x, float y, float z, float w)
	{
		this->x = x;
		this->y = y;
		this->z = z;
		this->w = w;
	}
	
	void set(const class QiMatrix3& matrix);
	
	inline QiQuat getNormalized() const
	{
		float square = x*x + y*y + z*z + w*w;
		if (square > 0.0f)
			return (*this) / QiSqrt(square);
		else
			return QiQuat();
	}

	inline void normalize()
	{
		(*this) = getNormalized();
	}

	inline QiQuat getConjugate() const
	{
		return QiQuat(-x, -y, -z, w);
	}

	inline QiVec3 rotate(const QiVec3& vec) const
	{
		QiVec3 v(x, y, z);
		float d = ::dot(v, vec);
		return vec * (2.0f*w*w-1.0f) + cross(v, vec)*(w*2.0f) + v*(d*2.0f);
	}

	inline QiVec3 rotateInv(const QiVec3& vec) const
	{
		QiVec3 v(x, y, z);
		float d = ::dot(v, vec);
		return vec * (2.0f*w*w-1.0f) - cross(v, vec)*(w*2.0f) + v*(d*2.0f);
	}

	inline QiQuat rotate(const QiQuat& quat) const
	{
		return (*this)*quat;
	}
	
	inline QiQuat rotateInv(const QiQuat& quat) const
	{
		return (*this).getConjugate()*quat;
	}
	
	void getAxisAngle(QiVec3& axis, float& angle) const;
	
	void setAxisAngle(const QiVec3& axis, float angle);
	
	void alignAxisX(const QiVec3& axis);
	
	QiVec3 getBase(int dim) const;

	inline QiVec3 getX() const
	{
		return QiVec3(1.0f - 2.0f*(y*y + z*z), 2.0f*(x*y + z*w), 2.0f*(x*z - y*w));
	}

	inline QiVec3 getY() const
	{
		return QiVec3(2.0f*(x*y - z*w), 1.0f - 2.0f*(x*x + z*z), 2.0f*(y*z + x*w));
	}

	inline QiVec3 getZ() const
	{
		return QiVec3(2.0f*(x*z + y*w), 2.0f*(y*z - x*w), 1.0f - 2.0f*(x*x + y*y));
	}

	inline QiQuat operator-() const
	{
		return QiQuat(-x, -y, -z, -w);
	}
	
	inline QiQuat operator*(const QiQuat& other) const
	{
		QiVec3 v(x, y, z);
		QiVec3 ov(other.x, other.y, other.z);
		QiVec3 v2 = cross(v, ov) + ov*w + v*other.w;
		float w2 = w*other.w - ::dot(v, ov);
		return QiQuat(v2.x, v2.y, v2.z, w2);
	}

	inline void operator*=(const QiQuat& other)
	{
		(*this) = (*this) * other;
	}

	inline QiQuat operator*(float scale) const
	{
		return QiQuat(x*scale, y*scale, z*scale, w*scale);
	}

	inline QiQuat operator/(float scale) const
	{
		return QiQuat(x/scale, y/scale, z/scale, w/scale);
	}

	inline void operator*=(float scale)
	{
		(*this) = (*this) * scale;
	}

	inline void operator/=(float scale)
	{
		(*this) = (*this) / scale;
	}

	operator QiString() const;

	inline float operator[](int index) const
	{
		QI_ASSERT(index >= 0 && index < 4, "Invalid index");
		return (&x)[index];
	}

	inline float& operator[](int index)
	{
		QI_ASSERT(index >= 0 && index < 4, "Invalid index");
		return (&x)[index];
	}

	inline float dot(const QiQuat& other) const
	{
		return x*other.x + y*other.y + z*other.z + w*other.w;
	}

	QiQuat log() const;
	QiQuat exp() const;

	static QiQuat slerp(const QiQuat& a, const QiQuat& b, float t, bool allowFlip=true);
	static QiQuat squad(const QiQuat& a, const QiQuat& tgA, const QiQuat& tgB, const QiQuat& b, float t);
	QiQuat getSquadTangent(const QiQuat& before, const QiQuat& after) const;
	static QiQuat random();
};



