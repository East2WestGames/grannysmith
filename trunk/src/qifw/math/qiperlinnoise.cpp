/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qimath.h"
#include "math/qiinterpolate.h"


static inline float noise(int x, int y)
{
	int n = x + y * 57;
    n = (n<<13) ^ n;
    return (1.0f - ( (n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0f);
}


static inline float smoothedNoise(int x, int y)
{
    float corners = ( noise(x-1, y-1) + noise(x+1, y-1) + noise(x-1, y+1) + noise(x+1, y+1) ) / 16.0f;
	float sides   = ( noise(x-1, y) + noise(x+1, y) + noise(x, y-1) + noise(x, y+1) ) /  8.0f;
	float center  =  noise(x, y) / 4.0f;
    return corners + sides + center;
}


static inline float interpolatedNoise(float x, float y)
{
	int integer_X = int(x);
	float fractional_X = x - integer_X;

	int integer_Y = int(y);
	float fractional_Y = y - integer_Y;

	float v1 = smoothedNoise(integer_X,     integer_Y);
	float v2 = smoothedNoise(integer_X + 1, integer_Y);
	float v3 = smoothedNoise(integer_X,     integer_Y + 1);
	float v4 = smoothedNoise(integer_X + 1, integer_Y + 1);

	float i1 = QiInterpolateCosine(v1 , v2 , fractional_X);
	float i2 = QiInterpolateCosine(v3 , v4 , fractional_X);

	return QiInterpolateCosine(i1 , i2 , fractional_Y);
}

  
float QiPerlinNoise(float x, float y, float frequency, float amplitude, int octaves)
{
	float total = 0;
	for(int i=0; i<octaves; i++)
	{
		float f = frequency * QiPow(2.0f, (float)i);
		float a = amplitude * QiPow(0.5f, (float)i);
		total += interpolatedNoise(x * f, y * f) * a;
	}
	return total;
}


void QiPerlinNoise(float* data, int width, int height, float frequency, float amplitude, int octaves)
{
	for(int y=0; y<height; y++)
	{
		for(int x=0; x<width; x++)
		{
			data[y*width+x] = QiPerlinNoise(x/(float)width, y/(float)height, frequency, amplitude, octaves);
		}
	}
}


void QiPerlinNoise(QiUInt8* data, int width, int height, float frequency, float amplitude, int octaves)
{
	for(int y=0; y<height; y++)
	{
		for(int x=0; x<width; x++)
		{
			float n = QiPerlinNoise(x/(float)width, y/(float)height, frequency, amplitude, octaves);
			data[y*width+x] = QiUInt8(QiClamp(0.5f + 0.5f*n, 0.0f, 1.0f) * 255.0f);
		}
	}
}


