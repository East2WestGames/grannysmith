/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "qitransform3.h"
#include "base/qistring.h"
#include "math/qitransform2.h"


QiTransform3::QiTransform3(const QiTransform2& transform2)
{
	pos = QiVec3(transform2.pos.x, transform2.pos.y, 0.0f);
	rot = QiQuat(QiVec3::Z, transform2.rot);
}


QiTransform3::operator QiString() const
{
	return QiString("{") + pos.x + ", " + pos.y + ", " + pos.z + " / " + rot.x + ", " + rot.y + ", " + rot.z + ", " + rot.w + "}";
}


