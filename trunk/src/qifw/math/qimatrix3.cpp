/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "math/qimatrix3.h"
#include "math/qitransform2.h"

QiMatrix3::QiMatrix3(const class QiTransform2& t)
{
	QiVec2 x(QiCos(t.rot), QiSin(t.rot));
	QiVec2 y = perpendicular(x);
	m[0] = x.x;
	m[1] = x.y;
	m[2] = 0.0f;
	m[3] = y.x;
	m[4] = y.y;
	m[5] = 0.0f;
	m[6] = t.pos.x;
	m[7] = t.pos.y;
	m[8] = 1.0f;
}


float QiMatrix3::getDeterminant() const
{
	return  m[0]*m[4]*m[8] + m[1]*m[5]*m[6] + m[2]*m[3]*m[7] -
			m[2]*m[4]*m[6] - m[1]*m[3]*m[8] - m[0]*m[5]*m[7];	
}


bool QiMatrix3::invert()
{
	float det = getDeterminant();
	if(det==0.0)
		return false;
	float tmp[9];
	float detInv=1.0f/det;
	tmp[0] = (m[4] * m[8] - m[7] * m[5]) * detInv;
	tmp[3] = -1 * (m[3] * m[8] - m[6] * m[5]) * detInv;
	tmp[6] = (m[3] * m[7] - m[6] * m[4]) * detInv;
	tmp[1] = -1 * (m[1] * m[8] - m[7] * m[2]) * detInv;
	tmp[4] = (m[0] * m[8] - m[6] * m[2]) * detInv;
	tmp[7] = -1 * (m[0] * m[7] - m[6] * m[1]) * detInv;
	tmp[2] = (m[1] * m[5] - m[4] * m[2]) * detInv;
	tmp[5] = -1 * (m[0] * m[5] - m[3] * m[2]) * detInv;
	tmp[8] = (m[0] * m[4] - m[3] * m[1]) * detInv;
	memcpy(m, tmp, sizeof(float)*9);
	return true;
}


QiMatrix3 QiMatrix3::operator*(const QiMatrix3& other) const
{
	QiMatrix3 result;
    result.m[0]=m[0]*other.m[0] + m[3]*other.m[1] + m[6]*other.m[2];
    result.m[3]=m[0]*other.m[3] + m[3]*other.m[4] + m[6]*other.m[5];
    result.m[6]=m[0]*other.m[6] + m[3]*other.m[7] + m[6]*other.m[8];
    result.m[1]=m[1]*other.m[0] + m[4]*other.m[1] + m[7]*other.m[2];
    result.m[4]=m[1]*other.m[3] + m[4]*other.m[4] + m[7]*other.m[5];
    result.m[7]=m[1]*other.m[6] + m[4]*other.m[7] + m[7]*other.m[8];
    result.m[2]=m[2]*other.m[0] + m[5]*other.m[1] + m[8]*other.m[2];
    result.m[5]=m[2]*other.m[3] + m[5]*other.m[4] + m[8]*other.m[5];
    result.m[8]=m[2]*other.m[6] + m[5]*other.m[7] + m[8]*other.m[8];
	return result;	
}


