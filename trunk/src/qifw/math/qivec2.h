#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qimath.h"

#define QI_ASSERT_VEC2_NORMALIZED(vec) { QI_ASSERT(vec.lengthSquared() > 0.999f && vec.lengthSquared() < 1.001f, "Vector not normalized");  }

class QiVec2
{
public:
	float x,y;

	inline QiVec2() { x=y=0.0f; }
	explicit inline QiVec2(QiUninitialized) { }
	inline QiVec2(float x, float y) : x(x), y(y) { }
	explicit inline QiVec2(const float e[2]) { x=e[0]; y=e[1]; }
	inline void set(float x, float y) { this->x=x; this->y=y; }
	inline float operator[](int i) const { return (&x)[i]; }
	inline float& operator[](int i) { return (&x)[i]; }

	inline QiVec2 operator+(const QiVec2& v) const { return QiVec2(x+v.x, y+v.y); }
	inline QiVec2 operator-(const QiVec2& v) const { return QiVec2(x-v.x, y-v.y); }
	inline void operator+=(const QiVec2& v) { x+=v.x; y+=v.y; }
	inline void operator-=(const QiVec2& v) { x-=v.x; y-=v.y; }
	inline QiVec2 operator-() const { return QiVec2(-x, -y); }

	inline void operator*=(float f) { x*=f; y*=f; } 
	inline void operator/=(float f) { x/=f; y/=f; } 	
	inline QiVec2 operator*(float f) const { return QiVec2(x*f, y*f); }
	inline QiVec2 operator/(float f) const { return QiVec2(x/f, y/f); }

	inline bool operator==(const QiVec2& vec) const { return x==vec.x && y==vec.y; }
	inline bool operator!=(const QiVec2 &vec) const { return x!=vec.x || y!=vec.y; }

	inline bool equals(const QiVec2& vec, float epsilon) const { return QiAbs(x-vec.x)<=epsilon && QiAbs(y-vec.y)<=epsilon; }

	inline float length() const { return QiSqrt(x*x + y*y); }
	inline float lengthSquared() const { return x*x + y*y; }

	class QiVec3 vec3() const;

	static QiVec2 random(float length=1.0f);
	static QiVec2 X;
	static QiVec2 Y;

	operator class QiString() const;
};


inline QiVec2 operator*(float f, const QiVec2& vec)
{
	return QiVec2(vec.x*f, vec.y*f);
}


inline QiVec2 operator/(float f, const QiVec2& vec)
{
	return QiVec2(vec.x/f, vec.y/f);
}


inline float length(const QiVec2& vec)
{
	return QiSqrt(vec.x*vec.x + vec.y*vec.y);
}


inline float lengthSquared(const QiVec2& vec)
{
	return vec.x*vec.x + vec.y*vec.y;
}


inline float manhattanDistance(const QiVec2& vec)
{
	return QiAbs(vec.x)+QiAbs(vec.y);
}


inline QiVec2 normalize(const QiVec2& vec)
{
	float l = length(vec);
	if (l > 0.0f)
		return vec / l;
	else
		return QiVec2(1.0f, 0.0f);
}


inline QiVec2 normalize(const QiVec2& vec, float& oldLength)
{
	oldLength = length(vec);
	if (oldLength > 0.0f)
		return vec / oldLength;
	else
		return QiVec2(1.0f, 0.0f);
}


inline QiVec2 normalizeFast(const QiVec2& vec)
{
	float rec = QiRSqrtFast(vec.x*vec.x + vec.y*vec.y);
	return QiVec2(vec.x*rec, vec.y*rec);
}


inline QiVec2 normalizeCarmack(const QiVec2& vec)
{
	float rec = QiRSqrtCarmack(vec.x*vec.x + vec.y*vec.y);
	return QiVec2(vec.x*rec, vec.y*rec);
}


inline QiVec2 multiplyPerElement(const QiVec2& a, const QiVec2& b)
{
	return QiVec2(a.x*b.x, a.y*b.y);
}


inline float cross(const QiVec2& a, const QiVec2& b) 
{
	return a.x*b.y-a.y*b.x;
}

	
inline float dot(const QiVec2 &a, const QiVec2& b)
{
	return a.x*b.x + a.y*b.y;
}
	
	
inline QiVec2 project(const QiVec2& normal, const QiVec2& vec)
{
	QI_ASSERT_VEC2_NORMALIZED(normal);
	return normal*dot(normal, vec);
}


inline float angle(const QiVec2& normal0, const QiVec2& normal1)
{		
	QI_ASSERT_VEC2_NORMALIZED(normal0);
	QI_ASSERT_VEC2_NORMALIZED(normal1);
	return QiACos(QiClamp(dot(normal0, normal1), -1.0f, 1.0f));
}


inline float signedAngle(const QiVec2& normal0, const QiVec2& normal1)
{		
	float ang = angle(normal0, normal1);
	return cross(normal0, normal1) > 0.0f ? ang : -ang;
}


inline QiVec2 perpendicular(const QiVec2& vec)
{
	return QiVec2(-vec.y, vec.x);
}


inline QiVec2 rotate(const QiVec2& vec, float angle)
{
	QiVec2 x(QiCos(angle), QiSin(angle));
	return QiVec2(x.x*vec.x - x.y*vec.y, x.y*vec.x + x.x*vec.y); 
}

