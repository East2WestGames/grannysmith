/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qistring.h"
#include "math/qiquat.h"
#include "math/qimatrix3.h"
#include "math/qirnd.h"

inline static QiQuat lnDif(const QiQuat& a, const QiQuat& b)
{
	QiQuat dif = a.getConjugate()*b;
	return dif.getNormalized().log();
}

void QiQuat::set(const QiMatrix3& matrix)
{
	float trace = matrix.m[0] + matrix.m[4] + matrix.m[8];
	if ( trace > 0.0f )
	{
		float s = QiSqrt( trace + 1.0f );
		w = s*0.5f;
		float recip = 0.5f/s;
		x = (matrix.m[5] - matrix.m[7])*recip;
		y = (matrix.m[6] - matrix.m[2])*recip;
		z = (matrix.m[4] - matrix.m[3])*recip;
	}
	else
	{
		unsigned int i = 0;
		if ( matrix.m[4] > matrix.m[0] )
			i = 1;
		if ( matrix.m[8] > matrix.m[i*3+i] )
			i = 2;
		unsigned int j = (i+1)%3;
		unsigned int k = (j+1)%3;
		float s = QiSqrt( matrix.m[i*3+i] - matrix.m[j*3+j] - matrix.m[k*3+k] + 1.0f );
		if (i == 0) {
			x = 0.5f * s;
		} else if (i == 1) {
			y = 0.5f * s;
		} else {
			z = 0.5f * s;
		}

		float recip = 0.5f/s;
		w = (matrix.m[j*3+k] - matrix.m[k*3+j])*recip;

		if (j == 0) {
			x = (matrix.m[i*3+j] + matrix.m[j*3+i])*recip;
		} else if (j == 1) {
			y = (matrix.m[i*3+j] + matrix.m[j*3+i])*recip;
		} else {
			z = (matrix.m[i*3+j] + matrix.m[j*3+i])*recip;
		}

		if (k == 0) {
			x = (matrix.m[i*3+k] + matrix.m[k*3+i])*recip;
		} else if (k == 1) {
			y = (matrix.m[i*3+k] + matrix.m[k*3+i])*recip;
		} else {
			z = (matrix.m[i*3+k] + matrix.m[k*3+i])*recip;
		}
	}
	normalize();
}

QiVec3 QiQuat::getBase(int dim) const
{
	QI_ASSERT(dim >= 0 && dim <=2, "Invalid base");
	switch(dim)
	{
		case 0:
			return getX();
		case 1:
			return getY();
		case 2:
			return getZ();
	}
	return QiVec3::X;
}

void QiQuat::getAxisAngle(QiVec3& axis, float& angle) const 
{
    angle = QiACos(w) * 2.0f;
    float sa = QiSqrt(1.0f - w*w);
	if (sa > 0.0f)
		axis.set(x/sa, y/sa, z/sa);
	else
		axis = QiVec3::X;
}

void QiQuat::setAxisAngle(const QiVec3& axis, float angle)
{
	QI_ASSERT_VEC3_NORMALIZED(axis);
	float half = angle*0.5f;
	float tmp = QiSin(half);
	x = axis.x * tmp;
	y = axis.y * tmp;
	z = axis.z * tmp;
	w = QiCos(half);
}

QiQuat QiQuat::log() const
{
	float len = QiSqrt(x*x + y*y + z*z);
	if (len < QI_FLOAT_EPSILON)
		return QiQuat(x, y, z, 0.0f);
	else
	{
		float coef = QiACos(w) / len;
		return QiQuat(x*coef, y*coef, z*coef, 0.0f);
	}
}

QiQuat QiQuat::exp() const
{
	float theta = QiSqrt(x*x + y*y + z*z);
	if (theta < QI_FLOAT_EPSILON)
		return QiQuat(x, y, z, QiCos(theta));
	else
	{
		float coef = QiSin(theta) / theta;
		return QiQuat(x*coef, y*coef, z*coef, QiCos(theta));
	}
}

QiQuat QiQuat::slerp(const QiQuat& a, const QiQuat& b, float t, bool allowFlip)
{
	float cosAngle = a.dot(b);

	float c1, c2;
	if ((1.0f - QiAbs(cosAngle)) < 0.01f)
	{
		// Linear interpolation for close orientations
		c1 = 1.0f - t;
		c2 = t;
	}
	else
	{
		// Spherical interpolation
		float angle    = QiACos(fabs(cosAngle));
		float sinAngle = QiSin(angle);
		c1 = QiSin(angle * (1.0f - t)) / sinAngle;
		c2 = QiSin(angle * t) / sinAngle;
	}

	// Use the shortest path
	if (allowFlip && (cosAngle < 0.0f))
		c1 = -c1;

	return QiQuat(c1*a[0] + c2*b[0], c1*a[1] + c2*b[1], c1*a[2] + c2*b[2], c1*a[3] + c2*b[3]);
}

QiQuat QiQuat::squad(const QiQuat& a, const QiQuat& tgA, const QiQuat& tgB, const QiQuat& b, float t)
{
	QiQuat ab = QiQuat::slerp(a, b, t);
	QiQuat tg = QiQuat::slerp(tgA, tgB, t, false);
	return QiQuat::slerp(ab, tg, 2.0f*t*(1.0f-t), false);
}

QiQuat QiQuat::getSquadTangent(const QiQuat& before, const QiQuat& after) const
{
	QiQuat l1 = lnDif(*this, before);
	QiQuat l2 = lnDif(*this, after);
	QiQuat e;
	for (int i=0; i<4; ++i)
		e[i] = -0.25f * (l1[i] + l2[i]);
	return (*this)*(e.exp());
}

QiQuat QiQuat::random()
{
	float seed = QiRnd(0.0f, 1.0f);
	float r1 = QiSqrt(1.0f - seed);
	float r2 = QiSqrt(seed);
	float t1 = 2.0f * QI_PI * (QiRnd(0.0f, 1.0f));
	float t2 = 2.0f * QI_PI * (QiRnd(0.0f, 1.0f));
	return QiQuat(QiSin(t1)*r1, QiCos(t1)*r1, QiSin(t2)*r2, QiCos(t2)*r2);
}

void QiQuat::alignAxisX(const QiVec3& axis)
{
	QiVec3 r=cross(QiVec3::X, axis);
	float angle;
	r = ::normalize(r, angle);
	setAxisAngle(r, angle);
}

QiQuat::operator QiString() const
{
	return QiString("{") + x + ", " + y + ", " + z + ", " + w + "}";
}

