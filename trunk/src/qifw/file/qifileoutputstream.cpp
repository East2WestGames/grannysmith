/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "file/qifileoutputstream.h"

QiFileOutputStream::QiFileOutputStream()
{
	mFile = NULL;
}
	
QiFileOutputStream::QiFileOutputStream(const char* fileName)
{
	open(fileName);
}
	
QiFileOutputStream::~QiFileOutputStream()
{
	close();
}

bool QiFileOutputStream::open(const char* fileName)
{
	mFile = fopen(fileName, "wb");
	mFileName = fileName;
	resetOutputStream();
	return mFile != NULL;
}

void QiFileOutputStream::close()
{
	if (mFile)
	{
		flush();
		fclose(mFile);
		mFile = NULL;
		mFileName = "";
	}
}

bool QiFileOutputStream::writeInternal(const char* buffer, size_t bytes)
{
	size_t writtenBytes = 0;
	while(writtenBytes < bytes) 
	{
		int ret = fwrite(buffer + writtenBytes, 1, bytes - writtenBytes, mFile);
		if (ret == 0)
			return false;
		writtenBytes += ret;
	}
	return true;
}


bool QiFileOutputStream::isOpen() const
{
	return mFile != NULL;
}

