#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include <sys/types.h>

#include "base/qistring.h"
#include "base/qiarray.h"

#ifdef QI_MACOS
#define off_t size_t
#endif

class QiPath
{
public:
	//Check if path exists (dir or file)
	static bool exists(const QiString& fileName);

	//Check if path is file
	static bool isFile(const QiString& path);

	//Check if path is directory
	static bool isDir(const QiString& path);

	//Return file size
	//Zero is returned if file does not exist or is dir
	static off_t getSize(const QiString& fileName);

	//List files (and folders) in given path
	//Returns false if path is file or non-existent
	static bool listDir(const QiString& path, QiArray<QiString>& output, bool includeDirs=true);

	//Create directory
	static bool createDir(const QiString& path);

	//Remove file or dir
	//Will return false if path is non-empty dir and non-recursive
	static bool remove(const QiString& path, bool recursive=false);

	//Move file or directory (recursively)
	//If new path is existing directory, path is NOT moved inside it
	static bool move(const QiString& oldPath, const QiString& newPath);

	//Return current working directory
	static QiString getWorkingDir();

	//Convert relative to absolute in current environment
	//Input path can be absolute or relative win/unix/smb and does not have to exist
	static QiString getAbsolutePath(const QiString& path);

	//Convert absolute to relative in current environment
	//Input path can be absolute or relative win/unix/smb and does not have to exist
	//Absolute paths that does not match current working directory will be
	//mangled into relative path
	static QiString getRelativePath(const QiString& path);

	//Extract file part of path (what's after last slash)
	//Method does not require path to exist
	static QiString getFilePart(const QiString& path);

	//Extract dir part of path (what's before last slash)
	//Method does not require path to exist
	static QiString getDirPart(const QiString& path);
};


