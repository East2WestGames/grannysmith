#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qistream.h"
#include "base/qistring.h"

#include <stdio.h>

class QiFileInputStream : public QiInputStream
{
public:
	QiFileInputStream();
	QiFileInputStream(const char* fileName);
	virtual ~QiFileInputStream();	
	virtual bool hasMoreData();

	bool open(const char* fileName);
	bool openLeanAndMean(const char* fileName);
	void close();
	bool isOpen() const;
	int getSize() const;
	inline QiString getFileName() const { return mFileName; }

protected:
	virtual bool readInternal(char* buffer, size_t bytes);
	
private:
	FILE* mFile;
	QiString mFileName;
	int mFileSize;
	int mReadBytes;
	void* mAndroidAsset;
};


