#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qistream.h"
#include "base/qistring.h"

#include <stdio.h>

class QiFileOutputStream : public QiOutputStream
{
public:
	QiFileOutputStream();
	QiFileOutputStream(const char* fileName);
	~QiFileOutputStream();

	bool open(const char* fileName);
	void close();
	bool isOpen() const;
	inline QiString getFileName() const { return mFileName; }

protected:
	virtual bool writeInternal(const char* buffer, size_t bytes);
		
private:
	FILE* mFile;
	QiString mFileName;
};

