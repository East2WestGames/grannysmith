/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"
#include "file/qipath.h"
#include "file/qifile.h"

#ifdef QI_WIN32
#include <windows.h>
#else
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stddef.h>
#include <dirent.h>
#endif

#if defined(QI_ANDROID)
#include "file/qifileinputstream.h"
#endif

static inline QiString normalize(const QiString& path)
{
	QiString newPath;
	if (path.startsWith("\\\\"))
		newPath = QiString("\\\\") + path.substring(2).replace("\\", "/");
	else
		newPath = path.replace("\\", "/");
	while(newPath.contains("//"))
		newPath = newPath.replace("//", "/");
	if (newPath.endsWith("/"))
		newPath = newPath.substring(0, newPath.getLength()-1);
	return newPath;
}

inline QiString getDirName(const QiString& path)
{
	QiString tmp = path;
	if (!tmp.endsWith("/."))
	{
		if (!tmp.endsWith("/"))
			tmp += "/";
		tmp += ".";
	}
	return tmp;
}


off_t QiPath::getSize(const QiString& fileName)
{
	if (!isFile(fileName))
		return 0;
	QiFile tmp(fileName, QiFile::READ);
	return tmp.getSize();
}

bool QiPath::isDir(const QiString& fileName)
{
#ifdef QI_WIN32
	DWORD ret = ::GetFileAttributes(fileName.c_str());
	if (ret == INVALID_FILE_ATTRIBUTES)
		return false;
	return (ret & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY;
#else
	QiString tmp = getDirName(fileName);
	struct stat buf;
	if (lstat(tmp.c_str(), &buf)!=0)
		return false;
	return S_ISDIR(buf.st_mode);
#endif
}


bool QiPath::isFile(const QiString& fileName)
{
#ifdef QI_WIN32
	DWORD attrib = ::GetFileAttributes(fileName.c_str());
	if (attrib == INVALID_FILE_ATTRIBUTES)
		return false;
	if ((attrib & FILE_ATTRIBUTE_DEVICE) != 0)
		return false;
	return (attrib & FILE_ATTRIBUTE_DIRECTORY) == 0;
#else
	struct stat buf;
	if (lstat(fileName.c_str(), &buf)!=0)
		return false;
	return S_ISREG(buf.st_mode);
#endif
}


bool QiPath::exists(const QiString& fileName)
{
#if defined(QI_WIN32)
	return ::GetFileAttributes(fileName.c_str()) != INVALID_FILE_ATTRIBUTES;	
#elif defined(QI_ANDROID)
	QiFileInputStream s(fileName);
	return s.isOpen();
#else
	return ::access(fileName.c_str(), F_OK)==0;
#endif
}

bool QiPath::createDir(const QiString &path)
{
	QiString parent = getDirPart(path);
	if (parent != "" && !isDir(parent) && !isFile(parent))
		createDir(parent);
#ifdef QI_WIN32
	return ::CreateDirectory(path.c_str(), NULL)!=0;
#else
	return ::mkdir(path.c_str(), S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH)==0;
#endif
}


bool QiPath::move(const QiString& srcPath, const QiString& dstPath)
{
#ifdef QI_WIN32
	return ::MoveFile(srcPath.c_str(), dstPath.c_str())!=0;
#else
	return ::rename(srcPath.c_str(), dstPath.c_str())==0;
#endif
}

bool QiPath::listDir(const QiString& path, QiArray<QiString>& output, bool includeDirs)
{
	QiString newPath = normalize(path) + "/";

	if (!isDir(newPath))
		return false;

#ifdef QI_WIN32
	WIN32_FIND_DATA findData;
	::ZeroMemory(&findData, sizeof(findData));
	HANDLE hFind = ::FindFirstFile( (newPath + "*.*").c_str(), &findData );
	if ( hFind == INVALID_HANDLE_VALUE )
		return false;
	do
	{
		QiString fileName = findData.cFileName;
		if (fileName == "." || fileName == "..")
			continue;
		if (includeDirs || isFile(newPath + fileName))
			output.add(fileName);
	}
	while (::FindNextFile(hFind, &findData));
	::FindClose( hFind );
	return true;
#else
	DIR *dp;
	struct dirent *ep;
	dp = ::opendir (getDirName(path).c_str());
	if (dp == NULL)
		return false;
	while ((ep = ::readdir (dp)))
	{
		QiString fileName = ep->d_name;
		if (fileName == "." || fileName == "..")
			continue;
		if (includeDirs || isFile(newPath + fileName))
			output.add(fileName);
	}
	::closedir (dp);
	return true;
#endif
}

static inline bool removeFile(const QiString& path)
{
#ifdef QI_WIN32
	return ::DeleteFile(path.c_str()) != 0;
#else
	return ::unlink(path.c_str()) == 0;
#endif
}

static inline bool removeDir(const QiString& path)
{
#ifdef QI_WIN32
	return ::RemoveDirectory(path.c_str()) != 0;
#else
	return ::rmdir(path.c_str()) == 0;
#endif
}

bool QiPath::remove(const QiString& path, bool recursive)
{
	if (isDir(path))
	{
		QiArray<QiString> arr;
		if (!listDir(path, arr, true))
			return false;
		if (recursive)
		{
			for(int i=0; i<arr.getCount(); i++)
			{
				if (!remove(path + "/" + arr[i], true))
					return false;
			}
		}
		else if (arr.getCount() > 0)
			return false;
		return removeDir(path);
	}
	else if (isFile(path))
		return removeFile(path);
	else
		return false;
}

QiString QiPath::getWorkingDir()
{
	char tmp[256]; 
#ifdef QI_WIN32
	if (::GetCurrentDirectory(sizeof(tmp), tmp) == 0)
		return normalize("c:");
	else
		return normalize(tmp);
#else
	if (::getcwd(tmp, sizeof(tmp)) == NULL)
		return normalize("/");
	else
		return normalize(tmp);
#endif
}


QiString QiPath::getAbsolutePath(const QiString& path)
{
	if (path.contains(":") || path.startsWith("\\\\") || path.startsWith("/"))
		return normalize(path);
	else
		return getWorkingDir() + "/" + normalize(path);
}

QiString QiPath::getRelativePath(const QiString& path)
{
	if (path.contains(":") || path.contains("\\\\") || path.startsWith("/"))
	{
		QiString cwd = getWorkingDir();
		QiString newPath = normalize(path);
		if (newPath.startsWith(cwd + "/"))
			return newPath.substring(cwd.getLength() + 1);
		else
		{
			newPath = newPath.replace(":", "");
			newPath = newPath.replace("\\\\", "");
			while(newPath.startsWith("/"))
				newPath = newPath.substring(1);
			return newPath;
		}
	}
	else
		return normalize(path);
}


QiString QiPath::getFilePart(const QiString& path)
{
	if (path.endsWith("/") || path.endsWith("\\"))
		return "";
	QiString newPath = normalize(path);
	int index = newPath.getLastIndexOf("/");
	if (index == -1)
		return newPath;
	else
		return newPath.substring(index+1);
}

QiString QiPath::getDirPart(const QiString& path)
{
	if (path.endsWith("/") || path.endsWith("\\"))
		return normalize(path);
	QiString newPath = normalize(path);
	int index = newPath.getLastIndexOf("/");
	if (index == -1)
		return "";
	else
		return newPath.substring(0, index);
}

