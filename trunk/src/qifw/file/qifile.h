#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include <stdio.h>
#include <sys/types.h>
#include "base/qistring.h"

class QiFile
{
public:
	enum Mode
	{
		READ,
		WRITE
	};
	
	QiFile();
	QiFile(const char* name, Mode mode = READ);
	virtual ~QiFile();

	virtual bool open(const char* name, Mode mode = READ);
	virtual void close();
	inline bool isOpen() const {return mFile!=0;}

	virtual off_t getSize() const;
	virtual void truncate();
	virtual void grow(off_t newSize);

	virtual void read(void* buf, size_t size) const;
	virtual void write(const void* buf, size_t size);

	virtual void read(off_t offset, void* buf, size_t size) const;
	virtual void write(off_t offset, const void* buf, size_t size);

protected:
	int mFile;
	Mode mMode;
};

