/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"
#include "base/qidebug.h"
#include "file/qifile.h"

#ifdef QI_WIN32
#include <io.h>
#include <shlobj.h>
#include "base/qisync.h"
static QiMutex crtMutex;
#else
#include <pwd.h>
#include <unistd.h>
#endif

#include <fcntl.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>

QiFile::QiFile()
{
	mFile = 0;
}


QiFile::QiFile(const char* name, Mode mode)
{
	mFile = 0;
	mMode = mode;
	open(name, mode);
}


QiFile::~QiFile()
{
	if (mFile)
	{
#ifdef QI_WIN32
		::_close(mFile);
#else
		::close(mFile);
#endif
	}
}


bool QiFile::open(const char* name, Mode mode)
{
	mMode = mode;
	if (mode == READ)
	{
#ifdef QI_WIN32
		mFile = ::_open(name, _O_RDONLY | _O_BINARY, -1);
		if (mFile==-1)
			mFile=0;
#else
		mFile = ::open(name, O_RDONLY, -1);
#endif
	}
	else
#ifdef QI_WIN32
		mFile = ::_open(name, _O_CREAT | _O_RDWR | _O_BINARY, -1);
		if (mFile==-1)
			mFile=0;
#else
		mFile = ::open(name, O_CREAT | O_RDWR, -1);
#endif
	return mFile != -1;
}


void QiFile::close()
{
	if (!mFile)
		return;
	::close(mFile);
	mFile = 0;
}

off_t QiFile::getSize() const
{
	if (!isOpen())
		return 0;
#ifdef QI_WIN32
	return ::_filelength(mFile);
#else
	struct stat s;
	::fstat(mFile, &s);
	return s.st_size;	
#endif
}


void QiFile::truncate()
{
	QI_ASSERT(mMode == WRITE, "Cannot truncate read-only file");
	if (!mFile)
		return;
#ifdef QI_WIN32
	::_chsize(mFile, 0);
#else
	::ftruncate(mFile, 0);
#endif
}


void QiFile::grow(off_t newSize)
{
	QI_ASSERT(mMode == WRITE, "Cannot grow read-only file");
	if (!mFile)
		return;
#ifdef QI_WIN32
	::_chsize(mFile, newSize);
#else
	::ftruncate(mFile, newSize);
#endif
}


void QiFile::read(void* buf, size_t size) const
{
#ifdef QI_WIN32
	::_read(mFile, buf, size);
#else
	::read(mFile, buf, size);
#endif
}


void QiFile::write(const void* buf, size_t size)
{
	QI_ASSERT(mMode == WRITE, "Cannot write to read-only file");
#ifdef QI_WIN32
	::_write(mFile, buf, size);
#else
	::write(mFile, buf, size);
#endif
}


void QiFile::read(off_t offset, void* buf, size_t size) const
{
#ifdef QI_WIN32
	crtMutex.lock();
	long oldPos = ::tell(mFile);
	::lseek(mFile, offset, SEEK_SET);
	::read(mFile, buf, size);
	::lseek(mFile, oldPos, SEEK_SET);
	crtMutex.unlock();
#else
	::pread(mFile, buf, size, offset);
#endif
}


void QiFile::write(off_t offset, const void* buf, size_t size)
{
	QI_ASSERT(mMode == WRITE, "Cannot write to read-only file");
#ifdef QI_WIN32
	crtMutex.lock();
	long oldPos = ::tell(mFile);
	::lseek(mFile, offset, SEEK_SET);
	::write(mFile, buf, size);
	::lseek(mFile, oldPos, SEEK_SET);
	crtMutex.unlock();
#else
#ifdef QI_ANDROID
	::pwrite(mFile, (void*)buf, size, offset);
#else
	::pwrite(mFile, buf, size, offset);
#endif
#endif
}




