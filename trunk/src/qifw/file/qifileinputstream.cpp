/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "file/qifileinputstream.h"
#include "file/qifile.h"

#if defined(QI_ANDROID)
#include <android/asset_manager.h>
#include <unistd.h>
extern AAssetManager* gAndroidAssetManager;
#endif

QiFileInputStream::QiFileInputStream()
{
	mAndroidAsset = NULL;
	mFile = NULL;
	mFileSize = 0;
	mReadBytes = 0;
}
	
QiFileInputStream::QiFileInputStream(const char* fileName)
{
	mAndroidAsset = NULL;
	mFile = NULL;
	mFileSize = 0;
	mReadBytes = 0;
	open(fileName);
}
	
	
QiFileInputStream::~QiFileInputStream()
{
	close();
}
#include "base/qidebug.h"
bool QiFileInputStream::open(const char* fileName)
{
	mFileName = fileName;
#if defined(QI_ANDROID)
	off_t fileSize=0;
	off_t fileOffset=0;
	mAndroidAsset = AAssetManager_open(gAndroidAssetManager, fileName, AASSET_MODE_UNKNOWN);
	if (!mAndroidAsset)
		return false;
	int fd = AAsset_openFileDescriptor((AAsset*)mAndroidAsset, &fileOffset, &fileSize);
	if (fd >= 0)
	{
		mFile = fdopen(dup(fd),"r");
		::close(fd);
		if (mFile)
		{
			mFileSize = fileSize;
			mReadBytes = 0;
			fseek(mFile, fileOffset, SEEK_SET);
			return true;
		}
	}
	return false;
#else
	mFile = fopen(fileName, "rb");
#endif
	resetInputStream();
	return mFile != NULL;
}

bool QiFileInputStream::openLeanAndMean(const char* fileName)
{
	mFileName = fileName;
#if defined(QI_ANDROID)
	{
		QiFile tmp(fileName);
		if (tmp.isOpen())
			mFileSize = tmp.getSize();
		else 
			mFileSize = 0;
	}
	mAndroidAsset = NULL;
#endif
	mFile = fopen(fileName, "rb");
	resetInputStream();
	return mFile != NULL;
}

int QiFileInputStream::getSize() const
{
#if defined(QI_ANDROID)
	return mFileSize;
#else
	return QiFile(mFileName).getSize();
#endif
}


void QiFileInputStream::close()
{
	if (mFile)
	{
		fclose(mFile);
		mFile = NULL;
		mFileName = "";
		mFileSize = 0;
#if defined(QI_ANDROID)
		if (mAndroidAsset)
		{
			AAsset_close((AAsset*)mAndroidAsset);
			mAndroidAsset = NULL;
		}
#endif
	}
}

bool QiFileInputStream::hasMoreData()
{
#if defined(QI_ANDROID)
	return mReadBytes < mFileSize;
#else
	return feof(mFile)==0;
#endif
}

bool QiFileInputStream::readInternal(char* buffer, size_t bytes)
{
#if defined(QI_ANDROID)
	if (mReadBytes+bytes > mFileSize)
		return false;
#endif
	size_t readBytes = 0;
	while(readBytes < bytes) 
	{
		int ret = fread(buffer + readBytes, 1, bytes - readBytes, mFile);
		if (ret == 0)
			return false;
		readBytes += ret;
		mReadBytes += ret;
	}
	return true;
}

bool QiFileInputStream::isOpen() const
{
	return mFile != NULL;
}

