/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qidebug.h"
#include "simpleapp/qisimpleapp.h"
#include "gfx/qigl.h"
#include "gfx/qimesh.h"
#include "file/qifileinputstream.h"
#include "file/qifileoutputstream.h"


void QiSimpleApp::init()
{
#ifndef QI_MACOS
	glewInit();
#endif
	reshape(mWidth, mHeight, 1000);
	mQuit = false;
	mCameraTransform.pos.set(0,10,10);
	mCameraSpeed = 0.01f;
	
	mTrapArrowKeys = true;
	mTrapEscapeKey = true;
	mTrapMouse = true;

	glClearColor(0,0,0,0);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);

	GLfloat lightdiffuse[]={1,1,1,1};
	GLfloat lightspecular[]={1,1,1,1};
	GLfloat lightambient[]={0, 0, 0, 1};
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightdiffuse);
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightambient);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightspecular);
	glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 1.0f);
	glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.0);
	glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0.0);
}

void QiSimpleApp::reshape(int width, int height, float farClipping) 
{
	mWidth = width;
	mHeight = height;

	mViewport.setBounds(0, 0, mWidth, mHeight);
	mViewport.setMode3D(90, 0.05f, farClipping);

	mViewport.push();
	mViewport.setFixedAspectRatio(1.0f);
	mViewport.setMode2D(40, -100, 100);
	mViewport.pop();
}


void QiSimpleApp::setCameraTransform(const QiTransform3& t)
{
	mCameraTransform = t;
}


const QiTransform3& QiSimpleApp::getCameraTransform() const 
{
	return mCameraTransform;
}


void QiSimpleApp::beginRender()
{
	mViewport.setCameraPos(mCameraTransform.pos);
	mViewport.setCameraRot(mCameraTransform.rot);
	mViewport.apply();

	glClearColor(0,0,0,0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	QiVec3 dir(0,0,1);
	
	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, &dir.x);
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);

	float light = 0.5f;
	GLfloat white[]={0.8f*light, 0.8f*light, light, 1.0f};
	GLfloat black[]={0.0f,0.0f,0.0f,1.0f};
	GLfloat grey[]={0.1f,0.1f,0.1f,1.0f};
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, white);
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, grey);
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, grey);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, black);
	glMateriali(GL_FRONT_AND_BACK, GL_SHININESS, 50);
	glColorMaterial(GL_FRONT_AND_BACK, GL_DIFFUSE);
	glEnable(GL_COLOR_MATERIAL);
}

void QiSimpleApp::renderMesh(const QiMesh& mesh, bool flat, bool back)
{
	glEnable(GL_NORMALIZE);
	glBegin(GL_TRIANGLES);
	for(int f=0; f<mesh.getFaceCount(); f++)
	{
		QiVec3 n;
		const QiMesh::Face& face = mesh.getFace(f);
		if (flat)
		{
			const QiVec3& p0 = mesh.getVertex(face.vertices[0]).point;
			const QiVec3& p1 = mesh.getVertex(face.vertices[1]).point;
			const QiVec3& p2 = mesh.getVertex(face.vertices[2]).point;
			n = triangleNormal(p0, p1, p2);
		}
		if (back)
		{
			glNormal3f(-n.x, -n.y, -n.z);
			for(int v=2; v>=0; v--)
			{
				const QiVec3& p = mesh.getVertex(face.vertices[v]).point;
				if (!flat)
				{
					const QiVec3& n = mesh.getVertex(face.vertices[v]).normal;
					glNormal3f(-n.x, -n.y, -n.z);
				}
				glVertex3f(p.x, p.y, p.z);				
			}	
		}
		else
		{
			glNormal3f(n.x, n.y, n.z);
			for(int v=0; v<3; v++)
			{
				const QiVec3& p = mesh.getVertex(face.vertices[v]).point;
				if (!flat)
				{
					const QiVec3& n = mesh.getVertex(face.vertices[v]).normal;
					glNormal3f(n.x, n.y, n.z);
				}
				glVertex3f(p.x, p.y, p.z);				
			}
		}	
	}
	glEnd();
	
	//Draw wireframe
	#if 0
	glDisable(GL_LIGHTING);
	glColor4f(1,1,1,0.5f);
	for(int f=0; f<mesh.getFaceCount(); f++)
	{
		glBegin(GL_LINE_STRIP);
		const QiMesh::Face& face = mesh.getFace(f);
		for(int v=0; v<3; v++)
		{
			const QiVec3& p = mesh.getVertex(face.vertices[v]).point;
			glVertex3f(p.x, p.y, p.z);				
		}
		glEnd();
	}
	glEnable(GL_LIGHTING);
	#endif
}


QiVec3 QiSimpleApp::getPixelDirection(int x, int y) const 
{
	return mViewport.getPixelDirection(QiVec2((float)x, (float)y));
}


void QiSimpleApp::processInput()
{
	//Update the mInput object
	mInput.registerBegin();
	processInputInternal();
	mInput.registerEnd();

	mViewport.pick(mInput.getMousePosX(), mInput.getMousePosY(), 1);

	bool shift = mInput.isKeyDown(QI_KEY_SHIFT);
	if (mTrapArrowKeys)
	{
		if (mInput.isKeyDown(QI_KEY_UP))
			mCameraAcc += QiVec3(0, 0, shift ? -mCameraSpeed*10 : -mCameraSpeed);
		if (mInput.isKeyDown(QI_KEY_DOWN))
			mCameraAcc += QiVec3(0, 0, shift ? mCameraSpeed*10 : mCameraSpeed);
		if (mInput.isKeyDown(QI_KEY_LEFT))
			mCameraAcc += QiVec3(shift ? -mCameraSpeed*10 : -mCameraSpeed, 0, 0);
		if (mInput.isKeyDown(QI_KEY_RIGHT))
			mCameraAcc += QiVec3(shift ? mCameraSpeed*10 : mCameraSpeed, 0, 0);
	}
	mCameraTransform.pos += mCameraTransform.rot.getBase(0) * mCameraAcc.x;
	mCameraTransform.pos += mCameraTransform.rot.getBase(1) * mCameraAcc.y;
	mCameraTransform.pos += mCameraTransform.rot.getBase(2) * mCameraAcc.z;
	mCameraAcc *= 0.7f;

	if (mTrapMouse)
	{
		if (mInput.isButtonDown(QI_BUTTON_LEFT) && (mInput.getMouseDiffX()!=0 || mInput.getMouseDiffY()!=0))
		{
			QiQuat q(QiVec3::X, -mInput.getMouseDiffY()*0.003f);
			mCameraTransform.rot *= q;
			float rotUpAngle = angle(mCameraTransform.rot.getBase(1), QiVec3::Y);
			if (mCameraTransform.rot.getBase(2).y < 0)
				rotUpAngle = -rotUpAngle;
			QiQuat rotUp(QiVec3::X, rotUpAngle);
			mCameraTransform.rot *= rotUp;
			q.setAxisAngle(QiVec3::Y, -mInput.getMouseDiffX()*0.003f);
			mCameraTransform.rot *= q;
			rotUp.setAxisAngle(QiVec3::X, -rotUpAngle);
			mCameraTransform.rot *= rotUp;
		}
	}

	if (mTrapEscapeKey && mInput.wasKeyPressed(QI_KEY_ESCAPE))
		mQuit = true;
}


bool QiSimpleApp::shouldQuit()
{
	return mQuit;
}


void QiSimpleApp::signalQuit()
{
	mQuit = true;
}


void QiSimpleApp::setCameraSpeed(float speed)
{
	mCameraSpeed = speed;
}


int QiSimpleApp::getWidth()
{
	return mWidth;
}


int QiSimpleApp::getHeight()
{
	return mHeight;
}


void QiSimpleApp::trapArrowKeys(bool enable)
{
	mTrapArrowKeys = enable;
}


void QiSimpleApp::trapEscapeKey(bool enable)
{
	mTrapEscapeKey = enable;
}


void QiSimpleApp::trapMouse(bool enable)
{
	mTrapMouse = enable;
}

