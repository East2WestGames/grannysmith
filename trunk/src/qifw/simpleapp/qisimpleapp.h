#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "math/qivec3.h"
#include "math/qitransform3.h"
#include "gfx/qiviewport.h"
#include "app/qiinput.h"

class QiMesh;

class QiSimpleApp 
{
public:

	QiSimpleApp(const char* title, int width, int height, bool fullscreen=false);
	~QiSimpleApp();

	void reshape(int width, int height, float farClipping);
	void setCameraTransform(const QiTransform3& transform);
	const QiTransform3& getCameraTransform() const;
	void setCameraSpeed(float speed=0.03f);
	QiVec3 getPixelDirection(int x, int y) const;

	void beginRender();
	void endRender();

	static void renderMesh(const QiMesh& mesh, bool flat=false, bool back=false);

	void processInput();
	bool shouldQuit();
	void signalQuit();

	void init();
	void processInputInternal();
	
	int getWidth();
	int getHeight();
	
	void trapArrowKeys(bool enable);
	void trapEscapeKey(bool enable);
	void trapMouse(bool enable);

	class QiSimpleAppImpl* mImpl;
	int mWidth;
	int mHeight;
	bool mQuit;
	bool mTrapArrowKeys;
	bool mTrapEscapeKey;
	bool mTrapMouse;
	
	QiTransform3 mCameraTransform;
	QiVec3 mCameraAcc;
	float mCameraSpeed;

	QiInput mInput;
	QiViewport mViewport;
};


