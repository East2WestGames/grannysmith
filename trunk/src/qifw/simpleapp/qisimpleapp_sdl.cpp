/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"

#ifdef QI_USE_SDL

#include "base/qidebug.h"
#include "simpleapp/qisimpleapp.h"
#include "gfx/qigl.h"

#ifdef WIN32
#include <windows.h>
#endif

#ifdef QI_MACOS
#include <SDL/SDL.h>
#else
#include "SDL.h"
#endif

#include "gfx/qimesh.h"


class QiSimpleAppImpl
{
public:
	SDL_Surface* screen;
};


QiButton getButton(int sdlButton)
{
	if (sdlButton == 1)
		return QI_BUTTON_LEFT;
	else if (sdlButton == 3)
		return QI_BUTTON_RIGHT;
	else if (sdlButton == 2)
		return QI_BUTTON_MIDDLE;
	else
		return QI_BUTTON_UNDEFINED;
}


QiKey getKey(int sdlKey)
{
	if (sdlKey == SDLK_ESCAPE)
		return QI_KEY_ESCAPE;
	else if (sdlKey == SDLK_UP)
		return QI_KEY_UP;
	else if (sdlKey == SDLK_DOWN)
		return QI_KEY_DOWN;
	else if (sdlKey == SDLK_DOWN)
		return QI_KEY_DOWN;
	else if (sdlKey == SDLK_LEFT)
		return QI_KEY_LEFT;
	else if (sdlKey == SDLK_RIGHT)
		return QI_KEY_RIGHT;
	else if (sdlKey == SDLK_LSHIFT)
		return QI_KEY_LSHIFT;
	else if (sdlKey == SDLK_RSHIFT)
		return QI_KEY_RSHIFT;
	else if (sdlKey == SDLK_DELETE)
		return QI_KEY_DELETE;
	else if (sdlKey == SDLK_BACKSPACE)
		return QI_KEY_BACKSPACE;
	else if (sdlKey == SDLK_RETURN)
		return QI_KEY_RETURN;
	else if (sdlKey == SDLK_SPACE)
		return QI_KEY_SPACE;
	else if (sdlKey < QI_MAX_KEYS)
		return (QiKey)sdlKey;
	else
		return QI_KEY_UNDEFINED;
}


QiSimpleApp::QiSimpleApp(const char* title, int width, int height, bool fullscreen) :
mImpl(NULL), mWidth(width), mHeight(height)
{
	mImpl = QI_NEW QiSimpleAppImpl();

	// Open a window with an OpenGL context. Please check SDL manual for details.
	SDL_Init(SDL_INIT_VIDEO);
	atexit(SDL_Quit);
	
	SDL_WM_SetCaption(title, NULL);
	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 16);
	SDL_GL_SetAttribute( SDL_GL_STENCIL_SIZE, 8);
	mImpl->screen = SDL_SetVideoMode( width, height, 32, SDL_OPENGL | (SDL_FULLSCREEN * fullscreen));
	init();
}


QiSimpleApp::~QiSimpleApp()
{
	SDL_Quit();
	QI_DELETE(mImpl);
}


void QiSimpleApp::endRender()
{
	glFlush();
	SDL_GL_SwapBuffers();
}


void QiSimpleApp::processInputInternal()
{
	SDL_Event event;	
	while(SDL_PollEvent(&event)) 
	{
		QiButton b = getButton(event.button.button);
		QiKey k = getKey(event.key.keysym.sym);
		switch(event.type) 
		{
		case SDL_QUIT :
			mQuit = true;
			break;
		case SDL_MOUSEMOTION:
			mMouseX = event.motion.x;
			mMouseY = event.motion.y;
			mMouseDiffX = event.motion.xrel;
			mMouseDiffY = event.motion.yrel;
			break;
		case SDL_MOUSEBUTTONDOWN:
			mMousePressed[b] = true;
			mMouseDown[b] = true;
			break;
		case SDL_MOUSEBUTTONUP :
			mMouseReleased[b] = true;
			mMouseDown[b] = false;
			break;
		case SDL_KEYDOWN:
			mKeyPressed[k] = true;
			mKeyDown[k] = true;
			break;
		case SDL_KEYUP:
			mKeyReleased[k] = true;
			mKeyDown[k] = false;
			break;
		}
	}
}


#endif
