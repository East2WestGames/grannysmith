/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"

#ifdef QI_MACOS
#ifndef QI_USE_SDL

#include "simpleapp/qisimpleapp.h"

#include <OpenGl/gl.h>
#include <OpenGl/glu.h>
#include <OpenGL/glext.h>
#include <OpenGL/OpenGL.h>
#include <Carbon/Carbon.h>

#ifndef FULLSCREEN
#include <AGL/agl.h>
#endif



QiButton getButton(int b)
{
	if (b == 1)
		return QI_BUTTON_LEFT;
	else if (b == 2)
		return QI_BUTTON_RIGHT;
	else if (b == 3)
		return QI_BUTTON_MIDDLE;
	else
		return QI_BUTTON_UNDEFINED;
}


QiKey getKey(int key, int asc)
{
	if (key == 0x35)
		return QI_KEY_ESCAPE;
	else if (key == 0x7e)
		return QI_KEY_UP;
	else if (key == 0x7d)
		return QI_KEY_DOWN;
	else if (key == 0x7b)
		return QI_KEY_LEFT;
	else if (key == 0x7c)
		return QI_KEY_RIGHT;
	else if (key == 0x33)
		return QI_KEY_BACKSPACE;
	else if (key == 0x24)
		return QI_KEY_RETURN;
	else if (key == 0x31)
		return (QiKey)' ';
	else if (key == 115)
		return QI_KEY_HOME;
	else if (key == 119)
		return QI_KEY_END;
	else if (key == 53)
		return QI_KEY_TAB;
	else if (key == 116)
		return QI_KEY_PGUP;
	else if (key == 121)
		return QI_KEY_PGDOWN;
	else if (key == 122)
		return QI_KEY_F1;
	else if (key == 120)
		return QI_KEY_F2;
	else if (key == 99)
		return QI_KEY_F3;
	else if (key == 118)
		return QI_KEY_F4;
	else if (key == 96)
		return QI_KEY_F5;
	else if (key == 97)
		return QI_KEY_F6;
	else if (key == 98)
		return QI_KEY_F7;
	else if (asc < QI_MAX_KEYS)
		return (QiKey)asc;
	else
		return QI_KEY_UNDEFINED;
}


static pascal OSErr quitEventHandler( const AppleEvent *appleEvt, AppleEvent *reply, SInt32 refcon )
{
	((QiSimpleApp*)refcon)->mQuit = true;
	return false;
}


class QiSimpleAppImpl
{
public:
	WindowRef window;
	AGLContext context;
	CGLContextObj contextObj;
	bool fullscreen;
};


pascal OSStatus keyboardEventHandler( EventHandlerCallRef nextHandler, EventRef event, void * userData )
{
	UInt32 eventClass = GetEventClass( event );
	UInt32 eventKind = GetEventKind( event );
	QiSimpleApp* app = (QiSimpleApp*)userData;
	if ( eventClass == kEventClassKeyboard )
	{
		char macCharCodes;
		UInt32 macKeyCode;
		UInt32 macKeyModifiers;

		GetEventParameter( event, kEventParamKeyMacCharCodes, typeChar, NULL, sizeof(macCharCodes), NULL, &macCharCodes );
		GetEventParameter( event, kEventParamKeyCode, typeUInt32, NULL, sizeof(macKeyCode), NULL, &macKeyCode );
		GetEventParameter( event, kEventParamKeyModifiers, typeUInt32, NULL, sizeof(macKeyModifiers), NULL, &macKeyModifiers );

		/*
		printf("chr: %x\n", macCharCodes);
		printf("key: %x\n", macKeyCode);
		printf("mod: %x\n", macKeyModifiers);
		fflush(stdout);
		fflush(stderr);
		*/
		
		static bool shift = 0;
		if ((macKeyModifiers & 0x200) && !shift)
		{
			app->mInput.registerKeyDown(QI_KEY_LSHIFT);
			app->mInput.registerKeyDown(QI_KEY_RSHIFT);
			shift = true;
		}
		else if (!(macKeyModifiers & 0x200) && shift)
		{
			app->mInput.registerKeyUp(QI_KEY_LSHIFT);
			app->mInput.registerKeyUp(QI_KEY_RSHIFT);
			shift = false;
		}

		static bool ctrl = 0;
		if ((macKeyModifiers & 0x1000) && !ctrl)
		{
			app->mInput.registerKeyDown(QI_KEY_CTRL);
			ctrl = true;
		}
		else if (!(macKeyModifiers & 0x1000) && ctrl)
		{
			app->mInput.registerKeyUp(QI_KEY_CTRL);
			ctrl = false;
		}
		
		if ( eventKind == kEventRawKeyDown )
		{
			int key=getKey(macKeyCode, macCharCodes);
			if (key >= 1 && key <= 28)
				key += 'a' - 1;
			app->mInput.registerKeyDown(key);
			if (macCharCodes >= ' ' && macCharCodes <= 'z')
				app->mInput.registerCharacter(macCharCodes);
		}
		else if ( eventKind == kEventRawKeyUp )
		{
			int key=getKey(macKeyCode, macCharCodes);
			app->mInput.registerKeyUp(key);
		}
	}
	if ( eventClass == kEventClassMouse )
	{
		if ( eventKind == kEventMouseDown )
		{
			EventMouseButton b;
			GetEventParameter( event, kEventParamMouseButton, typeMouseButton, NULL, sizeof(b), NULL, &b );
			int button=getButton(b);
			app->mInput.registerButtonDown(button);
		}
		else if ( eventKind == kEventMouseUp )
		{
			EventMouseButton b;
			GetEventParameter( event, kEventParamMouseButton, typeMouseButton, NULL, sizeof(b), NULL, &b );
			int button=getButton(b);
			app->mInput.registerButtonUp(button);
		}
		else if ( eventKind == kEventMouseMoved || eventKind == kEventMouseDragged)
		{
			Point p;
			
			GetEventParameter( event, kEventParamWindowMouseLocation, typeQDPoint, NULL, sizeof(p), NULL, &p );

	        Rect titleRect;
	        GetWindowBounds(app->mImpl->window, kWindowTitleBarRgn, &titleRect);
	        float windowTitleHeight = abs(titleRect.bottom - titleRect.top);
				
			//GetEventParameter( event, kEventParamMouseLocation, typeQDPoint, NULL, sizeof(p), NULL, &p );
			//if (!app->mImpl->fullscreen)
			//	QDGlobalToLocalPoint( GetWindowPort( app->mImpl->window ), &p );
			
			app->mInput.registerMousePos(p.h, p.v - windowTitleHeight);
		}
	}
	if ( eventClass == kEventClassWindow )
	{
		if ( eventKind == kEventWindowClosed )
		{
			app->mQuit = true;
			
			//Window is already disposed
			app->mImpl->context = 0;
			app->mImpl->window = 0;
		}
	}
	return noErr;
}


static AGLContext setupAGL( WindowRef window, int width, int height )
{
	GLint attributes[] = { AGL_RGBA, 
		                   AGL_DOUBLEBUFFER, 
		                   AGL_DEPTH_SIZE, 16,
						   AGL_NO_RECOVERY,
		                   AGL_NONE,
	                   	   AGL_NONE };	

	AGLPixelFormat format = NULL;
	format = aglChoosePixelFormat( NULL, 0, attributes );
	if ( !format ) 
		return 0;

	AGLContext context = aglCreateContext( format, 0 );

	if ( !context )
		return 0;

	aglDestroyPixelFormat( format );
	aglSetWindowRef( context, window );
	aglSetCurrentContext( context );
	return context;
}


static void cleanupAGL( AGLContext context )
{
	aglDestroyContext( context );
}


static int GetDisplayWidth()
{
	return CGDisplayPixelsWide( kCGDirectMainDisplay );
}


static int GetDisplayHeight()
{
	return CGDisplayPixelsHigh( kCGDirectMainDisplay );
}


/*
static void HideMouseCursor()
{
	CGDisplayHideCursor( kCGNullDirectDisplay );
	CGAssociateMouseAndMouseCursorPosition( false );
	CGDisplayMoveCursorToPoint( kCGDirectMainDisplay, CGPointZero );
}


static void ShowMouseCursor()
{
	CGAssociateMouseAndMouseCursorPosition( true );
	CGDisplayShowCursor( kCGNullDirectDisplay );
}
*/


QiSimpleApp::QiSimpleApp(const char* title, int width, int height, bool fullscreen) :
mImpl(NULL), mWidth(width), mHeight(height)
{
	mImpl = QI_NEW QiSimpleAppImpl();
	mImpl->fullscreen = fullscreen;
	if (!fullscreen)
	{
		int screenWidth = GetDisplayWidth();
		int screenHeight = GetDisplayHeight();

		Rect rect;
		rect.left = ( screenWidth - width ) / 2;
		rect.top = ( screenHeight - height ) / 2;
		rect.right = rect.left + width;
		rect.bottom = rect.top + height;

		CreateNewWindow( kDocumentWindowClass, 
						                ( kWindowStandardDocumentAttributes |
//										  kWindowNoTitleBarAttribute |
										  kWindowStandardHandlerAttribute ) &
										 ~kWindowResizableAttribute,
						                &rect, &mImpl->window );

		SetWindowTitleWithCFString( mImpl->window, CFStringCreateWithCString( 0, title, CFStringGetSystemEncoding() ) );

		mImpl->context = setupAGL( mImpl->window, width, height );

		ShowWindow( mImpl->window );
		SelectWindow( mImpl->window );

		// install standard event handlers
	    InstallStandardEventHandler( GetWindowEventTarget( mImpl->window ) );	
	}

	EventTypeSpec eventTypes[8];
	eventTypes[0].eventClass = kEventClassKeyboard;
	eventTypes[0].eventKind  = kEventRawKeyDown;
	eventTypes[1].eventClass = kEventClassKeyboard;
	eventTypes[1].eventKind  = kEventRawKeyUp;
	eventTypes[2].eventClass = kEventClassKeyboard;
	eventTypes[2].eventKind  = kEventRawKeyModifiersChanged;
	eventTypes[3].eventClass = kEventClassMouse;
	eventTypes[3].eventKind  = kEventMouseDown;
	eventTypes[4].eventClass = kEventClassMouse;
	eventTypes[4].eventKind  = kEventMouseUp;
	eventTypes[5].eventClass = kEventClassMouse;
	eventTypes[5].eventKind  = kEventMouseMoved;
	eventTypes[6].eventClass = kEventClassMouse;
	eventTypes[6].eventKind  = kEventMouseDragged;
	eventTypes[7].eventClass = kEventClassWindow;
	eventTypes[7].eventKind = kEventWindowClosed;
	EventHandlerUPP handlerUPP = NewEventHandlerUPP( keyboardEventHandler );
	InstallApplicationEventHandler( handlerUPP, 8, eventTypes, this, NULL );

#if 0
	if (fullscreen)
	{
		CGDisplayCapture( kCGDirectMainDisplay );
		CGLPixelFormatAttribute attribs[] = 
		{ 
			kCGLPFANoRecovery,
			kCGLPFADoubleBuffer,
		    kCGLPFAFullScreen,
			kCGLPFAStencilSize, ( CGLPixelFormatAttribute ) 8,
		    kCGLPFADisplayMask, ( CGLPixelFormatAttribute ) CGDisplayIDToOpenGLDisplayMask( kCGDirectMainDisplay ),
		    ( CGLPixelFormatAttribute ) NULL
		};

		CGLPixelFormatObj pixelFormatObj;
		GLint numPixelFormats;
		CGLChoosePixelFormat( attribs, &pixelFormatObj, &numPixelFormats );

		CGLCreateContext( pixelFormatObj, NULL, &mImpl->contextObj );

		CGLDestroyPixelFormat( pixelFormatObj );

		CGLSetCurrentContext( mImpl->contextObj );
		CGLSetFullScreenOnDisplay( mImpl->contextObj, CGDisplayIDToOpenGLDisplayMask( kCGDirectMainDisplay ));
		
		mWidth = GetDisplayWidth();
		mHeight = GetDisplayHeight();
	}
#endif
	AEInstallEventHandler( kCoreEventClass, kAEQuitApplication, NewAEEventHandlerUPP(quitEventHandler), (SRefCon)this, false );

	init();
}


QiSimpleApp::~QiSimpleApp()
{
	if (mImpl->fullscreen)
	{
		CGLSetCurrentContext( NULL );
		CGLClearDrawable( mImpl->contextObj );
		CGLDestroyContext( mImpl->contextObj );
		CGReleaseAllDisplays();
	}
	else
	{
	    cleanupAGL( mImpl->context );
		mImpl->context = 0;
		DisposeWindow( (WindowPtr) mImpl->window );
		mImpl->window = 0;
	}
	QI_DELETE(mImpl);
}


void QiSimpleApp::endRender()
{
	glFlush();

	int interval = 1;
	if (mImpl->fullscreen)
	{
		CGLSetParameter( mImpl->contextObj, kCGLCPSwapInterval, &interval );
		CGLFlushDrawable( mImpl->contextObj );
	}
	else
	{
		GLint swapInterval = interval;
		aglSetInteger( mImpl->context, AGL_SWAP_INTERVAL, &swapInterval );
		aglSwapBuffers( mImpl->context );
	}
}

void QiSimpleApp::processInputInternal()
{
	EventRef event = 0; 
	while(ReceiveNextEvent( 0, NULL, 0.0f, kEventRemoveFromQueue, &event ) == noErr && event)
	{
		bool sendEvent = true;

		if (!mImpl->fullscreen)
		{
			// note: required for menu bar to work properly
			if ( GetEventClass( event ) == kEventClassMouse && GetEventKind( event ) == kEventMouseDown )
			{
				WindowRef window;
				Point location;
				GetEventParameter( event, kEventParamMouseLocation, typeQDPoint, NULL, sizeof(Point), NULL, &location );
				if ( MacFindWindow( location, &window ) == inMenuBar )
				{
					sendEvent = false;
					MenuSelect( location );
				}
			}
		}

		if ( sendEvent )
			SendEventToEventTarget( event, GetEventDispatcherTarget() ); 
	
		ReleaseEvent( event );
	}
}

#endif
#endif

