/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"

#ifdef QI_WIN32
#ifndef QI_USE_SDL

#include "base/qidebug.h"
#include "simpleapp/qisimpleapp.h"
#include <windows.h>

#include <gl\gl.h>
#include <gl\glu.h>

QiButton getButton(int button)
{
	if (button == 16)
		return QI_BUTTON_MIDDLE;
	else
		return QI_BUTTON_UNDEFINED;
}


QiKey getKey(int key)
{
	if (key == VK_ESCAPE)
		return QI_KEY_ESCAPE;
	else if (key == VK_LEFT)
		return QI_KEY_LEFT;
	else if (key == VK_RIGHT)
		return QI_KEY_RIGHT;
	else if (key == VK_UP)
		return QI_KEY_UP;
	else if (key == VK_DOWN)
		return QI_KEY_DOWN;
	else if (key == VK_SHIFT)
		return QI_KEY_LSHIFT;
	else if (key == VK_RETURN)
		return QI_KEY_RETURN;
	else if (key == VK_BACK)
		return QI_KEY_BACKSPACE;
	else if (key == VK_DELETE)
		return QI_KEY_DELETE;
	else if (key == VK_SPACE)
		return (QiKey)' ';
	else if (key >=65 && key <=90)
		return (QiKey)(key+32);
	else if (key == VK_CONTROL)
		return (QiKey)QI_KEY_CTRL;
	else if (key == VK_OEM_MINUS)
		return (QiKey)'-';
	else if (key == VK_OEM_PLUS)
		return (QiKey)'+';
	else if (key == VK_OEM_PERIOD)
		return (QiKey)'.';
	else if (key == VK_OEM_COMMA)
		return (QiKey)',';
	else if (key == VK_HOME)
		return QI_KEY_HOME;
	else if (key == VK_END)
		return QI_KEY_END;
	else if (key == VK_PRIOR)
		return QI_KEY_PGUP;
	else if (key == VK_NEXT)
		return QI_KEY_PGDOWN;
	else if (key == VK_INSERT)
		return QI_KEY_INSERT;
	else if (key == VK_TAB)
		return QI_KEY_TAB;
	else if (key >= VK_F1 && key <= VK_F12)
		return (QiKey)(QI_KEY_F1 + (key-VK_F1));
	else if (key < QI_MAX_KEYS)
		return (QiKey)key;
	else
		return QI_KEY_UNDEFINED;
}

class QiSimpleAppImpl
{
public:
	HDC			hDC;
	HGLRC		hRC;
	HWND		hWnd;
	HINSTANCE	hInstance;
	bool	active;
	bool	fullscreen;
};

QiSimpleApp* gApp = NULL;
QiSimpleAppImpl* gImpl = NULL;


LRESULT CALLBACK WndProc(	HWND	hWnd,			// Handle For This Window
							UINT	uMsg,			// Message For This Window
							WPARAM	wParam,			// Additional Message Information
							LPARAM	lParam)			// Additional Message Information
{
	switch (uMsg)									// Check For Windows Messages
	{
		case WM_ACTIVATE:							// Watch For Window Activate Message
		{
			if (!HIWORD(wParam))					// Check Minimization State
			{
				gImpl->active=TRUE;						// Program Is Active
			}
			else
			{
				gImpl->active=FALSE;						// Program Is No Longer Active
			}

			return 0;								// Return To The Message Loop
		}

		case WM_SYSCOMMAND:							// Intercept System Commands
		{
			switch (wParam)							// Check System Calls
			{
				case SC_SCREENSAVE:					// Screensaver Trying To Start?
				case SC_MONITORPOWER:				// Monitor Trying To Enter Powersave?
				return 0;							// Prevent From Happening
			}
			break;									// Exit
		}

		case WM_CLOSE:								// Did We Receive A Close Message?
		{
			PostQuitMessage(0);						// Send A Quit Message
			return 0;								// Jump Back
		}

		case WM_CHAR:
		{
			gApp->mInput.registerCharacter(wParam);
			return 0;
		}

		case WM_KEYDOWN:							// Is A Key Being Held Down?
		{
			gApp->mInput.registerKeyDown(getKey(wParam));
			return 0;								// Jump Back
		}

		case WM_KEYUP:								// Has A Key Been Released?
		{
			gApp->mInput.registerKeyUp(getKey(wParam));
			return 0;								// Jump Back
		}

		case WM_LBUTTONDOWN:
		{
			gApp->mInput.registerButtonDown(QI_BUTTON_LEFT);
			SetCapture(gImpl->hWnd);
			return 0;
		}

		case WM_LBUTTONUP:
		{
			gApp->mInput.registerButtonUp(QI_BUTTON_LEFT);
			ReleaseCapture();
			return 0;
		}

		case WM_RBUTTONDOWN:
		{
			gApp->mInput.registerButtonDown(QI_BUTTON_RIGHT);
			SetCapture(gImpl->hWnd);
			return 0;
		}

		case WM_RBUTTONUP:
		{
			gApp->mInput.registerButtonUp(QI_BUTTON_RIGHT);
			ReleaseCapture();
			return 0;
		}

		case WM_MBUTTONDOWN:
		{
			gApp->mInput.registerButtonDown(getButton(wParam));
			SetCapture(gImpl->hWnd);
			return 0;
		}

		case WM_MBUTTONUP:
		{
			gApp->mInput.registerButtonUp(getButton(wParam));
			ReleaseCapture();
			return 0;
		}

		case WM_MOUSEMOVE:
		{
			unsigned short int xx = LOWORD(lParam);
			unsigned short int yy = HIWORD(lParam);
			gApp->mInput.registerMousePos((short int&)xx, (short int&)yy);
			return 0;
		}

		case WM_SIZE:								// Resize The OpenGL Window
		{
			gApp->reshape(LOWORD(lParam),HIWORD(lParam), 1000.0f);
			return 0;								// Jump Back
		}
	}

	// Pass All Unhandled Messages To DefWindowProc
	return DefWindowProc(hWnd,uMsg,wParam,lParam);
}


GLvoid KillGLWindow(QiSimpleAppImpl* impl)								// Properly Kill The Window
{
	if (impl->fullscreen)										// Are We In Fullscreen Mode?
	{
		ChangeDisplaySettings(NULL,0);					// If So Switch Back To The Desktop
	}

	if (impl->hRC)											// Do We Have A Rendering Context?
	{
		if (!wglMakeCurrent(NULL,NULL))					// Are We Able To Release The DC And RC Contexts?
		{
			MessageBox(NULL,"Release Of DC And RC Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}

		if (!wglDeleteContext(impl->hRC))						// Are We Able To Delete The RC?
		{
			MessageBox(NULL,"Release Rendering Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}
		impl->hRC=NULL;										// Set RC To NULL
	}

	if (impl->hDC && !ReleaseDC(impl->hWnd,impl->hDC))					// Are We Able To Release The DC
	{
		MessageBox(NULL,"Release Device Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		impl->hDC=NULL;										// Set DC To NULL
	}

	if (impl->hWnd && !DestroyWindow(impl->hWnd))					// Are We Able To Destroy The Window?
	{
		MessageBox(NULL,"Could Not Release hWnd.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		impl->hWnd=NULL;										// Set hWnd To NULL
	}

	if (!UnregisterClass("OpenGL",impl->hInstance))			// Are We Able To Unregister Class
	{
		MessageBox(NULL,"Could Not Unregister Class.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		impl->hInstance=NULL;									// Set hInstance To NULL
	}
}

BOOL CreateGLWindow(QiSimpleAppImpl* impl, const char* title, int width, int height, int bits, bool fullscreenflag)
{
	GLuint		PixelFormat;			// Holds The Results After Searching For A Match
	WNDCLASS	wc;						// Windows Class Structure
	DWORD		dwExStyle;				// Window Extended Style
	DWORD		dwStyle;				// Window Style
	RECT		WindowRect;				// Grabs Rectangle Upper Left / Lower Right Values
	WindowRect.left=(long)0;			// Set Left Value To 0
	WindowRect.right=(long)width;		// Set Right Value To Requested Width
	WindowRect.top=(long)0;				// Set Top Value To 0
	WindowRect.bottom=(long)height;		// Set Bottom Value To Requested Height

	impl->fullscreen=fullscreenflag;			// Set The Global Fullscreen Flag

	impl->hInstance			= GetModuleHandle(NULL);				// Grab An Instance For Our Window
	wc.style			= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// Redraw On Size, And Own DC For Window.
	wc.lpfnWndProc		= (WNDPROC) WndProc;					// WndProc Handles Messages
	wc.cbClsExtra		= 0;									// No Extra Window Data
	wc.cbWndExtra		= 0;									// No Extra Window Data
	wc.hInstance		= impl->hInstance;							// Set The Instance
	wc.hIcon			= LoadIcon(NULL, IDI_WINLOGO);			// Load The Default Icon
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);			// Load The Arrow Pointer
	wc.hbrBackground	= NULL;									// No Background Required For GL
	wc.lpszMenuName		= NULL;									// We Don't Want A Menu
	wc.lpszClassName	= "OpenGL";								// Set The Class Name

	if (!RegisterClass(&wc))									// Attempt To Register The Window Class
	{
		MessageBox(NULL,"Failed To Register The Window Class.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;											// Return FALSE
	}
	
	if (impl->fullscreen)												// Attempt Fullscreen Mode?
	{
		DEVMODE dmScreenSettings;								// Device Mode
		memset(&dmScreenSettings,0,sizeof(dmScreenSettings));	// Makes Sure Memory's Cleared
		dmScreenSettings.dmSize=sizeof(dmScreenSettings);		// Size Of The Devmode Structure
		dmScreenSettings.dmPelsWidth	= width;				// Selected Screen Width
		dmScreenSettings.dmPelsHeight	= height;				// Selected Screen Height
		dmScreenSettings.dmBitsPerPel	= bits;					// Selected Bits Per Pixel
		dmScreenSettings.dmFields=DM_BITSPERPEL|DM_PELSWIDTH|DM_PELSHEIGHT;

		// Try To Set Selected Mode And Get Results.  NOTE: CDS_FULLSCREEN Gets Rid Of Start Bar.
		if (ChangeDisplaySettings(&dmScreenSettings,CDS_FULLSCREEN)!=DISP_CHANGE_SUCCESSFUL)
		{
			// If The Mode Fails, Offer Two Options.  Quit Or Use Windowed Mode.
			if (MessageBox(NULL,"The Requested Fullscreen Mode Is Not Supported By\nYour Video Card. Use Windowed Mode Instead?","NeHe GL",MB_YESNO|MB_ICONEXCLAMATION)==IDYES)
			{
				impl->fullscreen=FALSE;		// Windowed Mode Selected.  Fullscreen = FALSE
			}
			else
			{
				// Pop Up A Message Box Letting User Know The Program Is Closing.
				MessageBox(NULL,"Program Will Now Close.","ERROR",MB_OK|MB_ICONSTOP);
				return FALSE;									// Return FALSE
			}
		}
	}

	if (impl->fullscreen)												// Are We Still In Fullscreen Mode?
	{
		dwExStyle=WS_EX_APPWINDOW;								// Window Extended Style
		dwStyle=WS_POPUP;										// Windows Style
	}
	else
	{
		dwExStyle=WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;			// Window Extended Style
		dwStyle=WS_OVERLAPPEDWINDOW;							// Windows Style
	}

	AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);		// Adjust Window To True Requested Size

	// Create The Window
	if (!(impl->hWnd=CreateWindowEx(	dwExStyle,							// Extended Style For The Window
								"OpenGL",							// Class Name
								title,								// Window Title
								dwStyle |							// Defined Window Style
								WS_CLIPSIBLINGS |					// Required Window Style
								WS_CLIPCHILDREN,					// Required Window Style
								0, 0,								// Window Position
								WindowRect.right-WindowRect.left,	// Calculate Window Width
								WindowRect.bottom-WindowRect.top,	// Calculate Window Height
								NULL,								// No Parent Window
								NULL,								// No Menu
								impl->hInstance,					// Instance
								NULL)))								// Dont Pass Anything To WM_CREATE
	{
		KillGLWindow(impl);								// Reset The Display
		MessageBox(NULL,"Window Creation Error.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	static	PIXELFORMATDESCRIPTOR pfd=				// pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
		1,											// Version Number
		PFD_DRAW_TO_WINDOW |						// Format Must Support Window
		PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,							// Must Support Double Buffering
		PFD_TYPE_RGBA,								// Request An RGBA Format
		bits,										// Select Our Color Depth
		0, 0, 0, 0, 0, 0,							// Color Bits Ignored
		0,											// No Alpha Buffer
		0,											// Shift Bit Ignored
		0,											// No Accumulation Buffer
		0, 0, 0, 0,									// Accumulation Bits Ignored
		16,											// 16Bit Z-Buffer (Depth Buffer)  
		8,											// No Stencil Buffer
		0,											// No Auxiliary Buffer
		PFD_MAIN_PLANE,								// Main Drawing Layer
		0,											// Reserved
		0, 0, 0										// Layer Masks Ignored
	};
	
	if (!(impl->hDC=GetDC(impl->hWnd)))							// Did We Get A Device Context?
	{
		KillGLWindow(impl);								// Reset The Display
		MessageBox(NULL,"Can't Create A GL Device Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	if (!(PixelFormat=ChoosePixelFormat(impl->hDC,&pfd)))	// Did Windows Find A Matching Pixel Format?
	{
		KillGLWindow(impl);								// Reset The Display
		MessageBox(NULL,"Can't Find A Suitable PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	if(!SetPixelFormat(impl->hDC,PixelFormat,&pfd))		// Are We Able To Set The Pixel Format?
	{
		KillGLWindow(impl);								// Reset The Display
		MessageBox(NULL,"Can't Set The PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	if (!(impl->hRC=wglCreateContext(impl->hDC)))				// Are We Able To Get A Rendering Context?
	{
		KillGLWindow(impl);								// Reset The Display
		MessageBox(NULL,"Can't Create A GL Rendering Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	if(!wglMakeCurrent(impl->hDC,impl->hRC))					// Try To Activate The Rendering Context
	{
		KillGLWindow(impl);								// Reset The Display
		MessageBox(NULL,"Can't Activate The GL Rendering Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	ShowWindow(impl->hWnd,SW_SHOW);						// Show The Window
	SetForegroundWindow(impl->hWnd);						// Slightly Higher Priority
	SetFocus(impl->hWnd);									// Sets Keyboard Focus To The Window
	return TRUE;
}


QiSimpleApp::QiSimpleApp(const char* title, int width, int height, bool fullscreen) :
mImpl(NULL), mWidth(width), mHeight(height)
{
	mImpl = QI_NEW QiSimpleAppImpl;
	gImpl = mImpl;
	gApp = this;
	CreateGLWindow(mImpl, title, width, height, 32, fullscreen);
	init();
}


QiSimpleApp::~QiSimpleApp()
{
	KillGLWindow(mImpl);
	QI_DELETE(mImpl);
	gImpl = NULL;
	gApp = NULL;
}


void QiSimpleApp::endRender()
{
	glFlush();
	SwapBuffers(mImpl->hDC);
}

void QiSimpleApp::processInputInternal()
{
	MSG	msg;
	while (PeekMessage(&msg,NULL,0,0,PM_REMOVE))	// Is There A Message Waiting?
	{
		if (msg.message==WM_QUIT)				// Have We Received A Quit Message?
		{
			mQuit=TRUE;							// If So done=TRUE
		}
		else									// If Not, Deal With Window Messages
		{
			TranslateMessage(&msg);				// Translate The Message
			DispatchMessage(&msg);				// Dispatch The Message
		}
	}
}

#endif
#endif
