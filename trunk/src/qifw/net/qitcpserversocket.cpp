/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qidebug.h"
#include "net/qitcpserversocket.h"
#include "net/qitcpsocket.h"
#include "net/qisocketinit.h"

#include <string.h>

#ifdef QI_WIN32
#include <windows.h>
#include <winsock.h>
#include <io.h>
#else
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>
#endif

QiTcpServerSocket::QiTcpServerSocket()
{
	QiIncSocket();
	mSock = 0;
	mPort = 0;
}



QiTcpServerSocket::~QiTcpServerSocket()
{
	close();
	QiDecSocket();
}



bool QiTcpServerSocket::open(int port, int backLog)
{
	int status;
	struct sockaddr_in s_inet;

	mSock = (int)socket(AF_INET,SOCK_STREAM, 0);

	if (mSock == -1) 
		return false;
	memset(&s_inet, 0, sizeof(s_inet));
	s_inet.sin_family = AF_INET;
	s_inet.sin_port = htons(port);
	s_inet.sin_addr.s_addr = htonl(INADDR_ANY);

	int flag = 1;
	status = setsockopt(mSock, IPPROTO_TCP, TCP_NODELAY, (char *)&flag, sizeof(int));
	if (status != 0) 
		return false;

#if !defined(QI_WIN32) && !defined(QI_ANDROID)
	status = setsockopt(mSock, SOL_SOCKET, SO_NOSIGPIPE, (char *)&flag, sizeof(int));
	if (status != 0) 
		return false;
#endif

	status = bind(mSock,(struct sockaddr*)&s_inet, sizeof(s_inet));
	if (status == -1) 
		return false;

	if (port == 0){
#ifdef QI_WIN32
		int a = sizeof(s_inet);
#else
		socklen_t a = sizeof(s_inet);
#endif
		status = getsockname(mSock,(struct sockaddr*)&s_inet, &a);
		if (status == -1) return false;
		port = ntohs(s_inet.sin_port);
	}
	
	mPort = port;

	mPort = port;

	listen(mSock, backLog);
	
	return true;
}


void QiTcpServerSocket::close()
{
	if (mSock)
	{
#ifdef QI_WIN32
		closesocket(mSock);
#else
		::close(mSock);
#endif
	}
	mSock = 0;
}


bool QiTcpServerSocket::accept(QiTcpSocket& socket){
#ifdef QI_WIN32
	int s_inet_size;
#else
	socklen_t s_inet_size;
#endif
	struct sockaddr_in s_inetin;

	if (!mSock)
		return false;
	
	s_inet_size = sizeof(s_inetin);
	int inSock = (int)::accept(mSock, (struct sockaddr*)&s_inetin, &s_inet_size);
	if (inSock == -1)
	{
		mSock = 0;
		return false;
	}

	socket.mAddress.mAddress = (struct sockaddr&)s_inetin;
	socket.mSock = inSock;
	return true;
}

