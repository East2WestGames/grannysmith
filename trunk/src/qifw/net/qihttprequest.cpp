/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "net/qihttprequest.h"
#include "net/qitcpsocket.h"
#include "base/qidebug.h"

bool decodeUrl(const QiString& url, QiString& host, QiString& path, int& port)
{
	//Don't care about protocol
	QiString tmp = url.replace("http://", "", false);

	//Path
	int t = tmp.getIndexOf("/");
	if (t == -1)
		return false;
	path = tmp.substring(t);

	//Host and port
	int p = tmp.getIndexOf(":");
	if (p != -1)
	{
		port = tmp.substring(p+1, t).toInt();
		host = tmp.substring(0, p);
	}
	else
	{
		port = 80;
		host = tmp.substring(0, t);
	}
	return true;
}

bool QiHttpRequest::get(const QiString& url, QiOutputStream& out)
{
	QiString host;
	QiString path;
	int port = 0;

	if (!decodeUrl(url, host, path, port))
	{
		mStatusCode = 0;
		mStatusMessage = "Malformed URL";
		return false;
	}

	QiString tmp = QiString("GET ") + path + QiString(" HTTP/1.1\n");
	tmp += QiString("Host: ") + host + QiString("\n");
	tmp += "\n";

	QiTcpSocket socket;
	if (socket.connect(QiInetAddress(host, port)))
	{
		socket.writeBuffer(tmp, tmp.getLength());
		socket.flush();
		return readResponse(socket, out);
	}
	return false;
}

bool QiHttpRequest::post(const QiString& url, const QiString& text, QiOutputStream& out)
{
	return post(url, text.c_str(), text.getLength(), out);
}

bool QiHttpRequest::post(const QiString& url, const void* data, int size, QiOutputStream& out)
{
	QiString host;
	QiString path;
	int port = 0;

	if (!decodeUrl(url, host, path, port))
	{
		mStatusCode = 0;
		mStatusMessage = "Malformed URL";
		return false;
	}

	QiString tmp = QiString("POST ") + path + QiString(" HTTP/1.0\n");
	tmp += QiString("Host: ") + host + QiString("\n");
	tmp += "Content-Type: application/octet-stream\n";
	tmp += QiString("Content-Length: ") + size + QiString("\n");
	tmp += "\n";

	QiTcpSocket socket;
	if (socket.connect(QiInetAddress(host, port)))
	{
		socket.writeBuffer(tmp.c_str(), tmp.getLength());
		socket.writeBuffer(data, size);
		socket.flush();
	}

	return readResponse(socket, out);
}

bool QiHttpRequest::readResponse(QiTcpSocket& socket, QiOutputStream& out)
{
	mStatusCode = 0;
	mStatusMessage = "";
	mContentLength = 0;
	mContentType = "";
	char t[256];
	while(socket.readLine(t, 256))
	{
		QiString r(t);
		if (r == "")
			break;
		if (r.startsWith("HTTP/", false))
		{
			QiString c = r.getWord(1);
			mStatusCode = c.toInt();
			mStatusMessage = r.substring(r.getIndexOf(c)+c.getLength()+1);
		}
		if (r.startsWith("Content-Length:", false))
			mContentLength = r.getWord(1).toInt();
		if (r.startsWith("Content-Type:", false))
			mContentType = r.getWord(1);
	}
	//QI_PRINT(mStatusCode + ": " + mStatusMessage);
	//QI_PRINT(mContentType + ": " + mContentLength);

	if (mStatusCode == 200)
	{
		if (mContentLength > 0)
			return out.writeBuffer(socket, mContentLength);

		//Empty response, but still okay
		return true;
	}
	else
		return false;
}


