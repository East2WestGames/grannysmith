/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"

#ifdef QI_WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>
#else
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>
#endif

#include "net/qiinetaddress.h"
#include "net/qisocketinit.h"

#include <string.h>

QiInetAddress::QiInetAddress()
{
	QiIncSocket();
	memset(&mAddress, 0, sizeof(mAddress));
	((sockaddr_in&)mAddress).sin_family = AF_INET;
}

QiInetAddress::QiInetAddress(int port)
{
	QiIncSocket();
	memset(&mAddress, 0, sizeof(mAddress));
	((sockaddr_in&)mAddress).sin_family = AF_INET;
	set(port);
}

QiInetAddress::QiInetAddress(const char* hostName, int port)
{
	QiIncSocket();
	memset(&mAddress, 0, sizeof(mAddress));
	((sockaddr_in&)mAddress).sin_family = AF_INET;
	set(hostName, port);
}

QiInetAddress::~QiInetAddress()
{
	QiDecSocket();
}

bool QiInetAddress::set(int port)
{
	((sockaddr_in&)mAddress).sin_port = htons(port);
	((sockaddr_in&)mAddress).sin_addr.s_addr = INADDR_BROADCAST;
	return true;
}

bool QiInetAddress::set(const char* hostName, int port)
{
	struct hostent* host = ::gethostbyname(hostName);
	if (!host)
		return false;
	((sockaddr_in&)mAddress).sin_port = htons(port);
	((sockaddr_in&)mAddress).sin_addr.s_addr = *(long *)(host->h_addr);
	return true;
}


int QiInetAddress::getPort() const
{
	return ((sockaddr_in&)mAddress).sin_port;
}

QiString QiInetAddress::getName(bool fullyQualified) const
{
	char host[NI_MAXHOST];
	int flags = fullyQualified ? 0 : NI_NOFQDN;
	if (getnameinfo(&mAddress, sizeof(struct sockaddr), host, NI_MAXHOST, NULL, 0, flags) == 0)
	{
		return QiString(host);	
	}
	else
	{
		unsigned long ip = (unsigned long)ntohl(((sockaddr_in&)mAddress).sin_addr.s_addr);
		return QiString() + (int)(ip>>24) + "." + (int)((ip&0xFF0000)>>16) + "." + (int)((ip&0xFF00)>>8) + "." + (int)(ip&0xFF);
	}
}
