/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qidebug.h"
#include "net/qiudpsocket.h"
#include "net/qisocketinit.h"

#include <string.h>

#ifdef QI_WIN32
#include <winsock.h>
#include <windows.h>
typedef int socklen_t;
#else
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>
#endif

QiUdpSocket::QiUdpSocket() :
mSock(0), mBroadcastMode(false)
{
	QiIncSocket();
}

QiUdpSocket::~QiUdpSocket()
{
	close();
	QiDecSocket();
}


bool QiUdpSocket::open(int port)
{
	mSock = socket(AF_INET, SOCK_DGRAM, 0);
	if (mSock == -1) 
	{
		mSock = 0;
		return false;
	}

	int one = 1;
	if (setsockopt(mSock, SOL_SOCKET, SO_REUSEADDR, (char*) &one, sizeof(one)) != 0)
	{
		mSock = 0;
		return false;
	}

	((struct sockaddr_in&)mLocalAddress.mAddress).sin_port = htons(port);
	((struct sockaddr_in&)mLocalAddress.mAddress).sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(mSock, &mLocalAddress.mAddress, sizeof(mLocalAddress.mAddress)) == -1)
	{
		mSock = 0;
		return false;
	}

	socklen_t a = sizeof(mLocalAddress);
	return getsockname(mSock,&mLocalAddress.mAddress, &a) != -1;
}

void QiUdpSocket::close()
{
	if (mSock)
	{
		#ifdef QI_WIN32
		::closesocket(mSock);
		#else
		::shutdown(mSock, SHUT_RDWR);
		::close(mSock);
		#endif
		mSock = 0;
	}
}

bool QiUdpSocket::write(char* buffer, size_t size, const QiInetAddress& address)
{
	socklen_t fromLen = sizeof(struct sockaddr);
	struct sockaddr_in* pAddr = (struct sockaddr_in*)&address.mAddress;
	if ((pAddr->sin_addr.s_addr == INADDR_BROADCAST) ^ mBroadcastMode)
	{
		if (!setBroadcastMode(!mBroadcastMode))
			return false;
	}
	int s = ::sendto(mSock, buffer, size, 0, &address.mAddress, fromLen);
	return s == (int)size;
}

bool QiUdpSocket::read(char* buffer, size_t size, QiInetAddress* address)
{
	struct sockaddr from;
	socklen_t fromLen = sizeof(struct sockaddr);
	struct sockaddr* pFrom = (address ? &address->mAddress : &from);
	int tmp = ::recvfrom(mSock, buffer, size, 0, pFrom, &fromLen);
	return fromLen == sizeof(struct sockaddr) && tmp == (int)size;
}

bool QiUdpSocket::setBroadcastMode(bool broadcast)
{
#ifdef QI_WIN32
	char enable;
#else
	int enable;
#endif
	enable = broadcast ? 1 : 0;
	if (::setsockopt(mSock, SOL_SOCKET, SO_BROADCAST, &enable, sizeof(enable)) != -1)
	{
		mBroadcastMode = broadcast;
		return true;
	}
	return false;
}


