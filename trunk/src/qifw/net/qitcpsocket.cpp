/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qidebug.h"
#include "net/qitcpsocket.h"
#include "net/qisocketinit.h"

#include <string.h>

#ifdef QI_WIN32
#include <windows.h>
#include <winsock.h>
#else
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>
#endif

QiTcpSocket::QiTcpSocket()
{
	QiIncSocket();
	mSock = 0;
	setInputByteOrder(QI_NETWORK_BYTE_ORDER);
	setOutputByteOrder(QI_NETWORK_BYTE_ORDER);
}


QiTcpSocket::QiTcpSocket(int sock)
{
	QiIncSocket();
	mSock = sock;
	setInputByteOrder(QI_NETWORK_BYTE_ORDER);
	setOutputByteOrder(QI_NETWORK_BYTE_ORDER);
}


QiTcpSocket::~QiTcpSocket()
{
	close();
	QiDecSocket();
}


void QiTcpSocket::close()
{
	flush();
	if (mSock)
	{
#ifdef QI_WIN32
		::shutdown(mSock, 2);
		::closesocket(mSock);
#else
		::shutdown(mSock, 2);
		::close(mSock);
#endif
	}
	mSock = 0;
}

bool QiTcpSocket::connect(const QiInetAddress& address, float timeOut)
{
		mAddress = address;
		mSock = socket(AF_INET, SOCK_STREAM, 0);

		if (mSock == -1)
		{
			mSock = 0;
			return false;
		}

		int one = 1;
		if (::setsockopt(mSock, SOL_SOCKET, SO_REUSEADDR, (char*) &one, sizeof(one)) != 0)
		{
			mSock = 0;
			return false;
		}

		if (::setsockopt(mSock, IPPROTO_TCP, TCP_NODELAY, (char *)&one, sizeof(int)) != 0)
		{
			mSock = 0;
			return false;
		}

		#if !defined(QI_WIN32) && !defined(QI_ANDROID)
		if (::setsockopt(mSock, SOL_SOCKET, SO_NOSIGPIPE, (char *)&one, sizeof(int)) != 0)
		{
			mSock = 0;
			return false;
		}
		#endif


#if defined(QI_WIN32)
		u_long iMode=1;
		ioctlsocket(mSock, FIONBIO, &iMode);
#else
		int flags = fcntl(mSock, F_GETFL);
		fcntl(mSock, F_SETFL, flags | O_NONBLOCK);
#endif

		::connect(mSock, &address.mAddress, sizeof(address.mAddress));

		fd_set fdset;
		FD_ZERO(&fdset);
	    FD_SET(mSock, &fdset);
		struct timeval tv;
		tv.tv_sec = (int)timeOut;
		tv.tv_usec = (long)(1000000 * (timeOut-(int)timeOut));

		if (select(mSock + 1, NULL, &fdset, NULL, &tv) != 1)
		{
			close();
			return false;
		}

#if defined(QI_WIN32)
		iMode=0;
		ioctlsocket(mSock, FIONBIO, &iMode);
#else
		flags = fcntl(mSock, F_GETFL);
		fcntl(mSock, F_SETFL, flags & ~O_NONBLOCK);
#endif

		resetInputStream();
		resetOutputStream();

		return true;
}


bool QiTcpSocket::writeBuffered(const char *buffer, size_t bytes){
	if (!mSock)
		return false;

	size_t sentBytes = 0;
	while(sentBytes < bytes)
	{
		size_t tmp = send(mSock, buffer + sentBytes, bytes - sentBytes, 0);
		if (tmp == 0 || tmp == (size_t)-1)
			return false;
		sentBytes += tmp;
	}
	return true;
}


bool QiTcpSocket::readInternal(char* buffer, size_t bytes){
	if (!mSock)
		return false;
		
	size_t readBytes = 0;
	while (readBytes < bytes)
	{
		size_t tmp;
#ifdef QI_WIN32
		tmp = recv(mSock, buffer + readBytes, bytes - readBytes, 0);
#else
		tmp = read(mSock, buffer + readBytes, bytes - readBytes);
#endif
		if (tmp == 0 || tmp == (size_t)-1)
			return false;
		readBytes += tmp;
	}
	return true;
}

