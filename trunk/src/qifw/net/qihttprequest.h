/* Qi Framework. Copyright 2007-2012 Dengu AB */

#pragma once

#include "base/qistring.h"

class QiTcpSocket;
class QiString;
class QiOutputStream;
class QiHttpRequest
{
public:
	bool get(const QiString& url, QiOutputStream& out);
	bool post(const QiString& url, const QiString& text, QiOutputStream& out);
	bool post(const QiString& url, const void* data, int size, QiOutputStream& out);

	inline int getStatusCode() const { return mStatusCode; }
	inline const QiString& getStatusMessage() const { return mStatusMessage; }

private:
	bool readResponse(QiTcpSocket& socket, QiOutputStream& out);

	int mStatusCode;
	QiString mStatusMessage;

	int mContentLength;
	QiString mContentType;
};
