#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"

#ifdef QI_WIN32
extern int gQiSocketCount;
bool QiIncSocket();
void QiDecSocket();
#else
inline bool QiIncSocket() { return true; }
inline void QiDecSocket() {}
#endif

