/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qidebug.h"
#include "qisocketinit.h"

#ifdef QI_WIN32
#include <windows.h>
#include <winsock.h>

int gQiSocketCount = 0;

bool QiIncSocket()
{
	if (gQiSocketCount==0)
	{
		WORD vreq;
		WSADATA wsaData;
		vreq = MAKEWORD(2,2);
		if (WSAStartup(vreq, &wsaData)!=0){
			QI_ERROR("Failed to init WinSock");
			return false;
		}
	}
	gQiSocketCount++;
	return true;
}

void QiDecSocket()
{
	gQiSocketCount--;
	if (gQiSocketCount==0)
		WSACleanup();
}

#endif
