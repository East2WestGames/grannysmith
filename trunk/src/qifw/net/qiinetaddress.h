#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"
#include "base/qistring.h"

#include <sys/types.h>
#ifdef QI_WIN32
#include <winsock.h>
#else
#include <sys/socket.h>
#endif

class QiInetAddress
{
public:
	QiInetAddress();
	QiInetAddress(int port);
	QiInetAddress(const char* hostName, int port);
	~QiInetAddress();

	bool set(int port);
	bool set(const char* hostName, int port);

	QiString getName(bool fullyQualified=false) const;
	int getPort() const;

	sockaddr mAddress;
};


