#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"
#include "base/qistream.h"
#include "base/qibufferedstream.h"
#include "net/qiinetaddress.h"

class QiTcpServerSocket;

class QiTcpSocket : public QiInputStream, public QiBufferedOutputStream
{
	friend class QiTcpServerSocket;
public:
	QiTcpSocket();
	QiTcpSocket(int fd);
	~QiTcpSocket();

	bool connect(const QiInetAddress& address, float timeOut=2.0f);
	void close();
	inline bool isOpen() const { return mSock != 0; }
	inline const QiInetAddress& getRemoteAddress() const { return mAddress; }

	virtual bool hasMoreData() { return true; }

protected:
	virtual bool readInternal(char* buffer, size_t bytes);
	virtual bool writeBuffered(const char* buffer, size_t bytes);

private:
	int mSock;
	QiInetAddress mAddress;
};


