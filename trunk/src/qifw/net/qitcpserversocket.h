#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"

class QiTcpSocket;

class QiTcpServerSocket{
public:
	QiTcpServerSocket();
	~QiTcpServerSocket();

	bool open(int port, int backLog);
	void close();
	bool isOpen() { return mSock != 0; }

	bool accept(QiTcpSocket &socket);
	int getPort() const { return mPort; }
	
private:
	int mSock;
	int mPort;
};



