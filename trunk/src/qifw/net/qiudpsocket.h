#pragma once

/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qiconfig.h"
#include "net/qiinetaddress.h"

class QiUdpSocket
{
public:
	QiUdpSocket();
	~QiUdpSocket();

	bool open(int localPort = 0);
	void close();
	inline bool isOpen() const { return mSock!=0; }

	bool setBroadcastMode(bool broadcast);

	bool read(char* buffer, size_t size, QiInetAddress* address = NULL);
	bool write(char* buffer, size_t size, const QiInetAddress& address);

	inline const QiInetAddress& getLocalAddress() { return mLocalAddress; }
	
private:
	int mSock;	
	bool mBroadcastMode;
	QiInetAddress mLocalAddress;	
};

