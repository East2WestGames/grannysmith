/* Qi Framework. Copyright 2007-2012 Dengu AB */

#ifdef QI_ANDROID

#include "qiaudio.h"
#include "base/qimath.h"
#include "base/qisync.h"
#include "base/qidebug.h"

#include <SLES/OpenSLES.h>
#include "SLES/OpenSLES_Android.h"

const int STREAMING_BUFFER_SIZE = 4096;

// this callback handler is called every time a buffer finishes playing
static void bqPlayerCallback(SLAndroidSimpleBufferQueueItf bq, void *context)
{
	QiAudioChannel* c = (QiAudioChannel*)context;
	if (c)
		c->callback();
}



class QiStreamingAudioBuffer
{
public:
	inline QiStreamingAudioBuffer(QiAudioStream* stream, int frequency, int channels) : 
	mStream(stream), mFillBuffer(0), mPlayBuffer(0), mFrequency(frequency), mChannelCount(channels)
	{
		mBufferSize = 0;
	}

	inline ~QiStreamingAudioBuffer()
	{
	}

	inline void fillBuffer()
	{
		mBufferSize = mStream->getData(mBufferData, STREAMING_BUFFER_SIZE);
		
		//Convert to mono, since current implementation assumes that
		if (mChannelCount == 2)
		{
			short* samples = (short*)mBufferData;
			for(int i=0; i<mBufferSize; i+=2)
				samples[i/2] = samples[i]/2 + samples[i+1]/2;
			mBufferSize /= 2;
		}
	}

	char mBufferData[STREAMING_BUFFER_SIZE];
	int mBufferSize;
	QiAudioStream* mStream;
	int mFillBuffer;
	int mPlayBuffer;
	int mFrequency;
	int mChannelCount;
};



/////////////////////// BUFFERS ////////////////////////////////


QiAudioBuffer::QiAudioBuffer(QiAudio* audio) : mAudio(audio)
{
	//Regular audio buffer
	mData = NULL;
	mSampleCount = 0;
	mStreamingBuffer = NULL;
}


QiAudioBuffer::QiAudioBuffer(QiAudio* audio, QiAudioStream* stream, int frequency, int channels) : mAudio(audio)
{
	//Streaming audio buffer
	mData = NULL;
	mSampleCount = 0;
	mStreamingBuffer = QI_NEW QiStreamingAudioBuffer(stream, frequency, channels);
}


QiAudioBuffer::~QiAudioBuffer()
{
	if (mData)
		QiFree(mData);
}


/////////////////////// CHANNELS ////////////////////////////////


QiAudioChannel::QiAudioChannel(QiAudio* audio) : mAudio(audio), mBuffer(NULL), 
mVolume(1.0f), mPitch(1.0f), mLooping(false), mTimeStamp(0), mStreaming(false), mStopStreaming(false),
mWantToStop(false)
{
	mInitialized = false;
	mVolumeItf = NULL;
	mPlayerObject = NULL;
	mPlayItf = NULL;
	mRateItf = NULL;
	mBufferQueueItf = NULL;
	mMasterPaused = false;
	init();
} 


QiAudioChannel::~QiAudioChannel()
{
	if (mInitialized)
		setBuffer(NULL);
	shutdown();
}


void QiAudioChannel::shutdown()
{
	if (!mInitialized)
		return;
		
	QI_PRINT("Shutdown channel");
	if (mPlayerObject)
	{
		(*mPlayerObject)->Destroy(mPlayerObject);
		mPlayerObject = NULL;
	}
	mInitialized = false;
}


bool QiAudioChannel::init()
{
	if (mInitialized)
		return true;

	QI_PRINT("Init channel");	
	mInitialized = false;
	
	SLresult result;
  
	// configure audio source
	SLDataLocator_AndroidSimpleBufferQueue loc_bufq = {SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE, 2};
	SLDataFormat_PCM format_pcm = {SL_DATAFORMAT_PCM, 1, SL_SAMPLINGRATE_44_1,
		SL_PCMSAMPLEFORMAT_FIXED_16, SL_PCMSAMPLEFORMAT_FIXED_16,
		SL_SPEAKER_FRONT_CENTER, SL_BYTEORDER_LITTLEENDIAN};
	SLDataSource audioSrc = {&loc_bufq, &format_pcm};
  
	// configure audio sink
	SLDataLocator_OutputMix loc_outmix = {SL_DATALOCATOR_OUTPUTMIX, mAudio->mOutputMixObject};
	SLDataSink audioSnk = {&loc_outmix, NULL};

	// create audio player
	const SLInterfaceID ids[3] = {SL_IID_BUFFERQUEUE, SL_IID_PLAYBACKRATE, SL_IID_VOLUME};
	const SLboolean req[3] = {SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE};
	result = (*mAudio->mEngineItf)->CreateAudioPlayer(mAudio->mEngineItf, &mPlayerObject, &audioSrc, &audioSnk, 3, ids, req);
	if (SL_RESULT_SUCCESS != result)
	{
		QI_PRINT("Error in audio CreateAudioPlayer");
		return false;
	}
  
	// realize the player
	result = (*mPlayerObject)->Realize(mPlayerObject, SL_BOOLEAN_FALSE);
	if (SL_RESULT_SUCCESS != result)
	{
		QI_PRINT("Error in audio Realize mPlayerObject");
		return false;
	}
  
	// get the play interface
	result = (*mPlayerObject)->GetInterface(mPlayerObject, SL_IID_PLAY, &mPlayItf);
	if (SL_RESULT_SUCCESS != result)
	{
		QI_PRINT("Error in audio GetInterface SL_IID_PLAY");
		return false;
	}
  
	// get the buffer queue interface
	result = (*mPlayerObject)->GetInterface(mPlayerObject, SL_IID_BUFFERQUEUE, &mBufferQueueItf);
	if (SL_RESULT_SUCCESS != result)
	{
		QI_PRINT("Error in audio GetInterface SL_IID_BUFFERQUEUE");
		return false;
	}
  
	// register callback on the buffer queue
	result = (*mBufferQueueItf)->RegisterCallback(mBufferQueueItf, bqPlayerCallback, this);
	if (SL_RESULT_SUCCESS != result)
	{
		QI_PRINT("Error in audio RegisterCallback bqPlayerCallback");
		return false;
	}

	result = (*mPlayerObject)->GetInterface(mPlayerObject, SL_IID_PLAYBACKRATE, &mRateItf);
	if (SL_RESULT_SUCCESS != result)
	{
		QI_PRINT("Error in audio GetInterface SL_IID_PLAYBACKRATE");
		return false;
	}

	result = (*mPlayerObject)->GetInterface(mPlayerObject, SL_IID_VOLUME, &mVolumeItf);
	if (SL_RESULT_SUCCESS != result)
	{
		QI_PRINT("Error in audio GetInterface SL_IID_VOLUME");
		return false;
	}
	
	mInitialized = true;
	return true;
}


void QiAudioChannel::setBuffer(const QiAudioBuffer* buffer)
{
	bool wasPlaying = isPlaying();
	if (wasPlaying)
		stop();
	mBuffer = buffer;
	mStreaming = (mBuffer!=NULL && mBuffer->mStreamingBuffer!=NULL);	
	if (wasPlaying && mBuffer)
		play();
	
}


void QiAudioChannel::updateStreaming()
{
}


void QiAudioChannel::callback()
{
	if (!mInitialized)
		return;
	mMutex.lock();
	if (mStreaming)
	{
		if (mStopStreaming)
		{
			mMutex.unlock();
			return;
		}
		mBuffer->mStreamingBuffer->fillBuffer();
		char* data = mBuffer->mStreamingBuffer->mBufferData;
		int size = mBuffer->mStreamingBuffer->mBufferSize;
		if (size > 0)
			(*mBufferQueueItf)->Enqueue(mBufferQueueItf, data, size);        	
		else
		{
			mMutex.unlock();
			stop();
			return;
		}
	}
	else
	{
		if (mBuffer && mLooping)
	        (*mBufferQueueItf)->Enqueue(mBufferQueueItf, mBuffer->mData, mBuffer->mSampleCount*2);
		else
		{
			mMutex.unlock();
			stop();
			return;
		}
	}
	mMutex.unlock();
}


void QiAudioChannel::setVolume(float volume)
{
	mVolume = volume;
	if (!mInitialized)
		return;
	int mB = int(QiLog10(QiClamp(mVolume, 0.000001f, 1.0f)) * 1000);
	(*mVolumeItf)->SetVolumeLevel(mVolumeItf, mB);
}


void QiAudioChannel::setPitch(float pitch)
{
	mPitch = pitch;
	if (!mInitialized)
		return;
	int p = QiClamp(int(pitch*1000), 500, 2000);
	(*mRateItf)->SetRate(mRateItf, p); 
}


void QiAudioChannel::setLooping(bool enabled)
{
	mLooping = enabled;
}

void QiAudioChannel::play()
{
	mStopStreaming = false;
	mWantToStop = false;
	
	if (!mInitialized)
		return;
	
	if (isPlaying())
		return;
    (*mBufferQueueItf)->Clear(mBufferQueueItf);
	if (mStreaming)
	{
		mBuffer->mStreamingBuffer->fillBuffer();
		char* data = mBuffer->mStreamingBuffer->mBufferData;
		int size = mBuffer->mStreamingBuffer->mBufferSize;
		if (size > 0)
			(*mBufferQueueItf)->Enqueue(mBufferQueueItf, data, size);        	
	}
	else if (mBuffer && mBuffer->mSampleCount > 0)
	{
        (*mBufferQueueItf)->Enqueue(mBufferQueueItf, mBuffer->mData, mBuffer->mSampleCount*2);
	}
	(*mPlayItf)->SetPlayState(mPlayItf, SL_PLAYSTATE_PLAYING);

}


void QiAudioChannel::stop()
{
	if (!mInitialized)
		return;

	if (!isPlaying())
		return;
	mMutex.lock();
	(*mPlayItf)->SetPlayState(mPlayItf, SL_PLAYSTATE_STOPPED);
    (*mBufferQueueItf)->Clear(mBufferQueueItf);
	mStopStreaming = true;
	mMutex.unlock();
}


bool QiAudioChannel::isPlaying() const
{
	if (!mInitialized)
		return false;

	SLuint32 pState = SL_PLAYSTATE_STOPPED;
	(*mPlayItf)->GetPlayState(mPlayItf, &pState);
	return pState == SL_PLAYSTATE_PLAYING;
}


/////////////////////// AUDIO ////////////////////////////////


QiAudio::QiAudio() : mTimeStamp(0)
{
	mEngineObject = NULL;
	mEngineItf = NULL;
	mOutputMixObject = NULL;
	mMasterPause = false;
}


QiAudio::~QiAudio()
{
	shutdown();
}


void QiAudio::init(int channels)
{
	startEngine();

	for(int i=0; i<channels; i++)
	{
		QiAudioChannel* c = QI_NEW QiAudioChannel(this);
		mFreeChannels.add(c);
	}
	
	QI_PRINT("Audio done");
}


void QiAudio::shutdown()
{
	for(int i=0; i<mFreeChannels.getCount(); i++)
		QI_DELETE(mFreeChannels[i]);
	mFreeChannels.clear();

	for(int i=0; i<mAcquiredChannels.getCount(); i++)
		QI_DELETE(mAcquiredChannels[i]);
	mAcquiredChannels.clear();

	for(int i=0; i<mBuffers.getCount(); i++)
		QI_DELETE(mBuffers[i]);
	mBuffers.clear();
	
	stopEngine();
}


void QiAudio::startEngine()
{
		    
    SLresult result;

	if (!mEngineObject)
	{
	    result = slCreateEngine(&mEngineObject, 0, NULL, 0, NULL, NULL);
	    if (SL_RESULT_SUCCESS != result)
		{
			QI_PRINT("Error in slCreateEngine");
			return;
		}

	    result = (*mEngineObject)->Realize(mEngineObject, SL_BOOLEAN_FALSE);
	    if (SL_RESULT_SUCCESS != result)
		{
			QI_PRINT("Error in audio Realize mEngineObject");
			return;
		}
	}
	
	if (!mEngineItf)
	{
	    result = (*mEngineObject)->GetInterface(mEngineObject, SL_IID_ENGINE, &mEngineItf);
	    if (SL_RESULT_SUCCESS != result)
		{
			QI_PRINT("Error in audio GetInterface SL_IID_ENGINE");
			return;
		}
	}

	if (!mOutputMixObject)
	{
	    result = (*mEngineItf)->CreateOutputMix(mEngineItf, &mOutputMixObject, 0, NULL, NULL);
	    if (SL_RESULT_SUCCESS != result)
		{
			QI_PRINT("Error in audio CreateOutputMix");
			return;
		}

	    result = (*mOutputMixObject)->Realize(mOutputMixObject, SL_BOOLEAN_FALSE);
	    if (SL_RESULT_SUCCESS != result)
		{
			QI_PRINT("Error in audio Realize mOutputMixObject");
			return;
		}
	}
}


void QiAudio::stopEngine()
{
    if (mOutputMixObject != NULL) {
        (*mOutputMixObject)->Destroy(mOutputMixObject);
        mOutputMixObject = NULL;
    }

    if (mEngineObject != NULL) {
        (*mEngineObject)->Destroy(mEngineObject);
        mEngineObject = NULL;
        mEngineItf = NULL;
    }
}


void QiAudio::updateStreaming()
{
	for(int i=0; i<mFreeChannels.getCount(); i++)
	{
		if (mFreeChannels[i]->isStreaming())
			mFreeChannels[i]->updateStreaming();
	}
	for(int i=0; i<mAcquiredChannels.getCount(); i++)
	{
		if (mAcquiredChannels[i]->isStreaming())
			mAcquiredChannels[i]->updateStreaming();
	}
}


QiAudioBuffer* QiAudio::createBuffer(short int* data, int size, int frequency, int channels, int bitsPerSample)
{
	QiAudioBuffer* b = QI_NEW QiAudioBuffer(this);

	//Copy data
	if (bitsPerSample == 16)
	{
		b->mSampleCount = size/channels/2;
		b->mData = (short int*)QiAlloc(b->mSampleCount*2);

		if (channels == 2)
		{
			for(int i=0; i<b->mSampleCount; i++)
				b->mData[i] = (short int)(((int)data[i*2] + (int)data[i*2+1]) / 2);
		}
		else
			memcpy(b->mData, data, size);
	}
	
	mBuffers.add(b);
	return b;
}


QiAudioBuffer* QiAudio::createStreamingBuffer(QiAudioStream* stream, int frequency, int channels, int bitsPerSample)
{
	QiAudioBuffer* b = QI_NEW QiAudioBuffer(this, stream, frequency, channels);
	mBuffers.add(b);
	return b;	
}


void QiAudio::destroyBuffer(QiAudioBuffer* buffer)
{
	for(int i=0; i<mFreeChannels.getCount(); i++)
	{
		if (mFreeChannels[i]->mBuffer == buffer)
		{
			mFreeChannels[i]->stop();
			mFreeChannels[i]->setBuffer(NULL);
		}
	}
	for(int i=0; i<mAcquiredChannels.getCount(); i++)
	{
		if (mAcquiredChannels[i]->mBuffer == buffer)
		{
			mAcquiredChannels[i]->stop();
			mAcquiredChannels[i]->setBuffer(NULL);
		}
	}
	mBuffers.removeAll(buffer);
	QI_DELETE(buffer);
}


QiAudioChannel* QiAudio::acquireChannel()
{
	QiAudioChannel* c = findBestChannel();
	if (c)
	{
		mFreeChannels.removeAll(c);
		mAcquiredChannels.add(c);
		c->setPitch(1.0f);
		c->setVolume(1.0f);
		c->setLooping(false);
	}
	return c;
}


void QiAudio::releaseChannel(QiAudioChannel* channel)
{
	if (!channel)
		return;
	channel->stop();
	channel->setBuffer(NULL);
	channel->setLooping(false);
	mAcquiredChannels.removeAll(channel);
	mFreeChannels.add(channel);
}


void QiAudio::play(const QiAudioBuffer* buffer, float volume, float pitch)
{
	QiAudioChannel* c = findBestChannel();
	if (c)
	{
		c->setLooping(false);
		c->setBuffer(buffer);
		c->setVolume(volume);
		c->setPitch(pitch);
		c->play();
	}
}


void QiAudio::setMasterVolume(float volume)
{
//	alListenerf(AL_GAIN, volume);
}


float QiAudio::getMasterVolume() const
{
	float tmp=0.0f;
//	alGetListenerf(AL_GAIN, &tmp);
	return tmp;
}


void QiAudio::setMasterPause(bool paused)
{
	if (mMasterPause == paused)
		return;
	if (!paused)
	{
		startEngine();
		for(int i=0; i<mAcquiredChannels.getCount(); i++)
		{
			mAcquiredChannels[i]->init();
			mAcquiredChannels[i]->setVolume(mAcquiredChannels[i]->mVolume);
		}
		for(int i=0; i<mFreeChannels.getCount(); i++)
			mFreeChannels[i]->init();
	}
	for(int i=0; i<mFreeChannels.getCount(); i++)
	{
		if (mFreeChannels[i] && mFreeChannels[i]->getBuffer())
		{
			if (mFreeChannels[i]->isPlaying() && paused)
			{
				mFreeChannels[i]->mMasterPaused = true;
				mFreeChannels[i]->stop();
			}
			else if (mFreeChannels[i]->mMasterPaused && !paused)
			{
				mFreeChannels[i]->play();
				mFreeChannels[i]->mMasterPaused = false;
			}
		}
	}
	for(int i=0; i<mAcquiredChannels.getCount(); i++)
	{
		if (mAcquiredChannels[i] && mAcquiredChannels[i]->getBuffer())
		{
			if (mAcquiredChannels[i]->isPlaying() && paused)
			{
				mAcquiredChannels[i]->mMasterPaused = true;
				mAcquiredChannels[i]->stop();
			}
			else if (mAcquiredChannels[i]->mMasterPaused && !paused)
			{
				mAcquiredChannels[i]->play();
				mAcquiredChannels[i]->mMasterPaused = false;
			}
		}
	}
	if (paused)
	{
		for(int i=0; i<mAcquiredChannels.getCount(); i++)
			mAcquiredChannels[i]->shutdown();
		for(int i=0; i<mFreeChannels.getCount(); i++)
			mFreeChannels[i]->shutdown();
		stopEngine();
	}
	mMasterPause = paused;
}


QiAudioChannel* QiAudio::findBestChannel()
{
	int oldest = QI_INT_MAX;
	QiAudioChannel* best = NULL;
	for(int i=0; i<mFreeChannels.getCount(); i++)
	{
		if (!mFreeChannels[i]->isPlaying() && mFreeChannels[i]->mInitialized)
		{
			best = mFreeChannels[i];
			break;
		}
		else if (mFreeChannels[i]->mTimeStamp < oldest && mFreeChannels[i]->mInitialized)
		{
			best = mFreeChannels[i];
			oldest = best->mTimeStamp;
		}
	}
	if (best)
	{
		static int timeStamp = 0;
		best->mTimeStamp = timeStamp++;
		best->stop();
	}
	return best;
}

#endif

