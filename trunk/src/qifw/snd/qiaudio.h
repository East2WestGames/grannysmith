/* Qi Framework. Copyright 2007-2012 Dengu AB */

#pragma once
#include "base/qiarray.h"

#ifndef QI_ANDROID

class QiAudioStream
{
public:
	~QiAudioStream() {}
	virtual int getData(void* buffer, int size) = 0;
};


class QiAudioBuffer
{
	friend class QiAudio;
	friend class QiAudioChannel;

public:
	~QiAudioBuffer();
	int getSize() const { return mBytes; }
	bool isStreaming() const { return mStreamingBuffer != NULL; }

protected:
	QiAudioBuffer(class QiAudio* audio);
	QiAudioBuffer(class QiAudio* audio, QiAudioStream* stream, int frequency, int format);
	
	class QiAudio* mAudio;
	int mInternal;
	int mBytes;
	class QiStreamingAudioBuffer* mStreamingBuffer;
};


class QiAudioChannel
{
	friend class QiAudio;
	friend class QiAudioBuffer;

public:
	~QiAudioChannel();
	void setBuffer(const QiAudioBuffer* buffer);
	inline const QiAudioBuffer* getBuffer() const { return mBuffer; }

	void setVolume(float volume);
	inline float getVolume() const { return mVolume; }

	void setPitch(float pitch);
	inline float getPitch() const { return mPitch; }

	void setLooping(bool enabled);
	inline bool isLooping() const { return mLooping; }

	void play();
	void stop();
	bool isPlaying() const;
	bool isStreaming() const { return mStreaming; }

protected:
	QiAudioChannel(QiAudio* audio);
	void updateStreaming();

	class QiAudio* mAudio;
	int mInternal;

	const QiAudioBuffer* mBuffer;
	float mVolume;
	float mPitch;
	bool mLooping;
	int mTimeStamp;
	bool mStreaming;
	bool mStopStreaming;
};


class QiAudio
{
	friend class QiAudioBuffer;
	friend class QiAudioSource;

public:
	QiAudio();
	~QiAudio();

	void init(int channels);
	void shutdown();
	void updateStreaming();

	QiAudioBuffer* createBuffer(short int* data, int size, int frequency, int channels, int bitsPerSample);
	QiAudioBuffer* createStreamingBuffer(QiAudioStream* stream, int frequency, int channels, int bitsPerSample);

	void destroyBuffer(QiAudioBuffer* buffer);

	QiAudioChannel* acquireChannel();
	void releaseChannel(QiAudioChannel* channel);

	void play(const QiAudioBuffer* buffer, float volume=1.0f, float pitch=1.0f);

	void setMasterVolume(float volume);
	float getMasterVolume() const;

	void setMasterPause(bool paused);

	int getAllocatedBytes() const;

protected:
	QiAudioChannel* findBestChannel();

	void* mContext;
	void* mDevice;
	int mTimeStamp;

	QiArray<QiAudioChannel*> mFreeChannels;
	QiArray<QiAudioChannel*> mAcquiredChannels;
	QiArray<QiAudioBuffer*> mBuffers;
};

#else

#include <SLES/OpenSLES.h>
#include "SLES/OpenSLES_Android.h"
#include "base/qisync.h"

class QiAudioStream
{
public:
	~QiAudioStream() {}
	virtual int getData(void* buffer, int size) = 0;
};


class QiAudioBuffer
{
	friend class QiAudio;
	friend class QiAudioChannel;

public:
	~QiAudioBuffer();
	int getSize() const { return 0; }

protected:
	QiAudioBuffer(class QiAudio* audio);
	QiAudioBuffer(class QiAudio* audio, QiAudioStream* stream, int frequency, int channels);

	class QiAudio* mAudio;
	short int* mData;
	int mSampleCount;
	class QiStreamingAudioBuffer* mStreamingBuffer;
};


class QiAudioChannel
{
	friend class QiAudio;
	friend class QiAudioBuffer;

public:
	~QiAudioChannel();
	void setBuffer(const QiAudioBuffer* buffer);
	inline const QiAudioBuffer* getBuffer() const { return mBuffer; }

	void setVolume(float volume);
	inline float getVolume() const { return mVolume; }

	void setPitch(float pitch);
	inline float getPitch() const { return mPitch; }

	void setLooping(bool enabled);
	inline bool isLooping() const { return mLooping; }

	void play();
	void stop();
	bool isPlaying() const;
	bool isStreaming() const { return mStreaming; }

	void callback();
	
	bool init();
	void shutdown();

protected:
	QiAudioChannel(QiAudio* audio);
	void updateStreaming();

	class QiAudio* mAudio;

	SLObjectItf mPlayerObject;
	SLPlayItf mPlayItf;
	SLAndroidSimpleBufferQueueItf mBufferQueueItf;
	SLPlaybackRateItf mRateItf;
	SLVolumeItf mVolumeItf;

	const QiAudioBuffer* mBuffer;
	float mVolume;
	float mPitch;
	bool mLooping;
	int mTimeStamp;
	bool mStreaming;
	bool mStopStreaming;
	bool mWantToStop;
	bool mMasterPaused;
	bool mInitialized;
	QiMutex mMutex;
};


class QiAudio
{
	friend class QiAudioBuffer;
	friend class QiAudioChannel;

public:
	QiAudio();
	~QiAudio();

	void init(int channels);
	void shutdown();
	
	void startEngine();
	void stopEngine();
	
	void updateStreaming();

	QiAudioBuffer* createBuffer(short int* data, int size, int frequency, int channels, int bitsPerSample);
	QiAudioBuffer* createStreamingBuffer(QiAudioStream* stream, int frequency, int channels, int bitsPerSample);

	void destroyBuffer(QiAudioBuffer* buffer);

	QiAudioChannel* acquireChannel();
	void releaseChannel(QiAudioChannel* channel);

	void play(const QiAudioBuffer* buffer, float volume=1.0f, float pitch=1.0f);

	void setMasterVolume(float volume);
	float getMasterVolume() const;
	
	void setMasterPause(bool paused);
	bool isMasterPaused() const { return mMasterPause; }

protected:
	QiAudioChannel* findBestChannel();

	SLObjectItf mEngineObject;
	SLEngineItf mEngineItf;
	SLObjectItf mOutputMixObject;

	int mTimeStamp;

	QiArray<QiAudioChannel*> mFreeChannels;
	QiArray<QiAudioChannel*> mAcquiredChannels;
	QiArray<QiAudioBuffer*> mBuffers;
	
	bool mMasterPause;
};

#endif

