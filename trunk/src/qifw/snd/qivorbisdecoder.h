/* Qi Framework. Copyright 2007-2012 Dengu AB */

#pragma once

class QiVorbisDecoder
{
public:
	QiVorbisDecoder();
	QiVorbisDecoder(class QiInputStream& stream, int size);
	~QiVorbisDecoder();

	bool init(class QiInputStream& stream, int size);
	void shutdown();
	int getChannelCount() const;
	int getFrequency() const;
	int getBitsPerSample() const { return 16; }
	int getBitRate() const;

	int decode(void* buffer, int size);
	bool decodeAll(class QiOutputStream& stream);

protected:
	bool loadPacket();
	int decodeInternal(char* buffer, int maxSize);

	class QiVorbisDecoderImpl* mImpl;
	class QiInputStream* mStream;
	int mStreamSize;
	bool mInitialized;
};


