/* Qi Framework. Copyright 2007-2012 Dengu AB */

#pragma once

class QiWavDecoder
{
public:
	QiWavDecoder();
	QiWavDecoder(class QiInputStream& stream, int size);
	~QiWavDecoder();

	bool init(class QiInputStream& stream, int size);
	void shutdown();

	inline int getChannelCount() const { return mChannels; }
	inline int getFrequency() const { return mFrequency; }
	inline int getBitsPerSample() const { return mBitsPerSample; }
	inline int getBitRate() const { return mBitRate; }

	int decode(void* buffer, int size);
	void decodeAll(class QiOutputStream& stream);

protected:
	QiInputStream* mStream;
	int mChannels;
	int mFrequency;
	int mBitsPerSample;
	int mBitRate;

	int mRemaining;
};

