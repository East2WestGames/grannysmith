/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "qiaudio.h"
#include "base/qimath.h"
#include "base/qithread.h"

#ifndef QI_ANDROID

#ifdef QI_USE_OPENAL

#ifdef QI_WIN32
#include "external/OpenAL/al.h"
#include "external/OpenAL/alc.h"
#pragma comment(lib, "external/OpenAL32.lib")
#else
#include <OpenAL/al.h>
#include <OpenAL/alc.h>
#endif

static const int STREAMING_BUFFER_COUNT = 24;
static const int STREAMING_BUFFER_SIZE = 2048;

inline ALenum getFormat(int channels, int bitsPerSample)
{
	if (channels == 1)
	{
		if (bitsPerSample == 8)
			return AL_FORMAT_MONO8;
		else if (bitsPerSample == 16)
			return AL_FORMAT_MONO16;
		else
			return 0;
	}
	else if (channels == 2)
	{
		if (bitsPerSample == 8)
			return AL_FORMAT_STEREO8;
		else if (bitsPerSample == 16)
			return AL_FORMAT_STEREO16;
		else
			return 0;
	}
	return 0;
}


class QiStreamingAudioBuffer
{
public:
	inline QiStreamingAudioBuffer(QiAudioStream* stream, int frequency, int format) : 
	mStream(stream), mCurrentBuffer(0), mFrequency(frequency), mFormat(format)
	{
		memset(mBufferSize, 0, sizeof(int)*STREAMING_BUFFER_COUNT);
		alGenBuffers(STREAMING_BUFFER_COUNT, mBuffers); 
	}

	inline ~QiStreamingAudioBuffer()
	{
		alDeleteBuffers(STREAMING_BUFFER_COUNT, mBuffers); 
	}

	inline void fillBuffer(int i)
	{
		mBufferSize[i] = mStream->getData(mBufferData[i], STREAMING_BUFFER_SIZE);
		if (mBufferSize[i] > 0)
			alBufferData(mBuffers[i], mFormat, mBufferData[i], mBufferSize[i], mFrequency);
	}

	ALuint mBuffers[STREAMING_BUFFER_COUNT];
	char mBufferData[STREAMING_BUFFER_COUNT][STREAMING_BUFFER_SIZE];
	int mBufferSize[STREAMING_BUFFER_COUNT];
	QiAudioStream* mStream;
	int mCurrentBuffer;
	int mFrequency;
	ALuint mFormat;
};


/////////////////////// BUFFERS ////////////////////////////////


QiAudioBuffer::QiAudioBuffer(QiAudio* audio) : mAudio(audio), mBytes(0)
{
	//Regular audio buffer
	alGenBuffers(1, (ALuint*)&mInternal); 
	mStreamingBuffer = NULL;
}


QiAudioBuffer::QiAudioBuffer(QiAudio* audio, QiAudioStream* stream, int frequency, int format) : mAudio(audio), mBytes(0)
{
	//Streaming audio buffer
	mInternal = NULL;
	mStreamingBuffer = QI_NEW QiStreamingAudioBuffer(stream, frequency, format);
}


QiAudioBuffer::~QiAudioBuffer()
{
	if (mInternal)
		alDeleteBuffers(1, (ALuint*)&mInternal);
	if (mStreamingBuffer)
		QI_DELETE(mStreamingBuffer);
}


/////////////////////// CHANNELS ////////////////////////////////


QiAudioChannel::QiAudioChannel(QiAudio* audio) : mAudio(audio), mBuffer(NULL), 
mVolume(1.0f), mPitch(1.0f), mLooping(false), mTimeStamp(0), mStreaming(false), mStopStreaming(false)
{
	alGenSources(1, (ALuint*)&mInternal); 
}


QiAudioChannel::~QiAudioChannel()
{
	setBuffer(NULL);
	alDeleteSources(1, (ALuint*)&mInternal);
}


void QiAudioChannel::setBuffer(const QiAudioBuffer* buffer)
{
	//Stop playing here
	bool wasPlaying = isPlaying();
	alSourceStop(mInternal);

	//Unqueue streaming buffers if necessary
	if (mBuffer && mBuffer->mStreamingBuffer)
	{
		ALuint tmp = 0;
		for(int i=0; i<STREAMING_BUFFER_COUNT; i++)
			alSourceUnqueueBuffers(mInternal, 1, &tmp);
		for(int i=0; i<STREAMING_BUFFER_COUNT; i++)
			mBuffer->mStreamingBuffer->mBufferSize[i] = 0;

		//Recreate channel due to bugs in fucking OpenAL. Rewind should be enough.
		alDeleteSources(1, (ALuint*)&mInternal);
		alGenSources(1, (ALuint*)&mInternal); 
	}
	
	mBuffer = buffer;
	if (mBuffer)
	{
		mStreaming = (mBuffer && mBuffer->mStreamingBuffer);
		if (isStreaming())
		{
			//Recreate channel due to bugs in fucking OpenAL. Rewind should be enough.
			alDeleteSources(1, (ALuint*)&mInternal);
			alGenSources(1, (ALuint*)&mInternal); 

			//Load up new buffers
			for(int i=0; i<STREAMING_BUFFER_COUNT; i++)
			{
				mBuffer->mStreamingBuffer->fillBuffer(i);
				if (mBuffer->mStreamingBuffer->mBufferSize[i] > 0)
					alSourceQueueBuffers(mInternal, 1, mBuffer->mStreamingBuffer->mBuffers+i);
			}
			mBuffer->mStreamingBuffer->mCurrentBuffer = 0;
		}
		else
			alSourcei(mInternal, AL_BUFFER, mBuffer->mInternal);

		//Start playing again if it was before
		if (wasPlaying)
			alSourcePlay(mInternal);
	}
	else
	{
		mStreaming = false;
	}
}


void QiAudioChannel::updateStreaming()
{
	if (isStreaming() && !mStopStreaming)
	{
		ALint processed;
		alGetSourcei(mInternal, AL_BUFFERS_PROCESSED, &processed);
		for(int i=0; i<processed; i++)
		{
			//Unqueue buffer
			ALuint tmp = 0;
			alSourceUnqueueBuffers(mInternal, 1, &tmp);

			//Find the corresponding audio buffer
			int b = -1;
			for(int i=0; i<STREAMING_BUFFER_COUNT; i++)
			{
				if (mBuffer->mStreamingBuffer->mBuffers[i] == tmp)
					b = i;
			}

			//Fill in the unqueued buffer and put it back on the queue
			if (b != -1)
			{
				mBuffer->mStreamingBuffer->fillBuffer(b);
				if (mBuffer->mStreamingBuffer->mBufferSize[b] > 0)
					alSourceQueueBuffers(mInternal, 1, &tmp);
			}
		}
		if (!mStopStreaming && !isPlaying())
		{
			//Stream must have stopped incidentally
			play();
		}
	}
}


void QiAudioChannel::setVolume(float volume)
{
	mVolume = volume;
	alSourcef(mInternal, AL_GAIN, mVolume);
}


void QiAudioChannel::setPitch(float pitch)
{
	mPitch = pitch;
	alSourcef(mInternal, AL_PITCH, mPitch);
}


void QiAudioChannel::setLooping(bool enabled)
{
	mLooping = enabled;
	alSourcei(mInternal, AL_LOOPING, mLooping ? 1 : 0);
}

void QiAudioChannel::play()
{
	alSourcePlay(mInternal);
	mStopStreaming = false;
}


void QiAudioChannel::stop()
{
	alSourceStop(mInternal);
	mStopStreaming = true;
}


bool QiAudioChannel::isPlaying() const
{
	ALenum state;
	alGetSourcei(mInternal, AL_SOURCE_STATE, &state);
	return state == AL_PLAYING;
}


/////////////////////// AUDIO ////////////////////////////////


QiAudio::QiAudio() : mContext(NULL), mDevice(NULL), mTimeStamp(0)
{
}


QiAudio::~QiAudio()
{
	shutdown();
}


void QiAudio::init(int channels)
{
	shutdown();
	mDevice = alcOpenDevice(NULL);
	if (mDevice) {
		mContext=alcCreateContext((ALCdevice*)mDevice,NULL);
		alcMakeContextCurrent((ALCcontext*)mContext);
	}

	for(int i=0; i<channels; i++)
	{
		QiAudioChannel* c = QI_NEW QiAudioChannel(this);
		mFreeChannels.add(c);
	}
}


void QiAudio::shutdown()
{
	for(int i=0; i<mFreeChannels.getCount(); i++)
		QI_DELETE(mFreeChannels[i]);
	mFreeChannels.clear();

	for(int i=0; i<mAcquiredChannels.getCount(); i++)
		QI_DELETE(mAcquiredChannels[i]);
	mAcquiredChannels.clear();

	for(int i=0; i<mBuffers.getCount(); i++)
		QI_DELETE(mBuffers[i]);
	mBuffers.clear();

	if (mContext)
	{
		alcDestroyContext((ALCcontext*)mContext);
		mContext = NULL;
	}
	if (mDevice)
	{
		alcCloseDevice((ALCdevice*)mDevice);
		mDevice = NULL;
	}
}


void QiAudio::updateStreaming()
{
	for(int i=0; i<mFreeChannels.getCount(); i++)
	{
		if (mFreeChannels[i]->isStreaming())
			mFreeChannels[i]->updateStreaming();
	}
	for(int i=0; i<mAcquiredChannels.getCount(); i++)
	{
		if (mAcquiredChannels[i]->isStreaming())
			mAcquiredChannels[i]->updateStreaming();
	}
}


QiAudioBuffer* QiAudio::createBuffer(short int* data, int size, int frequency, int channels, int bitsPerSample)
{
	ALenum format = getFormat(channels, bitsPerSample);
	if (!format)
		return NULL;

	QiAudioBuffer* b = QI_NEW QiAudioBuffer(this);
	b->mBytes = size;

	alBufferData(b->mInternal, format, data, size, frequency);
	mBuffers.add(b);
	return b;
}


QiAudioBuffer* QiAudio::createStreamingBuffer(QiAudioStream* stream, int frequency, int channels, int bitsPerSample)
{
	ALenum format = getFormat(channels, bitsPerSample);
	if (!format)
		return NULL;

	QiAudioBuffer* b = QI_NEW QiAudioBuffer(this, stream, frequency, format);

	mBuffers.add(b);
	return b;	
}


void QiAudio::destroyBuffer(QiAudioBuffer* buffer)
{
	for(int i=0; i<mFreeChannels.getCount(); i++)
	{
		if (mFreeChannels[i]->mBuffer == buffer)
		{
			mFreeChannels[i]->stop();
			mFreeChannels[i]->setBuffer(NULL);
		}
	}
	for(int i=0; i<mAcquiredChannels.getCount(); i++)
	{
		if (mAcquiredChannels[i]->mBuffer == buffer)
		{
			mAcquiredChannels[i]->stop();
			mAcquiredChannels[i]->setBuffer(NULL);
		}
	}
	mBuffers.removeAll(buffer);
	QI_DELETE(buffer);
}


QiAudioChannel* QiAudio::acquireChannel()
{
	QiAudioChannel* c = findBestChannel();
	if (c)
	{
		mFreeChannels.removeAll(c);
		mAcquiredChannels.add(c);
		c->setPitch(1.0f);
		c->setVolume(1.0f);
		c->setLooping(false);
	}
	return c;
}


void QiAudio::releaseChannel(QiAudioChannel* channel)
{
	if (!channel)
		return;
	channel->stop();
	channel->setBuffer(NULL);
	channel->setLooping(false);
	mAcquiredChannels.removeAll(channel);
	mFreeChannels.add(channel);
}


void QiAudio::play(const QiAudioBuffer* buffer, float volume, float pitch)
{
	if (!buffer)
		return;
	QiAudioChannel* c = findBestChannel();
	if (c)
	{
		c->setLooping(false);
		c->setBuffer(buffer);
		c->setVolume(volume);
		c->setPitch(pitch);
		c->play();
	}
}


void QiAudio::setMasterVolume(float volume)
{
	alListenerf(AL_GAIN, volume);
}


float QiAudio::getMasterVolume() const
{
	float tmp=0.0f;
	alGetListenerf(AL_GAIN, &tmp);
	return tmp;
}


int QiAudio::getAllocatedBytes() const
{
	int b=0;
	for(int i=0; i<mBuffers.getCount(); i++)
		b += mBuffers[i]->mBytes;
	return b;
}


QiAudioChannel* QiAudio::findBestChannel()
{
	int oldest = QI_INT_MAX;
	QiAudioChannel* best = NULL;
	for(int i=0; i<mFreeChannels.getCount(); i++)
	{
		if (!mFreeChannels[i]->isPlaying())
		{
			best = mFreeChannels[i];
			break;
		}
		else if (mFreeChannels[i]->mTimeStamp < oldest)
		{
			best = mFreeChannels[i];
			oldest = best->mTimeStamp;
		}
	}
	if (best)
	{
		static int timeStamp = 0;
		best->mTimeStamp = timeStamp++;
		best->stop();
	}
	return best;
}

#else

QiAudioChannel::~QiAudioChannel() {}
void QiAudioChannel::setBuffer(const QiAudioBuffer* buffer) {}
void QiAudioChannel::setVolume(float volume) {}
void QiAudioChannel::setPitch(float pitch) {}
void QiAudioChannel::setLooping(bool enabled) {}

void QiAudioChannel::play() {}
void QiAudioChannel::stop() {}
bool QiAudioChannel::isPlaying() const { return false; }

QiAudio::QiAudio() {}
QiAudio::~QiAudio() {}

void QiAudio::init(int channels) {}
void QiAudio::shutdown() {}
void QiAudio::updateStreaming() {}

QiAudioBuffer* QiAudio::createBuffer(short int* data, int size, int frequency, int channels, int bitsPerSample) { return NULL; }
QiAudioBuffer* QiAudio::createStreamingBuffer(QiAudioStream* stream, int frequency, int channels, int bitsPerSample) { return NULL; }

void QiAudio::destroyBuffer(QiAudioBuffer* buffer) {}

QiAudioChannel::QiAudioChannel(QiAudio* audio) {}

QiAudioChannel* QiAudio::acquireChannel() { return QI_NEW QiAudioChannel(NULL); }
void QiAudio::releaseChannel(QiAudioChannel* channel) { QI_DELETE(channel); }

void QiAudio::play(const QiAudioBuffer* buffer, float volume, float pitch) {}

void QiAudio::setMasterVolume(float volume) {}
float QiAudio::getMasterVolume() const { return 0.0f; }

#endif

#endif

