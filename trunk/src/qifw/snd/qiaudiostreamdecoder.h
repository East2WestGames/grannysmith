/* Qi Framework. Copyright 2007-2012 Dengu AB */

#pragma once

#include "base/qistream.h"
#include "snd/qiaudio.h"

//Wrapper for feeding data to an audio stream from an audio decoder
template <class T>
class QiAudioStreamDecoder : public QiAudioStream
{
public:

	inline QiAudioStreamDecoder()
	{
	}

	inline QiAudioStreamDecoder(QiInputStream& stream, int size)
	{
		mDecoder.init(stream, size);
	}

	inline void init(QiInputStream& stream, int size)
	{
		mDecoder.init(stream, size);
	}

	virtual int getData(void* data, int size)
	{
		return mDecoder.decode(data, size);
	}

	inline int getFrequency() { return mDecoder.getFrequency(); }
	inline int getChannelCount() { return mDecoder.getChannelCount(); }
	inline int getBitsPerSample() { return mDecoder.getBitsPerSample(); }

protected:
	T mDecoder;
};

