/* Qi Framework. Copyright 2007-2012 Dengu AB */

#pragma once

#include "base/qistream.h"

class QiTheoraDecoder
{
public:
	QiTheoraDecoder();
	QiTheoraDecoder(class QiInputStream& stream, int size);
	~QiTheoraDecoder();

	bool init(class QiInputStream& stream, int size);
	void shutdown();

	void setPostProcessingLevel(int level);

	int getWidth() const;
	int getHeight() const;
	float getFps() const;

	bool hasFrame() const;
	void decodeFrameYUV(unsigned char* yuvBuffer);
	void decodeFrameRGB(unsigned char* rgbBuffer);

protected:
	void decodeFrame(bool convertToRgb);
	void writeFrame(bool convertToRgb);
	int buffer_data();
	int queue_page();

	class QiTheoraDecoderImpl* mImpl;
	class QiInputStream* mStream;
	int mStreamSize;
	int mBytesRead;
	bool mInitialized;
	unsigned char* mBuffer;
};


