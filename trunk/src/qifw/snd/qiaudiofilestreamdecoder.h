/* Qi Framework. Copyright 2007-2012 Dengu AB */

#pragma once

#include "file/qifileinputstream.h"
#include "snd/qiaudio.h"

//Wrapper for feeding data to an audio stream from an audio decoder
template <class T>
class QiAudioFileStreamDecoder : public QiAudioStream
{
public:

	inline QiAudioFileStreamDecoder()
	{
	}

	inline QiAudioFileStreamDecoder(QiString& fileName, bool loop = false)
	{
		init(fileName, loop);
	}

	inline bool init(const QiString& fileName, bool loop = false)
	{
		mFileName = fileName;
		mStream.open(fileName);
		mLoop = loop;
		if (mStream.isOpen())
		{
			mDecoder.init(mStream, mStream.getSize());
			return true;
		}
		return false;
	}

	virtual int getData(void* data, int size)
	{
		int ret = mDecoder.decode(data, size);
		if (ret == 0 && mLoop)
		{
			//End of stream, reopen if looping is enabled
			mStream.close();
			init(mFileName, true);
			ret = mDecoder.decode(data, size);
		}
		return ret;
	}

	inline int getFrequency() { return mDecoder.getFrequency(); }
	inline int getChannelCount() { return mDecoder.getChannelCount(); }
	inline int getBitsPerSample() { return mDecoder.getBitsPerSample(); }

protected:
	T mDecoder;

	QiString mFileName;
	QiFileInputStream mStream;
	bool mLoop;
};

