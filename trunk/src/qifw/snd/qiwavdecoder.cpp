/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qistream.h"
#include "snd/qiwavdecoder.h"

#include <string.h>


QiWavDecoder::QiWavDecoder()
{
}


QiWavDecoder::QiWavDecoder(class QiInputStream& stream, int size)
{
	init(stream, size);
}


QiWavDecoder::~QiWavDecoder()
{
}


bool QiWavDecoder::init(class QiInputStream& stream, int size)
{
	int m0 = stream.getInputByteCount();
	
	char riff[4];
	stream.readBuffer(riff, 4);
	if (strncmp(riff, "RIFF", 4)!=0)
		return false;

	int siz=0;
	stream.readInt32(siz);
	if (siz==0)
		return false;
	
	char wave[4];
	stream.readBuffer(wave, 4);
	if (strncmp(wave, "WAVE", 4)!=0)
		return false;

	char fmt[4];
	stream.readBuffer(fmt, 4);
	if (strncmp(fmt, "fmt ", 4)!=0)
		return false;

	int formatLength=0;
	stream.readInt32(formatLength);

	short formatTag=0;
	stream.readInt16(formatTag);

	short channels=0;
	stream.readInt16(channels);

	int sampleRate=0;
	stream.readInt32(sampleRate);

	int avgBytesPerSec=0;
	stream.readInt32(avgBytesPerSec);

	short blockAlign=0;
	stream.readInt16(blockAlign);

	short bitsPerSample=0;
	stream.readInt16(bitsPerSample);

	for(int i=0; i<formatLength-16; i++)
	{
		char c;
		stream.readInt8(c);
	}

	char dat[4];
	stream.readBuffer(dat, 4);

	int dataSize;
	stream.readInt32(dataSize);

	mChannels = channels;
	mBitsPerSample = bitsPerSample;
	mFrequency = sampleRate;
	mBitRate = avgBytesPerSec * 8;

	int m1 = stream.getInputByteCount();
	int posInStream = m1-m0;

	mStream = &stream;
	mRemaining = QiMin(dataSize, size-posInStream);
	return true;
}


void QiWavDecoder::shutdown()
{
}


int QiWavDecoder::decode(void* buffer, int size)
{
	int toCopy = QiMin(size, mRemaining);
	if (!mStream->readBuffer(buffer, toCopy))
	{
		mRemaining = 0;
		return 0;
	}
	mRemaining-=toCopy;
	return toCopy;
}


void QiWavDecoder::decodeAll(class QiOutputStream& stream)
{
	mStream->readBuffer(stream, mRemaining);
	mRemaining = 0;
}

