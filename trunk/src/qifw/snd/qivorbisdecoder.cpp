/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "base/qistream.h"
#include "base/qidebug.h"
#include "base/qimath.h"
#include "base/qimem.h"
#include "snd/qivorbisdecoder.h"

#include "vorbis/ogg.h"
#include "vorbis/codec.h"

class QiVorbisDecoderImpl
{
public:
	ogg_sync_state		oy; /* sync and verify incoming physical bitstream */
	ogg_stream_state	os; /* take physical pages, weld into a logical
							stream of packets */
	ogg_page			og; /* one Ogg bitstream page. Vorbis packets are inside */
	ogg_packet			op; /* one raw packet of data for decode */

	vorbis_info			vi; /* struct that stores all the static vorbis bitstream
							settings */
	vorbis_comment		vc; /* struct that stores all the bitstream user comments */
	vorbis_dsp_state	vd; /* central working state for the packet->PCM decoder */
	vorbis_block		vb; /* local working space for packet->PCM decode */	
};


QiVorbisDecoder::QiVorbisDecoder() : mInitialized(false)
{
	mImpl = QI_NEW QiVorbisDecoderImpl;
	memset(mImpl, 0, sizeof(QiVorbisDecoderImpl));
}


QiVorbisDecoder::QiVorbisDecoder(QiInputStream& stream, int size) : mInitialized(false)
{
	mImpl = QI_NEW QiVorbisDecoderImpl;
	memset(mImpl, 0, sizeof(QiVorbisDecoderImpl));
	init(stream, size);
}


QiVorbisDecoder::~QiVorbisDecoder()
{
	if (mInitialized)
		shutdown();
	QI_DELETE(mImpl);
}

bool QiVorbisDecoder::init(QiInputStream& stream, int size)
{
	if (mInitialized)
		shutdown();

	mStream = &stream;
	mStreamSize = size;

	ogg_sync_init(&mImpl->oy); /* Now we can read pages */

	char *buffer = ogg_sync_buffer(&mImpl->oy, 4096);

	int bytes = QiMin(mStreamSize, 4096);
	mStreamSize -= bytes;
	mStream->readBuffer(buffer, bytes);
	ogg_sync_wrote(&mImpl->oy, bytes);

	if(ogg_sync_pageout(&mImpl->oy, &mImpl->og) != 1)
	{
		return false;
	}
	ogg_stream_init(&mImpl->os, ogg_page_serialno(&mImpl->og));

	vorbis_info_init(&mImpl->vi);
	vorbis_comment_init(&mImpl->vc);
	if(ogg_stream_pagein(&mImpl->os, &mImpl->og)<0)
	{ 
		QI_WARNING("Error reading first page of Ogg bitstream data.");
		return false;
	}
    
	if(ogg_stream_packetout(&mImpl->os, &mImpl->op)!=1)
	{ 
		QI_WARNING("Error reading initial header packet.");
		return false;
	}
    
	if(vorbis_synthesis_headerin(&mImpl->vi, &mImpl->vc, &mImpl->op)<0)
	{ 
		QI_WARNING("This Ogg bitstream does not contain Vorbis audio data.");
		return false;
	}

	int i=0;
	while(i<2)
	{
		while(i<2)
		{
			int result=ogg_sync_pageout(&mImpl->oy, &mImpl->og);
			if(result==0)
			{
				/* Need more data */
				buffer = ogg_sync_buffer(&mImpl->oy, 4096);
				bytes = QiMin(mStreamSize, 4096);
				mStreamSize -= bytes;
				mStream->readBuffer(buffer, bytes);
				ogg_sync_wrote(&mImpl->oy, bytes);
				break;
			}

			/* Don't complain about missing or corrupt data yet. We'll
			catch it at the packet output phase */
			if(result==1)
			{
				ogg_stream_pagein(&mImpl->os, &mImpl->og); /* we can ignore any errors here as they'll also become apparent at packetout */
				while(i<2)
				{
					result=ogg_stream_packetout(&mImpl->os, &mImpl->op);
					if(result==0)
						break;
					if(result<0)
					{
						/* Uh oh; data at some point was corrupted or missing!
						We can't tolerate that in a header.  Die. */
						QI_WARNING("Corrupt secondary header.  Exiting.\n");
						return false;
					}
					result=vorbis_synthesis_headerin(&mImpl->vi, &mImpl->vc, &mImpl->op);
					if(result<0)
					{
						QI_WARNING("Corrupt secondary header.  Exiting.\n");
						exit(1);
					}
					i++;
				}
			}
		}
	}
	/* no harm in not checking before adding more */
	buffer=ogg_sync_buffer(&mImpl->oy, 4096);

	bytes = QiMin(mStreamSize, 4096);
	mStreamSize -= bytes;
	mStream->readBuffer(buffer, bytes);

	if(bytes==0 && i<2)
	{
		QI_WARNING("End of file before finding all Vorbis headers!\n");
		return false;
	}
	ogg_sync_wrote(&mImpl->oy, bytes);

    if(vorbis_synthesis_init(&mImpl->vd, &mImpl->vi)==0)
		vorbis_block_init(&mImpl->vd, &mImpl->vb);  

	mInitialized = true;
	return true;
}


int QiVorbisDecoder::getChannelCount() const
{
	return mImpl->vi.channels;
}


int QiVorbisDecoder::getFrequency() const
{
	return mImpl->vi.rate;
}


int QiVorbisDecoder::getBitRate() const
{
	return mImpl->vi.rate;
}


bool QiVorbisDecoder::decodeAll(class QiOutputStream& stream)
{
	const int CHUNK_SIZE = 8192;
	char tmp[CHUNK_SIZE];
	int bytes;
	while((bytes = decode(tmp, CHUNK_SIZE)) > 0)
		stream.writeBuffer(tmp, bytes);
	return true;
}


int QiVorbisDecoder::decode(void* buffer, int size)
{
	int bytesWritten = 0;
	char* dst = (char*)buffer;
	int s;
	while(bytesWritten < size)
	{
		s = decodeInternal(dst+bytesWritten, size-bytesWritten);
		bytesWritten += s;
		if (s == 0)
			break;
	}
	return bytesWritten;
}


void QiVorbisDecoder::shutdown()
{
    vorbis_block_clear(&mImpl->vb);
    vorbis_dsp_clear(&mImpl->vd);

	ogg_stream_clear(&mImpl->os);
    vorbis_comment_clear(&mImpl->vc);
    vorbis_info_clear(&mImpl->vi);
	ogg_sync_clear(&mImpl->oy);

	memset(mImpl, 0, sizeof(QiVorbisDecoderImpl));
	mInitialized = false;
}


inline bool QiVorbisDecoder::loadPacket()
{
	int result;
	while(1)
	{
		result=ogg_stream_packetout(&mImpl->os, &mImpl->op);
              
		if(result==0)
		{
			//Get more data
			while((result=ogg_sync_pageout(&mImpl->oy, &mImpl->og)) <= 0)
			{
				if (result < 0)
				{
					QI_WARNING("QiVorbisDecoder: Corrupt data stream.");
					return false;
				}

				//Check if end of stream
				//if(ogg_page_eos(&mImpl->og))
				//	return false;

				//Fetch more data
				char* buffer=ogg_sync_buffer(&mImpl->oy,4096);

				int bytes = QiMin(mStreamSize, 4096);

				//No more data in stream
				if(bytes==0)
					return false;

				mStreamSize -= bytes;
				bool ret = mStream->readBuffer(buffer, bytes);

				//Failed to read data from stream
				if (!ret)
					return false;

				ogg_sync_wrote(&mImpl->oy,bytes);
			}    
			ogg_stream_pagein(&mImpl->os, &mImpl->og);
			continue;
		}
		if(result<0)
		{
			return false;
		}
		else
		{
			/* we have a packet.  Decode it */                
			if(vorbis_synthesis(&mImpl->vb, &mImpl->op)==0) /* test for success! */
				vorbis_synthesis_blockin(&mImpl->vd, &mImpl->vb);

			return true;
		}
	}
}


int QiVorbisDecoder::decodeInternal(char* dst, int maxSize)
{
	int channels = mImpl->vi.channels;
	int sampleSize = channels == 1 ? 2 : 4;
	int samplesWritten = 0;
	int maxSamples = maxSize / sampleSize;
	ogg_int16_t* convbuffer = (ogg_int16_t*)dst;

	float **pcm;
	int samples;
	while(samplesWritten < maxSamples)
	{
		while((samples=vorbis_synthesis_pcmout(&mImpl->vd, &pcm)) > 0)
		{
			int j;
			int bout=QiMin(samples, maxSamples-samplesWritten);
                  
			/* convert floats to 16 bit signed ints (host order) and
			interleave */
			for(int i=0;i<channels;i++)
			{
				ogg_int16_t *ptr = convbuffer + samplesWritten*channels + i;
				float  *mono=pcm[i];
				for(j=0;j<bout;j++)
				{
					int val=QiFloor(mono[j]*32767.f+.5f);
					val = QiClamp(val, -32767, 32767);
					*ptr=val;
					ptr+=channels;
				}
			}
			samplesWritten += bout;

			vorbis_synthesis_read(&mImpl->vd, bout);

			if (samplesWritten == maxSamples)
				return samplesWritten * sampleSize;
		}
		if (!loadPacket())
			break;
	}
	return samplesWritten * sampleSize;
}

