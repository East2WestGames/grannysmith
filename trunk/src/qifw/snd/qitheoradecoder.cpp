/* Qi Framework. Copyright 2007-2012 Dengu AB */

#include "qitheoradecoder.h"
#include "base/qimem.h"
#include "base/qitimer.h"
#include "base/qidebug.h"

#include "vorbis/ogg.h"
#include "vorbis/codec.h"
#include "theora/theoradec.h"

#include <stdio.h>
#include <string.h>

class QiTheoraDecoderImpl
{
public:
	ogg_sync_state   oy;
	ogg_page         og;
	ogg_packet       op;
	ogg_stream_state vo;
	ogg_stream_state to;
	th_info      ti;
	th_comment   tc;
	th_dec_ctx       *td;
	th_setup_info    *ts;
	vorbis_info      vi;
	vorbis_dsp_state vd;
	vorbis_block     vb;
	vorbis_comment   vc;
	th_pixel_fmt     px_fmt;

	int pp_level_max;
	int pp_level;
	int pp_inc;

	int theora_p;
	int vorbis_p;
	int stateflag;

	int          videobuf_ready;
	ogg_int64_t  videobuf_granulepos;
	double       videobuf_time;

	/* single audio fragment audio buffering */
	int          audiobuf_fill;
	int          audiobuf_ready;
	ogg_int16_t *audiobuf;
	ogg_int64_t  audiobuf_granulepos; /* time position of last sample */

	long         audiofd_totalsize;
	int          audiofd_fragsize;
	int          audiofd;
	ogg_int64_t  audiofd_timer_calibrate;

};


QiTheoraDecoder::QiTheoraDecoder() : mStream(NULL), mStreamSize(0), mInitialized(false)
{
	mImpl = QI_NEW QiTheoraDecoderImpl;
}


QiTheoraDecoder::QiTheoraDecoder(class QiInputStream& stream, int size) : mStream(NULL), mStreamSize(0), mInitialized(false)
{
	mImpl = QI_NEW QiTheoraDecoderImpl;
	init(stream, size);
}


QiTheoraDecoder::~QiTheoraDecoder()
{
	shutdown();
	QI_DELETE(mImpl);
}


bool QiTheoraDecoder::init(class QiInputStream& stream, int size)
{
	if (mInitialized)
		shutdown();
		
	mInitialized = true;
	mStream = &stream;
	mStreamSize = size;
	mBytesRead = 0;

	//Init impl
	memset(mImpl, 0, sizeof(QiTheoraDecoderImpl));
	mImpl->videobuf_granulepos=-1;
	mImpl->audiofd_totalsize=-1;
	mImpl->audiofd=-1;
	mImpl->audiofd_timer_calibrate=-1;

	ogg_sync_init(&mImpl->oy);

	/* init supporting Vorbis structures needed in header parsing */
	vorbis_info_init(&mImpl->vi);
	vorbis_comment_init(&mImpl->vc);

	/* init supporting Theora structures needed in header parsing */
	th_comment_init(&mImpl->tc);
	th_info_init(&mImpl->ti);

	/* Ogg file open; parse the headers */
	/* Only interested in Vorbis/Theora streams */
	while(!mImpl->stateflag)
	{
		int ret=buffer_data();
		if(ret==0)break;
		while(ogg_sync_pageout(&mImpl->oy,&mImpl->og)>0)
		{
			ogg_stream_state test;

			/* is this a mandated initial header? If not, stop parsing */
			if(!ogg_page_bos(&mImpl->og))
			{
				/* don't leak the page; get it into the appropriate stream */
				queue_page();
				mImpl->stateflag=1;
				break;
			}

			ogg_stream_init(&test,ogg_page_serialno(&mImpl->og));
			ogg_stream_pagein(&test, &mImpl->og);
			ogg_stream_packetout(&test, &mImpl->op);

			/* identify the codec: try theora */
			if(!mImpl->theora_p && th_decode_headerin(&mImpl->ti,&mImpl->tc,&mImpl->ts,&mImpl->op)>=0)
			{
				/* it is theora */
				memcpy(&mImpl->to,&test,sizeof(test));
				mImpl->theora_p=1;
			}
			else if(!mImpl->vorbis_p && vorbis_synthesis_headerin(&mImpl->vi,&mImpl->vc,&mImpl->op)>=0)
			{
				/* it is vorbis */
				memcpy(&mImpl->vo,&test,sizeof(test));
				mImpl->vorbis_p=1;
			}
			else
			{
				/* whatever it is, we don't care about it */
				ogg_stream_clear(&test);
			}
		}
		/* fall through to non-bos page parsing */
	}

	/* we're expecting more header packets. */
	while((mImpl->theora_p && mImpl->theora_p<3) || (mImpl->vorbis_p && mImpl->vorbis_p<3))
	{
		int ret;

		/* look for further theora headers */
		while(mImpl->theora_p && (mImpl->theora_p<3) && (ret=ogg_stream_packetout(&mImpl->to, &mImpl->op)))
		{
			if(ret<0)
			{
				QI_WARNING("Error parsing Theora stream headers; corrupt stream?\n");
				return false;
			}
			if(!th_decode_headerin(&mImpl->ti, &mImpl->tc, &mImpl->ts, &mImpl->op))
			{
				QI_WARNING("Error parsing Theora stream headers; corrupt stream?\n");
				return false;
			}
			mImpl->theora_p++;
		}

		/* look for more vorbis header packets */
		while(mImpl->vorbis_p && (mImpl->vorbis_p<3) && (ret=ogg_stream_packetout(&mImpl->vo,&mImpl->op)))
		{
			if(ret<0)
			{
				QI_WARNING("Error parsing Vorbis stream headers; corrupt stream?");
				return false;
			}
			if(vorbis_synthesis_headerin(&mImpl->vi, &mImpl->vc, &mImpl->op))
			{
				QI_WARNING("Error parsing Vorbis stream headers; corrupt stream?");
				return false;
			}
			mImpl->vorbis_p++;
			if(mImpl->vorbis_p==3)
				break;
		}

		/* The header pages/packets will arrive before anything else we
		   care about, or the stream is not obeying spec */

		if(ogg_sync_pageout(&mImpl->oy,&mImpl->og)>0)
		{
			queue_page(); /* demux into the appropriate stream */
		}
		else
		{
			int ret=buffer_data(); /* someone needs more data */
			if(ret==0)
			{
				QI_WARNING("End of file while searching for codec headers.\n");
				return false;
			}
		}
	}

  /* and now we have it all.  initialize decoders */
	if(mImpl->theora_p)
	{
		mImpl->td=th_decode_alloc(&mImpl->ti, mImpl->ts);
		//printf("Ogg logical stream %lx is Theora %dx%d %.02f fps", mImpl->to.serialno,mImpl->ti.pic_width,mImpl->ti.pic_height, (double)mImpl->ti.fps_numerator/mImpl->ti.fps_denominator);
		mImpl->px_fmt=mImpl->ti.pixel_fmt;
		QI_ASSERT(mImpl->ti.pixel_fmt == TH_PF_420, "Only YUV 420 pixel format supported");
		if (mImpl->ti.pixel_fmt != TH_PF_420)
			return false;

		setPostProcessingLevel(0);
	}
	else
	{
		/* tear down the partial theora setup */
		th_info_clear(&mImpl->ti);
		th_comment_clear(&mImpl->tc);
	}
  
	th_setup_free(mImpl->ts);

	if(mImpl->vorbis_p)
	{
		vorbis_synthesis_init(&mImpl->vd,&mImpl->vi);
		vorbis_block_init(&mImpl->vd,&mImpl->vb);
		//fprintf(stderr,"Ogg logical stream %lx is Vorbis %d channel %ld Hz audio.\n", mImpl->vo.serialno,mImpl->vi.channels, mImpl->vi.rate);
	}
	else
	{
		/* tear down the partial vorbis setup */
		vorbis_info_clear(&mImpl->vi);
		vorbis_comment_clear(&mImpl->vc);
	}

	mImpl->stateflag=0;

	if (mImpl->theora_p)
		return true;
	else
		return false;
}


void QiTheoraDecoder::shutdown()
{
	if (!mInitialized)
		return;
	if(mImpl->vorbis_p)
	{
		ogg_stream_clear(&mImpl->vo);
		vorbis_block_clear(&mImpl->vb);
		vorbis_dsp_clear(&mImpl->vd);
		vorbis_comment_clear(&mImpl->vc);
		vorbis_info_clear(&mImpl->vi);
	}
	if(mImpl->theora_p)
	{
		
		ogg_stream_clear(&mImpl->to);
		//th_decode_free(mImpl->td);
		th_comment_clear(&mImpl->tc);
		th_info_clear(&mImpl->ti);
	}
	ogg_sync_clear(&mImpl->oy);
	mInitialized = true;
}


int QiTheoraDecoder::getWidth() const
{
	return mImpl->ti.pic_width;
}


int QiTheoraDecoder::getHeight() const
{
	return mImpl->ti.pic_height;
}


float QiTheoraDecoder::getFps() const
{
	return mImpl->ti.fps_numerator/(float)mImpl->ti.fps_denominator;
}


void QiTheoraDecoder::setPostProcessingLevel(int level)
{
	th_decode_ctl(mImpl->td,TH_DECCTL_SET_PPLEVEL,&level,sizeof(level));
}


bool QiTheoraDecoder::hasFrame() const
{
	return mBytesRead < mStreamSize;
}


void QiTheoraDecoder::decodeFrameYUV(unsigned char* yuvBuffer)
{
	mBuffer = yuvBuffer;
	decodeFrame(false);
}


void QiTheoraDecoder::decodeFrameRGB(unsigned char* rgbBuffer)
{
	mBuffer = rgbBuffer;
	decodeFrame(true);
}


#define OC_CLAMP255(_x) ((unsigned char)((((_x)<0)-1)&((_x)|-((_x)>255))))
void QiTheoraDecoder::writeFrame(bool convertToRgb)
{
	th_ycbcr_buffer yuv;
	th_decode_ycbcr_out(mImpl->td, yuv);

	th_ycbcr_buffer& _ycbcr = yuv;
    unsigned char   *data;
    unsigned char   *y_row;
    unsigned char   *u_row;
    unsigned char   *v_row;
    unsigned char   *rgb_row;
    int              cstride;
    int              w;
    int              h;
    int              x;
    int              y;
    int              hdec;
    int              vdec;
    w=_ycbcr[0].width;
    h=_ycbcr[0].height;

	data = mBuffer;
    cstride = w*3;
    y_row=_ycbcr[0].data;
    u_row=_ycbcr[1].data;
    v_row=_ycbcr[2].data;
    rgb_row=data;
	if (!convertToRgb)
	{
		//Expend decoded frame to YUV 888 
	    for(y=0;y<h;y+=2)
		{
			int p=0;
			int pn=cstride;
			int yi = 0;
			int yin = _ycbcr[0].stride;
	    	for(x=0;x<w;x+=2)
			{
				int hx = x>>1;
				unsigned char u = u_row[hx];
				unsigned char v = v_row[hx];
				rgb_row[p++] = y_row[yi++];
				rgb_row[p++] = u;
				rgb_row[p++] = v;
				rgb_row[p++] = y_row[yi++];
				rgb_row[p++] = u;
				rgb_row[p++] = v;
				rgb_row[pn++] = y_row[yin++];
				rgb_row[pn++] = u;
				rgb_row[pn++] = v;
				rgb_row[pn++] = y_row[yin++];
				rgb_row[pn++] = u;
				rgb_row[pn++] = v;
			}
			rgb_row+=2*cstride;
			y_row+=2*_ycbcr[0].stride;
			u_row+=_ycbcr[1].stride;
			v_row+=_ycbcr[2].stride;
		}
	}
	else
	{		
		//Expend and convert decoded frame to RGB 888 
		hdec = 1;
		vdec = 1;
	    for(y=0; y<h; y++)
		{
			for(x=0; x<w; x++)
			{
		        int r;
		        int g;
		        int b;
		        r=(1904000*y_row[x]+2609823*v_row[x>>hdec]-363703744)/1635200;
		        g=(3827562*y_row[x]-1287801*u_row[x>>hdec]-2672387*v_row[x>>hdec]+447306710)/3287200;
		        b=(952000*y_row[x]+1649289*u_row[x>>hdec]-225932192)/817600;
		        rgb_row[3*x+0]=OC_CLAMP255(r);
		        rgb_row[3*x+1]=OC_CLAMP255(g);
		        rgb_row[3*x+2]=OC_CLAMP255(b);
			}
			y_row+=_ycbcr[0].stride;
			u_row+=_ycbcr[1].stride&-((y&1)|!vdec);
			v_row+=_ycbcr[2].stride&-((y&1)|!vdec);
			rgb_row+=cstride;
		}
	}
	return;

}

void QiTheoraDecoder::decodeFrame(bool convertToRgb)
{
	mImpl->pp_level = -1;
	int i, j;

	bool frameWritten = false;
	while(!frameWritten)
	{
		/* we want a video and audio frame ready to go at all times.  If
			we have to buffer incoming, buffer the compressed data (ie, let
			ogg do the buffering) */
		while(mImpl->vorbis_p && !mImpl->audiobuf_ready)
		{
			int ret;
			float **pcm;

			/* if there's pending, decoded audio, grab it */
			if((ret=vorbis_synthesis_pcmout(&mImpl->vd,&pcm))>0)
			{
				int count=mImpl->audiobuf_fill/2;
				int maxsamples=(mImpl->audiofd_fragsize-mImpl->audiobuf_fill)/2/mImpl->vi.channels;
				for(i=0;i<ret && i<maxsamples;i++)
					for(j=0;j<mImpl->vi.channels;j++)
					{
						int val=int(pcm[j][i]*32767.f);
						if(val>32767)
							val=32767;
						if(val<-32768)
							val=-32768;
			            mImpl->audiobuf[count++]=val;
					}
		        vorbis_synthesis_read(&mImpl->vd,i);
				mImpl->audiobuf_fill+=i*mImpl->vi.channels*2;
				if(mImpl->audiobuf_fill==mImpl->audiofd_fragsize)
					mImpl->audiobuf_ready=1;
				if(mImpl->vd.granulepos>=0)
					mImpl->audiobuf_granulepos=mImpl->vd.granulepos-ret+i;
				else
					mImpl->audiobuf_granulepos+=i;
			}
			else
			{
				/* no pending audio; is there a pending packet to decode? */
				if(ogg_stream_packetout(&mImpl->vo,&mImpl->op)>0)
				{
					if(vorbis_synthesis(&mImpl->vb,&mImpl->op)==0) /* test for success! */
						vorbis_synthesis_blockin(&mImpl->vd,&mImpl->vb);
				}
				else   /* we need more data; break out to suck in another page */
					break;
			}
		}

		while(mImpl->theora_p && !mImpl->videobuf_ready)
		{
			/* theora is one in, one out... */
			if(ogg_stream_packetout(&mImpl->to,&mImpl->op)>0)
			{
				if(th_decode_packetin(mImpl->td,&mImpl->op,&mImpl->videobuf_granulepos)==0)
				{
					mImpl->videobuf_time=th_granule_time(mImpl->td,mImpl->videobuf_granulepos);
					mImpl->videobuf_ready=1;
				}
			}
			else
		        break;
		}

		if(!mImpl->videobuf_ready && !mImpl->audiobuf_ready && mBytesRead==mStreamSize)
			break;

		if(!mImpl->videobuf_ready || !mImpl->audiobuf_ready)
		{
			/* no data yet for somebody.  Grab another page */
			buffer_data();
			while(ogg_sync_pageout(&mImpl->oy,&mImpl->og)>0)
				queue_page();
		}

		/* If playback has begun, top audio buffer off immediately. */
		//if(mImpl->stateflag)
		//	audio_write();

		/* are we at or past time for this video frame? */
		if(mImpl->stateflag && mImpl->videobuf_ready){
			writeFrame(convertToRgb);
			mImpl->videobuf_ready=0;
			frameWritten = true;
		}

		/* if our buffers either don't exist or are ready to go,
		we can begin playback */
		if((!mImpl->theora_p || mImpl->videobuf_ready) && (!mImpl->vorbis_p || mImpl->audiobuf_ready))
			mImpl->stateflag=1;
		/* same if we've run out of input */
		if(mBytesRead == mStreamSize)
			mImpl->stateflag=1;
	}
}


int QiTheoraDecoder::buffer_data()
{
	char *buffer=ogg_sync_buffer(&mImpl->oy,4096);
	int bytes=QiMin(4096, mStreamSize-mBytesRead);
	if (!mStream->readBuffer(buffer, bytes))
		return 0;
	ogg_sync_wrote(&mImpl->oy, bytes);
	mBytesRead += bytes;
	return bytes;
}


int QiTheoraDecoder::queue_page()
{
	if(mImpl->theora_p)
		ogg_stream_pagein(&mImpl->to, &mImpl->og);
	if(mImpl->vorbis_p)
		ogg_stream_pagein(&mImpl->vo, &mImpl->og);
	return 0;
}


