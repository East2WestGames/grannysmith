/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "scene.h"
#include "file/qipath.h"


Scene::Scene() : mScript(&mResMan), mDrawCallCount(0)
{
}


Scene::~Scene()
{
	unload();
}


void Scene::load(const QiString& path)
{
	QiString basePath = QiPath::getDirPart(path);
	mResMan.setAdditionalPath(basePath);
	mPath = path;
	mScript.load(path);
}


void Scene::unload()
{
	mScript.unload();
	mResMan.clear();
	mPath = "";
}


void Scene::draw()
{
	int dcc = gGame->mRenderer->getDrawCallCount();
	mScript.tick();
	mScript.draw();
	mDrawCallCount = gGame->mRenderer->getDrawCallCount() - dcc;
}


void Scene::reload()
{
	mScript.unload();
	mScript.load(mPath);
}

