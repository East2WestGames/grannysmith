/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "gfx.h"
#include "resman.h"
#include "game.h"
#include "device.h"
#include "display.h"

Gfx::Gfx(ResMan* resMan)
{
	mFmt.addField("aPosition", mFmt.FLOAT32, 2);
	mFmt.addField("aTexCoord", mFmt.FLOAT32, 2);
	for(int i=0; i<5; i++)
	{
		int res = (2 << i);
		mVbo[i].init(mFmt, res*res);
		mIbo[i].init((res-1)*(res-1)*6);
		for(int y=0; y<res; y++)
		{
			for(int x=0; x<res; x++)
			{
				QiVec2 p(x/float(res-1), y/float(res-1));
				QiVec2 t(x/float(res-1), y/float(res-1));
				mVbo[i].vertex();
				mVbo[i].addFast(p);
				mVbo[i].addFast(t);
				if (x < res-1 && y < res-1)
					mIbo[i].quad(y*res+x, (y+1)*res+x, (y+1)*res+x+1, y*res+x+1);
			}
		}
		mVbo[i].makeVbo();
		mIbo[i].makeIbo();
	}

	m2DShader = resMan->acquireShader("shaders/2d.glsl");
	m2DTexShader = resMan->acquireShader("shaders/2dtex.glsl");
	mFontShader = resMan->acquireShader("shaders/font.glsl");
	mBodyShader = resMan->acquireShader("shaders/body.glsl");
	mWaterShader = resMan->acquireShader("shaders/water.glsl");
	mFoliageShader = resMan->acquireShader("shaders/foliage.glsl");
	mDecalShader = resMan->acquireShader("shaders/decal.glsl");
	mGlassShader = resMan->acquireShader("shaders/glass.glsl");
	mGlassTexShader = resMan->acquireShader("shaders/glasstex.glsl");
	mWireShader = resMan->acquireShader("shaders/wire.glsl");
	mShadowShader = resMan->acquireShader("shaders/bodyshadow.glsl");

	if (gGame->mDevice->getCpuCount() == 1 && gGame->mDevice->getPlatformName() == "ios")
		mBodyShaderTex = resMan->acquireShader("shaders/bodytexlow.glsl");
	else
		mBodyShaderTex = resMan->acquireShader("shaders/bodytex.glsl");
	
    bool iPad3 = (gGame->mDevice->getPlatformName()=="ios" && gGame->mDevice->getModelName()=="iPad3");
	if (gGame->mDevice->getCpuCount() == 1 || iPad3 || gGame->mDevice->getPlatformName() == "android")
		mSepiaShader = resMan->acquireShader("shaders/sepialow.glsl");
	else
		mSepiaShader = resMan->acquireShader("shaders/sepia.glsl");

	mGaussBlob.init(128, 128, GL_ALPHA);
	mGaussBlob.loadBlob();
}


Gfx::~Gfx()
{
}


void Gfx::drawFullScreen()
{
	QiMatrix4 m;
	m.m[0] = 1024;
	m.m[5] = 768;
	drawRectangle(m);
}


void Gfx::drawFullScreenSolid(const QiColor& color)
{
	QiRenderState state;
	state.blendMode = state.BLEND;
	state.color = color;
	state.shader = m2DShader.getShader();
	gGame->mRenderer->setState(state);	
	drawFullScreen();	
}


void Gfx::drawRectangle(const QiMatrix4& mat, int res)
{
	gGame->mRenderer->drawTriangles(mat, &mVbo[res], &mIbo[res]);
}


void Gfx::drawRectangle(const QiVec2& p0, const QiVec2& p1, const QiColor& color)
{
	QiRenderState state;
	state.blendMode = state.BLEND;
	state.color = color;
	state.shader = m2DShader.getShader();
	gGame->mRenderer->setState(state);	

	QiMatrix4 mat = QiTransform3(QiVec3(p0.x, p0.y, 0.0f));
	mat.m[0] = p1.x-p0.x;
	mat.m[5] = p1.y-p0.y;
	mat.m[10] = 1.0f;
	drawRectangle(mat);
}


void Gfx::drawLine(const QiVec3& p0, const QiVec3& p1, const QiColor& col, float lineWidth)
{
	QiRenderState state;
	state.blendMode = state.BLEND;
	state.color = col;
	state.shader = m2DShader.getShader();
	gGame->mRenderer->setState(state);	
	gGame->mRenderer->drawLine(p0, p1);
}

