/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "game.h"
#include "script.h"
#include "resman.h"

class Scene
{
public:
	Scene();
	~Scene();

	void load(const QiString& path);
	void unload();
	void reload();

	void update();
	void draw();

	QiString mPath;
	ResMan mResMan;
	Script mScript;
	int mDrawCallCount;
};

