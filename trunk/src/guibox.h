/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "qi_base.h"
#include "qi_gfx.h"
#include "resman.h"

bool isAnythingModal();

class GuiBox
{
public:
	enum ScrollDir
	{
		GUI_SCROLL_NONE,
		GUI_SCROLL_UP,
		GUI_SCROLL_DOWN,
		GUI_SCROLL_LEFT,
		GUI_SCROLL_RIGHT
	};

	class Selection
	{
	public:
		QiString id;
		bool enabled;
		QiVec2 lower;
		QiVec2 upper;
		QiString command;
		int radioGroup;
		bool radioSelect;
	};

	GuiBox();
	~GuiBox();

	void init(QiInputStream& in, int length, class ResMan* resMan);
	void init(QiTexture* texture, QiTexture* textureSelect, bool overlayMode=false);
	void clear();
	void abortSelect();

	//Upper/lower in texture pixels
	void addSelection(const QiString& id, const QiVec2& lower, const QiVec2& upper, const QiString& command);
	void setRadio(const QiString& id, int group, bool selected);
	void selectRadio(const QiString& id);

	void render(const QiMatrix4& mat, const QiColor& color, bool doInput=true);

	bool hasClickedOutside() const { return mOutside; }

	void select(int i);

	void setModal(bool enabled);
	void setEnabled(bool enabled);

	QiString getSelection();

	QiTexture* mTexture;
	QiTexture* mTextureSelect;
	bool mOverlayMode;
	
	QiArray<Selection> mSelections;

	bool mOutside;
	QiString mSelectValue;
	QiString mOutsideCommand;

	Resource mTextureResource;
	Resource mTextureSelectResource;
	Resource mTextureOverlayResource;
	
	static void tick();

	bool mEnabled;
	bool mShade;
};

