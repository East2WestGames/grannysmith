/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "resman.h"
#include "script.h"
#include "game.h"
#include "resman.h"
#include "gfx.h"
#include "audio.h"
#include "file/qipath.h"
#include "gfx/qigl.h"
#include "particlesystem.h"

#include "script_hud.h"
#include "script_world.h"


Script::Script(ResMan* resMan) : mResMan(resMan), mHasScript(false), mSimTime(0.0f), mFrame(0), mInitTime(0.0f), mTickTime(0.0f), mDrawTime(0.0f), mCurrentCanvas(NULL), mCanvasColor(1,1,1,1),
mCanvasEnabled(true), mDragTouch(-1), mDragCanvas(NULL)
{
}

#define REG_FUNC(func) mScript.registerFunction(#func, func)

bool Script::load(const QiString& path)
{
	mCurrentCanvas = NULL;
	unload();
	mScript.init();
	mScript.userData = this;
	QiMemoryStream<256> in;
	if (mResMan->load(path, in))
	{
		if (mScript.load("", in, in.getSize()))
		{
			REG_FUNC(mgCreateImage);
			REG_FUNC(mgCreateImage);
			REG_FUNC(mgCreateUi);
			REG_FUNC(mgCreateText);
			REG_FUNC(mgSetOrigo);
			REG_FUNC(mgSetCrop);
			REG_FUNC(mgRadioSelect);
			REG_FUNC(mgSetUiEnabled);
			REG_FUNC(mgSetUiModal);
			REG_FUNC(mgSetText);

			REG_FUNC(mgSetPos);
			REG_FUNC(mgSetRot);
			REG_FUNC(mgSetScale);
			REG_FUNC(mgSetAlpha);
			REG_FUNC(mgSetColor);

			REG_FUNC(mgGetPos);
			REG_FUNC(mgGetRot);
			REG_FUNC(mgGetScale);
			REG_FUNC(mgGetAlpha);
			REG_FUNC(mgGetColor);

			REG_FUNC(mgIsVisible);
			REG_FUNC(mgDraw);

			REG_FUNC(mgGet);
			REG_FUNC(mgSet);
			REG_FUNC(mgCommand);
			REG_FUNC(mgScriptTime);
			REG_FUNC(mgScriptFrame);
			REG_FUNC(mgFullScreenColor);

			REG_FUNC(mgCreateSound);
			REG_FUNC(mgPlaySound);

			REG_FUNC(mgIsTouched);
			REG_FUNC(mgIsAdRemoved);
			REG_FUNC(mgGetLastTouch);
			REG_FUNC(mgGetScreenCoord);
			REG_FUNC(mgGetLevelInfo);
			REG_FUNC(mgIsCharacterAvailable);
			
			REG_FUNC(mgCreateCanvas);
			REG_FUNC(mgPushCanvas);
			REG_FUNC(mgPopCanvas);
			REG_FUNC(mgSetCanvasEnabled);
			REG_FUNC(mgSetCanvasWindow);
			REG_FUNC(mgSetCanvasMovable);

			REG_FUNC(mgGetUiSelectionCount);
			REG_FUNC(mgGetUiSelectionInfo);
			
			REG_FUNC(mgDrawReplay);

			REG_FUNC(mgGetPose);
			REG_FUNC(mgSetPose);
			REG_FUNC(mgGetVelocity);
			REG_FUNC(mgSetVelocity);
			REG_FUNC(mgSetFilter);
			REG_FUNC(mgSetDynamic);
			REG_FUNC(mgMove);
			REG_FUNC(mgRotate);

			REG_FUNC(mgGetDistanceBetweenBodies);
			REG_FUNC(mgGetDistanceToPoint);

			REG_FUNC(mgInSensor);
			REG_FUNC(mgBreakJoint);
			REG_FUNC(mgSetSensorEnabled);

			REG_FUNC(mgIsGrabbed);
			REG_FUNC(mgIsBroken);

			REG_FUNC(mgCreateEffect);
			REG_FUNC(mgParticle);
			REG_FUNC(mgRnd);
			REG_FUNC(mgParticlePoof);
			
			REG_FUNC(mgCameraShake);

			QiTimer t;
			if (mScript.hasFunction("init"))
				mScript.execute("init()");
			mInitTime = t.getTime();

			mHasScript = true;
			mTimer.reset();
			mSimTime = 0.0f;
			mFrame = 0;
		}
		else
		{
			gGame->logE("Error compiling script: " + path);
			return false;
		}
	}
	else
	{
		gGame->logE("Could not load script: " + path);
		return false;
	}
	return true;
}


void Script::unload()
{
	if (mHasScript)
	{
		for(int i=0; i<mObjects.getCount(); i++)
			QI_DELETE(mObjects[i]);
		mObjects.clear();
		mHasScript = false;
	}
}


void Script::tick(bool callFrameFunction)
{
	QiTimer t;

	for(int i=0; i<QI_MAX_TOUCH; i++)
	{
		if (gGame->mInput->hasTouch(i))
			mLastTouch = gGame->mDisplay->pixelToWorld(gGame->mInput->getTouchPos(i));
	}

	if (!mHasScript)
		return;

	if (callFrameFunction && mScript.hasFunction("frame"))
		mScript.call("frame");

	for(int i=0; i<mObjects.getCount(); i++)
	{
		if (mObjects[i]->isVisual())
		{
			Visual* v = (Visual*)mObjects[i];
			v->mPos.update(gGame->mFrameTime);
			v->mRot.update(gGame->mFrameTime);
			v->mScale.update(gGame->mFrameTime);
			v->mAlpha.update(gGame->mFrameTime);
			v->mColor.update(gGame->mFrameTime);
		}
		if (mObjects[i]->mType == Script::Object::EFFECT)
		{
			Script::Effect* effect = (Script::Effect*)mObjects[i];
			if (effect->mParticleSystem)
				effect->mParticleSystem->update();
		}
		if (mObjects[i]->mType == Script::Object::UI)
		{
			Script::Ui* ui = (Script::Ui*)mObjects[i];
			QiString cmd = ui->mGuiBox.getSelection();
			if (cmd != "")
			{
				ui->mGuiBox.mSelectValue = "";
				int i;
				do
				{
					i = cmd.getIndexOf(";");
					QiString first;
					if (i != -1)
					{
						first = cmd.substring(0, i);
						cmd = cmd.substring(i+1);
					}
					else
						first = cmd;
					cmd.trim();
					first.trim();

					if (first.startsWith("script:"))
						handleCommand(first.substring(7));
					else
						gGame->handleCommand(first);
				} while (i != -1);
				break;
			}
		}
		if (!isAnythingModal() && mObjects[i]->mType == Script::Object::CANVAS)
		{
			Canvas* c = (Canvas*)mObjects[i];
			if (c->mMovable && c->mEnabled)
			{
				QiVec2 maxPos = QiVec2((float)c->mWindow[0], (float)c->mWindow[1]);
				QiVec2 minPos = QiVec2((float)c->mWindow[2]-c->mWidth, (float)c->mWindow[3]-c->mHeight);
				if (mDragCanvas == c)
				{
					if (gGame->mInput->hasTouch(mDragTouch))
					{
						QiVec2 p = gGame->mDisplay->pixelToWorld(gGame->mInput->getTouchPos(mDragTouch));
						QiVec2 diff = (p-mDragPivot);

						//Abort selection if scrolled a bit
						if (lengthSquared(diff) > 30*30)
						{
							for(int j=0; j<mObjects.getCount(); j++)
							{
								if (mObjects[j]->mType == Script::Object::UI)
								{
									Script::Ui* ui = (Script::Ui*)mObjects[j];
									ui->mGuiBox.abortSelect();
								}
							}
						}

						QiMatrix4 m = c->mMatrix;
						m.invert();
						diff = m.transformVec(diff.vec3()).vec2();

						float xClamped = QiClamp(diff.x, minPos.x-mDragOrgPos.x, maxPos.x-mDragOrgPos.x);
						float yClamped = QiClamp(diff.y, minPos.y-mDragOrgPos.y, maxPos.y-mDragOrgPos.y);
						
						float t = 1.0f/(1.0f+0.01f*QiAbs(diff.x - xClamped));
						diff.x = diff.x*t+xClamped*(1.0f-t);
						t = 1.0f/(1.0f+0.01f*QiAbs(diff.y - yClamped));
						diff.y = diff.y*t+yClamped*(1.0f-t);

						if (c->mMovable == 1)
							diff.y = 0;
						else if (c->mMovable == 2)
							diff.x = 0;
						QiVec2 newPos = mDragOrgPos + diff;
						c->mDragVel = c->mDragVel * 0.8f + (newPos - c->mPos.get()) * 0.2f;
						c->mPos.set(newPos, Transition<QiVec2>::IMMEDIATE, 0.0f);
					}
					else
					{
						mDragCanvas = NULL;
						/*
						QiVec2 p = c->mPos.get();
						p.x = QiClamp(p.x, minPos.x, maxPos.x);
						p.y = QiClamp(p.y, minPos.y, maxPos.y);
						c->mPos.set(p, Transition<QiVec2>::COSINE, 0.2f);
						*/
					}
				}
				else
				{
					if (gGame->mInput->wasTouchPressed(0))
					{
						QiVec2 p = gGame->mDisplay->pixelToWorld(gGame->mInput->getTouchPos(0));				

						QiVec2 lower = c->mMatrix.transformPoint(QiVec3((float)c->mWindow[0], (float)c->mWindow[1], 0.0f)).vec2();
						QiVec2 upper = c->mMatrix.transformPoint(QiVec3((float)c->mWindow[2], (float)c->mWindow[3], 0.0f)).vec2();
						if (p.x > lower.x && p.x < upper.x && p.y > lower.y && p.y < upper.y )
						{
							mDragCanvas = c;
							mDragTouch = 0;
							mDragPivot = p;
							mDragOrgPos = c->mPos.get();
						}
					}
					else
					{
						QiVec2 newPos = c->mPos.get() + c->mDragVel;
						c->mDragVel *= 0.9f;

						QiVec2 p = newPos;
						p.x = QiClamp(p.x, minPos.x, maxPos.x);
						p.y = QiClamp(p.y, minPos.y, maxPos.y);
						newPos = p * 0.3f + newPos * 0.7f;

						c->mPos.set(newPos);
					}
				}
			}
		}
	}

	mFrame++;

	mTickTime = t.getTime();
	mSimTime += gGame->mTimeStep;
}


void Script::drawEffects()
{
	for(int i=0; i<mObjects.getCount(); i++)
	{
		if (mObjects[i]->mType == Script::Object::EFFECT)
		{
			Script::Effect* effect = (Script::Effect*)mObjects[i];
			if (effect->mParticleSystem)
				effect->mParticleSystem->render();
		}
	}
}


void Script::draw()
{
	QiTimer t;

	if (!mHasScript)
		return;

	if (mScript.hasFunction("draw"))
		mScript.call("draw");

	mDrawTime = t.getTime();
}


void Script::handleCommand(const QiString& cmd)
{
	if (mScript.hasFunction("handleCommand"))
		mScript.call("handleCommand", cmd);
}


QiMatrix4 Script::Visual::getTransform() const
{
	QiVec2 pos = mPos.get();
	float rot = mRot.get();
	QiVec2 scale = mScale.get();

	QiMatrix4 mat(QiVec3(pos.x, pos.y, 0.0f));
	mat *= QiMatrix4(QiQuat(QiVec3::Z, -rot));
	QiMatrix4 sm;
	sm.m[0] = scale.x;
	sm.m[5] = scale.y;
	mat *= sm;
	mat *= QiMatrix4(QiVec3(-mOrigo.x, -mOrigo.y, 0.0f));
	sm.m[0] = (float)getWidth();
	sm.m[5] = (float)getHeight();
	mat *= sm;
	return mScript->mCanvasMatrix * mat;
}


bool Script::Visual::isVisible() const
{
	bool r = true;

	//No Alpha?
	if (getColor().a <= 0.0f)
		r = false;

	QiMatrix4 mat = getTransform();
	QiVec3 p0 = mat.transformPoint(QiVec3(0,0,0));
	QiVec3 p1 = mat.transformPoint(QiVec3(1,0,0));
	QiVec3 p2 = mat.transformPoint(QiVec3(1,1,0));
	QiVec3 p3 = mat.transformPoint(QiVec3(0,1,0));
	QiVec2 mi(QiMin(QiMin(p0.x, p1.x), QiMin(p2.x, p3.x)), QiMin(QiMin(p0.y, p1.y), QiMin(p2.y, p3.y)));
	QiVec2 ma(QiMax(QiMax(p0.x, p1.x), QiMax(p2.x, p3.x)), QiMax(QiMax(p0.y, p1.y), QiMax(p2.y, p3.y)));
	mi = gGame->mDisplay->worldToPixel(mi);
	ma = gGame->mDisplay->worldToPixel(ma);

	//Smaller than a pixel?
	if (ma.x-mi.x < 1.0f || ma.y-mi.y < 1.0f)
		r = false;

	//Off-screen?
	if (mi.x > gGame->mDisplay->mWidth || mi.y > gGame->mDisplay->mHeight || ma.x < 0 || ma.y < 0)
		r = false;

	return r;
}


QiColor Script::Visual::getColor() const
{
	QiVec3 c = mColor.get();
	float a = mAlpha.get();
	return QiColor(c.x, c.y, c.z, a) * mScript->mCanvasColor;
}



float Script::Image::getWidth() const 
{
	if (mTexture)
		return mTexture->getWidth() * (mTexHigh.x - mTexLow.x);
	else
		return 0;
}


float Script::Image::getHeight() const
{
	if (mTexture)
		return mTexture->getHeight() * (mTexHigh.y - mTexLow.y);
	else
		return 0;
}


void Script::Image::draw()
{
	QiColor color = getColor();
	if (color.a <= 0.0f || lengthSquared(mScale.get()) == 0.0f)
		return;

	QiRenderState state;
	state.blendMode = state.BLEND;
	state.shader = gGame->mGfx->m2DTexShader.getShader();
	state.texture[0] = mTexture;
	state.color = color;
	state.texOffset[0] = mTexLow;
	state.texScale[0] = mTexHigh-mTexLow;
	gGame->mRenderer->setState(state);

	gGame->mGfx->drawRectangle(getTransform());
}


Script::Visual* Script::getVisual(int i)
{
	if (i < 0 || i >= mObjects.getCount())
	{
		gGame->logE(QiString("Script: Object not found: ") + i);
		return NULL;
	}
	Script::Object* obj= mObjects[i];
	if (!obj->isVisual())
	{
		gGame->logE(QiString("Script: Object not visual: ") + i);
		return NULL;
	}
	return (Script::Visual*)obj;
}


Script::Image* Script::getImage(int i)
{
	if (i < 0 || i >= mObjects.getCount())
	{
		gGame->logE(QiString("Script: Object not found: ") + i);
		return NULL;
	}
	Script::Object* obj= mObjects[i];
	if (obj->mType != Object::IMAGE)
	{
		gGame->logE(QiString("Script: Object not image: ") + i);
		return NULL;
	}
	return (Script::Image*)obj;
}


Script::Ui* Script::getUi(int i)
{
	if (i < 0 || i >= mObjects.getCount())
	{
		gGame->logE(QiString("Script: Object not found: ") + i);
		return NULL;
	}
	Script::Object* obj= mObjects[i];
	if (obj->mType != Object::UI)
	{
		gGame->logE(QiString("Script: Object not UI: ") + i);
		return NULL;
	}
	return (Script::Ui*)obj;
}


Script::Text* Script::getText(int i)
{
	if (i < 0 || i >= mObjects.getCount())
	{
		gGame->logE(QiString("Script: Object not found: ") + i);
		return NULL;
	}
	Script::Object* obj= mObjects[i];
	if (obj->mType != Object::TEXT)
	{
		gGame->logE(QiString("Script: Object not Text: ") + i);
		return NULL;
	}
	return (Script::Text*)obj;
}


Script::Sound* Script::getSound(int i)
{
	if (i < 0 || i >= mObjects.getCount())
	{
		gGame->logE(QiString("Script: Object not found: ") + i);
		return NULL;
	}
	Script::Object* obj= mObjects[i];
	if (obj->mType != Object::SOUND)
	{
		gGame->logE(QiString("Script: Object not sound: ") + i);
		return NULL;
	}
	return (Script::Sound*)obj;
}


Script::Canvas* Script::getCanvas(int i)
{
	if (i < 0 || i >= mObjects.getCount())
	{
		gGame->logE(QiString("Script: Object not found: ") + i);
		return NULL;
	}
	Script::Object* obj= mObjects[i];
	if (obj->mType != Object::CANVAS)
	{
		gGame->logE(QiString("Script: Object not Canvas: ") + i);
		return NULL;
	}
	return (Script::Canvas*)obj;
}


Script::Effect* Script::getEffect(int i)
{
	if (i < 0 || i >= mObjects.getCount())
	{
		gGame->logE(QiString("Script: Object not found: ") + i);
		return NULL;
	}
	Script::Object* obj= mObjects[i];
	if (obj->mType != Object::EFFECT)
	{
		gGame->logE(QiString("Script: Object not Effect: ") + i);
		return NULL;
	}
	return (Script::Effect*)obj;
}


float Script::Ui::getWidth() const
{
	if (mGuiBox.mTexture)
		return (float)mGuiBox.mTexture->getWidth();
	else
		return 0;
}


float Script::Ui::getHeight() const
{
	if (mGuiBox.mTexture)
		return (float)mGuiBox.mTexture->getHeight();
	else
		return 0;
}


void Script::Ui::draw()
{
	QiColor color = getColor();
	if (color.a <= 0.0f || lengthSquared(mScale.get()) == 0.0f)
		return;

	QiVec3 rgb = mColor.get();
	mGuiBox.render(getTransform(), color, mScript->mCanvasEnabled);
}


float Script::Text::getWidth() const 
{
	return mFont.getWidth();
}


float Script::Text::getHeight() const
{
	return mFont.getHeight();
}


void Script::Text::draw()
{
	QiColor color = getColor();
	if (color.a <= 0.0f || lengthSquared(mScale.get()) == 0.0f)
		return;

	QiRenderState state;
	state.blendMode = state.BLEND;
	state.shader = gGame->mGfx->m2DTexShader.getShader();
	state.texture[0] = mFont.mTexture.getTexture();
	state.color = color;
	gGame->mRenderer->setState(state);

	QiMatrix4 sm;
	sm.m[0] = 1.0f/(float)getWidth();
	sm.m[5] = 1.0f/(float)getHeight();

	gGame->mRenderer->drawTriangles(getTransform()*sm, &mFont.mVb, &mFont.mIb, mFont.mChars*6);
}


void Script::pushCanvas(Canvas* c)
{
	c->mMatrix = mCanvasMatrix;

	if (c->mUseWindow)
	{
//		glPushAttrib(GL_SCISSOR_BIT);
		glEnable(GL_SCISSOR_TEST);

		QiVec2 worldMin = c->mMatrix.transformPoint(QiVec3((float)c->mWindow[0], (float)c->mWindow[1], 0.0f)).vec2();
		QiVec2 worldMax = c->mMatrix.transformPoint(QiVec3((float)c->mWindow[2], (float)c->mWindow[3], 0.0f)).vec2();

		QiVec2 p0 = gGame->mDisplay->worldToPixel(worldMin);
		QiVec2 p1 = gGame->mDisplay->worldToPixel(worldMax);
		glScissor((int)p0.x, (int)(gGame->mDisplay->mHeight-p1.y), (int)(p1.x-p0.x), (int)(p1.y-p0.y));
	}

	mCanvasStack.add(c);
	updateCanvasStack();
}


void Script::popCanvas()
{
	if (mCanvasStack.getCount() == 0)
		return;

	if (mCurrentCanvas->mUseWindow)
	{
		glDisable(GL_SCISSOR_TEST);
		//glPopAttrib();
	}

	mCanvasStack.removeLast();
	updateCanvasStack();
}


QiMatrix4 Script::Canvas::getTransform()
{
	QiVec2 pos = mPos.get();
	float rot = mRot.get();
	QiVec2 scale = mScale.get();

	QiMatrix4 mat(QiVec3(pos.x, pos.y, 0.0f));
	mat *= QiMatrix4(QiQuat(QiVec3::Z, -rot));
	QiMatrix4 sm;
	sm.m[0] = scale.x;
	sm.m[5] = scale.y;
	mat *= sm;
	mat *= QiMatrix4(QiVec3(-mOrigo.x, -mOrigo.y, 0.0f));
	sm.m[0] = 1.0f;
	sm.m[5] = 1.0f;
	mat *= sm;
	return mat;
}


void Script::updateCanvasStack()
{
	mCanvasMatrix.setIdentity();
	mCanvasColor.set(1,1,1,1);
	mCanvasEnabled = true;
	for(int i=0; i<mCanvasStack.getCount(); i++)
	{
		QiVec3 color = mCanvasStack[i]->mColor.get();
		float alpha = mCanvasStack[i]->mAlpha.get();
		mCanvasMatrix *= mCanvasStack[i]->getTransform();
		mCanvasColor *= QiColor(color.x, color.y, color.z, alpha);
		mCanvasEnabled &= mCanvasStack[i]->mEnabled;
	}

	if (mCanvasStack.isEmpty())
		mCurrentCanvas = NULL;
	else
		mCurrentCanvas = mCanvasStack.last();
}


Script::Effect::Effect(Script* script, const QiString& name) : Object(script, EFFECT)
{
	mParticleSystem = QI_NEW ParticleSystem();
	if (!mParticleSystem->load(name))
	{
		QI_DELETE(mParticleSystem);
		mParticleSystem = NULL;
		return;
	}
}


Script::Effect::~Effect()
{
	if (mParticleSystem)
		QI_DELETE(mParticleSystem);
}

