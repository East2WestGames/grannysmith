/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "config.h"
#include "editor.h"

#if ENABLE_EDITOR

#if _MSC_VER
#pragma warning (disable: 4355)
#endif

#include "game.h"
#include "level.h"
#include "display.h"
#include "app/qiinput.h"
#include "body.h"
#include "wire.h"
#include "dude.h"
#include "resman.h"
#include "gfx.h"
#include "audio.h"
#include "profiling.h"
#include "player.h"

#include "replay.h"

#include "qi_file.h"

#include "clipper/clipper.h"

const int UNDO_STEPS = 100;

static QiColor cRed(1,0,0,1);
static QiColor cGreen(0,1,0,1);
static QiColor cYellow(1,1,0,1);
static QiColor cCyan(0,1.0f,1.0f,1);
static QiColor cGrey(0.6f,0.6f,0.6f,1);

bool gExplicitVertexEditing = false;

static inline void drawPoint(const QiVec2& p, const QiColor& col, float size = 4)
{
	glPointSize(size);
	glBegin(GL_POINTS);
	glColor4f(col.r, col.g, col.b, col.a);
	glVertex2f(p.x, p.y);
	glEnd();
}

static inline void drawLine(const QiVec2& p0, const QiVec2& p1, const QiColor& col0, const QiColor& col1, float width = 1.0f)
{
	glBegin(GL_LINES);
	glColor4f(col0.r, col0.g, col0.b, col0.a);
	glVertex2f(p0.x, p0.y);
	glColor4f(col1.r, col1.g, col1.b, col1.a);
	glVertex2f(p1.x, p1.y);
	glEnd();
}

static inline void fillRect(const QiVec2& p0, const QiVec2& p1, const QiColor& col)
{
	glBegin(GL_QUADS);
	glColor4f(col.r, col.g, col.b, col.a);
	glVertex2f(p0.x, p0.y);
	glVertex2f(p1.x, p0.y);
	glVertex2f(p1.x, p1.y);
	glVertex2f(p0.x, p1.y);
	glEnd();
}

static inline void drawLine(const QiVec2& p0, const QiVec2& p1, const QiColor& col, float width = 1.0f)
{
	drawLine(p0, p1, col, col, width);
}

static inline void drawCircle(const QiVec2& center, float radius, const QiColor& col, float width = 1.0f)
{
	for(int i=0; i<32; i++)
	{
		float v0 = i * QI_PI * 2.0f / 32.0f;
		float v1 = (i+1) * QI_PI * 2.0f / 32.0f;
		drawLine(center+QiVec2(QiCos(v0), QiSin(v0))*radius, center+QiVec2(QiCos(v1), QiSin(v1))*radius, col, width);
	}
}


Editor::Editor() : mSelectedEntity(NULL), mSelectedVertex(-1), mUndo(this, 50), mGridSize(0.0f), mWorkMode(false)
{
	mState = IDLE;
}


void Editor::loadPlayerPath()
{
	mPlayerPath.clear();

	QiMemoryStream<256> tmp;
	QiString path = gGame->mPlayer->getLevelPath() + QiString(".motion");
	if (!gGame->mResMan->load(path, tmp))
		return;

	Replay r;
	r.load(tmp);

	for(int i=0; i<r.mFrames.getCount(); i+=5)
		mPlayerPath.add(r.mFrames[i].pos);
}


void Editor::init()
{
	QiMemoryStream<256> tmp;
	QiString path = gGame->mPlayer->getLevelPath();
	gGame->mResMan->load(path, tmp);
	gGame->mLevel->load(tmp, tmp.getSize());

	loadPlayerPath();

	mPropertyPage.init(&gGame->mDisplay->mViewport);

	mUndo.reset();

	mText.init(gGame->mRenderer, gGame->mGfx->mFontShader.getShader());
	mText.setFont(QiTextRenderer::TAHOMA_10);
	mText.setPosition(10, 10);
	mText.flip(true);
	mText.setColor(0,0,0,1);

	mSelection.clear();
	mSelectedEntity = NULL;
	mSelectedVertex = -1;
	mSelectedControlPoint = -1;
	mSelectedControlLine = -1;

	gGame->mAudio->stopBackgroundMusic();

	mWorkMode = gGame->mLevel->mProperties.getBool("editWorkMode");

	mHeatMap.clear();
	QiFileInputStream is("heatmap.xml");
	QiXmlParser xml;
 	if (is.isOpen() && xml.parse(is, is.getSize()))
	{
		QI_PRINT("Found heatmap file " + gGame->mLevelName);
		xml.enter();
		while(xml.isValid())
		{
			if (xml.getName() == "level" && xml.getAttribute("name") == gGame->mLevelName)
			{
				QI_PRINT("Found heatmap for level");
				xml.enter();
				while(xml.isValid())
				{
					if (xml.getName() == "die")
					{
						HeatMapEntry e;
						e.pos.x = xml.getAttribute("x").toFloat();
						e.pos.y = xml.getAttribute("y").toFloat();
						e.fraction = xml.getAttribute("fraction").toFloat();
						mHeatMap.add(e);
					}
					xml.next();
				}
				xml.leave();
			}
			xml.next();
		}
		xml.leave();
	}

	QiRandomize();
}


void Editor::update()
{
}


void Editor::draw()
{
	PROFILE_FUNC();
	QiString text;

	QiInput& input = *gGame->mInput;
	QiViewport& vp = gGame->mDisplay->mViewport;
	Level* level = gGame->mLevel;

	gGame->mRenderer->resetState();
	gGame->mDisplay->mViewport.apply();
	mMenu.beginFrame(input);
	mPropertyPage.beginFrame(input);

	glPointSize(4.0f);
	glEnable(GL_BLEND);

	//In screen coords
	QiVec2 mousePixel((float)input.getMousePosX(), (float)input.getMousePosY()); 
	QiVec2 mouseDiffPixel((float)input.getMouseDiffX(), (float)input.getMouseDiffY()); 

	//In world coords
	QiVec2 mousePoint = gGame->mDisplay->pixelToWorld(mousePixel);
	QiVec2 mouseDiff = mousePoint-gGame->mDisplay->pixelToWorld(mousePixel-mouseDiffPixel);

	QiVec2 mousePointNoGrid = mousePoint;

	if (!mPropertyPage.isShown())
	{
		if (input.wasKeyPressed('e'))
		{
			gGame->setState(Game::PLAY);
			mSelectedEntity = NULL;
			mSelection.clear();
		}

		if (input.wasKeyPressed('m'))
		{
			gGame->requestState(Game::MENU);
		}

		if (input.wasKeyPressed('s'))
		{
			QiMemoryStream<256> tmp;
			level->save(tmp);
			QiString path = gGame->mPlayer->getLevelPath();
			QI_PRINT("Saving: " + path);
			gGame->mResMan->save(path, tmp.getData(), tmp.getSize());
		}

		if (input.wasKeyPressed('u'))
		{
			if (input.isKeyDown(QI_KEY_SHIFT))
				mUndo.redo();
			else
				mUndo.undo();
		}

		if (input.wasKeyPressed('g'))
		{
			if (mGridSize == 0.0f)
				mGridSize = 0.1f;
			else if (mGridSize == 0.1f)
				mGridSize = 0.2f;
			else if (mGridSize == 0.2f)
				mGridSize = 0.4f;
			else if (mGridSize == 0.4f)
				mGridSize = 0.0f;
		}
		
		if (input.wasKeyPressed('n'))
		{
			gExplicitVertexEditing = !gExplicitVertexEditing;
			QI_PRINT("Explicit vertex mode: " + gExplicitVertexEditing);
		}

		if (input.wasKeyPressed('3'))
			gGame->mDisplay->mEdit3D = (gGame->mDisplay->mEdit3D + 1) % 4;

		if (input.isKeyDown(QI_KEY_SPACE))
		{                  
			if (input.isButtonDown(QI_BUTTON_LEFT))
				gGame->mLevel->mProperties.setVec2("editPan", gGame->mDisplay->mEditPan - mouseDiff);

			if (input.isButtonDown(QI_BUTTON_RIGHT))
				gGame->mLevel->mProperties.setFloat("editZoom", QiClamp(gGame->mDisplay->mEditZoom*(1.0f - input.getMouseDiffY()*0.005f), 0.01f, 10.0f));

			input.clear();
		}
	}

	if (mGridSize > 0.0f)
	{
		mousePoint.x += mGridSize*0.5f*QiSign(mousePoint.x);
		mousePoint.y += mGridSize*0.5f*QiSign(mousePoint.y);
		mousePoint.x -= QiMod(mousePoint.x, mGridSize);
		mousePoint.y -= QiMod(mousePoint.y, mGridSize);
		if (input.wasButtonPressed(QI_BUTTON_LEFT))
			mGridPivot = mousePoint;

		mouseDiff = mousePoint-mGridPivot;
		mGridPivot = mousePoint;
	}

	text += mousePoint;

	float eps = 5.0f;
	float epsWorld = QiAbs(mousePoint.x - gGame->mDisplay->pixelToWorld(mousePixel+QiVec2(eps,0)).x);

	for(int i=0; i<mHeatMap.getCount(); i++)
	{
		const QiVec2& p = mHeatMap[i].pos;
		float f = mHeatMap[i].fraction;
		fillRect(p-QiVec2(1, 1), p+QiVec2(1, 1), QiColor(1, 0, 0, f));
	}

	bool out = false;
	{
	PROFILE_ZONE("draw objects");
	for(int i=0; i<level->mBodies.getCount() && !out; i++)
	{
		vp.pickPush(i);
		Entity* e = level->mBodies[i];
		if (e->mParent || e->mEditorHidden || e->mEditorGroup!=0)
		{
			vp.pickPop();
			continue;
		}
		int c = level->mBodies[i]->mConvexes.getCount();
		
		for(int j=0; j<c; j++)
		{
			BodyConvex* bc = level->mBodies[i]->mConvexes[j];
			for(int k=0; k<bc->count; k++)
			{
				QiVec2 p0 = e->mTransform.toParentPoint(bc->verts[k]);
				QiVec2 p1 = e->mTransform.toParentPoint(bc->verts[(k+1)%bc->count]);
				drawLine(p0, p1, QiColor(0, 0, 0, 0.9f));
			}
		}
		
		bool curve = level->mBodies[i]->mProperties.getBool("curve");

		if (mSelectedEntity == level->mBodies[i] && curve)
		{
			c = level->mBodies[i]->mControlPoints.getCount();
			for(int j=0; j<c; j++)
			{
				ControlPoint& cp = level->mBodies[i]->mControlPoints[j];
				QiVec2 p = level->mBodies[i]->mTransform.toParentPoint(cp.mPoint);
				QiVec2 h0 = level->mBodies[i]->mTransform.toParentPoint(cp.mHandle0);
				QiVec2 h1 = level->mBodies[i]->mTransform.toParentPoint(cp.mHandle1);

				bool pickedPoint = vp.pickPoint(j, QiVec3(p.x, p.y, 0.0f), eps);
				bool pickedHandle0 = vp.pickPoint(j, QiVec3(h0.x, h0.y, 0.0f), eps);
				bool pickedHandle1 = vp.pickPoint(j, QiVec3(h1.x, h1.y, 0.0f), eps);

				if (input.wasButtonPressed(QI_BUTTON_LEFT) && !input.isKeyDown(QI_KEY_SHIFT))
				{
					if (pickedPoint)
					{
						mUndo.checkpoint();
						mSelectedEntity = e;
						mSelectedControlPoint = j;
						mSelectedControlType = 0;
						mState = MOVE_CONTROL_POINT;
						out = true;
						vp.pickPop();
						break;
					}
					if (pickedHandle0)
					{
						mUndo.checkpoint();
						mSelectedEntity = e;
						mSelectedControlPoint = j;
						mSelectedControlType = -1;
						mState = MOVE_CONTROL_POINT;
						out = true;
						vp.pickPop();
						break;
					}
					if (pickedHandle1)
					{
						mUndo.checkpoint();
						mSelectedEntity = e;
						mSelectedControlPoint = j;
						mSelectedControlType = 1;
						mState = MOVE_CONTROL_POINT;
						out = true;
						vp.pickPop();
						break;
					}
				}

				if (pickedPoint)
					drawPoint(p, cRed);
				else
					drawPoint(p, cCyan*(mSelection.contains(e) ? 1.0f : 0.5f));

				if (pickedHandle0)
					drawPoint(h0, cRed);
				else
					drawPoint(h0, cCyan*(mSelection.contains(e) ? 1.0f : 0.5f));

				if (pickedHandle1)
					drawPoint(h1, cRed);
				else
					drawPoint(h1, cCyan*(mSelection.contains(e) ? 1.0f : 0.5f));

				drawLine(p, h0, cCyan);
				drawLine(p, h1, cCyan);
				drawPoint(h0, cCyan, 2);
				drawPoint(h1, cCyan, 2);
			}
			c = level->mBodies[i]->mPolygon.mPoints.getCount();
			int line = -1;
			for(int j=0; j<c; j++)
			{
				QiVec2& p0 = level->mBodies[i]->mPolygon.mPoints[j];
				QiVec2& p1 = level->mBodies[i]->mPolygon.mPoints[(j+1) % c];
				QiVec2 wp0 = level->mBodies[i]->mTransform.toParentPoint(p0);
				QiVec2 wp1 = level->mBodies[i]->mTransform.toParentPoint(p1);
				if (vp.pickLine(j, QiVec3(wp0.x, wp0.y, 0.0f), QiVec3(wp1.x, wp1.y, 0.0f), eps))
					line = level->mBodies[i]->mVertexControlPoint[j];

				if (level->mBodies[i]->mVertexControlPoint[j] == mSelectedControlLine)
					drawLine(wp0, wp1, cRed);
				else
					drawLine(wp0, wp1, cYellow*(mSelection.contains(e) ? 1.0f : 0.5f));
			}
			mSelectedControlLine = line;
			if (mSelectedControlLine != -1 && mSelectedControlPoint == -1 && input.wasButtonPressed(QI_BUTTON_LEFT) && !input.isKeyDown(QI_KEY_SHIFT))
			{
				mUndo.checkpoint();
				ControlPoint cp;
				cp.mPoint = level->mBodies[i]->mTransform.toLocalPoint(mousePoint);
				QiVec2 h0 = level->mBodies[i]->mControlPoints[mSelectedControlLine].mHandle1 - level->mBodies[i]->mControlPoints[mSelectedControlLine].mPoint;
				int n = (mSelectedControlLine + 1) % level->mBodies[i]->mControlPoints.getCount();
				QiVec2 h1 = level->mBodies[i]->mControlPoints[n].mPoint - level->mBodies[i]->mControlPoints[n].mHandle0;
				QiVec2 h = (h0+h1)*0.5f;
				cp.mHandle0 = cp.mPoint-h;
				cp.mHandle1 = cp.mPoint+h;
				level->mBodies[i]->mControlPoints.insertAt(mSelectedControlLine+1, cp);

				mSelectedControlPoint = mSelectedControlLine+1;
				mSelectedControlType = 0;
				mState = MOVE_CONTROL_POINT;
			}
		}
		else
		{
			bool testBody = true;
			if (gExplicitVertexEditing && !mSelection.contains(level->mBodies[i]))
				testBody = false;
			c = level->mBodies[i]->mPolygon.mPoints.getCount();
			for(int j=0; j<c; j++)
			{
				QiVec2& p0 = level->mBodies[i]->mPolygon.mPoints[j];
				QiVec2& p1 = level->mBodies[i]->mPolygon.mPoints[(j+1) % c];
				QiVec2 wp0 = level->mBodies[i]->mTransform.toParentPoint(p0);
				QiVec2 wp1 = level->mBodies[i]->mTransform.toParentPoint(p1);

				bool pickedVertex0 = vp.pickPoint(j, QiVec3(wp0.x, wp0.y, 0.0f), eps);
				bool pickedVertex1 = vp.pickPoint(j+1, QiVec3(wp1.x, wp1.y, 0.0f), eps);
				bool pickedLine = !pickedVertex0 && !pickedVertex1 && vp.pickLine(j, QiVec3(wp0.x, wp0.y, 0.0f), QiVec3(wp1.x, wp1.y, 0.0f), eps);
			
				if (!testBody)
				{
					pickedVertex0 = false;
					pickedVertex1 = false;
					pickedLine = false;
				}

				if (!curve && input.wasButtonPressed(QI_BUTTON_LEFT) && !input.isKeyDown(QI_KEY_SHIFT))
				{
					if (pickedVertex0)
					{
						mUndo.checkpoint();
						mSelectedEntity = e;
						mSelectedVertex = j;
						mState = MOVE_VERTEX;
						out = true;
						vp.pickPop();
						break;
					}
					else if (pickedLine)
					{
						mUndo.checkpoint();
						Body* b = ((Body*)e);
						QiVec2 p = b->mTransform.toLocalPoint(mousePoint);
						b->mPolygon.mPoints.insertAt(j+1, p);
						mSelectedEntity = e;
						mSelectedVertex = j+1;
						mState = MOVE_VERTEX;
						out = true;
						vp.pickPop();
						break;
					}
				}

				if (pickedLine)
					drawLine(wp0, wp1, cRed);
				else
					drawLine(wp0, wp1, cYellow*(mSelection.contains(e) ? 1.0f : 0.5f));

				if (pickedVertex0 || (mSelection.contains(e) && j==mSelectedVertex))
					drawPoint(wp0, cRed);
				else
					drawPoint(wp0, cYellow*(mSelection.contains(e) ? 1.0f : 0.5f));
			}
		}
		vp.pickPop();
	}
	}

	//Draw entities
	for(int i=0; i<level->mEntities.getCount(); i++)
		drawEntity(level->mEntities[i]);

	//Draw player path
	for(int i=0; i<mPlayerPath.getCount()-1; i++)
		drawLine(mPlayerPath[i], mPlayerPath[i+1], QiColor(0.5f, 1.0f, 0.5f, 0.5f));

	if (mState == IDLE)
	{
		if (input.wasButtonReleased(QI_BUTTON_RIGHT))
		{
			mMenuPoint = mousePoint;
			mMenu.init(vp);

			Entity* e = level->findEntity(mousePointNoGrid);
			if (mSelection.getCount() > 1 && mSelection.contains(e))
			{
				mMenu.open(1);
			}
			else if (e)
			{
				select(e);
				mMenu.open(1);
			}
			else
			{
				mSelectedEntity = NULL;
				mSelection.clear();
				mMenu.open(0);
			}
		}

		if (input.wasButtonPressed(QI_BUTTON_LEFT))
		{
			PROFILE_ZONE("QI_BUTTON_LEFT");
			mUndo.checkpoint();
			mSelectedEntity = level->findEntity(mousePointNoGrid);
			if (mSelectedEntity)
			{
				mModifyPivot = mSelectedEntity->mTransform.toLocalPoint(mousePoint);
				mState = MOVE_ENTITY;
				if (!mSelection.contains(mSelectedEntity))
					select(mSelectedEntity);
				else
				{
					if (input.isKeyDown(QI_KEY_SHIFT))
					{
						deselect(mSelectedEntity);
						mState = IDLE;
					}
				}
			}
			else if (input.isKeyDown(QI_KEY_SHIFT))
			{
				mModifyPivot = mousePoint;
				mState = SELECT;
			}
			else
			{
				mSelection.clear();
				mModifyPivot = mousePoint;
				mState = CREATE_BODY;
			}
		}

		for(int i=0; i<Entity::COUNT; i++)
		{
			if (i==Entity::DUDE || i==Entity::THROWABLE)
				continue;
			QiString caption = QiString("Add ") + gEntityTypeNames[i];
			if (mMenu.item(0, caption))
			{
				mUndo.checkpoint();
				Entity* entity = level->createEntity((Entity::Type)i);
				if (i == Entity::BODY)
				{
					Body* b = (Body*)entity;
					b->mPolygon.mPoints.add(QiVec2(-0.4f, -0.4f));
					b->mPolygon.mPoints.add(QiVec2(0.4f, -0.4f));
					b->mPolygon.mPoints.add(QiVec2(0.4f, 0.4f));
					b->mPolygon.mPoints.add(QiVec2(-0.4f, 0.4f));
					b->updateGeometry();
				}
				entity->setTransform(QiTransform2(mMenuPoint));
				entity->init();
				mSelectedEntity = entity;
			}
		}

		if (mMenu.item(0, "Paste") || (input.wasKeyPressed('v') && input.isKeyDown(QI_KEY_CTRL)))
		{
			if (input.wasKeyPressed('v'))
				mMenuPoint = mousePoint;

			//Add this to group identifier
			int groupInc = (rand() << 16) | rand();

			mUndo.checkpoint();
			mSelection.clear();
			QiMemoryStream<256> tmp;
			if (gGame->mResMan->load("user://granny.clipboard.xml", tmp))
			{
				QiXmlParser xml;
				if (!xml.parse(tmp, tmp.getSize()))
					return;
				if (xml.getName() == "clipboard")
				{
					xml.enter();
					while(xml.isValid())
					{
						Entity* e = gGame->mLevel->loadEntity(xml);
						e->init();
						QiTransform2 t = e->mTransform;
						t.pos += mMenuPoint;
						e->setTransform(t);
						if (e->mEditorGroup != 0)
							e->mEditorGroup += groupInc;
						mSelection.add(e);
						xml.next();
					}
					xml.leave();
				}
			}
		}
		
		if (mMenu.item(0, "Move dude here"))
		{
			mUndo.checkpoint();
			gGame->mLevel->mDude->setTransform(QiTransform2(mMenuPoint));
		}

		if (mMenu.item(0, "Level properties"))
		{
			mUndo.checkpoint();
			mSelectedEntity = NULL;
			mPropertyPage.setPropertyBag(&gGame->mLevel->mProperties);
			mPropertyPage.show();
		}

		if (mSelection.getCount() > 0 && (mMenu.item(1, "Remove") || input.wasKeyPressed(QI_KEY_DELETE)))
		{
			mUndo.checkpoint();
			for(int i=0; i<mSelection.getCount(); i++)
				level->destroy(mSelection[i]);
			mSelectedEntity = NULL;
			mState = IDLE;
			mSelection.clear();
		}

		if (mSelection.getCount() == 1 && mSelectedEntity && mSelectedEntity->getType() == Entity::BODY && !mSelectedEntity->mProperties.getBool("curve") && mMenu.item(1, "Make curve"))
		{
			mUndo.checkpoint();
			((Body*)mSelectedEntity)->makeCurve();
			((Body*)mSelectedEntity)->updateGeometry();
		}

		if (mSelection.getCount() == 1 && mMenu.item(1, "Properties"))
		{
			mPropertyMode = ENTITY;
			mPropertyPage.setPropertyBag(&mSelectedEntity->mProperties);
			mPropertyPage.show();
		}

		if (!mWorkMode && (mMenu.item(0, "Show all") || (input.wasKeyPressed('h') && input.isKeyDown(QI_KEY_SHIFT))))
		{
			mUndo.checkpoint();
			for(int i=0; i<gGame->mLevel->mEntities.getCount(); i++)
				gGame->mLevel->mEntities[i]->mEditorHidden = false;
		}

		if (!mWorkMode && mSelection.getCount() > 0 && (mMenu.item(1, "Hide") || (input.wasKeyPressed('h') && !input.isKeyDown(QI_KEY_SHIFT))))
		{
			mUndo.checkpoint();
			for(int i=0; i<mSelection.getCount(); i++)
				mSelection[i]->mEditorHidden = true;
			mSelection.clear();
			mSelectedEntity = NULL;
		}

		if (!mWorkMode && mSelection.getCount() > 0 && mMenu.item(1, "Hide all but this"))
		{
			mUndo.checkpoint();
			for(int i=0; i<level->mEntities.getCount(); i++)
				level->mEntities[i]->mEditorHidden = true;
			for(int i=0; i<mSelection.getCount(); i++)
				mSelection[i]->mEditorHidden = false;
		}

		if (!mWorkMode && mSelection.getCount() > 0 && (mMenu.item(1, "Group") || (input.wasKeyPressed('l') && !input.isKeyDown(QI_KEY_SHIFT))))
		{
			mUndo.checkpoint();
			int g = (rand() << 16) | rand();
			for(int i=0; i<mSelection.getCount(); i++)
				mSelection[i]->mEditorGroup = g;
		}

		if (!mWorkMode && mSelectedEntity && mSelectedEntity->mEditorGroup && (mMenu.item(1, "Ungroup") || (input.wasKeyPressed('l') && input.isKeyDown(QI_KEY_SHIFT))))
		{
			mUndo.checkpoint();
			for(int i=0; i<mSelection.getCount(); i++)
				mSelection[i]->mEditorGroup = 0;
		}

		if (!mWorkMode && mSelection.getCount() > 0)
		{
			int g = mSelection[0]->mEditorGroup;
			for(int i=1; i<mSelection.getCount(); i++)
				if (mSelection[i]->mEditorGroup != g)
					g = 0;
			if (g != 0)
			{
				if (mMenu.item(1, "Open workgroup") || input.wasKeyPressed('w'))
				{
					mUndo.checkpoint();
					for(int i=0; i<level->mEntities.getCount(); i++)
						level->mEntities[i]->mEditorHidden = true;
					for(int i=0; i<mSelection.getCount(); i++)
					{
						mSelection[i]->mEditorGroup = 0;
						mSelection[i]->mEditorHidden = false;
					}
					mWorkMode = true;
					mSelection.clear();
					mSelectedEntity = NULL;
				}
			}
		} 
		else if (mWorkMode && (mMenu.item(0, "Close workgroup") || input.wasKeyPressed('w')))
		{
			mUndo.checkpoint();
			int g = (rand() << 16) | rand();
			for(int i=0; i<level->mEntities.getCount(); i++)
			{
				if (!level->mEntities[i]->mEditorHidden)
					level->mEntities[i]->mEditorGroup = g;
				level->mEntities[i]->mEditorHidden = false;
			}
			mWorkMode = false;
			mSelection.clear();
			mSelectedEntity = NULL;
		}

		if ((mMenu.item(1, "Copy") || (input.wasKeyPressed('c') && input.isKeyDown(QI_KEY_CTRL))) && mSelection.getCount() > 0)
		{
			if (input.wasKeyPressed('c'))
			{
				mMenuPoint = mSelection[0]->mTransform.pos;
				mMenuPoint.x += mGridSize*0.5f*QiSign(mMenuPoint.x);
				mMenuPoint.y += mGridSize*0.5f*QiSign(mMenuPoint.y);
				if (mGridSize > 0.0f)
				{
				mMenuPoint.x -= QiMod(mMenuPoint.x, mGridSize);
				mMenuPoint.y -= QiMod(mMenuPoint.y, mGridSize);
			}
			}
			QiMemoryStream<256> tmp;
			QiXmlWriter xml;
			xml.enter("clipboard");
			for(int i=0; i<mSelection.getCount(); i++)
			{
				QiTransform2 old = mSelection[i]->mTransform;
				QiTransform2 t = old;
				t.pos -= mMenuPoint;
				mSelection[i]->setTransform(t);
				gGame->mLevel->saveEntity(mSelection[i], xml);
				mSelection[i]->setTransform(old);
			}
			xml.leave();
			xml.write(tmp);

			gGame->mResMan->save("user://granny.clipboard.xml", tmp.getData(), tmp.getSize());
		}
		if (mMenu.item(1, "Copy properties") && mSelectedEntity)
		{
			mClipboardProperties = mSelectedEntity->mProperties;
		}
		if (mMenu.item(1, "Move Z") && mSelection.getCount() > 0)
		{
			mPropertyMode = MOVE_Z;
			mProperties.clear();
			mProperties.add("z", "0");
			mPropertyPage.setPropertyBag(&mProperties);
			mPropertyPage.show();
		}
		if (mMenu.item(1, "Scale") && mSelection.getCount() > 0)
		{
			mPropertyMode = SCALE;
			mProperties.clear();
			mProperties.add("scale", "1.0");
			mPropertyPage.setPropertyBag(&mProperties);
			mPropertyPage.show();
		}
		if (mMenu.item(1, "Paste properties") && mSelection.getCount() > 0)
		{
			mUndo.checkpoint();
			for(int i=0; i<mSelection.getCount(); i++)
			{
				QiTransform2 t = mSelection[i]->mTransform;
				mSelection[i]->mProperties = mClipboardProperties;
				mSelection[i]->loadProperties();
				mSelection[i]->init();		
				mSelection[i]->setTransform(t);
			}
		}
	}

	if (mState == MOVE_ENTITY)
	{
		if (input.isKeyDown(QI_KEY_CTRL) && mSelectedEntity->getType() == Entity::SENSOR)
		{
			if (mSelectedEntity->mProperties.getString("type") == "box")
			{
				QiVec2 s = mSelectedEntity->mProperties.getVec2("size");
				s.x = QiMax(s.x+mouseDiff.x, 0.0f);
				s.y = QiMax(s.y+mouseDiff.y, 0.0f);
				mSelectedEntity->mProperties.setVec2("size", s);
			}
			else
			{
				float r = mSelectedEntity->mProperties.getFloat("size");
				r = QiMax(r+mouseDiff.y, 0.0f);
				mSelectedEntity->mProperties.setFloat("size", r);
			}		
		}
		else
		{
			if (input.isKeyDown(QI_KEY_SHIFT))
			{
				{
					float amount = mouseDiff.y * QI_PI * 0.125f;
					QiTransform2 t = mSelectedEntity->mTransform;
					for(int i=0; i<mSelection.getCount(); i++)
					{
						QiTransform2 d = mSelection[i]->mTransform;
						d.pos = t.pos + QiTransform2(amount).toParentVec(d.pos-t.pos);
						d.rot += amount;
						mSelection[i]->setTransform(d);
						if (input.isKeyDown(QI_KEY_CTRL) && mSelection[i] == mSelectedEntity)
							mSelectedEntity->localTransform(QiTransform2(-amount));
					}
				}
			}
			else
			{
				for(int i=0; i<mSelection.getCount(); i++)
				{
					QiTransform2 t = mSelection[i]->mTransform;
					QiVec2 diff = mouseDiff;
					if (input.isKeyDown(QI_KEY_CTRL) && mSelection[i] == mSelectedEntity)
					{
						diff = mousePoint - t.pos;
						mSelection[i]->localTransform(QiTransform2(mSelection[i]->mTransform.toLocalVec(-diff)));
					}
					t.pos += diff;
					mSelection[i]->setTransform(t);
				}
			}
		}
		if (input.wasButtonReleased(QI_BUTTON_LEFT))
			mState = IDLE;
	}

	if (mState == MOVE_VERTEX)
	{
		Body* selectedBody = (Body*)mSelectedEntity;
		selectedBody->mPolygon.mPoints[mSelectedVertex] = selectedBody->mTransform.toLocalPoint(mousePoint);

		if (input.wasButtonReleased(QI_BUTTON_LEFT))
		{
			int c = selectedBody->mPolygon.mPoints.getCount();
			for(int j=0; j<c; j++)
			{
				if (c>3 && j!=mSelectedVertex && length(selectedBody->mTransform.toParentPoint(selectedBody->mPolygon.mPoints[j]) - mousePoint) < epsWorld)
				{
					selectedBody->mPolygon.mPoints.removeElementAt(j, true);
					break;
				}
			}

			mSelectedVertex = -1;
			mState = IDLE;
			((Body*)mSelectedEntity)->updateGeometry();
		}
	}

	if (mState == MOVE_CONTROL_POINT)
	{
		Body* selectedBody = (Body*)mSelectedEntity;
		ControlPoint& cp = selectedBody->mControlPoints[mSelectedControlPoint];
		if (mSelectedControlType == -1)
		{
			cp.mHandle0 = selectedBody->mTransform.toLocalPoint(mousePoint);
			if (!input.isKeyDown(QI_KEY_CTRL))
			{
				QiVec2 dir = normalize(cp.mPoint - cp.mHandle0);
				cp.mHandle1 = cp.mPoint + dir * length(cp.mPoint - cp.mHandle1);
			}
		}
		else if (mSelectedControlType == 1)
		{
			cp.mHandle1 = selectedBody->mTransform.toLocalPoint(mousePoint);
			if (!input.isKeyDown(QI_KEY_CTRL))
			{
				QiVec2 dir = normalize(cp.mPoint - cp.mHandle1);
				cp.mHandle0 = cp.mPoint + dir * length(cp.mPoint - cp.mHandle0);
			}
		}
		else
		{
			QiVec2 diff = selectedBody->mTransform.toLocalVec(mouseDiff);
			selectedBody->mControlPoints[mSelectedControlPoint].mPoint += diff;
			selectedBody->mControlPoints[mSelectedControlPoint].mHandle0 += diff;
			selectedBody->mControlPoints[mSelectedControlPoint].mHandle1 += diff;
		}
		if (input.wasButtonReleased(QI_BUTTON_LEFT))
		{
			int c = selectedBody->mControlPoints.getCount();
			for(int j=0; j<c; j++)
			{
				if (c>3 && j!=mSelectedControlPoint && length(selectedBody->mTransform.toParentPoint(selectedBody->mControlPoints[j].mPoint) - mousePoint) < epsWorld)
				{
					selectedBody->mControlPoints.removeElementAt(j, true);
					break;
				}
			}
			mSelectedControlPoint = -1;
			mState = IDLE;
			((Body*)mSelectedEntity)->updateGeometry();
		}
		selectedBody->generatePolygon();
	}

	if (mState == CREATE_BODY)
	{
		QiVec2 p0 = mModifyPivot;
		QiVec2 p1 = mousePoint;

		if (p0.x > p1.x)
			QiSwap(p0.x, p1.x);
		if (p0.y > p1.y)
			QiSwap(p0.y, p1.y);

		drawLine(QiVec2(p0.x, p0.y), QiVec2(p1.x, p0.y), QiColor(1,0,0,1));
		drawLine(QiVec2(p0.x, p1.y), QiVec2(p1.x, p1.y), QiColor(1,0,0,1));
		drawLine(QiVec2(p0.x, p0.y), QiVec2(p0.x, p1.y), QiColor(1,0,0,1));
		drawLine(QiVec2(p1.x, p0.y), QiVec2(p1.x, p1.y), QiColor(1,0,0,1));

		if (input.wasButtonReleased(QI_BUTTON_LEFT))
		{
			float w = (p1.x-p0.x)/2;
			float h = (p1.y-p0.y)/2;
			float x = (p1.x+p0.x)/2;
			float y = (p1.y+p0.y)/2;
			if (w > 0 && h > 0)
			{
				mUndo.checkpoint();
				Body* b = (Body*)level->createEntity(Entity::BODY);
				b->mPolygon.mPoints.add(QiVec2(-w, -h));
				b->mPolygon.mPoints.add(QiVec2(w, -h));
				b->mPolygon.mPoints.add(QiVec2(w, h));
				b->mPolygon.mPoints.add(QiVec2(-w, h));
				b->updateGeometry();
				b->setTransform(QiTransform2(QiVec2(x,y)));
				mSelectedEntity = b;
				mSelection.add(b);
			}
			mState = IDLE;
		}
		if (input.wasKeyPressed(QI_KEY_ESCAPE))
			mState = IDLE;
	}

	if (mState == SELECT)
	{
		QiVec2 p0 = mModifyPivot;
		QiVec2 p1 = mousePoint;

		if (p0.x > p1.x)
			QiSwap(p0.x, p1.x);
		if (p0.y > p1.y)
			QiSwap(p0.y, p1.y);

		drawLine(QiVec2(p0.x, p0.y), QiVec2(p1.x, p0.y), QiColor(1,1,0,1));
		drawLine(QiVec2(p0.x, p1.y), QiVec2(p1.x, p1.y), QiColor(1,1,0,1));
		drawLine(QiVec2(p0.x, p0.y), QiVec2(p0.x, p1.y), QiColor(1,1,0,1));
		drawLine(QiVec2(p1.x, p0.y), QiVec2(p1.x, p1.y), QiColor(1,1,0,1));

		if (input.wasButtonReleased(QI_BUTTON_LEFT))
		{
			for(int i=0; i<level->mEntities.getCount(); i++)
			{
				Entity* e = level->mEntities[i];
				if (!e->mParent && !e->mEditorHidden)
				{
					if (e->mTransform.pos.x > p0.x && e->mTransform.pos.y > p0.y && e->mTransform.pos.x < p1.x && e->mTransform.pos.y < p1.y)
					{
						if (!mSelection.contains(e))
						{
							mSelection.add(e);
							if (e->mEditorGroup)
							{
								for(int i=0; i<gGame->mLevel->mEntities.getCount(); i++)
								{
									if (gGame->mLevel->mEntities[i] != e && gGame->mLevel->mEntities[i]->mEditorGroup == e->mEditorGroup)
									{
										if (!mSelection.contains(gGame->mLevel->mEntities[i]))
											mSelection.add(gGame->mLevel->mEntities[i]);
									}
								}
							}
						}
					}
				}
			}
			mState = IDLE;
		}
		if (input.wasKeyPressed(QI_KEY_ESCAPE))
			mState = IDLE;
	}

	mPropertyPage.endFrame();

	if (mPropertyPage.hasFinished())
	{
		if (mPropertyMode == MOVE_Z)
		{
			mUndo.checkpoint();
			float zd = mProperties.getFloat("z");
			for(int i=0; i<mSelection.getCount(); i++)
			{
				float z = mSelection[i]->mProperties.getFloat("z");
				z += zd;
				mSelection[i]->mProperties.setFloat("z", z);
				mSelection[i]->loadProperties();
				mSelection[i]->init();		
			}
		}
		else if (mPropertyMode == SCALE)
		{
			mUndo.checkpoint();
			float scale = mProperties.getFloat("scale");
			for(int i=0; i<mSelection.getCount(); i++)
			{
				Entity* e = mSelection[i];
				QiTransform2 t = e->mTransform;
				t.pos = mMenuPoint + (t.pos-mMenuPoint) * scale;;
				e->setTransform(t);
				if (e->getType() == Entity::BODY && !e->mProperties.getBool("curve"))
				{
					//Scale verts
					Body* b = (Body*)e;
					for(int j=0; j<b->mPolygon.mPoints.getCount(); j++)
						b->mPolygon.mPoints[j] *= scale;
				}
				else if (e->getType() == Entity::DECAL)
				{
					e->mProperties.setFloat("xscale", e->mProperties.getFloat("xscale") * scale);
					e->mProperties.setFloat("yscale", e->mProperties.getFloat("yscale") * scale);
				}
				e->loadProperties();
				e->init();		
			}
		}
		else if (mPropertyMode == ENTITY && mSelectedEntity)
		{
			mUndo.checkpoint();
			mSelectedEntity->loadProperties();
			mSelectedEntity->init();		
		}
	}
	
	mMenu.endFrame();

	drawGrid();

	gGame->mRenderer->resetState();

	if (mSelectedEntity)
	{
		text += QiString("\nType: ") + gGame->mLevel->getEntityTypeName(mSelectedEntity->getType());
		text += QiString("\nName: ") + mSelectedEntity->mProperties.getString("name");
		text += QiString("\nPos: ") + mSelectedEntity->mTransform.pos;
		text += QiString("\nRot: ") + mSelectedEntity->mTransform.rot;
	}

	if (text != "")
	{
		gGame->mDisplay->leaveLevel();
		int x0,y0,x1,y1;
		mText.getBounds(text, x0, y0, x1, y1);
		if (x1 < 200)
			x1 = 200;
		gGame->mGfx->drawRectangle(QiVec2((float)x0-5, (float)y0-5), QiVec2((float)x1+5, (float)y1+5), QiColor(1,1,1,0.7f));
		mText.print(text);
		gGame->mDisplay->enterLevel();
	}

	level->mProperties.setBool("editWorkMode", mWorkMode);

	gGame->mInput->clear();
}


void Editor::drawEntity(Entity* e)
{
	if (e->mEditorHidden)
		return;

	if (e->getType() == Entity::SENSOR)
	{
		QiColor c(1,0,1,0.5f);
		if (e->mProperties.getString("action") == "spawn")
			c = QiColor(1,1,0,0.5f);
		if (e->mProperties.getString("action") == "win")
			c = QiColor(0,1,0,0.5f);
		if (e->mProperties.getString("action") == "die")
			c = QiColor(1,0,0,0.5f);
		if (e->mProperties.getString("action") == "live")
			c = QiColor(0,1,1,0.5f);
		if (e->mProperties.getString("action").startsWith("gravity"))
			c = QiColor(1, 0.7f, 0.4f, 0.5f);
		if (e->mProperties.getString("action").startsWith("speed"))
			c = QiColor(1,1,0,0.5f);
		if (e->mProperties.getString("action").startsWith("force"))
			c = QiColor(0.4f,0.4f,1,0.5f);
		if (e->mProperties.getString("action").startsWith("camera"))
			c = QiColor(0.8f, 0.8f, 0.8f,0.5f);
		if (e->mProperties.getString("type") == "box")
		{
			float w = e->mProperties.getVec2("size").x;
			float h = e->mProperties.getVec2("size").y;
			QiVec2 p0 = e->mTransform.toParentPoint(QiVec2(-w*0.5f, -h*0.5f));
			QiVec2 p1 = e->mTransform.toParentPoint(QiVec2(w*0.5f, -h*0.5f));
			QiVec2 p2 = e->mTransform.toParentPoint(QiVec2(w*0.5f, h*0.5f));
			QiVec2 p3 = e->mTransform.toParentPoint(QiVec2(-w*0.5f, h*0.5f));
			drawLine(QiVec2(p0.x, p0.y), QiVec2(p1.x, p1.y), c);
			drawLine(QiVec2(p1.x, p1.y), QiVec2(p2.x, p2.y), c);
			drawLine(QiVec2(p2.x, p2.y), QiVec2(p3.x, p3.y), c);
			drawLine(QiVec2(p3.x, p3.y), QiVec2(p0.x, p0.y), c);
		}
		else
			drawCircle(e->mTransform.pos, e->mProperties.getFloat("size"), c);
	}

	QiVec2 o = e->mTransform.pos;
	QiVec2 ox = e->mTransform.toParentPoint(QiVec2(1/gGame->mDisplay->mEditZoom,0));
	QiVec2 oy = e->mTransform.toParentPoint(QiVec2(0,1/gGame->mDisplay->mEditZoom));
	drawLine(o, ox, cGreen*(mSelection.contains(e) ? 1.0f : 0.3f), cGreen*0.0f);
	drawLine(o, oy, cGreen*(mSelection.contains(e) ? 1.0f : 0.3f), cGreen*0.0f);
	drawCircle(e->mTransform.pos, 0.1f/gGame->mDisplay->mEditZoom, cGreen*(mSelection.contains(e) ? 1.0f : 0.3f));

	/*
	if (e==mSelectedEntity && mSelectedEntity->getType() == Entity::BODY)
	{
		Body* b = ((Body*)mSelectedEntity);
		for(int j=0; j<b->mConvexes.getCount(); j++)
		{
			BodyConvex* bc = b->mConvexes[j];
			for(int k=0; k<bc->count; k++)
			{
				QiVec2 p0 = b->mTransform.toParentPoint(bc->verts[k]);
				QiVec2 p1 = b->mTransform.toParentPoint(bc->verts[(k+1)%bc->count]);
				drawLine(p0, p1, QiColor(0, 0, 0, 0.9f));
			}		
		}
	}
	*/
}

void Editor::loadUndoState(QiInputStream& stream, int size)
{
	PROFILE_FUNC();
	gGame->mLevel->load(stream, size);
	mSelectedEntity = NULL;
	mSelection.clear();
	mWorkMode = gGame->mLevel->mProperties.getBool("editWorkMode");
}

void Editor::saveUndoState(QiOutputStream& stream)
{
	PROFILE_FUNC();
	gGame->mLevel->save(stream);
}


void Editor::select(Entity* e)
{
	mSelectedEntity = e;
	if (!gGame->mInput->isKeyDown(QI_KEY_SHIFT))
		mSelection.clear();
	if (!mSelection.contains(e))
		mSelection.add(e);
	if (e->mEditorGroup)
	{
		for(int i=0; i<gGame->mLevel->mEntities.getCount(); i++)
		{
			if (gGame->mLevel->mEntities[i] != e && gGame->mLevel->mEntities[i]->mEditorGroup == e->mEditorGroup)
			{
				if (!mSelection.contains(gGame->mLevel->mEntities[i]))
					mSelection.add(gGame->mLevel->mEntities[i]);
			}
		}
	}
}


void Editor::deselect(Entity* e)
{
	mSelectedEntity = NULL;
	mSelection.removeAll(e);
	if (e && e->mEditorGroup)
	{
		for(int i=0; i<gGame->mLevel->mEntities.getCount(); i++)
		{
			if (gGame->mLevel->mEntities[i] != e && gGame->mLevel->mEntities[i]->mEditorGroup == e->mEditorGroup)
				mSelection.removeAll(gGame->mLevel->mEntities[i]);
		}
	}
}


void Editor::drawGrid()
{
	if (mGridSize == 0.0f)
		return;
	QiVec2 center = gGame->mDisplay->mEditPan;
	center.x -= QiMod(center.x, mGridSize);
	center.y -= QiMod(center.y, mGridSize);
	float size = 5.0f/gGame->mDisplay->mEditZoom;
	size -= QiMod(size, mGridSize);
	if (size / mGridSize < 120)
	{
		for(float y=center.y-size; y < center.y+size; y+= mGridSize)
			drawLine(QiVec2(center.x-size, y),  QiVec2(center.x+size, y), QiColor(1,1,1,0.15f));
		for(float x=center.x-size; x < center.x+size; x+= mGridSize)
			drawLine(QiVec2(x, center.y-size),  QiVec2(x, center.y+size), QiColor(1,1,1,0.15f));
	}
	drawLine(QiVec2(center.x-size, 0),  QiVec2(center.x+size, 0), QiColor(1,1,1,0.2f), 2);
	drawLine(QiVec2(0, center.y-size),  QiVec2(0, center.y+size), QiColor(1,1,1,0.2f), 2);
}

#else

Editor::Editor() {}
void Editor::init() {}
void Editor::update() {}
void Editor::draw() {}
void Editor::drawEntity(class Entity* e) {}
void Editor::loadUndoState(QiInputStream& stream, int size) {}
void Editor::saveUndoState(QiOutputStream& stream) {}

#endif


