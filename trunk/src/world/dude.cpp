/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "dude.h"
#include "body.h"
#include "level.h"
#include "game.h"
#include "display.h"
#include "app/qiinput.h"
#include "gfx.h"
#include "cloth.h"
#include "handle.h"
#include "profiling.h"
#include "audio.h"
#include "wire.h"
#include "scene.h"
#include "script.h"
#include "player.h"
#include "throwable.h"

#include "qi_snd.h"
#include "Box2D.h"

extern bool gCapture;
static const QiVec2 GRAB_POINT(0.9f, 1.7f);

class ContactCallback : public b2ContactListener
{
public:
	ContactCallback(Dude* dude): mDude(dude) {}

	virtual void PreSolve(b2Contact* contact, const b2Manifold* oldManifold)
	{
	}

	virtual void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse)
	{
		int pointCount = contact->GetManifold()->pointCount;
		b2WorldManifold m;
		contact->GetWorldManifold(&m);

		b2Body* b = mDude->mTorso->mPhysBody;
		if (contact->GetFixtureA()->GetBody() == b || contact->GetFixtureB()->GetBody() == b )
		{
			b2Body* other = contact->GetFixtureA()->GetBody();
			if (other == b)
				other = contact->GetFixtureB()->GetBody();
			if (other->GetUserData() && ((Body*)other->GetUserData())->mDynamic == 3)
			{
				b2Fixture* f = other->GetFixtureList();
				while(f)
				{
					b2Filter filter = f->GetFilterData();
					filter.maskBits &= 0xFFFC;
					f->SetFilterData(filter);
					f = f->GetNext();
				}
			}
			else
			{		
				float imp = 0.0f;
				for(int i=0; i<pointCount && i < 2; i++)
					imp += impulse->normalImpulses[i];
				bool keepAlive = ((Body*)other->GetUserData())->mSlip;
				if (!keepAlive && dot(vec(m.normal), mDude->mTransform.toParentVec(QiVec2(0,1))) < 0.3f  && imp > 8.0f)
					mDude->die();
				else if (dot(vec(m.normal), mDude->mTransform.toParentVec(QiVec2(0,1))) < 0.3f  && imp > 2.0f && !mDude->mDead)
					mDude->playSound(mDude->mSndGrunt.next());
			}
		}
		b2Vec2 va = contact->GetFixtureA()->GetBody()->GetLinearVelocityFromWorldPoint(m.points[0]);
		b2Vec2 vb = contact->GetFixtureB()->GetBody()->GetLinearVelocityFromWorldPoint(m.points[0]);

		float v = b2Dot(m.normal, vb-va);
		if (v > 0.5f && contact->GetFixtureA()->GetBody() && contact->GetFixtureB()->GetBody())
		{
			QiVec2 p = vec(m.points[0]);
			QiAudioBuffer* snd = gGame->mAudio->mObjectHit.next();
			if (snd)
				gGame->mAudio->playSound(snd, p, QiClamp(v*0.3f, 0.0f, 1.0f));
		}
	}

	Dude* mDude;
};


int getTouch(const QiVec2& point, float radius)
{
	for(int i=0; i<QI_MAX_TOUCH; i++)
	{
		if (gGame->mInput->hasTouch(i))
		{
			QiVec2 t = gGame->mInput->getTouchPos(i);
			if (lengthSquared(t-point) < radius*radius)
				return i;
		}
	}
	return -1;
}

inline bool isTouched(const QiVec2& point, float radius)
{
	return getTouch(point, radius) != -1;
}

Dude::Dude(Level* level, int playerId) : Entity(level, DUDE), mJoint(NULL), 
mGrabJoint(NULL), mGrabbedHandle(NULL), mGrabTimer(0.0f), mGrab(false), 
mPlayerId(playerId), mCrouch(false), mJump(false), mJumpFrame(0), mAbortJumpFrame(0), mJumpAngle(0.0f), mInput(NULL), mOutput(NULL), 
mGrabbedWire(NULL), mRailBody(NULL), mKeepAlive(false), mSensorSpeed(0.0f), mSensorSpeedAir(0.0f), mSpeedMultiplier(1.0f), mFrame(0), mReplayHints(0), mSlowDownTimer(0.0f),
mDeadTimer(0.0f), mReviveTimer(1.0f), mJumpOrgVel(0.0f), mFacialExpressionTimer(0.0f), mSpawnHelmet(false), mHelmetTimer(0.0f),
mVoiceChannel(NULL), mNoCorrection(false), mStartDelay(0.0f)
{
	if (playerId == 1)
		mProperties.setString("name", "dude");
	else
		mProperties.setString("name", "badguy");

	mTorso = (Body*)mLevel->createEntity(Entity::BODY);
	mTorso->mParent = this;
	mTorso->mProperties.setString("name", mProperties.getString("name") + ".torso");
	mTorso->mProperties.setBool("dynamic", true);
	mTorso->mProperties.setVec3("color", QiVec3(0.9f, 0.6f, 0.9f));
	mTorso->mProperties.setBool("hidden", true);
	mTorso->mProperties.setFloat("density", 2.0f);
//	mTorso->mProperties.setInt("dynamicShadow", 2);
	mTorso->mProperties.setFloat("z", -0.05f);
	mTorso->mProperties.setFloat("depth", 0.01f);
	mTorso->loadProperties();
	mTorso->init();
	mTorso->mPhysBody->SetBullet(true);

	//This polygon will be used for shadows only
	mTorso->mPolygon.mPoints.add(QiVec2(-0.3f, 0.0f));
	mTorso->mPolygon.mPoints.add(QiVec2(-0.2f, -0.3f));
	mTorso->mPolygon.mPoints.add(QiVec2(0.0f, -0.4f));
	mTorso->mPolygon.mPoints.add(QiVec2(0.2f, -0.3f));
	mTorso->mPolygon.mPoints.add(QiVec2(0.3f, 0.0f));
	mTorso->mPolygon.mPoints.add(QiVec2(0.2f, 0.3f));
	mTorso->mPolygon.mPoints.add(QiVec2(0.0f, 0.4f));
	mTorso->mPolygon.mPoints.add(QiVec2(-0.2f, 0.3f));

	b2CircleShape s;
	s.m_p.Set(0, 0.0f);
	s.m_radius = 0.3f;

	b2FixtureDef fd;
	fd.filter.categoryBits = mPlayerId;
	fd.filter.maskBits = 252;
	fd.shape = &s;
	fd.density = 1.0f;
	fd.friction = 0.0f;
	fd.restitution = 0.0f;
 	mTorso->mPhysBody->CreateFixture(&fd);

	mContactCallback = NULL;

	s.m_p.Set(0, 0.2f);
 	mTorso->mPhysBody->CreateFixture(&fd);
	//mTorso->mPhysBody->SetFixedRotation(true);
	
	mTargetHeight = 1.0f;
	mUp.set(0,1);
	mBreakPower = 0.0f;
 
    mBoost = false;
    mGestureTouch = -1;
	
	mAngle = 0.0f;
	mRotVel = 0.0f;
	mRotAngle = 0.0f;

	mContactBody = NULL;
	mDead = false;
	mSpeed = 0.0f;

	mGroundHit = false;
	mGroundBody = NULL;
	mGroundTime = 0.0f;

	mName = gGame->mPlayer->getCharacter();

	if (mPlayerId == 2)
	{
		loadConfig("characters/slyngel.xml");
		mKeepAlive = true;
	}
	else
		loadConfig("characters/" + mName + ".xml");

	mArmRot = 0.0f;
	mLegRot = 0.0f;
	mArmAcc = 0.0f;
	mLegCirculate = 0.0f;
	mThrustAng = 0.0f;
	mThrowTimer = 0.0f;
	mAnimSpeed = 0.0f;

	mAirTime = 0.0f;
	mRagDoll = false;

	mTracksFmt.addField("aPosition", QiVertexFormat::FLOAT32, 3);
	mTracksVb.init(mTracksFmt, 8*TRACKS_COUNT);
	mTracksIb.init(12*TRACKS_COUNT);
	for(int i=0; i<2*TRACKS_COUNT; i++)
		mTracksIb.quad(i*4+0, i*4+1, i*4+2, i*4+3);
	mTracksIb.makeIbo();
	mTracksVerts0.redim(TRACKS_COUNT*4);
	mTracksVerts1.redim(TRACKS_COUNT*4);
	mTrackCurrent = 0;
	mTrackBody = NULL;

	mRailSound = mLevel->mResMan->acquireSound("snd/rail.ogg");
	mRailEndSound = mLevel->mResMan->acquireSound("snd/rail_end.ogg");
	mWireSound = mLevel->mResMan->acquireSound("snd/wire.ogg");
	mWireOnSound = mLevel->mResMan->acquireSound("snd/wire_on.ogg");
	mWireOffSound = mLevel->mResMan->acquireSound("snd/wire_off.ogg");
	
	if (mPlayerId == 1)
	{
		mSkatesGeneric = mLevel->mResMan->acquireSound("snd/character/skates_gravel.ogg");
		mSkatesGrass = mLevel->mResMan->acquireSound("snd/character/skates_grass.ogg");
		mSkatesMud = mLevel->mResMan->acquireSound("snd/character/skates_mud.ogg");
		mSkatesMetal  = mLevel->mResMan->acquireSound("snd/character/skates_metal.ogg");
		mSkatesWood = mLevel->mResMan->acquireSound("snd/character/skates_wood.ogg");
		mSkatesRock = mLevel->mResMan->acquireSound("snd/character/skates_rock.ogg");
	}
	else
	{
		mSkatesGeneric = mLevel->mResMan->acquireSound("snd/character/skates_badguy.ogg");
		mSkatesGrass = mLevel->mResMan->acquireSound("snd/character/skates_badguy.ogg");
		mSkatesMud = mLevel->mResMan->acquireSound("snd/character/skates_badguy.ogg");
		mSkatesMetal  = mLevel->mResMan->acquireSound("snd/character/skates_badguy.ogg");
		mSkatesWood = mLevel->mResMan->acquireSound("snd/character/skates_badguy.ogg");
		mSkatesRock = mLevel->mResMan->acquireSound("snd/character/skates_badguy.ogg");
	}
	
	mRollingChannel = gGame->mAudio->acquireChannel();
	if (mRollingChannel)
	{
		mRollingChannel->setBuffer(mSkatesGeneric.getSound());
		mRollingChannel->setVolume(0);
		mRollingChannel->setLooping(true);
		mRollingChannel->play();
	}
	mRailChannel = gGame->mAudio->acquireChannel();
	if (mRailChannel)
		mRailChannel->setLooping(true);
		
	mVoiceChannel = gGame->mAudio->acquireChannel();

	mCaneHitBadGuySound = mLevel->mResMan->acquireSound("snd/cane_hitbadguy.ogg");
	mHelmetOnSnd = mLevel->mResMan->acquireSound("snd/character/helmet_on.ogg");
	mHelmetOffSnd = mLevel->mResMan->acquireSound("snd/character/helmet_off.ogg");
	mSplashSnd = mLevel->mResMan->acquireSound("snd/character/splash.ogg");
	mCoinsDropSnd = mLevel->mResMan->acquireSound("snd/coins_drop.ogg");

	if (mPlayerId == 1)
	{
		mSndGrunt.load(mLevel->mResMan, "snd/" + mName + "/grunt");
		mSndScream.load(mLevel->mResMan, "snd/" + mName + "/scream");
		mSndJump.load(mLevel->mResMan, "snd/" + mName + "/jump");
		mSndLand.load(mLevel->mResMan, "snd/" + mName + "/land");
		mSndOh.load(mLevel->mResMan, "snd/" + mName + "/oh");
		mSndUhOh.load(mLevel->mResMan, "snd/" + mName + "/uhoh");
		mSndBreak.load(mLevel->mResMan, "snd/" + mName + "/break");
		mSndYay.load(mLevel->mResMan, "snd/" + mName + "/yay");
		mSndLaugh.load(mLevel->mResMan, "snd/" + mName + "/laugh");
		mSndMurmur.load(mLevel->mResMan, "snd/" + mName + "/murmur");
	}

	mCurrentFace = FACE_IDLE;
}


Dude::~Dude()
{
	for(int i=0; i<mCloths.getCount(); i++)
		QI_DELETE(mCloths[i]);

	if (mJoint)
		mLevel->mPhysWorld->DestroyJoint(mJoint);

	if (mRollingChannel)
		gGame->mAudio->releaseChannel(mRollingChannel);

	if (mVoiceChannel)
		gGame->mAudio->releaseChannel(mVoiceChannel);

	if (mRailChannel)
		gGame->mAudio->releaseChannel(mRailChannel);

	if (mContactCallback)
	{
		mLevel->mPhysWorld->SetContactListener(NULL);
		QI_DELETE(mContactCallback);
	}

	mLevel->destroy(mTorso);
}


void Dude::onStart()
{
	mDustParticles.load("dust");
	mDustParticles.mEndRadius *= 2.0f;
	mPebbleParticles.load("pebbles");
	mGrassParticles.load("grass");
	mWaterParticles.load("water");
	mMudParticles.load("mud");
	mOilParticles.load("oil");
	mSparkParticles.load("sparks");

	mStartDelay = gGame->isHard() ? mLevel->mProperties.getFloat("startDelayHard") : mLevel->mProperties.getFloat("startDelay");
}


void Dude::die()
{
	if (mDead)
		return;
	if (mHelmetTimer > 0.0f)
	{
		if (mHelmetTimer > 0.5f)
		{
			mSpawnHelmet = true;
			mHelmetTimer = 0.0f;
		}
	}
	else
	{
		if (!mKeepAlive && mReviveTimer > 1.0f)
		{
			mReplayHints |= HINT_DIE;			
			mDead = true;
		}
	}
}


void Dude::setTransform(const QiTransform2& t)
{
	Entity::setTransform(t);
	mTorso->setTransform(t);
	mAngle = t.rot;

	updatePose();
	transformParts();
	for(int i=0; i<mCloths.getCount(); i++)
		mCloths[i]->reshape();
}


void Dude::processControls(bool disableInput)
{
	bool oldCrouch=mCrouch;
	bool oldGrab=mGrab;
	int powerUp = 0;

	if (!mInput)
	{
		mInputFrame = true;
		if (!disableInput)
		{
			mCrouch = !gGame->mProperties.getBool("ctrljump");
			mGrab = gGame->mProperties.getBool("ctrlcane");

			if (gGame->mProperties.getBool("ctrlbaseball") && gGame->mPlayer->getPowerUpCount("baseball") > 0)
				powerUp |= 1;
			if (gGame->mProperties.getBool("ctrlbanana") && gGame->mPlayer->getPowerUpCount("banana") > 0)
				powerUp |= 2;
			if (mHelmetTimer == 0.0f && gGame->mPlayer->getPowerUpCount("helmet") > 0)
				powerUp |= 4;
		}
			
		QiVec2 ta = mTransform.pos-mTimingAidPoint;
		if (dot(ta, mTimingAidDirection) < 0.0f)
		{
			if (mJumpFrame==0)
				mCrouch = true;
			if (mGrabbedHandle && !mGrab)
				mGrab = true;
		}
	}
	else
	{
		if (!mInput || mFrame < mLevel->mFrame)
		{
			mFrame += 1.0f/getSpeedMultiplier();
			//Load from stream
			QiUInt32 flags = 0;
			if (!mInput->readUInt32(flags))
				return;
			mCrouch = ((flags & 0x01) != 0);
			mGrab = ((flags & 0x02) != 0);
			mDead = ((flags & 0x04) != 0);
			powerUp = ((flags >> 8) & 0xFF);
			mInput->readUInt16(mReplayHints);
			bool correction = ((flags & 0x10000) != 0);
			if (correction)
			{
				float vx=0, vy=0, va=0;
				QiTransform2 t;
				mInput->readFloat32(t.pos.x);
				mInput->readFloat32(t.pos.y);
				mInput->readFloat32(t.rot);
				mInput->readFloat32(vx);
				mInput->readFloat32(vy);
				mInput->readFloat32(va);
				if (!gCapture && !mNoCorrection)
				{
					mTorso->setTransform(t);
					mTorso->mPhysBody->SetLinearVelocity(b2Vec2(vx, vy));
					mTorso->mPhysBody->SetAngularVelocity(va);
				}
			}
			mInputFrame = true;
		}
		else
			mInputFrame = false;
	}

	if (mOutput)
	{
		//Save to stream
		bool correction = (gGame->mFrame%5 == 0);
		if (mPlayerId == 2)
			mDead = false;
		QiUInt32 flags = (mCrouch ? 0x01 : 0) | (mGrab ? 0x02 : 0) | (mDead ? 0x04 : 0);
		flags |= (powerUp << 8);
		if (correction)
			flags |= 0x10000;
		mOutput->writeUInt32(flags);
		mOutput->writeUInt16(mReplayHints);
		if (correction)
		{
			mOutput->writeFloat32(mTorso->mTransform.pos.x);
			mOutput->writeFloat32(mTorso->mTransform.pos.y);
			mOutput->writeFloat32(mTorso->mTransform.rot);
			mOutput->writeFloat32(mTorso->mPhysBody->GetLinearVelocity().x);
			mOutput->writeFloat32(mTorso->mPhysBody->GetLinearVelocity().y);
			mOutput->writeFloat32(mTorso->mPhysBody->GetAngularVelocity());
		}
		mReplayHints = 0;
	}

	if (mPlayerId == 1)
	{
		if ((powerUp & 1) == 1)
		{
			//Baseball
			mThrowTimer = 0.3f;
			QiVec2 v = getVelocity();
			//mLevel->mThrowables->spawn(1, mTransform.pos + QiVec2(0.5f, 0.7f), v + QiVec2(12.0f, 2.0f));
			QiVec2 vel = v + mTransform.toParentVec(QiVec2(12.0f, 2.0f));
			if (mGrabbedHandle || mGrabbedWire || mRailBody)
				vel = v + mTransform.toParentVec(QiVec2(12.0f, -2.0f));
			mLevel->mThrowables->spawn(1, mTransform.pos + QiVec2(0.5f, 0.7f), vel);
			if (!mLevel->mReplay)
				gGame->mPlayer->consumePowerUp("baseball");
		}
		if ((powerUp & 2) == 2)
		{
			//Banana
			mThrowTimer = -0.3f;
			QiVec2 v = getVelocity();
			mLevel->mThrowables->spawn(2, mTransform.pos + QiVec2(0.7f, 0.8f), v + QiVec2(-3.0f, 4.0f));
			if (!mLevel->mReplay)
				gGame->mPlayer->consumePowerUp("banana");
		}
		if ((powerUp & 4) == 4)
		{
			//Helmet
			gGame->mAudio->playSound(mHelmetOnSnd.getSound());
			mHelmetTimer = 0.01f;
			if (!mLevel->mReplay)
				gGame->mPlayer->consumePowerUp("helmet");
		}
	}

	mJump = (oldCrouch && !mCrouch);

	if (mGrab)
	{
		mGrabTimer += gGame->mTimeStep;
		if (!oldGrab)
		{
			gGame->mAudio->playSound(gGame->mAudio->mCaneSwoosh.next(), mTransform.pos);
			float dist = length(mLevel->mBadGuy->mTransform.pos - mTransform.pos);
			if (mPlayerId == 1 && dist > 0.0f && dist < 1.5f)
			{
				mReplayHints |= HINT_HIT;
				mLevel->mBadGuy->slowDown();
			}
		}
	}
	else
	{
		mGrabTimer = 0.0f;
		if (mGrabJoint)
			release();
	}
}


void Dude::addTrack()
{
	if (!mGroundHit || mGroundBody->mDynamic || mGroundBody->mNoFx || mDead)
	{
		mTrackBody = NULL;
		return;
	}
	static const float minDist = 0.1f;
	QiVec2 p = mGroundPoint + mGroundNormal*0.05f;
	QiVec2 last = mTracksVerts1[(mTrackCurrent+TRACKS_COUNT-1)%TRACKS_COUNT];
	if (mGroundBody == mTrackBody)
	{
		if (lengthSquared(p-last) > minDist*minDist)
		{
			mTracksVerts0[mTrackCurrent] = last;
			mTracksVerts1[mTrackCurrent] = p;
			mTrackCurrent = (mTrackCurrent+1)%TRACKS_COUNT;
		}
	}
	else
	{
		mTracksVerts0[mTrackCurrent] = p;
		mTracksVerts1[mTrackCurrent] = p;
		mTrackCurrent = (mTrackCurrent+1)%TRACKS_COUNT;
	}
	mTrackBody = mGroundBody;
}


void Dude::update()
{
	PROFILE_ZONE("Dude::update");

	float flipUp = 1.0f;
	if (mLevel->mUpSideDown)
		flipUp = -1.0f;

	if (mInput)
		mTorso->mPhysBody->mSpeedMultiplier = getSpeedMultiplier();

	if (mSlowDownTimer > 0.0f)
		mSlowDownTimer -= gGame->mTimeStep;

	if (mPlayerId == 1 && mLevel->mSimTime < mStartDelay)
	{
		QiTransform2 t(mProperties.getVec2("pos"), mProperties.getFloat("rot"));
		setTransform(t);
		mTorso->mPhysBody->SetLinearVelocity(b2Vec2(0.0f, 0.0f));
		mTorso->mPhysBody->SetAngularVelocity(0.0f);
		processControls(true);
		return;
	}
	else
	{
		processControls(mLevel->mCutScene);
		if (mLevel->mCutScene)
		{
			mCrouch = true;
			mJump = false;
			mGrab = false;
		}
	}

	if (mJumpFrame > 0)
	{
		if (mInputFrame)
			mJumpFrame++;
	}

	if (!mContactCallback && this == mLevel->mDude)
	{
		mContactCallback = QI_NEW ContactCallback(this);
		mLevel->mPhysWorld->SetContactListener(mContactCallback);
	}
	
    mBoost = false;
	mTransform = mTorso->mTransform;
	
	if (mJoint)
	{
		mLevel->mPhysWorld->DestroyJoint(mJoint);
		mJoint = NULL;
	}

	float halfHeight = 0.6f;
	float fullHeight = 1.0f;
	float legStrength = 0.1f;

	if (mGrab)
	{
		if (!mGrabJoint)
			grab();
	}

	if (mCrouch && mGroundHit)
        mTargetHeight = QiMax(mTargetHeight-0.05f, halfHeight);
    else
        mTargetHeight = QiMin(mTargetHeight+0.2f, fullHeight);

	QiVec2 currentUp = mTorso->mTransform.toParentVec(QiVec2(0, flipUp));

	QiVec2 v = mTorso->getVelocity();

	float rayHeight = (mJumpFrame > 0) ? halfHeight : fullHeight;
	Body *b = NULL;
	QiVec2 p, n;
	bool c = mLevel->raycast(mTorso->mTransform.pos, mTorso->mTransform.pos + mTorso->mTransform.toParentVec(QiVec2(0, -flipUp*rayHeight-0.1f)), 252, mPlayerId, &p, &n, &b);
	float d = dot(currentUp, n);

	Body *b3 = NULL;
	QiVec2 p3, n3;
	bool c3 = mLevel->raycast(mTorso->mTransform.pos, mTorso->mTransform.pos + mTorso->mTransform.toParentVec(QiVec2(flipUp*0.2f, -flipUp*rayHeight-0.1f)), 252, mPlayerId, &p3, &n3, &b3);
	float d3 = dot(currentUp, n3);
	if (c3 && d3 > d)
	{
		c = true;
		b = b3;
		p = p3;
		n = n3;
	}
	
	if (!c)
		c = mLevel->raycast(mTorso->mTransform.pos, mTorso->mTransform.pos + QiVec2(0, -flipUp*rayHeight-flipUp*0.05f+flipUp*QiClamp(v.y*gGame->mTimeStep, -0.5f, 0.0f)), 252, mPlayerId, &p, &n, &b);

	if (c && b && b->mSlip)
	{
		c = false;
		b = NULL;
	}
	
	float damp = 0.05f + (c ? 0.5f * (mTargetHeight-halfHeight)/(fullHeight-halfHeight) : 0.0f);
	mTorso->mPhysBody->SetLinearDamping(damp);

	b2MassData md;
	mTorso->mPhysBody->GetMassData(&md);
	QiVec2 jointPos = vec(md.center);
	if (c && b)
	{
		b2MotionJointDef def;
		def.collideConnected = true;
		def.bodyA = b->mPhysBody;
		def.bodyB = mTorso->mPhysBody;
		mJoint = (b2MotionJoint*)mLevel->mPhysWorld->CreateJoint(&def);
		mJoint->m_upMin = 0.0f;
		mJoint->m_upMax = 0.0f;
		mJoint->m_tanMin = 0.0f;
		mJoint->m_tanMax = 0.0f;
		mJoint->m_rotMin = 0.0f;
		mJoint->m_rotMax = 0.0f;

		float l = length(mTorso->mTransform.pos - p);

		n = normalize(n);
		mUp = n;

		mAngle = QiATan2(mUp.y, mUp.x) - QI_PI*0.5f;

		mJoint->m_point = vec(mTorso->mTransform.toParentPoint(jointPos));
		mJoint->m_up = vec(mUp);
		mJoint->m_upTarget = QiClamp(mTargetHeight-l, -0.03f, 0.03f) * 60.0f;
		legStrength = 0.3f;
		mJoint->m_upMax = legStrength * QiClamp((mTargetHeight - l)*3.0f, 0.2f, 1.0f);
		mJoint->m_tan = -vec(perpendicular(mUp)*flipUp);

		if (!mGroundHit)
		{
			//Reset ground normal ring buffer at touch down
			mGroundNormal = n;
			for(int i=0; i<5; i++)
				mOldNormal[i] = n;
		}
		else
		{
			for(int i=4; i>=1; i--)
				mOldNormal[i] = mOldNormal[i-1];
			mOldNormal[0] = mGroundNormal;
		}

		if (mCrouch || mJumpFrame > 30)
			mJumpFrame = 0;
		
		float ang = QiAngle(mAngle, mTransform.rot);
		mJoint->m_rotTarget = QiClamp(ang*15.0f, -10.0f, 10.0f);

		if (!mContactBody)
		{
			float maxLandingAngle = 1.6f;
			float landPenalty = 0.75f;
			if (gGame->isHard())
			{
				maxLandingAngle = 1.0f;
				landPenalty = 0.9f;
			}
			float angDiff = QiAbs(QiAngle(mTorso->mPhysBody->GetAngle() + QI_PI*0.5f, QiATan2(n.y, n.x)));
			if (angDiff > maxLandingAngle)
				die();
			else
			{
				float landing = 1.0f-angDiff/maxLandingAngle;
				landing = QiClamp(landing, 0.0f, 1.0f);
				float ang = QiAbs(mRotAngle);
				if (ang > QI_PI && mPlayerId == 1)
				{
					//"Perfect" landing should give perfect score
					if (landing > 0.5f && !mDead && !mRagDoll && mLevel->mProperties.getString("state")=="play")
					{
						gGame->mHudScene->mScript.handleCommand(QiString("land ") + landing + QiString(" ") + ang);

						if (landing > 0.9f)
							landing = 1.0f;

						int score = 0;
						if (ang > 5.0f && ang < 8.5f)
							score = 100;
						else if (ang > 11.3f && ang < 14.8f)
							score = 300;
						else if (ang > 18.0f)
							score = 500;
						score = (int)(score*landing);
						if (score > 0)
							gGame->mHudScene->mScript.handleCommand(QiString("score ") + score);
						mLevel->mScore += score;
					}
				}
				float f = QiInterpolateLinear(1.0f-landPenalty, 1.0f, landing);
				mTorso->mPhysBody->SetLinearVelocity(f*mTorso->mPhysBody->GetLinearVelocity());

				if (landing > 0.8f && QiAbs(mRotAngle) > 3*QI_PI)
					mReplayHints |= HINT_LAND_GOOD;

				mReplayHints |= HINT_LAND;
				if (mPlayerId == 1 && mAirTime > 2.0f || QiAbs(mRotAngle) > QI_PI)
				{
					if (landing > 0.8f && QiAbs(mRotAngle) > QI_PI)
						playSound(mSndYay.next());
					else
						playSound(mSndLand.next());
				}
				gGame->mAudio->playSound(gGame->mAudio->mSkatesLand.next(), mTransform.pos, QiClamp(-dot(mTorso->getVelocity(), n)*0.03f, 0.0f, 1.0f));
			}
			
			if (b->mSurface == Body::WATER)
			{
				gGame->mAudio->playSound(mSplashSnd.getSound(), mTransform.pos);
				QiVec3 p = mTransform.pos.vec3();
				p.y -= 0.5f;
				for(int i=0; i<30; i++)
					mWaterParticles.spawn(p, 6.0f*(n*0.7f + QiVec2::random(QiRnd(-0.3f, 0.3f))).vec3());			
			}
		}

		if (mJumpFrame == 0)
			mRotVel = 0.0f;

		if (mJumpFrame == 0)
			mGroundNormal = n;

		mGroundHit = true;
		mGroundBody = b;

		mGroundPoint = p;

		mAirTime = 0.0f;
		mGroundTime += gGame->mTimeStep;
		mRotAngle = 0.0f;

		if (flipUp * mGroundNormal.y > 0.3f && !mGroundBody->mSlip && !mGroundBody->mRoll)
		{
			float baseVel = 10.0f;
			float minVel = 7.0f;
			float maxVel = 20.0f;
			float velAcc = 0.2f;
			float velDec= 0.1f;
			QiVec2 vel = getVelocity() - mGroundBody->getVelocity();
			float v = dot(vec(mJoint->m_tan), vel);

			float targetVel = baseVel + QiClamp(mGroundNormal.x*30.0f, minVel-baseVel, maxVel-baseVel);
			if (mSensorSpeed != 0.0f)
			{
				targetVel = mSensorSpeed;
				velAcc = 0.3f;
				velDec = 0.3f;
			}

			if (mInputFrame)
				v += QiClamp(targetVel - v, -velDec, velAcc);

			mJoint->m_tanMax = 0.1f;
			mJoint->m_tanMin = -0.1f;
			mJoint->m_tanTarget = v;
		}
	}
	else
    {
		b2MotionJointDef def;
		def.collideConnected = true;
		def.bodyA = mLevel->mPhysGround;
		def.bodyB = mTorso->mPhysBody;
		mJoint = (b2MotionJoint*)mLevel->mPhysWorld->CreateJoint(&def);
		mJoint->m_point = vec(mTorso->mTransform.toParentPoint(jointPos));
		mJoint->m_upMin = 0.0f;
		mJoint->m_upMax = 0.0f;
		mJoint->m_tan.Set(1.0f, 0.0f);
		mJoint->m_tanMin = 0.0f;
		mJoint->m_tanMax = 0.0f;
		mJoint->m_rotMin = 0.0f;
		mJoint->m_rotMax = 0.0f;
		
		if (mInputFrame)
		{
			float rotAcc = 0.6f;
			if (!mCrouch && !mJump)
				if (mJumpFrame > 0)
					mRotVel -= flipUp * rotAcc * QiMin(mJumpFrame / 10.0f, 1.0f);
				else
					mRotVel -= flipUp * rotAcc;
			mRotVel *= 0.92f;
		}
		
		mJoint->m_rotTarget = mRotVel;

		mGroundHit = false;


		float dr = mRotVel*gGame->mTimeStep;
		if (!mCrouch)
		{	
			if (-mRotAngle < QI_PI && -mRotAngle-dr > QI_PI ||
				-mRotAngle < 3*QI_PI && -mRotAngle-dr > 3*QI_PI ||
				-mRotAngle < 5*QI_PI && -mRotAngle-dr > 5*QI_PI)
				gGame->mAudio->playSound(gGame->mAudio->mRotate.next(), mTransform.pos);
				
		}
		mRotAngle += dr;
		
		mAirTime += gGame->mTimeStep;
   		mGroundTime = 0.0f;

		//Add some base speed in jumps. This will help jumping over obstacles when stuck
		if (mJumpFrame > 0 && mJumpFrame < 30 && mGroundNormal.y > 0.8f)
		{
			mJoint->m_tan = -vec(perpendicular(mGroundNormal));
			mJoint->m_tanMax = 0.1f;
			mJoint->m_tanMin = 0.0f;
			mJoint->m_tanTarget = QiClamp(mJumpFrame*0.2f, 0.0f, 4.0f);
		}
	}
	
	//Slow down if level is complete
	if (mTransform.pos.x > mLevel->mGoalX)
	{
		mJoint->m_tanTarget = 0.0f;
		mJoint->m_tanMax = (mPlayerId == 1 ? 0.03f : 0.02f);
		mJoint->m_tanMin = -mJoint->m_tanMax;
	}

	//Jump
	if (mInputFrame && mJump && (mGroundHit || mAirTime < 0.1f))
	{
		mReplayHints |= HINT_JUMP;
		float jumpSpeed= 9.0f;
		mJumpFrame = 1;
		for(int i=0; i<5; i++)
		{
			if (flipUp*mOldNormal[i].y > flipUp*n.y)
				mGroundNormal = mOldNormal[i];
		}
		mJumpOrgVel = dot(mGroundNormal, mTorso->getVelocity());
		mJoint->m_up = vec(mGroundNormal);
		mJoint->m_upTarget = jumpSpeed;
		mJoint->m_upMax = 10.0f;
		gGame->mAudio->playSound(gGame->mAudio->mSkatesLeave.next(), mTransform.pos);
		playSound(mSndJump.next());
	}
	
	//Rotate back to upright if aborting a jump
	if (mCrouch && mJumpFrame > 0 && mJumpFrame < 20)
    {
		if (mInputFrame)
		{
			float a = QiAngle(mTransform.rot, mAngle);
			mRotVel -= QiClamp(a*3.0f, -1.5f, 1.5f);
			mRotVel *= 0.85f;
		}
    }
	
	//Reduce upward speed when aborting a jump
	if (mInputFrame)
	{
		float f = QiClamp(mGroundNormal.y, 0.0f, 1.0f);
		if (mCrouch && mJumpFrame >= 10 && mJumpFrame < 25 && mAbortJumpFrame < 7)
		{
			mJumpOrgVel = 0.0f;
			mAbortJumpFrame++;
			mJoint->m_up = vec(mGroundNormal);
			mJoint->m_upTarget = mJumpOrgVel + QiClamp(7.0f - mAbortJumpFrame*1.0f, 0.0f, 10.0f);
			mJoint->m_upMax = 0.0f;
			mJoint->m_upMin = -0.1f * f;
		}
		else
			mAbortJumpFrame = 0;
	}

	//Jump longer jumps when holding button,but only first revolution and only for actual jump
	if (!mCrouch && QiAbs(mRotAngle) < QI_TWO_PI && mJumpFrame > 0)
		mTorso->mPhysBody->ApplyForce(b2Vec2(0, flipUp*1.0f), mTorso->mPhysBody->GetPosition());

	if (mGrabbedHandle)
	{
		if (mGrabbedHandle->mProperties.getBool("followRotation") && mGrabbedHandle->mBody)
		{
			float a = QiAngle(mTransform.rot, mGrabbedHandle->mBody->mTransform.rot + 0.0f);
			mRotVel = -QiClamp(a*15.0f, -15.0f, 15.0f) + mGrabbedHandle->mBody->mPhysBody->GetAngularVelocity();
			mJoint->m_rotTarget = mRotVel;
			mJoint->m_rotMin = -10.0f;
			mJoint->m_rotMax = 10.0f;
		}
	}

	addTrack();

	if (!mDead && mAirTime > 2.0f)
	{
		float ySpeed = mTorso->getVelocity().y;
		if (mLastYSpeed > -10.0f && ySpeed < -10.0f && QiRnd(0.0f, 1.0f) < 0.7f)
			playSound(mSndOh.next());
		if (mLastYSpeed > -24.0f && ySpeed < -24.0f)
			playSound(mSndUhOh.next());
		mLastYSpeed = ySpeed;
	}

	mContactBody = b;

	mSpeed = length(mTorso->getVelocity());

	if (mDead && mGroundHit)
	{
		mJoint->m_tanTarget = 0.0f;
		mJoint->m_tanMin = -0.03f;
		mJoint->m_tanMax = 0.03f;
	}

	if (mGrabJoint)
	{
		if (mGrabJoint)
		{
			if (mGrabJoint->m_correctionMultiplier < 1.0f)
				mGrabJoint->m_correctionMultiplier += 0.01f;
		}
	}
	else if (mGrabbedWire || mRailBody)
	{
		mJoint->m_rotTarget = 0.0f;
		mJoint->m_rotMin = -0.005f;
		mJoint->m_rotMax = 0.005f;
	}
	else
	{
		mJoint->m_rotMin = -10.0f;
		mJoint->m_rotMax = 10.0f;
	}

	updatePose();
	transformParts();

	if (mDead && !mRagDoll)
	{
		playSound(mSndScream.next());
		if (mLevel->mDieTimer == 0.0f)
		{
			int c = QiMin(mLevel->mCoins, 10);
			if (mLevel->mReplay)
				c = 5;
			if (c > 0)
			{
				for(int i=0; i<c; i++)
					mLevel->mThrowables->spawn(4, mTransform.pos, QiVec2(getVelocity().x*0.7f + QiRnd(-5.0f, 5.0f), QiRnd(2.0f, 6.0f)));
				gGame->mAudio->playSound(mCoinsDropSnd.getSound(), mTransform.pos);
	            if (!mLevel->mReplay)
	                mLevel->mCoins -= c;
			}

			//First level wants notification here to show message
			if (mLevel->mScript)
				mLevel->mScript->handleCommand("die");
		}
		makeRagDoll();
	}

	for(int i=0; i<mCloths.getCount(); i++)
		mCloths[i]->update();

	mBoundsMin = mTransform.pos + QiVec2(-2, -2);
	mBoundsMax = mTransform.pos + QiVec2(2, 2);

	if (mGroundHit && !mGroundBody->mNoFx && mInputFrame)
	{
		ParticleSystem* dust = NULL;
		ParticleSystem* things = NULL;
		
		Body::Surface mat = mGroundBody ? mGroundBody->mSurface : Body::GENERIC;
		if (mat == Body::GRASS)
			things = &mGrassParticles;
		else if (mat == Body::WATER)
			things = &mWaterParticles;
		else if (mat == Body::OIL)
			things = &mOilParticles;
		else if (mat == Body::MUD)
			things = &mMudParticles;
		else if (mat == Body::WOOD)
			dust = &mDustParticles;
		else if (mat == Body::ROCK)
			things = &mPebbleParticles;
		else if (mat == Body::GRAVEL)
		{
			things = &mPebbleParticles;
			dust = &mDustParticles;
		}
		
		QiVec2 v = mTorso->getVelocity();
		int df = 3;
		int tf = 2;
		if (gGame->extraEffects())
		{
			df = 7;
			tf = 3;
		}
		if (length(v) > 3 && QiRnd(0, 10) < df && dust)
			dust->spawn(mGroundPoint.vec3(), (mGroundNormal*0.5f + v*0.6f - QiVec2::random(0.2f)).vec3());
		if (length(v) > 3 && QiRnd(0, 10) < tf && things)
			things->spawn(mGroundPoint.vec3(), (mGroundNormal*2 + v*0.4f - QiVec2::random(1.0f)).vec3());

		if (dot(v, mGroundNormal) < -3.0f)
		{
			for(int i=0; i<12; i++)
			{
				if (dust)
					dust->spawn(mGroundPoint.vec3(), (mGroundNormal*0.7f - QiVec2::random(QiRnd(0.3f, 0.7f))).vec3());
				if (things)
					things->spawn(mGroundPoint.vec3(), (mGroundNormal*2.5f - QiVec2::random(QiRnd(0.5f, 1.2f))).vec3());
			}		
		}
	}

	mDustParticles.update();
	mPebbleParticles.update();
	mGrassParticles.update();
	mWaterParticles.update();
	mMudParticles.update();
	mOilParticles.update();
	mSparkParticles.update();

	if (mRollingChannel)
	{
		if (mGroundHit)
		{
			QiAudioBuffer* buffer = NULL;
			Body::Surface mat = mGroundBody ? mGroundBody->mSurface : Body::GENERIC;
			if (mat == Body::GRASS)
				buffer = mSkatesGrass.getSound();
			else if (mat == Body::METAL)
				buffer = mSkatesMetal.getSound();
			else if (mat == Body::WOOD)
				buffer = mSkatesWood.getSound();
			else if (mat == Body::ROCK)
				buffer = mSkatesRock.getSound();
			else if (mat == Body::MUD || mat == Body::WATER || mat == Body::OIL)
				buffer = mSkatesMud.getSound();
			else
				buffer = mSkatesGeneric.getSound();
				
			if (mRollingChannel->getBuffer() != buffer)
				mRollingChannel->setBuffer(buffer);
				
			float v = length(mTorso->getVelocity());
			float vol = QiClamp(v*0.05f, 0.0f, 1.0f);
			mRollingChannel->setPitch((0.7f + QiClamp(v*0.01f, 0.0f, 0.3f)));
			if (mLevel->mCutScene)
				vol = 0.0f;
			gGame->mAudio->setSoundChannelVolume(mRollingChannel, mTransform.pos, vol);
		}
		else
			gGame->mAudio->setSoundChannelVolume(mRollingChannel, 0.0f);
	}

	doWire();
	doRail();

	if (mSensorSpeedAir > 0.0f && (mRailBody || mGrabbedWire))
	{
		QiVec2 vel = getVelocity();
		QiVec2 t = vec(mJoint->m_tan);
		float v = dot(t, vel);
		float vDiff = 0.0f;
		if (mInputFrame)
			vDiff = QiClamp(mSensorSpeedAir - v, -0.3f, 0.3f);
		vel += t*vDiff;
		mTorso->mPhysBody->SetLinearVelocity(vec(vel));
	}

	mSensorSpeed = 0.0f;
	mSensorSpeedAir = 0.0f;

	if (mLevel->mReplay)
	{
		if (!mDead && mRagDoll)
			unRagDoll();
	}
	else
	{
		if (mDead && mRagDoll)
		{
			mReviveTimer = 0.0f;

			if (length(vec(mParts[TORSO].mPhysBody->GetLinearVelocity())) < 1.0f)
				mDeadTimer += gGame->mTimeStep;
			else if (mGroundHit)
				mDeadTimer += gGame->mTimeStep * 0.4f;

			if (mDeadTimer > 0.8f)
			{
				mDead = false;
				unRagDoll();
			}
		}
		else
		{
			mDeadTimer = 0.0f;
			mReviveTimer += gGame->mTimeStep;
		}
	}
	
	mFacialExpressionTimer -= gGame->mTimeStep;

	if (mInputFrame)
	{
		if (mFacialExpressionTimer < 0.0f)
			mCurrentFace = FACE_IDLE;
	
		if (!mGroundHit && mFacialExpressionTimer < 0.0f)
		{
			if (QiAbs(mRotAngle) > QI_PI)
				mCurrentFace = FACE_OOPS;
			else
				mCurrentFace = FACE_FLY;
		}
	
		if (mGroundHit && mCurrentFace == FACE_IDLE && QiRnd(0.0f, 1.0f) < 0.02f*(gGame->mTimeStep/0.01667f))
		{
			mCurrentFace = FACE_BLINK;
			mFacialExpressionTimer = 0.1f;
		}
	
		if (mDead)
			mCurrentFace = FACE_CRASH;

		if (mLevel->mProperties.getString("state") == "cleared")
			mCurrentFace = FACE_WIN;
		if (mLevel->mProperties.getString("state") == "failed")
			mCurrentFace = FACE_LOOSE;
	}
	
	if (mSpawnHelmet)
	{
		gGame->mAudio->playSound(mHelmetOffSnd.getSound());
		mLevel->mThrowables->spawn(3, mTransform.pos, QiVec2::random(5.0f));
		mTorso->mPhysBody->SetLinearVelocity(0.25f*mTorso->mPhysBody->GetLinearVelocity());
		mSpawnHelmet = false;
	}

	if (mHelmetTimer > 0.0f)
		mHelmetTimer += gGame->mTimeStep;
		
	b2MassData mdat;
	mTorso->mPhysBody->GetMassData(&mdat);
	float rotI = 0.1f;
	if (mGrab && (mRailBody || mGrabbedHandle || mGrabbedWire))
	{
		mRotAngle = 0.0f;
		if (QiAbs(mdat.I-rotI) > 0.01f)
		{
			mdat.I = rotI;
			mTorso->mPhysBody->SetMassData(&mdat);
		}
	}
	else
	{
		if (QiAbs(mdat.I-rotI) < 0.01f)
			mTorso->mPhysBody->ResetMassData();
	}
}


bool Dude::contains(const QiVec2& point) const
{
	return mTorso->contains(point);
}


QiVec2 Dude::getVelocity() const
{
	if (mRagDoll)
		return vec(mParts[TORSO].mPhysBody->GetLinearVelocity());
	else
		return mTorso->getVelocity();
}

void Dude::renderTransparent()
{
    if (!PROFILE_PARAM("render/dude", true))
        return;
    
    PROFILE_ZONE("Dude::renderTransparent");
    
	if (PROFILE_PARAM("render/dude/tracks", true) && this == mLevel->mDude)
	{
		//Tracks
		float z0 = 0.05f;
		float z1 = 0.15f;
		mTracksVb.clear();
		for(int i=0; i<TRACKS_COUNT; i++)
		{
			mTracksVb.vertex();
			mTracksVb.addFast(QiVec3(mTracksVerts0[i].x, mTracksVerts0[i].y, z1));
			mTracksVb.vertex();
			mTracksVb.addFast(QiVec3(mTracksVerts1[i].x, mTracksVerts1[i].y, z1));
			mTracksVb.vertex();
			mTracksVb.addFast(QiVec3(mTracksVerts1[i].x, mTracksVerts1[i].y, z0));
			mTracksVb.vertex();
			mTracksVb.addFast(QiVec3(mTracksVerts0[i].x, mTracksVerts0[i].y, z0));

			mTracksVb.vertex();
			mTracksVb.addFast(QiVec3(mTracksVerts0[i].x, mTracksVerts0[i].y, -z1));
			mTracksVb.vertex();
			mTracksVb.addFast(QiVec3(mTracksVerts1[i].x, mTracksVerts1[i].y, -z1));
			mTracksVb.vertex();
			mTracksVb.addFast(QiVec3(mTracksVerts1[i].x, mTracksVerts1[i].y, -z0));
			mTracksVb.vertex();
			mTracksVb.addFast(QiVec3(mTracksVerts0[i].x, mTracksVerts0[i].y, -z0));
		}
		QiRenderState state;
		state.color.set(0,0,0,0.15f);
		state.blendMode = state.BLEND;
		state.depthMask = false;
		state.depthTest = true;
		state.shader = gGame->mGfx->m2DShader.getShader();
		gGame->mRenderer->setState(state);
		gGame->mRenderer->drawTriangles(&mTracksVb, &mTracksIb);
	}

	mDustParticles.render();
	mPebbleParticles.render();
	mGrassParticles.render();
	mWaterParticles.render();
	mMudParticles.render();
	mOilParticles.render();
	mSparkParticles.render();

	QiRenderState state;
	state.blendMode = state.BLEND;
	state.depthMask = false;
	state.depthTest = true;
	state.texture[0] = mTexture.getTexture();
	state.shader = gGame->mGfx->m2DTexShader.getShader();

	mVb.clear();
	mIb.clear();
	
	if (!mDead)
	{
		float maxShadowDist = 2.0f;
		QiVec2 p, n;
		Body* b = NULL;
		bool c = mLevel->raycast(mTorso->mTransform.pos, mTorso->mTransform.pos + mTorso->mTransform.toParentVec(QiVec2(0, -maxShadowDist)), 252, mPlayerId, &p, &n, &b);
		if (c && b && !b->mNoFx)
		{
			float dist = length(mTorso->mTransform.pos-p);
			p += n*0.05f;
			float angle = QiATan2(n.x, -n.y);
			float size = QiInterpolateLinear(0.0f, 1.5f, QiClamp(1.0f-dist/maxShadowDist, 0.0f, 1.0f));
			QiTransform3 t(QiVec3(p.x, p.y, 0.0f), QiQuat(QiVec3::X, -QI_PI*0.5f));
			t.rot = QiQuat(QiVec3::Z, angle).rotate(t.rot);

			QiMatrix4 m(t);
			QiMatrix4 scale;
			scale.m[0] = size;
			scale.m[5] = size;
			m = m * QiMatrix4(QiVec3(-size/2, -size/2, 0)) * scale;
			
			int v = mVb.getCount();
			QiVec3 p0(0.0f, 0.0f, 0.0f);
			QiVec3 p1(1.0f, 0.0f, 0.0f);
			QiVec3 p2(1.0f, 1.0f, 0.0f);
			QiVec3 p3(0.0f, 1.0f, 0.0f);
			QiVec2 t0 (mShadowTexMin.x, 1.0f-mShadowTexMin.y);
			QiVec2 t1 (mShadowTexMax.x, 1.0f-mShadowTexMin.y);
			QiVec2 t2 (mShadowTexMax.x, 1.0f-mShadowTexMax.y);
			QiVec2 t3 (mShadowTexMin.x, 1.0f-mShadowTexMax.y);
			mVb.vertex();
			mVb.addFast(m.transformPoint(p0));
			mVb.addFast(t0);
			mVb.vertex();
			mVb.addFast(m.transformPoint(p1));
			mVb.addFast(t1);
			mVb.vertex();
			mVb.addFast(m.transformPoint(p2));
			mVb.addFast(t2);
			mVb.vertex();
			mVb.addFast(m.transformPoint(p3));
			mVb.addFast(t3);
			mIb.quad(v+0, v+3, v+2, v+1);	
		}
	}	
	
	QiQuat rot = gGame->mDisplay->mCameraRot;
	QiTransform3 t3(mTransform);
	QiMatrix4 mat = QiTransform3(t3.pos, QiQuat::slerp(t3.rot, rot*t3.rot, 0.5f));
	if (mLevel->mUpSideDown && !mDead)
	{
		mat.m[0] = -mat.m[0];
		mat.m[1] = -mat.m[1];
		mat.m[2] = -mat.m[2];
	}
	
	for(int i=0; i<mDrawOrder.getCount(); i++)
	{
		bool isCloth = ((mDrawOrder[i] & 256)!=0);
		bool isHelmet = ((mDrawOrder[i] & 512)!=0);
		int id = (mDrawOrder[i] & 255);
		if (isCloth)
		{
			Cloth* c = mCloths[id];
			c->render(mVb, mIb);
		}
		else
		{
			const Part& p = (isHelmet ? mParts[HEAD] : mParts[id]);
			QiVec2 s = p.mSize;
			QiVec3 p0 = mat.transformPoint(mTransform.toLocalPoint(p.mTransform.toParentPoint(QiVec2(-s.x, -s.y))).vec3());
			QiVec3 p1 = mat.transformPoint(mTransform.toLocalPoint(p.mTransform.toParentPoint(QiVec2(s.x, -s.y))).vec3());
			QiVec3 p2 = mat.transformPoint(mTransform.toLocalPoint(p.mTransform.toParentPoint(QiVec2(s.x, s.y))).vec3());
			QiVec3 p3 = mat.transformPoint(mTransform.toLocalPoint(p.mTransform.toParentPoint(QiVec2(-s.x, s.y))).vec3());
			
			QiVec2 h = p.mTexHigh;
			QiVec2 l = p.mTexLow;
			if (id == HEAD && mFaces[mCurrentFace].mEnabled)
			{
				l = mFaces[mCurrentFace].mTexLow;
				h = mFaces[mCurrentFace].mTexHigh;
			}
			if (isHelmet)
			{
				if (mHelmetTimer > 0.5f)
				{
					h = mHelmetFrontTexMax;
					l = mHelmetFrontTexMin;
					if (id == 1)
					{
						h = mHelmetBackTexMax;
						l = mHelmetBackTexMin;
					}
				}
				else
					continue;
			}

			int v = mVb.getCount();
			QiVec2 t0 (l.x, 1.0f-l.y);
			QiVec2 t1 (h.x, 1.0f-l.y);
			QiVec2 t2 (h.x, 1.0f-h.y);
			QiVec2 t3 (l.x, 1.0f-h.y);
			mVb.vertex();
			mVb.addFast(p0);
			mVb.addFast(t0);
			mVb.vertex();
			mVb.addFast(p1);
			mVb.addFast(t1);
			mVb.vertex();
			mVb.addFast(p2);
			mVb.addFast(t2);
			mVb.vertex();
			mVb.addFast(p3);
			mVb.addFast(t3);
			mIb.quad(v+0, v+1, v+2, v+3);
		}
	}
	
	gGame->mRenderer->setState(state);
	gGame->mRenderer->drawTriangles(&mVb, &mIb);
}


static const char* partNames[] = {"torso",	
	"front_leg_upper", "front_leg_lower", "front_foot", 
	"back_leg_upper", "back_leg_lower", "back_foot", 
	"front_arm_upper", "front_arm_lower", 
	"back_arm_upper", "back_arm_lower", 
	"head", "cane"};

int getPart(const QiString& name)
{
	for(int i=0; i<Dude::PART_COUNT; i++)
		if (name == partNames[i])
			return i;
	return -1;
}

static const char* faceNames[] = {"idle", "blink", "afraid", "oops", "crash", "fly", "win", "loose"};

int getFace(const QiString& name)
{
	for(int i=0; i<Dude::FACE_COUNT; i++)
		if (name == faceNames[i])
			return i;
	return -1;
}

bool Dude::loadConfig(const QiString& path)
{
	QiMemoryStream<256> tmp;
	if (!mLevel->mResMan->load(path, tmp))
		return false;

	QiXmlParser xml(tmp, tmp.getSize());
	if (!xml.isValid() || xml.getName() != "character")
		return false;

	int pixMin[PART_COUNT][2];
	int pixMax[PART_COUNT][2];
	int pixPivot[PART_COUNT][2];
	int pixAttach[PART_COUNT][2];

	QiString tex = xml.getAttribute("texture");
	mTexture = mLevel->mResMan->acquireTexture(tex);
	mAppleTexture = mLevel->mResMan->acquireTexture("gfx/apple.png");

	int pixSizeX = mTexture.getTexture()->getWidth();
	int pixSizeY = mTexture.getTexture()->getHeight();

	float scaleX = xml.getAttribute("scale").toFloat();
	float scaleY = scaleX;
	scaleX *= pixSizeX / 512.0f;
	scaleY *= pixSizeY / 512.0f;

	for(int i=0; i<FACE_COUNT; i++)
		mFaces[i].mEnabled = false;

	xml.enter();
	while(xml.isValid())
	{
		if (xml.getName() == "parts")
		{
			xml.enter();
			while(xml.isValid())
			{
				if (xml.getName() == "part")
				{
					int id = getPart(xml.getAttribute("name"));
					if (id == -1)
					{
						gGame->logE("Unknown character part: " + xml.getAttribute("name"));
						return false;
					}
					int parentId = getPart(xml.getAttribute("parent"));
					QiString tmp = xml.getAttribute("min");
					pixMin[id][0] = tmp.getWord(0).toInt();
					pixMin[id][1] = tmp.getWord(1).toInt();
					tmp = xml.getAttribute("max");
					pixMax[id][0] = tmp.getWord(0).toInt();
					pixMax[id][1] = tmp.getWord(1).toInt();
					tmp = xml.getAttribute("pivot");
					pixPivot[id][0] = tmp.getWord(0).toInt();
					pixPivot[id][1] = tmp.getWord(1).toInt();
					tmp = xml.getAttribute("attach");
					pixAttach[id][0] = tmp.getWord(0).toInt();
					pixAttach[id][1] = tmp.getWord(1).toInt();
					tmp = xml.getAttribute("angles");
					mParts[id].mLowerAngle = tmp.getWord(0).toFloat();
					mParts[id].mUpperAngle = tmp.getWord(1).toFloat();

					if (parentId != -1)
						mParts[id].mParent = &mParts[parentId];
					else
						mParts[id].mParent = NULL;

					mParts[id].mAngle = 0.0f;
					mParts[id].mTargetAngle = 0.0f;
					mParts[id].mLastAngle = 0.0f;
				}
				xml.next();
			}
			xml.leave();
		}
		if (xml.getName() == "order")
		{
			xml.enter();
			while(xml.isValid())
			{
				if (xml.getName() == "part")
				{
					int id = getPart(xml.getAttribute("name"));
					mDrawOrder.add(id);
				}
				if (xml.getName() == "cloth")
				{
					QiString type = xml.getAttribute("name");
					for(int i=0; i<mCloths.getCount(); i++)
					{
						if (mCloths[i]->mType == type)
						{
							mDrawOrder.add(256 + i);
							break;
						}
					}
				}
				if (xml.getName() == "helmet")
				{
					QiString type = xml.getAttribute("name");
					if (type == "front")
						mDrawOrder.add(512);
					else
						mDrawOrder.add(513);
				}
				xml.next();
			}
			xml.leave();
		}
		if (xml.getName() == "cloth")
		{
			QiVec2 texMin, texMax;
			texMin.x = xml.getAttribute("min").getWord(0).toFloat();
			texMin.y = xml.getAttribute("min").getWord(1).toFloat();
			texMax.x = xml.getAttribute("max").getWord(0).toFloat();
			texMax.y = xml.getAttribute("max").getWord(1).toFloat();
			Cloth* cloth = QI_NEW Cloth(this, xml.getAttribute("name"), texMin, texMax);
			mCloths.add(cloth);
		}
		if (xml.getName() == "helmet")
		{
			QiVec2 texMin, texMax;
			texMin.x = xml.getAttribute("min").getWord(0).toFloat();
			texMin.y = xml.getAttribute("min").getWord(1).toFloat();
			texMax.x = xml.getAttribute("max").getWord(0).toFloat();
			texMax.y = xml.getAttribute("max").getWord(1).toFloat();
			if (xml.getAttribute("name") == "front")
			{
				mHelmetFrontTexMin.set(texMin.x/(float)(pixSizeX-1), 1.0f-texMax.y/(float)(pixSizeY-1));
				mHelmetFrontTexMax.set(texMax.x/(float)(pixSizeX-1), 1.0f-texMin.y/(float)(pixSizeY-1));
			}
			else
			{
				mHelmetBackTexMin.set(texMin.x/(float)(pixSizeX-1), 1.0f-texMax.y/(float)(pixSizeY-1));
				mHelmetBackTexMax.set(texMax.x/(float)(pixSizeX-1), 1.0f-texMin.y/(float)(pixSizeY-1));
			}
		}
		if (xml.getName() == "face")
		{
			int f=getFace(xml.getAttribute("name"));
			if (f != -1)
			{
				mFaces[f].mEnabled = true;
				QiVec2 texMin, texMax;
				texMin.x = xml.getAttribute("min").getWord(0).toFloat();
				texMin.y = xml.getAttribute("min").getWord(1).toFloat();
				texMax.x = xml.getAttribute("max").getWord(0).toFloat();
				texMax.y = xml.getAttribute("max").getWord(1).toFloat();
				mFaces[f].mTexLow.set(texMin.x/(float)(pixSizeX-1), 1.0f-texMax.y/(float)(pixSizeY-1));
				mFaces[f].mTexHigh.set(texMax.x/(float)(pixSizeX-1), 1.0f-texMin.y/(float)(pixSizeY-1));
			}
		}
		if (xml.getName() == "shadow")
		{
			QiVec2 texMin, texMax;
			texMin.x = xml.getAttribute("min").getWord(0).toFloat();
			texMin.y = xml.getAttribute("min").getWord(1).toFloat();
			texMax.x = xml.getAttribute("max").getWord(0).toFloat();
			texMax.y = xml.getAttribute("max").getWord(1).toFloat();
			mShadowTexMin.set(texMin.x/(float)(pixSizeX-1), 1.0f-texMax.y/(float)(pixSizeY-1));
			mShadowTexMax.set(texMax.x/(float)(pixSizeX-1), 1.0f-texMin.y/(float)(pixSizeY-1));
		}
		xml.next();
	}

	xml.leave();

	mFmt.clear();
	mFmt.addField("aPosition", QiVertexFormat::FLOAT32, 3);
	mFmt.addField("aTexCoord", QiVertexFormat::FLOAT32, 2);

	mVb.init(mFmt, PART_COUNT*4);

	mIb.init(PART_COUNT*6);

	for(int i=0; i<PART_COUNT; i++)
	{
		mParts[i].mAngle = 0.0f;
		mParts[i].mTexLow.set(pixMin[i][0]/(float)(pixSizeX-1), 1.0f-pixMax[i][1]/(float)(pixSizeY-1));
		mParts[i].mTexHigh.set(pixMax[i][0]/(float)(pixSizeX-1), 1.0f-pixMin[i][1]/(float)(pixSizeY-1));
		mParts[i].mSize = multiplyPerElement(mParts[i].mTexHigh - mParts[i].mTexLow, QiVec2(scaleX, scaleY)) * 0.5f;
		QiVec2 mid = (mParts[i].mTexHigh + mParts[i].mTexLow)*0.5f;
		mParts[i].mPivot = (QiVec2(pixPivot[i][0]/(float)(pixSizeX-1), 1.0f-pixPivot[i][1]/(float)(pixSizeY-1)) - mid);
		mParts[i].mPivot.x *= scaleX;
		mParts[i].mPivot.y *= scaleY;
		if (i > 0)
		{
			QiVec2 parentMid = (mParts[i].mParent->mTexHigh + mParts[i].mParent->mTexLow)*0.5f;
			mParts[i].mAttachment = (QiVec2(pixAttach[i][0]/(float)(pixSizeX-1), 1.0f-pixAttach[i][1]/(float)(pixSizeY-1)) - parentMid);
			mParts[i].mAttachment.x *= scaleX;
			mParts[i].mAttachment.y *= scaleY;
		}
	}
	return true;
}

void Dude::transformParts()
{
	if (mRagDoll)
	{
		for(int i=0; i<PART_COUNT; i++)
		{
			mParts[i].mTransform.pos = vec(mParts[i].mPhysBody->GetPosition());
			mParts[i].mTransform.rot = mParts[i].mPhysBody->GetAngle();
		}
		mTransform = mParts[TORSO].mTransform;
		mTorso->mTransform = mParts[TORSO].mTransform;
		return;
	}

	//Interpolate angles
	for(int i=0; i<PART_COUNT; i++)
	{
		float d = QiAngle(mParts[i].mAngle, mParts[i].mTargetAngle) * 0.3f * mAnimSpeed * mLevel->mTimeScale;
		d = QiClamp(d, -0.2f*mAnimSpeed, 0.2f*mAnimSpeed);
		mParts[i].mLastAngle = mParts[i].mAngle;
		mParts[i].mAngle -= d;
	}

	for(int i=TORSO; i<PART_COUNT; i++)
	{
		Part* parent = mParts[i].mParent;
		if (parent)
		{
			mParts[i].mTransform.pos = parent->mTransform.toParentPoint(mParts[i].mAttachment);
			mParts[i].mTransform.rot = parent->mTransform.rot + mParts[i].mAngle;
			mParts[i].mTransform.pos -= mParts[i].mTransform.toParentVec(mParts[i].mPivot);
		}
		else
		{
			mParts[i].mTransform.pos = mParts[i].mTransform.pos;
			mParts[i].mTransform.rot = mParts[i].mAngle;
		}
	}
}


void Dude::updatePose()
{
	if (mRagDoll)
		return;

	float armSeparation = QiCos(mLegCirculate) * 0.3f;

	float l = (mTargetHeight-0.6f)/0.6f;
	float diff = length(mGroundPoint-mTorso->mTransform.pos);
	if (!mGroundHit)
		diff = 0.0f;

	mAnimSpeed = 1.0f;
	if (mReviveTimer < 0.5f)
		mAnimSpeed = 0.5f;

	bool cane = (mGrabJoint || mGrab);
	{
		if (cane)
		{
			mParts[FRONT_ARM_UPPER].mTargetAngle = 2.5f;
			mParts[FRONT_ARM_LOWER].mTargetAngle = 0.1f;
			mParts[BACK_ARM_UPPER].mTargetAngle = 2.7f;
			mParts[BACK_ARM_LOWER].mTargetAngle = 0.1f;
			mParts[CANE].mTargetAngle = QI_PI;
			mParts[CANE].mPivot.set(0.0f, -0.38f);
		}
		else
		{
			mParts[FRONT_ARM_UPPER].mTargetAngle = (mTargetHeight+diff*0.1f)*4.5f - 2.0f + armSeparation;
			mParts[FRONT_ARM_LOWER].mTargetAngle = 1.0f-l;
			mParts[FRONT_ARM_UPPER].mTargetAngle += mArmRot;

			mParts[BACK_ARM_UPPER].mTargetAngle = (mTargetHeight+diff*0.1f)*4.5f - 2.0f + QiClamp(QiAbs(mArmAcc*30.0f), 0.0f, QI_PI) - armSeparation;
			mParts[BACK_ARM_LOWER].mTargetAngle = 1.0f-l;
			mParts[BACK_ARM_UPPER].mTargetAngle += mArmRot;

			mParts[CANE].mTargetAngle = -3.0f+mParts[FRONT_ARM_UPPER].mTargetAngle;
			mParts[CANE].mPivot.set(0.0f,0.38f);
		}
	}
	//mParts[TORSO].mTransform.pos = mTransform.toParentPoint(mParts[TORSO].mPivot);
	//mParts[TORSO].mTransform.rot = mTransform.rot;
	//mParts[TORSO].mTransform.rot -= 0.3f*(1.0f-l);

	mParts[TORSO].mTransform.pos = mTransform.toParentPoint(mParts[TORSO].mPivot);
	mParts[TORSO].mTargetAngle = mTransform.rot - 0.3f*(1.0f-l); 

	if (mReviveTimer > 0.5f)
		mParts[TORSO].mAngle = mParts[TORSO].mTargetAngle;

	float legSeparation = QiCos(mLegCirculate) * 0.3f;

	if (mGroundHit)
	{
		QiVec2 p, n;
		Body* b = NULL;
		for(int i=0; i<2; i++)
		{
			int legOffset = i*3;
			float legAngle = legSeparation - i*legSeparation*2;
			float h = mTargetHeight + 0.5f;
			QiVec2 p = mTorso->mTransform.toParentPoint(mParts[FRONT_LEG_UPPER+legOffset].mAttachment);
			if (mLevel->raycast(p, p + QiVec2(QiCos(mTorso->mTransform.rot-QI_PI*0.5f+legAngle)*h, QiSin(mTorso->mTransform.rot-QI_PI*0.5f+legAngle)*h), 252, mPlayerId, &p, &n, &b))
			{
				float dist = length(p - mParts[TORSO].mTransform.toParentPoint(mParts[FRONT_LEG_UPPER+legOffset].mAttachment)) - 0.25f;
				float upperLegLength = length(mParts[FRONT_LEG_UPPER+legOffset].mPivot-mParts[FRONT_LEG_LOWER+legOffset].mAttachment);
				float lowerLegLength = length(mParts[FRONT_LEG_LOWER+legOffset].mPivot-mParts[FRONT_FOOT+legOffset].mAttachment);
				float legLength = upperLegLength + lowerLegLength;
				float frac = QiClamp(dist/legLength, 0.0f, 1.0f);
				float v = QiACos(frac);
				mParts[FRONT_LEG_UPPER+legOffset].mTargetAngle = -legAngle + v;
				mParts[FRONT_LEG_LOWER+legOffset].mTargetAngle = -v*2;
				mParts[FRONT_FOOT+legOffset].mTargetAngle = v + QiATan2(mGroundNormal.y, mGroundNormal.x)-QI_PI*0.5f -mParts[TORSO].mTransform.rot + legAngle ;
			}
			bool push = (l < 0.1f && mGroundTime > 0.1f && mSpeed < 9.0f && !mGrabJoint && !mGrab && mSpeed > 1.0f);
			if (push)
			{
				mThrustAng += (QiAbs(mSpeed)*0.5f+4.0f)*gGame->mTimeStep;
				if (mThrustAng > QI_TWO_PI - 1.1f)
				{
					mThrustAng -= QI_TWO_PI;
					gGame->mAudio->playSound(gGame->mAudio->mCaneSpeed.next(), mTransform.pos);
				}
				mParts[FRONT_ARM_UPPER].mTargetAngle = 0.8f + QiSin(mThrustAng) * 0.8f;
				mParts[FRONT_ARM_LOWER].mTargetAngle = 1.0f-QiSin(mThrustAng-QI_HALF_PI) * 0.4f;
				mParts[CANE].mTargetAngle = -2.0f + QiSin(mThrustAng-QI_HALF_PI) * 0.5f;
			}
			else
				mThrustAng = 0.0f;
		}

		mArmAcc = 0.0f;
		mArmRot -= QiClamp(QiAngle(mArmRot, 0.0f)*0.1f, -0.2f, 0.2f);
	}
	else
	{
		if (mAirTime > 0.5f && l > 0.5f)
			mArmAcc -= 0.003f;
		else
		{
			mArmAcc = 0.0f;
			mArmRot -= QiClamp(QiAngle(mArmRot, 0.0f)*0.1f, -0.2f, 0.2f);
		}
		if (l > 0.5f)
		{
			mLegCirculate += 0.02f + QiClamp(mAirTime*0.1f, 0.0f, 0.1f);
			if (!mGrabJoint && !mGrab)
				mParts[CANE].mTargetAngle = 0.0f;
		}
		if (mAirTime > 0.1)
		{
			if (!mCrouch && !mGrab)
			{
				mAnimSpeed = 0.3f;
				mArmAcc = 0.0f;
				mParts[FRONT_ARM_UPPER].mTargetAngle = -1.2f + QiSin(mArmRot)*0.5f;
				mParts[FRONT_ARM_LOWER].mTargetAngle = -0.3f;
				mParts[BACK_ARM_UPPER].mTargetAngle = -1.1f - QiSin(mArmRot)*0.5f;
				mParts[BACK_ARM_LOWER].mTargetAngle = -0.2f;
				mParts[HEAD].mTargetAngle = 0.5f;

				mParts[FRONT_LEG_UPPER].mTargetAngle = 0.5f;
				mParts[FRONT_LEG_LOWER].mTargetAngle = 0.0f;
				mParts[FRONT_FOOT].mTargetAngle = -1.0f;
				mParts[BACK_LEG_UPPER].mTargetAngle = 0.6f;
				mParts[BACK_LEG_LOWER].mTargetAngle = 0.1f;
				mParts[BACK_FOOT].mTargetAngle = -1.0f;
			}
			else
			{
				mParts[FRONT_LEG_UPPER].mTargetAngle = QiSin(mLegRot)*0.9f + 0.5f;
				mParts[FRONT_LEG_LOWER].mTargetAngle = QiSin(2.0f + mLegRot)*+1.2f - 1.2f;
				mParts[FRONT_FOOT].mTargetAngle = 0.0f;
				mParts[BACK_LEG_UPPER].mTargetAngle = QiSin(QI_PI + mLegRot)*0.9f + 0.5f;
				mParts[BACK_LEG_LOWER].mTargetAngle = QiSin(QI_PI + 2.0f + mLegRot)*+1.2f - 1.2f;
				mParts[BACK_FOOT].mTargetAngle = 0.0f;
			}
		}
	}

	if (mSlowDownTimer > 0.0f)
	{
		mParts[FRONT_LEG_UPPER].mTargetAngle = 0.6f;
		mParts[FRONT_LEG_LOWER].mTargetAngle = 0.0f;
		mParts[BACK_LEG_UPPER].mTargetAngle = -0.6f;
		mParts[BACK_LEG_LOWER].mTargetAngle = 0.0f;
		mParts[FRONT_ARM_UPPER].mTargetAngle = 2.6f;
		mParts[FRONT_ARM_LOWER].mTargetAngle = 0.0f;
		mParts[BACK_ARM_UPPER].mTargetAngle = -2.6f;
		mParts[BACK_ARM_LOWER].mTargetAngle = 0.0f;
	}

	mArmRot += QiClamp(mArmAcc, -0.15f, 0.15f) * mLevel->mTimeScale;
	mLegRot += QiClamp(mArmAcc, -0.15f, 0.15f) * mLevel->mTimeScale;

	mParts[HEAD].mTargetAngle = -0.3f*(1.0f-l);

	if (!mGrabJoint)
	{
		mParts[FRONT_ARM_UPPER].mTargetAngle += QiSin(mLevel->mSimTime*1.5f)*0.1f;
		mParts[FRONT_ARM_LOWER].mTargetAngle += QiSin(mLevel->mSimTime*1.8f)*0.1f;
		mParts[BACK_ARM_UPPER].mTargetAngle += QiSin(mLevel->mSimTime*2.23f)*0.1f;
		mParts[BACK_ARM_LOWER].mTargetAngle += QiSin(mLevel->mSimTime*2.23f)*0.1f;
		mParts[CANE].mTargetAngle += QiSin(mLevel->mSimTime*1.93f)*0.1f;
	}
	mParts[HEAD].mTargetAngle += QiSin(mLevel->mSimTime*3.23f)*0.1f;
	
	if (mThrowTimer != 0.0f)
	{
		if (mThrowTimer > 0.0f)
		{
			//Forward throw
			if (mThrowTimer >= 0.25f)
			{
				mParts[BACK_ARM_UPPER].mAngle = mParts[BACK_ARM_UPPER].mTargetAngle = QI_PI*0.8f;
				mParts[BACK_ARM_LOWER].mAngle = mParts[BACK_ARM_UPPER].mTargetAngle = QI_PI*0.5f;
			}
			else
			{
				mParts[BACK_ARM_UPPER].mTargetAngle = QI_PI*0.6f;
				mParts[BACK_ARM_LOWER].mTargetAngle = 0.0f;
			}
			mThrowTimer = QiMax(0.0f, mThrowTimer - gGame->mTimeStep);
		}
		else
		{
			//Backward throw
			if (mThrowTimer <= -0.25f)
			{
				mParts[BACK_ARM_UPPER].mAngle = mParts[BACK_ARM_UPPER].mTargetAngle = QI_PI*0.6f;
				mParts[BACK_ARM_LOWER].mAngle = mParts[BACK_ARM_UPPER].mTargetAngle = QI_PI*0.0f;
			}
			else
			{
				mParts[BACK_ARM_UPPER].mTargetAngle = QI_PI*1.1f;
				mParts[BACK_ARM_LOWER].mTargetAngle = QI_PI*0.4f;
			}
			mThrowTimer = QiMin(0.0f, mThrowTimer + gGame->mTimeStep);
		}
	}
}


void Dude::makeRagDoll()
{
	for(int i=0; i<PART_COUNT; i++)
	{
		b2BodyDef bd;
		bd.position = (b2Vec2&)mParts[i].mTransform.pos;
		bd.angle = mParts[i].mTransform.rot;
		bd.type = b2_dynamicBody;
		bd.angularDamping = 0.0f;
		bd.linearVelocity = vec(mTorso->getVelocity());
		mParts[i].mPhysBody = mLevel->getPhysWorld()->CreateBody(&bd);

		b2PolygonShape s;
		b2Vec2 pv[4];
		pv[0].Set(-mParts[i].mSize.x*0.7f, -mParts[i].mSize.y*0.7f);
		pv[1].Set(mParts[i].mSize.x*0.7f, -mParts[i].mSize.y*0.7f);
		pv[2].Set(mParts[i].mSize.x*0.7f, mParts[i].mSize.y*0.7f);
		pv[3].Set(-mParts[i].mSize.x*0.7f, mParts[i].mSize.y*0.7f);
		s.Set(&pv[0], 4);

		b2FixtureDef fd;
		fd.filter.categoryBits = mPlayerId;
		fd.filter.maskBits = 252;
		fd.shape = &s;
		fd.density = 1.0f;
		fd.friction = 0.7f;
		fd.restitution = 0.0f;
 		mParts[i].mPhysBody->CreateFixture(&fd);

		if (mParts[i].mParent && i != CANE)
		{
			b2RevoluteJointDef j;
			j.Initialize(mParts[i].mParent->mPhysBody, mParts[i].mPhysBody, vec(mParts[i].mTransform.toParentPoint(mParts[i].mPivot)));
			j.referenceAngle = 0.0f;
			j.enableLimit = true;
			j.lowerAngle = mParts[i].mLowerAngle;
			j.upperAngle = mParts[i].mUpperAngle;
			j.enableMotor = true;
			j.motorSpeed = 0.0f;
			j.maxMotorTorque = 0.02f;
			mParts[i].mPhysJoint = (b2RevoluteJoint*)mLevel->mPhysWorld->CreateJoint(&j);	
		}
	}
	mRagDoll = true;
}


void Dude::unRagDoll()
{
	for(int i=0; i<PART_COUNT; i++)
		if (mParts[i].mParent)
			mParts[i].mAngle = mParts[i].mPhysBody->GetAngle() - mParts[i].mParent->mPhysBody->GetAngle();
		else
			mParts[i].mAngle = mParts[i].mPhysBody->GetAngle();
	mAnimSpeed = 0.1f;

	mTorso->mPhysBody->SetTransform(mParts[TORSO].mPhysBody->GetPosition(), mParts[TORSO].mPhysBody->GetAngle());
	mTorso->mPhysBody->SetLinearVelocity(b2Vec2(0.0f, 0.0f));
	mTorso->mPhysBody->SetAngularVelocity(0.0f);

	for(int i=0; i<PART_COUNT; i++)
		mLevel->mPhysWorld->DestroyBody(mParts[i].mPhysBody);
	mRagDoll = false;
}


void Dude::onReset()
{
	Entity::loadProperties();

	mTorso->mPhysBody->SetLinearVelocity(b2Vec2(0.0f, 0.0f));
	mTorso->mPhysBody->SetAngularVelocity(0.0f);

	if (mGrabJoint)
		release();

	if (mJoint)
		mLevel->mPhysWorld->DestroyJoint(mJoint);
	mJoint = NULL;

	mJump = false;
	mCrouch = false;
	mGrab = false;

	mRotAngle = 0.0f;

	mKeepAlive = (mPlayerId == 2);
	mDead = false;
	if (mRagDoll)
		unRagDoll();

	mTrackBody = NULL;
	mTrackCurrent = 0;
	for(int i=0; i < mTracksVerts0.getCount(); i++)
	{
		mTracksVerts0[i].set(0,0);
		mTracksVerts1[i].set(0,0);
	}

	mArmAcc = 0.0f;

	mFrame=0;
	mReplayHints = 0;
	mDeadTimer = 0.0f;
	mJumpFrame = 0;
	mLastYSpeed = 0.0f;

	mHelmetTimer = 0.0f;
	
	mZ = (mPlayerId==2 ? -0.01f : 0.0f);
}


void Dude::grab()
{
	QiVec2 pos;
	QiVec2 grabPoint = GRAB_POINT;
	QiVec2 wp = mTransform.toParentPoint(grabPoint);
	Handle* handle = mLevel->findHandle(wp, mPlayerId);
	if (!mGrabJoint && handle)
	{
		b2RevoluteJointDef j;
		j.bodyA = mTorso->mPhysBody;
		j.bodyB = handle->mBody ? handle->mBody->mPhysBody : mLevel->mPhysGround;
		if (!j.bodyB) 
			j.bodyB = mLevel->mPhysGround;
		if (handle->mProperties.getBool("followRotation"))
		{
			j.localAnchorA = vec(QiVec2(0,0));
			j.localAnchorB = vec(handle->mPos - grabPoint);
		}
		else
		{
			j.localAnchorA = vec(grabPoint);
			j.localAnchorB = vec(handle->mPos);
		}
		mGrabJoint = (b2RevoluteJoint*)mLevel->mPhysWorld->CreateJoint(&j);	
		mGrabJoint->m_correctionMultiplier = 0.05f;

		if (mPlayerId == 1)
			gGame->mAudio->playSound(gGame->mAudio->mCaneHit.next(), mTransform.pos);
		mGrabbedHandle = handle;
		mReplayHints |= HINT_HANDLE;
	}
}


void Dude::release()
{
	if (mGrabJoint)
	{
		mReplayHints |= HINT_RELEASE;
		mLevel->mPhysWorld->DestroyJoint(mGrabJoint);
		mGrabJoint = NULL;
		mRotVel = mTorso->mPhysBody->GetAngularVelocity();

		if (mPlayerId == 1)
			gGame->mAudio->playSound(gGame->mAudio->mCaneHit.next(), mTransform.pos, 1.0f, 1.2f);
		mGrabbedHandle = NULL;
	}
}


void Dude::doWire()
{
	if (!mGrabbedWire && mRailChannel && mRailChannel->isPlaying() && mRailChannel->getBuffer() == mWireSound.getSound())
	{
		mReplayHints |= HINT_RELEASE;
		if (mPlayerId == 1)
			gGame->mAudio->playSound(mWireOffSound.getSound(), mTransform.pos);
		mRailChannel->stop();
	}

	if (!mGrab || mGrabJoint)
	{
		mGrabbedWire = NULL;
		return;
	}

	QiVec2 pos = mTransform.toParentPoint(GRAB_POINT);

	QiVec2 normal;
	float dist;

	QiVec2 n = mTransform.toParentVec(QiVec2(0, 1.0f));
	Wire* w = mGrabbedWire;
	float t = 0.0f;
	if (w)
	{
		QiVec2 point;
		dist = 10.0f;
		if (!w->intersects(pos, vec(mJoint->m_up), point, normal, dist, &t))
			w = NULL;
	}
	else
	{
		dist = 1.0f;
		QiVec2 point;
		w = mLevel->findWire(pos, n, point, normal, dist, &t);
	}
	if (!w)
	{
		mGrabbedWire = NULL;
		return;
	}

	if (mRailChannel && !mRailChannel->isPlaying())
	{
		mRailChannel->setBuffer(mWireSound.getSound());
		mRailChannel->play();
		if (mPlayerId == 1)
			gGame->mAudio->playSound(mWireOnSound.getSound(), mTransform.pos);
	}

	mReplayHints |= HINT_WIRE;

	gGame->mAudio->setSoundChannelVolume(mRailChannel, mTransform.pos, 1.0f);
	if (t > 0.5f)
		t = 1.0f-t;
	t = (1.0f-QiAbs(t)/0.5f);
	float strength = 0.03f*dist*dist + 0.2f*t*t*dist;

	mJoint->m_point = vec(pos); //vec(pos*0.4f + mTransform.pos*0.6f); // Attach joint somewhere in between hand and torso to avoid dramatic rotations
	mJoint->m_tan = -vec(perpendicular(normal));
	mJoint->m_tanTarget = 0.0f;
	mJoint->m_tanMin = 0.0f;
	mJoint->m_tanMax = 0.0f;
	mJoint->m_up = vec(normal);
	mJoint->m_upMin = 0.0f;
	mJoint->m_upMax = QiMax(strength, 0.0f);
	mJoint->m_upTarget = QiClamp(dist*5.0f, -5.0f, 5.0f);

	mGrabbedWire = w;
	w->attach(pos);
	mJumpFrame = 0;
}


void Dude::doRail()
{
	if (!mRailBody && mRailChannel && mRailChannel->isPlaying() && mRailChannel->getBuffer() == mRailSound.getSound())
	{
		mReplayHints |= HINT_RELEASE;
		mRailChannel->stop();
		if (mPlayerId == 1)
			gGame->mAudio->playSound(mRailEndSound.getSound(), mTransform.pos);
	}

	if (!mGrab || mGrabJoint || mGrabbedWire)
	{
		mRailBody = NULL;
		return;
	}

	QiVec2 pos = mTransform.toParentPoint(GRAB_POINT);
	QiVec2 n = mTransform.toParentVec(QiVec2(0, 1.0f));

	QiVec2 normal;
	float dist;
	QiVec2 point;

	float l = 0.5f + length(mTorso->getVelocity())*gGame->mTimeStep*1.5f;
	Body* body = NULL;
	if (!body && mRailBody)
		body = mLevel->findRail(pos, mRailNormal, point, normal, dist);
	if (!body)
	{
		body = mLevel->findRail(pos, QiVec2(0, l), point, normal, dist);
		if (body && dot(body->mTransform.toParentVec(QiVec2(0,1)), normal) < 0.1f)
			body = NULL;
	}
	if (!body)
	{
		body = mLevel->findRail(pos, QiVec2(0, -l), point, normal, dist);
		if (body && dot(body->mTransform.toParentVec(QiVec2(0,1)), normal) < 0.1f)
			body = NULL;
	}
	if (!body)
	{
		body = mLevel->findRail(pos, QiVec2(l, 0), point, normal, dist);
		if (body && dot(body->mTransform.toParentVec(QiVec2(0,1)), normal) < 0.1f)
			body = NULL;
	}
	if (!body)
	{
		body = mLevel->findRail(pos, QiVec2(-l, 0), point, normal, dist);
		if (body && dot(body->mTransform.toParentVec(QiVec2(0,1)), normal) < 0.1f)
			body = NULL;
	}	

	if (!body)
	{
		if (mRailBody)
			mTorso->mPhysBody->ResetMassData();
		mRailBody = NULL;
		return;
	}
	
	if (mRailChannel && !mRailChannel->isPlaying())
	{
		mRailChannel->setBuffer(mRailSound.getSound());
		mRailChannel->play();
		if (mPlayerId == 1)
			gGame->mAudio->playSound(gGame->mAudio->mCaneHit.next(), mTransform.pos);
	}

	mReplayHints |= HINT_RAIL;

	gGame->mAudio->setSoundChannelVolume(mRailChannel, mTransform.pos, 1.0f);

	mJoint->m_point = vec(pos); //*0.4f + mTransform.pos*0.6f); // Attach joint somewhere in between hand and torso to avoid dramatic rotations
	mJoint->m_tan = -vec(perpendicular(normal));
	mJoint->m_tanTarget = 0.0f;
	mJoint->m_tanMin = 0.0f;
	mJoint->m_tanMax = 0.0f;
	mJoint->m_up = vec(normal);
	mJoint->m_upMin = 0.0f;
	mJoint->m_upMax = 10.0f;
	mJoint->m_upTarget = QiClamp(dist*30.0f, -5.0f, 5.0f);
	mRailBody = body;
	mJumpFrame = 0;
	mRailNormal = normal;
	
	if (QiRnd(0.0f, 1.0f) < 0.2f && mInputFrame)
	{
		QiVec2 v = getVelocity();
		mSparkParticles.spawn(pos.vec3(), (v*0.7f + normal*2.0f + QiVec2::random(3.0f)).vec3());	
	}
}


void Dude::slowDown()
{
	if (mSlowDownTimer <= 0.0f)
		gGame->mAudio->playSound(mCaneHitBadGuySound.getSound(), mTransform.pos);
	mSlowDownTimer = 0.5f;
}


float Dude::getSpeedMultiplier()
{
	float extraSlowDown = QiClamp((0.5f - mSlowDownTimer)*2.0f, 0.2f, 1.0f);
	return mSpeedMultiplier * extraSlowDown;
}


void Dude::playSound(QiAudioBuffer* buffer)
{
	if (mPlayerId == 1 && buffer && mVoiceChannel)
	{
		mVoiceChannel->stop();
		mVoiceChannel->setBuffer(buffer);
		gGame->mAudio->setSoundChannelVolume(mVoiceChannel, mTransform.pos, 1.0f);
		mVoiceChannel->play();
	}
}


