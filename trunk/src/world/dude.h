/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "propertybag.h"
#include "entity.h"
#include "resman.h"
#include "audio.h"
#include "particlesystem.h"

enum ReplayHints
{
	HINT_JUMP =		0x0001,
	HINT_LAND =		0x0002,
	HINT_BREAK =	0x0004,
	HINT_HANDLE =	0x0008,
	HINT_WIRE =		0x0010,
	HINT_RAIL =		0x0020,
	HINT_RELEASE =	0x0040,
	HINT_HIT =		0x0080,
	HINT_WIN =		0x0100,
	HINT_APPLE =	0x0200,
	HINT_LAND_GOOD=	0x0400,
	HINT_DIE =		0x0800
};

class Dude : public Entity
{
public:
	Dude(class Level* level, int playerId);
	~Dude();
	
	void processControls(bool disableInput);
	void update();
	virtual void renderTransparent();
	virtual void setTransform(const QiTransform2& t);
	virtual bool contains(const QiVec2& point) const;
	virtual void onStart();
	virtual void onReset();

	void addTrack();

	void grab();
	void release();

	void doWire();
	void doRail();

	void slowDown();
	float getSpeedMultiplier();

	void die();
	void playSound(QiAudioBuffer* buffer);

	QiVec2 getVelocity() const;
	
	class Body* mTorso;
	class b2MotionJoint* mJoint;
	class b2RevoluteJoint* mGrabJoint;
	class Handle* mGrabbedHandle;
	float mGrabTimer;
	bool mGrab;
	int mPlayerId;

	float mTargetHeight;
	QiVec2 mUp;
	float mBreakPower;
    bool mBoost;
    int mGestureTouch;
    QiVec2 mGesturePivot;
	float mAngle;

	Body* mContactBody;

	float mRotVel;
	float mRotAngle;
	bool mDead;
	float mSpeed;
	bool mCrouch;
	bool mJump;
	int mJumpFrame;
	int mAbortJumpFrame;
	float mJumpAngle;

	QiInputStream* mInput;
	QiOutputStream* mOutput;

	bool mGroundHit;
	QiVec2 mGroundPoint;
	QiVec2 mGroundNormal;
	Body* mGroundBody;
	float mGroundTime;

	QiVec2 mOldNormal[5];

	Resource mTexture;
	Resource mAppleTexture;

	enum EPart
	{
		TORSO,
		FRONT_LEG_UPPER,
		FRONT_LEG_LOWER,
		FRONT_FOOT,
		BACK_LEG_UPPER,
		BACK_LEG_LOWER,
		BACK_FOOT,
		FRONT_ARM_UPPER,
		FRONT_ARM_LOWER,
		BACK_ARM_UPPER,
		BACK_ARM_LOWER,
		HEAD,
		CANE,
		PART_COUNT
	};

	enum EFace
	{
		FACE_IDLE,
		FACE_BLINK,
		FACE_AFRAID,
		FACE_OOPS,
		FACE_CRASH,
		FACE_FLY,
		FACE_WIN,
		FACE_LOOSE,
		FACE_COUNT
	};

	struct Part
	{
		float mTargetAngle;
		float mAngle;
		float mLastAngle;
		Part* mParent;
		QiTransform2 mTransform;
		QiVec2 mPivot;
		QiVec2 mAttachment;
		QiVec2 mSize;
		QiVec2 mTexLow;
		QiVec2 mTexHigh;
		float mLowerAngle;
		float mUpperAngle;
		class b2Body* mPhysBody;
		class b2RevoluteJoint* mPhysJoint;
	};

	struct Face
	{
		bool mEnabled;
		QiVec2 mTexLow;
		QiVec2 mTexHigh;
	};

	QiArrayInplace<int, 20> mDrawOrder;
	Face mFaces[FACE_COUNT];

	EFace mCurrentFace;

	QiVec2 mHelmetFrontTexMin;
	QiVec2 mHelmetFrontTexMax;
	QiVec2 mHelmetBackTexMin;
	QiVec2 mHelmetBackTexMax;
	QiVec2 mShadowTexMin;
	QiVec2 mShadowTexMax;
	
	QiVertexFormat mFmt;
	QiVertexBuffer mVb;
	QiIndexBuffer mIb;
	Part mParts[PART_COUNT];

	bool loadConfig(const QiString& path);
	void updatePose();
	void transformParts();
	void makeRagDoll();
	void unRagDoll();

	float mAirTime;
	float mArmRot;
	float mLegRot;
	float mArmAcc;
	float mLegCirculate;
	float mAnimSpeed;
	float mThrustAng;
	float mThrowTimer;
	bool mRagDoll;
	
	QiArray<class Cloth*> mCloths;
	class ContactCallback* mContactCallback;

	ParticleSystem mDustParticles;
	ParticleSystem mPebbleParticles;
	ParticleSystem mGrassParticles;
	ParticleSystem mWaterParticles;
	ParticleSystem mMudParticles;
	ParticleSystem mOilParticles;
	ParticleSystem mSparkParticles;

	static const int TRACKS_COUNT = 64;
	QiArrayInplace<QiVec2, TRACKS_COUNT*4> mTracksVerts0;
	QiArrayInplace<QiVec2, TRACKS_COUNT*4> mTracksVerts1;
	int mTrackCurrent;
	QiVertexFormat mTracksFmt;
	QiVertexBuffer mTracksVb;
	QiIndexBuffer mTracksIb;
	Body* mTrackBody;

	class QiAudioChannel* mRollingChannel;
	class QiAudioChannel* mRailChannel;
	Resource mCaneHitBadGuySound;

	class Wire* mGrabbedWire;
	class Body* mRailBody;
	QiVec2 mRailNormal;
	bool mKeepAlive;

	Resource mSkatesGeneric;
	Resource mSkatesGrass;
	Resource mSkatesMud;
	Resource mSkatesMetal;
	Resource mSkatesWood;
	Resource mSkatesRock;

	Resource mRailSound;
	Resource mRailEndSound;
	Resource mWireSound;
	Resource mWireOnSound;
	Resource mWireOffSound;
	SoundBank mSndGrunt;
	SoundBank mSndScream;
	SoundBank mSndJump;
	SoundBank mSndLand;
	SoundBank mSndOh;
	SoundBank mSndUhOh;
	SoundBank mSndBreak;
	SoundBank mSndYay;
	SoundBank mSndLaugh;
	SoundBank mSndMurmur;

	float mSensorSpeed;
	float mSensorSpeedAir;
	float mSpeedMultiplier;
	float mFrame;

	bool mInputFrame;
	QiUInt16 mReplayHints;
	float mSlowDownTimer;

	float mDeadTimer;
	float mReviveTimer;
	float mJumpOrgVel;
	float mLastYSpeed;

	float mWindVolume;
	float mFacialExpressionTimer;
	QiVec2 mTimingAidPoint;
	QiVec2 mTimingAidDirection;

	bool mSpawnHelmet;
	float mHelmetTimer;
	
	QiAudioChannel* mVoiceChannel;

	Resource mHelmetOnSnd;
	Resource mHelmetOffSnd;
	Resource mSplashSnd;
	Resource mCoinsDropSnd;
	
	QiString mName;
	bool mNoCorrection;
	float mStartDelay;
};

