/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "entity.h"
#include "game.h"
#include "display.h"
#include "level.h"

const char* gEntityTypeNames[] =
{
	"dude",
	"body",
	"joint",
	"sensor",
	"decal",
	"handle",
	"wire",
	"breakable",
	"powerup",
	"note",
	"throwable",
	"INVALID"
};


Entity::Entity(Level* level, Type type) : 
mLevel(level), mType(type), mParent(NULL), mVisible(true), mZ(0.0f), mEditorHidden(false), mEditorGroup(0)
{
	mProperties.add("name", "");
	mProperties.add("template", "");
	mProperties.add("pos", "0 0");
	mProperties.add("rot", "0");
}


Entity::~Entity()
{
}


void Entity::setTransform(const QiTransform2& t)
{
	mTransform = t;
	if (gGame->getState() == Game::EDIT)
	{
		mProperties.setVec2("pos", mTransform.pos);
		mProperties.setFloat("rot", mTransform.rot);
	}
}


void Entity::loadProperties()
{
	QiVec2 p = mProperties.getVec2("pos");
	mBoundsMin = p;
	mBoundsMax = p;
	setTransform(QiTransform2(p, mProperties.getFloat("rot")));
	mLevel->applyTemplate(mProperties.getString("template"), &mProperties);
}


void Entity::onReset()
{
	loadProperties();
}


bool Entity::contains(const QiVec2& point) const
{
	if (gGame->getState() == Game::EDIT)
	{
		float r = 0.1f/gGame->mDisplay->mEditZoom;
		return lengthSquared(mTransform.pos-point) < r*r;
	}
		return false;
}


void Entity::mergeBounds(Entity* other)
{
	mBoundsMin.x = QiMin(mBoundsMin.x, other->mBoundsMin.x);
	mBoundsMin.y = QiMin(mBoundsMin.y, other->mBoundsMin.y);
	mBoundsMax.x = QiMax(mBoundsMax.x, other->mBoundsMax.x);
	mBoundsMax.y = QiMax(mBoundsMax.y, other->mBoundsMax.y);
}

void Entity::invalidateBounds()
{
	mBoundsMin.set(QI_FLOAT_MAX, QI_FLOAT_MAX);
	mBoundsMax.set(-QI_FLOAT_MAX, -QI_FLOAT_MAX);
}

