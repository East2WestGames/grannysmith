/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "propertybag.h"
#include "entity.h"

class Joint : public Entity
{
public:
	Joint(class Level* level);
	virtual ~Joint();

	virtual void update();

	virtual void onStart();
	virtual void onStop();
	virtual void onReset();

	void breakJoint();

	class Body* mBodyA;
	class Body* mBodyB;
	class b2Joint* mPhysJoint;

	bool mEnabled;
};

