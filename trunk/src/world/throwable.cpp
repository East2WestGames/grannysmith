/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "throwable.h"
#include "level.h"
#include "game.h"
#include "gfx.h"
#include "dude.h"
#include "audio.h"
#include "player.h"
#include "Box2D.h"
#include "breakable.h"
#include "display.h"

class Object
{
public:
	b2Body* mBody;
	QiTransform2 mTransform;
	int mType;
	bool mEnabled;
	int mFrame;
};



Throwable::Throwable(Level* level) : Entity(level, THROWABLE)
{
	mTexture = mLevel->mResMan->acquireTexture("gfx/baseball.png");
	mTextureBanana = mLevel->mResMan->acquireTexture("gfx/banana.png");
	if (gGame->mPlayer->getCharacter() == "ouie")
		mTextureHelmet = mLevel->mResMan->acquireTexture("gfx/helmet_ouie.png");
	else
		mTextureHelmet = mLevel->mResMan->acquireTexture("gfx/helmet.png");
	mTextureCoin = mLevel->mResMan->acquireTexture("gfx/coin.png");
	mSound.load(mLevel->mResMan, "snd/character/throw");
}


void Throwable::onReset()
{
	Entity::onReset();
	mBoundsMin = QiVec2(-QI_FLOAT_MAX, -QI_FLOAT_MAX);
	mBoundsMax = QiVec2(QI_FLOAT_MAX, QI_FLOAT_MAX);

	for(int i=0; i<mObjects.getCount(); i++)
	{
		mLevel->mPhysWorld->DestroyBody(mObjects[i]->mBody);
		QI_DELETE(mObjects[i]);
	}
	mObjects.clear();
}


void Throwable::update()
{
	for(int i=0; i<mObjects.getCount(); i++)
	{
		Object* o = mObjects[i];
		o->mTransform.pos = vec(o->mBody->GetPosition());
		o->mTransform.rot = o->mBody->GetAngle();
		o->mFrame++;
		
		if (o->mType == 1 || o->mType == 2)
		{
			float dist = 1.5f;
			if (o->mEnabled && lengthSquared(mLevel->mBadGuy->mTransform.pos - o->mTransform.pos) < dist*dist)
			{
				if (o->mType == 2 || mLevel->mDude->mTransform.pos.x < mLevel->mBadGuy->mTransform.pos.x)
				{
					o->mBody->SetLinearVelocity(-0.5f * o->mBody->GetLinearVelocity());
					mLevel->mBadGuy->slowDown();
					o->mEnabled = false;
				}
			}
		}
		if (o->mType == 4)
		{
			float dist = 1.0f;
			if (o->mFrame > 30 && lengthSquared(mLevel->mDude->mTransform.pos - o->mTransform.pos) < dist*dist)
			{
				mLevel->pickUpCoin(o->mTransform.pos);
				
				mObjects.removeElementAt(i);
				mLevel->mPhysWorld->DestroyBody(o->mBody);
				QI_DELETE(o);
				i--;			
			}
		}
		if (o->mType == 1)
		{
			for(int j=0; j<mLevel->mEntities.getCount(); j++)
			{
				if (mLevel->mEntities[j]->getType() == Entity::BREAKABLE)
				{
					Breakable* b = (Breakable*)mLevel->mEntities[j];
					if (b->mFragments.getCount() == 1 && b->crosses(o->mTransform.pos, vec(o->mBody->GetLinearVelocity()), 0.1f))
					{
						o->mBody->SetLinearVelocity(0.5f * o->mBody->GetLinearVelocity());
						b->breakUp(o->mTransform.pos, vec(o->mBody->GetLinearVelocity()));	
					}
				}
			}
		}
	}
}


void Throwable::renderTransparent()
{
	if (mTexture.getTexture() == NULL)
		return;

	for(int i=0; i<mObjects.getCount(); i++)
	{
		Object* o = mObjects[i];

		if (gGame->mDisplay->isVisible(o->mTransform.pos - QiVec2(1,1), o->mTransform.pos + QiVec2(1,1), 0.0f))
		{
			QiRenderState state;
			state.shader = gGame->mGfx->m2DTexShader.getShader();
			state.blendMode = state.BLEND;
			if (o->mType == 1)
				state.texture[0] = mTexture.getTexture();
			if (o->mType == 2)
				state.texture[0] = mTextureBanana.getTexture();
			if (o->mType == 3)
				state.texture[0] = mTextureHelmet.getTexture();
			if (o->mType == 4)
				state.texture[0] = mTextureCoin.getTexture();
			state.depthTest = true;
			state.depthMask = false;
			gGame->mRenderer->setState(state);

			float w = 0.4f;
			float h = 0.4f;
			if (o->mType==3)
			{
				if (gGame->mPlayer->getCharacter() == "ouie")
					w = h = 0.9f;
				else
					w = h = 0.6f;
			}
			
			QiTransform3 t;
			t.pos.set(o->mTransform.pos.x, o->mTransform.pos.y, 0.0f);
			t.rot = QiQuat(QiVec3::Z, o->mTransform.rot);
			QiMatrix4 mat(t);
			QiMatrix4 scale;
			scale.m[0] = w;
			scale.m[5] = -h;
			QiMatrix4 trans(QiVec3(-w*0.5f, h*0.5f, 0.0f));
			gGame->mGfx->drawRectangle(mat*trans*scale);
		}
		else
		{
			if (o->mType != 2)
			{
				mObjects.removeElementAt(i);
				mLevel->mPhysWorld->DestroyBody(o->mBody);
				QI_DELETE(o);
				i--;
			}

		}
	}
}

// 1 = baseball
// 2 = banana
// 3 = helmet
// 4 = coin
void Throwable::spawn(int type, const QiVec2& pos, const QiVec2& vel)
{
	if (type != 4)
		gGame->mAudio->playSound(mSound.next(), pos);

	Object* obj = QI_NEW Object();

	b2BodyDef bd;
	bd.position = vec(pos);
	bd.linearVelocity = vec(vel);
	bd.type = b2_dynamicBody;
	bd.fixedRotation = (type == 2);
	bd.userData = NULL;
	obj->mBody = mLevel->mPhysWorld->CreateBody(&bd);

	b2CircleShape s;
	s.m_p.Set(0.0f, 0.0f);
	s.m_radius = 0.2f;

	b2FixtureDef fd;
	fd.filter.categoryBits = 1;
	fd.filter.maskBits = 252;
	fd.shape = &s;
	fd.density = 1.0f;
	if (type == 4)
		fd.density = 0.01f;
	fd.friction = 1.0f;
	if (type == 1)
		fd.restitution = 0.6f;
	if (type == 3 || type == 4)
		fd.restitution = 0.4f;

	obj->mBody->CreateFixture(&fd);

	if (type == 3)
	{
		s.m_p.Set(0.1f, 0.0f);
		obj->mBody->CreateFixture(&fd);
	}

	obj->mTransform.pos = pos;
	obj->mType = type;
	obj->mEnabled = true;
	obj->mFrame = 0;
	mObjects.add(obj);
}
