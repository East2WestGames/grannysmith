/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "entity.h"
#include "resman.h"

class Wire : public Entity
{
public:
	Wire(class Level* level);

	virtual void update();
	virtual void setTransform(const QiTransform2& transform);
	virtual void renderTransparent();
	virtual void onReset();

	void buildWire(const QiVec2& p0, const QiVec2& p1, QiArray<QiVec2>& points, int segments);

	bool intersects(const QiVec2& point, const QiVec2& direction, QiVec2& result, QiVec2& normal, float& dist, float* t = NULL) const;
	void attach(const QiVec2& point);

	QiVec2 mPoint0;
	QiVec2 mPoint1;

	QiVec2 mAttach[2];
	float mFlap[3];
	int mAttachCount;
	int mLastAttachCount;
	int mAttachCount2;

	float mHangAmount;

	QiVertexFormat mFmt;
	QiVertexBuffer mVb;
	QiIndexBuffer mIb;
};

