/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "resman.h"
#include "qi_gfx.h"

class Batch
{
public:
	Batch();
	~Batch();

	void init();
	void clear();
	void add(int i0, int i1, int i2, const QiVec3& p0, const QiVec3& p1, const QiVec3& p2);
	void finish();

	void render(bool positive);

	QiRenderState mState;
	QiVertexFormat mFmt;
	QiVertexBuffer mVb;
	QiArray<class Chunk*> mChunks;
};

