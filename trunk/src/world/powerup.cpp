/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "powerup.h"
#include "level.h"
#include "game.h"
#include "gfx.h"
#include "dude.h"
#include "audio.h"
#include "player.h"

PowerUp::PowerUp(Level* level) : Entity(level, POWERUP), mTaken(false)
{
	mProperties.add("type", "coin");
	loadProperties();
}


void PowerUp::loadProperties()
{
	Entity::loadProperties();
	QiString type = mProperties.getString("type");
	if (type == "coin")
	{
		mTexture = mLevel->mResMan->acquireTexture("gfx/coin.png");
	}
	if (type == "banana")
	{
		mTexture = mLevel->mResMan->acquireTexture("gfx/pu_banana.png");
		mSound = mLevel->mResMan->acquireSound("snd/item.ogg");
	}
	if (type == "baseball")
	{
		mTexture = mLevel->mResMan->acquireTexture("gfx/pu_baseball.png");
		mSound = mLevel->mResMan->acquireSound("snd/item.ogg");
	}
	if (type == "helmet")
	{
		mTexture = mLevel->mResMan->acquireTexture("gfx/pu_helmet.png");
		mSound = mLevel->mResMan->acquireSound("snd/item.ogg");
	}
	if (type == "apple")
	{
		mTexture = mLevel->mResMan->acquireTexture("gfx/apple.png");
		mSound = mLevel->mResMan->acquireSound("snd/apple.ogg");
	}
}


void PowerUp::update()
{
	if (mTaken || mLevel->mDude->mDead)
		return;

	QiString type = mProperties.getString("type");

	float coinRadius = 1.5f;
	const QiVec2& p = mLevel->mDude->mTransform.pos;
	if (lengthSquared(mTransform.pos-p) < coinRadius*coinRadius)
	{
		mTaken = true;
		
		if (type == "apple")
			mTexture = mLevel->mResMan->acquireTexture("gfx/apple_taken.png");
		else
			invalidateBounds();

		if (!mLevel->mReplay)
			gGame->mAudio->playSound(mSound.getSound());

		if (type == "coin")
		{
			mLevel->pickUpCoin(mTransform.pos);
		}
		else if (type == "apple")
		{
			if (!mLevel->mReplay)
				mLevel->pickUpApple(mTransform.pos);			
		}
		else
		{
			if (!mLevel->mReplay)
				gGame->mPlayer->addPowerUp(type);
		}
	}

	if (type == "apple")
	{
		const QiVec2& p2 = mLevel->mBadGuy->mTransform.pos;
		if (lengthSquared(mTransform.pos-p2) < coinRadius*coinRadius)
		{
			mTaken = true;
			if (type == "apple")
				mTexture = mLevel->mResMan->acquireTexture("gfx/apple_taken.png");
			else
				invalidateBounds();
		}
	}
}


void PowerUp::renderTransparent()
{
	if (mTexture.getTexture() == NULL)
		return;

	QiRenderState state;
	state.shader = gGame->mGfx->m2DTexShader.getShader();
	state.blendMode = state.BLEND;
	state.texture[0] = mTexture.getTexture();
	state.depthTest = true;
	state.depthMask = false;
	gGame->mRenderer->setState(state);

	float w = 0.5f;
	float h = 0.5f;
	float offY = 0.0f;
	if (mProperties.getString("type") == "apple")
	{
		w = 1.7f + QiSin(mLevel->mSimTime*6.0f)*0.3f;
		h = w;
	}
	QiTransform3 t;
	t.pos.set(mTransform.pos.x, mTransform.pos.y + offY, mZ);
	t.rot = QiQuat(QiVec3::Z, mTransform.rot);
	QiMatrix4 mat(t);
	QiMatrix4 scale;
	scale.m[0] = w;
	scale.m[5] = -h;
	QiMatrix4 trans(QiVec3(-w*0.5f, h*0.5f, 0.0f));
	gGame->mGfx->drawRectangle(mat*trans*scale);
}


void PowerUp::onReset()
{
	Entity::onReset();
	mTaken = false;
	mBoundsMin = mTransform.pos - QiVec2(0.5f, 0.5f);
	mBoundsMax = mTransform.pos + QiVec2(0.5f, 0.5f);
}


