/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "level.h"
#include "game.h"
#include "display.h"
#include "script.h"
#include "gfx.h"
#include "profiling.h"
#include "device.h"

#include "body.h"
#include "dude.h"
#include "joint.h"
#include "sensor.h"
#include "decal.h"
#include "handle.h"
#include "wire.h"
#include "breakable.h"
#include "powerup.h"
#include "note.h"
#include "throwable.h"

#include "batch.h"
#include "scene.h"
#include "player.h"
#include "particlesystem.h"

#include "qi_base.h"
#include "qi_file.h"
#include "qi_math.h"
#include "qi_app.h"
#include "store.h"
#include "Box2D.h"
#include "td/td.h"
#ifdef QI_ANDROID
#include <android/log.h>
#endif
Sensor* gLastFanSensor = NULL;

extern bool gCapture;
extern bool gCaptureMode;

Level::Level() : mResMan(NULL), mScript(NULL), mPhysWorld(NULL), mDude(NULL), mBadGuy(NULL), mSimTime(0.0f), mReplay(false), mTrigReset(false),
mOwnApple(false), mCoins(0), mApples(0), mScore(0), mDrawCallCount(0), mFrame(0), mTimeScale(1.0f), mResetTimer(0.0f), mLoaded(false), mDieTimer(0.0f)
{
	mProperties.add("script", "");
	mProperties.add("background", "");
	mProperties.add("startDelay", "1.0");
	mProperties.add("startDelayHard", "1.0");
	mProperties.add("grass", "decals/grass.xml");
	mProperties.add("decals", "decals/farm.xml");
	mProperties.add("badguyspeed", "0.9");
	mProperties.add("badguyspeedhard", "0.95");
	mProperties.add("gravity", "13.0");
	mProperties.add("bgscrollspeed", "1.0");
	mProperties.add("grassmovement", "1.0");
	mProperties.add("fanloop", "snd/fan_loop.ogg");
	mProperties.add("fanswoosh", "snd/fan_swoosh.ogg");
	mProperties.add("timescale", "1.0");
	mProperties.add("clearmem", "0");

	mProperties.add("simtime", "0.0", PROPERTY_INVISIBLE | PROPERTY_VOLATILE);
	mProperties.add("state", "", PROPERTY_INVISIBLE | PROPERTY_VOLATILE);
	mProperties.add("replay", "0", PROPERTY_INVISIBLE | PROPERTY_VOLATILE);
	mProperties.add("score", "0", PROPERTY_INVISIBLE | PROPERTY_VOLATILE);
	mProperties.add("highscore", "0", PROPERTY_INVISIBLE | PROPERTY_VOLATILE);
	mProperties.add("progress.dude", "0.0", PROPERTY_INVISIBLE | PROPERTY_VOLATILE);
	mProperties.add("progress.badguy", "0.0", PROPERTY_INVISIBLE | PROPERTY_VOLATILE);
	mProperties.add("ownapple", "0", PROPERTY_INVISIBLE | PROPERTY_VOLATILE);
	mProperties.add("coins", "0", PROPERTY_INVISIBLE | PROPERTY_VOLATILE);
	mProperties.add("apples", "0", PROPERTY_INVISIBLE | PROPERTY_VOLATILE);
	mProperties.add("applepos0", "0", PROPERTY_INVISIBLE | PROPERTY_VOLATILE);
	mProperties.add("applepos1", "0", PROPERTY_INVISIBLE | PROPERTY_VOLATILE);
	mProperties.add("applepos2", "0", PROPERTY_INVISIBLE | PROPERTY_VOLATILE);
	mProperties.add("name", "", PROPERTY_INVISIBLE | PROPERTY_VOLATILE);

	mProperties.add("editZoom", "1", PROPERTY_INVISIBLE);
	mProperties.add("editPan", "0 0", PROPERTY_INVISIBLE);
	mProperties.add("editWorkMode", "0", PROPERTY_INVISIBLE);

	mTdSolver = tdSolverCreate();
	tdSolverSetIterationCount(mTdSolver, 3);
	mTdContext = tdContextCreate(malloc, free);
	tdSpace space = {TD_SPACE_DYNAMIC_AABB, 0.0f};
	mTdSpace = tdSpaceCreate(&space);

	mResMan = QI_NEW ResMan();
	mResMan->mPreventUnload = true;

	mPhysWorld = NULL;
	mPhysGround = NULL;
	mThrowables = NULL;
	mScript = NULL;
	
	mDecalBatch = QI_NEW Batch();
	mDecalBatch->mFmt.addField("aPosition", QiVertexFormat::FLOAT32, 3);
	mDecalBatch->mFmt.addField("aTexCoord", QiVertexFormat::UINT8, 2);
	mDecalBatch->mFmt.addField("aColor", QiVertexFormat::UINT8, 4);
	mDecalBatch->mState.shader = gGame->mGfx->mDecalShader.getShader();
	mDecalBatch->init();

	mShadowBatch = QI_NEW Batch();
	mShadowBatch->mFmt.addField("aPosition", QiVertexFormat::FLOAT32, 3);
	mShadowBatch->mFmt.addField("aAlpha", QiVertexFormat::FLOAT32, 1);
	mShadowBatch->mState.shader = gGame->mGfx->mShadowShader.getShader();
	mShadowBatch->init();

	mFoliageBatch = QI_NEW Batch();
	mFoliageBatch->mFmt.addField("aPosition", QiVertexFormat::FLOAT32, 3);
	mFoliageBatch->mFmt.addField("aTexCoord", QiVertexFormat::UINT8, 2);
	mFoliageBatch->mFmt.addField("aColor", QiVertexFormat::UINT8, 4);
	mFoliageBatch->mFmt.addField("aAmplitude", QiVertexFormat::UINT8, 1);
	mFoliageBatch->mFmt.addField("aFrequency", QiVertexFormat::UINT8, 1);
	mFoliageBatch->mState.shader = gGame->mGfx->mFoliageShader.getShader();
	mFoliageBatch->init();

	mParticleFmt.clear();
	mParticleFmt.addField("aPosition", QiVertexFormat::FLOAT32, 3);
	mParticleFmt.addField("aTexCoord", QiVertexFormat::FLOAT32, 2);
	mParticleFmt.addField("aAlpha", QiVertexFormat::UINT8, 1);
	mParticleVb.init(mParticleFmt, 1024);
	mParticleIb.init(1024);	
	
	mFanChannel = NULL;
	mEffects = QI_NEW ParticleSystem();
	mFanParticles = QI_NEW ParticleSystem();
	
	mCutScene = false;
	mBreakableFragments = 0;
	mUpSideDown = false;
	mGravity = 13.0f;
}


Level::~Level()
{
	QI_DELETE(mFanParticles);
	QI_DELETE(mEffects);
	
	QI_DELETE(mFoliageBatch);
	QI_DELETE(mShadowBatch);
	QI_DELETE(mDecalBatch);

	tdSpaceDestroy(mTdSpace);
	tdContextDestroy(mTdContext);
	tdSolverDestroy(mTdSolver);
	clear();

	mParticleTexture.release();
	mParticleShader.release();

	QI_DELETE(mResMan);
}


void Level::init()
{
	mDecals.clear();

	mProperties.setString("simtime", "0.0");
	mProperties.setString("state", "");
	mProperties.setString("mode", "roll");
	mProperties.setString("script", "");

	QiString path = gGame->mPlayer->getLevelPath();
	if (path != "")
		mResMan->setAdditionalPath(QiPath::getDirPart(path));

	mPhysWorld = QI_NEW b2World(b2Vec2(0, -10.0f));

	b2BodyDef gnd;
	gnd.type = b2_staticBody;
	mPhysGround = mPhysWorld->CreateBody(&gnd);

	QiTimer t;
	mDude = QI_NEW Dude(this, 1);
	mEntities.add(mDude);

	mThrowables= QI_NEW Throwable(this);
	mEntities.add(mThrowables);

	mSimTime = 0.0f;
	mRespawnTimer = 0.0f;

	loadTemplates();

	gGame->mDisplay->reset();
	
	mParticleShader = mResMan->acquireShader("shaders/particles.glsl");
}


void Level::clear()
{
	mProperties.reset();

	mDecalTexture.release();
	mFoliageTexture.release();
	mParticleTexture.release();

	while(mEntities.getCount() > 0)
	{
		for(int i=0; i<mEntities.getCount(); i++)
		{
			if (!mEntities[i]->mParent)
				destroy(mEntities[i]);
		}
	}

	mDude = NULL;
	mBadGuy = NULL;
	mThrowables = NULL;

	if (mPhysGround)
		mPhysWorld->DestroyBody(mPhysGround);
	mPhysGround = NULL;
	
	if (mPhysWorld)
		QI_DELETE(mPhysWorld);
	mPhysWorld = NULL;

	if (mScript)
	{
		QI_DELETE(mScript);
		mScript = NULL;
	}

	tdSpaceClear(mTdSpace);	
}


void Level::start()
{	
	mProperties.setString("name", gGame->mLevelName);		
	mGravity = mProperties.getFloat("gravity");
	
	mStartX = mDude->mTransform.pos.x;
	mGoalX = mStartX + 1000.0f;
	bool hasFans = false;
	for(int i=0; i<mSensors.getCount(); i++)
	{
		if (mSensors[i]->mProperties.getString("action")=="win")
			mGoalX = mSensors[i]->mTransform.pos.x;
		if (mSensors[i]->mProperties.getString("action").startsWith("force"))
			hasFans = true;
	}	
	
	QiString bgPath = mProperties.getString("background");
	if (bgPath)
	{
		mBackground = mResMan->acquireTexture(bgPath);	
		mBackground.getTexture()->enableRepeat(true);
	}

	mBadGuy = QI_NEW Dude(this, 2);
	mEntities.add(mBadGuy);
	mBadGuy->mProperties.setVec2("pos", mDude->mProperties.getVec2("pos"));
	mBadGuy->mProperties.setFloat("rot", mDude->mProperties.getFloat("rot"));
	if (gGame->isHard())
		mBadGuy->mSpeedMultiplier = mProperties.getFloat("badguyspeedhard");
	else
		mBadGuy->mSpeedMultiplier = mProperties.getFloat("badguyspeed");
  
	for(int i=0; i<mEntities.getCount(); i++)
		mEntities[i]->onStart();

	int a = 0;
	for(int i=0; i<mEntities.getCount(); i++)
	{
		if (mEntities[i]->getType() == Entity::POWERUP)
		{
			PowerUp* p = (PowerUp*)mEntities[i];
			if (p->mProperties.getString("type") == "apple")
			{
				float f = (p->mTransform.pos.x - mStartX) / (mGoalX-mStartX);
				mProperties.setFloat(QiString("applepos")+a, f); 
				a++;
			}
		}
	}
	
	if (hasFans)
	{
		mFanSound = mResMan->acquireSound(mProperties.getString("fanloop"));
		mFanSwooshSound = mResMan->acquireSound(mProperties.getString("fanswoosh"));
		if (!mFanChannel)
			mFanChannel = gGame->mAudio->mEngine.acquireChannel();
		if (mFanChannel)
		{
			if (mFanSound.getSound())
			{
				mFanChannel->setBuffer(mFanSound.getSound());
				mFanChannel->setLooping(true);
				mFanChannel->setVolume(0.0f);
				mFanChannel->play();
			}
		}
	}
	
	fillBatches();

	mBadGuyMotion.clear();
	if (gGame->mInput->isKeyDown(QI_KEY_SHIFT))
		mBadGuy->mOutput = &mBadGuyMotion;
	else
	{
		QiString path = gGame->mPlayer->getLevelPath();
		mResMan->load(path + ".motion", mBadGuyMotion);
		mBadGuy->mInput = &mBadGuyMotion;
	}

	mResMan->clearUnused();

	mLoaded = true;
	
	mEffects->load("powerup");
	mFanParticles->load("fan");
	
    mNamedEntities.clear();
    for(int i=0; i<mEntities.getCount(); i++)
    {
        if (mEntities[i]->mProperties.getString("name") != "")
            mNamedEntities.add(mEntities[i]);
    }

	reset();	
}


void Level::stop()
{
	if (mFanChannel)
		gGame->mAudio->mEngine.releaseChannel(mFanChannel);

	mFanChannel = NULL;
	mFanSound.release();
	mFanSwooshSound.release();

	if (mBadGuy->mOutput == &mBadGuyMotion)
	{
		mBadGuy->mOutput = NULL;
		QiString path = gGame->mPlayer->getLevelPath();
		mResMan->save(path + ".motion", mBadGuyMotion.getData(), mBadGuyMotion.getSize());
	}
	
	if (mBadGuy)
	{
		destroy(mBadGuy);
		mBadGuy = NULL;
	}

	mBackground.release();
	
	for(int i=0; i<mEntities.getCount(); i++)
		mEntities[i]->onStop();

	clearBatches();

	setTimeScale(1.0f);

	gGame->mPlayer->save();

	mLoaded = false;
}


void Level::reset()
{
	mDieTimer = 0.0f;
	gLastFanSensor = NULL;

	setTimeScale(1.0f);

	mPhysWorld->SetGravity(b2Vec2(0, -mGravity));

	mFrame = 0;
	mProperties.setBool("replay", mReplay);
	mProperties.setInt("highscore", gGame->mPlayer->getScore(gGame->mLevelName));

	if (!mReplay)
	{
		gGame->mPlayer->registerPlayed();
		gGame->mPlayer->save();
	}
	
	gGame->mHudScene->mScript.handleCommand("start");

	mProperties.setString("state", "play");
	mSimTime = 0.0f;
	mRespawnTimer = 0.0f;
	mOwnApple = false;
	mCoins = 0;
	mApples = 0;
	mScore = 0;

	for(int i=0; i<mEntities.getCount(); i++)
		if (!mEntities[i]->mParent && mEntities[i]->getType() != Entity::JOINT)
			mEntities[i]->onReset();
	for(int i=0; i<mEntities.getCount(); i++)
		if (!mEntities[i]->mParent && mEntities[i]->getType() == Entity::JOINT)
			mEntities[i]->onReset();

	QiString script = mProperties.getString("script");
	if (script != "")
	{
		if (mScript)
			QI_DELETE(mScript);
		mScript = QI_NEW Script(mResMan);
		if (!mScript->load(script))
			gGame->logE("Error loading script: " + script);
	}

	gGame->mDisplay->reset();

	if (mBadGuy->mOutput)
		mBadGuyMotion.clear();
	else
		mBadGuyMotion.seekRead(0);

	if (mReplay)
	{
		mPlayerMotion.seekRead(0);
		mDude->mInput = &mPlayerMotion;
		mDude->mOutput = NULL;
	}
	else
	{
		mDude->mInput = NULL;
		mPlayerMotion.clear();
		mDude->mOutput = &mPlayerMotion;
	}

	mTrigReset = false;

	for(int i=0; i<mBodies.getCount(); i++)
	{
		if (mBodies[i]->mPhysBody)
		{
			mBodies[i]->mPhysBody->SetActive(false);
			mBodies[i]->mPhysBody->SetActive(true);
		}
	}

	//Yes, this is ugly, but Box2D oviously cannot put a newly awaken object to sleep immediately
	mPhysWorld->Step(0.001f, 5, 2);

	for(int i=0; i<mBodies.getCount(); i++)
	{
		if (mBodies[i]->mPhysBody)
			mBodies[i]->mPhysBody->SetAwake(false);
	}
}


Entity* Level::createEntity(Entity::Type type)
{
	Entity* entity = NULL;
	switch(type)
	{
	case Entity::BODY:
		{
			entity = QI_NEW Body(this);
			mBodies.add((Body*)entity);
			break;
		}
	case Entity::JOINT:
		{
			entity = QI_NEW Joint(this);
			break;
		}
	case Entity::SENSOR:
		{
			entity = QI_NEW Sensor(this);
			mSensors.add((Sensor*)entity);
			break;
		}
	case Entity::DECAL:
		{
			entity = QI_NEW Decal(this);
			break;
		}
	case Entity::HANDLE:
		{
			entity = QI_NEW Handle(this);
			break;
		}
	case Entity::WIRE:
		{
			entity = QI_NEW Wire(this);
			break;
		}
	case Entity::BREAKABLE:
		{
			entity = QI_NEW Breakable(this);
			break;
		}
	case Entity::POWERUP:
		{
			entity = QI_NEW PowerUp(this);
			break;
		}
	case Entity::NOTE:
		{
			entity = QI_NEW Note(this);
			break;
		}
	case Entity::THROWABLE:
		{
			entity = QI_NEW Throwable(this);
			break;
		}
	default:
		QI_ASSERT(true, "Unregistered type");
		break;
	}
	if (entity)
		mEntities.add(entity);

	return entity;
}


void Level::destroy(Entity* ent)
{
	if (ent->getType() == Entity::BODY)
		mBodies.removeAll((Body*)ent);
	if (ent->getType() == Entity::SENSOR)
		mSensors.removeAll((Sensor*)ent);
	mEntities.removeAll(ent);
	QI_DELETE(ent);
}


Entity::Type Level::getEntityType(const QiString& name)
{
	for(int i=0; i<Entity::COUNT; i++)
	{
		if (name == gEntityTypeNames[i])
			return (Entity::Type)i;
	}
	return Entity::COUNT;
}


QiString Level::getEntityTypeName(Entity::Type type)
{
	QI_ASSERT(type < Entity::COUNT, "Invalid type");
	return gEntityTypeNames[type];
}


Entity* Level::findEntity(const QiString& name) const
{
	for(int i=0; i<mNamedEntities.getCount(); i++)
	{
		if (mNamedEntities[i]->mProperties.getString("name") == name)
			return mNamedEntities[i];
	}
	return NULL;
}


void Level::win()
{
	if (mProperties.getString("state") == "cleared")
		return;

	if (mBadGuy->mOutput)
		gGame->requestState(Game::MENU);

	//int score = 100 + QiMax(0, (int)((mDude->mTransform.pos.x - mBadGuy->mTransform.pos.x) * 50.0f));
	//mScore += score;
	//mProperties.setInt("score", mScore);
	//gGame->mHudScene->mScript.handleCommand(QiString("score ") + score);

	if (mScore == 0)
		mScore = 1;

	mDude->mReplayHints |= HINT_WIN;

	mProperties.setString("state", "cleared");
	if (!mReplay)
	{
		//Save replay on high score
		if (mScore > gGame->mPlayer->getScore(gGame->mLevelName) || gCaptureMode)
		{
			QiString levelName = gGame->mLevelName;
			if (gGame->isHard())
				levelName += "-hard";		
			gGame->mResMan->save("user://replay-" + levelName + ".motion", mPlayerMotion.getData(), mPlayerMotion.getSize());
		}
		gGame->mPlayer->registerCleared(mScore, mApples);

		gGame->mHudScene->mScript.handleCommand("cleared");

		if (mCoins > 0)
			gGame->mPlayer->addCoins(mCoins);

		gGame->mPlayer->save();
	}
}


void Level::loose()
{
	if (mProperties.getString("state") != "play")
		return;

	mProperties.setString("state", "failed");
	if (!mReplay)
		gGame->mHudScene->mScript.handleCommand("failed");

	gGame->mPlayer->save();
}


void Level::pickUpApple(const QiVec2& pos)
{
	mApples += 1;
	int score = 1000 + QiMax(0, (int)((mDude->mTransform.pos.x - mBadGuy->mTransform.pos.x) * 50.0f));
	mScore += score;
	gGame->mHudScene->mScript.handleCommand(QiString("score ") + score);
	gGame->mHudScene->mScript.handleCommand(QiString("apple ") + score);

	QiVec3 v = mDude->getVelocity().vec3() * 0.5f;
	for(int i=0; i<16; i++)
		mEffects->spawn(pos.vec3(), v + QiVec3::random(3.0f) + QiVec3(0,0,2), 1);
}


void Level::pickUpCoin(const QiVec2& pos)
{
	if (!mReplay)
	{
		mCoins += 10;
		gGame->mAudio->playSound(gGame->mAudio->mCoin.next());
	}

	QiVec3 v = mDude->getVelocity().vec3() * 0.5f;
	for(int i=0; i<8; i++)
		mEffects->spawn(pos.vec3(), v + QiVec3::random(3.0f) + QiVec3(0,0,2), 0);
}


void Level::update()
{
#if defined(QI_WIN32) && defined(QI_DEBUG)
	QiSystem::setFpu(QiSystem::FPU_80_BITS, true);
#endif

	PROFILE_FUNC();

	if (mUpSideDown)
		mPhysWorld->SetGravity(b2Vec2(0, mGravity));
	else
		mPhysWorld->SetGravity(b2Vec2(0, -mGravity));

	//For replay only
	if (mDude->mInput == &mPlayerMotion && !mPlayerMotion.hasMoreData())
	{
		if (gGame->mReplay)
			gGame->requestState(Game::MENU);
		else
		{
			//Reset in one second
			if (mResetTimer == 0.0f)
				mResetTimer = 2.0f;
			if (mResetTimer > 0.0f)
			{
				mResetTimer -= gGame->mTimeStep;
				if (mResetTimer < 0.0f)
				{
					mResetTimer = 0.0f;
					mTrigReset = true;
					gGame->mDevice->setAlwaysOn(false);
				}
			}
		}
	}

	if (mProperties.getString("state")=="play" && mSimTime > 1.0f && mBadGuy->mOutput != &mBadGuyMotion)
	{
		static float sOvertakeSoundTimer = 0.0f;
		sOvertakeSoundTimer -= gGame->mTimeStep;

		if (!mDude->mDead && mDude->mTransform.pos.x > mBadGuy->mTransform.pos.x)
		{
			//Got the apple
			if (!mOwnApple)
			{
				mDude->mReplayHints |= HINT_APPLE;
				if (length(mDude->mTransform.pos - mBadGuy->mTransform.pos) < 4.0f)
				{
					if (sOvertakeSoundTimer < 0.0f)
					{
						gGame->mAudio->playSound(mDude->mSndLaugh.next(), mDude->mTransform.pos);
						sOvertakeSoundTimer = 3.0f;
					}
				}
			}
			mOwnApple = true;
		}
		else if (mDude->mTransform.pos.x < mBadGuy->mTransform.pos.x)
		{
			//Lost the apple
			if (mOwnApple)
			{
				mDude->mReplayHints |= HINT_APPLE;
				if (length(mDude->mTransform.pos - mBadGuy->mTransform.pos) < 4.0f)
					if (sOvertakeSoundTimer < 0.0f)
					{
						gGame->mAudio->playSound(mDude->mSndMurmur.next(), mDude->mTransform.pos);
						sOvertakeSoundTimer = 3.0f;
					}
			}
			mOwnApple = false;
		}
	}

	{
		PROFILE_ZONE("Update Box2D");
		mPhysWorld->Step(gGame->mTimeStep, 5, 2);
	}

	{
		PROFILE_ZONE("Update entities");
		mBreakableFragments = 0;
		for(int i=0; i<mEntities.getCount(); i++)
		{
			if (mEntities[i]->getType() != Entity::DUDE)
				mEntities[i]->update();
				
			if (mEntities[i]->getType() == Entity::BREAKABLE)
				mBreakableFragments += ((Breakable*)mEntities[i])->mFragments.getCount();
		}

		//Update dudes manually after other entities
		//They are the most important, so we want the latest state in update
		mBadGuy->update();
		if (mBadGuy->mOutput==NULL)	// Do not update dude if we're recording bad guy motion
			mDude->update();
	}

	if (mDieTimer > 0.0f)
	{
		mDieTimer -= gGame->mTimeStep;
		if (mDieTimer <= 0.0f){
			if(gGame->getDieCount() >= 2 && !gGame->mPlayer->hasUnlockAll()){
				gGame->showAds("com.mediocre.grannysmith.die");
			}else{
				mTrigReset = true;
				gGame->die();
				ShowAd();

			}
		}
	}

	mDude->mKeepAlive = (mDude->mInput != NULL);
	gGame->mDisplay->mCameraSensor = 0;
	gGame->mDisplay->mCameraSensorRot = 2;
	gGame->mDisplay->mCamera2Sensor = NULL;
	mGravity = mProperties.getFloat("gravity");
	mCutScene = false;
	mUpSideDown = false;
	float fanVolume = 0.0f;
	Sensor* fanSensor = NULL;
	for(int j=0; j<2; j++)
	{
        PROFILE_ZONE("Sensors");
		Dude* dude = (j == 0 ? mDude : mBadGuy);
		dude->mNoCorrection = false;
		dude->mTimingAidPoint.set(-QI_FLOAT_MAX, 0.0f);
		dude->mTimingAidDirection.set(1.0f, 0.0f);
		bool mainCharacter = (mBadGuy->mOutput != NULL ? (j == 1) : (j == 0));
		QiVec2 p = dude->mTransform.pos;
		for(int i=0; i<mSensors.getCount(); i++)
		{
			if (mSensors[i]->testPoint(p))
			{
				QiString str = mSensors[i]->mProperties.getString("action");
				QiString action = str.getWord(0);
				if (mainCharacter && action == "die" && !mReplay)
				{
					if (mDieTimer <= 0.0f)
					{
						gGame->mPlayer->registerDie(mDude->mTransform.pos);
						gGame->mPlayer->save();
						mDieTimer = 1.0f;
						mDiePos = dude->mTransform.pos;
						dude->mDead = true;
					}
				}
				else if (mainCharacter && action == "win")
				{
					win();
				}
				else if (action == "live")
					dude->mKeepAlive = true;
				else if (mainCharacter && action == "camera")
				{
					gGame->mDisplay->mCameraSensor = str.getWord(1).toInt();
					QiString rot = str.getWord(2);
					if (rot != "")
						gGame->mDisplay->mCameraSensorRot = rot.toInt();
					QiString time = str.getWord(3);
					if (time != "")
						gGame->mDisplay->mCameraSensorTime = time.toFloat();
					else
						gGame->mDisplay->mCameraSensorTime = 0.0f;
				}
				else if (mainCharacter && action == "camera2")
				{
					gGame->mDisplay->mCamera2Sensor = mSensors[i];
					gGame->mDisplay->mCamera2Offset.x = str.getWord(1).toFloat();
					gGame->mDisplay->mCamera2Offset.y = str.getWord(2).toFloat();
					gGame->mDisplay->mCamera2Offset.z = str.getWord(3).toFloat();
					gGame->mDisplay->mCamera2Rot = str.getWord(4).toFloat();
					QiString time = str.getWord(5);
					if (time != "")
						gGame->mDisplay->mCamera2SensorTime = time.toFloat();
					else
						gGame->mDisplay->mCamera2SensorTime = 0.0f;
				}
				else if (action == "speed")
				{
					dude->mSensorSpeed = mSensors[i]->mProperties.getString("action").getWord(1).toFloat();
					dude->mSensorSpeedAir = mSensors[i]->mProperties.getString("action").getWord(2).toFloat();
				}
				else if (action == "force")
				{
					if (dude == mDude)
						fanSensor = mSensors[i];
					QiVec2 f = mSensors[i]->mTransform.toParentVec(QiVec2(str.getWord(1).toFloat(), 0.0f));
					dude->mTorso->mPhysBody->ApplyForce(vec(f), dude->mTorso->mPhysBody->GetPosition());
				}
				else if (action == "penalty")
				{
					if ((mSensors[i]->mUsed & dude->mPlayerId) == 0)
					{
						float f = QiClamp(1.0f-str.getWord(1).toFloat(), 0.0f, 1.0f);
						dude->mTorso->mPhysBody->SetLinearVelocity(f*dude->mTorso->mPhysBody->GetLinearVelocity());
						mSensors[i]->mUsed |= dude->mPlayerId;
					}
				}
				else if (action == "timing")
				{
					dude->mTimingAidPoint = mSensors[i]->mTransform.pos;
					dude->mTimingAidDirection = mSensors[i]->mTransform.toParentVec(QiVec2(1.0f, 0.0f));
				}
				else if (mainCharacter && action == "sound")
				{
					if (!mSensors[i]->mUsed)
					{
						mSensors[i]->mUsed = 1;
						gGame->mAudio->playSound(mSensors[i]->mResource.getSound());
					}
				}
				else if (mainCharacter && action == "cutscene")
				{
					mCutScene = true;
				}
				else if (mainCharacter && action == "upsidedown")
				{
					mUpSideDown = true;
				}
				else if (mainCharacter && action == "gravity")
				{
					mGravity = str.getWord(1).toFloat();
				}
				else if (action == "nocorrection")
				{
					dude->mNoCorrection = true;
				}
			}
			if (mainCharacter && mSensors[i]->mIsForce)
			{
				Sensor* s = mSensors[i];
				QiVec2 p = s->mTransform.toParentPoint(QiVec2(-s->mSize.x*0.5f, 0.0f));
				float v = QiClamp(4.0f / (1.0f+0.1f*length(p-dude->mTransform.pos)) - 0.7f, 0.0f, 1.0f);
				v = v*v;
				fanVolume = QiMax(fanVolume, v );

				p = s->mTransform.toParentPoint(QiVec2(-s->mSize.x*0.5f, QiRnd(-s->mSize.y*0.5f, s->mSize.y*0.5f)));
				mFanParticles->spawn(p.vec3(), s->mTransform.toParentVec(QiVec2(QiRnd(20.0f, 30.0f),QiRnd(-1.0f, 1.0f))).vec3());
			}	
		}
	}
	if (mFanChannel)
		gGame->mAudio->setSoundChannelVolume(mFanChannel, fanVolume);

	if (fanSensor != gLastFanSensor)
	{
		gLastFanSensor = fanSensor;
		if (fanSensor)
			gGame->mAudio->playSound(mFanSwooshSound.getSound(), mDude->mTransform.pos);
	}	

	QiString state = mProperties.getString("state");
    if (state == "play")
    {
        mSimTime += gGame->mTimeStep;
        mProperties.setFloat("simtime", mSimTime);
		mProperties.setFloat("progress.dude", (mDude->mTransform.pos.x-mStartX)/(mGoalX-mStartX));
		mProperties.setFloat("progress.badguy", (mBadGuy->mTransform.pos.x-mStartX)/(mGoalX-mStartX));
		mProperties.setBool("ownapple", mOwnApple);
		mProperties.setInt("coins", mCoins);
		mProperties.setInt("apples", mApples);
		mProperties.setInt("score", mScore);
    }

	mProperties.setBool("replay", mReplay);

    {
        PROFILE_ZONE("Level script");
        if (mScript)
            mScript->tick();
    }

    {
        PROFILE_ZONE("Effects");
        mEffects->update();
        mFanParticles->update();
    }

	mFrame += mTimeScale;
}


void Level::updateAsync()
{
	PROFILE_ZONE("Level::updateAsync");
	for(int i=0; i<mEntities.getCount(); i++)
		mEntities[i]->updateAsync();
}


void Level::syncBreakables()
{
	for(int i=0; i<mEntities.getCount(); i++)
    {
        if (mEntities[i]->getType() == Entity::BREAKABLE)
        {
            Breakable* b = (Breakable*)mEntities[i];
            b->syncTransforms();
        }
    }
}


struct DepthComparator
{
	int compare(const Entity* a, const Entity* b) const
	{
		if (a->mZ < b->mZ)
			return -1;
		else if (a->mZ > b->mZ)
			return 1;
		else
			return 0;
	}
} comp;


void Level::draw()
{
	PROFILE_FUNC();

	if (gGame->mPaused)
	{
		gGame->mAudio->setSoundChannelVolume(mFanChannel, 0.0f);
		gGame->mAudio->setSoundChannelVolume(mDude->mRailChannel, 0.0f);
		gGame->mAudio->setSoundChannelVolume(mDude->mRollingChannel, 0.0f);
		gGame->mAudio->setSoundChannelVolume(mBadGuy->mRailChannel, 0.0f);
		gGame->mAudio->setSoundChannelVolume(mBadGuy->mRollingChannel, 0.0f);
	}

	int dcc = gGame->mRenderer->getDrawCallCount();

	mParticleVb.clear();
	mParticleIb.clear();

    QiArrayInplace<Entity*, 256> visibleEntities;
    {
        PROFILE_ZONE("Cull")
        for(int i=0; i<mEntities.getCount(); i++)
        {
            Entity* e = mEntities[i];
            if (e->mEditorHidden)
                continue;
            if (e->getType() == Entity::BODY)
            {
                Body* b = (Body*)e;
				if (gGame->mDisplay->isVisible(e->mBoundsMin, e->mBoundsMax, b->mBoundsMinZ) || gGame->mDisplay->isVisible(e->mBoundsMin, e->mBoundsMax, b->mBoundsMaxZ))
				{
					e->mVisible = true;
                    visibleEntities.add(e);
				}
				else
					e->mVisible = false;
            }
            else
                if (gGame->mDisplay->isVisible(e->mBoundsMin, e->mBoundsMax, e->mZ))
				{
					e->mVisible = true;
                    visibleEntities.add(e);
				}
				else
					e->mVisible = false;
        }
    }

	//QI_PRINT("Visible: " + visibleEntities.getCount() + "/" + mEntities.getCount());

    QiArrayInplace<Entity*, 256> order;
    {
        PROFILE_ZONE("Sort");
        order.addAll(visibleEntities.getIterator());
        QiSort(order.getData(), order.getCount(), DepthComparator());
    }
    
    if (PROFILE_PARAM("render/opaque", true))
    {
        PROFILE_ZONE("Render opaque")
        for(int i=order.getCount()-1; i>=0; i--)
            order[i]->render();
    }

    if (PROFILE_PARAM("render/background", true))
    {
        PROFILE_ZONE("Render background")
        gGame->mDisplay->leaveLevel();
        if (mBackground.getTexture())
        {
			QiVec3 c = gGame->mDisplay->mCameraRot.rotate(QiVec3(0, 0, -1.0f));
            QiRenderState state;
            state.shader = gGame->mGfx->m2DTexShader.getShader();
            state.texture[0] = mBackground.getTexture();
			float speed = mProperties.getFloat("bgscrollspeed");
            state.texOffset[0].x += speed * (gGame->mDisplay->mCameraPos.x * 0.01f + c.x);
            //state.texOffset[0].y += gGame->mDisplay->mCameraPos.x * 0.01f + c.y;
            state.depthTest = true;
            state.depthMask = false;
            gGame->mRenderer->setState(state);

            QiMatrix4 mat(QiTransform3(QiVec3(0, 0, 0.99f)));
            mat.m[0] = 1024.0f;
            mat.m[5] = 768.0f;
            mat.m[10] = 1.0f;
            gGame->mGfx->drawRectangle(mat);
        }
        gGame->mDisplay->enterLevel();
    }
    
	/*
	float z = 2.0f;
	float d = gGame->mDisplay->mViewport.getCameraPos().z - z - 0.2f;
	QiVec3 p0 = gGame->mDisplay->mViewport.getCameraPos() + gGame->mDisplay->mFrustumDir0*d;
	QiVec3 p1 = gGame->mDisplay->mViewport.getCameraPos() + gGame->mDisplay->mFrustumDir1*d;
	QiVec3 p2 = gGame->mDisplay->mViewport.getCameraPos() + gGame->mDisplay->mFrustumDir2*d;
	QiVec3 p3 = gGame->mDisplay->mViewport.getCameraPos() + gGame->mDisplay->mFrustumDir3*d;
	p0.z = z;
	p1.z = z;
	p2.z = z;
	p3.z = z;
	gGame->mGfx->drawLine(p0, p1);
	gGame->mGfx->drawLine(p1, p2);
	gGame->mGfx->drawLine(p2, p3);
	gGame->mGfx->drawLine(p3, p0);
	*/
	
    if (PROFILE_PARAM("render/decals", true))
        mDecalBatch->render(false);
    if (PROFILE_PARAM("render/shadow", true))
        mShadowBatch->render(false);
    if (PROFILE_PARAM("render/foliage", true))
        mFoliageBatch->render(false);

    if (PROFILE_PARAM("render/transparent", true))
    {
        PROFILE_ZONE("Render transparent")
        for(int i=0; i<order.getCount(); i++)
            order[i]->renderTransparent();
    }

    if (PROFILE_PARAM("render/decals", true))
        mDecalBatch->render(true);
    if (PROFILE_PARAM("render/shadow", true))
        mShadowBatch->render(true);
    if (PROFILE_PARAM("render/foliage", true))
        mFoliageBatch->render(true);

	if (!gCapture)
		mEffects->render();
	mFanParticles->render();

	if (mScript)
		mScript->drawEffects();

    if (PROFILE_PARAM("render/particles", true))
	{
		QiRenderState state;
		state.blendMode = state.BLEND;
		state.shader = mParticleShader.getShader();
		state.depthMask = false;
		state.depthTest = true;
		state.texture[0] = mParticleTexture.getTexture();

		gGame->mRenderer->setState(state);
		gGame->mRenderer->drawTriangles(&mParticleVb, &mParticleIb, (mParticleVb.getCount()/4)*6);
    }

	mDrawCallCount = gGame->mRenderer->getDrawCallCount()-dcc;
}


Entity* Level::findEntity(const QiVec2& point) const
{
	Entity* e = NULL;
	if (gGame->getState() == Game::EDIT)
	{
		float r = 0.1f/gGame->mDisplay->mEditZoom;
		for(int i=0; i<mEntities.getCount(); i++)
		{
			if (mEntities[i]->mEditorHidden)
				continue;
			if (mEntities[i]->mParent==NULL && lengthSquared(mEntities[i]->mTransform.pos-point) < r*r && (e==NULL || mEntities[i]->mZ > e->mZ))
				e = mEntities[i];
		}
		if (e)
			return e;
	}
	for(int i=0; i<mEntities.getCount(); i++)
	{
		if (mEntities[i]->mEditorHidden)
			continue;
		if (!mEntities[i]->mParent && mEntities[i]->contains(point) && (e==NULL || mEntities[i]->mZ > e->mZ))
			e = mEntities[i];
	}
	return e;
}


bool Level::findBodies(const QiVec2& point, QiArray<Body*>& arr) const
{
	arr.clear();
	for(int i=0; i<mEntities.getCount(); i++)
	{
		if (mEntities[i]->mEditorHidden)
			continue;
		if (!mEntities[i]->mParent && mEntities[i]->getType() == Entity::BODY && mEntities[i]->contains(point))
			arr.add((Body*)mEntities[i]);
	}
	return arr.getCount() > 0;
}


Handle* Level::findHandle(const QiVec2& point, int playerId) const
{
	float maxGrabDist = 1.8f;
	for(int i=0; i<mEntities.getCount(); i++)
	{
		if (mEntities[i]->getType() == Entity::HANDLE)
		{
			Handle* handle = (Handle*)mEntities[i];
			if (lengthSquared(handle->getWorldPos() - point) < maxGrabDist*maxGrabDist && (handle->mProperties.getInt("mask") & playerId) != 0)
				return handle;
		}
	}
	return NULL;
}


void Level::load(QiInputStream& stream, int length)
{
	QiArrayInplace<Entity*, 2048> loaded;
    {
	QiTimer t;
	PROFILE_ZONE("Level::load");
	QI_PRINT("Clear " + t.getTime());
	clear();
	QI_PRINT("Init " + t.getTime());
	init();
    
#ifdef QI_IOS
    if (gGame->mDevice->getCpuCount() == 1)
    {
        QI_PRINT("Clearing ununsed memory");
        mResMan->clearUnused();
    }
#endif  
   
	QI_PRINT("Parse xml " + t.getTime());
	QiXmlParser xml;
	if (!xml.parse(stream, length))
		return;

	QI_PRINT("Read xml " + t.getTime());

	QI_PRINT("Effects " + t.getTime());
	//Load effect texture
	{
		QiMemoryStream<256> tmp;
		if (mResMan->load("effects/default.xml", tmp))
		{
			QiXmlParser xml;
			if (xml.parse(tmp, tmp.getSize()) && xml.getName() == "effects")
			{
				QiString fileName = xml.getAttribute("texture");
				mParticleTexture = mResMan->acquireTexture(fileName);
				QI_PRINT("Particle texture " +fileName + ": " + mParticleTexture.getTexture()->getWidth());
			}
		}
	}

	if (xml.getName() == "level")
	{
		mProperties.readXml(xml);

	    if (mProperties.getBool("clearmem"))
		{
			QI_PRINT("Clearing ununsed memory");
	        mResMan->clearUnused();
		}

		QI_PRINT("Decals " + t.getTime());
		loadDecals(mProperties.getString("grass"), true);
		loadDecals(mProperties.getString("decals"), false);

		xml.enter();
		while(xml.isValid())
		{
			if (xml.getName() == "entities")
			{
				xml.enter();
				while(xml.isValid())
				{
					if (gGame->getState() == Game::EDIT || (xml.getName() != "note" && xml.getName() != "throwable"))
						loaded.add(loadEntity(xml));
					xml.next();
				}
				xml.leave(); //entities
			}
			xml.next();
		}
		xml.leave(); //level
	}

    QI_PRINT("Done loading " + t.getTime());

	QI_PRINT("Entities: " + mEntities.getCount());
	//QiAllocator::printStats();
	mDude->mProperties.setString("name", "dude");
	xml.reset();
	}

	//QiAllocator::printStats();
	//This is where bodies and meshes are setup
	//This is done after the XML parser has gone out of context to save memory
	for(int i=0; i<loaded.getCount(); i++)
	{
		loaded[i]->loadProperties();
		loaded[i]->init();
	}
}


Entity* Level::loadEntity(QiXmlParser& xml)
{
	Entity::Type type = getEntityType(xml.getName());
	Entity* entity = NULL;
	if (type == Entity::DUDE)
		entity = mDude;
	else
		entity = createEntity(type);
	if (entity)
	{
		entity->mProperties.readXml(xml);

		//Body special case
		if (type == Entity::BODY)
		{
			if (xml.getAttribute("zMin") != "")
			{
				float zMin = xml.getAttribute("zMin").toFloat();
				float zMax = xml.getAttribute("zMax").toFloat();
				entity->mProperties.setFloat("z", -(zMax+zMin)/2);
				entity->mProperties.setFloat("depth", QiAbs(zMax-zMin));
			}
			if (xml.getAttribute("edgeSize") != "")
			{
				if (entity->mProperties.getString("edge") == "chamfer")
					entity->mProperties.setString("edge", "1");
				if (entity->mProperties.getString("edge") == "fillet")
					entity->mProperties.setString("edge", "3");
				entity->mProperties.setString("edge", entity->mProperties.getString("edge") + " " + xml.getAttribute("edgeSize"));
			}
			xml.enter();
			while(xml.isValid())
			{
				if (xml.getName() == "shape")
				{
					xml.enter();
					while(xml.isValid())
					{
						QiVec2 p(xml.getValue().getWord(0).toFloat(), xml.getValue().getWord(1).toFloat());
						((Body*)entity)->mPolygon.mPoints.add(p);
						xml.next();
					}
					xml.leave(); // shape
				}
				if (xml.getName() == "curve")
				{
					xml.enter();
					while(xml.isValid())
					{
						ControlPoint cp;
						cp.mPoint.set(xml.getValue().getWord(0).toFloat(), xml.getValue().getWord(1).toFloat());
						cp.mHandle0.set(xml.getValue().getWord(2).toFloat(), xml.getValue().getWord(3).toFloat());
						cp.mHandle1.set(xml.getValue().getWord(4).toFloat(), xml.getValue().getWord(5).toFloat());
						((Body*)entity)->mControlPoints.add(cp);
						xml.next();
					}
					xml.leave(); // curve
				}
				xml.next();
			}
			xml.leave(); // shape
		}

		//entity->loadProperties();
		//entity->init();
	}

	if (xml.getAttribute("__hidden") == "1" && gGame->getState() == Game::EDIT)
		entity->mEditorHidden = true;
	entity->mEditorGroup = xml.getAttribute("__group").toInt();

	return entity;
}



void Level::save(QiOutputStream& stream)
{
	PROFILE_ZONE("Level::save");
	QiXmlWriter xml;
	xml.enter("level");
	mProperties.writeXml(xml);
	xml.enter("entities");

	for(int i=0; i<mEntities.getCount(); i++)
	{
		if (mEntities[i]->mParent || mEntities[i]->getType() == Entity::THROWABLE)
			continue;

		saveEntity(mEntities[i], xml);
	}

	xml.leave();
	xml.leave();
	xml.write(stream);
}


void Level::saveEntity(Entity* entity, QiXmlWriter& xml)
{
	PROFILE_FUNC();
	QiString typeName = getEntityTypeName(entity->getType());
	xml.enter(typeName);
	entity->mProperties.writeXml(xml);

	//Body special case
	if (entity->getType() == Entity::BODY)
	{
		Body* b = (Body*)entity;
		if (b->mProperties.getBool("curve"))
		{
			xml.enter("curve");
			for(int j=0; j<b->mControlPoints.getCount(); j++)
			{
				xml.enter("v");
				QiVec2 p = b->mControlPoints[j].mPoint;
				QiVec2 h0 = b->mControlPoints[j].mHandle0;
				QiVec2 h1 = b->mControlPoints[j].mHandle1;
				xml.setValue(QiString() + p.x + " " + p.y + " " + h0.x + " " + h0.y + " " + h1.x + " " + h1.y);
				xml.leave();
			}
			xml.leave();
		}
		else
		{
			xml.enter("shape");
			for(int j=0; j<b->mPolygon.mPoints.getCount(); j++)
			{
				xml.enter("v");
				QiVec2 p = b->mPolygon.mPoints[j];
				xml.setValue(QiString() + p.x + QiString(" ") + p.y);
				xml.leave();
			}
			xml.leave();
		}
	}

	if (entity->mEditorHidden)
		xml.setAttribute("__hidden", "1");
	if (entity->mEditorGroup)
		xml.setAttribute("__group", QiString() + entity->mEditorGroup);

	xml.leave();
}



class Raycast : public b2RayCastCallback
{
public:
	bool mHit;
	QiVec2 mPoint;
	QiVec2 mNormal;
	Body* mBody;
	int mMask;
	int mCategory;

	Raycast(const Level* level, const QiVec2& start, const QiVec2& end, int mask, int category)
	{
		mMask = mask;
		mCategory = category;
		mHit = false;
		mBestDist = QI_FLOAT_MAX;
		mStart = start;
		mBody = NULL;
		level->mPhysWorld->RayCast(this, (b2Vec2&)start, (b2Vec2&)end);
	}

protected:
	QiVec2 mStart;
	float mBestDist;

	virtual float32 ReportFixture(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float32 fraction)
	{
		QiVec2 p = (QiVec2&)point;
		QiVec2 n = (QiVec2&)normal;
		float dist = lengthSquared(p-mStart);
		if ((fixture->GetFilterData().categoryBits & mMask) != 0 && (fixture->GetFilterData().maskBits & mCategory) &&  dist < mBestDist)
		{
			mBestDist = dist;
			mPoint = p;
			mNormal = n;
			if (fixture->GetUserData())
				mBody = ((BodyConvex*)fixture->GetUserData())->body;
			else
				mBody = NULL;
			mHit = true;
		}
		return -1;
	}
};


bool Level::raycast(const QiVec2& start, const QiVec2& end, int mask, int category, QiVec2* point, QiVec2* normal, Body** body) const
{
	Raycast r(this, start, end, mask, category);
	if (point)
		*point = r.mPoint;
	if (normal)
		*normal = r.mNormal;
	if (body)
		*body = r.mBody;
	return r.mHit;
}


inline float distance(b2Fixture* f0, const b2Transform& t0, b2Fixture* f1, const b2Transform& t1) 
{
	b2DistanceInput input;
	input.proxyA.Set(f0->GetShape(), 0);
	input.proxyB.Set(f1->GetShape(), 0);
	input.transformA = t0;
	input.transformB = t1;

	b2SimplexCache cache;
	cache.count = 0;

	b2DistanceOutput output;
	b2Distance(&output, &cache, &input);
	return output.distance;
}


float distance(b2Fixture* fixture, const QiVec2& pp, QiVec2& normal)
{
	BodyConvex* bc = ((BodyConvex*)fixture->GetUserData());
	if (bc)
	{
		b2DistanceInput input;
		b2CircleShape cs;
		cs.m_p = (b2Vec2&)pp;
		input.proxyA.Set(fixture->GetShape(), 0);
		input.proxyB.Set(&cs, 0);
		input.transformA.SetIdentity();
		input.transformB.SetIdentity();

		b2SimplexCache cache;
		cache.count = 0;

		b2DistanceOutput output;
		b2Distance(&output, &cache, &input);

		if (output.distance > 0)
		{
			normal = ((QiVec2&)output.pointB)-((QiVec2&)output.pointA);
			normal = normalize(normal);
			return output.distance;
		}
	}
	normal.set(0,1);
	return 0.0f;
}


float Level::getDistance(Body* b0, Body* b1) const
{
	float minDist = QI_FLOAT_MAX;
	if (!b0->mPhysBody || !b1->mPhysBody)
		return 0.0f;
		
	b2Fixture* f0 = b0->mPhysBody->GetFixtureList();
	while(f0)
	{
		b2Fixture* f1 = b1->mPhysBody->GetFixtureList();
		while(f1)
		{
			float d = distance(f0, b0->mPhysBody->GetTransform(), f1, b1->mPhysBody->GetTransform());
			minDist = QiMin(minDist, d);
			f1 = f1->GetNext();
		}
		f0 = f0->GetNext();
	}
	return minDist;
};


float Level::getDistance(Body* b0, const QiVec2& point) const
{
	float minDist = QI_FLOAT_MAX;
	if (!b0->mPhysBody)
		return 0.0f;

	b2Fixture* f0 = b0->mPhysBody->GetFixtureList();
	while(f0)
	{
		if (f0->GetUserData())
		{
			QiVec2 n;
			BodyConvex* bc = ((BodyConvex*)f0->GetUserData());
			QiVec2 localPos = bc->body->mTransform.toLocalPoint(point);
			float d = distance(f0, localPos, n);
			minDist = QiMin(minDist, d);
		}
		f0 = f0->GetNext();
	}
	return minDist;
};


class Collector : public b2QueryCallback
{
public:
	Collector(QiArray<Body*>& bodies, int mask) : mMask(mask), mBodies(bodies) {}
	virtual bool ReportFixture(b2Fixture* fixture)
	{
		if ((mMask & fixture->GetFilterData().categoryBits) != 0)
			mBodies.add((Body*)fixture->GetBody()->GetUserData());
		return true;
	}
	uint16 mMask;
	QiArray<Body*>& mBodies;
};


void Level::collectBodies(const QiVec2& boundsMin, const QiVec2& boundsMax, int mask, QiArray<Body*>& bodies) const
{
	Collector collector(bodies, mask);
	b2AABB aabb;
	aabb.lowerBound = vec(boundsMin);
	aabb.upperBound = vec(boundsMax);
	mPhysWorld->QueryAABB(&collector, aabb);
}


void Level::collectBodies(const QiVec2& boundsMin, const QiVec2& boundsMax, QiArray<Body*>& bodies) const
{
	for(int i=0; i<mBodies.getCount(); i++)
	{
		Body* b = mBodies[i];
		if (!(boundsMin.x > b->mBoundsMax.x || boundsMax.x < b->mBoundsMin.x || boundsMin.y > b->mBoundsMax.y || boundsMax.y < b->mBoundsMin.y))
			bodies.add(b);
	}
}


void Level::loadDecals(const QiString& file, bool foliage)
{
	QiMemoryStream<256> tmp;
	mResMan->load(file, tmp);

	QiXmlParser xml;
	if (!xml.parse(tmp, tmp.getSize()))
		return;

	if (xml.getName() == "decals")
	{
		float w = 1.0f;
		float h = 1.0f;
		if (foliage)
		{
			mFoliageTexture = mResMan->acquireTexture(xml.getAttribute("texture"));
			w = (float)mFoliageTexture.getTexture()->getWidth();
			h = (float)mFoliageTexture.getTexture()->getHeight();
		}
		else
		{
			mDecalTexture = mResMan->acquireTexture(xml.getAttribute("texture"));
			w = (float)mDecalTexture.getTexture()->getWidth();
			h = (float)mDecalTexture.getTexture()->getHeight();
		}
		xml.enter();
		while(xml.isValid())
		{
			if (xml.getName() == "decal")
			{
				DecalInfo info;
				info.name = xml.getAttribute("name");
				QiString c = xml.getAttribute("coords");
				info.texMin.set(c.getWord(0).toFloat()/(w-1.0f), c.getWord(1).toFloat()/(h-1.0f));
				info.texMax.set(c.getWord(2).toFloat()/(w-1.0f), c.getWord(3).toFloat()/(h-1.0f));
				mDecals.add(info);
			}
			xml.next();
		}
	}	
}

bool Level::findDecal(const QiString& name, DecalInfo& info)
{
	for(int i=0; i<mDecals.getCount(); i++)
	{
		if (mDecals[i].name == name)
		{
			info = mDecals[i];
			return true;
		}
	}
	return false;
}


void Level::loadTemplates()
{
	mTemplateNames.clear();
	for(int i=0; i<mTemplateProperties.getCount(); i++)
		QI_DELETE(mTemplateProperties[i]);
	mTemplateProperties.clear();

	QiMemoryStream<256> tmp;
	if (!mResMan->load("templates.xml", tmp))
		return;
	QiXmlParser xml(tmp, tmp.getSize());
	if (xml.getName() == "templates")
	{
		xml.enter();
		while(xml.isValid())
		{
			if (xml.getName() == "template")
			{
				QiString name = xml.getAttribute("name");
				xml.enter();
				PropertyBag* p = QI_NEW PropertyBag();
				p->readXml(xml, true);
				mTemplateNames.add(name);
				mTemplateProperties.add(p);
				xml.leave();
			}
			xml.next();
		}
	}
}


void Level::applyTemplate(const QiString& name, PropertyBag* properties)
{
	for(int i=0; i<mTemplateNames.getCount(); i++)
	{
		if (mTemplateNames[i] == name)
		{
			properties->inheritFrom(*mTemplateProperties[i]);
			return;
		}
	}
}


Wire* Level::findWire(const QiVec2& point, const QiVec2& direction, QiVec2& result, QiVec2& normal, float& dist, float* t) const
{
	for(int i=0; i<mEntities.getCount(); i++)
	{
		if (mEntities[i]->getType() == Entity::WIRE)
		{
			Wire* w = (Wire*) mEntities[i];
			if (w->intersects(point, direction, result, normal, dist, t))
				return w;
		}
	}
	return NULL;
}


Body* Level::findRail(const QiVec2& point, const QiVec2& direction, QiVec2& result, QiVec2& normal, float& dist) const
{
	Body* body = NULL;
	raycast(point+direction, point-direction*0.5f, 256, -1, &result, &normal, &body);
	if (body)
	{
		QiString curve = body->mProperties.getString("curve");
		if (curve.getWord(0) == "2" && body->mControlPoints.getCount() >= 2)
		{
			float thickness = curve.getWord(1).toFloat() * 2.0f;
			QiVec2 p0 = body->mTransform.toParentPoint(body->mControlPoints.first().mPoint);
			QiVec2 n0 = normalize(body->mTransform.toParentPoint(body->mControlPoints.first().mHandle0) - p0);
			QiVec2 p1 = body->mTransform.toParentPoint(body->mControlPoints.last().mPoint);
			QiVec2 n1 = normalize(body->mTransform.toParentPoint(body->mControlPoints.last().mHandle1) - p1);
			if (length(result-p0) < thickness && dot(normal, n0) > 0.5f) 
				body = NULL;
			if (length(result-p1) < thickness && dot(normal, n1) > 0.5f) 
				body = NULL;
		}
	}
	if (body)
		dist = dot(normal, result-point);
	return body;
}


void Level::fillBatches()
{
	mDecalBatch->mState.texture[0] = mDecalTexture.getTexture();
	mFoliageBatch->mState.texture[0] = mFoliageTexture.getTexture();

	for(int i=0; i<mEntities.getCount(); i++)
	{
		if (mEntities[i]->getType() == Entity::DECAL)
		{
			Decal* d = (Decal*)mEntities[i];
			d->addToBatch(mDecalBatch);
		}
		if (mEntities[i]->getType() == Entity::BODY)
		{
			Body* b = (Body*)mEntities[i];
			b->computeShadow(mShadowBatch);
			b->addFoliage(mFoliageBatch);
		}
	}

	mDecalBatch->finish();
	mShadowBatch->finish();
	mFoliageBatch->finish();
}


void Level::clearBatches()
{
	mDecalBatch->clear();
	mShadowBatch->clear();
	mFoliageBatch->clear();

	mDecalBatch->mState.texture[0] = NULL;
	mFoliageBatch->mState.texture[0] = NULL;
}



void Level::setTimeScale(float scale)
{
	float prop = mProperties.getFloat("timescale");
	if (prop > 1.0f && scale < 0.5f)
		prop = 1.0f;

	gGame->mLevel->mTimeScale = scale;
	gGame->mTimeStep = 0.01667f * scale * prop;
}

