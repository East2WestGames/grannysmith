/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "joint.h"
#include "level.h"
#include "game.h"
#include "body.h"
#include "Box2D.h"

Joint::Joint(Level* level) : Entity(level, JOINT), mBodyA(NULL), mBodyB(NULL), mPhysJoint(NULL), mEnabled(true)
{
	mProperties.add("collideConnected", "0");
	mProperties.add("fixed", "0");
	mProperties.add("z", "0.0");
}


Joint::~Joint()
{
}


void Joint::onStart()
{
	if (mPhysJoint)
		onStop();

	QiArrayInplace<Body*, 2> bodies;
	mLevel->findBodies(mTransform.pos, bodies);
	if (bodies.getCount() == 1)
	{
		mBodyA = bodies[0];
		mBodyB = NULL;
		if (mBodyA->mPhysBody)
		{
			if (mProperties.getBool("fixed"))
			{
				b2WeldJointDef j;
				j.Initialize(mBodyA->mPhysBody, mLevel->mPhysGround, vec(mTransform.pos));
				mPhysJoint = mLevel->mPhysWorld->CreateJoint(&j);
			}
			else
			{
				b2RevoluteJointDef j;
				j.Initialize(mBodyA->mPhysBody, mLevel->mPhysGround, vec(mTransform.pos));
				j.collideConnected = mProperties.getBool("collideConnected");
				mPhysJoint = mLevel->mPhysWorld->CreateJoint(&j);
			}
		}
		return;
	}
	else if (bodies.getCount() > 1)
	{
		float z = mProperties.getFloat("z");
		Body* a=bodies[0];
		Body* b=bodies[1];
		if (QiAbs(z-b->mProperties.getFloat("z")) < QiAbs(z-a->mProperties.getFloat("z")))
			QiSwap(b, a);

		for(int i=2; i<bodies.getCount(); i++)
		{
			if (QiAbs(z-bodies[i]->mProperties.getFloat("z")) < QiAbs(z-a->mProperties.getFloat("z")))
			{
				if (QiAbs(z-a->mProperties.getFloat("z")) < QiAbs(z-b->mProperties.getFloat("z")))
					b = a;
				a = bodies[i];
			}
			else if (QiAbs(z-bodies[i]->mProperties.getFloat("z")) < QiAbs(z-b->mProperties.getFloat("z")))
				b = bodies[i];
		}

		mBodyA = a;
		mBodyB = b;
		b2Body* a2 = NULL;
		b2Body* b2 = NULL;
		if (mBodyA->mPhysBody)
			a2 = mBodyA->mPhysBody;
		if (mBodyB->mPhysBody)
			b2 = mBodyB->mPhysBody;
		if (a2 || b2)
		{
			if (!a2)
				a2 = mLevel->mPhysGround;
			if (!b2)
				b2 = mLevel->mPhysGround;

			if (mProperties.getBool("fixed"))
			{
				b2WeldJointDef j;
				j.Initialize(a2, b2, vec(mTransform.pos));
				mPhysJoint = mLevel->mPhysWorld->CreateJoint(&j);
			}
			else
			{
				b2RevoluteJointDef j;
				j.Initialize(a2, b2, vec(mTransform.pos));
				j.collideConnected = mProperties.getBool("collideConnected");
				mPhysJoint = mLevel->mPhysWorld->CreateJoint(&j);
			}
		}
		return;
	}
	mPhysJoint = NULL;
}


void Joint::onStop()
{
	if (mPhysJoint)
	{
		mLevel->mPhysWorld->DestroyJoint(mPhysJoint);
		mPhysJoint = NULL;
	}
}


void Joint::onReset()
{
	mEnabled = true;
	onStart();
}


void Joint::update()
{
}


void Joint::breakJoint()
{
	if (mEnabled)
		onStop();
	mEnabled = false;
}


