/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "Box2D/common/b2Polygon.h"
#include "decompose.h"

void Decompose(std::vector<QiVec2> inVerts, std::vector<std::vector<QiVec2> >& outVerts)
{
	b2Polygon p;
	p.nVertices = inVerts.size();
	p.x = new float[inVerts.size()];
	p.y = new float[inVerts.size()];
	for(unsigned int i=0; i<inVerts.size(); i++)
	{
		p.x[i] = inVerts[i].x;
		p.y[i] = inVerts[i].y;
	}
	static b2Polygon results[1024];
	int n=DecomposeConvex(&p, results, 1024);
	for(int i=0; i<n; i++)
	{
		std::vector<QiVec2> obj;
		for(int j=0; j<results[i].nVertices; j++)
			obj.push_back(QiVec2(results[i].x[j], results[i].y[j]));
		outVerts.push_back(obj);
	}
	delete [] p.x;
	delete [] p.y;
	p.x = NULL;
	p.y = NULL;
}

