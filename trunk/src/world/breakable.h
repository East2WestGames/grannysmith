/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "entity.h"
#include "resman.h"

class Breakable : public Entity
{
public:
	Breakable(class Level* level);
	~Breakable();

	void clear();
	virtual void loadProperties();
	virtual void update();
	virtual void updateAsync();
	virtual void setTransform(const QiTransform2& transform);
	virtual void render();
	virtual void renderTransparent();
	void draw();
	virtual void onReset();
    void syncTransforms();

	void copySplitVerts(class Fragment* from, class Fragment* to, const QiVec2& point, const QiVec2& normal);
	void split(class Fragment* f, Fragment** f0, class Fragment** f1, const QiVec2& normal);
	void split(class Fragment* f, int depth, const QiVec2& pos, const QiVec2& vel, float minArea=0.2f);
	void addParticles(class Fragment* f);

	bool crosses(const QiVec2& point, const QiVec2& vel, float radius);
	void breakUp(const QiVec2& pos, const QiVec2& vel, float minArea=0.2f);

	void generateContacts();

	QiVertexFormat mFmt;
	QiVertexBuffer mVb;
	QiIndexBuffer mIb;

	QiArray<class Fragment*> mFragments;
	Resource mTexture;
	class SoundBank* mSound;
	int mTdSpace;
	int mType;
	float mThickness;
	float mHeight;

	class ParticleSystem* mParticles;
	bool mSmoke;
	int mBreak;
	bool mBreakFromSide;
};


