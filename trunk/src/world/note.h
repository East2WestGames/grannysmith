/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "resman.h"
#include "entity.h"

class Note : public Entity
{
public:
	Note(class Level* level);

	virtual void renderTransparent();

private:
	Resource mTexture;
};

