/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "propertybag.h"
#include "entity.h"
#include "resman.h"

class Decal : public Entity
{
public:
	Decal(class Level* level);
	~Decal();

	virtual void loadProperties();
	virtual void renderTransparent();

	void updateMatrix();
	void addToBatch(class Batch* batch);

	QiVec2 mTexMin;
	QiVec2 mTexMax;
	QiMatrix4 mMatrix;
};

