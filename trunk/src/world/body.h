/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "entity.h"
#include "propertybag.h"
#include "resman.h"

const int MAX_CONVEX_VERTS = 32;

class Polygon2D
{
public:
	QiArrayInplace<QiVec2, 8> mPoints;

	bool contains(const QiVec2& point) const
	{
		if (mPoints.getCount() == 0)
			return false;
		float ang = 0.0f;
		QiVec2 vref = normalize(mPoints[0]-point);
		int c = mPoints.getCount();
		for(int i=1; i<=c; i++)
		{
			QiVec2 v = normalize(mPoints[(i%c)]-point);
			float a = QiACos(dot(v, vref));
			if (cross(v, vref) < 0.0f)
				ang += a;
			else
				ang -= a;
			vref = v;
		}
		return ang > 3.0f;
	}
};


class ControlPoint
{
public:
	QiVec2 mPoint;
	QiVec2 mHandle0;
	QiVec2 mHandle1;
};


struct BodyConvex 
{
	BodyConvex(class Body* body);
	~BodyConvex();

	int count;
	int last;
	QiVec2 verts[MAX_CONVEX_VERTS];
	QiVec2 normals[MAX_CONVEX_VERTS];
	bool internal[MAX_CONVEX_VERTS];
	float d[MAX_CONVEX_VERTS];
	QiVec2 worldNormals[MAX_CONVEX_VERTS];
	float worldD[MAX_CONVEX_VERTS];
	class Body* body;

	QiVec3 mTdVerts[MAX_CONVEX_VERTS*2];
	int mTdAabb;
};


class Body : public Entity
{
public:
	enum Surface
	{
		GENERIC,
		GRASS,
		MUD,
		WATER,
		WOOD,
		METAL,
		OIL,
		GRAVEL,
		ROCK
	};

	Body(class Level* level);
	~Body();

	void update();

	void render(bool transparent);
	virtual void render();
	virtual void renderTransparent();
	virtual void setTransform(const class QiTransform2& t);
	virtual bool contains(const class QiVec2& point) const { return Entity::contains(point) || mPolygon.contains(mTransform.toLocalPoint(point)); }
	virtual void localTransform(const class QiTransform2& d);
	virtual void loadProperties();
	virtual void onStart();
	virtual void onReset();
	virtual void init();
	
	QiVec2 getVelocity() const;

	void updateGeometry();
	void updatePhysics();
	void updateMesh();
	void computeShadow(class Batch* batch);
	void computeBounds();
	
	void makeCurve();
	void generatePolygon();

	Polygon2D mPolygon;
	QiArray<ControlPoint> mControlPoints;
	QiArray<int> mVertexControlPoint;
	QiArrayInplace<BodyConvex*, 2> mConvexes;

	class b2Body* mPhysBody;
	QiTransform3 mTransform3;

	QiVec2 mMoveTarget;
	float mMoveStrength;
	float mMoveMaxSpeed;
	float mRotateTarget;
	float mRotateStrength;
	float mRotateMaxSpeed;
	class b2MotionJoint* mMotionJoint;

	QiVertexFormat mVbFormat;
	QiVertexBuffer mVb;
	QiIndexBuffer mIb;

	QiVertexFormat mShadowFmt;
	QiVertexBuffer mShadowVb;
	QiIndexBuffer mShadowIb;

	Resource mTexture[2];
	QiVec2 mTexScale[2];
	QiVec2 mTexOffset[2];
	float mDepth;
	QiQuat mRot;
	bool mWater;
	int mDynamic;
	float mAwakeTimer;
	bool mHidden;

	float mBoundsMinZ;
	float mBoundsMaxZ;

	bool mSlip;
	bool mNoFx;
	bool mRoll;

	void addFoliage(class Batch* batch);
	
	Surface mSurface;
};

