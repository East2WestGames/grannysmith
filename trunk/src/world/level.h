/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "propertybag.h"
#include "entity.h"
#include "resman.h"

class Entity;
class Level
{
public:
	Level();
	~Level();

	void init();
	void clear();

	void start();
	void stop();
	void reset();

	void update();
	void updateAsync();
    void syncBreakables();
	void draw();

	void win();
	void loose();
	void pickUpApple(const QiVec2& pos);
	void pickUpCoin(const QiVec2& pos);

	void load(QiInputStream& stream, int size);
	Entity* loadEntity(QiXmlParser& xml);

	void save(QiOutputStream& stream);
	void saveEntity(Entity* entity, QiXmlWriter& xml);

	void fillBatches();
	void clearBatches();

	void setTimeScale(float scale);

	Entity* createEntity(Entity::Type type);
	Entity::Type getEntityType(const QiString& name);
	QiString getEntityTypeName(Entity::Type type);
	Entity* findEntity(const QiString& name) const;

	void destroy(class Entity* entity);
	Entity* findEntity(const QiVec2& point) const;
	bool findBodies(const QiVec2& point, QiArray<class Body*>& arr) const;
	class Handle* findHandle(const QiVec2& point, int playerId) const;
	class Wire* findWire(const QiVec2& point, const QiVec2& direction, QiVec2& result, QiVec2& normal, float& dist, float* t=NULL) const;
	class Body* findRail(const QiVec2& point, const QiVec2& direction, QiVec2& result, QiVec2& normal, float& dist) const;
	
	class b2World* getPhysWorld() { return mPhysWorld; }
	class b2Body* getPhysGround() { return mPhysGround; }
	
	PropertyBag mProperties;
	
	QiArray<class Entity*> mEntities;
	QiArray<class Body*> mBodies;
	QiArray<class Sensor*> mSensors;
	QiArray<class Entity*> mNamedEntities;

	class ResMan* mResMan;
	class Script* mScript;
	Resource mBackground;

	class b2World* mPhysWorld;
	class b2Body* mPhysGround;
	class Dude* mDude;
	class Dude* mBadGuy;

	float mSimTime;
	float mRespawnTimer;

	bool raycast(const QiVec2& start, const QiVec2& end, int mask, int category, QiVec2* point, QiVec2* normal, Body** body) const;
	float getDistance(Body* b0, Body* b1) const;
	float getDistance(Body* b0, const QiVec2& point) const;
	void collectBodies(const QiVec2& boundsMin, const QiVec2& boundsMax, int mask, QiArray<Body*>& bodies) const;
	void collectBodies(const QiVec2& boundsMin, const QiVec2& boundsMax, QiArray<Body*>& bodies) const;

	struct DecalInfo
	{
		QiString name;
		QiString texture;
		QiVec2 texMin;
		QiVec2 texMax;
	};

	QiArray<DecalInfo> mDecals;
	void loadDecals(const QiString& file, bool foliage);
	bool findDecal(const QiString& name, DecalInfo& info);

	QiMemoryStream<256> mBadGuyMotion;
	QiMemoryStream<256> mPlayerMotion;
	bool mReplay;

	void loadTemplates();
	void applyTemplate(const QiString& name, PropertyBag* properties);
	QiArray<QiString> mTemplateNames;
	QiArray<PropertyBag*> mTemplateProperties;

	bool mTrigReset;
	bool mOwnApple;
	int mCoins;
	int mApples;
	int mScore;
	int mTdSolver;
	int mTdContext;
	int mTdSpace;

	int mDrawCallCount;

	Resource mDecalTexture;
	Resource mFoliageTexture;
	class Batch* mDecalBatch;
	class Batch* mShadowBatch;
	class Batch* mFoliageBatch;

	float mStartX;
	float mGoalX;
	float mFrame;

	float mTimeScale;

	class Throwable* mThrowables;
	float mResetTimer;

	bool mLoaded;
	
	QiVertexFormat mParticleFmt;
	QiVertexBuffer mParticleVb;
	QiIndexBuffer mParticleIb;
	Resource mParticleShader;
	Resource mParticleTexture;
	
	class ParticleSystem* mEffects;
	class ParticleSystem* mFanParticles;
	
	Resource mFanSound;
	class QiAudioChannel* mFanChannel;
	Resource mFanSwooshSound;
	
	float mDieTimer;
	QiVec2 mDiePos;
	
	bool mCutScene;
	int mBreakableFragments;
	bool mUpSideDown;
	float mGravity;
};

