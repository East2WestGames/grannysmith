/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "entity.h"

class Handle : public Entity
{
public:
	Handle(class Level* level);

	virtual void onStart();
	QiVec2 getWorldPos();

	class Body* mBody;
	QiVec2 mPos;
};

