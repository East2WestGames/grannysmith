/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "decal.h"
#include "level.h"
#include "game.h"
#include "gfx.h"
#include "profiling.h"
#include "batch.h"


Decal::Decal(Level* level) : Entity(level, DECAL)
{
	mProperties.add("decal", "");
	mProperties.add("xscale", "1.0");
	mProperties.add("yscale", "1.0");
	mProperties.add("z", "0.0");
	mProperties.add("color", "1 1 1 1");
	mProperties.add("extrarot", "0 0 0");
}


Decal::~Decal()
{
}


void Decal::loadProperties()
{
	Entity::loadProperties();

	QiString t = mProperties.getString("decal");

	Level::DecalInfo info;
	if (t!="" && mLevel->findDecal(t, info))
	{
		QiString fileName = info.texture;	
		mTexMin = info.texMin;
		mTexMax = info.texMax;
	}

	mZ = mProperties.getFloat("z");

	updateMatrix();
}


void Decal::updateMatrix()
{
	if (mLevel->mDecalTexture.getTexture())
	{
		float w = 2.56f*mProperties.getFloat("xscale");
		float h = 2.56f*mProperties.getFloat("yscale");
		QiVec2 p0 = mTransform.toParentPoint(QiVec2(-w,-h));
		QiVec2 p1 = mTransform.toParentPoint(QiVec2(w,-h));
		QiVec2 p2 = mTransform.toParentPoint(QiVec2(w,h));
		QiVec2 p3 = mTransform.toParentPoint(QiVec2(-w,h));
		mBoundsMin.x = QiMin(QiMin(p0.x, p1.x), QiMin(p2.x, p3.x));
		mBoundsMin.y = QiMin(QiMin(p0.y, p1.y), QiMin(p2.y, p3.y));
		mBoundsMax.x = QiMax(QiMax(p0.x, p1.x), QiMax(p2.x, p3.x));
		mBoundsMax.y = QiMax(QiMax(p0.y, p1.y), QiMax(p2.y, p3.y));

		float rx = mProperties.getString("extrarot").getWord(0).toFloat();
		float ry = mProperties.getString("extrarot").getWord(1).toFloat();
		float rz = mProperties.getString("extrarot").getWord(2).toFloat();
		QiQuat rot = QiQuat(QiVec3::X, rx) * QiQuat(QiVec3::Y, ry) * QiQuat(QiVec3::Z, rz);

		QiTransform3 xf;
		xf.pos.set(mTransform.pos.x, mTransform.pos.y, mZ);
		xf.rot = QiQuat(QiVec3::Z, mTransform.rot);
		QiMatrix4 mat(xf);
		QiMatrix4 scale;
		scale.m[0] = w;
		scale.m[5] = -h;
		QiMatrix4 trans(QiVec3(-w*0.5f, h*0.5f, 0.0f));
		mMatrix = mat*QiMatrix4(rot)*trans*scale;
	}
}


void Decal::addToBatch(class Batch* batch)
{
	QiColor col = mProperties.getColor("color");
	QiVec3 p0 = mMatrix.transformPoint(QiVec3(0, 0, 0));
	QiVec3 p1 = mMatrix.transformPoint(QiVec3(1, 0, 0));
	QiVec3 p2 = mMatrix.transformPoint(QiVec3(1, 1, 0));
	QiVec3 p3 = mMatrix.transformPoint(QiVec3(0, 1, 0));
	int c = batch->mVb.getCount();
	batch->mVb.vertex();
	batch->mVb.addFast(p0);
	batch->mVb.add(QiVec2(mTexMin.x, mTexMin.y));
	batch->mVb.add(col.r/2, col.g/2, col.b/2, col.a/2);
	batch->mVb.vertex();
	batch->mVb.addFast(p1);
	batch->mVb.add(QiVec2(mTexMax.x, mTexMin.y));
	batch->mVb.add(col.r/2, col.g/2, col.b/2, col.a/2);
	batch->mVb.vertex();
	batch->mVb.addFast(p2);
	batch->mVb.add(QiVec2(mTexMax.x, mTexMax.y));
	batch->mVb.add(col.r/2, col.g/2, col.b/2, col.a/2);
	batch->mVb.vertex();
	batch->mVb.addFast(p3);
	batch->mVb.add(QiVec2(mTexMin.x, mTexMax.y));
	batch->mVb.add(col.r/2, col.g/2, col.b/2, col.a/2);
	batch->add(c+0, c+1, c+2, p0, p1, p2);
	batch->add(c+0, c+2, c+3, p0, p2, p3);
}



void Decal::renderTransparent()
{
	if (gGame->getState() == Game::PLAY)
		return;

	QiColor col = mProperties.getColor("color");
	if (col.a <= 0.0f) 
		return;

	QiRenderState state;
	state.color = col;
	state.shader = gGame->mGfx->m2DTexShader.getShader();
	state.blendMode = state.BLEND;
	state.texture[0] = mLevel->mDecalTexture.getTexture();
	state.depthTest = true;
	state.depthMask = false;

	updateMatrix();
	state.texOffset[0] = mTexMin;
	state.texScale[0] = mTexMax-mTexMin;
	gGame->mRenderer->setState(state);
	gGame->mGfx->drawRectangle(mMatrix);
}

