/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "sensor.h"
#include "level.h"
#include "game.h"

Sensor::Sensor(Level* level) : Entity(level, SENSOR), mIsBox(false), mUsed(0), mEnabled(true), mIsForce(false)
{
	mProperties.add("type", "circle");
	mProperties.add("size", "1.0");
	mProperties.add("action", "");
}


void Sensor::onStart()
{
	mSize = mProperties.getVec2("size");
	if (mProperties.getString("type") == "box")
		mIsBox = true;
	if (mProperties.getString("action").startsWith("sound"))
		mResource = mLevel->mResMan->acquireSound(mProperties.getString("action").getWord(1));
	mIsForce = mProperties.getString("action").startsWith("force");
}


void Sensor::onReset()
{
	mUsed = 0;
}


bool Sensor::testPoint(const QiVec2& point) const
{
	if (!mEnabled)
		return false;

	if (mIsBox)
	{
		QiVec2 localPoint = mTransform.toLocalPoint(point);
		return	localPoint.x < mSize.x*0.5f && 
				localPoint.x > -mSize.x*0.5f && 
				localPoint.y < mSize.y*0.5f && 
				localPoint.y > -mSize.y*0.5f;
	}
	else
		return lengthSquared(point-mTransform.pos) < mSize.x * mSize.x;
}

