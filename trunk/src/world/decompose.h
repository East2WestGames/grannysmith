/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "qi_base.h"
#include "qi_math.h"
#include <vector>

void Decompose( std::vector<QiVec2> inVerts, std::vector< std::vector<QiVec2> >& outVerts );

