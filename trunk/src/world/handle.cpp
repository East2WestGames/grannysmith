/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "handle.h"
#include "level.h"
#include "body.h"

Handle::Handle(Level* level) : Entity(level, HANDLE), mBody(NULL)
{
	mProperties.add("mask", "3");
	mProperties.add("z", "0");
	mProperties.add("followRotation", "0");
}

void Handle::onStart()
{
	QiArray<Body*> arr;
	if (mLevel->findBodies(mTransform.pos, arr))
	{
		Body* best = NULL;
		float z = mProperties.getFloat("z");
		for(int i=0; i<arr.getCount(); i++)
		{
			if (best==NULL || QiAbs(z-arr[i]->mProperties.getFloat("z")) < QiAbs(z-best->mProperties.getFloat("z")))
				best = arr[i];
		}

		if (best->mPhysBody)
		{
			mBody = best;
			mPos = mBody->mTransform.toLocalPoint(mTransform.pos);
		}
		else
		{
			mBody = NULL;
			mPos = mTransform.pos;
		}
	}
	else
	{
		mBody = NULL;
		mPos = mTransform.pos;
	}
}


QiVec2 Handle::getWorldPos()
{
	if (mBody)
		return mBody->mTransform.toParentPoint(mPos);
	else
		return mPos;
}

