/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "wire.h"
#include "level.h"
#include "game.h"
#include "gfx.h"
#include "audio.h"
#include "dude.h"
#include "profiling.h"

static const int POINT_COUNT = 128;
static const float THICKNESS = 0.03f;

Wire::Wire(Level* level) : Entity(level, WIRE), mAttachCount(0), mLastAttachCount(0), mAttachCount2(0),  mHangAmount(0.0f)
{
	mProperties.add("point0", "-1 0");
	mProperties.add("point1", "1 0");

	mFmt.addField("aPosition", QiVertexFormat::FLOAT32, 2);
	mFmt.addField("aNormal", QiVertexFormat::INT8, 2);
	mVb.init(mFmt, POINT_COUNT*4);
	mIb.init(POINT_COUNT*6*2);

	mFlap[0] = mFlap[1] = mFlap[2] = 0.0f;
}

void Wire::setTransform(const QiTransform2& transform)
{
	Entity::setTransform(transform);
	mPoint0 = mTransform.toParentPoint(mProperties.getVec2("point0"));
	mPoint1 = mTransform.toParentPoint(mProperties.getVec2("point1"));
	mBoundsMin.set(QiMin(mPoint0.x, mPoint1.x)-1.0f, QiMin(mPoint0.y, mPoint1.y)-1.0f);
	mBoundsMax.set(QiMax(mPoint0.x, mPoint1.x)+1.0f, QiMax(mPoint0.y, mPoint1.y)+1.0f);
}


void Wire::update()
{
	if (mAttachCount == 0)
		mHangAmount = QiMin(1.0f, mHangAmount + 0.05f);

	if (mAttachCount > 0 && mFlap[0] < 0.1f && QiRnd(0.0f, 1.0f) < 0.1f)
		mFlap[0] = QiRnd(0.1f, 0.2f);
	mFlap[0] = QiMax(0.0f, mFlap[0] * 0.95f - 0.005f);    
    
	mAttachCount = mAttachCount2;
    mAttachCount2 = 0;
}


void Wire::buildWire(const QiVec2& p0, const QiVec2& p1, QiArray<QiVec2>& points, int pointCount)
{
	QiVec2 up = normalize(perpendicular(p1-p0));
	float len = length(p1-p0);
	float hangAmount = mHangAmount * QiMin(len * 0.1f, 1.0f) * (1.0f + QiSin(7.0f*mLevel->mSimTime)*0.1f);
	for(int i=0; i<pointCount; i++)
	{
		float t = i/(float)(pointCount-1);
		float l = len*t;
		QiVec2 offset = up * (QiSin(5.0f*l)*0.3f + QiSin(2.0f*l + 50.0f*mLevel->mSimTime)*0.7f) * QiSin(t*QI_PI) * mFlap[0];
		offset -= up * (hangAmount * QiSin(t*QI_PI));
		points.add(p0+ (p1-p0) * t + offset);
	}
}


void Wire::renderTransparent()
{
    PROFILE_ZONE("Wire::renderTransparent");
    
	if (mAttachCount != mLastAttachCount)
	{
		if (mAttachCount < mLastAttachCount)
			mFlap[0] = 1.0f;
		else
			mFlap[0] = 0.3f;
		mLastAttachCount = mAttachCount;
	}

	QiArrayInplace<QiVec2, POINT_COUNT> rendPos;
	QiArrayInplace<QiVec2, POINT_COUNT> rendTan;

	int pointCount = QiClamp((int)(length(mPoint1-mPoint0) / 0.1f), 32, POINT_COUNT);

	if (mAttachCount == 0)
	{
		buildWire(mPoint0, mPoint1, rendPos, pointCount);
	}
	else if (mAttachCount == 1)
	{
		mHangAmount = 0.0f;
		buildWire(mPoint0, mAttach[0], rendPos, pointCount/2);
		buildWire(mAttach[0], mPoint1, rendPos, pointCount/2);
	}
	else if (mAttachCount == 2)
	{
		mHangAmount = 0.0f;
		QiVec2 t = normalize(mPoint1 - mPoint0);
		if (dot(t, mAttach[0]-mPoint0) > dot(t, mAttach[1]-mPoint0))
			QiSwap(mAttach[0], mAttach[1]);
		buildWire(mPoint0, mAttach[0], rendPos, pointCount/3);
		buildWire(mAttach[0], mAttach[1], rendPos, pointCount/3);
		buildWire(mAttach[1], mPoint1, rendPos, pointCount/3);
	}

	pointCount = rendPos.getCount();
	rendTan.redim(pointCount);
	rendTan[0] = normalizeCarmack(rendPos[1]-rendPos[0]);
	for(int i=1; i<pointCount-1; i++)
		rendTan[i] = normalizeCarmack(rendPos[i+1]-rendPos[i-1]);

	mVb.clear();
	for(int i=0; i<pointCount; i++)
	{
		QiVec2 norm = perpendicular(rendTan[i]);
		mVb.vertex();
		mVb.addFast(rendPos[i]-norm*THICKNESS);
		mVb.add(norm);
		mVb.vertex();
		mVb.addFast(rendPos[i]+norm*THICKNESS);
		mVb.add(-norm);
	}
	mIb.clear();
	for(int i=pointCount-4; i>=0; i--)
		mIb.quad(i*2+0, i*2+1, i*2+3, i*2+2);

	QiRenderState state;
	//state.color.set(0.8f, 0.8f, 0.8f, 1.0f);
	state.depthTest = true;
	state.shader = gGame->mGfx->mWireShader.getShader();

	gGame->mRenderer->setState(state);
	gGame->mRenderer->drawTriangles(&mVb, &mIb); 

	mAttachCount = 0;
}


void Wire::onReset()
{
	Entity::onReset();
}


bool Wire::intersects(const QiVec2& point, const QiVec2& direction, QiVec2& result, QiVec2& normal, float& dist, float* t) const
{
	QiVec2 tan = normalize(mPoint1 - mPoint0);
	QiVec2 n = perpendicular(tan);
	if (dot(n, direction) < 0.0f)
		n = -n;

	float q = dot(tan, point-mPoint0);
	if (q < 0.0f || q*q > lengthSquared(mPoint1-mPoint0))
		return false;

	if (t)
		*t = q/length(mPoint1-mPoint0);

	float d = -dot(n, point-mPoint0);
	if (d > -0.2f && d < dist)
	{
		normal = n;
		dist = d;
		return true;
	}
	return false;
}


void Wire::attach(const QiVec2& point)
{
	if (mAttachCount2 < 2)
		mAttach[mAttachCount2++] = point;
}

