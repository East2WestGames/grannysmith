/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "entity.h"
#include "resman.h"
#include "audio.h"

class Throwable: public Entity
{
public:
	Throwable(class Level* level);
	virtual void onReset();
	
	virtual void spawn(int type, const QiVec2& pos, const QiVec2& vel);

	virtual void update();
	virtual void renderTransparent();

	Resource mTexture;
	Resource mTextureBanana;
	Resource mTextureHelmet;
	Resource mTextureCoin;
	SoundBank mSound;

	QiArray<class Object*> mObjects;
};

