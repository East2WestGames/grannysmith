/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "propertybag.h"

extern const char* gEntityTypeNames[];

class Entity
{
public:
	enum Type
	{
		DUDE,
		BODY,
		JOINT,
		SENSOR,
		DECAL,
		HANDLE,
		WIRE,
		BREAKABLE,
		POWERUP,
		NOTE,
		THROWABLE,
		COUNT
	};

	Entity(class Level* level, Type type);
	virtual ~Entity();

	Type getType() { return mType; }

	virtual void init() {}
	virtual void update() {}
	virtual void updateAsync() {}
	virtual void render() {}
	virtual void renderTransparent() {}
	virtual void setTransform(const class QiTransform2& t);
	virtual bool contains(const class QiVec2& point) const;
	virtual void localTransform(const class QiTransform2& d) {}
	virtual void onStart() {}
	virtual void onStop() {}
	virtual void onReset();
	virtual void loadProperties();

	void mergeBounds(Entity* other);
	void invalidateBounds();

	class Level* mLevel;
	Type mType;
	Entity* mParent;
	QiTransform2 mTransform;
	PropertyBag mProperties;
	bool mVisible;

	float mZ;
	QiVec2 mBoundsMin;
	QiVec2 mBoundsMax;

	//For editing purposes only
	bool mEditorHidden;
	int mEditorGroup;
};

