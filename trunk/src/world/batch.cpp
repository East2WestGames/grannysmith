/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "batch.h"
#include "game.h"
#include "level.h"
#include "gfx.h"
#include "display.h"

class Chunk
{
public:
	Chunk()
	{
		mIb.init(32768);
	}

	QiIndexBuffer mIb;
	QiArray<float> mDepths;

	float mBoundsMinZ;
	float mBoundsMaxZ;
	QiVec2 mBoundsMin;
	QiVec2 mBoundsMax;
	
	QiVec2 mPos;
};


Batch::Batch()
{
	mState.blendMode = QiRenderState::BLEND;
	mState.depthMask = false;
	mState.depthTest = true;
}


Batch::~Batch()
{
	clear();
}


void Batch::init()
{
	mVb.init(mFmt, 32768);
	clear();
}


void Batch::clear()
{
	for(int i=0; i<mChunks.getCount(); i++)
		QI_DELETE(mChunks[i]);
	mChunks.clear();
	mVb.clear();
}


void Batch::add(int i0, int i1, int i2, const QiVec3& p0, const QiVec3& p1, const QiVec3& p2)
{
	Chunk* chunk = NULL;
	
	float maxDist = 100.0f;
	QiVec2 mid = ((p0+p1+p2)/3.0f).vec2();
	for(int i=0; i<mChunks.getCount(); i++)
	{
		if (lengthSquared(mChunks[i]->mPos-mid) < maxDist*maxDist)
		{
			chunk = mChunks[i];
			break;
		}			
	}
	
	if (!chunk)
	{
		chunk = QI_NEW Chunk();
		chunk->mPos = mid;
		chunk->mBoundsMin = mid;
		chunk->mBoundsMax = mid;
		chunk->mBoundsMinZ = 0.0f;
		chunk->mBoundsMaxZ = 0.0f;
		mChunks.add(chunk);
	}

	float d = (p0.z + p1.z + p2.z) / 3.0f;
	chunk->mDepths.add(d);
	chunk->mIb.triangle(i0, i1, i2);
	chunk->mBoundsMin.x = QiMin(chunk->mBoundsMin.x, QiMin(p0.x, p1.x, p2.x));
	chunk->mBoundsMin.y = QiMin(chunk->mBoundsMin.y, QiMin(p0.y, p1.y, p2.y));
	chunk->mBoundsMax.x = QiMax(chunk->mBoundsMax.x, QiMax(p0.x, p1.x, p2.x));
	chunk->mBoundsMax.y = QiMax(chunk->mBoundsMax.y, QiMax(p0.y, p1.y, p2.y));
	chunk->mBoundsMinZ = QiMin(chunk->mBoundsMinZ, QiMin(p0.z, p1.z, p2.z));
	chunk->mBoundsMaxZ = QiMax(chunk->mBoundsMaxZ, QiMax(p0.z, p1.z, p2.z));
}


void Batch::finish()
{
	for(int i=0; i<mChunks.getCount(); i++)
	{
		//TODO: Depth sort
		Chunk* c = mChunks[i];
		QI_PRINT("Chunk size: " + c->mIb.getCount()/3 + " triangles");
		c->mIb.makeIbo();
	}
	mVb.makeVbo();
}


void Batch::render(bool positive)
{
	if (mChunks.getCount() == 0)
		return;

	gGame->mRenderer->setState(mState);

	if (mState.shader == gGame->mGfx->mFoliageShader.getShader())
	{
		float t = gGame->mLevel->mProperties.getFloat("grassmovement");
		mState.shader->setUniform("uAngle", gGame->mLevel->mSimTime*5*t);
	}

	//Todo: Cull chunks

	if (!positive)
	{
		for(int i=0; i<mChunks.getCount(); i++)
		{
			Chunk* c = mChunks[i];
			if (gGame->mDisplay->isVisible(c->mBoundsMin, c->mBoundsMax, c->mBoundsMinZ) || gGame->mDisplay->isVisible(c->mBoundsMin, c->mBoundsMax, c->mBoundsMaxZ))
				gGame->mRenderer->drawTriangles(&mVb, &c->mIb);
		}
	}
}


