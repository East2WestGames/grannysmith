/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "cloth.h"
#include "dude.h"
#include "level.h"
#include "game.h"
#include "gfx.h"
#include "profiling.h"


Cloth::Cloth(Dude* dude, const QiString& type, const QiVec2& texMin, const QiVec2& texMax) : mDude(dude), mType(type)
{

	float strength = 0.9f;
	float diagStrength = 0.1f;

	if (mType == "skirt")
	{
		mWidth = 8;
		mHeight = 8;
		mPart = Dude::TORSO;
		mTransform.pos.set(0.0f, -0.19f);
		mSize.set(0.4f, 0.35f);
	}

	if (mType == "cape")
	{
		mWidth = 4;
		mHeight = 8;
		mPart = Dude::TORSO;
		mTransform.pos.set(-0.2f, 0.05f);
		mSize.set(0.2f, 0.25f);
		strength = 0.9f;
		diagStrength = 0.01f;
	}

	if (mType == "tie")
	{
		mWidth = 2;
		mHeight = 6;
		mPart = Dude::TORSO;
		mTransform.pos.set(0.1f, -0.03f);
		mSize.set(0.08f, 0.25f);
		strength = 0.9f;
		diagStrength = 0.01f;
	}

	if (mType == "jacket")
	{
		mWidth = 4;
		mHeight = 8;
		mPart = Dude::TORSO;
		mTransform.pos.set(-0.15f, 0.05f);
		mSize.set(0.25f, 0.35f);
		strength = 0.9f;
		diagStrength = 0.01f;
	}

	if (mType == "hair")
	{
		mWidth = 8;
		mHeight = 8;
		mPart = Dude::HEAD;
		mTransform.pos.set(-0.0f, 0.0f);
		mSize.set(0.3f, 0.3f);
		strength = 1.0f;
		diagStrength = 1.0f;
	}

	if (mType == "tail")
	{
		mWidth = 4;
		mHeight = 3;
		mPart = Dude::TORSO;
		mTransform.pos.set(-0.5f, -0.2f);
		mSize.set(0.4f, 0.3f);
		strength = 0.9f;
		diagStrength = 0.8f;
	}

	if (mType == "antenna1" || mType == "antenna2")
	{
		mWidth = 3;
		mHeight = 4;
		mPart = Dude::HEAD;
		if (mType == "antenna1")
		{
			mTransform.pos.set(-0.3f, 0.7f);
			mSize.set(0.15f, 0.5f);
		}
		else
		{
			mTransform.pos.set(-0.1f, 0.75f);
			mSize.set(0.15f, 0.4f);
		}
		strength = 0.9f;
		diagStrength = 0.8f;
	}

	if (mType == "tongue")
	{
		mWidth = 2;
		mHeight = 5;
		mPart = Dude::HEAD;
		mTransform.pos.set(-0.13f, -0.3f);
		mSize.set(0.1f, 0.15f);
		strength = 1.0f;
		diagStrength = 0.0f;
	}

	if (mType == "ear")
	{
		mWidth = 2;
		mHeight = 5;
		mPart = Dude::HEAD;
		mTransform.pos.set(-0.25f, -0.15f);
		mSize.set(0.1f, 0.3f);
		strength = 1.0f;
		diagStrength = 0.0f;
	}

	reshape();

	mIterationCount = 2;

	for(int y=0; y<mHeight; y++)
	{
		for(int x=0; x<mWidth; x++)
		{
			int i = y*mWidth+x;
			if (x<mWidth-1)
				addConstraint(i, i+1, strength);
			if (y<mHeight-1)
				addConstraint(i, i+mWidth, strength);
			if (x<mWidth-1 && y<mHeight-1)
				addConstraint(i, i+mWidth+1, diagStrength);
			if (x<mWidth-1 && y<mHeight-1)
				addConstraint(i+1, i+mWidth, diagStrength);
		}
	}

	QiVec2 size(1/(float)dude->mTexture.getTexture()->getWidth(), 1/(float)dude->mTexture.getTexture()->getHeight());
	mTexMin = multiplyPerElement(texMin, size);
	mTexMax = multiplyPerElement(texMax, size);

}


void Cloth::reshape()
{
	const QiTransform2& t = mDude->mParts[mPart].mTransform;
	mParticles.clear();
	mTexCoords.clear();
	Particle p;
	bool cape = (mType == "cape");
	bool tie = (mType == "tie");
	bool hair = (mType == "hair");
	bool ear = (mType == "ear");
	bool tongue = (mType == "tongue");
	bool tail = (mType == "tail");
	bool antenna = (mType == "antenna1" || mType == "antenna2");
	for(int y=0; y<mHeight; y++)
	{
		for(int x=0; x<mWidth; x++)
		{
			p.mPin = y/(float)mHeight;
			if (cape)
				p.mPin = (y==mHeight-1.0f ? 1.0f : 0);
			if (tie || ear || tongue)
				p.mPin = (y==mHeight-1.0f ? 1.0f : 0);
			if (hair)
			{
				p.mPin = (y / (float)mHeight * x/(float)mWidth ? 1.0f : 0);
				p.mPin = 0.15f*QiPow(p.mPin, 100.0f);
				//p.mPin = 0.0f;
				if (y < 6 && x >2)
					p.mPin = 1.0f;
			}
			if (tail)
				p.mPin = QiPow(x / float(mWidth-1), 3.0f) + 0.01f;
			if (antenna)
				p.mPin = QiPow(1.0f-y/float(mHeight-1), 2.0f) + 0.08f;
			p.mPinnedPos = mTransform.toParentPoint(QiVec2(2.0f*mSize.x*x/(float)(mWidth-1) - mSize.x, 2.0f*mSize.y*y/(float)(mHeight-1) - mSize.y));
			p.mPos = t.toParentPoint(p.mPinnedPos);
			p.mOldPos = p.mPos;
			mParticles.add(p);
			QiVec2 tc = QiVec2(x/(float)(mWidth-1), y/(float)(mHeight-1));
			tc.y = 1.0f - tc.y;
			tc = mTexMin + multiplyPerElement((mTexMax-mTexMin), tc);
			mTexCoords.add(tc);
		}
	}
}


void Cloth::addConstraint(QiUInt16 p0, QiUInt16 p1, float strength)
{
	float rl2 = lengthSquared(mParticles[p0].mPos - mParticles[p1].mPos);
	mConstraints.add(Constraint(p0, p1, rl2, strength));
}


void Cloth::update()
{
    PROFILE_ZONE("Cloth::update");

	bool skirt = (mType == "skirt");

	//Integrate
	QiVec2 g(0, -0.02f);
	QiVec2 v = QiVec2::random(length(mDude->getVelocity())*0.001f);
	for(int i=0; i<mParticles.getCount(); i++)
	{
		Particle& p = mParticles[i];
		QiVec2 temp = p.mPos;
		QiVec2 vel = p.mPos - p.mOldPos;
		vel *= 0.9f;
		//p.mPos += v;
		p.mPos += vel + g;
		p.mOldPos = temp;
	}

	QiTransform2& t = mDude->mParts[mPart].mTransform;
	for(int i=0; i<mIterationCount; i++)
	{
		//Solve constraints
		for(int i=0; i<mConstraints.getCount(); i++)
		{
			Constraint& c = mConstraints[i];
			Particle& p0 = mParticles[c.mParticle0];
			Particle& p1 = mParticles[c.mParticle1];
			float l = c.mRestLength2;
			QiVec2 delta = p1.mPos-p0.mPos;
			delta *= l/(dot(delta,delta)+l)-0.5f;
			delta *= c.mStrength;
			p0.mPos -= delta;
			p1.mPos += delta;
		}

		//Collision
		if (skirt)
		{
			//Check for knees
			for(int i=1; i<8; i++)
			{
				float r = 0.3f-QiAbs(4-i)*0.07f;
				QiVec2 fwd = mDude->mTransform.toParentVec(QiVec2(1,0));
				QiVec2 pos = mDude->mParts[Dude::FRONT_LEG_LOWER].mTransform.pos + fwd*r;
				Particle& p = mParticles[mWidth*i-1];
				float d = dot(fwd, p.mPos-pos);
				if (d < 0.0f)
					p.mPos -= fwd*d;

				pos = mDude->mParts[Dude::BACK_LEG_LOWER].mTransform.pos + fwd*r;
				d = dot(fwd, p.mPos-pos);
				if (d < 0.0f)
					p.mPos -= fwd*d;
			}
			//Dont show the panties dammit
			for(int i=1; i<mWidth; i++)
			{
				QiVec2 fwd = mDude->mTransform.toParentVec(QiVec2(0,-1));
				Particle& p = mParticles[mWidth*2+i];
				QiVec2 pos = t.toParentPoint(p.mPinnedPos);
				float d = dot(fwd, p.mPos-pos);
				if (d < 0.0f)
					p.mPos -= fwd*d;
			}
		}

		//Pin particles
		for(int i=0; i<mParticles.getCount(); i++)
		{
			Particle& p = mParticles[i];
			if (p.mPin > 0.0f)
			{
				QiVec2 pos = t.toParentPoint(p.mPinnedPos);
				p.mPos += (pos-p.mPos)*p.mPin;
			}
		}
	}
}


void Cloth::render(QiVertexBuffer& vb, QiIndexBuffer& ib)
{
	if (mDude->mLevel->mUpSideDown && mDude->mName != "granny")
		return;
	int v = vb.getCount();
	for(int i=0; i<mParticles.getCount(); i++)
	{
		vb.vertex();
		vb.addFast(mParticles[i].mPos.vec3());
		vb.addFast(mTexCoords[i]);
	}
	for(int y=0; y<mHeight-1; y++)
	{
		for(int x=0; x<mWidth-1; x++)
		{
			int i = v + y*mWidth+x;
			ib.quad(i, i+1, i+mWidth+1, i+mWidth);
		}
	}
}

