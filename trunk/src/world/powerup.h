/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "entity.h"
#include "resman.h"

class PowerUp : public Entity
{
public:
	PowerUp(class Level* level);
	
	virtual void update();

	virtual void loadProperties();
	virtual void renderTransparent();
	virtual void onReset();

	bool mTaken;
	Resource mTexture;
	Resource mSound;
};

