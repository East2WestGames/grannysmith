/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "genmesh.h"
#include "decompose.h"
#include <vector>
#include "profiling.h"
#include "clipper/clipper.h"
#include "gfx.h"
#include "triangulator.h"

float getAngle(const QiArray<QiVec2>& verts, int i)
{
	int c = verts.getCount();
	const QiVec2& p = verts[(i+c-1) % c];
	const QiVec2& m = verts[i%c];
	const QiVec2& n = verts[(i+1) % c];
	QiVec2 d0 = normalize(m-p);
	QiVec2 d1 = normalize(n-m);
	return signedAngle(d0, d1);
}

QiVec2 getNormal(const QiArray<QiVec2>& verts, int i)
{
	int c = verts.getCount();
	const QiVec2& p = verts[(i+c-1) % c];
	const QiVec2& m = verts[i%c];
	const QiVec2& n = verts[(i+1) % c];
	QiVec2 d0 = normalize(m-p);
	QiVec2 d1 = normalize(n-m);
	QiVec2 n0 = perpendicular(d0);
	QiVec2 n1 = perpendicular(d1);
	return -normalize(n0+n1);
}


inline bool BiasedDistanceFromLineLessThanZero(QiVec2 a, QiVec2 b, QiVec2 p)
{
	QiVec2 v=b-a;
	if(v.x<0 || (v.x==0 && v.y<0))
	{
		return cross(p-a, v)<0;
	}
	else
	{
		return cross(p-b, v)<=0;
	}
}

inline bool PointInTriangle(QiVec2 p, QiVec2 a, QiVec2 b, QiVec2 c, float &w)
{
	if(	BiasedDistanceFromLineLessThanZero(a, b, p) ||
		BiasedDistanceFromLineLessThanZero(b, c, p) ||
		BiasedDistanceFromLineLessThanZero(c, a, p))
	{
		return false;
	}
	w=cross(p-c, a-c);
	return true;
}


inline bool segmentBelow(const QiVec2& seg0, const QiVec2& seg1, const QiVec2& ref0, const QiVec2& ref1)
{
	QiVec2 n = -perpendicular(ref1-ref0);
	if (dot(n, ref0-seg0) < 0.0f)
		return false;
	if (dot(n, ref0-seg1) < 0.0f)
		return false;
	QiVec2 p = normalize(ref1-ref0);
	float limit = dot(p, ref1-ref0);
	float d0 = dot(p, ref0-seg0);
	float d1 = dot(p, ref0-seg1);
	return d0 > 0.0f && d0 < limit && d1 > 0.0f && d1 < limit;
}


bool RayIntersect(const QiVec2& p1, const QiVec2& p2, const QiVec2& p3, const QiVec2& p4, QiVec2& result)
{
	float mua,mub;
	float denom,numera,numerb;

	denom  = (p4.y-p3.y) * (p2.x-p1.x) - (p4.x-p3.x) * (p2.y-p1.y);
	numera = (p4.x-p3.x) * (p1.y-p3.y) - (p4.y-p3.y) * (p1.x-p3.x);
	numerb = (p2.x-p1.x) * (p1.y-p3.y) - (p2.y-p1.y) * (p1.x-p3.x);

	// Are the line coincident? 
	if (QiAbs(numera) < QI_FLOAT_EPSILON && QiAbs(numerb) < QI_FLOAT_EPSILON && QiAbs(denom) < QI_FLOAT_EPSILON) {
		result.x = (p1.x + p2.x) / 2;
		result.y = (p1.y + p2.y) / 2;
		return true;
	}

	// Are the line parallel 
	if (QiAbs(denom) < QI_FLOAT_EPSILON) {
		result.x = 0;
		result.y = 0;
		return false;
	}

	mua = numera / denom;
	mub = numerb / denom;
	result.x = p1.x + mua * (p2.x - p1.x);
	result.y = p1.y + mua * (p2.y - p1.y);
	return true;
}

inline bool clipSegment(QiVec2& seg0, QiVec2& seg1, const QiVec2& ref0, const QiVec2& ref1)
{
	return LineIntersect(seg0, seg1, ref0, ref1, seg1);
}

void ShadePolygon(const QiArray<QiVec2>& inVerts, QiArray<float>& shadow)
{
	shadow.clear();
	for(int i=0; i<inVerts.getCount(); i++)
	{
		float angle = getAngle(inVerts, i);
		shadow.add(QiClamp(-0.42f*angle, 0.0f, 0.65f));
	}
}

void AddSides(const QiArray<QiVec2>& inVerts, const QiArray<float>& inShadow, Mesh& outMesh, float z0, float z1)
{
	//Add sides to loop
	int count=inVerts.getCount();
	for(int i=0; i<count; i++)
	{
		int i0 = i;
		int i1 = ((i+1)%count);
		QiVec2 v0 = inVerts[i0];
		QiVec2 v1 = inVerts[i1];
		QiVec2 e = v1-v0;
		QiVec2 n = perpendicular(normalize(e));
		QiVec2 t;

		int a0 = outMesh.verts.getCount();
		outMesh.verts.add(QiVec3(v0.x, v0.y, z0));
		outMesh.light.add(1.0f-inShadow[i0]);
		t=QiVec2(v0.x, v0.y);
		outMesh.smooth.add(1);

		int a1 = outMesh.verts.getCount();
		outMesh.verts.add(QiVec3(v1.x, v1.y, z0));
		outMesh.light.add(1.0f-inShadow[i1]);
		t=QiVec2(v1.x, v1.y);
		outMesh.smooth.add(1);

		int a2 = outMesh.verts.getCount();
		outMesh.verts.add(QiVec3(v0.x, v0.y, z1));
		outMesh.light.add(1.0f-inShadow[i0]);
		t=QiVec2(v0.x, v0.y);
		t-=n*(z0-z1);
		outMesh.smooth.add(1);

		int a3 = outMesh.verts.getCount();
		outMesh.verts.add(QiVec3(v1.x, v1.y, z1));
		outMesh.light.add(1.0f-inShadow[i1]);
		t=QiVec2(v1.x, v1.y);
		t-=n*(z0-z1);
		outMesh.smooth.add(1);

		outMesh.indices.add(a2);
		outMesh.indices.add(a0);
		outMesh.indices.add(a3);

		outMesh.indices.add(a3);
		outMesh.indices.add(a0);
		outMesh.indices.add(a1);
	}
}


void ComputeNormals(Mesh& outMesh)
{
	outMesh.normals.redim(outMesh.verts.getCount());
	for(int i=0; i<outMesh.indices.getCount(); i+=3)
	{
		int i0 = outMesh.indices[i+0];
		int i1 = outMesh.indices[i+1];
		int i2 = outMesh.indices[i+2];
		QiVec3& p0 = outMesh.verts[i0];
		QiVec3& p1 = outMesh.verts[i1];
		QiVec3& p2 = outMesh.verts[i2];
		QiVec3 n = normalize(cross(p1-p0, p2-p1));
		outMesh.normals[i0] = n;
		outMesh.normals[i1] = n;
		outMesh.normals[i2] = n;
	}
}


void CorrelateVerts(const QiArray<QiVec2>& outer, const QiArray<QiVec2>& inner, QiArray<int>& outerMap, QiArray<int>& innerMap)
{
	for(int o=0; o<outer.getCount(); o++)
	{
		int best=0;
		float bestDist=QI_FLOAT_MAX;
		for(int i=0; i<inner.getCount(); i++)
		{
			float dist = lengthSquared(outer[o]-inner[i]);
			if (dist < bestDist && dot(getNormal(outer, o), getNormal(inner, i)) > -0.1f)
			{
				bestDist = dist;
				best = i;
			}
		}
		outerMap.add(best);
	}
	for(int i=0; i<inner.getCount(); i++)
	{
		int best=0;
		float bestDist=QI_FLOAT_MAX;
		for(int o=0; o<outer.getCount(); o++)
		{
			float dist = lengthSquared(outer[o]-inner[i]);
			if (dist < bestDist && dot(getNormal(outer, o), getNormal(inner, i)) > -0.1f)
			{
				bestDist = dist;
				best = o;
			}
		}
		innerMap.add(best);
	}
}

void StitchPolys(const QiArray<QiVec2>& outer, const QiArray<QiVec2>& inner, QiArray<int>& triangles)
{
	if (outer.getCount() < 3 || inner.getCount() < 3)
		return;
	int outerOffset=0;
	int innerOffset=outer.getCount(); 
	int in = 0;
	int out = 0;
	int outOffset = 0;
	int inOffset = 0;

	float bestDist=QI_FLOAT_MAX;
	for(int i=0; i<inner.getCount(); i++)
	{
		float dist = lengthSquared(outer[0]-inner[i]);
		if (dist < bestDist && dot(getNormal(outer, 0), getNormal(inner, i)) > -0.1f)
		{
			bestDist = dist;
			inOffset = i;
		}
	}

	while(out<outer.getCount() || in<inner.getCount())
	{
		int outIndex = (out+outOffset) % outer.getCount();
		int outIndexNext = (out+outOffset+1) % outer.getCount();
		int inIndex = (in+inOffset) % inner.getCount();
		int inIndexNext = (in+inOffset+1) % inner.getCount();

		bool doInner= false;

		QiVec2 e0 = outer[outIndex] - inner[inIndex];
		QiVec2 e1 = outer[outIndexNext] - inner[inIndex];
		float outArea = cross(e0, e1);
		float outLengthSq = lengthSquared(outer[outIndexNext] - inner[inIndex]);
		bool canDoOuter = outArea > 0.0f;

		e0 = outer[outIndex] - inner[inIndex];
		e1 = inner[inIndexNext] - inner[inIndex];
		float inArea = cross(e0, e1);
		float inLengthSq = lengthSquared(inner[inIndexNext] - outer[outIndex]);
		bool canDoInner = inArea > 0.0f;

		if (canDoOuter)
		{
			e0 = outer[outIndexNext] - inner[inIndex];
			e1 = inner[inIndexNext] - inner[inIndex];
			if (cross(e0, e1) < 0.0f)
				canDoOuter=false;
		}

		if (canDoInner)
		{
			e0 = inner[inIndexNext] - outer[outIndex];
			e1 = outer[outIndexNext] - outer[outIndex];
			if (cross(e0, e1) > 0.0f)
				canDoInner=false;
		}

		if (!canDoInner && canDoOuter)
			inLengthSq = QI_FLOAT_MAX;
		if (!canDoOuter && canDoInner)
			outLengthSq = QI_FLOAT_MAX;

		doInner = inLengthSq < outLengthSq;

		if (in == inner.getCount())
			doInner = false;
		if (out == outer.getCount())
			doInner = true;

		if (doInner)
		{
			triangles.add(outerOffset + outIndex);
			triangles.add(innerOffset + inIndexNext);
			triangles.add(innerOffset + inIndex);
			in++;
		}
		else
		{
			triangles.add(outerOffset + outIndex);
			triangles.add(outerOffset + outIndexNext);
			triangles.add(innerOffset + inIndex);
			out++;
		}

		//if (c++ > PROFILE_PARAM("stitch verts", 10))
		//	break;
	}
}

void AddFacet(const QiArray<QiVec2>& inVerts, int iter, int lastIter, float radius, float z0, float z1, const QiArray<float> & shadow, Mesh& outMesh)
{
	QiArray<QiVec2> verts = inVerts;
	QiArray<int> loops;
	loops.add(verts.getCount());

	bool last = (iter == lastIter);

	float ang = QI_PI*0.5f - QI_PI*0.5f*iter/(float)lastIter;
	float angNext = QI_PI*0.5f - QI_PI*0.5f*(iter+1)/(float)lastIter;
	float z = z0 + QiCos(ang)*(z1-z0);
	float zNext = z0 + QiCos(angNext)*(z1-z0);
	float r = radius*QiSin(ang);
	float rNext = radius*QiSin(angNext);
	float s = 1.0f-iter/(float)lastIter;
	float sNext = 1.0f-(iter+1)/(float)lastIter;

	int before = outMesh.verts.getCount();
	for(int i=0; i<verts.getCount(); i++)
	{
		outMesh.verts.add(QiVec3(verts[i].x, verts[i].y, z));
		outMesh.light.add(1.0f-s*shadow[i]);
		outMesh.smooth.add(last ? 0 : 2);
	}

	QiArray<int> outerMap;
	QiArray<int> innerMap;

	QiArray<QiVec2> smallerVerts;
	QiArray<int> smallerLoops;
	if (!last)
	{
		OffsetPolygon(verts, rNext-r, smallerVerts, smallerLoops);
		CorrelateVerts(verts, smallerVerts, outerMap, innerMap);

		for(int i=0; i<smallerVerts.getCount(); i++)
		{
			outMesh.verts.add(QiVec3(smallerVerts[i].x, smallerVerts[i].y, zNext));
			outMesh.light.add(1.0f-sNext*shadow[innerMap[i]]);
			outMesh.smooth.add(2);
		}

		QiArray<int> tri;
		StitchPolys(verts, smallerVerts, tri);
		for(int i=0; i<tri.getCount(); i++)
			outMesh.indices.add(before + tri[i]);

	}
	else
	{
		QiArray<int> tri;
		Triangulate(verts, loops, tri);
		for(int i=0; i<tri.getCount(); i++)
			outMesh.indices.add(before + tri[i]);
	}
	if (!last)
	{
		int t=0;
		for(int l=0; l<smallerLoops.getCount(); l++)
		{
			QiArray<float> tmpShadow;
			QiArray<QiVec2> tmpVerts;
			for(int i=0; i<smallerLoops[l]; i++)
			{
				tmpVerts.add(smallerVerts[t+i]);
				tmpShadow.add(shadow[innerMap[t+i]]);
			}

			AddFacet(tmpVerts, iter+1, lastIter, radius, z0, z1, tmpShadow, outMesh);
			t+=smallerLoops[l];
		}
	}
}

void GenerateTexCoords(Mesh& mesh, const QiString& mapping)
{
	mesh.texCoords.redim(mesh.verts.getCount());
	for(int i=0; i<mesh.verts.getCount(); i++)
	{
		QiVec2 t;
		if (mapping == "smooth")
		{
			t.x = mesh.verts[i].x + mesh.verts[i].z;
			t.y = mesh.verts[i].y - mesh.verts[i].z;
		}
		else if (mapping == "top")
		{
			t.x = mesh.verts[i].x;
			t.y = mesh.verts[i].z;
		}
		else if (mapping == "front")
		{
			t.x = mesh.verts[i].x;
			t.y = mesh.verts[i].y;
		}
		else if (mapping == "side")
		{
			t.x = mesh.verts[i].z;
			t.y = mesh.verts[i].y;
		}
		else
		{
			t.x = mesh.verts[i].x + mesh.normals[i].x * mesh.verts[i].z;
			t.y = mesh.verts[i].y + mesh.normals[i].y * mesh.verts[i].z;
		}
		mesh.texCoords[i] = t;
	}
}

void GenerateMesh(const QiArray<QiVec2>& inVertsOrg, Mesh& outMesh, int iterCount, float facetSize, float z0, float z1, const QiString& mapping)
{
	//PROFILE_ZONE("GenerateMesh func");
	QiArray<int> tri;

	QiArray<QiVec2> verts = inVertsOrg;

	//Prepare polygon for self-shadowing
	float maxDist = 1.2f;
	for(int i=0; i<verts.getCount(); i++)
	{
		QiVec2 p = verts[i];
		float a=getAngle(verts, i);
		if (a < 0.0f)
		{
			const QiVec2& vp = verts[(i+verts.getCount()-1) % verts.getCount()];
			float l = length(vp-p);
			if (l > maxDist)
			{
				float t = maxDist/l * 0.9f;
				verts.insertAt(i, vp*t + p*(1.0f-t));
				i++;
			}
			const QiVec2& vn = verts[(i+1) % verts.getCount()];
			l = length(vn-p);
			if (l > maxDist)
			{
				float t = maxDist/l * 0.9f;
				verts.insertAt(i+1, vn*t + p*(1.0f-t));
				i++;
			}
		}
	}

	QiArray<int> loops;
	loops.add(inVertsOrg.getCount());

	QiArray<float> shadow;
	ShadePolygon(verts, shadow);

	if (iterCount==0)
		AddFacet(verts, 1, 1, facetSize, z1, z1, shadow, outMesh);
	else
		AddFacet(verts, 0, iterCount, facetSize, z1-facetSize, z1, shadow, outMesh);

	AddSides(verts, shadow, outMesh, z0, z1-facetSize);

	ComputeNormals(outMesh);

	QiString m = mapping;
	if (m == "" && iterCount > 1)
		m = "smooth";
	GenerateTexCoords(outMesh, mapping);
	SmoothMesh(outMesh, iterCount > 1);
}


void SmoothMesh(Mesh& mesh, bool smooth)
{
	//PROFILE_ZONE("SmoothMesh func");
	Mesh tmp;
	QiArray<int> mapping;
	QiArray<float> totalLight;

	for(int i=0; i<mesh.verts.getCount(); i++)
	{
		int v = -1;
		for(int j=0; j<tmp.verts.getCount(); j++)
		{
			if (mesh.verts[i].equals(tmp.verts[j], 0.01f) && (smooth || dot(mesh.normals[i], tmp.normals[j]) > 0.9f))
			{
				if (smooth || mesh.smooth[i]==tmp.smooth[j])
				{
					v = j;
					tmp.light[j] += mesh.light[i];
					QiVec3 n = mesh.normals[i];
					if (smooth && n.z > 0.999f)
						n *= 1000.0f;
					tmp.normals[j] += n;
					totalLight[j] += 1.0f;
					break;
				}
			}
		}
		if (v == -1)
		{
			v = tmp.verts.getCount();
			tmp.verts.add(mesh.verts[i]);
			tmp.light.add(mesh.light[i]);
			tmp.texCoords.add(mesh.texCoords[i]);
			tmp.normals.add(mesh.normals[i]);
			tmp.smooth.add(mesh.smooth[i]);
			totalLight.add(1.0f);
		}
		mapping.add(v);
	}
	for(int i=0; i<tmp.light.getCount(); i++)
		tmp.light[i] /= totalLight[i];

	for(int i=0; i<mesh.indices.getCount(); i++)
		tmp.indices.add(mapping[mesh.indices[i]]);

	for(int i=0; i<tmp.normals.getCount(); i++)
		tmp.normals[i] = normalize(tmp.normals[i]);

	mesh.indices.clear();
	mesh.indices.addAll(tmp.indices.getIterator());
	mesh.light.clear();
	mesh.light.addAll(tmp.light.getIterator());
	mesh.normals.clear();
	mesh.normals.addAll(tmp.normals.getIterator());
	mesh.texCoords.clear();
	mesh.texCoords.addAll(tmp.texCoords.getIterator());
	mesh.verts.clear();
	mesh.verts.addAll(tmp.verts.getIterator());
}


bool Triangulate(const QiArray<QiVec2> &polygon, const QiArray<int> &loops, QiArray<int> &triangles)
{
	if (polygon.getCount() == 0)
		return false;

	static Triangulator* t = createTriangulator();
	t->reset();
	for(int i=0; i<polygon.getCount(); i++)
		t->addPoint(polygon[i].x, polygon[i].y, 0.0f);
	unsigned int tcount = 0;
	unsigned int* indices = t->triangulate(tcount);

	for(unsigned int i=0; i<tcount; i++)
	{
		triangles.add(indices[i*3+2]);
		triangles.add(indices[i*3+1]);
		triangles.add(indices[i*3+0]);
	}

	return true;
}


void OffsetPolygon(const QiArray<QiVec2> &inVerts, float offset, QiArray<QiVec2>& outVerts, QiArray<int>& outLoops)
{
	ClipperLib::Polygons polys(1);
	for(int i=0; i<inVerts.getCount(); i++)
		polys[0].push_back(ClipperLib::IntPoint((int)(inVerts[i].x*1000), (int)(inVerts[i].y*1000)));
	
	ClipperLib::Polygons result;
	ClipperLib::OffsetPolygons(polys, result, (int)(offset*1000), ClipperLib::jtMiter);
	for(unsigned int j=0; j<result.size(); j++)
	{
		ClipperLib::Polygon& poly = result[j];
		outLoops.add(poly.size());
		for(unsigned int i=0; i<poly.size(); i++)
			outVerts.add(QiVec2(poly[i].X*0.001f, poly[i].Y*0.001f));
	}
}


void ApplyShadow(const QiArray<QiVec2>& dstVerts, float dstZ0, float dstZ1, const QiArray<QiVec2>& srcVerts, float srcZ0, float srcZ1, Mesh& outMesh)
{
	//PROFILE_ZONE("joxjoxjox");
	if (srcZ1 < dstZ1 || srcZ1==dstZ1)
		return;

	float dist = srcZ0 - dstZ1;
	float thickness = (srcZ1-srcZ0);
	float maxDist = 2.0f;
	float umbraAmount = QiClamp((maxDist - dist)/maxDist * 0.6f, 0.1f, 0.6f);

	if (dist > maxDist)
		return;

	float penumbraSize = QiClamp(dist + thickness, 0.2f, 0.6f);

	float eps = 0.03f;

	ClipperLib::Polygon src;
	for(int i=0; i<srcVerts.getCount(); i++)
		src.push_back(ClipperLib::IntPoint((int)(srcVerts[i].x*1000), (int)(srcVerts[i].y*1000)));

	ClipperLib::Polygon dst;
	for(int i=0; i<dstVerts.getCount(); i++)
		dst.push_back(ClipperLib::IntPoint((int)(dstVerts[i].x*1000), (int)(dstVerts[i].y*1000)));

	ClipperLib::Polygons solution;
	ClipperLib::Clipper clipper;
	clipper.AddPolygon(src, ClipperLib::ptSubject);
	clipper.AddPolygon(dst, ClipperLib::ptClip);
	clipper.Execute(ClipperLib::ctIntersection, solution);

	for(unsigned int i=0; i<solution.size(); i++)
	{
		//Umbra
		ClipperLib::Polygon& p = solution[i];
        int firstVertex = outMesh.verts.getCount();

		//Penumbra
		ClipperLib::Polygons penumbraOutline;
		ClipperLib::Polygons x;
		x.push_back(p);
		ClipperLib::OffsetPolygons(x, penumbraOutline, int(penumbraSize*1000), ClipperLib::jtMiter);

		//Recompute umbra
		ClipperLib::Polygons clippedUmbra;
		//ClipperLib::OffsetPolygons(penumbraOutline, clippedUmbra, int(-penumbraSize*1000), ClipperLib::jtMiter);
		clippedUmbra.push_back(p);

		//Clip penumbra to polygon
		ClipperLib::Polygons clippedPenumbra;
		clipper.Clear();
		clipper.AddPolygons(penumbraOutline, ClipperLib::ptSubject);
		clipper.AddPolygon(dst, ClipperLib::ptClip);
		clipper.Execute(ClipperLib::ctIntersection, clippedPenumbra);

		if (clippedPenumbra.size() == 1 && clippedUmbra.size() == 1)
		{
			ClipperLib::Polygon& tmp = clippedPenumbra[0];
			QiArrayInplace<QiVec2, 64> penumbraVerts;
			for(unsigned int i=0; i<tmp.size(); i++)
				penumbraVerts.add(QiVec2(tmp[i].X*0.001f, tmp[i].Y*0.001f));

			for(int j=0; j<penumbraVerts.getCount(); j++)
			{
				outMesh.verts.add(QiVec3(penumbraVerts[j].x, penumbraVerts[j].y, dstZ1+eps));
				outMesh.light.add(0.9f);
			}

			ClipperLib::Polygon& tmpu = clippedUmbra[0];
			QiArrayInplace<QiVec2, 64> umbraVerts;
			for(unsigned int i=0; i<tmpu.size(); i++)
				umbraVerts.add(QiVec2(tmpu[i].X*0.001f, tmpu[i].Y*0.001f));

			for(int j=0; j<umbraVerts.getCount(); j++)
			{
				outMesh.verts.add(QiVec3(umbraVerts[j].x, umbraVerts[j].y, dstZ1+eps));
				outMesh.light.add(1.0f-umbraAmount);
			}

			//Triangulate umbra with ear clipping
			QiArrayInplace<int, 64> tri;
			QiArrayInplace<int, 4> loops;
			loops.add(umbraVerts.getCount());
			Triangulate(umbraVerts, loops, tri);
			for(int j=0; j<tri.getCount(); j++)
				outMesh.indices.add(firstVertex + penumbraVerts.getCount() + tri[j]);

			//Triangulate penumbra with homebrew stitching
			tri.clear();
			StitchPolys(penumbraVerts, umbraVerts, tri);
			for(int j=0; j<tri.getCount(); j++)
				outMesh.indices.add(firstVertex + tri[j]);
		}
	}

}
