/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "propertybag.h"
#include "entity.h"
#include "resman.h"

class Sensor : public Entity
{
public:
	Sensor(class Level* level);

	virtual void onStart();
	virtual void onReset();

	bool testPoint(const QiVec2& point) const;

	bool mIsBox;
	QiVec2 mSize;

public:
	int mUsed;
	bool mEnabled;
	bool mIsForce;
	Resource mResource;
};

