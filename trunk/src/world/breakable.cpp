/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "breakable.h"
#include "level.h"
#include "game.h"
#include "gfx.h"
#include "dude.h"
#include "body.h"
#include "audio.h"
#include "profiling.h"
#include "display.h"
#include "Box2D.h"
#include "particlesystem.h"
#include "scene.h"
#include "device.h"

#include "td/td.h"

static const int MAX_FRAGMENTS = 128;

inline QiVec3 convert(const tdVec& vec) { return QiVec3(vec.x, vec.y, vec.z); }
inline tdVec convert(const QiVec3& vec) { tdVec v; v.x=vec.x, v.y=vec.y, v.z=vec.z; return v;}
inline QiQuat convert(const tdQuat& q) { return QiQuat(q.x, q.y, q.z, q.w);}
inline tdQuat convert(const QiQuat& q) { tdQuat r; r.x=q.x, r.y=q.y, r.z=q.z; r.w=q.w; return r;}

class Fragment
{
public:
	bool mAlive;
	bool mParticle;
	bool mSleeping;
	tdBody mTdBody;
	QiTransform3 mRenderTransform;
	tdSimplex mTdSimplex;
	int mTdAabb;
	tdAabb mAabb;
	tdAabb mRenderAabb;
	bool mRemove;

	QiArrayInplace<QiVec3, 8> mVerts;
	QiArrayInplace<QiVec2, 8> mTexCoords;

	Fragment() : mAlive(true), mParticle(false), mSleeping(false), mRemove(false)
	{
		memset(&mTdBody, 0, sizeof(tdBody));
		memset(&mTdSimplex, 0, sizeof(tdSimplex));
		memset(&mAabb, 0, sizeof(tdAabb));
		memset(&mRenderAabb, 0, sizeof(tdAabb));
	}

	QiVec2 getMidPoint() const
	{
		QiVec2 midPoint;
		for(int i=0; i<mVerts.getCount(); i++)
			midPoint += mVerts[i].vec2();
		midPoint *= 1.0f / (float)mVerts.getCount();
		return midPoint;
	}

	void setMass()
	{
		tdVec h = {0, 0, 0};
		for(int i=0; i<mVerts.getCount(); i++)
		{
			h.x = QiMax(h.x, QiAbs(mVerts[i].x));
			h.y = QiMax(h.y, QiAbs(mVerts[i].y));
		}
		h.x*=1.0f;
		h.y*=1.0f;
		tdBodySetMassAsBox(&mTdBody, 1.0f, &h);
	}

	void computeAabb()
	{
		tdShape s;
		tdShapeInitHull(&s, mVerts.getCount(), sizeof(QiVec3), (float*)mVerts.getData());

		tdBoundingBoxQuery query = {0};
		query.padding = 0.1f;
		tdTransformInitPQ(&query.transform, &mTdBody.pos, &mTdBody.rot);
		query.shape = &s;
		tdBoundingBoxAabb(0, &query, &mAabb);
	}

	float getArea()
	{
		float area = 0.0f;
		int c = mVerts.getCount();
		for(int i=0; i<c; i++)
		{
			int a = (i+1)%c;
			int b = (i+c-1)%c;
			area += 0.5f * mVerts[i].x * (mVerts[a].y-mVerts[b].y);
		}
		return area;
	}

	QiVec2 getSplitDir()
	{
		QiVec2 longest;
		int c = mVerts.getCount();
		for(int i=0; i<c; i++)
		{
			int j = (i+1)%c;
			QiVec2 side = mVerts[j].vec2() - mVerts[i].vec2();
			if (lengthSquared(side) > lengthSquared(longest))
				longest = side;	
		}
		return normalize(perpendicular(longest));
	}
};


Breakable::Breakable(Level* level) : Entity(level, BREAKABLE), mType(0), mHeight(0.0f), mSmoke(false), mBreak(0), mBreakFromSide(false)
{
	mProperties.add("type", "glass");

	mProperties.add("break", "");
	mProperties.add("texture", "");
	mProperties.add("texScale", "1 1");
	mProperties.add("texOffset", "0 0");
	mProperties.add("color", "1 1 1 1");

	mProperties.add("width", "1");
	mProperties.add("height", "1");
	mProperties.add("thickness", "0.06");

	mProperties.add("penalty", "0.5");
	mProperties.add("stick", "0");
	mProperties.add("smoke", "0");
	mProperties.add("die", "0");

	mProperties.add("breakFromSide", "0");

	tdSpace space;
	space.type = TD_SPACE_DYNAMIC_AABB;
	mTdSpace = tdSpaceCreate(&space);

	mParticles = QI_NEW ParticleSystem();
	mParticles->load("breakables_dust");

	loadProperties();
}


Breakable::~Breakable()
{
	clear();
	tdSpaceDestroy(mTdSpace);
	QI_DELETE(mParticles);
}


void Breakable::clear()
{
	for(int i=0; i<mFragments.getCount(); i++)
	{
		tdSpaceRemoveAabb(mTdSpace, mFragments[i]->mTdAabb);
		QI_DELETE(mFragments[i]);
	}
	mFragments.clear();
}


void Breakable::loadProperties()
{
	Entity::loadProperties();

	clear();

	float w = mProperties.getFloat("width");
	float h = mProperties.getFloat("height");

	QiVec2 offset = mProperties.getVec2("texOffset");
	QiVec2 scale = mProperties.getVec2("texScale");

	Fragment* f = QI_NEW Fragment();
	f->mVerts.add(QiVec3(-w/2, -h/2, 0.0f));
	f->mVerts.add(QiVec3(w/2, -h/2, 0.0f));
	f->mVerts.add(QiVec3(w/2, h/2, 0.0f));
	f->mVerts.add(QiVec3(-w/2, h/2, 0.0f));
	f->setMass();

	for(int i=0; i<4; i++)
		f->mTexCoords.add(offset + multiplyPerElement(scale, f->mVerts[i].vec2()));

	tdAabb aabb;
	aabb.lower.x = aabb.lower.y = aabb.lower.z = 0;
	aabb.upper.x = aabb.upper.y = aabb.upper.z = 0;
	f->mTdAabb = tdSpaceInsertAabb(mTdSpace, &aabb, (int)f);

	mFragments.add(f);

	mThickness = mProperties.getFloat("thickness");
	mHeight = mProperties.getFloat("height");

	mFmt.clear();
	mFmt.addField("aPosition", QiVertexFormat::FLOAT32, 3);
	mFmt.addField("aTexCoord", QiVertexFormat::FLOAT32, 2);
	mVb.init(mFmt, MAX_FRAGMENTS * 6);
	mIb.init(MAX_FRAGMENTS * 24 * 2);

	if (mProperties.getString("type")=="wood")
	{
		mType = 1;
		mTexture = mLevel->mResMan->acquireTexture("gfx/planks.jpg");
		mSound = &gGame->mAudio->mBreakWood;
	}
	else if (mProperties.getString("type")=="brick")
	{
		mType = 2;
		mTexture = mLevel->mResMan->acquireTexture("gfx/bricks.jpg");
		mSound = &gGame->mAudio->mBreakBrick;
	}
	else if (mProperties.getString("type")=="metal")
	{
		mType = 3;
		mTexture = mLevel->mResMan->acquireTexture("gfx/tin.jpg");
		mSound = &gGame->mAudio->mBreakMetal;
	}
	else if (mProperties.getString("type")=="branch")
	{
		mType = 4;
		mTexture = mLevel->mResMan->acquireTexture("gfx/planks.jpg");
		mSound = &gGame->mAudio->mBreakBranch;
	}
	else
	{
		mSound = &gGame->mAudio->mBreakGlass;
		mType = 0;
		mTexture.release();
		mFmt.clear();
		mFmt.addField("aPosition", QiVertexFormat::FLOAT32, 3);
		mFmt.addField("aNormal", QiVertexFormat::FLOAT32, 3);
		mFmt.addField("aTexCoord", QiVertexFormat::FLOAT32, 2);
		mVb.init(mFmt, MAX_FRAGMENTS * 6);
		mIb.init(MAX_FRAGMENTS * 24 * 2);
	}

	if (mProperties.getString("texture") != "")
		mTexture = mLevel->mResMan->acquireTexture(mProperties.getString("texture"));

	if (mTexture.getTexture())
		mTexture.getTexture()->enableRepeat(true);
	
	mSmoke = mProperties.getBool("smoke");

	if (mProperties.getString("break") == "x")
		mBreak = 1;
	else if (mProperties.getString("break") == "y")
		mBreak = 2;
	else
		mBreak = 0;

	mBreakFromSide = mProperties.getBool("breakFromSide");
	mZ = -w * 0.5f;
	setTransform(mTransform);
}


void Breakable::setTransform(const QiTransform2& transform)
{
	Entity::setTransform(transform);

	if (mFragments.getCount() == 1)
	{
		QiTransform3 t;
		t.pos.set(mTransform.pos.x, mTransform.pos.y, 0);
		t.rot = QiQuat(QiVec3::Z, mTransform.rot) * QiQuat(QiVec3::Y, QI_HALF_PI);
		Fragment* f = mFragments[0];
		f->mTdBody.pos = (tdVec&)t.pos;
		f->mTdBody.rot = (tdQuat&)t.rot;

		mBoundsMin.set(t.pos.x-mHeight*0.5f, t.pos.y-mHeight*0.5f);
		mBoundsMax.set(t.pos.x+mHeight*0.5f, t.pos.y+mHeight*0.5f);

		f->mRenderTransform = QiTransform3((QiVec3&)f->mTdBody.pos, (QiQuat&)f->mTdBody.rot);
	}
}


void Breakable::update()
{
	PROFILE_FUNC();

	for(int i=0; i<2; i++)
	{
		Dude* dude = (i==0 ? mLevel->mDude : mLevel->mBadGuy);
		if (crosses(dude->mTransform.pos, dude->mTorso->getVelocity(), 0.8f))
		{
			if (mFragments.getCount() == 1)
			{
				if (dude == mLevel->mDude || dude->mOutput)
				{
					breakUp(dude->mTransform.pos, dude->mTorso->getVelocity());
					gGame->mDisplay->mShakeTimer = length(dude->getVelocity()) * 0.04f;
					dude->mCurrentFace = Dude::FACE_CRASH;
					dude->mFacialExpressionTimer = 0.7f;
					dude->mReplayHints |= HINT_BREAK;
					gGame->mAudio->playSound(dude->mSndBreak.next(), dude->mTransform.pos);
				}
				else
				{
					if (mLevel->mBadGuy->mTransform.pos.x - mLevel->mDude->mTransform.pos.x > 30.0f)
						breakUp(dude->mTransform.pos, dude->mTorso->getVelocity(), 0.6f);
					else
						breakUp(dude->mTransform.pos, dude->mTorso->getVelocity());
					gGame->mAudio->playSound(mSound->next(), mTransform.pos);
				}
				float t = 1.0f-mProperties.getFloat("penalty");
				if (!dude->mGrabbedHandle && !dude->mGrabbedWire && !dude->mRailBody)
				{
					/*
					if (dude == mLevel->mDude && dude->mGrabTimer > 0.0f && dude->mGrabTimer < 0.2f)
					{
						if (!mLevel->mReplay)
						{
							mLevel->mScore += 100;
							gGame->mHudScene->mScript.handleCommand(QiString("score 100"));
						}
					}
					*/
					if (!dude->mDead)
						dude->mTorso->mPhysBody->SetLinearVelocity(t * dude->mTorso->mPhysBody->GetLinearVelocity());
					if (mProperties.getBool("die") && dude->mPlayerId == 1)
						dude->die();
				}
			}
		}
	}
	
	mParticles->update();

	if (mFragments.getCount() > 1)
	{
		for(int i=0; i<mFragments.getCount(); i++)
		{
			Fragment* f = mFragments[i];
			f->mRenderTransform = QiTransform3((QiVec3&)f->mTdBody.pos, (QiQuat&)f->mTdBody.rot);
			f->mRenderAabb = f->mAabb;

			if (f->mRenderTransform.pos.x-f->mRenderTransform.pos.x != 0.0f || f->mRenderTransform.pos.y-f->mRenderTransform.pos.y != 0.0f || f->mRenderTransform.pos.z-f->mRenderTransform.pos.z != 0.0f)
				f->mRemove = true;

			if (f->mRemove)
	{
				tdSpaceRemoveAabb(mTdSpace, f->mTdAabb);
				QI_DELETE(f);
				mFragments.removeElementAt(i);
				i--;
				continue;
			}	
			
			if (f->mAlive)
				continue;

			QiVec3 p = mLevel->mDude->mTransform.pos.vec3();
			tdAabb& aabb = f->mAabb;
			float r = 0.5f;
			if (p.x > aabb.lower.x-r && p.y > aabb.lower.y-r && p.z > aabb.lower.z-r &&
				p.x < aabb.upper.x+r && p.y < aabb.upper.y+r && p.z < aabb.upper.z+r)
			{
				if (f->getArea() > 1.0f)
				{
					mFragments.removeElementAt(i);
					i--;
					int b = mBreak;
					mBreak = 0;
					float minArea = 0.5f;
					if (gGame->extraEffects())
						minArea = 0.2f;
					split(f, 1, mLevel->mDude->mTransform.pos, mLevel->mDude->mTorso->getVelocity(), minArea);
					mBreak = b;
				}
				else
				{
					if (!f->mSleeping)
						f->mAlive = true;
				}
			}
		}
	}
}


void Breakable::updateAsync()
{
	float g = mLevel->mGravity;
	if (mFragments.getCount() > 1)
	{
		int s = mLevel->mTdSolver;
		int bodyCount = 0;
		tdVec linAcc = {0,0,0};
		linAcc.y = -g;
		tdSolverSetLinAcc(s, &linAcc);
		for(int i=0; i<mFragments.getCount(); i++)
		{
			if (mFragments[i]->mAlive)
			{
				tdSolverInsertBody(s, &mFragments[i]->mTdBody);
				bodyCount++;
			}
		}
		if (bodyCount > 0)
		{
			generateContacts();
			tdSolverStep(s, gGame->mTimeStep);
		}
	}
}


bool Breakable::crosses(const QiVec2& point, const QiVec2& vel, float radius)
{
	QiVec2 v = vel * (gGame->mTimeStep*2);
	v = normalize(v) * (length(v)+0.7f);

	QiVec2 p0 = point;
	QiVec2 p1 = point + v;
	QiVec2 p2 = mTransform.toParentPoint(QiVec2(0, mHeight*0.5f+radius));
	QiVec2 p3 = mTransform.toParentPoint(QiVec2(0, -mHeight*0.5f-radius));
	QiVec2 result;
	if (LineIntersect(p0, p1, p2, p3, result))
		return true;

	if (mBreakFromSide)
	{
		//Check top
		p2 = mTransform.toParentPoint(QiVec2(radius, mHeight*0.5f));
		p3 = mTransform.toParentPoint(QiVec2(-radius, mHeight*0.5f));
		if (LineIntersect(p0, p1, p2, p3, result))
			return true;

		//Check bottom
		p2 = mTransform.toParentPoint(QiVec2(radius, -mHeight*0.5f));
		p3 = mTransform.toParentPoint(QiVec2(-radius, -mHeight*0.5f));
		if (LineIntersect(p0, p1, p2, p3, result))
			return true;
	}
	return false;
}


void Breakable::breakUp(const QiVec2& pos, const QiVec2& vel, float minArea)
{
	if (mFragments.getCount() == 1)
	{
		gGame->mAudio->playSound(mSound->next(), mTransform.pos);

		Fragment* f = mFragments.removeLast();

		if (gGame->extraEffects())
		{
			minArea *= 0.6f;		
			if (mLevel->mBreakableFragments > 200)
				minArea *= 1.5f;
			if (mLevel->mBreakableFragments > 500)
				minArea *= 1.5f;
		}
		else
		{
			if (mLevel->mBreakableFragments > 100)
				minArea *= 1.5f;
			if (mLevel->mBreakableFragments > 200)
				minArea *= 1.5f;
		}
		
		split(f, 0, pos, vel, minArea);

		int stick = mProperties.getInt("stick");
		float w = mProperties.getFloat("width");
		float h = mProperties.getFloat("height");

		for(int i=0; i<mFragments.getCount(); i++)
		{
			Fragment* f = mFragments[i];
			if (f->mAlive)
			{
				QiTransform3 t(convert(f->mTdBody.pos), convert(f->mTdBody.rot));
				for(int j=0; j<f->mVerts.getCount(); j++)
				{
					QiVec3 p = t.toParentPoint(f->mVerts[j]);
					QiVec2 local = mTransform.toLocalPoint(p.vec2());

					if ((stick & 1) && QiAbs(local.y - h*0.5f) < 0.01f)
						f->mAlive = false;

					if ((stick & 2) && QiAbs(local.y + h*0.5f) < 0.01f)
						f->mAlive = false;

					if ((stick & 4) && QiAbs(p.z - w*0.5f) < 0.01f)
						f->mAlive = false;

					if ((stick & 8) && QiAbs(p.z + w*0.5f) < 0.01f)
						f->mAlive = false;
				}
			}
		}
	}
}



void Breakable::generateContacts()
{
	PROFILE_FUNC();
	tdSolverResetState(mLevel->mTdSolver);
	tdSolverSetParam(mLevel->mTdSolver, TD_SOLVER_PARAM_FRICTION, 3.0f);
	tdSolverSetParam(mLevel->mTdSolver, TD_SOLVER_PARAM_RESTITUTION, 0.35f);
	tdSolverSetParam(mLevel->mTdSolver, TD_SOLVER_PARAM_CONTACT_OFFSET, 0.05f);

	//Update bounds
	{
		PROFILE_ZONE("Space");
		for(int i=0; i<mFragments.getCount(); i++)
		{
			if (mFragments[i]->mAlive)
			{
				mFragments[i]->computeAabb();
				tdSpaceUpdateAabb(mTdSpace, mFragments[i]->mTdAabb, &mFragments[i]->mAabb);
			}
		}
	}

	//Collide spaces
	{
		PROFILE_ZONE("Space");
		tdSpaceOverlapSpace(mLevel->mTdContext, mTdSpace, mLevel->mTdSpace);
	}

	int ud0[64];
	int ud1[64];
	int count = 0;
	int c=0;
	int cc=0;
	
	while((count = tdSpaceFetchOverlaps(mLevel->mTdContext, mTdSpace, ud0, ud1, 64)))
	{
		for(int i=0; i<count; i++)
		{
			c++;
			BodyConvex* bc = (BodyConvex*)ud1[i];
			if (!bc->body || bc->body->mHidden)
				continue;

			Fragment* f = (Fragment*)ud0[i];
			if (!f->mAlive)
				continue;

			QiTransform3 t3(bc->body->mTransform);

			tdShape s;
			tdShapeInitHull(&s, bc->count*2, sizeof(QiVec3), (float*)bc->mTdVerts);

			tdShape fs;
			tdShapeInitHull(&fs, f->mVerts.getCount(), sizeof(QiVec3), (float*)f->mVerts.getData());
			
			if (f->mParticle)
				tdShapeInitSphere(&fs, 0.05f);

			QiTransform3 ft((QiVec3&)f->mTdBody.pos, (QiQuat&)f->mTdBody.rot);
			QiTransform3 t = t3.toLocal(ft);

			tdConstraintContactBB contact;
			memset(&contact, 0, sizeof(tdConstraintContactBB));
			float extraEps = length(convert(f->mTdBody.linVel))*gGame->mTimeStep;

			tdDistanceQuery q = {0};
			q.flags = TD_QUERY_COMPUTE_POINTS | TD_QUERY_COMPUTE_NORMAL | TD_QUERY_USE_MAX_DISTANCE | TD_QUERY_HANDLE_PENETRATION | TD_QUERY_STORE_SIMPLEX | TD_QUERY_LOAD_SIMPLEX;
			tdTransformInitPQ(&q.transform, (tdVec*)&t.pos, (tdQuat*)&t.rot);
			q.shape0 = &s;
			q.shape1 = &fs;
			q.maxDistance = 0.05f + extraEps;
			q.simplex = &f->mTdSimplex;
			tdDistanceResult r;
			tdDistance(mLevel->mTdContext, &q, &r);
			if ((r.status & TD_RESULT_HAS_POINTS) &&  r.distance < 0.05f + extraEps)
			{
				if (QiAbs(r.point0.x) < 100.0f && QiAbs(r.point0.y) < 100.0f && QiAbs(r.point0.z) < 100.0f && 
					QiAbs(r.point1.x) < 100.0f && QiAbs(r.point1.y) < 100.0f && QiAbs(r.point1.z) < 100.0f)
				{
				contact.manifold.count = 1;
				contact.manifold.normal0 = r.normal;
				contact.manifold.normal1 = convert(ft.toLocalVec(convert(r.normal)));
				contact.manifold.points0[0] = r.point0;
				contact.manifold.points1[0] = r.point1;
				
				if (f->mParticle)
				{
					f->mAlive = false;
					f->mSleeping = true;
				}
				else
				{
					float d = dot(convert(contact.manifold.normal0), ft.toParentVec(QiVec3(0,0,1)));
					if (QiAbs(f->mTdBody.linVel.x) < 0.5f && QiAbs(f->mTdBody.linVel.z) < 0.5f && QiAbs(d) > 0.9f && contact.manifold.normal0.y > 0.7f)
					{
						f->mAlive = false;
						f->mSleeping = true;
					}
					else
					{
						cc++;
						//Stupid td
						for(int k=0; k<contact.manifold.count; k++)
						{
							QiVec3 p = (QiVec3&)contact.manifold.points0[k];
							QiVec3 wp = t3.toParentPoint(p);
							contact.manifold.points0[k] = (tdVec&)wp;
						}

						contact.bodies[0] = NULL;
						contact.bodies[1] = &f->mTdBody;
						tdSolverInsertContactBB(mLevel->mTdSolver, &contact);
					}
				}
			}
		}
	}
	}
}


void Breakable::render()
{
	if (mType > 0)
		draw();
}


void Breakable::renderTransparent()
{
	if (mType == 0)
		draw();

	mParticles->render();
}


void Breakable::draw()
{
    if (!PROFILE_PARAM("render/breakable", true))
        return;
	
	if (mFragments.getCount() > 1)
	{
		mBoundsMin.set(QI_FLOAT_MAX, QI_FLOAT_MAX);
		mBoundsMax.set(-QI_FLOAT_MAX, -QI_FLOAT_MAX);
		for(int i=0; i<mFragments.getCount(); i++)
		{
			Fragment* f = mFragments[i];
			mBoundsMin.x = QiMin(mBoundsMin.x, f->mRenderAabb.lower.x - 1.0f);
			mBoundsMin.y = QiMin(mBoundsMin.y, f->mRenderAabb.lower.y - 1.0f);
			mBoundsMax.x = QiMax(mBoundsMax.x, f->mRenderAabb.upper.x + 1.0f);
			mBoundsMax.y = QiMax(mBoundsMax.y, f->mRenderAabb.upper.y + 1.0f);
			if (!gGame->mDisplay->isVisible(convert(f->mRenderAabb.lower).vec2() + QiVec2(-5000.0f, -5000.0f), convert(f->mRenderAabb.upper).vec2() + QiVec2(0.0f, 5000.0f), f->mRenderAabb.lower.z))
				f->mRemove = true;
			}
		}

	PROFILE_ZONE("Breakable::render");
	mVb.clear();
	mIb.clear();

	for(int i=0; i<mFragments.getCount(); i++)
	{
		Fragment* f = mFragments[i];

		QiTransform3 t = f->mRenderTransform;
		int base = mVb.getCount();
		if (mType == 0)
		{
			QiVec3 p0 = t.toParentPoint(f->mVerts[0]);
			QiVec3 p1 = t.toParentPoint(f->mVerts[1]);
			QiVec3 p2 = t.toParentPoint(f->mVerts[2]);
			QiVec3 n = normalize(cross(p1-p0, p2-p0));
			for(int j=0; j<f->mVerts.getCount(); j++)
			{
				QiVec3 v = t.toParentPoint(f->mVerts[j] + QiVec3(0, 0, -mThickness*0.5f));
				mVb.vertex();
				mVb.addFast(v);
				mVb.addFast(n);
				mVb.addFast(f->mTexCoords[j]);
			}
			mIb.triangle(base, base+1, base+2);
			for(int j=2; j<f->mVerts.getCount()-1; j++)
				mIb.triangle(base, base+j, base+j+1);
			if (mThickness > 0.0f && !f->mParticle)
			{
				int base2 = mVb.getCount();
				for(int j=0; j<f->mVerts.getCount(); j++)
				{
					QiVec3 v = t.toParentPoint(f->mVerts[j] + QiVec3(0, 0, mThickness*0.5f));
					mVb.vertex();
					mVb.addFast(v);
					mVb.addFast(-n);
					mVb.addFast(f->mTexCoords[j]);
				}
				mIb.triangle(base2, base2+2, base2+1);
				for(int j=2; j<f->mVerts.getCount()-1; j++)
					mIb.triangle(base2, base2+j+1, base2+j);
				for(int j=0; j<f->mVerts.getCount(); j++)
				{
					int k = (j+1) % f->mVerts.getCount();
					mIb.quad(base+j, base+k, base2+k, base2+j);
				}
			}
		}
		else
		{
			for(int j=0; j<f->mVerts.getCount(); j++)
			{
				QiVec3 v = t.toParentPoint(f->mVerts[j] + QiVec3(0, 0, -mThickness*0.5f));
				mVb.vertex();
				mVb.addFast(v);
				mVb.addFast(f->mTexCoords[j]);
			}
			mIb.triangle(base, base+2, base+1);
			for(int j=2; j<f->mVerts.getCount()-1; j++)
				mIb.triangle(base, base+j+1, base+j);

			if (mThickness > 0.0f)
			{
				int base2 = mVb.getCount();
				for(int j=0; j<f->mVerts.getCount(); j++)
				{
					QiVec3 v = t.toParentPoint(f->mVerts[j] + QiVec3(0, 0, mThickness*0.5f));
					mVb.vertex();
					mVb.addFast(v);
					mVb.addFast(f->mTexCoords[j]);
				}
				mIb.triangle(base2, base2+1, base2+2);
				for(int j=2; j<f->mVerts.getCount()-1; j++)
					mIb.triangle(base2, base2+j, base2+j+1);
				for(int j=0; j<f->mVerts.getCount(); j++)
				{
					int k = (j+1) % f->mVerts.getCount();
					mIb.quad(base+j, base+k, base2+k, base2+j);
				}
			}
		}
	}

	QiRenderState state;
	state.depthTest = true;
	state.depthMask = true;

	if (mType > 0)
	{
		//Wood, brick, metal, etc
		state.color = mProperties.getColor("color");
		state.texture[0] = mTexture.getTexture();
		state.shader = gGame->mGfx->m2DTexShader.getShader();
	}
	else
	{
		//Glass
		if (mTexture.getTexture())
		{
			state.shader = gGame->mGfx->mGlassTexShader.getShader();
			state.texture[0] = mTexture.getTexture();
		}
		else
			state.shader = gGame->mGfx->mGlassShader.getShader();
		state.blendMode = state.BLEND;
	}
	if (mThickness > 0.0f && mType > 0)
		state.cullFace = true;
	gGame->mRenderer->setState(state);
	gGame->mRenderer->drawTriangles(&mVb, &mIb);
}


void Breakable::onReset()
{
	Entity::onReset();
}


void Breakable::split(Fragment* f, Fragment** f0, Fragment** f1, const QiVec2& n)
{
	PROFILE_FUNC();
	QiVec2 midPoint = f->getMidPoint();

	*f0 = QI_NEW Fragment();
	copySplitVerts(f, *f0, midPoint, n);

	*f1 = QI_NEW Fragment();
	copySplitVerts(f, *f1, midPoint, -n);

	tdSpaceRemoveAabb(mTdSpace, f->mTdAabb);
	QI_DELETE(f);
}


void Breakable::copySplitVerts(Fragment* from, Fragment* to, const QiVec2& point, const QiVec2& normal)
{
	QiVec2 p0 = point - normal;
	QiVec2 p1 = point + normal;
	QiVec2 perp = perpendicular(normal);
	bool in = false;
	if (dot(perp, from->mVerts[0].vec2()-point) > 0.0f)
	{
		to->mVerts.add(from->mVerts[0]);
		to->mTexCoords.add(from->mTexCoords[0]);
		in = true;
	}
	int c = from->mVerts.getCount();
	float t = 0.0f;
	for(int i=1; i<=c; i++)
	{
		QiVec2 curr = from->mVerts[i % c].vec2();
		QiVec2 prev = from->mVerts[(i+c-1) % c].vec2();
		QiVec2 currT = from->mTexCoords[i % c];
		QiVec2 prevT = from->mTexCoords[(i+c-1) % c];
		if (dot(perp, curr-p0) > 0.0f)
		{
			if (!in)
			{
				//Add intersection
				QiVec2 result;
				if (LineIntersect(prev, curr, p0, p1, result, &t))
				{
					to->mVerts.add(result.vec3());
					to->mTexCoords.add(prevT + (currT-prevT)*t);
				}
			}
			to->mVerts.add(curr.vec3());
			to->mTexCoords.add(currT);
			in = true;
		}
		else
		{
			if (in)
			{
				//Add intersection
				QiVec2 result;
				if (LineIntersect(prev, curr, p0, p1, result, &t))
				{
					to->mVerts.add(result.vec3());
					to->mTexCoords.add(prevT + (currT-prevT)*t);
				}
			}
			in = false;
		}
	}

	to->mTdBody.pos = from->mTdBody.pos;
	to->mTdBody.rot = from->mTdBody.rot;
	//to->mTdBody.pos.x += QiRnd(-0.1f, 0.1f);
	QiVec2 m0 = to->getMidPoint();
	for(int i=0; i<to->mVerts.getCount(); i++)
		to->mVerts[i] -= m0.vec3();
	QiTransform3 tf((QiVec3&)to->mTdBody.pos, (QiQuat&)to->mTdBody.rot);
	QiVec3 pp = (tf.pos + tf.toParentVec(QiVec3(m0.x, m0.y, 0.0f)));
	to->mTdBody.pos = (tdVec&)pp;
	to->setMass();

	to->computeAabb();
	to->mTdAabb = tdSpaceInsertAabb(mTdSpace, &to->mAabb, (int)to);
}

void Breakable::addParticles(Fragment* f)
{
	QiTransform3 t(convert(f->mTdBody.pos), convert(f->mTdBody.rot));

	int count = 2;
	if (mLevel->mBreakableFragments > 100)
		count = 1;
	if (mLevel->mBreakableFragments > 200)
		count = 0;
		
	if (gGame->extraEffects())
	{
		count = 3;
		if (mLevel->mBreakableFragments > 200)
			count = 1;
		if (mLevel->mBreakableFragments > 400)
			count = 0;
	}

	for(int i=0; i<count; i++)
	{
		QiVec3 pos = t.toParentPoint(f->mVerts[i]);
		Fragment* p = QI_NEW Fragment();
		p->mAlive = true;
		if (i<2)
		p->mParticle = true;
		float q = 0.1f;
		p->mVerts.add(QiVec3(QiRnd(-q, q), QiRnd(-q, q), QiRnd(-q, q)));
		p->mTexCoords.add(f->mTexCoords[i]);
		p->mVerts.add(QiVec3(QiRnd(-q, q), QiRnd(-q, q), QiRnd(-q, q)));
		p->mTexCoords.add(f->mTexCoords[i]);
		p->mVerts.add(QiVec3(QiRnd(-q, q), QiRnd(-q, q), QiRnd(-q, q)));
		p->mTexCoords.add(f->mTexCoords[i]);

		p->mTdBody.pos = convert(pos);
		p->mTdBody.rot.z = 1.0f;
		p->mTdBody.linVel = f->mTdBody.linVel;
		p->mTdBody.angVel = convert(QiVec3::random(2.0f));

		p->computeAabb();
		p->mTdAabb = tdSpaceInsertAabb(mTdSpace, &p->mAabb, (int)p);
		p->setMass();

		mFragments.add(p);

		if (mSmoke)
			mParticles->spawn(pos,  convert(f->mTdBody.linVel)*QiRnd(0.0f, 0.7f));
	}
}

void Breakable::split(Fragment* f, int depth, const QiVec2& pos, const QiVec2& vel, float minArea)
{
	Fragment* dst[2];
	QiVec3 v = (QiVec3&)f->mTdBody.linVel;
	QiVec3 a = (QiVec3&)f->mTdBody.angVel;
	QiVec2 n = QiVec2::random(10.0f);

	n = f->getSplitDir();
	n += perpendicular(n) * QiRnd(-1.0f, 1.0f);
	n *= 100.0f;

	float w = 0.0f;
	float h = 0.0f;
	for(int i=0; i<f->mVerts.getCount(); i++)
	{
		QiVec3 p = f->mVerts[i];
		w = QiMax(QiAbs(p.x)*2, w);
		h = QiMax(QiAbs(p.y)*2, h);
	}

	if (mBreak == 1 && h > 0.3f )
		n.set(100,0);
	if (mBreak == 2 && w > 0.3f )
		n.set(0,100);

	split(f, &dst[0], &dst[1], n);

	QiVec3 fwd = mTransform.toParentVec(QiVec2(1, 0)).vec3();

	for(int i=0; i<2; i++)
	{
		QiVec3 p = convert(dst[i]->mTdBody.pos);
		QiVec3 d = p - pos.vec3();

		//Only measure distance in breakable plane
		//d = d - fwd * dot(fwd, d);

		QiTransform3 ft(convert(dst[i]->mTdBody.pos), convert(dst[i]->mTdBody.rot));
		QiVec3 obstaclePos = ft.toLocalPoint(pos.vec3() + fwd*dot(fwd,d));

		tdShape sh;
		tdShapeInitHull(&sh, dst[i]->mVerts.getCount(), sizeof(QiVec3), &dst[i]->mVerts[0].x);
		tdShape sp;
		tdShapeInitSphere(&sp, 0);
		tdDistanceQuery q = {0};
		q.shape0 = &sh;
		q.shape1 = &sp;
		tdVec op = convert(obstaclePos);
		tdTransformInitP(&q.transform, &op);
		tdDistanceResult r;
		tdDistance(mLevel->mTdContext, &q, &r);
		float dist = 0.0;
		if ((r.status & TD_RESULT_SEPARATION))
			dist = r.distance;

		float aff = QiClamp(1.3f - 1.0f*dist, 0.0f, 1.0f);

		QiVec3 v = vel.vec3() * (QiRnd(1.0f, 1.1f) * aff * aff) + QiVec3::random(2.0f)*aff;
		dst[i]->mTdBody.linVel =  convert(v);

		QiVec3 angVel = (cross(vel.vec3(), p-pos.vec3())*1.5f*aff + QiVec3::random(2.0f)) *aff*1.0f;
		dst[i]->mTdBody.angVel = convert(angVel);

		if (aff == 0.0f)
			dst[i]->mAlive = false;

		bool s = dst[i]->mAlive;

		if (depth < 3)
			s = true;

		if (dst[i]->getArea() < minArea)
			s = false;

		if (depth > 12)
			s = false;

		if (s)
			split(dst[i], depth+1, pos, vel, minArea);
		else
		{
			mFragments.add(dst[i]);
			if (dst[i]->mAlive && minArea < 0.3f)
				addParticles(dst[i]);
		}
	}
}

void Breakable::syncTransforms()
{
    for(int i=0; i<mFragments.getCount(); i++)
    {
        Fragment* f = mFragments[i];
        if (f->mAlive)
            f->mRenderTransform = QiTransform3((QiVec3&)f->mTdBody.pos, (QiQuat&)f->mTdBody.rot);
    }
}


