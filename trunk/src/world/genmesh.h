/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "qi_base.h"
#include "qi_math.h"

struct Mesh
{
	QiArrayInplace<QiVec3, 256> verts;
	QiArrayInplace<QiVec3, 256> normals;
	QiArrayInplace<QiVec2, 256> texCoords;
	QiArrayInplace<float, 256> light;
	QiArrayInplace<int, 256> indices;
	QiArrayInplace<int, 256> smooth;
};

void GenerateMesh(const QiArray<QiVec2>& inVerts, Mesh& outMesh, int iterCount, float facetSize, float z0, float z1, const QiString& mapping);

void SmoothMesh(Mesh& mesh, bool smooth);

bool Triangulate(const QiArray<QiVec2> &polygon, const QiArray<int> &loops, QiArray<int> &triangles);

void OffsetPolygon(const QiArray<QiVec2>& inVerts, float offset, QiArray<QiVec2>& outVerts, QiArray<int>& outLoops);

void ApplyShadow(const QiArray<QiVec2>& dstVerts, float dstZ0, float dstZ1, const QiArray<QiVec2>& srcVerts, float srcZ0, float srcZ1, Mesh& outMesh);

