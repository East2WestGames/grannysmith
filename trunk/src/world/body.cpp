/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "body.h"
#include "level.h"
#include "game.h"
#include "display.h"
#include "gfx.h"
#include "qi_geometry.h"
#include "profiling.h"
#include "device.h"

#include "Box2D.h"
#include "decompose.h"
#include "genmesh.h"
#include <vector>

#include "td/td.h"
#include "batch.h"
#include "dude.h"

const float MESH_BATCH_DIST = 20.0f;

Body::Body(Level* level) : Entity(level, BODY), mPhysBody(NULL), 
mMoveStrength(0.0f), mMoveMaxSpeed(QI_FLOAT_MAX), mRotateTarget(0.0f), mRotateStrength(0.0f), mRotateMaxSpeed(QI_FLOAT_MAX), 
mMotionJoint(NULL), mWater(false), mDynamic(0), mAwakeTimer(0.0f), mHidden(false), mBoundsMinZ(0.0f), mBoundsMaxZ(0.0f),
mSlip(false), mNoFx(false), mRoll(false), mSurface(GENERIC)
{
	mProperties.add("dynamic", "0");
	mProperties.add("friction", "1.0");
	mProperties.add("restitution", "0");
	mProperties.add("density", "1.0");
	mProperties.add("category", "4");
	mProperties.add("mask", "255");
	mProperties.add("color", "0.9 0.8 0.7");
	mProperties.add("z", "0");
	mProperties.add("depth", "1.0");
	mProperties.add("hidden", "0");
	mProperties.add("fixedRotation", "0");
	mProperties.add("edge", "0 0");
	mProperties.add("texture0", "gfx/tin_roof.jpg 0.3 0.3");
	mProperties.add("texture1", "");
	mProperties.add("mapping", "");
	mProperties.add("curve", "0");
	mProperties.add("dynamicShadow", "0");
	mProperties.add("extrarot", "0 0 0");
	mProperties.add("surface", "");

	mVbFormat.clear();
	mVbFormat.addField("aPosition", QiVertexFormat::FLOAT32, 3);
	mVbFormat.addField("aTexCoord", QiVertexFormat::FLOAT32, 2);
	mVbFormat.addField("aNormal", QiVertexFormat::INT16, 3);
	mVbFormat.addField("aLight", QiVertexFormat::FLOAT32, 1);
	mVb.init(mVbFormat, 0);
	mIb.init(0);

	mShadowFmt.addField("aPosition", QiVertexFormat::FLOAT32, 3);
	mShadowFmt.addField("aAlpha", QiVertexFormat::FLOAT32, 1);
	mShadowVb.init(mShadowFmt, 0);
	mShadowIb.init(0);

	loadProperties();
}


Body::~Body()
{
	if (mPhysBody)
		mLevel->mPhysWorld->DestroyBody(mPhysBody);
	for(int i=0; i<mConvexes.getCount(); i++)
		QI_DELETE(mConvexes[i]);
}


BodyConvex::BodyConvex(Body* body)
{
	this->body = body;
	count = 0;
	last = 0;

	tdAabb aabb;
	aabb.lower.x = aabb.lower.y = 0;
	aabb.upper.x = aabb.upper.y = 0;
	mTdAabb = tdSpaceInsertAabb(body->mLevel->mTdSpace, &aabb, (int)this);
}


BodyConvex::~BodyConvex()
{
	tdSpaceRemoveAabb(body->mLevel->mTdSpace, mTdAabb);
}


void Body::update()
{
	if (mDynamic==0)
		return;

    if (mPhysBody && !mPhysBody->IsAwake())
        return;
    
    if (!mPhysBody)
        return;
        
    PROFILE_ZONE("Body::update");

	mTransform3 = QiTransform3(mTransform);
	mTransform3.pos.z = mZ;
	mTransform3.rot *= mRot;

	computeBounds();

	if (mPhysBody)
	{
		mTransform.pos.set(mPhysBody->GetPosition().x, mPhysBody->GetPosition().y);
		mTransform.rot = mPhysBody->GetAngle();

		if (mMotionJoint && mMoveStrength == 0.0f && mRotateStrength == 0.0f)
		{
			mLevel->mPhysWorld->DestroyJoint(mMotionJoint);
			mMotionJoint = NULL;
		}
		else if (!mMotionJoint && (mMoveStrength != 0.0f || mRotateStrength != 0.0f))
		{
			b2MotionJointDef def;
			def.collideConnected = true;
			def.bodyA = mLevel->mPhysGround;
			def.bodyB = mPhysBody;
			mMotionJoint = (b2MotionJoint*)mLevel->mPhysWorld->CreateJoint(&def);
		}

		if (mMotionJoint)
		{
			float f = mMoveStrength * mPhysBody->GetMass();
			float fa = mRotateStrength * mPhysBody->GetMass();
			QiVec2 v = (mMoveTarget - mTransform.pos) / gGame->mTimeStep * 0.5f;
			mMotionJoint->m_up = b2Vec2(0,1);
			mMotionJoint->m_tan = b2Vec2(1,0);
			float l = length(v);
			if (l > 0.0f)
			{
				//mMotionJoint->m_up = vec(v/l);
				//mMotionJoint->m_tan = vec(perpendicular(vec(mMotionJoint->m_up)));
			}
		
			float ang = QiAngle(mRotateTarget, mTransform.rot) / gGame->mTimeStep * 0.5f;
			mMotionJoint->m_point = vec(mTransform.pos); 
			mMotionJoint->m_upTarget = QiClamp(dot(v, vec(mMotionJoint->m_up)), -mMoveMaxSpeed, mMoveMaxSpeed);
			mMotionJoint->m_upMin = -f;
			mMotionJoint->m_upMax = f;
			mMotionJoint->m_tanTarget = QiClamp(dot(v, vec(mMotionJoint->m_tan)), -mMoveMaxSpeed, mMoveMaxSpeed);
			mMotionJoint->m_tanMin = -f;
			mMotionJoint->m_tanMax = f;
			mMotionJoint->m_rotTarget = QiClamp(ang, -mRotateMaxSpeed, mRotateMaxSpeed);
			mMotionJoint->m_rotMin = -fa;
			mMotionJoint->m_rotMax = fa;
		}
	}

	if (mPhysBody->IsActive() && mParent == NULL &&  mTransform.pos.x < mLevel->mDude->mTransform.pos.x-30.0f)
	{
		mPhysBody->SetActive(false);
		mPhysBody->SetAwake(false);
	}
}


void Body::loadProperties()
{
	Entity::loadProperties();

	float rx = mProperties.getString("extrarot").getWord(0).toFloat();
	float ry = mProperties.getString("extrarot").getWord(1).toFloat();
	float rz = mProperties.getString("extrarot").getWord(2).toFloat();
	mRot = QiQuat(QiVec3::X, rx) * QiQuat(QiVec3::Y, ry) * QiQuat(QiVec3::Z, rz);

	mZ = mProperties.getFloat("z");
	mDepth = mProperties.getFloat("depth");
	mDynamic = mProperties.getInt("dynamic");
	mHidden = mProperties.getBool("hidden");

	QiString surf = mProperties.getString("surface");
	mSlip = surf.contains("slip");
	mNoFx = surf.contains("nofx");
	mRoll = surf.contains("roll");
	if (surf.contains("grass"))
		mSurface = GRASS;
	if (surf.contains("gravel"))
		mSurface = GRAVEL;
	if (surf.contains("mud"))
		mSurface = MUD;
	if (surf.contains("water"))
		mSurface = WATER;
	if (surf.contains("wood"))
		mSurface = WOOD;
	if (surf.contains("metal"))
		mSurface = METAL;
	if (surf.contains("oil"))
		mSurface = OIL;
	if (surf.contains("rock"))
		mSurface = ROCK;

	//Bah! Stupid... but we need the call twice to get template values
	Entity::loadProperties();
}


void Body::init()
{
	updateGeometry();
}


void Body::onReset()
{
	if (!mPhysBody)
		return;

	mAwakeTimer = 0.0f;
	mMoveStrength = 0.0f;
	mRotateStrength = 0.0f;

	if (mDynamic != 0)
	{
		mPhysBody->SetLinearVelocity(b2Vec2(0.0f, 0.0f));
		mPhysBody->SetAngularVelocity(0.0f);
		Entity::loadProperties();
		b2BodyType type = (mDynamic==1 || mDynamic==3) ? b2_dynamicBody : b2_staticBody;
		if (mPhysBody->GetType() != type)
			mPhysBody->SetType(type);
	}

	int category = mProperties.getInt("category");
	int mask = mProperties.getInt("mask");
	b2Fixture* f = mPhysBody->GetFixtureList();
	while(f)
	{
		b2Filter filter = f->GetFilterData();
		filter.categoryBits = category;
		filter.maskBits = mask;
		f->SetFilterData(filter);
		f = f->GetNext();
	}
}


void Body::setTransform(const QiTransform2& t)
{
	Entity::setTransform(t);
	if (mPhysBody)
		mPhysBody->SetTransform((b2Vec2&)mTransform.pos, mTransform.rot);

	mTransform3 = QiTransform3(mTransform);
	mTransform3.pos.z = mZ;
	mTransform3.rot *= mRot;

	computeBounds();
}


void Body::localTransform(const class QiTransform2& t)
{
	for(int i=0; i<mPolygon.mPoints.getCount(); i++)
		mPolygon.mPoints[i] = t.toParentPoint(mPolygon.mPoints[i]);
	for(int i=0; i<mControlPoints.getCount(); i++)
	{
		mControlPoints[i].mPoint = t.toParentPoint(mControlPoints[i].mPoint);
		mControlPoints[i].mHandle0 = t.toParentPoint(mControlPoints[i].mHandle0);
		mControlPoints[i].mHandle1 = t.toParentPoint(mControlPoints[i].mHandle1);
	}
	updateGeometry();
}


inline QiVec2 getBezierPoint(const QiVec2& p0, const QiVec2& p1, const QiVec2& p2, const QiVec2& p3, float t)
{
	float u = 1.0f - t;
	float tt = t*t;
	float uu = u*u;
	float uuu = u*u*u;
	float ttt = t*t*t;
	return p0*uuu + p1*(3*uu*t) + p2*(3*u*tt) + p3*ttt;
}


inline float distanceToSegmentSq(const QiVec2& p0, const QiVec2& p1, const QiVec2& p)
{
	QiVec2 v = p - p0;
	float length;
	QiVec2 d = normalize(p1-p0, length);
	if (length==0.0f)
		return lengthSquared(p - p0);
	float tt = dot(d, v);
	if (tt <= 0.0f)
		return lengthSquared(p - p0);
	else if (tt >= length)
		return lengthSquared(p1 - p);
	else
		return lengthSquared(v-d*dot(v,d));
}


int addVertex(Polygon2D& poly, const QiVec2& p0, const QiVec2& p1, const QiVec2& p2, const QiVec2& p3, float t0, float t1, int i0, int i1)
{
	float limit = 0.05f;
	float t = (t0+t1)*0.5f;
	QiVec2 p = getBezierPoint(p0, p1, p2, p3, t);
	QiVec2 pa = getBezierPoint(p0, p1, p2, p3, t0*0.25f+t1*0.75f);
	QiVec2 pb = getBezierPoint(p0, p1, p2, p3, t0*0.75f+t1*0.25f);
	float d = distanceToSegmentSq(poly.mPoints[i0], poly.mPoints[i1], p);
	float da = distanceToSegmentSq(poly.mPoints[i0], poly.mPoints[i1], pa);
	float db = distanceToSegmentSq(poly.mPoints[i0], poly.mPoints[i1], pb);
	if ((d > limit*limit || da > limit*limit || db > limit*limit) && t1-t0 > 0.01f)
	{
		poly.mPoints.insertAt(i1, p);
		int addedBefore = addVertex(poly, p0, p1, p2, p3, t0, t, i0, i1);
		int addedAfter = addVertex(poly, p0, p1, p2, p3, t, t1, i1+addedBefore, i1+addedBefore+1);
		return 1 + addedBefore + addedAfter;
	}
	else
		return 0;
}


void Body::generatePolygon()
{
	mVertexControlPoint.clear();
	mPolygon.mPoints.clear();
	int count = mControlPoints.getCount();
	if (mProperties.getInt("curve") == 2)
		count--;
	for(int i=0; i<count; i++)
	{
		ControlPoint& c = mControlPoints[i];
		ControlPoint& cNext = mControlPoints[(i+1)%mControlPoints.getCount()];
		const QiVec2& p0 = c.mPoint;
		const QiVec2& p1 = c.mHandle1;
		const QiVec2& p2 = cNext.mHandle0;
		const QiVec2& p3 = cNext.mPoint;
		mPolygon.mPoints.add(getBezierPoint(p0, p1, p2, p3, 0.0f));
		mPolygon.mPoints.add(getBezierPoint(p0, p1, p2, p3, 1.0f));
		addVertex(mPolygon, p0, p1, p2, p3, 0.0f, 1.0f, mPolygon.mPoints.getCount()-2, mPolygon.mPoints.getCount()-1);
		if (i != count-1 || count == mControlPoints.getCount())
			mPolygon.mPoints.removeLast(); // Otherwise we will have duplets
		while(mVertexControlPoint.getCount() < mPolygon.mPoints.getCount())
			mVertexControlPoint.add(i);
	}
	if (mProperties.getInt("curve") != 1)
	{
		float thickness = mProperties.getString("curve").getWord(1).toFloat();
		QiArray<QiVec2> tmp = mPolygon.mPoints;
		QiArray<int> tmp2 = mVertexControlPoint;
		mVertexControlPoint.clear();
		mPolygon.mPoints.clear();
		for(int i=0; i<tmp.getCount(); i++)
		{
			int ip = QiMax(i-1, 0);
			int in = QiMin(i+1, tmp.getCount()-1);
			QiVec2 n = -normalize(perpendicular(tmp[in]-tmp[ip]));
			mPolygon.mPoints.add(tmp[i] + n*thickness);
			mVertexControlPoint.add(tmp2[i]);
		}
		for(int i=tmp.getCount()-1; i>=0; i--)
		{
			int ip = QiMax(i-1, 0);
			int in = QiMin(i+1, tmp.getCount()-1);
			QiVec2 n = normalize(perpendicular(tmp[in]-tmp[ip]));
			mPolygon.mPoints.add(tmp[i] + n*thickness);
			mVertexControlPoint.add(tmp2[i]);
		}
	}
}


void Body::updateGeometry()
{
	if (mProperties.getBool("curve"))
		generatePolygon();	

	updatePhysics();

	computeBounds();

	updateMesh();
}


void Body::updatePhysics()
{
	PROFILE_FUNC();

	if (mDynamic==0 && mProperties.getInt("category") == 0)
		return;

	if (mPhysBody)
		mLevel->mPhysWorld->DestroyBody(mPhysBody);
	b2BodyDef bd;
	bd.position = (b2Vec2&)mTransform.pos;
	bd.angle = mTransform.rot;
	bd.type = (mDynamic == 1 || mDynamic == 3)  ? b2_dynamicBody : b2_staticBody;
	bd.fixedRotation = mProperties.getBool("fixedRotation");
	bd.angularDamping = 1.0f;
	bd.userData = this;
	bd.allowSleep = true;
	bd.awake = false;
	mPhysBody = mLevel->getPhysWorld()->CreateBody(&bd);

	if (mPolygon.mPoints.getCount() > 0 && gGame->getState() != Game::EDIT)
	{
		//Decompose concave polygon into convex part (using Box2D contrib)
		std::vector< QiVec2 > vertsIn;
		std::vector< std::vector<QiVec2> > vertsOut;
		for(int i=0; i<mPolygon.mPoints.getCount(); i++)
			vertsIn.push_back(mPolygon.mPoints[i]);
		{
			PROFILE_ZONE("Decompose");
			Decompose(vertsIn, vertsOut);
		}

		mConvexes.clear();
		for(unsigned int i=0; i<vertsOut.size(); i++)
		{
			BodyConvex* bc = QI_NEW BodyConvex(this);
			mConvexes.add(bc);

			std::vector<b2Vec2> pv;
			for(unsigned int j=0; j<vertsOut[i].size() && j<(unsigned int)MAX_CONVEX_VERTS; j++)
			{
				pv.push_back(b2Vec2(vertsOut[i][j].x, vertsOut[i][j].y));
				bc->verts[bc->count] = vertsOut[i][j];
				bc->mTdVerts[bc->count*2+0] = QiVec3(vertsOut[i][j].x, vertsOut[i][j].y, -mDepth*0.5f);
				bc->mTdVerts[bc->count*2+1] = QiVec3(vertsOut[i][j].x, vertsOut[i][j].y, mDepth*0.5f);
				bc->count++;
			}

			b2PolygonShape s;
			int c = bc->count;
			if (c > b2_maxPolygonVertices)
			{
				QI_PRINT(" *** TOO MANY VERTICES IN CONVEX POLYGON *** ");
				c = b2_maxPolygonVertices;
			}
			s.Set(&pv[0], c);

			b2FixtureDef fd;
			fd.filter.categoryBits = mProperties.getInt("category");
			fd.filter.maskBits = mProperties.getInt("mask");
			fd.shape = &s;
			fd.density = mProperties.getFloat("density");
			fd.friction = mProperties.getFloat("friction");
			fd.restitution = mProperties.getFloat("restitution");
			fd.userData = bc;
 			mPhysBody->CreateFixture(&fd);
		}
	}
}

void Body::updateMesh()
{
	PROFILE_FUNC();

	mVb.clear();
	mIb.clear();

	if (mHidden)
		return;

	Mesh mesh;
	QiArray<QiVec2> inVerts;

	for(int i=0; i<mPolygon.mPoints.getCount(); i++)
		inVerts.add(mPolygon.mPoints[i]);

	for(int i=0; i<1; i++)
	{
		QiString t = mProperties.getString(QiString("texture") + i);
		t.trim();
		if (t != "")
		{
			if (t == "water")
			{
				mTexture[i] = mLevel->mResMan->acquireTexture("gfx/ripples.jpg");
				mTexture[i].getTexture()->enableRepeat(true);
				mWater = true;
			}
			else
			{
				mTexture[i] = mLevel->mResMan->acquireTexture(t.getWord(0));
				mTexture[i].getTexture()->enableRepeat(true);
				if (t.getWordCount() > 1)
				{
					mTexScale[i].set(t.getWord(1).toFloat(), t.getWord(2).toFloat());
					mTexOffset[i].set(t.getWord(3).toFloat(), t.getWord(4).toFloat());
				}
				else
				{
					mTexScale[i].set(1,1);
					mTexOffset[i].set(0,0);
				}
			}
		}
		else
			mTexture[i].release();
	}

	QiString edge = mProperties.getString("edge"); 
	int iterations = QiClamp(edge.getWord(0).toInt(), 0, 3);
	float size = edge.getWord(1).toFloat();
	if (iterations==0)
		size = 0;
	GenerateMesh(inVerts, mesh, iterations, size, -mDepth*0.5f, mDepth*0.5f, mProperties.getString("mapping"));

	QiVertexBuffer* vb = NULL;
	QiIndexBuffer* ib = NULL;

	QiTransform3 xform;
	if (gGame->getState() != Game::EDIT)
	{
		float d2 = MESH_BATCH_DIST * MESH_BATCH_DIST;
		for(int i=0; i<mLevel->mEntities.getCount(); i++)
		{
			if (mLevel->mEntities[i]->getType() == BODY)
			{
				Body* b = (Body*)mLevel->mEntities[i];
				if (b->mVb.getCount() > 0 && 
					b->mTexture[0].getTexture() == mTexture[0].getTexture() && 
					b->mTexture[1].getTexture() == mTexture[1].getTexture() && 
					b->mDynamic == 0 && mDynamic == 0 && 
					b->mProperties.getColor("color") == mProperties.getColor("color") && 
					lengthSquared(mTransform.pos-b->mTransform.pos) < d2)
				{
					b->mergeBounds(this);
					b->mBoundsMinZ = QiMin(b->mBoundsMinZ, mBoundsMinZ);
					b->mBoundsMaxZ = QiMax(b->mBoundsMaxZ, mBoundsMaxZ);
					//invalidateBounds();
					vb = &b->mVb;
					ib = &b->mIb;
					xform = b->mTransform3.toLocal(mTransform3);
					break;
				}
			}
		}
	}

	if (!vb)
	{
		vb = &mVb;
		ib = &mIb;
	}

	vb->redim(vb->getCount() + mesh.verts.getCount());
	ib->redim(ib->getCount() + mesh.indices.getCount());

	int c = vb->getCount();
	for(int i=0; i<mesh.verts.getCount(); i++)
	{
		QiVec3& p = mesh.verts[i];
		QiVec3& n = mesh.normals[i];
		QiVec2 t = mesh.texCoords[i];
		t = multiplyPerElement(t, mTexScale[0]) + mTexOffset[0];
		vb->vertex();
		vb->add(xform.toParentPoint(p));
		vb->add(t);
		vb->add(xform.toParentVec(n));
		vb->add(mesh.light[i]);
	}
	for(int i=0; i<mesh.indices.getCount(); i+=3)
		ib->triangle(c+mesh.indices[i+0], c+mesh.indices[i+1], c+mesh.indices[i+2]);
}


void Body::onStart()
{
	if (mDynamic==0)
	{
		if (mIb.getCount() > 0)
		{
			mVb.makeVbo();
			mIb.makeIbo();
		}
		if (mShadowIb.getCount() > 0)
		{
			mShadowVb.makeVbo();
			mShadowIb.makeIbo();
		}
	}
}


void Body::render()
{
    PROFILE_ZONE("Body::render");
	QiColor col = mProperties.getColor("color");
	if (col.a >= 1.0f)
		render(false);
}


void Body::renderTransparent()
{
    PROFILE_ZONE("Body::renderTransparent");

	QiColor col = mProperties.getColor("color");
	if (col.a > 0.0f && col.a < 1.0f)
		render(true);

	if (mDynamic!=0 && mProperties.getInt("dynamicShadow") == 2)
		computeShadow(NULL);

	if (mShadowIb.getCount() > 0 && PROFILE_PARAM("render/body/shadow", true))
	{
        PROFILE_ZONE("Shadow");
		QiRenderState state;
		state.blendMode = state.BLEND;
		state.depthTest = true;
		state.depthMask = false;
		state.cullFace = true;
		state.shader = gGame->mGfx->mShadowShader.getShader();
		gGame->mRenderer->setState(state);
		gGame->mRenderer->drawTriangles(QiTransform3(mTransform), &mShadowVb, &mShadowIb);
	}
}


void Body::render(bool transparent)
{
	if (mIb.getCount() == 0 || !PROFILE_PARAM("render/body/mesh", true))
		return;

	QiRenderState state;
	state.color = mProperties.getColor("color");

#if 0
	//Blink rails
	if ((mProperties.getInt("mask") & 256) != 0)
	{
		float t = QiSin(mLevel->mSimTime*10.0f)*0.2f + 0.2f;
		state.color.r += t;
		state.color.g += t;
		state.color.b += t;
	}
#endif

	state.depthTest = true;
	if (state.color.a < 1.0f)
	{
		state.blendMode = state.BLEND;
		state.depthMask = false;
	}
	state.cullFace = true;
	int tex = 0;
	for(int i=0; i<1; i++)
	{
		state.texture[i] = mTexture[i].getTexture();
		if (state.texture[i])
			tex++;
	}
	if (tex==0)
		state.shader = gGame->mGfx->mBodyShader.getShader();
	if (tex==1)
		state.shader = gGame->mGfx->mBodyShaderTex.getShader();
	if (mWater)
		state.shader = gGame->mGfx->mWaterShader.getShader();
	gGame->mRenderer->setState(state);
	if (mWater)
		state.shader->setUniform("uTime", mLevel->mSimTime);
	
	state.shader->setUniform2("uCameraPos", &gGame->mDisplay->mCameraPos.x);
	gGame->mRenderer->drawTriangles(mTransform3, &mVb, &mIb);
}


QiVec2 Body::getVelocity() const
{
	if (!mPhysBody)
		return QiVec2(0,0);
	return vec(mPhysBody->GetLinearVelocity());
}


void Body::makeCurve()
{
	for(int i=0; i<mPolygon.mPoints.getCount(); i++)
	{
		ControlPoint cp;
		cp.mPoint = mPolygon.mPoints[i];
		mControlPoints.add(cp);
	}

	for(int i=0; i<mControlPoints.getCount(); i++)
	{
		ControlPoint& cPrev = mControlPoints[(i+mControlPoints.getCount()-1)%mControlPoints.getCount()];
		ControlPoint& c = mControlPoints[i];
		ControlPoint& cNext = mControlPoints[(i+1)%mControlPoints.getCount()];
		c.mHandle0 = c.mPoint*0.5f + cPrev.mPoint*0.5f;
		c.mHandle1 = c.mPoint*0.5f + cNext.mPoint*0.5f;
		QiVec2 tan = c.mHandle1-c.mHandle0;
		c.mHandle0 = c.mPoint - tan*0.5f;
		c.mHandle1 = c.mPoint + tan*0.5f;

	}

	mProperties.setBool("curve", true);
}


void Body::computeShadow(Batch* batch)
{
	if (gGame->getState() == Game::EDIT)
		return;

	if (mDynamic!=0 && mProperties.getInt("dynamicShadow") == 0)
		return;

	if (mProperties.getInt("dynamicShadow") == -1 || lengthSquared(mProperties.getVec3("extrarot")) > 0.1f)
		return;

	if (mHidden && mParent == NULL)
		return;
    
	Mesh mesh;
	float maxShadowSize = 1.0f;

	QiArrayInplace<Body*, 32> bodies;
	mLevel->collectBodies(mBoundsMin-QiVec2(maxShadowSize, maxShadowSize), mBoundsMax+QiVec2(maxShadowSize, maxShadowSize), bodies);

	for(int i=0; i<bodies.getCount(); i++)
	{
		Body* b = bodies[i];
		if (b == this)
			continue;

		//The shadow of a dynamic object should not belong to a static
		if (mDynamic==0 && b->mDynamic!=0)
			continue;

		if (b->mProperties.getInt("dynamicShadow") == -1 || lengthSquared(b->mProperties.getVec3("extrarot")) > 0.1f)
			continue;

		if (b->mHidden)
			continue;

		//Apply shadow on this body
		QiTransform2 t = mTransform.toLocal(b->mTransform);
		QiArrayInplace<QiVec2, 128> localPoints;
		for(int i=0; i<b->mPolygon.mPoints.getCount(); i++)
			localPoints.add(t.toParentPoint(b->mPolygon.mPoints[i]));
		ApplyShadow(mPolygon.mPoints, mZ-mDepth*0.5f, mZ+mDepth*0.5f, localPoints, b->mZ-b->mDepth*0.5f, b->mZ+b->mDepth*0.5f, mesh);

		//Apply shadows on other static bodies if this is dynamic
		if (mDynamic!=0 && b->mDynamic==0)
			ApplyShadow(localPoints, b->mZ-b->mDepth*0.5f, b->mZ+b->mDepth*0.5f, mPolygon.mPoints, mZ-mDepth*0.5f, mZ+mDepth*0.5f, mesh);
	}

	if (batch && mDynamic==0)
	{
		QiTransform3 t = mTransform3;
		t.pos.z = 0.0f;

		int c = batch->mVb.getCount();
		batch->mVb.redim(batch->mVb.getCount() + mesh.verts.getCount());
		for(int i=0; i<mesh.verts.getCount(); i++)
		{
			batch->mVb.vertex();
			batch->mVb.addFast(t.toParentPoint(mesh.verts[i]));
			batch->mVb.addFast(1.0f - mesh.light[i]);
		}
		for(int i=0; i<mesh.indices.getCount(); i+=3)
		{
			int i0 = mesh.indices[i+0];
			int i1 = mesh.indices[i+1];
			int i2 = mesh.indices[i+2];
			QiVec3 p0 = t.toParentPoint(mesh.verts[i0]);
			QiVec3 p1 = t.toParentPoint(mesh.verts[i1]);
			QiVec3 p2 = t.toParentPoint(mesh.verts[i2]);
			batch->add(c+i0, c+i1, c+i2, p0, p1, p2);
		}
	}
	else
	{
		mShadowVb.clear();
		mShadowIb.clear();

		mShadowVb.redim(mesh.verts.getCount());
		for(int i=0; i<mesh.verts.getCount(); i++)
		{
			mShadowVb.vertex();
			mShadowVb.addFast(mesh.verts[i]);
			mShadowVb.addFast(1.0f - mesh.light[i]);
		}
		mShadowIb.redim(mesh.indices.getCount());
		for(int i=0; i<mesh.indices.getCount(); i+=3)
			mShadowIb.triangle(mesh.indices[i+0], mesh.indices[i+1], mesh.indices[i+2]);
	}
}


void Body::computeBounds()
{
	//Compute all td aabbs
	for(int i=0; i<mConvexes.getCount(); i++)
	{
		tdShape s;
		tdShapeInitHull(&s, mConvexes[i]->count*2, sizeof(QiVec3), (float*)mConvexes[i]->mTdVerts);
		tdAabb aabb;
		tdBoundingBoxQuery query = {0};
		query.padding = 0.0f;
		query.shape = &s;
		QiTransform3 t(mTransform);
		tdTransformInitPQ(&query.transform, (tdVec*)&t.pos, (tdQuat*)&t.rot);
		tdBoundingBoxAabb(mLevel->mTdContext, &query, &aabb);
		tdSpaceUpdateAabb(mLevel->mTdSpace, mConvexes[i]->mTdAabb, &aabb);
	}

	mBoundsMin.set(QI_FLOAT_MAX, QI_FLOAT_MAX);
	mBoundsMax.set(-QI_FLOAT_MAX, -QI_FLOAT_MAX);
	if (mTransform3.rot.w < 0.99f)
	{
		for(int i=0; i<mPolygon.mPoints.getCount(); i++)
		{
			//Need to take full 3d transform into consideration for heavily rotated objects
			QiVec3 p = mTransform3.toParentPoint(QiVec3(mPolygon.mPoints[i].x, mPolygon.mPoints[i].y, mZ - mDepth*0.5f));
			mBoundsMin.x = QiMin(mBoundsMin.x, p.x);
			mBoundsMin.y = QiMin(mBoundsMin.y, p.y);
			mBoundsMax.x = QiMax(mBoundsMax.x, p.x);
			mBoundsMax.y = QiMax(mBoundsMax.y, p.y);
			p = mTransform3.toParentPoint(QiVec3(mPolygon.mPoints[i].x, mPolygon.mPoints[i].y, mZ + mDepth*0.5f));
			mBoundsMin.x = QiMin(mBoundsMin.x, p.x);
			mBoundsMin.y = QiMin(mBoundsMin.y, p.y);
			mBoundsMax.x = QiMax(mBoundsMax.x, p.x);
			mBoundsMax.y = QiMax(mBoundsMax.y, p.y);
		}
	}
	else
	{
		for(int i=0; i<mPolygon.mPoints.getCount(); i++)
		{
			QiVec2 p = mTransform.toParentPoint(mPolygon.mPoints[i]);
			mBoundsMin.x = QiMin(mBoundsMin.x, p.x);
			mBoundsMin.y = QiMin(mBoundsMin.y, p.y);
			mBoundsMax.x = QiMax(mBoundsMax.x, p.x);
			mBoundsMax.y = QiMax(mBoundsMax.y, p.y);
		}
	}
	mBoundsMinZ = mZ-mDepth*0.5f;
	mBoundsMaxZ = mZ+mDepth*0.5f;
}

struct Quad
{
	QiVec3 pos[4];
	QiVec2 texCoord[4];
};

struct QuadComparator
{
	int compare(const Quad& a, const Quad& b) const
	{
		if (a.pos[0].z < b.pos[0].z)
			return -1;
		else if (a.pos[0].z > b.pos[0].z)
			return 1;
		else
			return 0;
	}
};

void Body::addFoliage(Batch* batch)
{
	if (!mProperties.getString("texture0").contains("grass"))
		return;

	Level::DecalInfo info[6];
	bool b=true;
	b &= mLevel->findDecal("foliage0", info[0]);
	b &= mLevel->findDecal("foliage1", info[1]);
	b &= mLevel->findDecal("foliage2", info[2]);
	b &= mLevel->findDecal("foliage3", info[3]);
	
	if (!b)
		return;
	mLevel->findDecal("tallgrass0", info[4]);
	mLevel->findDecal("tallgrass1", info[5]);

	QiVec2 size[6];
	for(int i=0; i<6; i++)
	{
		size[i].x = (info[i].texMax.x-info[i].texMin.x);
		size[i].y = (info[i].texMax.y-info[i].texMin.y);
		if (size[i].y != 0.0f)
		{
		size[i].x /= size[i].y;
		size[i].y /= size[i].y;
	}
	}

	QiArray<Quad> quads;
	for(int i=0; i<mPolygon.mPoints.getCount(); i++)
	{
		QiVec2 p0 = mPolygon.mPoints[i];
		QiVec2 p1 = mPolygon.mPoints[(i+1)%mPolygon.mPoints.getCount()];

		QiVec2 n = -perpendicular(normalize(p1-p0));
		if (n.y < 0.3f)
			continue;

		int t = int(length(p1-p0)*1.0f);
		if (gGame->extraEffects())
			t = int(length(p1-p0)*2.0f);
		
		for(int j=0; j<t; j++)
		{
			int g = QiRnd(0, 4);
			QiVec2 p = (p1-p0)*QiRnd(0.0f, 1.0f) + p0;
            
			float ang = QiATan2(n.y, n.x) - QI_HALF_PI;
			QiVec2 s=size[g];
			s *= QiRndNormal(0.8f, 1.2f);
			p -= n*s.y*0.1f;
			QiTransform2 t = QiTransform2(p, ang);
			s *= 1.0f + QiSin(t.pos.x/3.0f)*0.3f;

			if (!mPolygon.contains(t.toParentPoint(QiVec2(-s.x, 0.0f))) || !mPolygon.contains(t.toParentPoint(QiVec2(s.x, 0.0f))))
				continue;

			float z = QiRnd(-mDepth*0.5f, -mDepth*0.5f+0.1f);
			Quad q;
			{
				QiVec2 p0 = t.toParentPoint(QiVec2(-s.x*0.5f, 0.0f));
				QiVec2 p1 = t.toParentPoint(QiVec2(s.x*0.5f, 0.0f));
				QiVec2 p2 = t.toParentPoint(QiVec2(s.x*0.5f, s.y));
				QiVec2 p3 = t.toParentPoint(QiVec2(-s.x*0.5f, s.y));
				q.pos[0].set(p0.x, p0.y, z); 
				q.pos[1].set(p1.x, p1.y, z); 
				q.pos[2].set(p2.x, p2.y, z); 
				q.pos[3].set(p3.x, p3.y, z); 
				q.texCoord[0].set(info[g].texMin.x, info[g].texMax.y);
				q.texCoord[1].set(info[g].texMax.x, info[g].texMax.y);
				q.texCoord[2].set(info[g].texMax.x, info[g].texMin.y);
				q.texCoord[3].set(info[g].texMin.x, info[g].texMin.y);
				quads.add(q);
			}
		}
		
		if (gGame->extraEffects() && mZ < 1.0f)
		{
			int t = int(length(p1-p0)*4.0f);
			for(int j=0; j<t; j++)
			{
				int g = QiRnd(4, 6);
				QiVec2 p = (p1-p0)*QiRnd(0.0f, 1.0f) + p0;

				float ang = QiATan2(n.y, n.x) - QI_HALF_PI;
				QiVec2 s=size[g];
				s *= QiRndNormal(0.8f, 1.2f);
				p -= n*s.y*0.1f;
				QiTransform2 t = QiTransform2(p, ang);

				if (!mPolygon.contains(t.toParentPoint(QiVec2(-s.x, 0.0f))) || !mPolygon.contains(t.toParentPoint(QiVec2(s.x, 0.0f))))
					continue;

				float z = QiRnd(-mDepth*0.5f, -mDepth*0.5f+0.1f);
				z -= 1.0f;
				s *= 1.5f + QiSin(t.pos.x/5.0f)*0.6f;
				t.pos.y -= 0.6f;

				Quad q;
				{
					QiVec2 p0 = t.toParentPoint(QiVec2(-s.x*0.5f, 0.0f));
					QiVec2 p1 = t.toParentPoint(QiVec2(s.x*0.5f, 0.0f));
					QiVec2 p2 = t.toParentPoint(QiVec2(s.x*0.5f, s.y));
					QiVec2 p3 = t.toParentPoint(QiVec2(-s.x*0.5f, s.y));
					q.pos[0].set(p0.x, p0.y, z); 
					q.pos[1].set(p1.x, p1.y, z); 
					q.pos[2].set(p2.x, p2.y, z); 
					q.pos[3].set(p3.x, p3.y, z); 
					q.texCoord[0].set(info[g].texMin.x, info[g].texMax.y);
					q.texCoord[1].set(info[g].texMax.x, info[g].texMax.y);
					q.texCoord[2].set(info[g].texMax.x, info[g].texMin.y);
					q.texCoord[3].set(info[g].texMin.x, info[g].texMin.y);
					quads.add(q);
	}
			}
		}
	}
	
	QiSort(quads.getData(), quads.getCount(), QuadComparator());

	if (batch)
	{
		QiColor col = mProperties.getColor("color");
		for(int i=0; i<quads.getCount(); i++)
		{
			int c = batch->mVb.getCount();
			float f = QiRnd(0.2f, 0.8f);
			QiVec3 wp[4];
			for(int j=0; j<4; j++)
			{
				wp[j] = mTransform3.toParentPoint(quads[i].pos[j]);
				batch->mVb.vertex();
				batch->mVb.addFast(wp[j]);
				batch->mVb.add(quads[i].texCoord[j]);
				batch->mVb.add(col.r/2, col.g/2, col.b/2, col.a/2);
				batch->mVb.add(j > 1 ? 0.4f : 0.0f);
				batch->mVb.add(f);
			}
			batch->add(c+0, c+1, c+2, wp[0], wp[1], wp[2]);
			batch->add(c+0, c+2, c+3, wp[0], wp[2], wp[3]);
		}
	}
}

