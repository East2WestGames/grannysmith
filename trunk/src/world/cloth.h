/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "qi_math.h"
#include "qi_gfx.h"
#include "resman.h"

class Cloth
{
public:
	struct Particle
	{
		QiVec2 mPos;
		QiVec2 mOldPos;
		QiVec2 mPinnedPos;
		float mPin;
	};

	struct Constraint
	{
		Constraint() {}
		Constraint(QiUInt16 p0, QiUInt16 p1, float restLength2, float strength) : 
		mParticle0(p0), mParticle1(p1), mRestLength2(restLength2), mStrength(strength) {}

		QiUInt16 mParticle0;
		QiUInt16 mParticle1;
		float mRestLength2;
		float mStrength;
	};

	enum Type
	{
		CAPE,
		HAIR,
		COUNT
	};

	Cloth(class Dude* dude, const QiString& type, const QiVec2& texMin, const QiVec2& texMax);
	void update();
	void render(QiVertexBuffer& vb, QiIndexBuffer& ib);
	void addConstraint(QiUInt16 p0, QiUInt16 p1, float strength);

	void reshape();

	class Dude* mDude;
	int mPart;
	QiTransform2 mTransform;
	QiVec2 mSize;
	int mWidth;
	int mHeight;
	QiVec2 mTexMin;
	QiVec2 mTexMax;
	QiArray<Particle> mParticles;
	QiArray<Constraint> mConstraints;
	int mIterationCount;
	QiArray<QiVec2> mTexCoords;
	QiString mType;
};


