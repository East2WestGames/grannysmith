/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "note.h"
#include "level.h"
#include "game.h"
#include "gfx.h"

Note::Note(Level* level) : Entity(level, NOTE)
{
	mProperties.add("A1", "");
	mProperties.add("A2", "");
	mProperties.add("A3", "");
	mProperties.add("A4", "");
	mProperties.add("A5", "");
	mProperties.add("A6", "");
	mProperties.add("A7", "");
	mProperties.add("A8", "");
	mTexture = mLevel->mResMan->acquireTexture("gfx/note.png");
}

void Note::renderTransparent()
{
	if (mTexture.getTexture() == NULL)
		return;

	QiRenderState state;
	state.shader = gGame->mGfx->m2DTexShader.getShader();
	state.blendMode = state.BLEND;
	state.texture[0] = mTexture.getTexture();
	state.depthTest = true;
	state.depthMask = false;
	gGame->mRenderer->setState(state);

	float w = 5.0f;
	float h = 5.0f;
	QiTransform3 t;
	t.pos.set(mTransform.pos.x, mTransform.pos.y, mZ);
	t.rot = QiQuat(QiVec3::Z, mTransform.rot);
	QiMatrix4 mat(t);
	QiMatrix4 scale;
	scale.m[0] = w;
	scale.m[5] = -h;
	QiMatrix4 trans(QiVec3(-w*0.5f, h*0.5f, 0.0f));
	gGame->mGfx->drawRectangle(mat*trans*scale);
}
