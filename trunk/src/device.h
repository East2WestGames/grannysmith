/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "base/qistring.h"
#include "base/qisystem.h"

class Device
{
public:
	virtual QiString getAssetPath() { return "data"; }
	virtual QiString getUserPath() { return QiSystem::getHomeDir(); }

	virtual QiString getLanguage() { return QiString("en"); }
	virtual QiString getOsName() { return "unknown"; }
	virtual QiString getModelName() { return "uknown"; }
	virtual QiString getPlatformName() { return PLATFORM_STRING; }

	virtual int getCpuCount() { return QiSystem::getCpuCount(); }
	virtual bool isPhone() { return false; }	
	virtual bool showAds() { return true; }
	virtual bool isMusicPlaying() { return false; }

	virtual void visitUrl(const QiString& url) { QiSystem::openUrl(url); }
	virtual bool setAlwaysOn(bool enable) { return false; } 
	virtual bool hasTegra() { return false; } 

	virtual void quit() { }
};

