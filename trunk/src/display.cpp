/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "config.h"
#include "display.h"
#include "game.h"
#include "gfx.h"
#include "debug.h"
#include "profiling.h"

#include "level.h"
#include "dude.h"

float gExtraReplayTimeScale = 1.0f;

extern bool gCapture;
extern bool gCaptureMode;
extern int gCaptureSubFrames;

const float MAX_ASPECT_RATIO = 1.75f;						//Letterbox if wider than this
const float MAX_CLIP_RATIO = 1.0f + (80.0f / 768.0f);	//Clip at most 64 pixels from original resolution above and below

Display::Display()
{
	mEditZoom = 1.0;

	mPanelMode = false;
	mPanelFade = 0.0f;

	mProperties.add("width", "1024");
	mProperties.add("height", "768");
	mProperties.add("visibleLeft", "0");
	mProperties.add("visibleRight", "1024");
	mProperties.add("visibleTop", "0");
	mProperties.add("visibleBottom", "768");

	mEdit3D = 0;
	mCameraSensor = 0;

	reset();

	mShakeTimer = 0.0f;
}


void Display::reset()
{
	mCameraDistance = 0.0f;
	mCameraDistanceSpeed = 0.0f;
	mCameraDistance2 = 0.0f;
	mCameraDistanceSpeed2 = 0.0f;
	mReplayCameraMode = 0;
	mReplayCameraTimer = 0.0f;
	mReplayCameraFrame = 0;

	if (gGame->mLevel && gGame->mLevel->mReplay)
	{
		gGame->mLevel->mPlayerMotion.seekRead(0);
		mReplay.load(gGame->mLevel->mPlayerMotion);
		gGame->mLevel->mPlayerMotion.seekRead(0);
	}

	mRotY.set(-0.3f);
	mCameraSensorOld = 0;
	mCameraSensorRotOld = -2;
	mCameraSensorTime = 0.0f;

	mCamera2Sensor = mCamera2SensorOld = NULL;
	mCamera2Rot = 0.0f;
	mCamera2SensorUsage.set(0.0f);
	mCamera2SensorTime = 0.0f;
	mCamera2RotTrans.set(0.0f);
	mCamera2OffsetTrans.set(QiVec3(0,0,0));
}


void Display::setGeometry(int w, int h, bool letterBox)
{
	mWidth = w;
	mHeight = h;

	mViewport.init(0, 0, w, h);
	mViewport.setModePixel();

	if (mPanelFade > 0.0f)
	{
		float s = mPanelFade*200.0f;
		mViewport.translate(QiVec3(s, 0.0f, 0.0f));
		float sh = s*0.75f;
		mViewport.scale(QiVec3(1.0f-s/(float)w, 1.0f-sh/(float)h,  1.0f));
	}

	float desiredAspectRatio = mWidth/(float)mHeight;
	float scaleY = mHeight/767.0f;

	float extraScaleY = desiredAspectRatio / 1.3333f;

	if (extraScaleY > MAX_CLIP_RATIO)
		extraScaleY = MAX_CLIP_RATIO;

	float padY = -0.5f * mHeight * (extraScaleY-1.0f);
	scaleY *= extraScaleY;

	float targetAspectRatio = desiredAspectRatio;
	if (desiredAspectRatio > MAX_ASPECT_RATIO && letterBox)
		targetAspectRatio = MAX_ASPECT_RATIO;

	float scaleX = mWidth/1023.0f;
	scaleX *= targetAspectRatio/desiredAspectRatio;

	float padX = 0.0f;
	if (targetAspectRatio < desiredAspectRatio)
		padX = 0.5f * mWidth * (1.0f - targetAspectRatio/desiredAspectRatio);

	mViewport.translate(QiVec3(padX, padY, 0));
	mViewport.scale(QiVec3(scaleX, scaleY, 1.0f));

	int clipX = (int)QiMax(0.0f, padX);
	int clipY = (int)QiMax(0.0f, padY);
	if (clipX > 0 || clipY > 0)
		mViewport.enableScissor(clipX, clipY, mWidth-clipX, mHeight-clipY);
	else
		mViewport.disableScissor();

	//int offsetIniPadCoords = int(0.5f * 768.0f * (QiMax(1.0f, extraScaleY)-1.0f));
	int offsetIniPadCoords = int(-padY * 768.0f / mHeight / extraScaleY);
	mProperties.setInt("visibleTop", offsetIniPadCoords);
	mProperties.setInt("visibleBottom", 767-offsetIniPadCoords);

	if (mFbo.getWidth() == 0)
		mFbo.init(w, h, GL_RGBA, 1);
}


QiVec2 Display::pixelToWorld(const QiVec2& pixel)
{
	QiVec3 w = mViewport.unproject(QiVec3(pixel.x, pixel.y, 0.0f));
	return QiVec2(w.x, w.y);
}


QiVec2 Display::worldToPixel(const QiVec2& worldCoord)
{
	QiVec3 p = mViewport.project(QiVec3(worldCoord.x, worldCoord.y, 0.0f));
	return QiVec2(p.x, p.y);
}


void Display::update()
{
	gExtraReplayTimeScale = (gCapture ? 2.0f/(float)gCaptureSubFrames : 1.0f);	

	if (gGame->getState() == Game::EDIT)
	{
		mEditZoom = gGame->mLevel->mProperties.getFloat("editZoom");
		mEditPan = gGame->mLevel->mProperties.getVec2("editPan");
	}
	if (mPanelMode)
		mPanelFade = QiMin(1.0f, mPanelFade + 0.1f);
	else
		mPanelFade = QiMax(0.0f, mPanelFade - 0.1f);

	Dude* dude = gGame->mLevel->mDude;

	//If we're recording bad guy motion, follow bad guy
	if (gGame->mLevel->mBadGuy && gGame->mLevel->mBadGuy->mOutput)
		dude = gGame->mLevel->mBadGuy;

	if (dude)
	{
		QiVec2 pos = dude->mTransform.pos;
		
		if (gGame->mLevel->mDieTimer > 0.0f)
			pos = gGame->mLevel->mDiePos;

		float height = 100.0f;
		QiVec2 p;
		if (gGame->mLevel->raycast(pos, pos + QiVec2(20.0f, -100.0f), 254, 1, &p, NULL, NULL))
			height = length(p-pos);
		
		float dist = height;

		if (PROFILE_PARAM("camera/override dist", 0.0f) > 0.0f)
			dist = PROFILE_PARAM("camera/dist", 0.0f);
		dist = QiClamp(dist, 0.0f, 20.0f);

		if (gGame->mLevel->mDieTimer == 0.0f)
		{
			if (mCameraSensor == 1)
				dist = 0.0f;
			else if (mCameraSensor == 2)
				dist = 5.0f;
			else if (mCameraSensor == 3)
				dist = 10.0f;
			else if (mCameraSensor == 4)
				dist = 15.0f;
			else if (mCameraSensor == 5)
				dist = 20.0f;
		}

		mCameraDistanceSpeed = QiClamp((dist-mCameraDistance) * 0.03f, -0.2f, 0.25f);
		mCameraDistance += mCameraDistanceSpeed;

		float v = QiClamp(dude->getVelocity().x, 0.0f, 15.0f);
		mCameraDistanceSpeed2 = QiClamp((v-mCameraDistance2) * 0.05f, -0.1f, 0.15f);
		mCameraDistance2 += mCameraDistanceSpeed2;

		float d = mCameraDistance;
		float s = mCameraDistance2;
		float xOffset = PROFILE_PARAM("camera/offset_x", 0.0f) + d*PROFILE_PARAM("camera/offset_x_scale", 0.01f) + s* 0.1f;
		float yOffset = PROFILE_PARAM("camera/offset_y", 3.0f) + d*PROFILE_PARAM("camera/offset_y_scale", -0.03f);
		float zOffset = PROFILE_PARAM("camera/offset_z", 6.0f) + d*PROFILE_PARAM("camera/offset_z_scale", 1.1f) + s* 0.3f;

		if (gGame->mLevel->mDieTimer == 0.0f)
		{
			if (mCameraSensorRot != mCameraSensorRotOld)
			{
				float time = mCameraSensorTime;
				if (mCameraSensorRot == 2)
					mRotY.set(-0.3f, Transition<float>::COSINE, time);
				else if (mCameraSensorRot == 1)
					mRotY.set(-0.15f, Transition<float>::COSINE, time);
				else if (mCameraSensorRot == 0)
					mRotY.set(0.0f, Transition<float>::COSINE, time);
				else if (mCameraSensorRot == -1)
					mRotY.set(0.15f, Transition<float>::COSINE, time);
				else if (mCameraSensorRot == -2)
					mRotY.set(0.3f, Transition<float>::COSINE, time);
				mCameraSensorRotOld = mCameraSensorRot;
			}
		}

		float t = mCamera2SensorUsage.get();
		QiVec3 off = mCamera2OffsetTrans.get();
		xOffset = QiInterpolateLinear(xOffset, off.x, t);
		yOffset = QiInterpolateLinear(yOffset, off.y, t);
		zOffset = QiInterpolateLinear(zOffset, off.z, t);
		float rot = QiInterpolateLinear(mRotY.get(), mCamera2RotTrans.get(), t);

		if (mCamera2Sensor != mCamera2SensorOld)
		{
			if (!mCamera2Sensor)
				mCamera2SensorUsage.set(0.0f, Transition<float>::COSINE, mCamera2SensorTime);
			else
			{
				mCamera2SensorUsage.set(1.0f);
				mCamera2RotTrans.set(rot);
				mCamera2OffsetTrans.set(QiVec3(xOffset, yOffset, zOffset));
				mCamera2RotTrans.set(mCamera2Rot, Transition<float>::COSINE, mCamera2SensorTime);
				mCamera2OffsetTrans.set(mCamera2Offset, Transition<QiVec3>::COSINE, mCamera2SensorTime);
			}
			mCamera2SensorOld = mCamera2Sensor;
		}

		mCameraPos.set(pos.x + xOffset, pos.y + yOffset, zOffset);
		mCameraRot.setAxisAngle(normalize(QiVec3(1.0f, 0.0f, 0.0f)), -0.25f);
		mCameraRot *= QiQuat(QiVec3(0.0f, 1.0f, 0.0f), rot);

		mRotY.update(0.01667f * gExtraReplayTimeScale);
		mCameraFov.update(0.01667f * gExtraReplayTimeScale);
		mCamera2OffsetTrans.update(0.01667f * gExtraReplayTimeScale);
		mCamera2RotTrans.update(0.01667f * gExtraReplayTimeScale);

		mCamera2SensorUsage.update(gGame->mTimeStep);

		if (gGame->mLevel->mReplay)
		{
			int f = (int)(gGame->mLevel->mDude->mFrame);
			if (f < mReplay.mFrames.getCount())
			{
				if (mReplay.mFrames[f].camera != mReplayCameraMode)
				{
					mReplayCameraFrame = 0;
					mReplayCameraTimer = 0.0f;
					mReplayCameraMode = mReplay.mFrames[f].camera;
					mReplayCameraFixedPos = mReplay.mFrames[f].cameraPos;
				}
				gGame->mLevel->setTimeScale(mReplay.mFrames[f].timeScale * gExtraReplayTimeScale);
			}
			else
			{
				gGame->mLevel->setTimeScale(1.0f * gExtraReplayTimeScale);
			}

			mCameraRot.setAxisAngle(normalize(QiVec3(1.0f, 0.0f, 0.0f)), -0.25f);
			switch(mReplayCameraMode)
			{
				case 0:
				{
					break;
				}
				case 1:
				case 2:
				{
					//Close-up camera
					float s = (mReplayCameraMode == 2 ? -1.0f : 1.0f);
					mCameraPos.set(pos.x + s*2.5f + 2.0f, pos.y+3.0f, 11.0f);
					mCameraRot *= QiQuat(QiVec3(0.0f, 1.0f, 0.0f), s*0.2f);
					if (mReplayCameraFrame == 0)
						mCameraFov.set(30);
					else if (mReplayCameraFrame == 60)
						mCameraFov.set(60, Transition<float>::COSINE, 1.0f);
					break;
				}
				case 3:
				case 4:
				{
					//Regular camera
					float s = (mReplayCameraMode == 4 ? -1.0f : 1.0f);
					mCameraPos.set(pos.x + s*2.5f + 2.0f, pos.y+3.0f, 11.0f);
					mCameraRot *= QiQuat(QiVec3(0.0f, 1.0f, 0.0f), s*0.2f);
					mCameraFov.set(60);
					break;
				}

				case 5:
				{
					//Rotating sweep camera
					mCameraPos.set(mReplayCameraFixedPos.x, pos.y, mReplayCameraFixedPos.z);
					QiVec3 a = normalize(QiVec3(pos.x, mCameraPos.y, 0)-mCameraPos);
					QiVec3 b = QiVec3(0, 0, -1.0f);
					float ang = QiACos(dot(a, b));
					mCameraRot.setAxisAngle(normalize(cross(a, b)), -ang*0.95f);
					if (mReplayCameraFrame == 0)
						mCameraFov.set(60.0f);
					else if (mReplayCameraFrame == 60 && QiRnd(0, 2) == 0)
						mCameraFov.set(20, Transition<float>::COSINE, 0.3f);
					break;
				}

				case 6:
				{
					//Breakage camera
					mCameraPos.set(pos.x + 4.0f, pos.y+3.0f, 11.0f);
					mCameraRot *= QiQuat(QiVec3(0.0f, 1.0f, 0.0f), 0.3f);
					mCameraFov.set(40);
					break;
				}
				case 7:
				{
					//Breakage camera
					mCameraPos = mReplayCameraFixedPos;
					mCameraPos.y = pos.y + 3.0f;
					mCameraRot *= QiQuat(QiVec3(0.0f, 1.0f, 0.0f), 0.3f);
					if (mReplayCameraFrame == 0)
						mCameraFov.set(30.0f);
					if (mReplayCameraFrame == 1)
						mCameraFov.set(50, Transition<float>::LINEAR, 5.0f);
					break;
				}
				case 8:
				{
					//Close-up camera
					mCameraPos.set(pos.x + 1.5f, pos.y+1.5f, 6.0f);
					mCameraRot *= QiQuat(QiVec3(0.0f, 1.0f, 0.0f), 0.1f);
					mCameraFov.set(50.0f);
					break;
				}
			}
			mReplayCameraFrame++;
		}
		else
		{
			mCameraFov.set(60);
			//gGame->mLevel->setTimeScale(1.0f * gExtraReplayTimeScale);
		}
	}

	//Camera shake
	if (mShakeTimer > 0.0f)
	{
		float f = gGame->mTimeStep / 0.01667f;
		QiVec2 t(QiSin(gGame->mLevel->mSimTime * 60), QiSin(gGame->mLevel->mSimTime * 53));
		t += QiVec2(QiRnd(-0.2f*f, 0.5f*f), QiRnd(-0.5f*f, 0.5f*f));
		float scale = 0.11f * mShakeTimer;
		t = t * scale;
		mCameraPos += QiVec3(t.x, t.y, 0.0f);
		mCameraRot = QiQuat(QiVec3::Y, -0.01f*t.x) * mCameraRot;
		mShakeTimer -= gGame->mTimeStep;
	}
}


void Display::postDraw()
{
	if (mViewport.isScissorEnabled())
	{
		//Temporarily disable scissor
		int x0, y0, x1, y1;
		mViewport.getScissor(x0, y0, x1, y1);
		mViewport.disableScissor();
		gGame->mRenderer->setViewport(mViewport);

		//Draw black edges to cover schmutz
		QiRenderState state;
		state.color = QiColor(0,0,0,1);
		state.shader = gGame->mGfx->m2DShader.getShader();
		gGame->mRenderer->setState(state);
		QiVec2 outLow = gGame->mDisplay->pixelToWorld(QiVec2(0,0));
		QiVec2 outHigh = gGame->mDisplay->pixelToWorld(QiVec2((float)gGame->mDisplay->mWidth, (float)gGame->mDisplay->mHeight));
		QiVec2 inLow = QiVec2(0,0);
		QiVec2 inHigh = QiVec2(1024, 768);
		gGame->mRenderer->drawRect(outLow, QiVec2(inLow.x, outHigh.y));
		gGame->mRenderer->drawRect(QiVec2(inHigh.x, outLow.y), outHigh);
		gGame->mRenderer->drawRect(outLow, QiVec2(outHigh.x, inLow.y));
		gGame->mRenderer->drawRect(QiVec2(outLow.x, inHigh.y), outHigh);

		//Switch scissor back on
		mViewport.enableScissor(x0, y0, x1, y1);
	}

	if (mPanelFade > 0.0f)
	{
		mViewport.setModePixel();

		float x = -200.0f + mPanelFade*200.0f;
		float y = 0.0f;
		float w = 200.0f;
		float h = (float)mHeight;
		mViewport.push();
		mViewport.enableScissor(0, 0, (int)w, mHeight);
		mViewport.translate(QiVec3(x, y, 0));
		gGame->mRenderer->setViewport(mViewport);
		gGame->mDebug->drawLeftPanel(w, h);
		mViewport.disableScissor();
		mViewport.pop();

		x = 200.0f * mPanelFade;
		y = (float)mHeight - mPanelFade*150.0f;
		w = (float)mWidth-200.0f;
		h = 150.0f;
		mViewport.push();
		mViewport.enableScissor(0, (int)y, mWidth, mHeight);
		mViewport.translate(QiVec3(x, y, 0));
		gGame->mRenderer->setViewport(mViewport);
		gGame->mDebug->drawBottomPanel(w, h);
		mViewport.pop();
	}
}


void Display::enterLevel()
{
	QiViewport& vp = gGame->mDisplay->mViewport;
	vp.push();	
	if (gGame->getState() == Game::EDIT)
	{
		vp.setMode2D(10.0f/mEditZoom, -1000.0f, 1000.0f);
		float x = mEditPan.x;
		float y = mEditPan.y;
		vp.setCameraPos(QiVec3(x, y, 10.0f));
		if (mEdit3D == 1)
		{
			vp.setIsoAmount(0.1f*mEditZoom);
			vp.setIsoAngle(3*QI_PI/4);
		}
		else if (mEdit3D == 2)
		{
			vp.setIsoAmount(0.1f*mEditZoom);
			vp.setIsoAngle(QI_PI/4);
		}
		else if (mEdit3D == 3)
		{
			vp.setCameraPos(QiVec3(x, 0, 0));
			vp.rotate(QiQuat(QiVec3::X, QI_HALF_PI));
		}
	}
	else
	{
		float s = mPanelFade*200.0f;
		float sh = s*0.75f;
		vp.setBounds((int)s, (int)sh, mWidth, mHeight);
		vp.setMode3D(mCameraFov.get(), 1.0f, 1000.0f);
		vp.setCameraPos(mCameraPos);
		vp.setCameraRot(mCameraRot);
		mFrustumDir0 = vp.getPixelDirection(QiVec2(0, 0));
		mFrustumDir1 = vp.getPixelDirection(QiVec2(0, (float)vp.getHeight()));
		mFrustumDir2 = vp.getPixelDirection(QiVec2((float)vp.getWidth(), (float)vp.getHeight()));
		mFrustumDir3 = vp.getPixelDirection(QiVec2((float)vp.getWidth(), 0));
		mFrustumDir0 /= -mFrustumDir0.z;
		mFrustumDir1 /= -mFrustumDir1.z;
		mFrustumDir2 /= -mFrustumDir2.z;
		mFrustumDir3 /= -mFrustumDir3.z;
	}
	gGame->mRenderer->setViewport(vp);
}


void Display::leaveLevel()
{
	QiViewport& vp = gGame->mDisplay->mViewport;
	vp.pop();
	gGame->mRenderer->setViewport(vp);
}


bool Display::isVisible(const QiVec2& boundsMin, const QiVec2& boundsMax, float z)
{
	if (gGame->getState() == Game::EDIT)
		return true;

	float d = mViewport.getCameraPos().z - z;
	QiVec3 p0 = mViewport.getCameraPos() + mFrustumDir0*d;
	QiVec3 p1 = mViewport.getCameraPos() + mFrustumDir1*d;
	QiVec3 p2 = mViewport.getCameraPos() + mFrustumDir2*d;
	QiVec3 p3 = mViewport.getCameraPos() + mFrustumDir3*d;
	QiVec2 frustMin(QiMin(p0.x, p1.x), QiMin(p1.y, p2.y));
	QiVec2 frustMax(QiMax(p2.x, p3.x), QiMax(p0.y, p3.y));
	return !(boundsMin.x > frustMax.x || boundsMax.x < frustMin.x || boundsMin.y > frustMax.y || boundsMax.y < frustMin.y);
}

/*
void Display::updateFbo(bool menuMode)
{
	if (menuMode)
		mFbo.init((mWidth*3)/4, (mWidth*9)/16, GL_RGBA, 1);
	else
		mFbo.init(mWidth, (mWidth * 10) / 16, GL_RGBA, 1);
	QI_PRINT("Fbo: " + mFbo.getWidth() + ", " + mFbo.getHeight());
}
*/

