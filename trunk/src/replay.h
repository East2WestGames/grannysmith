/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"

#include "qi_base.h"
#include "qi_math.h"

class Replay
{
public:
	void load(QiInputStream& motion);

	struct Frame
	{
		Frame() : ang(0.0f), angVel(0.0f), hints(0), camera(-1), timeScale(1.0f) {}
		QiVec2 pos;
		QiVec2 vel;
		float ang;
		float angVel;
		QiUInt16 hints;
		int camera;
		QiVec3 cameraPos;
		float timeScale;
	};

	QiArray<Frame> mFrames;
};

