/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "config.h"
#include "game.h"
#include "level.h"
#include "gfx.h"
#include "debug.h"
#include "display.h"
#include "app/qiinput.h"
#include "body.h"
#include "Box2D.h"

#include "scene.h"
#include "script.h"

Debug::Debug()
{
	mFilter = LOG_INFO | LOG_WARNING | LOG_ERROR;
	memset(mLog, 0, LOG_MAX_LINES*LOG_MAX_LENGTH);
	mLogCurrent = 0;
}


void Debug::log(const char* message, int flags)
{
    QI_PRINT(message);
	strncpy(mLog[mLogCurrent], message, LOG_MAX_LENGTH-1);
	mLogFlags[mLogCurrent] = flags;
	mLogCurrent = (mLogCurrent+1) % LOG_MAX_LINES;
}


static void drawRect(float x, float y, float w, float h, const QiColor& color)
{
	QiMatrix4 mat(QiVec3(x, y, 0.0f));
	mat.m[0] = w;
	mat.m[5] = h;

	QiRenderState state;
	state.blendMode = color.a < 1.0f ? state.BLEND : state.NONE;
	state.color = color;
	state.shader = gGame->mGfx->m2DShader.getShader();
	gGame->mRenderer->setState(state);
	gGame->mGfx->drawRectangle(mat);
}


bool Debug::toggleButton(const QiString& caption, int x, int y, bool enabled)
{
	int w = mTextRenderer.getWidth(caption);
	int h = mTextRenderer.getHeight(caption);
	drawRect((float)x, (float)y, w+8.0f, h+4.0f, QiColor(0.5f, 0.5f, 0.5f, enabled ? 0.3f : 0.1f));
	if (gGame->mInput->wasButtonPressed(QI_BUTTON_LEFT) &&  gGame->mDisplay->mViewport.pickRect(caption.getHash(), QiVec2((float)x, (float)y), QiVec2((float)x+w+8, (float)y+h), 2))
		enabled = !enabled;
	mTextRenderer.setColor(0,0,0,1);
	mTextRenderer.setPosition(x+2, y+2);
	mTextRenderer.print(caption);
	return enabled;
}

void Debug::listScene(const QiString& name, class Scene* scene, int& yPos)
{
	mTextRenderer.setColor(0.0f, 0.0f, 0.5f, 1.0f);
	mTextRenderer.setPosition(10, yPos);
	mTextRenderer.print(name);
	yPos += 12;

	if (scene->mScript.mHasScript)
	{
		mTextRenderer.setColor(0.6f, 0.4f, 0.2f, 1.0f);
		mTextRenderer.setPosition(10, yPos);
		mTextRenderer.print(QiString("Init time: ") + int(scene->mScript.mInitTime * 1000.0f) + QiString(" ms"));
		yPos += 12;

		mTextRenderer.setColor(0.6f, 0.4f, 0.2f, 1.0f);
		mTextRenderer.setPosition(10, yPos);
		mTextRenderer.print(QiString("Tick time: ") + int(scene->mScript.mTickTime * 100000.0f)/100.0f + QiString(" ms"));
		yPos += 12;

		mTextRenderer.setPosition(10, yPos);
		mTextRenderer.print(QiString("Draw time: ") + int(scene->mScript.mDrawTime * 100000.0f)/100.0f + QiString(" ms"));
		yPos += 12;

		mTextRenderer.setPosition(10, yPos);
		mTextRenderer.print(QiString("Draw calls: ") + scene->mDrawCallCount);
		yPos += 12;

		/*
		mTextRenderer.setColor(0.5f, 0.5f, 0.5f, 1.0f);
		QiArray<Script::Object*>& objs = scene->mScript.mObjects;
		for(int i=0; i<objs.getCount(); i++)
		{
			mTextRenderer.setPosition(10, yPos);
			mTextRenderer.print(objs[i]->mDesc);
			yPos += 12;
		}
		*/
	}
	else
	{
		mTextRenderer.setColor(0.6f, 0.0f, 0.0f, 1.0f);
		mTextRenderer.setPosition(10, yPos);
		mTextRenderer.print("Not loaded");
		yPos += 12;
	}
	yPos += 12;
}


void Debug::drawLeftPanel(float w, float h)
{
	int dcc = gGame->mRenderer->getDrawCallCount();
	if (!mTextRenderer.isInitialized())
	{
		mTextRenderer.init(gGame->mRenderer, gGame->mGfx->mFontShader.getShader());
		mTextRenderer.setFont(QiTextRenderer::TAHOMA_10);
		mTextRenderer.flip(true);
		mTextRenderer.setColor(0, 0, 0, 1);
	}

	drawRect(0, 0, w, h, QiColor(1.0f, 0.95f, 0.9f, 1.0f));

	int yPos = 10;

	listScene("Menu scene", gGame->mMenuScene, yPos);
	listScene("Movie scene", gGame->mMovieScene, yPos);
	listScene("Hud scene", gGame->mHudScene, yPos);

	yPos += 20;
	mTextRenderer.setPosition(10, yPos);
	mTextRenderer.print(QiString("Level draw calls: ") + gGame->mLevel->mDrawCallCount);
	yPos += 12;
	mTextRenderer.setPosition(10, yPos);
	mTextRenderer.print(QiString("Total draw calls: ") + dcc);

	yPos += 20;
	mTextRenderer.setPosition(10, yPos);
	mTextRenderer.print(QiString("Allocations: ") + QiGetAllocationCount());

	yPos += 12;
	mTextRenderer.setPosition(10, yPos);
	mTextRenderer.print(QiString("Allocated: ") + (QiGetAllocatedBytes()/1024) + " kb");

	yPos += 12;
	mTextRenderer.setPosition(10, yPos);
	mTextRenderer.print(QiString("Texture memory: ") + (QiTexture::getAllocatedBytes()/1024) + " kb");

	int c = 0, t=0;
	for(int i=0; i<gGame->mLevel->mBodies.getCount(); i++)
	{
		if (gGame->mLevel->mBodies[i]->mDynamic != 0)
		{
			t++;
			if (gGame->mLevel->mBodies[i]->mPhysBody->IsAwake())
				c++;
		}
	}
	yPos += 12;
	mTextRenderer.setPosition(10, yPos);
	mTextRenderer.print(QiString("Awake ") + c + QiString("/") + t);
}


void Debug::drawBottomPanel(float w, float h)
{
	drawRect(0, 0, w, h, QiColor(0.95f, 0.9f, 0.85f, 1.0f));

	mTextRenderer.setColor(0.2f, 0.2f, 0.2f, 1);
	mTextRenderer.setPosition(10, 10);
	int yPos = 140;
	for(int i=0; i<LOG_MAX_LINES; i++)
	{
		int line = (mLogCurrent+255*(i+1)) % 256;
		if ((mLogFlags[line] & mFilter) && mLog[line][0])
		{
			yPos -= mTextRenderer.getHeight(mLog[line]);
			QiColor c(0,1,0,1);
			if (mLogFlags[line] & LOG_ERROR)
				drawRect(2, (float)yPos+2, 6, 6, QiColor(1, 0, 0, 1));
			if (mLogFlags[line] & LOG_WARNING)
				drawRect(2, (float)yPos+2, 6, 6, QiColor(1, 1, 0, 1));
			mTextRenderer.setPosition(10, yPos);
			mTextRenderer.print(mLog[line]);
			if (yPos < 0)
				break;
		}
	}

	int newFilter = 0;
	newFilter |= toggleButton("Info", int(w)-60, 10, (mFilter & LOG_INFO) != 0) ? LOG_INFO : 0;
	newFilter |= toggleButton("Warning", int(w)-60, 30, (mFilter & LOG_WARNING) != 0) ? LOG_WARNING : 0;
	newFilter |= toggleButton("Error", int(w)-60, 50, (mFilter & LOG_ERROR) != 0) ? LOG_ERROR : 0;
	mFilter = newFilter;
}

