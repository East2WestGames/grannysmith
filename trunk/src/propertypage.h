/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "qi_base.h"
#include "qi_gfx.h"
#include "qi_app.h"
#include "propertybag.h"

class PropertyPage
{
public:
	class Field
	{
	public:
		QiString mName;
		QiString mValue;
		QiString mDefault;
	};

	PropertyPage() { init(NULL); }
	PropertyPage(QiViewport* viewport) { init(viewport); }
	~PropertyPage() { shutdown(); }

	bool init(class QiViewport* viewport);
	void shutdown();

	void clear();
	//void addField(const QiString& name, const QiString& value);
	void setPropertyBag(PropertyBag* properties);
	void show();

	bool isShown() const { return mShow; }
	void beginFrame(class QiInput& input);
	void endFrame();
	bool hasFinished() const { return mFinished; }
	QiString getValue(const QiString& name) const;

protected:
	QiArray<Field*> mFields;
	PropertyBag* mProperties;
	QiViewport* mViewport;
	QiInput mInput;
	QiTextRenderer mTextRenderer;
	bool mFinished;
	bool mShow;
	int mSelected;
	QiInputBox mInputBox;
};

