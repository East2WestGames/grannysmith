/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "gfx/qitextrenderer.h"

const int LOG_MAX_LINES = 256;
const int LOG_MAX_LENGTH = 1024;

const int LOG_INFO = 1;
const int LOG_WARNING = 2;
const int LOG_ERROR = 4;

class Debug
{
public:
	Debug();

	void log(const char* message, int flags=LOG_INFO);

	bool toggleButton(const QiString& caption, int x, int y, bool enabled);
	void listScene(const QiString& name, class Scene* scene, int& yPos);
	void drawLeftPanel(float w, float h);
	void drawBottomPanel(float w, float h);

	QiTextRenderer mTextRenderer;
	char mLog[LOG_MAX_LINES][LOG_MAX_LENGTH];
	int mLogFlags[LOG_MAX_LINES];
	int mLogCurrent;

	int mFilter;
};

