/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "qi_base.h"
#include "qi_gfx.h"
#include "resman.h"

const int FONT_ROWS = 8;
const int FONT_COLS = 8;
const int FONT_CHARS = FONT_COLS * FONT_ROWS;
const int FONT_FIRST = (int)' ';
const int FONT_MAX_CHARS = 1024;

class Font
{
public:
	Font(const char* name, bool numbers);

	void setText(const QiString& str);
	float getWidth() const { return mWidth; }
	float getHeight() const { return mHeight; }

	QiVertexFormat mFmt;
	QiVertexBuffer mVb;
	QiIndexBuffer mIb;

	Resource mTexture;
	float mSpacing[FONT_CHARS];
	QiString mText;
	float mWidth;
	float mHeight;
	int mChars;
	bool mNumbers;
};

