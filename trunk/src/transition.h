/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "qi_math.H"

template <class T>
class Transition
{
public:
	enum Type
	{
		IMMEDIATE,
		LINEAR,
		EASE_IN,
		EASE_OUT,
		COSINE,
		BOUNCE
	};

	inline Transition() : mType(IMMEDIATE), mLength(0.0f), mCurrent(0.0f)
	{
	}

	inline Transition(const T& value) : mType(IMMEDIATE), mLength(0.0f), mCurrent(0.0f)
	{
		mOld = value;
		mNew = value;
	}

	inline void set(T newValue)
	{ 
		set(newValue, IMMEDIATE, 0.0f);
	}

	inline void set(T newValue, Type type, float length)
	{ 
		mOld = get();
		mNew = newValue;
		mType = type; 
		mLength = length; 
		mCurrent = 0.0f; 
	}

	inline void set(T newValue, const QiString& type, float length)
	{
		Type t = IMMEDIATE;
		if (type == "linear")
			t = LINEAR;
		else if (type == "easein")
			t = EASE_IN;
		else if (type == "easeout")
			t = EASE_OUT;
		else if (type == "cosine")
			t = COSINE;
		else if (type == "bounce")
			t = BOUNCE;
		set(newValue, t, length);
	}

	inline void update(float dt)
	{
		mCurrent += dt;
	}

	inline T get() const
	{
		static bool sFirst = true;
		const int BOUNCE_FRAMES = 1024;
		static float sCritDamp[BOUNCE_FRAMES];

		if (sFirst)
		{
			float val = 0.0f;
			float vel = 0.0f;
			for(int i=0; i<BOUNCE_FRAMES; i++)
			{
				vel = 0.98f*vel + (1.0f-val)*0.00035f;
				sCritDamp[i] = val;
				val += vel;
			}
			sFirst = false;
		}

		if (mLength == 0.0f)
			return mNew;

		float t = QiClamp(mCurrent / mLength, 0.0f, 1.0f);
		switch(mType)
		{
		case LINEAR:
			break;
		case EASE_IN:
			t = 1.0f-QiCos(t*QI_PI*0.5f);
			//t = QiPow(t, 2.0f);
			break;
		case EASE_OUT:
			t = QiSin(t*QI_PI*0.5f);
			//t = QiPow(t, 0.5f);
			break;
		case COSINE:
			t = QiInterpolateCosine(0.0f, 1.0f, t);
			break;
		case BOUNCE:
			t = sCritDamp[int(t*(BOUNCE_FRAMES-1))];
			break;
		default:
			t = 1.0f;
			break;
		}
		return mOld + (mNew-mOld)*t;
	}

protected:
	Type mType;
	float mLength;
	float mCurrent;
	T mOld;
	T mNew;
};
