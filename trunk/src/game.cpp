/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "config.h"
#include "game.h"
#include "scene.h"
#include "gfx.h"
#include "debug.h"
#include "resman.h"
#include "display.h"
#include "audio.h"
#include "level.h"
#include "editor.h"
#include "profiling.h"
#include "player.h"
#include "device.h"
#include "dude.h"
#include "httpthread.h"
#include "store.h"
#pragma comment(lib,"OpenAL32.lib")

#ifdef QI_IOS
#include "gamecenter.h"
#endif

#include "base/qisystem.h"
#include "qi_app.h"
#include "qi_gfx.h"
#ifdef QI_ANDROID
#include <android/log.h>
#endif
Game* gGame = NULL;
QiString gAssetServer = "";

#if defined(QI_MACOS) || defined(QI_WIN32)
extern bool gCapture;
extern bool gCaptureMode;
extern int gCaptureSubFrames;
#else
bool gCapture = false;
bool gCaptureMode =  false;
int gCaptureSubFrames = 64;
#endif

Resource gLoadingScreen,gLoadingScreen_MM;
Resource gNvSplashScreen;

#ifdef QI_ANDROID
#define IAP_UNLOCKALL "com.mediocre.grannysmith.unlockall3"
#else
#define IAP_UNLOCKALL "com.mediocre.grannysmith.unlockall"
#endif

#define AD_SHOP "com.mediocre.grannysmith.shop"
#define AD_DIE "com.mediocre.grannysmith.die"
#define AD_PAUSE "com.mediocre.grannysmith.pause"
#define AD_RESTART "com.mediocre.grannysmith.restart"
#define AD_REPLAY "com.mediocre.grannysmith.replay"
#define AD_REMOVE "com.mediocre.grannysmith.remove"
#define AD_FREECOINS "com.mediocre.grannysmith.freecoins"
#define AD_FREECOINS_MAIN "com.mediocre.grannysmith.freecoins.main"
#define AD_GIFT "com.mediocre.grannysmith.gift"

void menuButtonPressed();
void backButtonPressed();
void backFromResume();

class AsyncThread : public QiThread
{
public:
	AsyncThread() : mUpdates(0) {}
	
	virtual void run()
	{
		while(!shouldQuit())
		{
			mBegin.wait();
			for(int i=0; i<mUpdates; i++)
				gGame->mLevel->updateAsync();
			mEnd.signal();
		}
	}

	int mUpdates;
	QiCondition mBegin;
	QiCondition mEnd;
};

Game::Game(Device* device) : mLevel(NULL)
{
	PROFILE_OPEN();

	QI_PRINT("Enabling small object allocator");
	QiAllocator::init();

#if ENABLE_ASSET_SERVER && 0
	for(int i=1; i<63; i++)
	{
		QI_PRINT("Trying " + i);
		if (ResMan::connectAssetServer(QiString("192.168.0.")+i, 0.1f))
		{
			QI_PRINT("Connected to " + i);
            gAssetServer = QiString("192.168.0.")+i;
			break;
		}
	}
#endif

#ifdef DATE_CHECK
	if (QiSystem::getCurrentDateTime() > QiSystem::getDateTime(DATE_CHECK, 0, 0, 0) )
		exit(0);
#endif

	gGame = this;
	mDevice = device;

	mDebug = QI_NEW Debug();

	logI("Starting up");

	mInput = QI_NEW QiInput();
	mAudio = QI_NEW Audio();

	mResMan = QI_NEW ResMan();

	mRenderer = QI_NEW QiRenderer();
	mGfx = QI_NEW Gfx(mResMan);
	mDisplay = QI_NEW Display();

	mMenuScene = QI_NEW Scene();

	mMovieScene = QI_NEW Scene();

	mHudScene = QI_NEW Scene();
	mHudScene->mResMan.mPreventUnload = true;
	
	mPlayer = QI_NEW Player();
	mPlayer->init();
	mPlayer->load();

	mLevel = QI_NEW Level();
	mEditor = QI_NEW Editor();

	mTimeStep = 0.01666667f;
	mProperties.add("timeStep", "0.01666667");
	mProperties.add("frame", "0");
	mProperties.add("totalTime", "0.0");
	mProperties.add("frameTime", "0.0");
	mProperties.add("stateFade", "0.0");
	mProperties.add("controls", "1");
	mProperties.add("levelpath", "");
	mProperties.add("paused", "0");
	mProperties.add("assetserver", "");
	mProperties.add("levelpos", "0");
	mProperties.add("platform", PLATFORM_STRING);
	mProperties.add("purchasing", "0");
	mProperties.add("storeavailable", "0");
	mProperties.add("gamecenteravailable", "0");
	mProperties.add("disablesepia", "0");

	mProperties.add("ctrljump", "0");
	mProperties.add("ctrlcane", "0");
	mProperties.add("ctrlbaseball", "0");
	mProperties.add("ctrlbanana", "0");

	mFrame = 0;
	mFrameTime = 0.0f;
	mDieAccount = 0;
	mReplayAccount = 0;

	mCurrentState = LOADING;
	mNextState = LOADING;

	mStateFade = 0.0f;

	mReplay = false;
	mReplayStream = NULL;

	mTvDrawn = true;

	mHttpThread = QI_NEW HttpThread();

	mStoreTimer = 0.0f;
	
	gLoadingScreen = mResMan->acquireTexture("gfx/loading.jpg");
	gLoadingScreen_MM = mResMan->acquireTexture("gfx/load_mm.jpg");
#ifdef QI_ANDROID
	gNvSplashScreen = mResMan->acquireTexture("gfx/nvsplash.png");
#endif
	
	mTrigHudReload = false;
	mTrigBackButton = false;
	mTrigMenuButton = false;
	mTrigBackFromResume = false;
	mInPurchase = false;
	mGetGift = true;
	
	mAsyncThread = NULL;
	
//#ifdef QI_ANDROID
	if (mDevice->getCpuCount() > 1)
	{
		mAsyncThread = QI_NEW AsyncThread();
		mAsyncThread->start();
	}	
//#endif
}

Game::~Game()
{
	if (mAsyncThread)
	{
		mAsyncThread->signalQuit();
		mAsyncThread->mUpdates = 0;
		mAsyncThread->mBegin.signal();
		mAsyncThread->wait();
	}
	
	if (mLevel->mLoaded)
		mLevel->stop();

	mHttpThread->signalQuit();
	while(mHttpThread->isRunning())
		QiThread::sleep(0.01f);
	QI_DELETE(mHttpThread);

	mAudio->stopBackgroundMusic();
	mAudio->stopForegroundMusic();
	mAudio->unload();

	QI_DELETE(mEditor);
	QI_DELETE(mLevel);

	QI_DELETE(mPlayer);

	QI_DELETE(mMovieScene);
	QI_DELETE(mMenuScene);

	QI_DELETE(mDisplay);
	QI_DELETE(mGfx);
	QI_DELETE(mRenderer);

	QI_DELETE(mResMan);
	QI_DELETE(mAudio);
	QI_DELETE(mInput);

	QI_DELETE(mDebug);
	mDebug = NULL;

	PROFILE_CLOSE();
}

void Game::init(int frame)
{
	int code;
	switch(frame)
	{
		case 0:
		logI("Init system");
		loadConfig();
		mAudio->update();
		break;
		
		case 1:
		storeInit();
		code = storeEnabled();
		mProperties.setBool("storeavailable", code > 1);
		//when it's the first time startup
		if (code == 2) {
			mPlayer->addPowerUp("helmet", 2);
			mPlayer->addPowerUp("banana", 2);
			mPlayer->addPowerUp("baseball", 2);
			mPlayer->save();
		}
		break;

		case 2:
		logI("Loading menu");
		mMenuScene->load("menu/main.lua");
		break;
		
		case 3:
		logI("Loading sounds");
		mAudio->load();	
		break;
		
		case 4:
		mHttpThread->start();
		break;
		
		case 5:
		QI_PRINT("Loading level");
		loadAttractLevel();
		break;

		case 6:
		break;

		case 9:
		//onEnterState(mCurrentState);
		break;
	}
}


void Game::loadConfig()
{
	QiMemoryStream<256> tmp;
	if (!mResMan->load("user://granny.config", tmp))
		return;
	QiXmlParser xml(tmp, tmp.getSize());
	if (xml.getName() == "config")
    {
		mProperties.readXml(xml);
        xml.enter();
        while(xml.isValid())
        {
            if (xml.getName() == "audio")
                mAudio->mProperties.readXml(xml);
            xml.next();
        }
        xml.leave();
    }
}


void Game::saveConfig()
{
	QiXmlWriter xml;
	xml.enter("config");
	mProperties.writeXml(xml);
    xml.enter("audio");
	mAudio->mProperties.writeXml(xml);
    xml.leave();
	xml.leave();
	QiMemoryStream<256> tmp;
	if (xml.write(tmp))
		mResMan->save("user://granny.config", tmp.getData(), tmp.getSize());
}


const char* ATTRACT_LEVELS[] = { "4", "7", "10", "2-2", "2-8", "2-12", "3-3", "3-6", "3-10" };

void Game::loadAttractLevel()
{
	int c = sizeof(ATTRACT_LEVELS) / sizeof(ATTRACT_LEVELS[0]);
	int i = QiMin(c-1, QiRnd(0, c));
	if (mPlayer->getScore("warming_up") == 0)
		i = 0;
	mLevelName = ATTRACT_LEVELS[i];
	mLevel->mPlayerMotion.clear();
	if (mResMan->load("attract/" + mLevelName + ".motion", mLevel->mPlayerMotion))
		mLevel->mReplay = true;
	startLevel();
}

static int gLoadingFrame = 0;

void Game::frame()
{
	if (getState() == LOADING)
	{

		if(gLoadingFrame<5)
			draw_MM();
		if (gLoadingFrame < 5)
			init(gLoadingFrame);
		if (gLoadingFrame>= 5)
			draw();
		if (gLoadingFrame > 5)
			init(gLoadingFrame);


		if(gLoadingFrame == 100)
			setState(MENU);
		gLoadingFrame++;
		return;
	}
	
	PROFILE_FRAME();
    	
	if (mInPurchase)
		mInput->clear();

    if (gAssetServer!="" && ResMan::sAssetSocket)
        mProperties.setString("assetserver", gAssetServer);
    else
        mProperties.setString("assetserver", "");

	if (mNextLevel != "" && getState() == PLAY)
	{
		stopLevel();
		mLevelName = mNextLevel;
		startLevel();
		mNextLevel = "";
	}

	if (mReplay && getState() != PLAY)
	{
		QiInt32 s=0;
		mReplayStream->readInt32(s);
		if (s>0)
		{
			char path[512];
			if (mReplayStream->readString(path, 512))
			{
				mLevel->mReplay = true;
				mLevel->mPlayerMotion.clear();
				mLevel->mPlayerMotion.writeBuffer(*mReplayStream, s-4-strlen(path));
				mLevelName= path;
				setState(PLAY);
			}
		}
	}

	if (mNextState != mCurrentState)
	{
		mStateFade = QiMin(1.0f, mStateFade+0.1f);
		if (mStateFade == 1.0f)
		{
			onLeaveState(mCurrentState);
			mCurrentState = mNextState;
			onEnterState(mNextState);
		}
	}
	else
	{
		mStateFade = QiMax(0.0f, mStateFade-0.05f);
	}

#if defined(QI_IOS) || defined(QI_ANDROID)
    if (mInput->hasTouch(0))
        mDisplay->mViewport.pick(mInput->getTouchPosX(0), mInput->getTouchPosY(0), 1);
#else
    mDisplay->mViewport.pick(mInput->getMousePosX(), mInput->getMousePosY(), 1);
#endif
    
	if (getState() != EDIT)
	{
		if (mInput->wasKeyPressed(QI_KEY_RETURN))
			mDisplay->mPanelMode = !mDisplay->mPanelMode;

		if (mInput->wasKeyPressed('f'))
			mAudio->playForegroundMusic("snd/cleared.ogg");

		if (mInput->wasKeyPressed('q'))
			mPlayer->reset();
			mPlayer->save();

		if (mInput->wasKeyPressed('e') && getState() == PLAY)
			requestState(EDIT);

		if (mInput->wasKeyPressed('m'))
			gGame->requestState(Game::MENU);

		if (mInput->wasKeyPressed('p')){
			mProperties.setBool("paused", !mProperties.getBool("paused"));
		}

		if (mInput->wasKeyPressed('b'))
			backButtonPressed();

		if (mInput->wasKeyPressed('r'))
		{
			if (getState() == PLAY)
				handleCommand("level.restart");
			else if (getState() == MENU)
				mMenuScene->reload();
		}

		if (getState() == MOVIE && mInput->wasTouchPressed(0))
			handleCommand("game.menu");

		if (mInput->wasKeyPressed('d'))
		{
			QI_PRINT("-----------------------------");
			QI_PRINT("Main ResMan");
			mResMan->debugPrint();

			QI_PRINT("\nMenu ResMan");
			mMenuScene->mResMan.debugPrint();

			QI_PRINT("\nMovie ResMan");
			mMovieScene->mResMan.debugPrint();

			QI_PRINT("\nHud ResMan");
			mHudScene->mResMan.debugPrint();

			QI_PRINT("\nLevel ResMan");
			mLevel->mResMan->debugPrint();
			QI_PRINT("-----------------------------");
		}
	}

	mPaused = mProperties.getBool("paused");

	mAudio->update();
    	    
	mProperties.setString("level", mLevelName);

	if (mLevel->mTrigReset)
		mLevel->reset();

	if (mTrigHudReload)
	{
		mHudScene->reload();
		mTrigHudReload = false;

		if (getState() == PLAY && !mLevel->mReplay)
		{
			if (mAudio->isMusicEnabled())
				mAudio->playBackgroundMusic(mPlayer->getLevelMusicPath());
			else if (mAudio->isSoundEnabled())
				mAudio->playBackgroundMusic(mPlayer->getLevelMusicPath().replace(".ogg", "_silent.ogg"), true);
		}
	}

	mFrameTime = mFrameTimer.getTime();
	mFrameTimer.reset();
	if (mFrameTime > 0.0333f)
		mFrameTime = 0.0333f;
	if (gCaptureMode)
		mFrameTime = mTimeStep;
	mProperties.setInt("frame", mFrame);
	mProperties.setFloat("frameTime", mFrameTime);

	static float ttt = 0.0f;
	ttt += mFrameTime;

	bool two = false;
	
	if (!mPaused)
	{
		update();
		ttt-=0.01667f;
		if (ttt < 0.005f)
			ttt = 0.0f;
        else
			two = true;
			
		if (gCaptureMode)
			two = false;

		if (two)
		{
			QiInput inputSave = *mInput;
			mInput->registerBegin();
			mInput->registerEnd();
			update();
			*mInput = inputSave;
			ttt-=0.01667f;
			if (ttt > 0.01667f)
				ttt = 0.01667f;
		}	
	}
	else
	{
		//We need to tick script even if we don't update. Kind of icky. This is to get animations right.
		if (mLevel->mScript)
			mLevel->mScript->tick(false);
	}

	if (mAsyncThread)
	{
		mAsyncThread->mUpdates = 1;
		mAsyncThread->mBegin.signal();
	}
	else
	{
		if (!mPaused)
		{
			mLevel->updateAsync();
			if (two)
				mLevel->updateAsync();
            mLevel->syncBreakables();
		}
	}

    draw();

	if (mAsyncThread)
		mAsyncThread->mEnd.wait();

	GuiBox::tick();

	if (getState()==MENU && mTvDrawn && !mLevel->mLoaded)
		loadAttractLevel();

	if (getState() == EDIT)
		mEditor->update();

	if (mStoreTimer > 0.0f)
	{
		mStoreTimer -= mTimeStep;
		if (mStoreTimer < 0.0f)
		{
			//Abort purchase
			mStoreTimer = 0.0f;
			mInPurchase = false;
		}

		int status;
		
		if (mPurchase == "restore")
			status = storeGetRestoreStatus();
		else
			status = storeGetPurchaseStatus();
		//__android_log_print(ANDROID_LOG_INFO,"gs"," status = %d",status);
		//__android_log_print(ANDROID_LOG_INFO,"gs"," mPurchase = %s",mPurchase.c_str());
		if (status != STORE_FAILED && status != STORE_WAITING && (mPurchase == AD_FREECOINS
				|| mPurchase == AD_FREECOINS_MAIN)) {
			mPlayer->addCoins(status);
			mPlayer->save();
			mStoreTimer = 0.0f;
			mInPurchase = false;
		}

		//handle gift logic
		if(status != STORE_FAILED && status != STORE_WAITING && mPurchase == AD_GIFT){
			int g1 = status%10;
			int g2 = status/10;
#ifdef QI_ANDROID
			__android_log_print(ANDROID_LOG_INFO,"gs"," g1 = %d",g1);
			__android_log_print(ANDROID_LOG_INFO,"gs"," g2 = %d",g2);
#endif
			if(g1 == 1){
				mPlayer->addPowerUp("helmet", 1);
			}else if(g1 == 2){
				mPlayer->addPowerUp("banana", 1);
			}else if(g1 == 3){
				mPlayer->addPowerUp("baseball", 1);
			}
			if(g2 == 10){
				mPlayer->addCoins(1000);
			}else if(status == 20){
				mPlayer->addCoins(2000);
			}else if(status == 30){
				mPlayer->addCoins(3000);
			}else if(status == 40){
				mPlayer->unlockCharacter("stanley");
				mMenuScene->mScript.handleCommand("character stanley");
			}
			if(status > 0){
				mPlayer->save();
				mMenuScene->mScript.handleCommand("toggleShop");
			}
			mStoreTimer = 0.0f;
			mInPurchase = false;
		}
		if(mPurchase == AD_DIE && (status == STORE_FAILED || status == STORE_SUCCEEDED)){
			mProperties.setBool("paused", !mProperties.getBool("paused"));
			mLevel->mTrigReset = true;
		}
		if (status == 2000) {
			mPurchase = "com.mediocre.grannysmith.buycoins2000";
			confirmPurchase(mPurchase);
			mStoreTimer = 0.0f;
			mInPurchase = false;
		} else if (status == 5000) {
			mPurchase = "com.mediocre.grannysmith.buycoins5000";
			confirmPurchase(mPurchase);
			mStoreTimer = 0.0f;
			mInPurchase = false;
		} else if (status == 8000) {
			mPurchase = "com.mediocre.grannysmith.buycoins8000";
			confirmPurchase(mPurchase);
			mStoreTimer = 0.0f;
			mInPurchase = false;
		} else if (status == 15000) {
			mPurchase = "com.mediocre.grannysmith.buycoins15000";
			confirmPurchase(mPurchase);
			mStoreTimer = 0.0f;
			mInPurchase = false;
		} else if (status == 30000) {
			mPurchase = "com.mediocre.grannysmith.buycoins30000";
			confirmPurchase(mPurchase);
			mStoreTimer = 0.0f;
			mInPurchase = false;
		} else if (status == 50000) {
			mPurchase = "com.mediocre.grannysmith.buycoins50000";
			confirmPurchase(mPurchase);
			mStoreTimer = 0.0f;
			mInPurchase = false;
		}else if (status == STORE_SUCCEEDED)
		{
			confirmPurchase(mPurchase);
			mStoreTimer = 0.0f;
			mInPurchase = false;
		}

		if (status == STORE_FAILED)
		{
			mStoreTimer = 0.0f;
			mInPurchase = false;
		}
		if(status != STORE_WAITING
				&& mPurchase == AD_FREECOINS_MAIN && mGetGift){
			mGetGift = false;
			mPurchase = AD_GIFT;
			storeInitPurchase(AD_GIFT);
			mStoreTimer = 6000.0f;
			mInPurchase = true;
		}
	}
	else
		mInPurchase = false;
		
	mProperties.setBool("purchasing", mInPurchase);
	

#ifdef QI_IOS
	static bool first = true;
	if (first)
	{
		first = false;
		gameCenterInit();
		if (gameCenterIsAvailable())
			mProperties.setBool("gamecenteravailable", gameCenterIsAvailable() == 1);
	}
	gameCenterTick();
#endif

	if (mTrigMenuButton)
	{
		mTrigMenuButton = false;
		if (gGame->getState() == Game::PLAY)
			gGame->mHudScene->mScript.handleCommand("menubutton");
		else if (getState() == Game::MENU)
			gGame->mMenuScene->mScript.handleCommand("menubutton");
	}
	
	if (mTrigBackButton)
	{
		mTrigBackButton = false;
		if (getState() == Game::PLAY)
			mHudScene->mScript.handleCommand("backbutton");
		else if (getState() == Game::MENU)
		{
			if (mInPurchase)
				mInPurchase = false;
			else
				mMenuScene->mScript.handleCommand("backbutton");
		}
		else
			requestState(MENU);
	}
	if (mTrigBackFromResume)
	{
		mTrigBackFromResume = false;
		if (getState() == PLAY && !mProperties.getBool("paused"))
			mHudScene->mScript.handleCommand("showpausemenu");
	}
	
#if ENABLE_NET_ADS
	if (mHttpThread)
	{
		if (mHttpThread->mNewAds)
		{
			mPlayer->mProperties.setInt("adsrevision", mHttpThread->mAdsRevision);
			mPlayer->mProperties.setBool("adsfront", mHttpThread->mAdsFront);
			mPlayer->mProperties.setBool("adsplus", mHttpThread->mAdsPlus);
			mPlayer->mProperties.setBool("adsshown", false);
			mHttpThread->mNewAds = false;
			mPlayer->save();
			mMenuScene->mScript.handleCommand("checkads");
		}
	}
#endif
}


void Game::update()
{
	float bottom = mDisplay->mProperties.getFloat("visibleBottom");

	float scale = ((float)mDisplay->mWidth / 1024.0f);
	float mainButtonSize = 230.0f * scale;
	float puButtonSize = 105.0f * scale;

	mProperties.setBool("ctrlbanana", mInput->wasKeyPressed('1') || mInput->wasKeyPressed(QI_KEY_UP) || mInput->wasTouched(mDisplay->worldToPixel(QiVec2(50, bottom-300.0f)), puButtonSize));
	mProperties.setBool("ctrlbaseball", mInput->wasKeyPressed('2') || mInput->wasKeyPressed(QI_KEY_DOWN) ||  mInput->wasTouched(mDisplay->worldToPixel(QiVec2(970, bottom-300.0f)), puButtonSize));

	mProperties.setBool("ctrljump", mInput->isKeyDown(QI_KEY_RIGHT) || mInput->isKeyDown(QI_KEY_SPACE) || mInput->isTouched(mDisplay->worldToPixel(QiVec2(924.0f, bottom-90)), mainButtonSize));
	mProperties.setBool("ctrlcane", mInput->isKeyDown(QI_KEY_LEFT) || mInput->isKeyDown(QI_KEY_BACKSPACE) || mInput->isTouched(mDisplay->worldToPixel(QiVec2(100.0f, bottom-90)), mainButtonSize));

	if (mInput->isTouched(mDisplay->worldToPixel(QiVec2(50, bottom-270.0f)), 80*scale))
		mProperties.setBool("ctrlcane", false);
	if (mInput->isTouched(mDisplay->worldToPixel(QiVec2(970, bottom-270.0f)), 80*scale))
		mProperties.setBool("ctrljump", false);
		
	mDisplay->update();

	if (mPaused)
		return;

	mProperties.setFloat("totalTime", mGameTimer.getTime());
	mProperties.setFloat("stateFade", mStateFade);

	if (mLevel->mLoaded && (getState() == PLAY || (getState() == MENU && mTvDrawn)))
	{
		bool soundSave = mAudio->mSoundEnabled;
		if (getState() == MENU)
			mAudio->mSoundEnabled = false;
		mLevel->update();
		mAudio->mSoundEnabled = soundSave;
	}

	mFrame++;
}


void Game::draw()
{
	mRenderer->setViewport(mDisplay->mViewport);
	mRenderer->resetState();
	mRenderer->clear();

	if (getState() == LOADING)
	{
		QiRenderState state;
		state.shader = mGfx->m2DTexShader.getShader();
#ifdef QI_ANDROID
		if (gLoadingFrame < 2 && mDevice->hasTegra())
			state.texture[0] = gNvSplashScreen.getTexture();
		else
#endif
		state.texture[0] = gLoadingScreen.getTexture();
		mRenderer->setState(state);
		mGfx->drawFullScreen();
		return;
	}

	bool useFbo = false;
	bool drawLevel = mLevel->mLoaded && (mTvDrawn || getState() != MENU);

	if (drawLevel && mLevel->mReplay)
		useFbo = true;

	mTvDrawn = false;

	int wSave, hSave;

	if (useFbo)
	{
		mDisplay->mViewport.push();

		wSave = mDisplay->mWidth;
		hSave = mDisplay->mHeight;
		mDisplay->mFbo.useAsTarget();

		int w, h;
		if (getState() == MENU)
		{
			w = (mDisplay->mFbo.getWidth()*3)/4;
			h = (mDisplay->mFbo.getWidth()*9)/16;
			h = QiMin(h, mDisplay->mFbo.getHeight());
		}
		else
		{
			w = mDisplay->mFbo.getWidth();
			h = (mDisplay->mFbo.getWidth() * 10) / 16;
			h = QiMin(h, mDisplay->mFbo.getHeight());
		}
		mFboTexMax.set(w/(float)mDisplay->mFbo.getWidth(), h/(float)mDisplay->mFbo.getHeight());

		mDisplay->setGeometry(w, h, false);
		mRenderer->clear();
	}
	
	mDisplay->enterLevel();
	
	if (drawLevel || getState() == EDIT)
		mLevel->draw();
		
	if (getState() == EDIT)
		mEditor->draw();

	mDisplay->leaveLevel();

	if (useFbo && getState() == MENU)
		mHudScene->draw();
	
	if (useFbo)
	{
		mDisplay->mFbo.useDefaultTarget();

		mDisplay->mViewport.pop();
		gGame->mRenderer->setViewport(mDisplay->mViewport);

		mDisplay->setGeometry(wSave, hSave, true);
	}

	if (getState() == PLAY && mLevel->mReplay)
	{
		QiMatrix4 mat;
		mat.m[12] = 0.0f;
		mat.m[13] = 768.0f - 64.0f;
		mat.m[0] = 1024.0f;
		mat.m[5] = -768.0f + 128.0f;
		drawFbo(mat);
	}

	if (getState() == MENU)
		mMenuScene->draw();
	if (getState() == MOVIE)
	{
		if (gCaptureMode)
			gGame->mLevel->setTimeScale(gCapture ? 2.0f/(float)gCaptureSubFrames : 1.0f);	
		mMovieScene->draw();
	}

	if (getState() == PLAY)
	{
		if (!mLevel->mReplay && mLevel->mScript)
			mLevel->mScript->draw();
		mHudScene->draw();
	}

	if (mStateFade > 0.0f)
		mGfx->drawFullScreenSolid(QiColor(0, 0, 0, mStateFade));

	mDisplay->postDraw();
	//draw_MM();
}
void Game::draw_MM()
{
	mRenderer->setViewport(mDisplay->mViewport);
	mRenderer->resetState();
	mRenderer->clear();

	if (getState() == LOADING)
	{
		QiRenderState state;
		state.shader = mGfx->m2DTexShader.getShader();
#ifdef QI_ANDROID
		if (gLoadingFrame < 2 && mDevice->hasTegra())
			state.texture[0] = gNvSplashScreen.getTexture();
		else
#endif
		state.texture[0] = gLoadingScreen_MM.getTexture();
		mRenderer->setState(state);
		mGfx->drawFullScreen();
		return;
	}

	bool useFbo = false;
	bool drawLevel = mLevel->mLoaded && (mTvDrawn || getState() != MENU);

	if (drawLevel && mLevel->mReplay)
		useFbo = true;

	mTvDrawn = false;

	int wSave, hSave;

	if (useFbo)
	{
		mDisplay->mViewport.push();

		wSave = mDisplay->mWidth;
		hSave = mDisplay->mHeight;
		mDisplay->mFbo.useAsTarget();

		int w, h;
		if (getState() == MENU)
		{
			w = (mDisplay->mFbo.getWidth()*3)/4;
			h = (mDisplay->mFbo.getWidth()*9)/16;
			h = QiMin(h, mDisplay->mFbo.getHeight());
		}
		else
		{
			w = mDisplay->mFbo.getWidth();
			h = (mDisplay->mFbo.getWidth() * 10) / 16;
			h = QiMin(h, mDisplay->mFbo.getHeight());
		}
		mFboTexMax.set(w/(float)mDisplay->mFbo.getWidth(), h/(float)mDisplay->mFbo.getHeight());

		mDisplay->setGeometry(w, h, false);
		mRenderer->clear();
	}
	
	mDisplay->enterLevel();
	
	if (drawLevel || getState() == EDIT)
		mLevel->draw();
		
	if (getState() == EDIT)
		mEditor->draw();

	mDisplay->leaveLevel();

	if (useFbo && getState() == MENU)
		mHudScene->draw();
	
	if (useFbo)
	{
		mDisplay->mFbo.useDefaultTarget();

		mDisplay->mViewport.pop();
		gGame->mRenderer->setViewport(mDisplay->mViewport);

		mDisplay->setGeometry(wSave, hSave, true);
	}

	if (getState() == PLAY && mLevel->mReplay)
	{
		QiMatrix4 mat;
		mat.m[12] = 0.0f;
		mat.m[13] = 768.0f - 64.0f;
		mat.m[0] = 1024.0f;
		mat.m[5] = -768.0f + 128.0f;
		drawFbo(mat);
	}

	if (getState() == MENU)
		mMenuScene->draw();
	if (getState() == MOVIE)
	{
		if (gCaptureMode)
			gGame->mLevel->setTimeScale(gCapture ? 2.0f/(float)gCaptureSubFrames : 1.0f);	
		mMovieScene->draw();
	}

	if (getState() == PLAY)
	{
		if (!mLevel->mReplay && mLevel->mScript)
			mLevel->mScript->draw();
		mHudScene->draw();
	}

	if (mStateFade > 0.0f)
		mGfx->drawFullScreenSolid(QiColor(0, 0, 0, mStateFade));

	mDisplay->postDraw();
}

QiString Game::getProperty(const QiString& fullProp)
{
	int i = fullProp.getIndexOf(".");
	if (i != -1)
	{
		QiString domain = fullProp.substring(0, i);
		QiString prop = fullProp.substring(i+1);
		PropertyBag* pb = getPropertyBag(domain);
		if (pb)
			return pb->getString(prop);
		else
			logE("GetProperty: Object not found: " + domain);
	}
	else
	{
		logE("GetProperty: illegal argument: " + fullProp);
	}
	return "";
}

void Game::setProperty(const QiString& fullProp, const QiString& value)
{
	logI(fullProp + "=" + value);
	int i = fullProp.getIndexOf(".");
	if (i != -1)
	{
		QiString domain = fullProp.substring(0, i);
		QiString prop = fullProp.substring(i+1);
		PropertyBag* pb = getPropertyBag(domain);
		if (pb)
			pb->setString(prop, value);
		else
			logE("SetProperty: Object not found: " + domain);
	}
	else
	{
		logE("SetProperty: illegal argument: " + fullProp);
	}
}


PropertyBag* Game::getPropertyBag(const QiString& object)
{
	if (object == "game")
		return &mProperties;
	else if (object == "display")
		return &mDisplay->mProperties;
	else if (object == "audio")
		return &mAudio->mProperties;
	else if (object == "level")
		return &mLevel->mProperties;
	else if (object == "player")
		return &mPlayer->mProperties;
	else
		return NULL;
}


void Game::drawFbo(const QiMatrix4& mat)
{
	QiRenderState state;
	state.shader = mGfx->mSepiaShader.getShader();

	static bool disableSepia = false;	
	if (mInput->wasKeyPressed('x'))
	{
		disableSepia = !disableSepia;
		mProperties.setBool("disablesepia", disableSepia);
	}
	if (disableSepia)
		state.shader = mGfx->m2DTexShader.getShader();
		
	state.texture[0] = &mGfx->mGaussBlob;
	state.texScale[0] = mFboTexMax;
	mRenderer->setState(state);

	mDisplay->mFbo.useAsTexture();

	static float b = 1.0f;
	static float bVel = 0.0f;
	bVel = QiClamp(bVel + QiRnd(-0.02f, 0.02f), -0.03f, 0.03f);
	b = b + bVel;
	if (b > 1.2f)
	{
		b = 1.2f;
		bVel = 0.0f;
	}
	else if (b < 0.9f)
	{
		b = 0.9f;
		bVel = 0.0f;
	}
	mGfx->mSepiaShader.getShader()->setUniform("uSepia", 1.0f*b*0.33f, 0.9f*b*0.33f, 0.8f*b*0.33f, 1.0f);

	static float focus = 1.0f;
	focus = QiClamp(focus + QiRnd(-0.3f, 0.3f), 0.0f, 4.0f);
	mGfx->mSepiaShader.getShader()->setUniform("uFocus", QiClamp(focus, 0.0f, 1.0f));

	mGfx->drawRectangle(mat, 4);
}

#if  defined(QI_ANDROID)
#include <android/log.h>
#	define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, "Game", __VA_ARGS__))
#endif
int static adcount=0;
QiString Game::handleCommand(const QiString& cmd)
{
	logI(QiString("Command: ") + cmd);
	//LOGD("--------------%s",cmd.c_str());
	QiString word0 = cmd.getWord(0);

	if (word0 == "setproperty")
	{
		QiString p = cmd.getWord(1);
		if (p.contains("="))
		{
			QiString prop = p.substring(0, p.getIndexOf("="));
			QiString value = p.substring(p.getIndexOf("=")+1);
			setProperty(prop, value);
		}
	}

	if (word0 == "audio.playBackgroundMusic")
	{
		if(adcount==0)
			adcount++;
		else
			ShowAd();
		mAudio->update();
		mAudio->playBackgroundMusic(cmd.getWord(1));
	}

	else if (word0 == "audio.playForegroundMusic")
	{
		mAudio->update();
		mAudio->playForegroundMusic(cmd.getWord(1));
	}

	else if (word0 == "movie.start")
	{
		mMoviePath = cmd.getWord(1);
		requestState(MOVIE);
	}

	else if (word0 == "level.restart")
	{
		gGame->mPlayer->registerRestart(mLevel->mDude->mTransform.pos);
		gGame->mPlayer->save();
		mLevel->mReplay = false;
		mLevel->mPlayerMotion.clear();
		mLevel->mTrigReset = true;
	}

	else if (word0 == "level.replay")
	{
		showAds("show_ads");
		mLevel->mReplay = true;
		mLevel->mTrigReset = true;
		gGame->mDevice->setAlwaysOn(true);
		mReplayAccount++;
	}

	else if (word0 == "level.start")
	{
		mLevel->mResMan->clear();
		mLevel->mReplay = false;
		if (getState() == PLAY)
			mNextLevel = cmd.getWord(1);
		else
		{
			mLevelName = cmd.getWord(1);
			requestState(PLAY);
		}
	}

	else if (word0 == "level.startreplay")
	{
		mLevelName = cmd.getWord(1);
		mLevel->mPlayerMotion.clear();
		QiString levelName = mLevelName;
		if (isHard())
			levelName += "-hard";
		if (mResMan->load("user://replay-" + levelName + ".motion", mLevel->mPlayerMotion))
		{
			mLevel->mReplay = true;
			gGame->mDevice->setAlwaysOn(true);
			requestState(PLAY);
		}
	}

	else if (word0 == "level.skip")
	{
		QiString name = cmd.getWord(1);
		mPurchase = "com.mediocre.grannysmith.skip "+name;
		storeInitPurchase("com.mediocre.grannysmith.skip");
		mStoreTimer = 6000.0f;
		mInPurchase = true;
//		int price = 200;
//		if (mPlayer->getCoinCount() >= price)
//		{
//			mPlayer->addCoins(-price);
//			mPlayer->skipLevel(name);
//			mPlayer->save();
//		}
//		else
//			mMenuScene->mScript.handleCommand("showoutofcash");
	}

	else if (word0 == "game.quit")
		mDevice->quit();

	else if (word0 == "game.menu"){
		ShowAd();
		if (getState() != MENU)
			hideAds();
		requestState(MENU);
	}
		
	else if (word0 == "game.leaderboards")
	{
		backButtonPressed();
	}

	else if (word0 == "game.saveConfig")
		saveConfig();

	else if (word0 == "game.url")
		mDevice->visitUrl(cmd.getWord(1));

	else if (word0 == "game.difficulty")
	{
		mPlayer->mProperties.setBool("hard", cmd.getWord(1) == "hard");
		mPlayer->save();
	}

	else if (word0 == "hud.reload")
		mTrigHudReload = true;

	else if (word0 == "player.buy")
	{
		QiString item = cmd.getWord(1);
		if (item == "scruffy") {
			mPurchase = "com.mediocre.grannysmith.scruffy";
			storeInitPurchase(mPurchase);
			mStoreTimer = 6000.0f;
			mInPurchase = true;
		} else if(item == "stanley"){
			mPurchase = "com.mediocre.grannysmith.stanley";
			storeInitPurchase(mPurchase);
			mStoreTimer = 6000.0f;
			mInPurchase = true;
		} else if(item == "ouie"){
			mPurchase = "com.mediocre.grannysmith.ouie";
			storeInitPurchase(mPurchase);
			mStoreTimer = 6000.0f;
			mInPurchase = true;
		} else if (item == "removeAds") {
			mPurchase = AD_REMOVE;
			storeInitPurchase(mPurchase);
			mStoreTimer = 6000.0f;
			mInPurchase = true;
		}  else if (item == "freeCoins") {
			mPurchase = AD_FREECOINS;
			storeInitPurchase(mPurchase);
			mStoreTimer = 6000.0f;
			mInPurchase = true;
		} else if (item == "toMainMenu") {
			mPurchase = AD_FREECOINS_MAIN;
			storeInitPurchase(AD_FREECOINS_MAIN);
			mStoreTimer = 6000.0f;
			mInPurchase = true;
		} else if (item == "showPauseAds"){
			showAds("show_pause_ads");
		}
		else {
			int price = 0;
			if (item == "helmet")
				price = 2000;
			else if (item == "banana")
				price = 4000;
			else if (item == "baseball")
				price = 6000;
			if (mPlayer->getCoinCount() >= price) {
				mPlayer->addCoins(-price);
				mPlayer->addPowerUp(item, 5);
				mPlayer->save();
				return "1";
			} else {
				mPurchase = "com.mediocre.grannysmith.buycoins";
				storeInitPurchase(mPurchase);
				mStoreTimer = 6000.0f;
				mInPurchase = true;
			}
		}
	}

	else if (word0 == "player.buycoins")
	{
		//LOGD("--------------------------1");
		int amount = cmd.getWord(1).toInt();
		mPurchase = "";
		//LOGD("--------------------------%s----",mPurchase.c_str());
		if (amount == 1000)
			mPurchase = "com.mediocre.grannysmith.coins1000";
		if (amount == 3000)
			mPurchase = "com.mediocre.grannysmith.coins3000";
		if (mPurchase != "")
		{
		//LOGD("--------------------------2");
			storeInitPurchase(mPurchase);
		//LOGD("--------------------------3");

			mStoreTimer = 60.0f;
			mInPurchase = true;
		}
	}

	else if (word0 == "player.unlockall")
	{
		//mPurchase = IAP_UNLOCKALL;
		//storeInitPurchase(mPurchase);
		//mStoreTimer = 6000.0f;
		//mInPurchase = true;
	}

	else if (word0 == "player.restorepurchases")
	{
		mPurchase = "restore";
		storeInitRestore();
		mStoreTimer = 60.0f;
		mInPurchase = true;
	}

	else if (word0 == "player.rate")
	{
		mPlayer->mProperties.setBool("rated", true);
		mPlayer->save();
	}

	else if (word0 == "player.unlockallchar")
	{
		mPlayer->unlockAllCharacters();
		mPlayer->save();
	}
	else if (word0 == "player.selectcharacter")
	{
		mPlayer->selectCharacter(cmd.getWord(1));
		mPlayer->save();
	}
	else if (word0 == "player.ischaracteravailable")
	{
		if (mPlayer->isCharacterAvailable(cmd.getWord(1)))
			return "1";
		else
			return "0";
	}

	else if (word0 == "player.reset")
	{
		mPlayer->reset();
		mPlayer->save();
		mMenuScene->reload();			
	}

	else if (word0 == "player.adsshown")
	{
		mPlayer->mProperties.setBool("adsshown", true);
		mPlayer->save();
	}

	else
	{
		logE("Unknown command: " + cmd);
	}
	return "";
}


void Game::startLevel()
{
    mPlayer->setActiveLevel(mLevelName);

	QiMemoryStream<256> tmp;
	mResMan->load(mPlayer->getLevelPath(), tmp);
	mLevel->load(tmp, tmp.getSize());
	mHudScene->load("hud/main.lua");
	mLevel->start();

	mWallTime.reset();
	mUpdatedTime = 0.0f;
	mProperties.setBool("paused", false);
	
	if (getState() == PLAY)
	{
		if (mLevel->mReplay)
			mAudio->playBackgroundMusic("snd/replay.ogg");
		else
		{
			if (mAudio->isMusicEnabled())
				mAudio->playBackgroundMusic(mPlayer->getLevelMusicPath());
			else if (mAudio->isSoundEnabled())
				mAudio->playBackgroundMusic(mPlayer->getLevelMusicPath().replace(".ogg", "_silent.ogg"), true);
		}
	}
	
	//QiAllocator::printStats();
}


void Game::stopLevel()
{
	if (mAudio->mBackgroundMusicPath == "snd/replay.ogg")
		mAudio->playBackgroundMusic("menu/menu.ogg");

	if (!mAudio->isMusicEnabled())
		mAudio->stopBackgroundMusic();

	mLevel->stop();
	mLevel->clear();
	mHudScene->unload();
}


void Game::onEnterState(State state)
{
	Tweak::init(mDevice->getAssetPath() + "/tweak.config");
	logI(QiString("Entering state ") + state);
	if (state == MENU)
	{
		mDisplay->mEditPan.set(0,0);
		mDisplay->mEditZoom = 1.0f;
		mMenuScene->mScript.handleCommand("activate");
		//__android_log_print(ANDROID_LOG_INFO,"gs","onEnterState mReplayAccount = %d",mReplayAccount);
		if(mReplayAccount >= 3){
			showAds(AD_SHOP);
		}
	}
	if (state == MOVIE)
    {
		mMovieScene->load(mMoviePath);
        mDevice->setAlwaysOn(true);
    }
	if (state == EDIT)
		mEditor->init();
	if (state == PLAY){
		startLevel();
	}
}

void Game::onLeaveState(State state)
{
	if (state == LOADING)
	{
		gLoadingScreen.release();
		gLoadingScreen_MM.release();
		gNvSplashScreen.release();
	}
	if (state == MOVIE)
    {
		mMovieScene->unload();
        mDevice->setAlwaysOn(false);
		mAudio->playBackgroundMusic("menu/menu.ogg");
    }
	if (state == PLAY)
		stopLevel();

	if (state == MENU)
	{
		if (mLevel->mLoaded)
			stopLevel();
	}
}


void Game::setState(State state)
{
	if (mCurrentState != state)
	{
		onLeaveState(mCurrentState);
		mCurrentState = state; 
		mNextState = state;
		mStateFade = 0.0f; 
		onEnterState(state);
	}
}


void Game::logI(const char* msg)
{
	mDebug->log(msg, LOG_INFO);
}


void Game::logW(const char* msg)
{
	mDebug->log(msg, LOG_WARNING);
}


void Game::logE(const char* msg)
{
	mDebug->log(msg, LOG_ERROR);
}


void Game::confirmPurchase(const QiString& product){
	if(product == AD_REMOVE){
		mMenuScene->mScript.handleCommand("hideRemoveAdsBtn");
	}
	if (product == "com.mediocre.grannysmith.buycoins2000") {
		mPlayer->addCoins(2000);
		mPlayer->save();
	}
	if (product == "com.mediocre.grannysmith.buycoins5000") {
		mPlayer->addCoins(5000);
		mPlayer->save();
	}
	if (product == "com.mediocre.grannysmith.buycoins8000") {
		mPlayer->addCoins(8000);
		mPlayer->save();
	}
	if (product == "com.mediocre.grannysmith.buycoins15000") {
		mPlayer->addCoins(15000);
		mPlayer->save();
	}
	if (product == "com.mediocre.grannysmith.buycoins30000") {
		mPlayer->addCoins(30000);
		mPlayer->save();
	}
	if (product == "com.mediocre.grannysmith.buycoins50000") {
		mPlayer->addCoins(50000);
		mPlayer->save();
	}
	if (product == "com.mediocre.grannysmith.coins1000") {
		mPlayer->mProperties.setInt("iapcoins1000",
				mProperties.getInt("iapcoins1000") + 1);
		mPlayer->addCoins(1000);
		mPlayer->save();
	}

	if (product == "com.mediocre.grannysmith.coins3000") {
		mPlayer->mProperties.setInt("iapcoins3000",
				mProperties.getInt("iapcoins3000") + 1);
		mPlayer->addCoins(3000);
		mPlayer->save();
	}

	if (product == IAP_UNLOCKALL
			|| (product == "restore" && storeIsRestored(IAP_UNLOCKALL))) {
		mPlayer->mProperties.setBool("iapunlockall", true);
		mPlayer->unlockAll();
		mPlayer->addPowerUp("helmet", 5);
		mPlayer->addPowerUp("banana", 5);
		mPlayer->addPowerUp("baseball", 5);
		mPlayer->save();
	}

	if (product == "com.mediocre.grannysmith.scruffy") {
		mPlayer->unlockCharacter("scruffy");
		mMenuScene->mScript.handleCommand("character scruffy");
		mPlayer->save();
	}

	if(product == AD_DIE){
		mPlayer->mProperties.setBool("iapunlockall", true);
		mPlayer->unlockAll();
		mPlayer->addPowerUp("helmet", 5);
		mPlayer->addPowerUp("banana", 5);
		mPlayer->addPowerUp("baseball", 5);
		mPlayer->save();
		requestState(MENU);
	}

	if (product == AD_REPLAY) {
		mLevel->mReplay = true;
		mLevel->mTrigReset = true;
		gGame->mDevice->setAlwaysOn(true);
	}

	if (product == AD_RESTART) {
		mProperties.setBool("paused", !mProperties.getBool("paused"));
		gGame->mPlayer->registerRestart(mLevel->mDude->mTransform.pos);
		gGame->mPlayer->save();
		mLevel->mReplay = false;
		mLevel->mPlayerMotion.clear();
		mLevel->mTrigReset = true;
	}

	if (product == "com.mediocre.grannysmith.stanley") {
		mPlayer->unlockCharacter("stanley");
		mMenuScene->mScript.handleCommand("character stanley");
		mPlayer->save();
	}

	if (product == "com.mediocre.grannysmith.ouie") {
		mPlayer->unlockCharacter("ouie");
		mMenuScene->mScript.handleCommand("character ouie");
		mPlayer->save();
	}

	if (product == AD_SHOP){
		//__android_log_print(ANDROID_LOG_INFO,"gs","-----------AD_SHOP");
		mMenuScene->mScript.handleCommand("toggleShop");
	}

	QiString word0 = product.getWord(0);
	if(word0 == "com.mediocre.grannysmith.skip"){
		QiString name = product.getWord(1);
		mPlayer->skipLevel(name);
		mPlayer->save();
		mMenuScene->mScript.handleCommand("skippedlevel");
	}

	mMenuScene->mScript.handleCommand("purchased " + product);
}


bool Game::extraEffects()
{
	return (gGame->mDevice->hasTegra() && gGame->mDevice->getCpuCount() == 4);
}


bool Game::isHard() const
{
	return mPlayer->mProperties.getBool("hard");
}

void Game::showAds(const QiString& product)
{
	if (product == AD_SHOP){
		mReplayAccount = 0;
		mPurchase = AD_SHOP;
		storeInitPurchase(mPurchase);
		mStoreTimer = 6000.0f;
		mInPurchase = true;
	}else if (product == AD_DIE) {
		mDieAccount = 0;
		mPurchase = AD_DIE;
		storeInitPurchase(mPurchase);
		mStoreTimer = 6000.0f;
		mInPurchase = true;
		mProperties.setBool("paused", !mProperties.getBool("paused"));
	}else
		storeInitPurchase(product);
}

void Game::hideAds()
{
	storeInitPurchase("hide_ads");
}

void Game::die()
{
	mDieAccount++;
}

int Game::getDieCount()
{
//	__android_log_print(ANDROID_LOG_INFO,"gs"," mDieAccount = %d",mDieAccount);
	return mDieAccount;
}

//Android hardware buttons


void menuButtonPressed()
{
	if (gGame)
		gGame->mTrigMenuButton = true;
}


void backButtonPressed()
{
	if (gGame)
		gGame->mTrigBackButton = true;
}


void backFromResume()
{
	if (gGame)
		gGame->mTrigBackFromResume = true;
}
