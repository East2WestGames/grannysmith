/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "qi_base.h"

class Resource
{
	enum Type
	{
		UNKNOWN,
		TEXTURE,
		SOUND,
		SHADER
	};

	friend class ResMan;
public:
	Resource();
	Resource(const Resource& other);
	~Resource();

	void operator=(const Resource& other);
	void release();

	class QiTexture* getTexture();
	class QiAudioBuffer* getSound();
	class QiShader* getShader();

private:
	Resource(class ResMan* resMan, const QiString& path, void* pointer, Type type);

	class ResMan* mResMan;
	QiString mPath;
	void* mPointer;
	Type mType;
};


class ResMan
{
	friend class Resource;
public:
	ResMan();
	~ResMan();

	void clear();
	void clearUnused();

	void setAdditionalPath(const QiString& path);

	bool load(const QiString& path, QiOutputStream& outStream);
	bool save(const QiString& path, const void* data, int size);

	bool httpPost(const QiString& url, const void* data, int size);

	Resource acquireTexture(const QiString& path);
	Resource acquireSound(const QiString& path);
	Resource acquireShader(const QiString& path);

	const QiString& getErrorMessage() { return mErrorMessage; } 

	void debugPrint();

	static bool connectAssetServer(const QiString& host, float timeOut);
	static void disconnectAssetServer();
	static bool getFileFromAssetServer(const QiString& path, QiOutputStream& stream);

	bool mPreventUnload;

protected:
	void releaseResource(void* pointer);

	class Res
	{
	public:
		inline Res() : mRefCount(1), mPointer(NULL), mType(Resource::UNKNOWN) {}
		inline Res(const QiString& requestPath) : mRequestPath(requestPath), mRefCount(1), mPointer(NULL), mType(Resource::UNKNOWN) {}
		inline Res(const QiString& requestPath, QiTexture* tex) : mRequestPath(requestPath), mRefCount(1), mPointer(tex), mType(Resource::TEXTURE) {}
		inline Res(const QiString& requestPath, QiAudioBuffer* sound) : mRequestPath(requestPath), mRefCount(1), mPointer(sound), mType(Resource::SOUND) {}
		inline Res(const QiString& requestPath, QiShader* shader) : mRequestPath(requestPath), mRefCount(1), mPointer(shader), mType(Resource::SHADER) {}
		void release();

		QiString mRequestPath;
		int mRefCount;
		void* mPointer;
		Resource::Type mType;

		inline QiHash getHash() const { return mRequestPath.getHash(); }
		inline bool operator==(const Res& other) const { return mRequestPath == other.mRequestPath; }
	};
	
	void decRef(Res* resource);

	QiHashTable<Res> mResources;

	QiString mAssetPath;
	QiString mUserPath;
	QiString mAdditionalPath;

	QiString mErrorMessage;

public:
	static class QiTcpSocket* sAssetSocket;
};


