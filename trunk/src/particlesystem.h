/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "qi_gfx.h"
#include "resman.h"

class ParticleSystem
{
public:
	struct Particle
	{
		QiVec3 pos;
		QiVec3 vel;
		float angle;
		float angVel;
		float lifeTime;
		float age;
		int texture;
	};

	ParticleSystem();
	~ParticleSystem();

	void init(const QiVec2& texMin, const QiVec2& texMax, int rows, int cols, int maxParticles);
	bool load(const QiString& name);

	void spawn(const QiVec3& pos, const QiVec3& vel, int texture=-1);
	void update();
	void render();

	int mTextureRows;
	int mTextureCols;
	QiVec2 mTexMin;
	QiVec2 mTexMax;

	QiArray<Particle> mParticles;
	int mMaxParticleCount;

	float mMinLife;
	float mMaxLife;
	float mMinAngVel;
	float mMaxAngVel;

	float mStartRadius;
	float mEndRadius;
	float mStartAlpha;
	float mEndAlpha;

	float mGravity;
};

