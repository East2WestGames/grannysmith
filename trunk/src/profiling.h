/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once
#include "config.h"

#if ENABLE_PROFILING

#include "dresscode/dresscode.h"
#define PROFILE_FUNC() dcZone(__FUNCTION__)
#define PROFILE_OPEN() dcOpen("game")
#define PROFILE_CLOSE() dcClose()
#define PROFILE_PARAM(name, value) dcParam(name, value)
#define PROFILE_ZONE(name) dcZone(name)
#define PROFILE_PRINTF(...) dcPrintf(__VA_ARGS__)
#define PROFILE_FRAME() dcTick()

#else

#define PROFILE_FUNC() (void(0));
#define PROFILE_OPEN() (void(0));
#define PROFILE_CLOSE() (void(0));
#define PROFILE_PARAM(name, value) (value)
#define PROFILE_ZONE(name) (void(0));
#define PROFILE_PRINTF(...) (void(0));
#define PROFILE_FRAME() (void(0));

#endif

