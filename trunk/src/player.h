/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "qi_base.h"
#include "propertybag.h"
#include "config.h"

class Player
{
public:
	Player();
	~Player();

	void init();
	void cleanup();
	void reset();

	void load();
	void save();
	void encrypt(char* data, int size);
	void decrypt(char* data, int size);

	void load(QiInputStream& stream, int size, bool merge);
	void save(QiOutputStream& stream);

	int getCoinCount();
	void addCoins(int coins);

	void addPowerUp(const QiString& type, int count=1);
	void consumePowerUp(const QiString& type);
	int getPowerUpCount(const QiString& type);

	void registerPlayed();
	void registerCleared(int score, int apples);
	void registerDie(const QiVec2& pos);
	void registerRestart(const QiVec2& pos);
	
	QiString getCharacter(); 
	void selectCharacter(const QiString& name);
	bool isCharacterAvailable(const QiString& name);
	void unlockCharacter(const QiString& name);
	void unlockAllCharacters();

	QiString getLevelMusicPath();

	void skipLevel(const QiString& levelName);
	bool isSkipped(const QiString& levelName);
	QiString getLevelTitle(const QiString& levelName);

	int getScore(const QiString& levelName);
	int getApples(const QiString& levelName);
	QiString getLevelPath();

	bool isWorldAvailable(const QiString& worldName);
	bool isLevelAvailable(const QiString& levelName);

	void setActiveWorld(const QiString& worldName);
	void setActiveLevel(const QiString& levelName);
	QiString getActiveWorld() const;
	QiString getActiveLevel() const;
	
	int getWorldScore(const QiString& worldName);
	void updateLeaderboard(const QiString& worldName);

	void unlockAll();
	bool hasUnlockAll();
	void checkForUnlockedCharacters(bool notify);

	void registerAchievement(const QiString& achievement);

	PropertyBag mProperties;

	QiUInt64 mLastReportedStats;

private:
	QiArray<class PlayerLevel*> mLevels;
	QiArray<class PlayerWorld*> mWorlds;
	class PlayerLevel* mCurrentLevel;

	class PlayerLevel* getLevel(const QiString& name);
};
