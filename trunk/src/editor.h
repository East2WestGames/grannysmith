/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "app/qiundo.h"

#if ENABLE_EDITOR
#include "app/qimenu.h"
#include "propertypage.h"
#include "gfx/qitextrenderer.h"
#endif

class Editor : public QiUndoImplementation
{
public:
	enum State
	{
		IDLE,
		CREATE_BODY,
		MOVE_ENTITY,
		MOVE_VERTEX,
		MOVE_CONTROL_POINT,
		SELECT
	};

	Editor();

	void loadPlayerPath();

	void init();
	void update();
	void draw();

	void drawEntity(class Entity* e);
    
    virtual void loadUndoState(QiInputStream& stream, int size);
	virtual void saveUndoState(QiOutputStream& stream);

	QiArray<Entity*> mSelection;

	void select(Entity* e);
	void deselect(Entity* e);

#if ENABLE_EDITOR
	State mState;
	class Entity* mSelectedEntity;
	int mSelectedVertex;
	int mSelectedControlPoint;
	int mSelectedControlType;
	int mSelectedControlLine;

	QiVec2 mModifyPivot;

	QiMenu mMenu;
	QiVec2 mMenuPoint;

	enum PropertyMode
	{
		ENTITY,
		MOVE_Z,
		SCALE
	};
	PropertyMode mPropertyMode;

	PropertyBag mProperties;
	PropertyPage mPropertyPage;

	QiUndo mUndo;

	float mGridSize;
	void drawGrid();
	QiVec2 mGridPivot;

	QiTextRenderer mText;
	PropertyBag mClipboardProperties;

	QiArray<QiVec2> mPlayerPath;
	bool mWorkMode;
	
	struct HeatMapEntry
	{
		QiVec2 pos;
		float fraction;
	};
	
	QiArray<HeatMapEntry> mHeatMap;
#endif
};

