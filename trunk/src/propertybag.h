/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"
#include "qi_base.h"
#include "qi_gfx.h"
#include "qi_math.h"


const int PROPERTY_INVISIBLE = 1;
const int PROPERTY_VOLATILE = 2;


class Property
{
public:
	QiString mName;
	QiString mValue;
	QiString mInherited;
	QiString mDefault;
	int mFlags;
};


class PropertyBag
{
public:
	inline PropertyBag() { }
	inline ~PropertyBag() { clear(); }

	void clear();
	void reset(); // Reset all values to default

	void readXml(QiXmlParser& xml, bool create=false);
	void writeXml(QiXmlWriter& xml);

	int getPropertyCount();
	const Property* getProperty(int index) const;
	Property* getProperty(int index);

	Property* getProperty(const QiString& name);
	const Property* getProperty(const QiString& name) const;
	inline bool hasProperty(const QiString& name) const { return getProperty(name)!=NULL; }

	void add(const QiString& name, const QiString& value, int flags=0);
	void inheritFrom(const PropertyBag& parent);

	QiString getString(const QiString& name) const;
	void setString(const QiString& name, const QiString& value);

	int getInt(const QiString& name) const { return getString(name).toInt(); }
	void setInt(const QiString& name, int value) { setString(name, QiString() + value); }

	float getFloat(const QiString& name) const { return getString(name).toFloat(); }
	void setFloat(const QiString& name, float value) { setString(name, QiString() + value); }

	bool getBool(const QiString& name) const { return getString(name).toInt()!=0; }
	void setBool(const QiString& name, bool value) { setString(name, value ? "1" : "0"); }

	QiVec2 getVec2(const QiString& name) const { QiString tmp=getString(name); return QiVec2(tmp.getWord(0).toFloat(), tmp.getWord(1).toFloat()); }
	void setVec2(const QiString& name, const QiVec2& value) { setString(name, QiString() + value.x + QiString(" ") + value.y); }

	QiVec3 getVec3(const QiString& name) const { QiString tmp=getString(name); return QiVec3(tmp.getWord(0).toFloat(), tmp.getWord(1).toFloat(), tmp.getWord(2).toFloat()); }
	void setVec3(const QiString& name, const QiVec3& value) { setString(name, QiString() + value.x + QiString(" ") + value.y + QiString(" ") + value.z); }

	QiColor getColor(const QiString& name) const;

	void operator=(const PropertyBag& other);

protected:
	QiArrayInplace<Property, 10> mProperties;
};

