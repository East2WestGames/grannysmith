/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "config.h"
#include "profiling.h"

#include "qi_base.h"
#include "qi_simpleapp.h"
#include "qi_gfx.h"
#include "qi_file.h"
#include "qi_math.h"
#include "qi_init.h"

#include "game.h"
#include "display.h"
#include "device.h"

Device gDefaultDevice;

bool gCapture = false;
bool gCaptureMode = false;

int gCaptureSubFrames = 1;
float gaussGamma = 1.4f; // Higher = more blur

void app(QiCommandLine cmd, bool fullscreen)
{
	int width = 1136;
	int height = 640; //(width*9)/16;
	//width *= 0.9;
	//height *= 0.9;
	if (cmd.has("-r"))
	{
		width = (width * 4) / 3;
		height = (height * 4) / 3;
	}

	QiSimpleApp app("Granny Smith", int(width), int(height), fullscreen);

	gGame = QI_NEW Game(&gDefaultDevice);

	QiFileInputStream fin;
	if (cmd.has("-replay") && fin.open(cmd.get("-replay")))
	{
		cmd.consume("-replay", 1);
		gGame->mReplay = true;
		gGame->mReplayStream = &fin;
	}

	if (cmd.has("-ir"))
	{
		static QiFileOutputStream inputFile("input.bin");
		app.mInput.record(inputFile);
	}
	else if (cmd.has("-i"))
	{
		static QiFileInputStream inputFile("input.bin");
		app.mInput.playback(inputFile);
	}

	bool recordEnable = false;
	if (cmd.has("-r"))
	{
		recordEnable = true;
		gCapture = false;
		gCaptureMode = true;
		QiPath::remove("capture", true);
		QiPath::createDir("capture");
	}

	app.trapEscapeKey(false);
	app.trapArrowKeys(false);
	app.trapMouse(false);

	while(!app.shouldQuit())
	{
		{
			app.processInput();

			gGame->mDisplay->setGeometry(app.getWidth(), app.getHeight());
			*gGame->mInput = app.mInput;
			gGame->frame();

			static int frameNumber = 0;
			if (recordEnable && app.mInput.wasKeyPressed('+'))
			{
				gCapture = !gCapture;
				if (gCapture)
				{
					frameNumber = 0;
					QI_PRINT("Recording");
					QiPath::remove("capture", true);
					QiPath::createDir("capture");
				}
			}
			if (gCapture)
			{
				int accumulateFrames = gCaptureSubFrames;
				int outw = app.getWidth();
				int outh = app.getHeight();
				static unsigned int* acc = new unsigned int[outw*outh*3];
				if (frameNumber%accumulateFrames == 0)
					memset(acc, 0, outw*outh*3*4);

				unsigned char* tmp = (unsigned char*)QiAlloc(app.getWidth()*app.getHeight()*4);
				if ((frameNumber%accumulateFrames) < accumulateFrames)
				{
				glReadPixels(0, 0, app.getWidth(), app.getHeight(), GL_RGBA, GL_UNSIGNED_BYTE, tmp);
				float c = ((frameNumber%accumulateFrames)/(float)(accumulateFrames-1))*2.0f-1.0f;
				float f = QiGaussFilter(c, gaussGamma);
				if (accumulateFrames == 1)
					f = 1.0f;
				for(int y=0; y<app.getHeight(); y+=app.getWidth()/outw)
				{
					for(int x=0; x<app.getWidth(); x+=app.getHeight()/outh)
					{
						int source = (y*app.getWidth() + x)*4;
						int r = int(tmp[source+0]);
						int g = int(tmp[source+1]);
						int b = int(tmp[source+2]);
						int dest = (y*outw + x)*3;
						acc[dest+0] += (unsigned int)(r*f);
						acc[dest+1] += (unsigned int)(g*f);
						acc[dest+2] += (unsigned int)(b*f);
					}
				}
				}
				if (frameNumber%accumulateFrames == accumulateFrames-1)
				{
					int fn = frameNumber/accumulateFrames;
					QiString nr = QiString() + fn;
					if (fn < 1000)
						nr = QiString("0") + nr;
					if (fn < 100)
						nr = QiString("0") + nr;
					if (fn < 10)
						nr = QiString("0") + nr;
					QiString fileName = QiString("capture/frame-") + nr + ".jpg";
					QiFileOutputStream outStream(fileName);
					QiJpegEncoder enc;
					enc.init(outStream, outw, outh, 100);
					float f = 0.0f;
					for(int i=0; i<accumulateFrames; i++)
					 	f += QiGaussFilter(((i%accumulateFrames)/(float)(accumulateFrames-1))*2.0f-1.0f, gaussGamma);					
					f *= 1.1f;
					if (accumulateFrames == 1)
						f = 1.0f;
					unsigned char* bacc = (unsigned char*)acc;
					for(int i=0; i<app.getWidth()*app.getHeight()*3; i++)
						bacc[i] = (unsigned char)QiClamp(int(acc[i]/f), 0, 255);
					enc.encode(bacc, true);
				}
				QiFree(tmp);				
				frameNumber++;
			}
			
			//Swap buffers
			app.endRender();
			
			static QiTimer gVblTimer;
			while(gVblTimer.getTime() < 0.015f)
				QiThread::sleep(0.001f);
			gVblTimer.reset();

			//if (app.mInput.wasKeyPressed('q'))
			//	break;			
		}
	}

	QI_DELETE(gGame);
}


int main(int argc, char** argv)
{
	QiInit();
	QI_PRINT("Allocations " + QiGetAllocationCount());
	{
		QiCommandLine cmdLine(argc, argv);	
		app(cmdLine, cmdLine.has("-f"));
	}
	QI_PRINT("Allocations " + QiGetAllocationCount());
	QiShutdown();
	return 0;
}

