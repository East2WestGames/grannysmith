/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#pragma once

#include "config.h"

//#define ENABLE_TWEAK

#ifndef ENABLE_TWEAK

#define TWEAKF(name, value) value
#define TWEAKI(name, value) value
#define TWEAKB(name, value) value

class Tweak
{
public:
	static void init(const char* file) {}

	static float getFloat(const char* name, float value) { return value; }
	static int getInt(const char* name, int value) { return value; }
	static bool getBool(const char* name, bool value) { return value; }
};

#else

#define TWEAKF(name, value) Tweak::getFloat(name, value)
#define TWEAKI(name, value) Tweak::getInt(name, value)
#define TWEAKB(name, value) Tweak::getBool(name, value)

class Tweak
{
public:
	static void init(const char* file);

	static float getFloat(const char* name, float value);
	static int getInt(const char* name, int value);
	static bool getBool(const char* name, bool value);
};

#endif

