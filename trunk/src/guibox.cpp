/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "qi_app.h"
#include "guibox.h"
#include "game.h"
#include "gfx.h"
#include "resman.h"
#include "audio.h"
#include "display.h"
#include "device.h"

GuiBox* sModal[5] = {NULL, NULL, NULL, NULL, NULL};

static bool isModal(GuiBox* box)
{
	for(int i=0; i<5; i++)
	{
		if (sModal[i]==box)
			return true;
	}
	return false;
}

static void modal(GuiBox* box)
{
	if (isModal(box))
		return;
	for(int i=0; i<5; i++)
	{
		if (sModal[i]==NULL)
		{
			sModal[i] = box;
			break;
		}
	}
}

static void unmodal(GuiBox* box)
{
	for(int i=0; i<5; i++)
	{
		if (sModal[i]==box)
			sModal[i] = NULL;
	}
}

bool isAnythingModal()
{
	for(int i=0; i<5; i++)
	{
		if (sModal[i])
			return true;
	}
	return false;
}

static bool isEnabled(GuiBox* box)
{
	return (box->mEnabled && (!isAnythingModal() || isModal(box)));
}

static GuiBox::Selection* gSelectCandidate = NULL;
static GuiBox::Selection* gSelected = NULL;
static float gSelectDistance = QI_FLOAT_MAX;

float boxDistance(const QiVec2& mi, const QiVec2& ma, const QiVec2& p)
{
	if (p.x < mi.x)
	{
		if (p.y < mi.y)
			return length(p - QiVec2(mi.x, mi.y));
		else if (p.y > ma.y)
			return length(p - QiVec2(mi.x, ma.y));
		else
			return mi.x - p.x;
	}
	else if (p.x > ma.x)
	{
		if (p.y < mi.y)
			return length(p - QiVec2(ma.x, mi.y));
		else if (p.y > ma.y)
			return length(p - QiVec2(ma.x, ma.y));
		else
			return p.x - ma.x;
	}
	else
	{
		if (p.y < mi.y)
			return mi.y - p.y;
		else if (p.y > ma.y)
			return p.y - ma.y;
		else
			return -QiMin( QiMin(p.x-mi.x, ma.x-p.x), QiMin(p.y-mi.y, ma.y-p.y));
	}
}

GuiBox::GuiBox()
{
	mEnabled = true;
	mOutside = false;
	mTexture = NULL;
	mTextureSelect = NULL;
	mOverlayMode = false;
	mShade = false;
}


GuiBox::~GuiBox()
{
	unmodal(this);
}


void GuiBox::init(QiTexture* texture, QiTexture* textureSelect, bool overlayMode)
{
	mSelections.clear();
	mTexture = texture;
	mTextureSelect = textureSelect;
}


void GuiBox::init(QiInputStream& in, int length, ResMan* resMan)
{
	QiXmlParser xml;
	if (xml.parse(in, length))
	{
		if (xml.getName() == "ui")
		{
			mTextureResource = resMan->acquireTexture(xml.getAttribute("texture"));
			if (xml.getAttribute("shade") == "true")
				mShade = true;
			if (xml.getAttribute("select_overlay") != "")
			{
				mTextureSelectResource = resMan->acquireTexture(xml.getAttribute("select_overlay"));
				mTextureOverlayResource = resMan->acquireTexture(xml.getAttribute("overlay"));
				mOverlayMode = true;
			}
			else
			{
				mTextureSelectResource = resMan->acquireTexture(xml.getAttribute("selected"));
				mOverlayMode = false;
			}
			init(mTextureResource.getTexture(), mTextureSelectResource.getTexture());
			xml.enter();
			while(xml.isValid())
			{
				QiString name = xml.getName();
				if (name == "rect")
				{
					QiString id = xml.getAttribute("id");
					QiString cmd = xml.getAttribute("cmd");
					QiString coords = xml.getAttribute("coords");
					QiVec2 lower(coords.getWord(0).toFloat(), coords.getWord(1).toFloat());
					QiVec2 upper(coords.getWord(2).toFloat(), coords.getWord(3).toFloat());
					addSelection(id, lower, upper, cmd);
					int group = xml.getAttribute("group").toInt();
					bool selected = (xml.getAttribute("selected") == "true");
					if (group > 0)
						setRadio(id, group, selected);
				}
				if (name == "outside")
				{
					mOutsideCommand = xml.getAttribute("cmd");
				}
				xml.next();
			}
		}
	}
}


void GuiBox::clear()
{
	mSelections.clear();
	mSelectValue = "";
}


void GuiBox::addSelection(const QiString& id, const QiVec2& lower, const QiVec2& upper, const QiString& command)
{
	Selection& sel = mSelections.add();
	sel.id = id;
	sel.enabled = true;
	sel.lower = lower;
	sel.upper = upper;
	sel.command = command;
	sel.radioGroup = 0;
	sel.radioSelect = false;
}


void GuiBox::setRadio(const QiString& id, int group, bool selected)
{
	for(int i=0; i<mSelections.getCount(); i++)
	{
		if (mSelections[i].id == id)
		{
			mSelections[i].radioGroup = group;
			mSelections[i].radioSelect = selected;
		}
	}
}


void GuiBox::selectRadio(const QiString& id)
{
	//Find group
	int group = 0;
	for(int i=0; i<mSelections.getCount(); i++)
		if (mSelections[i].id == id)
			group = mSelections[i].radioGroup;

	for(int i=0; i<mSelections.getCount(); i++)
	{
		if (mSelections[i].radioGroup == group)
			mSelections[i].radioSelect = (mSelections[i].id == id);
	}
}


void GuiBox::select(int i)
{
	gGame->mAudio->playSound(gGame->mAudio->mClickUp.next());
	mSelectValue = mSelections[i].command;

	int rg = mSelections[i].radioGroup;
	if (rg)
	{
		for(int j=0; j<mSelections.getCount(); j++)
		{
			if (mSelections[j].radioGroup == rg)
				mSelections[j].radioSelect = false;
		}							
		mSelections[i].radioSelect = true;
	}
}


void GuiBox::render(const QiMatrix4& mat, const QiColor& color, bool doInput)
{
	QiRenderState state;
	state.color = color;
	state.blendMode = state.BLEND;
	state.texture[0] = mTexture;
	state.shader = gGame->mGfx->m2DTexShader.getShader();
	gGame->mRenderer->setState(state);
	gGame->mGfx->drawRectangle(mat);
	//mOutside = false;
	
	QiViewport& viewport = gGame->mDisplay->mViewport;
	viewport.push();
	viewport.transform(mat);
	mSelectValue = "";
	
	{
		{
			viewport.pickPush((int)this);
			for(int i=0; i<mSelections.getCount(); i++)
			{
				QiVec2 low(mSelections[i].lower.x/mTexture->getWidth(), mSelections[i].lower.y/mTexture->getHeight());
				QiVec2 high(mSelections[i].upper.x/mTexture->getWidth(), mSelections[i].upper.y/mTexture->getHeight());
				bool drawSelected = false;

				if (mOverlayMode)
				{
					QiRenderState state;
					state.color.set(1,1,1,1);
					state.blendMode = state.BLEND;
					state.shader = gGame->mGfx->m2DTexShader.getShader();
					state.texture[0] = mTextureOverlayResource.getTexture();

					gGame->mRenderer->setState(state);
					QiMatrix4 mat2 = mat;
					mat2 *= QiMatrix4(QiVec3(low.x, low.y, 0.0f));
					QiMatrix4 s;
					s.m[0] = high.x-low.x;
					s.m[5] = high.y-low.y;
					mat2 *= s;
					gGame->mGfx->drawRectangle(mat2);
				}
				
				if (mSelections[i].enabled)
				{
					float tolerance = gGame->mDevice->isPhone() ? 0.08f : 0.03f;
					bool picked = viewport.pickRect(i, low, high, tolerance);
					if (gGame->mInput->hasTouch(0) || gGame->mInput->wasTouchReleased(0))
					{			
									
						if (picked && isEnabled(this) && doInput)
						{
							if (gGame->mInput->wasTouchPressed(0))
							{
								QiVec2 p = viewport.unproject(gGame->mInput->getTouchPos(0).vec3()).vec2();
								float distance = boxDistance(low, high, p);
								if (distance < gSelectDistance)
								{
									gSelectCandidate = &mSelections[i];						
									gSelectDistance = distance;
								}
							}
							if (gSelected == &mSelections[i])
								drawSelected = true;
						}
					}

					if (drawSelected || (mSelections[i].radioSelect && mTextureSelect))
					{
						QiRenderState state;
						state.color.set(1,1,1,1);
						if (mShade)
							state.color.set(0.5f, 0.5f, 0.5f, 1.0f);
						state.blendMode = state.BLEND;
						state.shader = gGame->mGfx->m2DTexShader.getShader();
						state.texture[0] = mTextureSelect;
						if (!mOverlayMode)
						{
							state.texOffset[0] = low;
							state.texScale[0] = high-low;
						}
						gGame->mRenderer->setState(state);
						QiMatrix4 mat2 = mat;
						mat2 *= QiMatrix4(QiVec3(low.x, low.y, 0.0f));
						QiMatrix4 s;
						s.m[0] = high.x-low.x;
						s.m[5] = high.y-low.y;
						mat2 *= s;
						gGame->mGfx->drawRectangle(mat2);
					}

					if (picked && gSelected == &mSelections[i] && gGame->mInput->wasTouchReleased(0))
					{
						select(i);
					}
				}
			}
			if (gGame->mInput->wasTouchPressed(0) && mSelectValue == "" && !gSelected && !gSelectCandidate && doInput && isEnabled(this))
			{
				mOutside = true;
			}
			viewport.pickPop();

			if (gGame->mInput->wasTouchReleased(0) && !gSelected)
			{
				if (mOutside)
					mSelectValue = mOutsideCommand;
				mOutside = false;
			}
		}
	}
	
	viewport.pop();
}


QiString GuiBox::getSelection()
{
	return mSelectValue;
}


void GuiBox::setModal(bool enabled)
{
	if (enabled)
		modal(this);
	else
		unmodal(this);
}


void GuiBox::tick()
{
	if (gSelectCandidate)
	{
		gSelected = gSelectCandidate;
		gGame->mAudio->playSound(gGame->mAudio->mClickDown.next());
	}

	if (gGame->mInput->wasTouchReleased(0))
	{
		gSelected = NULL;
	}

	gSelectCandidate = NULL;
	gSelectDistance = QI_FLOAT_MAX;

	if (gGame->mInput->getTouchCount() == 0)
		gSelected = NULL;
}


void GuiBox::abortSelect()
{
	gSelected = NULL; 
}


void GuiBox::setEnabled(bool enabled)
{
	mEnabled = enabled;
}

