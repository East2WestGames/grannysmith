/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

const int STORE_WAITING = -1;
const int STORE_SUCCEEDED = -2;
const int STORE_FAILED = -3;

const int STORE_YES = 1;
const int STORE_NO = 2;

void storeInit();
void storeShutdown();
int storeEnabled();

void storeInitPurchase(const char* product);
int storeGetPurchaseStatus();
void ShowAd();
void storeInitRestore();
int storeGetRestoreStatus();
int storeIsRestored(const char* product);
void storeRestoreReset();

void storeInitAlert(const char* message);
void storeInitDialog(const char* message, const char* yes, const char* no);
int storeGetDialogStatus();
void storeResetDialog();

