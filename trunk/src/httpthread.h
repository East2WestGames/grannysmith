/* Granny Smith source code. Copyright 2011-2012, Mediocre AB */

#include "qi_base.h"
#include "resman.h"

class HttpThread : public QiThread
{
protected:
	virtual void run();

private:
	bool downloadFile(const QiString url, const QiString& filename);
	void checkBanners();
	bool reportStats();

	ResMan mResMan;

public:
	HttpThread();

	bool mNewAds;
	int mAdsRevision;
	bool mAdsFront;
	bool mAdsPlus;
};


