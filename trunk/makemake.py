basedir = "./"

import sys
sys.path.append(basedir + "tools")
import buildsys

name = "grannysmith"

cfg = []
cfg.append("release")
cfg.append("gl")
cfg.append("al")

src = []
src.append("src")

inc = []
inc.append(".")
inc.append("src/Box2D")
inc.append("src/world")
inc.append("src/qifw")

pre = []

flags = []

buildsys.buildMakefile(name, cfg, src, inc, pre, flags)
template = basedir + "tools/buildsys-vc2010-template.vcxproj"
buildsys.buildVc2010(template, name, cfg, src, inc, pre)

